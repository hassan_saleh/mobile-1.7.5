function AlertsIosClearCheckOnClick(eventobject) {
    return AS_Switch_fd37c81d67da4812bfc3decd982edeb1(eventobject);
}

function AS_Switch_fd37c81d67da4812bfc3decd982edeb1(eventobject) {
    if (frmAccountAlertsKA.checkClearanceSwitchAlert.selectedIndex === 1) {
        frmAccountAlertsKA.HiddenCheckClear.text = "false";
    } else {
        frmAccountAlertsKA.HiddenCheckClear.text = "true";
    }
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var controller = INSTANCE.getFormController("frmAccountAlertsKA");
    controller.performAction("saveData");
}