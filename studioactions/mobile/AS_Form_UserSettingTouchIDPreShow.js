function AS_Form_UserSettingTouchIDPreShow(eventobject) {
    return AS_Form_ceb90b0b375e41e693fb17f3602dc616(eventobject);
}

function AS_Form_ceb90b0b375e41e693fb17f3602dc616(eventobject) {
    frmUserSettingsTouchIdKA.flxSwitchOffTouchLogin.accessibilityConfig = {
        "a11yValue": geti18Value("i18n.common.touchIDEnable"),
        "a11yLabel": "",
        "a11yHint": ""
    };
    frmUserSettingsTouchIdKA.flxSwitchOnTouchLogin.accessibilityConfig = {
        "a11yValue": geti18Value("i18n.common.touchIDUnable"),
        "a11yLabel": "",
        "a11yHint": ""
    };
    // Edit for the Touch ID flexContainer
    frmUserSettingsTouchIdKA.imgCloseTouchId.accessibilityConfig = {
        "a11yValue": geti18Value("i18n.termDeposit.close"),
        "a11yLabel": "",
        "a11yHint": ""
    };
    frmUserSettingsTouchIdKA.Image096dc60e703ce4f.accessibilityConfig = {
        "a11yValue": geti18Value("i18n.fingerprint.ICON"),
        "a11yLabel": "",
        "a11yHint": ""
    };
    // Edit for the Touch ID flexContainer
}