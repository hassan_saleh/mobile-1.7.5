function AS_AnimateUsernameDownIphone(eventobject, changedtext) {
    return AS_TextField_ja28486c7a3043998424fea66ecabfa6(eventobject, changedtext);
}

function AS_TextField_ja28486c7a3043998424fea66ecabfa6(eventobject, changedtext) {
    if (kony.retailBanking.util.validation.isValidUsername(frmEnrolluserLandingKA.tbxUsernameKA.text)) {
        frmEnrolluserLandingKA.flxUnderlineUsername.skin = "sknFlxGreenLine";
        usernameFlag = true;
    } else {
        frmEnrolluserLandingKA.flxUnderlineUsername.skin = "sknFlxOrangeLine";
        usernameFlag = false;
    }
    frmEnrolluserLandingKA.flxUsernameValidation.isVisible = false;
    animateLabel("DOWN", "lblUsername", frmEnrolluserLandingKA.tbxUsernameKA.text);
}