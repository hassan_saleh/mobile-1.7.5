function AS_onEndEditingAccNoAddBeneiOS(eventobject, changedtext) {
    return AS_TextField_jdd8a709474e4942829ae99a8958dca2(eventobject, changedtext);
}

function AS_TextField_jdd8a709474e4942829ae99a8958dca2(eventobject, changedtext) {
    if (frmAddExternalAccountKA.externalAccountNumberTextField.text !== "") {
        frmAddExternalAccountKA.flxUnderlineAccountNumber.skin = "sknFlxGreenLine";
    } else {
        frmAddExternalAccountKA.flxUnderlineAccountNumber.skin = "sknFlxOrangeLine";
    }
    animateLabel("DOWN", "lblAccountNumber", frmAddExternalAccountKA.externalAccountNumberTextField.text);
}