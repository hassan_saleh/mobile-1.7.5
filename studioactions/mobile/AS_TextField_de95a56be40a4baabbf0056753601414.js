function AS_TextField_de95a56be40a4baabbf0056753601414(eventobject, changedtext) {
    if (frmRegisterUser.txtAccountNumber.text === null || frmRegisterUser.txtAccountNumber.text === "" || frmRegisterUser.txtAccountNumber.text.trim() === "") frmRegisterUser.flxBorderAccountNumber.skin = "skntextFieldDividerOrange";
    else frmRegisterUser.flxBorderAccountNumber.skin = "skntextFieldDividerGreen";
}