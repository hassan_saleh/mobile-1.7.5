function onClickTypesIPSRegistration(eventobject) {
    return AS_Button_d5bdddf90b68441f9306ca6944e82cf9(eventobject);
}

function AS_Button_d5bdddf90b68441f9306ca6944e82cf9(eventobject) {
    if (eventobject.id === "btnMobileNumber") {
        onSelect_REGISTRATION_TYPES("MOBILE", frmIPSRegistration.txtAlias);
        frmIPSRegistration.LblCountryCodeHint.setVisibility(true);
    } else if (eventobject.id === "btnAlias") {
        onSelect_REGISTRATION_TYPES("ALIAS", frmIPSRegistration.txtAlias);
        frmIPSRegistration.LblCountryCodeHint.setVisibility(false);
    }
}