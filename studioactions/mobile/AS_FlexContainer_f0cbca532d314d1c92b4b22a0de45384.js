function AS_FlexContainer_f0cbca532d314d1c92b4b22a0de45384(eventobject) {
    kony.print("gblFromModule::" + gblFromModule);
    if (gblFromModule == "RegisterUser") {
        frmLoginKA.show();
        frmRegisterUser.destroy();
    } else if (gblFromModule == "UpdateSMSNumber" && (previous_FORM !== null && previous_FORM.id === "frmManageCardsKA")) {
        frmManageCardsKA.show();
        gblToOTPFrom = "";
        gblFromModule = "";
        previous_FORM = null;
    } else if (gblFromModule == "UpdateSMSNumber") {
        callMyProfileData();
    } else if (gblFromModule == "UpdateMobileNumber") {
        frmManageCardsKA.show();
        gblToOTPFrom = "";
        gblFromModule = "";
    } else {
        gblFromModule = "ForgotPassword";
        gblReqField = "Username";
        initialiseAndCallEnroll();
        frmRegisterUser.destroy();
    }
}