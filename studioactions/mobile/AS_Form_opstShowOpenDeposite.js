function AS_Form_opstShowOpenDeposite(eventobject) {
    return AS_Form_a3b4e109cc5348e1943928cef50c9e7a(eventobject);
}

function AS_Form_a3b4e109cc5348e1943928cef50c9e7a(eventobject) {
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    if (frmOpenTermDeposit.lblFundDeductionAccountNumber.text === geti18Value("i18n.termDeposit.fundDeductionAccountNumberTitle")) {
        frmOpenTermDeposit.btnNextDeposite.setEnabled(false);
        frmOpenTermDeposit.txtDepositAmount.setEnabled(false);
        frmOpenTermDeposit.calOpenDeposite.setEnabled(false);
        frmOpenTermDeposit.btnMaturityInstruction.setEnabled(false);
        frmOpenTermDeposit.btnAccountToAddInterestTo.setEnabled(false);
    }
}