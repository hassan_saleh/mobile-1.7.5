function postShowFrmBills(eventobject) {
    return AS_Form_f042000a7fbd4d6ea21f76117fc2e17f(eventobject);
}

function AS_Form_f042000a7fbd4d6ea21f76117fc2e17f(eventobject) {
    if (frmBills.tbxAmount.text !== "") {
        frmBills.lblHiddenAmnt.text = frmBills.tbxAmount.text;
        animateLabel("UP", "lblAmount", frmBills.tbxAmount.text);
    }
}