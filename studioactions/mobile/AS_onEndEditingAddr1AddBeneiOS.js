function AS_onEndEditingAddr1AddBeneiOS(eventobject, changedtext) {
    return AS_TextField_j627eb3366ec4252ada4c890f2cb0a08(eventobject, changedtext);
}

function AS_TextField_j627eb3366ec4252ada4c890f2cb0a08(eventobject, changedtext) {
    if (frmAddExternalAccountKA.tbxAddress1.text !== "") {
        frmAddExternalAccountKA.flxUnderlineAddress1.skin = "sknFlxGreenLine";
    } else {
        frmAddExternalAccountKA.flxUnderlineAddress1.skin = "sknFlxOrangeLine";
    }
    animateLabel("DOWN", "lblAddress1", frmAddExternalAccountKA.tbxAddress1.text);
}