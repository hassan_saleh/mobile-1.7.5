function AS_TextField_a42d4786a77d4a768b4ec4fca300a730(eventobject, changedtext) {
    onendeditingCaredless();
    frmCardlessTransaction.txtFieldAmount.maxTextLength = 20;
    frmCardlessTransaction.txtFieldAmount.text = formatAmountwithcomma(frmCardlessTransaction.lblHiddenAmount.text, getDecimalNumForCurr(frmCardlessTransaction.lblCurrencyCode.text));
}