function onClickMyProfileSettings(eventobject) {
    return AS_FlexContainer_a58e6f56b73946a8895d63fb80aa38e4(eventobject);
}

function AS_FlexContainer_a58e6f56b73946a8895d63fb80aa38e4(eventobject) {
    var appContext = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var frmUserSettingsMyProfileKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmUserSettingsMyProfileKAConfig);
    var frmUserSettingsMyProfileKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmUserSettingsMyProfileKAController", appContext, frmUserSettingsMyProfileKAModelConfigObj);
    var frmUserSettingsMyProfileKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmUserSettingsMyProfileKAControllerExtension", frmUserSettingsMyProfileKAControllerObj);
    frmUserSettingsMyProfileKAControllerObj.setControllerExtensionObject(frmUserSettingsMyProfileKAControllerExtObj);
    var frmUserSettingsMyProfileKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmUserSettingsMyProfileKAFormModel", frmUserSettingsMyProfileKAControllerObj);
    var frmUserSettingsMyProfileKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmUserSettingsMyProfileKAFormModelExtension", frmUserSettingsMyProfileKAFormModelObj);
    frmUserSettingsMyProfileKAFormModelObj.setFormModelExtensionObj(frmUserSettingsMyProfileKAFormModelExtObj);
    appContext.setFormController("frmUserSettingsMyProfileKA", frmUserSettingsMyProfileKAControllerObj);
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var controller = INSTANCE.getFormController("frmUserSettingsMyProfileKA");
    var navObject = new kony.sdk.mvvm.NavigationObject();
    navObject.setRequestOptions("form", {
        "headers": {
            "session_token": kony.retailBanking.globalData.session_token
        },
        "queryParams": {
            "custId": custid,
            "language": kony.store.getItem("langPrefObj") == "en" ? "eng" : "ara"
        }
    });
    controller.loadDataAndShowForm(navObject);
}