function AS_onAddBeneficiaryClick(eventobject) {
    return AS_Button_ac78a366be28472dac76bef8a8641a1c(eventobject);
}

function AS_Button_ac78a366be28472dac76bef8a8641a1c(eventobject) {
    frmAddExternalAccountKA.destroy();
    kony.boj.detailsForBene.flag = false;
    kony.boj.addBeneVar.selectedType = "BOJ";
    gblFromModule = "";
    navigation_STORE_PREVIOUS_FORM(frmAddExternalAccountHome);
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var listController = INSTANCE.getFormController("frmAddExternalAccountKA");
    var navObject = new kony.sdk.mvvm.NavigationObject();
    var datamodelflxAddressKA = new kony.sdk.mvvm.DataModel();
    navObject.setDataModel(null, kony.sdk.mvvm.OperationType.ADD, "form");
    navObject.setRequestOptions("form", {
        "headers": {}
    });
    listController.performAction("navigateTo", ["frmAddExternalAccountKA", navObject]);
}