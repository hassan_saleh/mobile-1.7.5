function onEndEditingAliasJoMoPayRegistrationAndroid(eventobject, changedtext) {
    return AS_TextField_e1d39ff494e447148c03a0aa64418b15(eventobject, changedtext);
}

function AS_TextField_e1d39ff494e447148c03a0aa64418b15(eventobject, changedtext) {
    animate_NEWCARDSOPTIOS("DOWN", "lblAliasJMPRstatic", frmJoMoPayRegistration.txtAlias.text);
    if (frmJoMoPayRegistration.txtAlias.text !== "" && frmJoMoPayRegistration.txtAlias.text !== null && validateAliasField(frmJoMoPayRegistration.txtAlias.text, frmJoMoPayRegistration.txtAlias)) {
        frmJoMoPayRegistration.CopyflxBorderjomopayID0b6963ff7a40e46.skin = "skntextFieldDividerGreen";
    } else {
        frmJoMoPayRegistration.CopyflxBorderjomopayID0b6963ff7a40e46.skin = "skntextFieldDividerOrange";
    }
}