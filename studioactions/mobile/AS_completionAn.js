function AS_completionAn(eventobject) {
    return AS_Video_f9d8e3f5a48644139d95d9e420528693(eventobject);
}

function AS_Video_f9d8e3f5a48644139d95d9e420528693(eventobject) {
    var langFlag = kony.store.getItem("langFlagSettingObj");
    if (langFlag) {
        var langSelected = kony.store.getItem("langPrefObj");
        if (langSelected === "en") {
            onClickEnglish();
        } else {
            onClickArabic();
        }
        frmLoginKA.show();
    } else {
        frmLanguageChange.show();
    }
}