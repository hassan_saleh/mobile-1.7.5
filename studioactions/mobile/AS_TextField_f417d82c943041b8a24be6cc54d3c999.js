function AS_TextField_f417d82c943041b8a24be6cc54d3c999(eventobject, changedtext) {
    animateLabel("DOWN", "lblUsername", frmEnrolluserLandingKA.tbxUsernameKA.text);
    animateLabel("UP", "lblPassword", frmEnrolluserLandingKA.tbxPasswordKA.text);
    if (gblFromModule !== "RegisterUser" && gblFromModule !== "ChangePassword" && gblFromModule !== "ChangeUsername") {
        ShowLoadingScreen();
        callSave();
    }
}