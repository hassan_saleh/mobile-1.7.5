function AS_TextField_h5b4ea87deef4c27936ca96e77c5c288(eventobject, changedtext) {
    try {
        if (frmLoginKA.tbxusernameTextField.text !== null && frmLoginKA.tbxusernameTextField.text !== "") {
            frmLoginKA.borderBottom.skin = "skntextFieldDividerGreen";
        } else {
            frmLoginKA.borderBottom.skin = "skntextFieldDivider";
        }
        animateLabel("DOWN", "lblUserName", frmLoginKA.tbxusernameTextField.text);
        animateLabel("UP", "lblPassword", frmLoginKA.passwordTextField.text);
    } catch (err) {
        kony.print("err in pwd::" + err);
    }
}