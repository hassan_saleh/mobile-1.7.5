function ondoneJIDRegJP(eventobject, changedtext) {
    return AS_TextField_c690bd23793846d593b62f10793b55a2(eventobject, changedtext);
}

function AS_TextField_c690bd23793846d593b62f10793b55a2(eventobject, changedtext) {
    animate_NEWCARDSOPTIOS("DOWN", "lblJomopayIDstatic", frmJoMoPayRegistration.txtJomopayID.text);
    animate_NEWCARDSOPTIOS("UP", "lblNationalID", frmJoMoPayRegistration.txtNationalID.text);
    if (frmJoMoPayRegistration.txtJomopayID.text !== "" && frmJoMoPayRegistration.txtJomopayID.text !== null) {
        frmJoMoPayRegistration.flxBorderjomopayID.skin = "skntextFieldDividerGreen";
    } else {
        frmJoMoPayRegistration.flxBorderjomopayID.skin = "skntextFieldDividerJomoPay";
    }
}