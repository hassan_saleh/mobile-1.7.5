function animateRegisterPasswordLabelDown(eventobject, changedtext) {
    return AS_TextField_g7aac657578b42b2a48160c604f9046e(eventobject, changedtext);
}

function AS_TextField_g7aac657578b42b2a48160c604f9046e(eventobject, changedtext) {
    if (kony.retailBanking.util.validation.isValidPassword(frmEnrolluserLandingKA.tbxPasswordKA.text)) frmEnrolluserLandingKA.flxPasswordValidation.isVisible = false;
    animateLabel("DOWN", "lblPassword", frmEnrolluserLandingKA.tbxPasswordKA.text);
    animateLabel("UP", "lblConfirmPassword", frmEnrolluserLandingKA.tbxConfrmPwdKA.text);
}