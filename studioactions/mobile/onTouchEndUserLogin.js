function onTouchEndUserLogin(eventobject, x, y) {
    return AS_Label_ac7dbf71f91a4306b4e64bbbd005d013(eventobject, x, y);
}

function AS_Label_ac7dbf71f91a4306b4e64bbbd005d013(eventobject, x, y) {
    var currentForm = kony.application.getCurrentForm();
    if (currentForm.id === "frmLoginKA" || currentForm.id === "frmLanguageChange") {
        currentForm.tbxusernameTextField.text = "";
        currentForm.borderBottom.skin = "skntextFieldDivider";
        animateLabel("DOWN", "lblUserName", currentForm.tbxusernameTextField.text);
        currentForm.lblInvalidCredentialsKA.isVisible = false;
    }
}