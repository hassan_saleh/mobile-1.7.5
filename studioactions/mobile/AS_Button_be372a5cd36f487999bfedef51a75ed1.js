function AS_Button_be372a5cd36f487999bfedef51a75ed1(eventobject) {
    var currForm = kony.application.getCurrentForm().id;
    if (currForm !== "frmAccountsLandingKA") {
        if (kony.sdk.isNetworkAvailable()) {
            frmAccountsLandingKA.flxDeals.setVisibility(false);
            frmAccountsLandingKA.flxAccountsMain.setVisibility(true);
            frmAccountsLandingKA.flxAccountsMain.left = "0%"
            frmAccountsLandingKA.flxLoans.setVisibility(false);
            frmAccountsLandingKA.flxAccountsMain.zIndex = 1;
            frmAccountsLandingKA.flxDeals.zIndex = 1;
            frmAccountsLandingKA.flxLoans.zIndex = 1;
            frmAccountsLandingKA.forceLayout();
            accountDashboardDataCall();
            kony.print("Nav accounts Dadhboard:::");
        } else {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
        }
    } else {
        kony.print("same form:::");
    }
}