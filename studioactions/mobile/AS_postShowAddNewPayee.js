function AS_postShowAddNewPayee(eventobject) {
    return AS_Form_b008a95f9eab4724bfabe353e0e3f53d(eventobject);
}

function AS_Form_b008a95f9eab4724bfabe353e0e3f53d(eventobject) {
    if (kony.boj.addBillerFromQuickPay) {
        kony.boj.restoreBillerTabDetails();
        frmAddNewPayeeKA.tbxBillerNumber.text = frmNewBillKA.tbxBillerNumber.text.replace(/\s/g, "");
        kony.boj.storeDetailsAddBiller("BillerNumber", frmAddNewPayeeKA.tbxBillerNumber.text);
        animateLabel("UP", "lblBillerNumber", frmAddNewPayeeKA.tbxBillerNumber.text);
        kony.boj.addBillerFromQuickPay = false;
    }
}