function onTouchEndCardStatement_CardDetails(eventobject, x, y) {
    return AS_Label_b2f6f14a22e34b8295b1ea40b1d103ca(eventobject, x, y);
}

function AS_Label_b2f6f14a22e34b8295b1ea40b1d103ca(eventobject, x, y) {
    // navigate_StandingInstructions("Card Statement");
    gblDownloadPDFFlow = false;
    serv_cardStatement({
        "Year": (new Date().getFullYear()).toFixed(),
        "Month": (new Date().getMonth() + 1) < 10 ? "0" + (new Date().getMonth() + 1) : new Date().getMonth() + 1
    });
}