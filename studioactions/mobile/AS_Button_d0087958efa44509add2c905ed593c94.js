function AS_Button_d0087958efa44509add2c905ed593c94(eventobject) {
    function SHOW_ALERT__5a41f520b2a14f5090dd3309be82f65b_True() {
        frmAccountDetailKA.flxAccountDetailNavOptKA.isVisible = false;
        kony.sdk.mvvm.LogoutAction();
    }

    function SHOW_ALERT__5a41f520b2a14f5090dd3309be82f65b_False() {}

    function SHOW_ALERT__5a41f520b2a14f5090dd3309be82f65b_Callback(response) {
        if (response === true) {
            SHOW_ALERT__5a41f520b2a14f5090dd3309be82f65b_True();
        } else {
            SHOW_ALERT__5a41f520b2a14f5090dd3309be82f65b_False();
        }
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": kony.i18n.getLocalizedString("i18n.common.confirmSignOut"),
        "yesLabel": kony.i18n.getLocalizedString("i18n.common.signOut"),
        "noLabel": kony.i18n.getLocalizedString("i18n.common.cancel"),
        "message": kony.i18n.getLocalizedString("i18n.common.signOutAlert"),
        "alertHandler": SHOW_ALERT__5a41f520b2a14f5090dd3309be82f65b_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    });
}