function onTextChangeApplyCardsSupplementaryCardLimit(eventobject, changedtext) {
    return AS_TextField_ddf88f993079431789c712f1ad2a85fb(eventobject, changedtext);
}

function AS_TextField_ddf88f993079431789c712f1ad2a85fb(eventobject, changedtext) {
    if (new RegExp(/[^0-9]/g).test(frmApplyNewCardsKA.txtSuplementryCardLimit.text)) {
        frmApplyNewCardsKA.flxBorderSuplementryCardLimit.skin = "sknFlxOrangeLine";
    } else {
        frmApplyNewCardsKA.flxBorderSuplementryCardLimit.skin = "skntextFieldDividerGreen";
    }
    validate_APPLYCARDSDETAILS();
}