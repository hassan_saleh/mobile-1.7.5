function onconfirmWebCharge(eventobject) {
    return AS_Button_d71b09906c6644798b54816a4d974cce(eventobject);
}

function AS_Button_d71b09906c6644798b54816a4d974cce(eventobject) {
    if (kony.sdk.isNetworkAvailable()) {
        gblToOTPFrom = "web";
        ShowLoadingScreen();
        callRequestOTP();
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
}