function onTouchEndATMPOSLimit(eventobject, x, y) {
    return AS_TextField_c0d7475dffd44177b9ea0cad91e4a7f9(eventobject, x, y);
}

function AS_TextField_c0d7475dffd44177b9ea0cad91e4a7f9(eventobject, x, y) {
    if (eventobject.id === "txtWithdrawalLimit") {
        animateLabel("UP", "lblWithdrawalLimitTitle", frmATMPOSLimit.txtWithdrawalLimit.text);
        animateLabel("DOWN", "lblPOSLimitTitle", frmATMPOSLimit.txtPOSLimit.text);
    } else if (eventobject.id === "txtPOSLimit") {
        animateLabel("DOWN", "lblWithdrawalLimitTitle", frmATMPOSLimit.txtWithdrawalLimit.text);
        animateLabel("UP", "lblPOSLimitTitle", frmATMPOSLimit.txtPOSLimit.text);
    }
}