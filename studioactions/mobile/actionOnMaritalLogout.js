function actionOnMaritalLogout(eventobject) {
    return AS_Button_b2154ed14dcc4e9cbdf9d3ff31805889(eventobject);
}

function AS_Button_b2154ed14dcc4e9cbdf9d3ff31805889(eventobject) {
    function SHOW_ALERT_ide_onClick_af6df3ee492b4e4bbbb51dee7d7a8247_True() {
        kony.print("Deleted");
    }

    function SHOW_ALERT_ide_onClick_af6df3ee492b4e4bbbb51dee7d7a8247_False() {}

    function SHOW_ALERT_ide_onClick_af6df3ee492b4e4bbbb51dee7d7a8247_Callback(response) {
        if (response === true) {
            SHOW_ALERT_ide_onClick_af6df3ee492b4e4bbbb51dee7d7a8247_True();
        } else {
            SHOW_ALERT_ide_onClick_af6df3ee492b4e4bbbb51dee7d7a8247_False();
        }
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": kony.i18n.getLocalizedString("i18n.common.confirmSignOut"),
        "yesLabel": kony.i18n.getLocalizedString("i18n.common.signOut"),
        "noLabel": kony.i18n.getLocalizedString("i18n.common.cancel"),
        "message": kony.i18n.getLocalizedString("i18n.common.signOutAlert"),
        "alertHandler": SHOW_ALERT_ide_onClick_af6df3ee492b4e4bbbb51dee7d7a8247_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    });
}