function AS_onclickRequestPinCardConfirm(eventobject) {
    return AS_Button_j5768a5e27cc40fdbbecfacd5de9472f(eventobject);
}

function AS_Button_j5768a5e27cc40fdbbecfacd5de9472f(eventobject) {
    if (frmRequestPinCard.lblBranch.text != geti18Value("i18n.cards.reasonhead3")) {
        frmRequestPinCard.flxInvalidAmountField.setVisibility(false);
        confirmCallrequestNewPinService();
    } else {
        frmRequestPinCard.flxInvalidAmountField.setVisibility(true);
    }
}