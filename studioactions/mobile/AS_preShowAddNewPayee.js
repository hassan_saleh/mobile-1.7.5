function AS_preShowAddNewPayee(eventobject) {
    return AS_Form_e1be7a4a05554298be877b4310e9fced(eventobject);
}

function AS_Form_e1be7a4a05554298be877b4310e9fced(eventobject) {
    // frmAddNewPayeeKA.flxScrollCompanyListKA.isVisible = false;
    // frmAddNewPayeeKA.tboxReenterAccountNumberKA.text = "";
    // populateCompanyList();
    if (kony.boj.BillerResetFlag) {
        frmAddNewPayeeKA.lblTitle.text = geti18Value("i18n.billsPay.AddNewBill");
        frmAddNewPayeeKA.lblNickName.skin = "sknLblNextDisabled";
        frmAddNewPayeeKA.lblBillerNumber.skin = "sknLblNextDisabled";
        frmAddNewPayeeKA.lblNickName.top = "40%";
        frmAddNewPayeeKA.lblBillerNumber.top = "35%";
        frmAddNewPayeeKA.lblArrowServiceType.isVisible = true;
        frmAddNewPayeeKA.flxConfirmPopUp.isVisible = false;
        frmAddNewPayeeKA.lblEditBillerNameTitile.isVisible = false;
        frmAddNewPayeeKA.lblEditBillerServiceTypeTitle.isVisible = false;
        frmAddNewPayeeKA.flxFormMain.isVisible = true;
        frmAddNewPayeeKA.lblArrowBillerName.isVisible = true;
        frmAddNewPayeeKA.flxServiceType.setEnabled(true);
        frmAddNewPayeeKA.flxBillerNumber.setEnabled(true);
        frmAddNewPayeeKA.btnPostPaid.setEnabled(true);
        kony.boj.selectedBillerType = "PostPaid";
        kony.boj.resetSelectedItemsBiller("PostPaid");
        kony.boj.resetSelectedItemsBiller("PrePaid");
        kony.boj.resetBiller();
    }
    if (kony.boj.addBillerFromQuickPay) {
        frmAddNewPayeeKA.lblTitle.text = geti18Value("i18n.billsPay.AddNewBill");
        frmAddNewPayeeKA.flxConfirmPopUp.isVisible = false;
        frmAddNewPayeeKA.flxFormMain.isVisible = true;
        frmAddNewPayeeKA.flxServiceType.setEnabled(true);
        frmAddNewPayeeKA.flxBillerNumber.setEnabled(true);
        kony.boj.partialResetBiller();
    }
}