function AS_Label_h5133568962441858f9f7854d3bc38e5(eventobject, x, y) {
    var currentForm = kony.application.getCurrentForm();
    if (currentForm.id === "frmLoginKA" || currentForm.id === "frmLanguageChange") {
        currentForm.tbxusernameTextField.text = "";
        currentForm.borderBottom.skin = "skntextFieldDivider";
        animateLabel("DOWN", "lblUserName", currentForm.tbxusernameTextField.text);
        currentForm.lblInvalidCredentialsKA.isVisible = false;
    }
}