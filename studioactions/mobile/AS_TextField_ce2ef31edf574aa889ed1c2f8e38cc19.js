function AS_TextField_ce2ef31edf574aa889ed1c2f8e38cc19(eventobject, changedtext) {
    if (frmAddNewPrePayeeKA.tbxAmount.text.match(/[^0-9.]/g) === null) {
        animateLabel("UP", "lblAmount", frmAddNewPrePayeeKA.tbxAmount.text);
        frmAddNewPrePayeeKA.tbxAmount.text = fixDecimal(frmAddNewPrePayeeKA.tbxAmount.text, "3");
        frmAddNewPrePayeeKA.lblHiddenAmount.text = frmAddNewPrePayeeKA.tbxAmount.text;
        nextFromNewPrepaidBene();
        changeUnderlineskin();
    }
}