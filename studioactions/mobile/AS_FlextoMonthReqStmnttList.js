function AS_FlextoMonthReqStmnttList(eventobject) {
    return AS_FlexContainer_ecdccdab719f44509bb637db78c72e99(eventobject);
}

function AS_FlexContainer_ecdccdab719f44509bb637db78c72e99(eventobject) {
    if (frmRequestStatementAccounts.lblYearFromValue.text != geti18Value("i18n.filtertransaction.year") && frmRequestStatementAccounts.lblYearToValue.text != geti18Value("i18n.filtertransaction.year") && frmRequestStatementAccounts.lblMonthFromValue.text != geti18Value("i18n.common.month")) {
        gblsubacclist = "toMonth";
        frmAccountType.show();
    } else {
        if (frmRequestStatementAccounts.lblYearFromValue.text == geti18Value("i18n.filtertransaction.year")) {
            frmRequestStatementAccounts.lblLineFY.skin = "sknFlxOrangeLine";
        }
        if (frmRequestStatementAccounts.lblYearToValue.text == geti18Value("i18n.filtertransaction.year")) {
            frmRequestStatementAccounts.lblLineTY.skin = "sknFlxOrangeLine";
        }
        if (frmRequestStatementAccounts.lblMonthFromValue.text == geti18Value("i18n.common.month")) {
            frmRequestStatementAccounts.lblLineFM.skin = "sknFlxOrangeLine";
        }
    }
}