function AS_Button_7cc70941cc98453bb52351eb11d5f68c(eventobject) {
    function SHOW_ALERT_ide_onClick_2371b78305e04c698b37f963d545cf31_True() {
        deviceRegFrom = "logout";
        removeSwipe();
        kony.sdk.mvvm.LogoutAction();
    }

    function SHOW_ALERT_ide_onClick_2371b78305e04c698b37f963d545cf31_False() {}

    function SHOW_ALERT_ide_onClick_2371b78305e04c698b37f963d545cf31_Callback(response) {
        if (response === true) {
            SHOW_ALERT_ide_onClick_2371b78305e04c698b37f963d545cf31_True();
        } else {
            SHOW_ALERT_ide_onClick_2371b78305e04c698b37f963d545cf31_False();
        }
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": kony.i18n.getLocalizedString("i18n.common.confirmSignOut"),
        "yesLabel": kony.i18n.getLocalizedString("i18n.common.signOut"),
        "noLabel": kony.i18n.getLocalizedString("i18n.common.cancel"),
        "message": kony.i18n.getLocalizedString("i18n.common.signOutAlert"),
        "alertHandler": SHOW_ALERT_ide_onClick_2371b78305e04c698b37f963d545cf31_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    });
}