function AS_TextField_jf26f8e7869840cf8bb7f2c89b793e63(eventobject, changedtext) {
    frmCardlessTransaction.flxBorderAmount.skin = "skntextFieldDividerGreen";
    frmCardlessTransaction.lblHiddenAmount.text = frmCardlessTransaction.txtFieldAmount.text;
    frmCardlessTransaction.txtFieldAmount.maxTextLength = 20;
    frmCardlessTransaction.txtFieldAmount.text = formatAmountwithcomma(frmCardlessTransaction.lblHiddenAmount.text, getDecimalNumForCurr(frmCardlessTransaction.lblCurrencyCode.text));
}