function onTouchEndShowPassLogin(eventobject, x, y) {
    return AS_Label_ce3c4c1c032a4d1f8bd14f37a120f61b(eventobject, x, y);
}

function AS_Label_ce3c4c1c032a4d1f8bd14f37a120f61b(eventobject, x, y) {
    var currentForm = kony.application.getCurrentForm();
    if ((currentForm.id === "frmLoginKA" || currentForm.id === "frmLanguageChange") && currentForm.passwordTextField.text !== "") {
        if (frmLoginKA.lblShowPass.text === "B") {
            currentForm.passwordTextField.secureTextEntry = true;
            frmLoginKA.lblShowPass.text = "A";
        } else {
            currentForm.passwordTextField.secureTextEntry = false;
            frmLoginKA.lblShowPass.text = "B";
        }
    }
}