function btnConfirmOnclickfrmConfirmTransfer(eventobject) {
    return AS_Button_6b9fcdd57f9145a9b4e38255741be7fb(eventobject);
}

function AS_Button_6b9fcdd57f9145a9b4e38255741be7fb(eventobject) {
    if (kony.boj.addBeneVar.selectedType === "BOJ" || gblTModule === "own" || gblTModule === "web") {
        kony.print("Do nothing:");
        popupCommonAlertDimiss();
        gblToOTPFrom = "OwnAccounts";
        ShowLoadingScreen();
        if (gblTModule == "send") callRequestOTP();
        else performOwnTransfer();
    } else {
        kony.print("Show");
        popupCommonAlertDimiss();
        gblToOTPFrom = "OwnAccounts";
        ShowLoadingScreen();
        if (gblTModule == "send") callRequestOTP();
        else performOwnTransfer();
    }

    function onClickYesConfirm() {
        popupCommonAlertDimiss();
        gblToOTPFrom = "OwnAccounts";
        ShowLoadingScreen();
        if (gblTModule == "send") callRequestOTP();
        else performOwnTransfer();
    }

    function onClickNoConfirm() {
        popupCommonAlertDimiss();
        frmNewTransferKA.show();
    }
}