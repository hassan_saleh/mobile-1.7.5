function AS_Switch_j693afe310994ef69c2b83a86de2c1a4(eventobject) {
    if (frmAuthorizationAlternatives.flxQuickBalance.height === "8%") {
        frmAuthorizationAlternatives.flxQuickBalance.height = "40%";
        addDatatoSegQuickBalance(1);
    } else {
        frmAuthorizationAlternatives.flxQuickBalance.height = "8%";
        addDatatoSegQuickBalance(0);
    }
}