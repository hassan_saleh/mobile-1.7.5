function onTextChangeApplyCardsValidation(eventobject, changedtext) {
    return AS_TextField_i0277a740492477aac6742914cc998ae(eventobject, changedtext);
}

function AS_TextField_i0277a740492477aac6742914cc998ae(eventobject, changedtext) {
    if (new RegExp(/[^A-Za-z\s]/g).test(frmApplyNewCardsKA.txtCardHolder.text)) {
        frmApplyNewCardsKA.lblHintCardHolder.setVisibility(true);
        frmApplyNewCardsKA.flxBorderCardHolder.skin = "sknFlxOrangeLine";
    } else {
        frmApplyNewCardsKA.lblHintCardHolder.setVisibility(false);
        frmApplyNewCardsKA.flxBorderCardHolder.skin = "skntextFieldDividerGreen";
    }
    validate_APPLYCARDSDETAILS();
}