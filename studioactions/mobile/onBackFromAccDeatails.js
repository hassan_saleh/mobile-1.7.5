function onBackFromAccDeatails(eventobject) {
    return AS_FlexContainer_iac662718cce4814ab90c37a01c81ae3(eventobject);
}

function AS_FlexContainer_iac662718cce4814ab90c37a01c81ae3(eventobject) {
    kony.print("gblTModule ::" + gblTModule);
    if (kony.newCARD.applyCardsOption === true) {
        frmApplyNewCardsKA.show();
        kony.newCARD.applyCardsOption = false;
    } else if (kony.application.getPreviousForm().id === "frmCardLinkedAccounts") {
        frmCardLinkedAccounts.show();
    } else if (gblTModule == "AccNumberStatement") {
        onClickAccBackToReqStmntScreen();
    } else if (gblTModule == "AccountOrder") {
        onClickAccBackToOrderCheckBookScreen();
    } else if (gblTModule === "BillPrePayNewBillKA") {
        frmPrePaidPayeeDetailsKA.show();
    } else if (gblTModule === "BulkPrePayPayment") {
        frmBulkPaymentKA.show();
    } else if (gblTModule === "AccountsForPayments" || gblTModule === "AccountsForTransfer") {
        kony.print("in accountdetails on back " + gblTModule);
        frmUserSettingsSiri.show();
    } else if (gblTModule === "CardLessTransaction") {
        frmCardlessTransaction.show();
    } else if (gblTModule === "CLIQ") {
        frmEPS.show();
    } else if (gblTModule === "IPSRegistration") {
        frmIPSRegistration.show();
    } else if (gblTModule === "OpenTermDeposite") {
        frmOpenTermDeposit.show();
    } else if (gblTModule === "OpenTermDepositInterestAccount") {
        frmOpenTermDeposit.show();
    } else {
        backFromAccDetails();
    }
}