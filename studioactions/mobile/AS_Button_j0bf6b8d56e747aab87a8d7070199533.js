function AS_Button_j0bf6b8d56e747aab87a8d7070199533(eventobject) {
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var listController = INSTANCE.getFormController("frmAddExternalAccountKA");
    var navObject = new kony.sdk.mvvm.NavigationObject();
    var datamodelflxAddressKA = new kony.sdk.mvvm.DataModel();
    navObject.setDataModel(null, kony.sdk.mvvm.OperationType.ADD, "form");
    //navObject.setRequestOptions("ListCountryKA",{"headers":{"session_token":kony.retailBanking.globalData.session_token}});
    navObject.setRequestOptions("form", {
        "headers": {
            "session_token": kony.retailBanking.globalData.session_token
        }
    });
    listController.performAction("navigateTo", ["frmAddExternalAccountKA", navObject]);
}