function AS_Form_PostShowFrmAddExternalAcc(eventobject) {
    return AS_Form_a3dcbc8f479042f2a850f706e6e247a9(eventobject);
}

function AS_Form_a3dcbc8f479042f2a850f706e6e247a9(eventobject) {
    if (gblLaunchModeOBJ.lauchMode && (!isEmpty(gblLaunchModeOBJ.accno) || !isEmpty(gblLaunchModeOBJ.iban))) {
        animateLabel("UP", "lblAccountNumber", "up", frmAddExternalAccountKA);
    }
}