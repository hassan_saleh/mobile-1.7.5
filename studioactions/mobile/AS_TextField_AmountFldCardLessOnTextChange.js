function AS_TextField_AmountFldCardLessOnTextChange(eventobject, changedtext) {
    return AS_TextField_b4bebc9adec14ab4b3d5251ed61301cc(eventobject, changedtext);
}

function AS_TextField_b4bebc9adec14ab4b3d5251ed61301cc(eventobject, changedtext) {
    /*if(checkNextCardless())
      frmCardlessTransaction.btnSubmit.setEnabled(true);
    else
      frmCardlessTransaction.btnSubmit.setEnabled(false);*/
    checkNextCardless();
    frmCardlessTransaction.lblHiddenAmount.text = frmCardlessTransaction.txtFieldAmount.text;
    frmCardlessTransaction.txtFieldAmount.text = fixDecimal(frmCardlessTransaction.txtFieldAmount.text, getDecimalNumForCurr(frmCardlessTransaction.lblCurrencyCode.text));
}