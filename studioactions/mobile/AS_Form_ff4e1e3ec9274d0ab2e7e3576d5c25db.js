function AS_Form_ff4e1e3ec9274d0ab2e7e3576d5c25db(eventobject) {
    // frmAddNewPayeeKA.flxScrollCompanyListKA.isVisible = false;
    // frmAddNewPayeeKA.tboxReenterAccountNumberKA.text = "";
    // populateCompanyList();
    if (kony.boj.BillerResetFlag) {
        frmAddNewPrePayeeKA.lblTitle.text = geti18Value("i18n.billsPay.AddNewBill");
        frmAddNewPrePayeeKA.lblNickName.skin = "sknLblNextDisabled";
        frmAddNewPrePayeeKA.lblBillerNumber.skin = "sknLblNextDisabled";
        frmAddNewPrePayeeKA.lblNickName.top = "40%";
        //frmAddNewPrePayeeKA.lblBillerNumber.top = "35%";
        frmAddNewPrePayeeKA.lblArrowServiceType.isVisible = true;
        frmAddNewPrePayeeKA.flxConfirmPopUp.isVisible = false;
        frmAddNewPrePayeeKA.lblEditBillerNameTitile.isVisible = false;
        frmAddNewPrePayeeKA.lblEditBillerServiceTypeTitle.isVisible = false;
        frmAddNewPrePayeeKA.flxFormMain.isVisible = true;
        frmAddNewPrePayeeKA.lblArrowBillerName.isVisible = true;
        frmAddNewPrePayeeKA.flxServiceType.setEnabled(true);
        frmAddNewPrePayeeKA.flxBillerNumber.setEnabled(true);
        //kony.boj.selectedBillerType = "PostPaid";
        kony.boj.selectedBillerType = "PrePaid";
        kony.boj.resetSelectedItemsBiller("PrePaid");
        kony.boj.resetPreBiller();
    }
    if (kony.boj.addBillerFromQuickPay) {
        frmAddNewPayeeKA.lblTitle.text = geti18Value("i18n.billsPay.AddNewBill");
        frmAddNewPayeeKA.flxConfirmPopUp.isVisible = false;
        frmAddNewPayeeKA.flxFormMain.isVisible = true;
        frmAddNewPayeeKA.flxServiceType.setEnabled(true);
        frmAddNewPayeeKA.flxBillerNumber.setEnabled(true);
        kony.boj.partialResetBiller();
    }
}