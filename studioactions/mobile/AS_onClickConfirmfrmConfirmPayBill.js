function AS_onClickConfirmfrmConfirmPayBill(eventobject) {
    return AS_Button_af1e16a18e254a7eb3bccc7c7134e02b(eventobject);
}

function AS_Button_af1e16a18e254a7eb3bccc7c7134e02b(eventobject) {
    gblToOTPFrom = "ConfirmPayBill";
    //callRequestOTP();
    if (frmConfirmPayBill.flxTotalAmount.isVisible === true) {
        var amnt = frmConfirmPayBill.lblTotalAmount.text;
        amnt = amnt.substring(0, amnt.length - 4);
        if (parseFloat(kony.store.getItem("BillPayfromAcc").availableBalance) >= parseFloat(amnt) && kony.store.getItem("BillPayfromAcc").ccydesc === "JOD") {
            ShowLoadingScreen();
            performBillPayPostpaid();
        } else if (kony.store.getItem("BillPayfromAcc").ccydesc !== "JOD") {
            ShowLoadingScreen();
            performBillPayPostpaid();
        } else {
            customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.error.InsufficientFundInAccount"), popupCommonAlertDimiss, "");
        }
    } else {
        ShowLoadingScreen();
        performBillPayPostpaid();
    }
}