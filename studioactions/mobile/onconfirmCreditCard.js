function onconfirmCreditCard(eventobject) {
    return AS_Button_i2d725944f6a469da9acde0682c13ac9(eventobject);
}

function AS_Button_i2d725944f6a469da9acde0682c13ac9(eventobject) {
    if (kony.sdk.isNetworkAvailable()) {
        gblToOTPFrom = "credit";
        ShowLoadingScreen();
        //callRequestOTP(); 
        payFromCreditCard();
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
}