function AS_Label_cb8ecc8b5e8d44b6a3c9e1c053fd47da(eventobject, x, y) {
    var currentForm = kony.application.getCurrentForm();
    if (currentForm.id === "frmLoginKA" || currentForm.id === "frmLanguageChange") {
        currentForm.tbxusernameTextField.text = "";
        currentForm.borderBottom.skin = "skntextFieldDivider";
        animateLabel("DOWN", "lblUserName", currentForm.tbxusernameTextField.text);
        currentForm.lblInvalidCredentialsKA.isVisible = false;
    }
}