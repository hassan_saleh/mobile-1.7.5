function AccoutsAlertsRequiedOnClick(eventobject) {
    return AS_Switch_cc072f30bb294c15a0ec51090a76cf51(eventobject);
}

function AS_Switch_cc072f30bb294c15a0ec51090a76cf51(eventobject) {
    if (frmAccountAlertsKA.SwitchAlert.selectedIndex === 1) {
        frmAccountAlertsKA.androidAlerts.setVisibility(false);
        frmAccountAlertsKA.HiddenAlertsReq.text = "false";
    } else {
        frmAccountAlertsKA.androidAlerts.setVisibility(true);
        frmAccountAlertsKA.HiddenAlertsReq.text = "true";
    }
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var controller = INSTANCE.getFormController("frmAccountAlertsKA");
    controller.performAction("saveData");
}