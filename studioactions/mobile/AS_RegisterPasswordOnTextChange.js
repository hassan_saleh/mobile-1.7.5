function AS_RegisterPasswordOnTextChange(eventobject, changedtext) {
    return AS_TextField_d692480d02be416faf4ea8ea5cbdef29(eventobject, changedtext);
}

function AS_TextField_d692480d02be416faf4ea8ea5cbdef29(eventobject, changedtext) {
    if (kony.retailBanking.util.validation.isValidPassword(frmEnrolluserLandingKA.tbxPasswordKA.text)) {
        frmEnrolluserLandingKA.flxUnderlinePassword.skin = "sknFlxGreenLine";
        passwordFlag = true;
    } else {
        frmEnrolluserLandingKA.flxUnderlinePassword.skin = "sknFlxOrangeLine";
        passwordFlag = false;
    }
    frmEnrolluserLandingKA.flxUsernameValidation.isVisible = false;
    frmEnrolluserLandingKA.flxPasswordValidation.isVisible = true;
    if (validateConfirmPassword(frmEnrolluserLandingKA.tbxPasswordKA.text, frmEnrolluserLandingKA.tbxConfrmPwdKA.text)) frmEnrolluserLandingKA.flxUnderlineConfirmPassword.skin = "sknFlxGreenLine";
    else frmEnrolluserLandingKA.flxUnderlineConfirmPassword.skin = "sknFlxOrangeLine";
    if (usernameFlag === true && passwordFlag === true && confrmPasswordFlag === true) {
        frmEnrolluserLandingKA.lblNext.skin = "sknLblNextEnabled";
        frmEnrolluserLandingKA.flxProgress1.left = "80%";
    } else {
        frmEnrolluserLandingKA.lblNext.skin = "sknLblNextDisabled";
        frmEnrolluserLandingKA.flxProgress1.left = "65%";
    }
    isClearButtonVisible(frmEnrolluserLandingKA.tbxPasswordKA, frmEnrolluserLandingKA.CopylblClose0bb2f50619b5543);
    set_SHOW_BUTTON_VISIBILITY(frmEnrolluserLandingKA.tbxPasswordKA, frmEnrolluserLandingKA.lblShowPass);
}