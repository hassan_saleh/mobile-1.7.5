function AS_BankDetailsAddBeneiOS(eventobject, changedtext) {
    return AS_TextField_hc4c3247a1f44411a8ecf1b72935c90d(eventobject, changedtext);
}

function AS_TextField_hc4c3247a1f44411a8ecf1b72935c90d(eventobject, changedtext) {
    if (frmAddExternalAccountKA.tbxBeneBankDetails.text !== "") {
        frmAddExternalAccountKA.flxUnderlineBankDetails.skin = "sknFlxGreenLine";
    } else {
        frmAddExternalAccountKA.flxUnderlineBankDetails.skin = "sknFlxOrangeLine";
    }
    animateLabel("DOWN", "lblBeneBankDetails", frmAddExternalAccountKA.tbxBeneBankDetails.text);
}