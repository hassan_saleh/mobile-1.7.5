function AS_TextField_d2e6eecfc7e548b3ba6d46eab132dd0c(eventobject, changedtext) {
    var Text = frmNewTransferKA.txtDesc.text;
    animateLabel("UP", "lblDescription", Text);
    if (Text.match(/[^A-Za-z0-9\s]/g)) {
        customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.common.englishonly"), popupCommonAlertDimiss, null, "", "");
        frmNewTransferKA.txtDesc.text = Text.replace(/[^A-Za-z0-9,.\s]/g, "");
        frmNewTransferKA.lblNext.skin = sknLblNextDisabled;
    } else enableNext();
}