function AS_onEndEditingAccNoAddBeneAndroid(eventobject, changedtext) {
    return AS_TextField_c429df1fbad64580a9d467b7cf017182(eventobject, changedtext);
}

function AS_TextField_c429df1fbad64580a9d467b7cf017182(eventobject, changedtext) {
    if (frmAddExternalAccountKA.externalAccountNumberTextField.text !== "") {
        frmAddExternalAccountKA.flxUnderlineAccountNumber.skin = "sknFlxGreenLine";
    } else {
        frmAddExternalAccountKA.flxUnderlineAccountNumber.skin = "sknFlxOrangeLine";
    }
    animateLabel("DOWN", "lblAccountNumber", frmAddExternalAccountKA.externalAccountNumberTextField.text);
}