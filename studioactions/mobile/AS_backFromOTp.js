function AS_backFromOTp(eventobject) {
    return AS_FlexContainer_c5667325cd68430eb5b39a41c1600260(eventobject);
}

function AS_FlexContainer_c5667325cd68430eb5b39a41c1600260(eventobject) {
    if (!isAndroid()) {
        frmNewUserOnboardVerificationKA.flxEnterOTP.setVisibility(false);
        frmNewUserOnboardVerificationKA.loadCustomWidgetForm.clearOTPTextField();
    }
    backFromOTP();
}