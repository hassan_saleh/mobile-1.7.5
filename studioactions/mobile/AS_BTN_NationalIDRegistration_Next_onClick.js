function AS_BTN_NationalIDRegistration_Next_onClick(eventobject) {
    return AS_FlexContainer_f9d0cdfd81f943a68b13e5cb50e3d05e(eventobject);
}

function AS_FlexContainer_f9d0cdfd81f943a68b13e5cb50e3d05e(eventobject) {
    kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
    if (frmRegisterUser.flxNationalIDContainer.isVisible === true) {
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        gblToOTPFrom = "RegisterUserNational";
        callRequestOTP();
    } else {
        if (gblFromModule == "RetriveUsername") {
            sendUnamecase = true;
        }
        ShowLoadingScreen();
        nextFromCardDetails();
        frmRegisterUser.lblInvalidCredentialsKA.setVisibility(false);
    }
}