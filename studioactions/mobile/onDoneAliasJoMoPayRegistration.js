function onDoneAliasJoMoPayRegistration(eventobject, changedtext) {
    return AS_TextField_a6e1de9baa724aa6817912d44f746c8a(eventobject, changedtext);
}

function AS_TextField_a6e1de9baa724aa6817912d44f746c8a(eventobject, changedtext) {
    animate_NEWCARDSOPTIOS("DOWN", "lblAliasJMPRstatic", frmJoMoPayRegistration.txtAlias.text);
    if (frmJoMoPayRegistration.txtAlias.text !== "" && frmJoMoPayRegistration.txtAlias.text !== null && validateAliasField(frmJoMoPayRegistration.txtAlias.text, frmJoMoPayRegistration.txtAlias)) {
        frmJoMoPayRegistration.CopyflxBorderjomopayID0b6963ff7a40e46.skin = "skntextFieldDividerGreen";
    } else {
        frmJoMoPayRegistration.CopyflxBorderjomopayID0b6963ff7a40e46.skin = "skntextFieldDividerOrange";
    }
}