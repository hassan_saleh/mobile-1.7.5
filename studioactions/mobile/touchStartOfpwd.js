function touchStartOfpwd(eventobject, x, y) {
    return AS_TextField_a97a99d890794fdbae84264ec2f33229(eventobject, x, y);
}

function AS_TextField_a97a99d890794fdbae84264ec2f33229(eventobject, x, y) {
    try {
        var currentForm = kony.application.getCurrentForm();
        if (currentForm.id === "frmLoginKA" || currentForm.id === "frmLanguageChange") {
            currentForm.lblInvalidCredentialsKA.isVisible = false;
            currentForm.lblClosePass.setVisibility(true);
            //frmLoginKA.lblShowPass.setVisibility(true);
            currentForm.lblClose.setVisibility(false);
            currentForm.flxbdrpwd.skin = "skntextFieldDividerBlue";
            if (currentForm.tbxusernameTextField.text !== null && currentForm.tbxusernameTextField.text !== "") {
                currentForm.borderBottom.skin = "skntextFieldDividerGreen";
            } else {
                currentForm.borderBottom.skin = "skntextFieldDivider";
            }
        }
    } catch (err) {
        kony.print("err in pwd::" + err);
    }
}