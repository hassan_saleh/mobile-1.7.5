function onBackccpay(eventobject) {
    return AS_FlexContainer_c24609342f0b469ba80b500434210773(eventobject);
}

function AS_FlexContainer_c24609342f0b469ba80b500434210773(eventobject) {
    customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18nkey("i18n.common.sureExit"), function() {
        //1984 fix
        if (kony.boj.creditcardOption === "fromPayNow") {
            kony.print("from paynow" + kony.boj.creditcardOption);
            frmCardsLandingKA.show();
            frmCreditCardPayment.destroy();
        } else {
            kony.print("from creditcardpayment" + kony.boj.creditcardOption);
            frmPaymentDashboard.show();
            frmCreditCardPayment.destroy();
        }
        //   kony.print("Previous form id is: "+kony.application.getPreviousForm().id);
        //   if(kony.application.getPreviousForm().id === "frmPaymentDashboard")
        //   {
        //     kony.print("In if");
        //     frmPaymentDashboard.show();
        //     frmCreditCardPayment.destroy();
        //   }
        //   else{
        //     kony.print("In else");
        //     frmCardsLandingKA.show();
        //     //frmManageCardsKA.show();
        //     frmCreditCardPayment.destroy();
        //   }
        popupCommonAlertDimiss();
    }, popupCommonAlertDimiss, geti18nkey("i18n.common.YES"), geti18nkey("i18n.common.NO"));
}