function AS_TextField_g1c92d5380e549138c9eab200892e83f(eventobject, changedtext) {
    if (kony.retailBanking.util.validation.isValidUsername(frmEnrolluserLandingKA.tbxUsernameKA.text)) {
        frmEnrolluserLandingKA.flxUnderlineUsername.skin = "sknFlxGreenLine";
        usernameFlag = true;
    } else {
        frmEnrolluserLandingKA.flxUnderlineUsername.skin = "sknFlxOrangeLine";
        usernameFlag = false;
    }
    animateLabel("DOWN", "lblUsername", frmEnrolluserLandingKA.tbxUsernameKA.text);
}