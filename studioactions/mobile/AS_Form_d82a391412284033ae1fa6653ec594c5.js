function AS_Form_d82a391412284033ae1fa6653ec594c5(eventobject) {
    kony.print("---- addBillerFromQuickPay ::" + kony.boj.addBillerFromQuickPay + " :: " + frmNewBillKA.flxBillerNumber.isVisible + " :: " + frmNewBillKA.tbxBillerNumber.text);
    if (kony.boj.addBillerFromQuickPay) {
        kony.boj.restoreBillerTabDetails();
        frmAddNewPrePayeeKA.flxBillerNumber.setVisibility(frmNewBillKA.flxBillerNumber.isVisible);
        frmAddNewPrePayeeKA.tbxBillerNumber.text = frmNewBillKA.tbxBillerNumber.text;
        kony.boj.storeDetailsAddBiller("BillerNumber", frmAddNewPayeeKA.tbxBillerNumber.text);
        animateLabel("UP", "lblBillerNumber", frmAddNewPayeeKA.tbxBillerNumber.text);
        kony.boj.addBillerFromQuickPay = false;
    }
    frmAddNewPrePayeeKA.btnPrePaid.setEnabled(false);
}