function AS_TextField_e1c3e34a26a14b7dbc6bb761c83cb6cf(eventobject, changedtext) {
    if (frmLanguageChange.tbxusernameTextField.text !== null && frmLanguageChange.tbxusernameTextField.text !== "") {
        frmLanguageChange.borderBottom.skin = "skntextFieldDividerGreen";
    } else {
        frmLanguageChange.borderBottom.skin = "skntextFieldDivider";
    }
    animateLabel("DOWN", "lblUserName", frmLanguageChange.tbxusernameTextField.text);
    animateLabel("UP", "lblPassword", frmLanguageChange.passwordTextField.text);
}