function AS_Switch_j9c578f503304ba3952f4fce6e46b1eb(eventobject) {
    if (frmAuthorizationAlternatives.flxQuickBalance.height === "8%") {
        frmAuthorizationAlternatives.flxQuickBalance.height = "40%";
        addDatatoSegQuickBalance(1);
    } else {
        frmAuthorizationAlternatives.flxQuickBalance.height = "8%";
        addDatatoSegQuickBalance(0);
    }
}