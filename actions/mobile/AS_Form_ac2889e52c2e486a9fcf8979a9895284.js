function AS_Form_ac2889e52c2e486a9fcf8979a9895284() {
    function SHOW_ALERT__15cd84c1dcd446debab848130d4c171e_True() {
        userAllowedLocation();
    }
    function SHOW_ALERT__15cd84c1dcd446debab848130d4c171e_False() {}
    function SHOW_ALERT__15cd84c1dcd446debab848130d4c171e_Callback(response) {
        if (response == true) {
            SHOW_ALERT__15cd84c1dcd446debab848130d4c171e_True()
        } else {
            SHOW_ALERT__15cd84c1dcd446debab848130d4c171e_False()
        };
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": "Current Location",
        "yesLabel": "Allow",
        "noLabel": "Don't Allow",
        "message": "xyzBank would like to use your current location",
        "alertHandler": SHOW_ALERT__15cd84c1dcd446debab848130d4c171e_Callback
    }, {})
    locatorPostShow();
}