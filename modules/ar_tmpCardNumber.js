//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function initializetmpCardNumberAr() {
    flxCardNumberAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxCardNumber",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox"
    }, {}, {});
    flxCardNumber.setDefaultUnit(kony.flex.DP);
    var lblCardNum = new kony.ui.Label({
        "id": "lblCardNum",
        "isVisible": false,
        "right": "20%",
        "skin": "lblAmountCurrency",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "30%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCardHolderName = new kony.ui.Label({
        "id": "lblCardHolderName",
        "isVisible": true,
        "right": "10%",
        "skin": "lblAmountCurrency",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "6%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxCardNumberContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40%",
        "id": "flxCardNumberContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "10%",
        "skin": "slFbox",
        "top": "25dp",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    flxCardNumberContainer.setDefaultUnit(kony.flex.DP);
    var lblCardType = new kony.ui.Label({
        "id": "lblCardType",
        "isVisible": true,
        "right": "0%",
        "skin": "lblAmountCurrency",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblHiddenCardNum = new kony.ui.Label({
        "id": "lblHiddenCardNum",
        "isVisible": false,
        "right": "0%",
        "skin": "lblAmountCurrency",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxCardNumberContainer.add(lblCardType, lblHiddenCardNum);
    flxCardNumberAr.add(lblCardNum, lblCardHolderName, flxCardNumberContainer);
}
