//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmAccountsReorderKAAr() {
frmAccountsReorderKA.setDefaultUnit(kony.flex.DP);
var flxMain = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxMain",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknflxTransprnt",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxMain.setDefaultUnit(kony.flex.DP);
var flxSettingsHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxSettingsHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "CopyslFbox0f07559cdcd2e4e",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxSettingsHeader.setDefaultUnit(kony.flex.DP);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_i4a8d5dce2214594bbd5a03d0100e6c9,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 10
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBack = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"centerY": "50%",
"id": "lblBack",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBack.add(lblBackIcon, lblBack);
var lblSettingsTitle = new kony.ui.Label({
"height": "90%",
"id": "lblSettingsTitle",
"isVisible": true,
"left": "20%",
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.appsettings.ReOrderMyAccountList"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": "60%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var btnSaveAccountsReorder = new kony.ui.Button({
"focusSkin": "jomopaynextEnabled",
"height": "90%",
"id": "btnSaveAccountsReorder",
"isVisible": true,
"onClick": AS_Button_a413a573416e41da964af1b4d54bb0a5,
"right": "0%",
"skin": "jomopaynextEnabled",
"text": kony.i18n.getLocalizedString("i18n.common.save"),
"top": "0%",
"width": "20%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
flxSettingsHeader.add(flxBack, lblSettingsTitle, btnSaveAccountsReorder);
var FlexScrollContainer0aaefdaacee1040 = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": true,
"clipBounds": true,
"enableScrolling": true,
"height": "91%",
"horizontalScrollIndicator": true,
"id": "FlexScrollContainer0aaefdaacee1040",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "sknflxScrollBlue",
"top": "9%",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 5
}, {}, {});
FlexScrollContainer0aaefdaacee1040.setDefaultUnit(kony.flex.DP);
var lblReorderDesc = new kony.ui.Label({
"id": "lblReorderDesc",
"isVisible": true,
"right": "5%",
"skin": "sknlblBodytxt",
"text": kony.i18n.getLocalizedString("i18n.reorderDesc"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "3%",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxLine = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "flxLine",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknflxLinewhiteOp",
"top": "3%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxLine.setDefaultUnit(kony.flex.DP);
flxLine.add();
var segReOrderAccounts = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"data": [{
"lblAccNumber": "Label",
"lblAccount": "",
"lblAccountName": "",
"lblIncommingRing": "s",
"lblIncommingTick": "r"
}, {
"lblAccNumber": "Label",
"lblAccount": "",
"lblAccountName": "",
"lblIncommingRing": "s",
"lblIncommingTick": "r"
}, {
"lblAccNumber": "Label",
"lblAccount": "",
"lblAccountName": "",
"lblIncommingRing": "s",
"lblIncommingTick": "r"
}],
"groupCells": false,
"height": "80%",
"id": "segReOrderAccounts",
"isVisible": true,
"right": "0%",
"needPageIndicator": true,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "sknsegAcc",
"rowSkin": "sknsegAcc",
"rowTemplate": flxSegQuickBalance,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "ffffff64",
"separatorRequired": true,
"separatorThickness": 0,
"showScrollbars": false,
"top": "0%",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"flxSegQuickBalance": "flxSegQuickBalance",
"lblAccNumber": "lblAccNumber",
"lblAccount": "lblAccount",
"lblAccountName": "lblAccountName",
"lblIncommingRing": "lblIncommingRing",
"lblIncommingTick": "lblIncommingTick"
},
"width": "100%",
"zIndex": 1,
"enableReordering": true
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var LabelNoRecordsKA = new kony.ui.Label({
"id": "LabelNoRecordsKA",
"isVisible": false,
"right": "16%",
"skin": "skn383838LatoRegular107KA",
"text": kony.i18n.getLocalizedString("i18n.alerts.NoAccountsMsg"),
"top": "20dp",
"width": "65%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
FlexScrollContainer0aaefdaacee1040.add(lblReorderDesc, flxLine, segReOrderAccounts, LabelNoRecordsKA);
flxMain.add(flxSettingsHeader, FlexScrollContainer0aaefdaacee1040);
frmAccountsReorderKA.add(flxMain);
};
function frmAccountsReorderKAGlobalsAr() {
frmAccountsReorderKAAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmAccountsReorderKAAr,
"enabledForIdleTimeout": true,
"id": "frmAccountsReorderKA",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"skin": "slFormCommon"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_cf7dd7b0665f49efbd0ae3c679b2d1a9,
"retainScrollPosition": false,
"titleBar": true,
"titleBarSkin": "slTitleBar",
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
