//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetspopupCommonAlertAr() {
var hbxAlertHeader = new kony.ui.Box({
"id": "hbxAlertHeader",
"isVisible": true,
"orientation": constants.BOX_LAYOUT_HORIZONTAL,
"position": constants.BOX_POSITION_AS_NORMAL,
"skin": "slHbox"
}, {
"containerWeight": 100,
"layoutType": constants.CONTAINER_LAYOUT_BOX,
"margin": [ 0, 2,0, 0],
"marginInPixel": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false,
"percent": true,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_CENTER
}, {});
var lblAlertHeader = new kony.ui.Label({
"id": "lblAlertHeader",
"isVisible": true,
"skin": "sknCairoBold120",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
}
}, {
"containerWeight": 100,
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"hExpand": true,
"margin": [ 2, 1,2, 1],
"marginInPixel": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_CENTER
}, {
"textCopyable": false
});
hbxAlertHeader.add(lblAlertHeader);
var lineSortpopup = new kony.ui.Line({
"id": "lineSortpopup",
"isVisible": true,
"skin": "slLine"
}, {
"margin": [ 4, 1,4, 4],
"marginInPixel": false,
"thickness": 1
}, {});
var hbxAlertBody = new kony.ui.Box({
"id": "hbxAlertBody",
"isVisible": true,
"orientation": constants.BOX_LAYOUT_HORIZONTAL,
"position": constants.BOX_POSITION_AS_NORMAL,
"skin": "slHbox"
}, {
"containerWeight": 100,
"layoutType": constants.CONTAINER_LAYOUT_BOX,
"margin": [ 0, 0,0, 5],
"marginInPixel": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false,
"percent": true,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_TOP_RIGHT
}, {});
var lblDescription = new kony.ui.RichText({
"id": "lblDescription",
"isVisible": true,
"skin": "CopyslRichText0g9962491f08d48"
}, {
"containerWeight": 100,
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"hExpand": true,
"margin": [ 1, 0,1, 0],
"marginInPixel": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_CENTER
}, {});
hbxAlertBody.add(lblDescription);
var hbxSingleOk = new kony.ui.Box({
"id": "hbxSingleOk",
"isVisible": true,
"orientation": constants.BOX_LAYOUT_HORIZONTAL,
"position": constants.BOX_POSITION_AS_NORMAL,
"skin": "slHbox"
}, {
"containerWeight": 100,
"layoutType": constants.CONTAINER_LAYOUT_BOX,
"margin": [ 0, 0,0, 0],
"marginInPixel": false,
"padding": [ 0, 2,0, 4],
"paddingInPixel": false,
"percent": true,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_CENTER
}, {});
var Label0f420ba73f3ba40 = new kony.ui.Label({
"id": "Label0f420ba73f3ba40",
"isVisible": true,
"skin": "sknTrans",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
}
}, {
"containerWeight": 25,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"hExpand": true,
"margin": [ 1, 1,1, 1],
"marginInPixel": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_CENTER
}, {
"textCopyable": false
});
var btnSingleOkay = new kony.ui.Button({
"focusSkin": "slButtonBlueFocus",
"id": "btnSingleOkay",
"isVisible": true,
"onClick": AS_Button_cbc321b69fff43d1953ad3163ba632ab,
"skin": "sknCancleBtn",
"text": kony.i18n.getLocalizedString("i18n.NUO.OKay")
}, {
"containerWeight": 50,
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"hExpand": true,
"margin": [ 5, 2,5, 2],
"marginInPixel": false,
"padding": [ 1, 1,1, 1],
"paddingInPixel": false,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_CENTER
}, {});
var CopyLabel0fc57b930c2d740 = new kony.ui.Label({
"id": "CopyLabel0fc57b930c2d740",
"isVisible": true,
"skin": "sknTrans",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
}
}, {
"containerWeight": 25,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"hExpand": true,
"margin": [ 1, 1,1, 1],
"marginInPixel": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_CENTER
}, {
"textCopyable": false
});
hbxSingleOk.add( CopyLabel0fc57b930c2d740, btnSingleOkay,Label0f420ba73f3ba40);
var hbxTwoButtons = new kony.ui.Box({
"id": "hbxTwoButtons",
"isVisible": true,
"orientation": constants.BOX_LAYOUT_HORIZONTAL,
"position": constants.BOX_POSITION_AS_NORMAL,
"skin": "slHbox"
}, {
"containerWeight": 100,
"layoutType": constants.CONTAINER_LAYOUT_BOX,
"margin": [ 0, 0,0, 0],
"marginInPixel": false,
"padding": [ 0, 4,0, 4],
"paddingInPixel": false,
"percent": true,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_CENTER
}, {});
var btnOkay = new kony.ui.Button({
"focusSkin": "slButtonBlueFocus",
"id": "btnOkay",
"isVisible": true,
"skin": "sknCancleBtn",
"text": kony.i18n.getLocalizedString("i18n.NUO.OKay")
}, {
"containerWeight": 50,
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"hExpand": true,
"margin": [ 5, 2,5, 2],
"marginInPixel": false,
"padding": [ 1, 1,1, 1],
"paddingInPixel": false,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_CENTER
}, {});
var btnCancel = new kony.ui.Button({
"focusSkin": "slButtonBlueFocus",
"id": "btnCancel",
"isVisible": true,
"onClick": AS_Button_d9c34370c13547e7a59b7b921f82a4da,
"skin": "sknCancleBtn",
"text": kony.i18n.getLocalizedString("i18n.common.cancel")
}, {
"containerWeight": 50,
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"hExpand": true,
"margin": [ 5, 2,5, 2],
"marginInPixel": false,
"padding": [ 1, 1,1, 1],
"paddingInPixel": false,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_CENTER
}, {});
hbxTwoButtons.add( btnCancel,btnOkay);
popupCommonAlert.add(hbxAlertHeader, lineSortpopup, hbxAlertBody, hbxSingleOk, hbxTwoButtons);
};
function popupCommonAlertGlobalsAr() {
popupCommonAlertAr = new kony.ui.Popup({
"addWidgets": addWidgetspopupCommonAlertAr,
"id": "popupCommonAlert",
"isModal": true,
"skin": "sknWTBGWTBRDRND",
"transparencyBehindThePopup": 50
}, {
"containerWeight": 80,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"inTransitionConfig": {
"animation": 1
},
"outTransitionConfig": {
"animation": 8
},
"windowSoftInputMode": constants.POPUP_ADJUST_PAN
});
};
