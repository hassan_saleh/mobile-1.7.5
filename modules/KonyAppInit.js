kony = kony || {};
kony.sdk = kony.sdk || {};
kony.sdk.mvvm = kony.sdk.mvvm || {};
kony.sdk.mvvm.v2 = kony.sdk.mvvm.v2 || {};
kony.sdk.mvvm.initApplicationForms = function(appContext) {
  try {
    //kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
    //frmUserSettingsMyProfileKA
    var frmUserSettingsMyProfileKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmUserSettingsMyProfileKAConfig);
    var frmUserSettingsMyProfileKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmUserSettingsMyProfileKAController", appContext, frmUserSettingsMyProfileKAModelConfigObj);
    var frmUserSettingsMyProfileKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmUserSettingsMyProfileKAControllerExtension", frmUserSettingsMyProfileKAControllerObj);
    frmUserSettingsMyProfileKAControllerObj.setControllerExtensionObject(frmUserSettingsMyProfileKAControllerExtObj);
    var frmUserSettingsMyProfileKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmUserSettingsMyProfileKAFormModel", frmUserSettingsMyProfileKAControllerObj);
    var frmUserSettingsMyProfileKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmUserSettingsMyProfileKAFormModelExtension", frmUserSettingsMyProfileKAFormModelObj);
    frmUserSettingsMyProfileKAFormModelObj.setFormModelExtensionObj(frmUserSettingsMyProfileKAFormModelExtObj);
    appContext.setFormController("frmUserSettingsMyProfileKA", frmUserSettingsMyProfileKAControllerObj);
	//frmConfirmPrePayBill
	var frmConfirmPrePayBillModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmConfirmPrePayBillConfig);
	var frmConfirmPrePayBillControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmConfirmPrePayBillController", appContext, frmConfirmPrePayBillModelConfigObj);
	var frmConfirmPrePayBillControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmConfirmPrePayBillControllerExtension", frmConfirmPrePayBillControllerObj);
	frmConfirmPrePayBillControllerObj.setControllerExtensionObject(frmConfirmPrePayBillControllerExtObj);
	var frmConfirmPrePayBillFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmConfirmPrePayBillFormModel", frmConfirmPrePayBillControllerObj);
	var frmConfirmPrePayBillFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmConfirmPrePayBillFormModelExtension", frmConfirmPrePayBillFormModelObj);
	frmConfirmPrePayBillFormModelObj.setFormModelExtensionObj(frmConfirmPrePayBillFormModelExtObj);
	appContext.setFormController("frmConfirmPrePayBill", frmConfirmPrePayBillControllerObj);
	
    //frmJoMoPayLanding
    var frmJoMoPayLandingConfigModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmJoMoPayLandingConfig);
    var frmJoMoPayLandingConfigControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmJoMoPayLandingConfigController", appContext, frmJoMoPayLandingConfigModelConfigObj);
    var frmJoMoPayLandingConfigControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmJoMoPayLandingConfigControllerExtension", frmJoMoPayLandingConfigControllerObj);
    frmJoMoPayLandingConfigControllerObj.setControllerExtensionObject(frmJoMoPayLandingConfigControllerExtObj);
    var frmJoMoPayLandingConfigFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmJoMoPayLandingConfigFormModel", frmJoMoPayLandingConfigControllerObj);
    var frmJoMoPayLandingConfigFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmJoMoPayLandingConfigFormModelExtension", frmJoMoPayLandingConfigFormModelObj);
    frmJoMoPayLandingConfigFormModelObj.setFormModelExtensionObj(frmJoMoPayLandingConfigFormModelExtObj);
    appContext.setFormController("frmJoMoPayLanding", frmJoMoPayLandingConfigControllerObj);
    //frmJoMoPayRegistration
    var frmJoMoPayRegistrationModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmJoMoPayRegistrationConfig);
    var frmJoMoPayRegistrationControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmJoMoPayRegistrationController", appContext, frmJoMoPayRegistrationModelConfigObj);
    var frmJoMoPayRegistrationControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmJoMoPayRegistrationControllerExtension", frmJoMoPayRegistrationControllerObj);
    frmJoMoPayRegistrationControllerObj.setControllerExtensionObject(frmJoMoPayRegistrationControllerExtObj);
    var frmJoMoPayRegistrationFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmJoMoPayRegistrationFormModel", frmJoMoPayRegistrationControllerObj);
    var frmJoMoPayRegistrationFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmJoMoPayRegistrationFormModelExtension", frmJoMoPayRegistrationFormModelObj);
    frmJoMoPayRegistrationFormModelObj.setFormModelExtensionObj(frmJoMoPayRegistrationFormModelExtObj);
    appContext.setFormController("frmJoMoPayRegistration", frmJoMoPayRegistrationControllerObj);


    //frmSMSNotification
    var frmSMSNotificationModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmSMSNotificationConfig);
    var frmSMSNotificationControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmSMSNotificationController", appContext, frmSMSNotificationModelConfigObj);
    var frmSMSNotificationControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmSMSNotificationControllerExtension", frmSMSNotificationControllerObj);
    frmSMSNotificationControllerObj.setControllerExtensionObject(frmSMSNotificationControllerExtObj);
    var frmSMSNotificationFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmSMSNotificationFormModel", frmSMSNotificationControllerObj);
    var frmSMSNotificationFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmSMSNotificationFormModelExtension", frmSMSNotificationFormModelObj);
    frmSMSNotificationFormModelObj.setFormModelExtensionObj(frmSMSNotificationFormModelExtObj);
    appContext.setFormController("frmSMSNotification", frmSMSNotificationControllerObj);

    //frmAlertHistory
    var frmAlertHistoryModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmAlertHistoryConfig);
    var frmAlertHistoryControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmAlertHistoryController", appContext, frmAlertHistoryModelConfigObj);
    var frmAlertHistoryControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmAlertHistoryControllerExtension", frmAlertHistoryControllerObj);
    frmAlertHistoryControllerObj.setControllerExtensionObject(frmAlertHistoryControllerExtObj);
    var frmAlertHistoryFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmAlertHistoryFormModel", frmAlertHistoryControllerObj);
    var frmAlertHistoryFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmAlertHistoryFormModelExtension", frmAlertHistoryFormModelObj);
    frmAlertHistoryFormModelObj.setFormModelExtensionObj(frmAlertHistoryFormModelExtObj);
    appContext.setFormController("frmAlertHistory", frmAlertHistoryControllerObj);


    //frmUserSettingsMyProfileKA
    var frmUserSettingsMyProfileKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmUserSettingsMyProfileKAConfig);
    var frmUserSettingsMyProfileKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmUserSettingsMyProfileKAController", appContext, frmUserSettingsMyProfileKAModelConfigObj);
    var frmUserSettingsMyProfileKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmUserSettingsMyProfileKAControllerExtension", frmUserSettingsMyProfileKAControllerObj);
    frmUserSettingsMyProfileKAControllerObj.setControllerExtensionObject(frmUserSettingsMyProfileKAControllerExtObj);
    var frmUserSettingsMyProfileKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmUserSettingsMyProfileKAFormModel", frmUserSettingsMyProfileKAControllerObj);
    var frmUserSettingsMyProfileKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmUserSettingsMyProfileKAFormModelExtension", frmUserSettingsMyProfileKAFormModelObj);
    frmUserSettingsMyProfileKAFormModelObj.setFormModelExtensionObj(frmUserSettingsMyProfileKAFormModelExtObj);
    appContext.setFormController("frmUserSettingsMyProfileKA", frmUserSettingsMyProfileKAControllerObj);

    //frmCardsLandingKA
    var frmCardsLandingKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmCardsLandingKAConfig);
    var frmCardsLandingKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmCardsLandingKAController", appContext, frmCardsLandingKAModelConfigObj);
    var frmCardsLandingKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmCardsLandingKAControllerExtension", frmCardsLandingKAControllerObj);
    frmCardsLandingKAControllerObj.setControllerExtensionObject(frmCardsLandingKAControllerExtObj);
    var frmCardsLandingKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmCardsLandingKAFormModel", frmCardsLandingKAControllerObj);
    var frmCardsLandingKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmCardsLandingKAFormModelExtension", frmCardsLandingKAFormModelObj);
    frmCardsLandingKAFormModelObj.setFormModelExtensionObj(frmCardsLandingKAFormModelExtObj);
    appContext.setFormController("frmCardsLandingKA", frmCardsLandingKAControllerObj);
    //Device Registration
    var frmDeviceRegistrationModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmDeviceRegistrationConfig);
    var frmDeviceRegistrationControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmDeviceRegistrationController", appContext, frmDeviceRegistrationModelConfigObj);
    var frmDeviceRegistrationControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmDeviceRegistrationControllerExtension", frmDeviceRegistrationControllerObj);
    frmDeviceRegistrationControllerObj.setControllerExtensionObject(frmDeviceRegistrationControllerExtObj);
    var frmDeviceRegistrationFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmDeviceRegistrationFormModel", frmDeviceRegistrationControllerObj);
    var frmDeviceRegistrationFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmDeviceRegistrationFormModelExtension", frmDeviceRegistrationFormModelObj);
    frmDeviceRegistrationFormModelObj.setFormModelExtensionObj(frmDeviceRegistrationFormModelExtObj);
    appContext.setFormController("frmDeviceRegistration", frmDeviceRegistrationControllerObj);


    var frmCreditCardPaymentModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmCreditCardPaymentConfig);
    var frmCreditCardPaymentControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmCreditCardPaymentController", appContext, frmCreditCardPaymentModelConfigObj);
    var frmCreditCardPaymentControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmCreditCardPaymentControllerExtension", frmCreditCardPaymentControllerObj);
    frmCreditCardPaymentControllerObj.setControllerExtensionObject(frmCreditCardPaymentControllerExtObj);
    var frmCreditCardPaymentFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmCreditCardPaymentFormModel", frmCreditCardPaymentControllerObj);
    var frmCreditCardPaymentFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmCreditCardPaymentFormModelExtension", frmCreditCardPaymentFormModelObj);
    frmCreditCardPaymentFormModelObj.setFormModelExtensionObj(frmCreditCardPaymentFormModelExtObj);
    appContext.setFormController("frmCreditCardPayment", frmCreditCardPaymentControllerObj);


    //frmJoMoPayConfirmation
    var frmJoMoPayConfirmationModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmJoMoPayConfirmationConfig);
    var frmJoMoPayConfirmationControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmJoMoPayConfirmationController", appContext, frmJoMoPayConfirmationModelConfigObj);
    var frmJoMoPayConfirmationControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmJoMoPayConfirmationControllerExtension", frmJoMoPayConfirmationControllerObj);
    frmJoMoPayConfirmationControllerObj.setControllerExtensionObject(frmJoMoPayConfirmationControllerExtObj);
    var frmJoMoPayConfirmationFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmJoMoPayConfirmationFormModel", frmJoMoPayConfirmationControllerObj);
    var frmJoMoPayConfirmationFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmJoMoPayConfirmationFormModelExtension", frmJoMoPayConfirmationFormModelObj);
    frmJoMoPayConfirmationFormModelObj.setFormModelExtensionObj(frmJoMoPayConfirmationFormModelExtObj);
    appContext.setFormController("frmJoMoPayConfirmation", frmJoMoPayConfirmationControllerObj);

    //frmRegisterUser

    var frmRegisterUserModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmRegisterUserConfig);
    var frmRegisterUserControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmRegisterUserController", appContext, frmRegisterUserModelConfigObj);
    var frmRegisterUserControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmRegisterUserControllerExtension", frmRegisterUserControllerObj);
    frmRegisterUserControllerObj.setControllerExtensionObject(frmRegisterUserControllerExtObj);
    var frmRegisterUserFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmRegisterUserFormModel", frmRegisterUserControllerObj);
    var frmRegisterUserFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmRegisterUserFormModelExtension", frmRegisterUserFormModelObj);
    frmRegisterUserFormModelObj.setFormModelExtensionObj(frmRegisterUserFormModelExtObj);
    appContext.setFormController("frmRegisterUser", frmRegisterUserControllerObj);


    //frmAccountsLandingKA
    var frmAccountsLandingKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmAccountsLandingKAConfig);
    var frmAccountsLandingKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmAccountsLandingKAController", appContext, frmAccountsLandingKAModelConfigObj);
    var frmAccountsLandingKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmAccountsLandingKAControllerExtension", frmAccountsLandingKAControllerObj);
    frmAccountsLandingKAControllerObj.setControllerExtensionObject(frmAccountsLandingKAControllerExtObj);
    var frmAccountsLandingKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmAccountsLandingKAFormModel", frmAccountsLandingKAControllerObj);
    var frmAccountsLandingKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmAccountsLandingKAFormModelExtension", frmAccountsLandingKAFormModelObj);
    frmAccountsLandingKAFormModelObj.setFormModelExtensionObj(frmAccountsLandingKAFormModelExtObj);
    appContext.setFormController("frmAccountsLandingKA", frmAccountsLandingKAControllerObj);

    //frmAccountDetailKA
    var frmAccountDetailKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmAccountDetailKAConfig);
    var frmAccountDetailKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmAccountDetailKAController", appContext, frmAccountDetailKAModelConfigObj);
    var frmAccountDetailKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmAccountDetailKAControllerExtension", frmAccountDetailKAControllerObj);
    frmAccountDetailKAControllerObj.setControllerExtensionObject(frmAccountDetailKAControllerExtObj);
    var frmAccountDetailKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmAccountDetailKAFormModel", frmAccountDetailKAControllerObj);
    var frmAccountDetailKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmAccountDetailKAFormModelExtension", frmAccountDetailKAFormModelObj);
    frmAccountDetailKAFormModelObj.setFormModelExtensionObj(frmAccountDetailKAFormModelExtObj);
    appContext.setFormController("frmAccountDetailKA", frmAccountDetailKAControllerObj);

    //AccountInfo 
    var frmAccountInfoKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmAccountInfoKAConfig);
    var frmAccountInfoKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmAccountInfoKAController", appContext, frmAccountInfoKAModelConfigObj);
    var frmAccountInfoKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmAccountInfoKAControllerExtension", frmAccountInfoKAControllerObj);
    frmAccountInfoKAControllerObj.setControllerExtensionObject(frmAccountInfoKAControllerExtObj);
    var frmAccountInfoKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmAccountInfoKAFormModel", frmAccountInfoKAControllerObj);
    var frmAccountInfoKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmAccountInfoKAFormModelExtension", frmAccountInfoKAFormModelObj);
    frmAccountInfoKAFormModelObj.setFormModelExtensionObj(frmAccountInfoKAFormModelExtObj);
    appContext.setFormController("frmAccountInfoKA", frmAccountInfoKAControllerObj);


    //New Transfers
    var frmNewTransferKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmNewTransferKAConfig);
    var frmNewTransferKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmNewTransferKAController", appContext, frmNewTransferKAModelConfigObj);
    var frmNewTransferKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmNewTransferKAControllerExtension", frmNewTransferKAControllerObj);
    frmNewTransferKAControllerObj.setControllerExtensionObject(frmNewTransferKAControllerExtObj);
    var frmNewTransferKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmNewTransferKAFormModel", frmNewTransferKAControllerObj);
    var frmNewTransferKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmNewTransferKAFormModelExtension", frmNewTransferKAFormModelObj);
    frmNewTransferKAFormModelObj.setFormModelExtensionObj(frmNewTransferKAFormModelExtObj);
    appContext.setFormController("frmNewTransferKA", frmNewTransferKAControllerObj);


    //TransferLanding Details
    var frmRecentTransactionDetailsKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmRecentTransactionDetailsKAConfig);
    var frmRecentTransactionDetailsKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmRecentTransactionDetailsKAController", appContext, frmRecentTransactionDetailsKAModelConfigObj);
    var frmRecentTransactionDetailsKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmRecentTransactionDetailsKAControllerExtension", frmRecentTransactionDetailsKAControllerObj);
    frmRecentTransactionDetailsKAControllerObj.setControllerExtensionObject(frmRecentTransactionDetailsKAControllerExtObj);
    var frmRecentTransactionDetailsKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmRecentTransactionDetailsKAFormModel", frmRecentTransactionDetailsKAControllerObj);
    var frmRecentTransactionDetailsKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmRecentTransactionDetailsKAFormModelExtension", frmRecentTransactionDetailsKAFormModelObj);
    frmRecentTransactionDetailsKAFormModelObj.setFormModelExtensionObj(frmRecentTransactionDetailsKAFormModelExtObj);
    appContext.setFormController("frmRecentTransactionDetailsKA", frmRecentTransactionDetailsKAControllerObj);

    //Cofirm Transfer
    var frmConfirmTransferKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmConfirmTransferKAConfig);
    var frmConfirmTransferKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmConfirmTransferKAController", appContext, frmConfirmTransferKAModelConfigObj);
    var frmConfirmTransferKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmConfirmTransferKAControllerExtension", frmConfirmTransferKAControllerObj);
    frmConfirmTransferKAControllerObj.setControllerExtensionObject(frmConfirmTransferKAControllerExtObj);
    var frmConfirmTransferKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmConfirmTransferKAFormModel", frmConfirmTransferKAControllerObj);
    var frmConfirmTransferKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmConfirmTransferKAFormModelExtension", frmConfirmTransferKAFormModelObj);
    frmConfirmTransferKAFormModelObj.setFormModelExtensionObj(frmConfirmTransferKAFormModelExtObj);
    appContext.setFormController("frmConfirmTransferKA", frmConfirmTransferKAControllerObj);

    //frmSuccessFormKA
    var frmSuccessFormKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmSuccessFormKAConfig);
    var frmSuccessFormKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmSuccessFormKAController", appContext, frmSuccessFormKAModelConfigObj);
    var frmSuccessFormKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmSuccessFormKAControllerExtension", frmSuccessFormKAControllerObj);
    frmSuccessFormKAControllerObj.setControllerExtensionObject(frmSuccessFormKAControllerExtObj);
    var frmSuccessFormKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmSuccessFormKAFormModel", frmSuccessFormKAControllerObj);
    var frmSuccessFormKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmSuccessFormKAFormModelExtension", frmSuccessFormKAFormModelObj);
    frmSuccessFormKAFormModelObj.setFormModelExtensionObj(frmSuccessFormKAFormModelExtObj);
    appContext.setFormController("frmSuccessFormKA", frmSuccessFormKAControllerObj);

    //frmPayeeTransactionKA
    var frmPayeeTransactionsKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmPayeeTransactionsKAConfig);
    var frmPayeeTransactionsKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmPayeeTransactionsKAController", appContext, frmPayeeTransactionsKAModelConfigObj);
    var frmPayeeTransactionsKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmPayeeTransactionsKAControllerExtension", frmPayeeTransactionsKAControllerObj);
    frmPayeeTransactionsKAControllerObj.setControllerExtensionObject(frmPayeeTransactionsKAControllerExtObj);
    var frmPayeeTransactionsKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmPayeeTransactionsKAFormModel", frmPayeeTransactionsKAControllerObj);
    var frmPayeeTransactionsKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmPayeeTransactionsKAFormModelExtension", frmPayeeTransactionsKAFormModelObj);
    frmPayeeTransactionsKAFormModelObj.setFormModelExtensionObj(frmPayeeTransactionsKAFormModelExtObj);
    appContext.setFormController("frmPayeeTransactionsKA", frmPayeeTransactionsKAControllerObj);

    //Pay Bill
    var frmNewBillKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmNewBillKAConfig);
    var frmNewBillKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmNewBillKAController", appContext, frmNewBillKAModelConfigObj);
    var frmNewBillKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmNewBillKAControllerExtension", frmNewBillKAControllerObj);
    frmNewBillKAControllerObj.setControllerExtensionObject(frmNewBillKAControllerExtObj);
    var frmNewBillKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmNewBillKAFormModel", frmNewBillKAControllerObj);
    var frmNewBillKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmNewBillKAFormModelExtension", frmNewBillKAFormModelObj);
    frmNewBillKAFormModelObj.setFormModelExtensionObj(frmNewBillKAFormModelExtObj);
    appContext.setFormController("frmNewBillKA", frmNewBillKAControllerObj);

    //frmManagePayeeKA
    var frmManagePayeeKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmManagePayeeKAConfig);
    var frmManagePayeeKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmManagePayeeKAController", appContext, frmManagePayeeKAModelConfigObj);
    var frmManagePayeeKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmManagePayeeKAControllerExtension", frmManagePayeeKAControllerObj);
    frmManagePayeeKAControllerObj.setControllerExtensionObject(frmManagePayeeKAControllerExtObj);
    var frmManagePayeeKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmManagePayeeKAFormModel", frmManagePayeeKAControllerObj);
    var frmManagePayeeKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmManagePayeeKAFormModelExtension", frmManagePayeeKAFormModelObj);
    frmManagePayeeKAFormModelObj.setFormModelExtensionObj(frmManagePayeeKAFormModelExtObj);
    appContext.setFormController("frmManagePayeeKA", frmManagePayeeKAControllerObj);

    //frmEditPayeeKA
    var frmEditPayeeKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmEditPayeeKAConfig);
    var frmEditPayeeKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmEditPayeeKAController", appContext, frmEditPayeeKAModelConfigObj);
    var frmEditPayeeKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmEditPayeeKAControllerExtension", frmEditPayeeKAControllerObj);
    frmEditPayeeKAControllerObj.setControllerExtensionObject(frmEditPayeeKAControllerExtObj);
    var frmEditPayeeKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmEditPayeeKAFormModel", frmEditPayeeKAControllerObj);
    var frmEditPayeeKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmEditPayeeKAFormModelExtension", frmEditPayeeKAFormModelObj);
    frmEditPayeeKAFormModelObj.setFormModelExtensionObj(frmEditPayeeKAFormModelExtObj);
    appContext.setFormController("frmEditPayeeKA", frmEditPayeeKAControllerObj);


    //frmPayeeDetailsKA
    var frmPayeeDetailsKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmPayeeDetailsKAConfig);
    var frmPayeeDetailsKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmPayeeDetailsKAController", appContext, frmPayeeDetailsKAModelConfigObj);
    var frmPayeeDetailsKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmPayeeDetailsKAControllerExtension", frmPayeeDetailsKAControllerObj);
    frmPayeeDetailsKAControllerObj.setControllerExtensionObject(frmPayeeDetailsKAControllerExtObj);
    var frmPayeeDetailsKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmPayeeDetailsKAFormModel", frmPayeeDetailsKAControllerObj);
    var frmPayeeDetailsKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmPayeeDetailsKAFormModelExtension", frmPayeeDetailsKAFormModelObj);
    frmPayeeDetailsKAFormModelObj.setFormModelExtensionObj(frmPayeeDetailsKAFormModelExtObj);
    appContext.setFormController("frmPayeeDetailsKA", frmPayeeDetailsKAControllerObj);

    //frmDepositLandingKA
    var frmDepositPayLandingKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmDepositPayLandingKAConfig);
    var frmDepositPayLandingKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmDepositPayLandingKAController", appContext, frmDepositPayLandingKAModelConfigObj);
    var frmDepositPayLandingKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmDepositPayLandingKAControllerExtension", frmDepositPayLandingKAControllerObj);
    frmDepositPayLandingKAControllerObj.setControllerExtensionObject(frmDepositPayLandingKAControllerExtObj);
    var frmDepositPayLandingKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmDepositPayLandingKAFormModel", frmDepositPayLandingKAControllerObj);
    var frmDepositPayLandingKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmDepositPayLandingKAFormModelExtension", frmDepositPayLandingKAFormModelObj);
    frmDepositPayLandingKAFormModelObj.setFormModelExtensionObj(frmDepositPayLandingKAFormModelExtObj);
    appContext.setFormController("frmDepositPayLandingKA", frmDepositPayLandingKAControllerObj);

    //frmAddExternalAccount
    var frmAddExternalAccountKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmAddExternalAccountKAConfig);
    var frmAddExternalAccountKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmAddExternalAccountKAController", appContext, frmAddExternalAccountKAModelConfigObj);
    var frmAddExternalAccountKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmAddExternalAccountKAControllerExtension", frmAddExternalAccountKAControllerObj);
    frmAddExternalAccountKAControllerObj.setControllerExtensionObject(frmAddExternalAccountKAControllerExtObj);
    var frmAddExternalAccountKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmAddExternalAccountKAFormModel", frmAddExternalAccountKAControllerObj);
    var frmAddExternalAccountKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmAddExternalAccountKAFormModelExtension", frmAddExternalAccountKAFormModelObj);
    frmAddExternalAccountKAFormModelObj.setFormModelExtensionObj(frmAddExternalAccountKAFormModelExtObj);
    appContext.setFormController("frmAddExternalAccountKA", frmAddExternalAccountKAControllerObj);

    //frmShowAllBeneficiary

    var frmShowAllBeneficiaryModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmShowAllBeneficiaryConfig);
    var frmShowAllBeneficiaryControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmShowAllBeneficiaryController", appContext, frmShowAllBeneficiaryModelConfigObj);
    var frmShowAllBeneficiaryControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmShowAllBeneficiaryControllerExtension", frmShowAllBeneficiaryControllerObj);
    frmShowAllBeneficiaryControllerObj.setControllerExtensionObject(frmShowAllBeneficiaryControllerExtObj);
    var frmShowAllBeneficiaryFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmShowAllBeneficiaryFormModel", frmShowAllBeneficiaryControllerObj);
    var frmShowAllBeneficiaryFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmShowAllBeneficiaryFormModelExtension", frmShowAllBeneficiaryFormModelObj);
    frmShowAllBeneficiaryFormModelObj.setFormModelExtensionObj(frmShowAllBeneficiaryFormModelExtObj);
    appContext.setFormController("frmShowAllBeneficiary", frmShowAllBeneficiaryControllerObj);

    //frmCardOperations
    var frmCardOperationsKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmCardOperationsKAConfig);
    var frmCardOperationsKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmCardOperationsKAController", appContext, frmCardOperationsKAModelConfigObj);
    var frmCardOperationsKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmCardOperationsKAControllerExtension", frmCardOperationsKAControllerObj);
    frmCardOperationsKAControllerObj.setControllerExtensionObject(frmCardOperationsKAControllerExtObj);
    var frmCardOperationsKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmCardOperationsKAFormModel", frmCardOperationsKAControllerObj);
    var frmCardOperationsKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmCardOperationsKAFormModelExtension", frmCardOperationsKAFormModelObj);
    frmCardOperationsKAFormModelObj.setFormModelExtensionObj(frmCardOperationsKAFormModelExtObj);
    appContext.setFormController("frmCardOperationsKA", frmCardOperationsKAControllerObj);
    //AddNewPayee
    var frmAddNewPayeeKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmAddNewPayeeKAConfig);
    var frmAddNewPayeeKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmAddNewPayeeKAController", appContext, frmAddNewPayeeKAModelConfigObj);
    var frmAddNewPayeeKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmAddNewPayeeKAControllerExtension", frmAddNewPayeeKAControllerObj);
    frmAddNewPayeeKAControllerObj.setControllerExtensionObject(frmAddNewPayeeKAControllerExtObj);
    var frmAddNewPayeeKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmAddNewPayeeKAFormModel", frmAddNewPayeeKAControllerObj);
    var frmAddNewPayeeKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmAddNewPayeeKAFormModelExtension", frmAddNewPayeeKAFormModelObj);
    frmAddNewPayeeKAFormModelObj.setFormModelExtensionObj(frmAddNewPayeeKAFormModelExtObj);
    appContext.setFormController("frmAddNewPayeeKA", frmAddNewPayeeKAControllerObj);


    //frmConfirmDeposit
    var frmConfirmDepositKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmConfirmDepositKAConfig);
    var frmConfirmDepositKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmConfirmDepositKAController", appContext, frmConfirmDepositKAModelConfigObj);
    var frmConfirmDepositKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmConfirmDepositKAControllerExtension", frmConfirmDepositKAControllerObj);
    frmConfirmDepositKAControllerObj.setControllerExtensionObject(frmConfirmDepositKAControllerExtObj);
    var frmConfirmDepositKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmConfirmDepositKAFormModel", frmConfirmDepositKAControllerObj);
    var frmConfirmDepositKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmConfirmDepositKAFormModelExtension", frmConfirmDepositKAFormModelObj);
    frmConfirmDepositKAFormModelObj.setFormModelExtensionObj(frmConfirmDepositKAFormModelExtObj);
    appContext.setFormController("frmConfirmDepositKA", frmConfirmDepositKAControllerObj);


    //ConfirmPayBill
    var frmConfirmPayBillModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmConfirmPayBillConfig);
    var frmConfirmPayBillControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmConfirmPayBillController", appContext, frmConfirmPayBillModelConfigObj);
    var frmConfirmPayBillControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmConfirmPayBillControllerExtension", frmConfirmPayBillControllerObj);
    frmConfirmPayBillControllerObj.setControllerExtensionObject(frmConfirmPayBillControllerExtObj);
    var frmConfirmPayBillFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmConfirmPayBillFormModel", frmConfirmPayBillControllerObj);
    var frmConfirmPayBillFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmConfirmPayBillFormModelExtension", frmConfirmPayBillFormModelObj);
    frmConfirmPayBillFormModelObj.setFormModelExtensionObj(frmConfirmPayBillFormModelExtObj);
    appContext.setFormController("frmConfirmPayBill", frmConfirmPayBillControllerObj);

    //MyMoneyListKA
    var frmMyMoneyListKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmMyMoneyListKAConfig);
    var frmMyMoneyListKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmMyMoneyListKAController", appContext, frmMyMoneyListKAModelConfigObj);
    var frmMyMoneyListKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmMyMoneyListKAControllerExtension", frmMyMoneyListKAControllerObj);
    frmMyMoneyListKAControllerObj.setControllerExtensionObject(frmMyMoneyListKAControllerExtObj);
    var frmMyMoneyListKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmMyMoneyListKAFormModel", frmMyMoneyListKAControllerObj);
    var frmMyMoneyListKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmMyMoneyListKAFormModelExtension", frmMyMoneyListKAFormModelObj);
    frmMyMoneyListKAFormModelObj.setFormModelExtensionObj(frmMyMoneyListKAFormModelExtObj);
    appContext.setFormController("frmMyMoneyListKA", frmMyMoneyListKAControllerObj);

    //frmMoreForeignExchangeRatesKA
    var frmMoreForeignExchangeRatesKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmMoreForeignExchangeRatesKAConfig);
    var frmMoreForeignExchangeRatesKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmMoreForeignExchangeRatesKAController", appContext, frmMoreForeignExchangeRatesKAModelConfigObj);
    var frmMoreForeignExchangeRatesKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmMoreForeignExchangeRatesKAControllerExtension", frmMoreForeignExchangeRatesKAControllerObj);
    frmMoreForeignExchangeRatesKAControllerObj.setControllerExtensionObject(frmMoreForeignExchangeRatesKAControllerExtObj);
    var frmMoreForeignExchangeRatesKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmMoreForeignExchangeRatesKAFormModel", frmMoreForeignExchangeRatesKAControllerObj);
    var frmMoreForeignExchangeRatesKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmMoreForeignExchangeRatesKAFormModelExtension", frmMoreForeignExchangeRatesKAFormModelObj);
    frmMoreForeignExchangeRatesKAFormModelObj.setFormModelExtensionObj(frmMoreForeignExchangeRatesKAFormModelExtObj);
    appContext.setFormController("frmMoreForeignExchangeRatesKA", frmMoreForeignExchangeRatesKAControllerObj);

    //frmMoreInterestRatesKA
    var frmMoreInterestRatesKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmMoreInterestRatesKAConfig);
    var frmMoreInterestRatesKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmMoreInterestRatesKAController", appContext, frmMoreInterestRatesKAModelConfigObj);
    var frmMoreInterestRatesKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmMoreInterestRatesKAControllerExtension", frmMoreInterestRatesKAControllerObj);
    frmMoreInterestRatesKAControllerObj.setControllerExtensionObject(frmMoreInterestRatesKAControllerExtObj);
    var frmMoreInterestRatesKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmMoreInterestRatesKAFormModel", frmMoreInterestRatesKAControllerObj);
    var frmMoreInterestRatesKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmMoreInterestRatesKAFormModelExtension", frmMoreInterestRatesKAFormModelObj);
    frmMoreInterestRatesKAFormModelObj.setFormModelExtensionObj(frmMoreInterestRatesKAFormModelExtObj);
    appContext.setFormController("frmMoreInterestRatesKA", frmMoreInterestRatesKAControllerObj);

    //frmMoreFaqKA
    var frmMoreFaqKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmMoreFaqKAConfig);
    var frmMoreFaqKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmMoreFaqKAController", appContext, frmMoreFaqKAModelConfigObj);
    var frmMoreFaqKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmMoreFaqKAControllerExtension", frmMoreFaqKAControllerObj);
    frmMoreFaqKAControllerObj.setControllerExtensionObject(frmMoreFaqKAControllerExtObj);
    var frmMoreFaqKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmMoreFaqKAFormModel", frmMoreFaqKAControllerObj);
    var frmMoreFaqKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmMoreFaqKAFormModelExtension", frmMoreFaqKAFormModelObj);
    frmMoreFaqKAFormModelObj.setFormModelExtensionObj(frmMoreFaqKAFormModelExtObj);
    appContext.setFormController("frmMoreFaqKA", frmMoreFaqKAControllerObj);

    //frmMorePrivacyPolicyKA
    var frmMorePrivacyPolicyKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmMorePrivacyPolicyKAConfig);
    var frmMorePrivacyPolicyKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmMorePrivacyPolicyKAController", appContext, frmMorePrivacyPolicyKAModelConfigObj);
    var frmMorePrivacyPolicyKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmMorePrivacyPolicyKAControllerExtension", frmMorePrivacyPolicyKAControllerObj);
    frmMorePrivacyPolicyKAControllerObj.setControllerExtensionObject(frmMorePrivacyPolicyKAControllerExtObj);
    var frmMorePrivacyPolicyKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmMorePrivacyPolicyKAFormModel", frmMorePrivacyPolicyKAControllerObj);
    var frmMorePrivacyPolicyKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmMorePrivacyPolicyKAFormModelExtension", frmMorePrivacyPolicyKAFormModelObj);
    frmMorePrivacyPolicyKAFormModelObj.setFormModelExtensionObj(frmMorePrivacyPolicyKAFormModelExtObj);
    appContext.setFormController("frmMorePrivacyPolicyKA", frmMorePrivacyPolicyKAControllerObj);

    //frmMoreTermsAndConditionsKA
    var frmMoreTermsAndConditionsKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmMoreTermsAndConditionsKAConfig);
    var frmMoreTermsAndConditionsKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmMoreTermsAndConditionsKAController", appContext, frmMoreTermsAndConditionsKAModelConfigObj);
    var frmMoreTermsAndConditionsKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmMoreTermsAndConditionsKAControllerExtension", frmMoreTermsAndConditionsKAControllerObj);
    frmMoreTermsAndConditionsKAControllerObj.setControllerExtensionObject(frmMoreTermsAndConditionsKAControllerExtObj);
    var frmMoreTermsAndConditionsKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmMoreTermsAndConditionsKAFormModel", frmMoreTermsAndConditionsKAControllerObj);
    var frmMoreTermsAndConditionsKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmMoreTermsAndConditionsKAFormModelExtension", frmMoreTermsAndConditionsKAFormModelObj);
    frmMoreTermsAndConditionsKAFormModelObj.setFormModelExtensionObj(frmMoreTermsAndConditionsKAFormModelExtObj);
    appContext.setFormController("frmMoreTermsAndConditionsKA", frmMoreTermsAndConditionsKAControllerObj);

    //frmPickAProduct
    var frmPickAProductKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmPickAProductKAConfig);
    var frmPickAProductKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmPickAProductKAController", appContext, frmPickAProductKAModelConfigObj);
    var frmPickAProductKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmPickAProductKAControllerExtension", frmPickAProductKAControllerObj);
    frmPickAProductKAControllerObj.setControllerExtensionObject(frmPickAProductKAControllerExtObj);
    var frmPickAProductKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmPickAProductKAFormModel", frmPickAProductKAControllerObj);
    var frmPickAProductKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmPickAProductKAFormModelExtension", frmPickAProductKAFormModelObj);
    frmPickAProductKAFormModelObj.setFormModelExtensionObj(frmPickAProductKAFormModelExtObj);
    appContext.setFormController("frmPickAProductKA", frmPickAProductKAControllerObj);

    //frmEnterLocationKA
    var frmEnterLocationKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmEnterLocationKAConfig);
    var frmEnterLocationKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmEnterLocationKAController", appContext, frmEnterLocationKAModelConfigObj);
    var frmEnterLocationKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmEnterLocationKAControllerExtension", frmEnterLocationKAControllerObj);
    frmEnterLocationKAControllerObj.setControllerExtensionObject(frmEnterLocationKAControllerExtObj);
    var frmEnterLocationKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmEnterLocationKAFormModel", frmEnterLocationKAControllerObj);
    var frmEnterLocationKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmEnterLocationKAFormModelExtension", frmEnterLocationKAFormModelObj);
    frmEnterLocationKAFormModelObj.setFormModelExtensionObj(frmEnterLocationKAFormModelExtObj);
    appContext.setFormController("frmEnterLocationKA", frmEnterLocationKAControllerObj);

    //frmCreditCardsKA
    var frmCreditCardsKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmCreditCardsKAConfig);
    var frmCreditCardsKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmCreditCardsKAController", appContext, frmCreditCardsKAModelConfigObj);
    var frmCreditCardsKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmCreditCardsKAControllerExtension", frmCreditCardsKAControllerObj);
    frmCreditCardsKAControllerObj.setControllerExtensionObject(frmCreditCardsKAControllerExtObj);
    var frmCreditCardsKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmCreditCardsKAFormModel", frmCreditCardsKAControllerObj);
    var frmCreditCardsKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmCreditCardsKAFormModelExtension", frmCreditCardsKAFormModelObj);
    frmCreditCardsKAFormModelObj.setFormModelExtensionObj(frmCreditCardsKAFormModelExtObj);
    appContext.setFormController("frmCreditCardsKA", frmCreditCardsKAControllerObj);

    //frmAlertsKA
    var frmAlertsKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmAlertsKAConfig);
    var frmAlertsKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmAlertsKAController", appContext, frmAlertsKAModelConfigObj);
    var frmAlertsKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmAlertsKAControllerExtension", frmAlertsKAControllerObj);
    frmAlertsKAControllerObj.setControllerExtensionObject(frmAlertsKAControllerExtObj);
    var frmAlertsKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmAlertsKAFormModel", frmAlertsKAControllerObj);
    var frmAlertsKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmAlertsKAFormModelExtension", frmAlertsKAFormModelObj);
    frmAlertsKAFormModelObj.setFormModelExtensionObj(frmAlertsKAFormModelExtObj);
    appContext.setFormController("frmAlertsKA", frmAlertsKAControllerObj);

    //frmDeviceDeRegistrationKA
    var frmDeviceDeRegistrationKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmDeviceDeRegistrationKAConfig);
    var frmDeviceDeRegistrationKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmDeviceDeRegistrationKAController", appContext, frmDeviceDeRegistrationKAModelConfigObj);
    var frmDeviceDeRegistrationKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmDeviceDeRegistrationKAControllerExtension", frmDeviceDeRegistrationKAControllerObj);
    frmDeviceDeRegistrationKAControllerObj.setControllerExtensionObject(frmDeviceDeRegistrationKAControllerExtObj);
    var frmDeviceDeRegistrationKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmDeviceDeRegistrationKAFormModel", frmDeviceDeRegistrationKAControllerObj);
    var frmDeviceDeRegistrationKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmDeviceDeRegistrationKAFormModelExtension", frmDeviceDeRegistrationKAFormModelObj);
    frmDeviceDeRegistrationKAFormModelObj.setFormModelExtensionObj(frmDeviceDeRegistrationKAFormModelExtObj);
    appContext.setFormController("frmDeviceDeRegistrationKA", frmDeviceDeRegistrationKAControllerObj);

    //frmTransactionDetailKA
    var frmTransactionDetailKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmTransactionDetailKAConfig);
    var frmTransactionDetailKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmTransactionDetailKAController", appContext, frmTransactionDetailKAModelConfigObj);
    var frmTransactionDetailKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmTransactionDetailKAControllerExtension", frmTransactionDetailKAControllerObj);
    frmTransactionDetailKAControllerObj.setControllerExtensionObject(frmTransactionDetailKAControllerExtObj);
    var frmTransactionDetailKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmTransactionDetailKAFormModel", frmTransactionDetailKAControllerObj);
    var frmTransactionDetailKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmTransactionDetailKAFormModelExtension", frmTransactionDetailKAFormModelObj);
    frmTransactionDetailKAFormModelObj.setFormModelExtensionObj(frmTransactionDetailKAFormModelExtObj);
    appContext.setFormController("frmTransactionDetailKA", frmTransactionDetailKAControllerObj);

    //frmStandingInstructions
    kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("Initializing app");
    var frmStandingInstructionsModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmStandingInstructionsConfig);
    var frmStandingInstructionsControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmStandingInstructionsController", appContext, frmStandingInstructionsModelConfigObj);
    var frmStandingInstructionsControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmStandingInstructionsControllerExtension", frmStandingInstructionsControllerObj);
    frmStandingInstructionsControllerObj.setControllerExtensionObject(frmStandingInstructionsControllerExtObj);
    var frmStandingInstructionsFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmStandingInstructionsFormModel", frmStandingInstructionsControllerObj);
    var frmStandingInstructionsFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmStandingInstructionsFormModelExtension", frmStandingInstructionsFormModelObj);
    frmStandingInstructionsFormModelObj.setFormModelExtensionObj(frmStandingInstructionsFormModelExtObj);
    appContext.setFormController("frmStandingInstructions", frmStandingInstructionsControllerObj);

    //frmConfirmStandingInstructions
    kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("Initializing app");
    var frmConfirmStandingInstructionsModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmConfirmStandingInstructionsConfig);
    var frmConfirmStandingInstructionsControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmConfirmStandingInstructionsController", appContext, frmConfirmStandingInstructionsModelConfigObj);
    var frmConfirmStandingInstructionsControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmConfirmStandingInstructionsControllerExtension", frmConfirmStandingInstructionsControllerObj);
    frmConfirmStandingInstructionsControllerObj.setControllerExtensionObject(frmConfirmStandingInstructionsControllerExtObj);
    var frmConfirmStandingInstructionsFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmConfirmStandingInstructionsFormModel", frmConfirmStandingInstructionsControllerObj);
    var frmConfirmStandingInstructionsFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmConfirmStandingInstructionsFormModelExtension", frmConfirmStandingInstructionsFormModelObj);
    frmConfirmStandingInstructionsFormModelObj.setFormModelExtensionObj(frmConfirmStandingInstructionsFormModelExtObj);
    appContext.setFormController("frmConfirmStandingInstructions", frmConfirmStandingInstructionsControllerObj);


  } catch (err) {
    kony.application.dismissLoadingScreen();
    var exception = appContext.getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_APP_INIT_FORMS, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_APP_INIT_FORMS, err);
    kony.sdk.mvvm.log.error(exception.toString());
    exceptionLogCall("initApplicationForms","UI ERROR","UI",e);
    throw exception;
  }
};