//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmStandingInstructionsAr() {
frmStandingInstructions.setDefaultUnit(kony.flex.DP);
var flxSIHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "15%",
"id": "flxSIHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "s",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxSIHeader.setDefaultUnit(kony.flex.DP);
var flxHeaderMain1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50%",
"id": "flxHeaderMain1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxHeaderMain1.setDefaultUnit(kony.flex.DP);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_b8317cd065d443688bdb2c7b3c9f499f,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 2
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"centerY": "50%",
"height": "50dp",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblBack0b59abd6f999148 = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblBack0b59abd6f999148",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBack.add(lblBackIcon, CopylblBack0b59abd6f999148);
var lblStandingInstructions = new kony.ui.Label({
"height": "100%",
"id": "lblStandingInstructions",
"isVisible": true,
"left": "20%",
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.paymentMenu.standingInstruction"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": "60%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var btnAdd = new kony.ui.Button({
"focusSkin": "CopyslButtonGlosssBlue0eff25d8e6d2f4d",
"height": "100%",
"id": "btnAdd",
"isVisible": false,
"right": "0%",
"skin": "CopyslButtonGlosssBlue0eff25d8e6d2f4d",
"text": "u",
"top": "0%",
"width": "20%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
flxHeaderMain1.add(flxBack, lblStandingInstructions, btnAdd);
var flxSearch = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "40%",
"id": "flxSearch",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "50%",
"width": "70%",
"zIndex": 1
}, {}, {});
flxSearch.setDefaultUnit(kony.flex.DP);
var lblSearch = new kony.ui.Label({
"height": "100%",
"id": "lblSearch",
"isVisible": true,
"right": "0%",
"skin": "sknSearchIcon",
"text": "h",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "10%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxUnderLineSearch = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderLineSearch",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "92%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxUnderLineSearch.setDefaultUnit(kony.flex.DP);
flxUnderLineSearch.add();
var tbxSearch = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"centerY": "50%",
"focusSkin": "sknTxtBoxSearch",
"height": "100%",
"id": "tbxSearch",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "10%",
"onTextChange": AS_TextField_f03949e0730e462893219cfb3c75e89f,
"placeholder": kony.i18n.getLocalizedString("i18n.transfer.searchPlh"),
"secureTextEntry": false,
"skin": "sknTxtBoxSearch",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "90%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
"placeholderSkin": "sknPlaceHolder",
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
flxSearch.add(lblSearch, flxUnderLineSearch, tbxSearch);
var lblSortSI = new kony.ui.Label({
"height": "40%",
"id": "lblSortSI",
"isVisible": true,
"onTouchEnd": AS_Label_b383598ba901460b9cc21770b7049544,
"left": "15%",
"skin": "CopyslLabel0i2b4c385eb5140",
"text": "l",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "50%",
"width": "10%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblFilter = new kony.ui.Label({
"height": "40%",
"id": "lblFilter",
"isVisible": true,
"onTouchEnd": AS_Label_d56f7251288a498e8b89984cc5e8d518,
"left": "3%",
"skin": "CopyslLabel0i2b4c385eb5140",
"text": "n",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "50%",
"width": "10%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxSIHeader.add(flxHeaderMain1, flxSearch, lblSortSI, lblFilter);
var flxSIList = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "83%",
"id": "flxSIList",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxSIList.setDefaultUnit(kony.flex.DP);
var segSIList = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"data": [{
"CopylblAmount0a2751be25d5944": "o",
"lblAmount": "13244 JOD",
"lblInitial": "JW",
"lblTransactionDesc": "Jordan Water",
"lblTransactiondate": "10 Feb 2010"
}, {
"CopylblAmount0a2751be25d5944": "o",
"lblAmount": "13244 JOD",
"lblInitial": "JW",
"lblTransactionDesc": "Jordan Water",
"lblTransactiondate": "10 Feb 2010"
}, {
"CopylblAmount0a2751be25d5944": "o",
"lblAmount": "13244 JOD",
"lblInitial": "JW",
"lblTransactionDesc": "Jordan Water",
"lblTransactiondate": "10 Feb 2010"
}],
"groupCells": false,
"height": "100%",
"id": "segSIList",
"isVisible": true,
"right": "0%",
"needPageIndicator": true,
"onRowClick": AS_Segment_efe741d62b2d40ba9bab8c4a6bbaf03a,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "Copyseg0c15349e52ca249",
"rowSkin": "Copyseg0c15349e52ca249",
"rowTemplate": flxtmpSIList,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "1e415b00",
"separatorRequired": true,
"separatorThickness": 1,
"showScrollbars": false,
"top": "0%",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"CopyflxTAmount0bc17ffab355945": "CopyflxTAmount0bc17ffab355945",
"CopylblAmount0a2751be25d5944": "CopylblAmount0a2751be25d5944",
"flxIcon1": "flxIcon1",
"flxTAmount": "flxTAmount",
"flxUserDetails": "flxUserDetails",
"flxtmpSIList": "flxtmpSIList",
"lblAmount": "lblAmount",
"lblInitial": "lblInitial",
"lblTransactionDesc": "lblTransactionDesc",
"lblTransactiondate": "lblTransactiondate"
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var lblNoSI = new kony.ui.Label({
"centerX": "50%",
"centerY": "5%",
"id": "lblNoSI",
"isVisible": false,
"skin": "sknlblNoSI",
"text": kony.i18n.getLocalizedString("i18n.SI.noSI"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblNoSearchResults = new kony.ui.Label({
"centerX": "50%",
"centerY": "5%",
"id": "lblNoSearchResults",
"isVisible": false,
"skin": "sknlblNoSI",
"text": "No Records Found",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxSIList.add(segSIList, lblNoSI, lblNoSearchResults);
var flxAccountDetailsSort = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxAccountDetailsSort",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"onClick": AS_FlexContainer_b79350c8460e4c76aabfa4b2bc5dcf34,
"skin": "sknWhiteBGroundedBorder",
"top": "5%",
"width": "100%",
"zIndex": 3
}, {}, {});
flxAccountDetailsSort.setDefaultUnit(kony.flex.DP);
var flxSortTitle = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "flxSortTitle",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxSortTitle.setDefaultUnit(kony.flex.DP);
var flxLine1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "3%",
"id": "flxLine1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknGreyLine50Opc",
"top": "70%",
"width": "80%",
"zIndex": 1
}, {}, {});
flxLine1.setDefaultUnit(kony.flex.DP);
flxLine1.add();
var lblSortby = new kony.ui.Label({
"centerX": "50%",
"id": "lblSortby",
"isVisible": true,
"skin": "sknCairoBold120",
"text": kony.i18n.getLocalizedString("i18n.common.sortby"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "3%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxSortTitle.add(flxLine1, lblSortby);
var flxDate = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "flxDate",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxDate.setDefaultUnit(kony.flex.DP);
var flxSortDateDesc = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "71%",
"centerY": "50%",
"clipBounds": true,
"height": "80%",
"id": "flxSortDateDesc",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_ef5b204a0565442db02d4bf6b6f166f7,
"skin": "slFbox",
"width": "10%",
"zIndex": 1
}, {}, {});
flxSortDateDesc.setDefaultUnit(kony.flex.DP);
var imgSortDateDesc = new kony.ui.Image2({
"centerX": "50%",
"centerY": "50%",
"id": "imgSortDateDesc",
"isVisible": true,
"skin": "slImage",
"src": "map_arrow_down_nonselected.png",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxSortDateDesc.add(imgSortDateDesc);
var lblSortDate = new kony.ui.Label({
"centerX": "40%",
"centerY": "50%",
"id": "lblSortDate",
"isVisible": true,
"skin": "sknCarioSemiBold130perDRKBluefont80Opc",
"text": kony.i18n.getLocalizedString("i18n.accounts.date"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxSortDateAsc = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "60%",
"centerY": "50%",
"clipBounds": true,
"height": "80%",
"id": "flxSortDateAsc",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_e9f9d35b64b247d79bd235d9188bcb3d,
"skin": "slFbox",
"width": "10%",
"zIndex": 1
}, {}, {});
flxSortDateAsc.setDefaultUnit(kony.flex.DP);
var imgSortDateAsc = new kony.ui.Image2({
"centerX": "50%",
"centerY": "50%",
"id": "imgSortDateAsc",
"isVisible": true,
"skin": "slImage",
"src": "map_arrow_straight_nonselected.png",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxSortDateAsc.add(imgSortDateAsc);
flxDate.add(flxSortDateDesc, lblSortDate, flxSortDateAsc);
var flxAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "flxAmount",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAmount.setDefaultUnit(kony.flex.DP);
var lblSortAmount = new kony.ui.Label({
"centerX": "40%",
"centerY": "50%",
"id": "lblSortAmount",
"isVisible": true,
"skin": "sknCarioSemiBold130perDRKBluefont80Opc",
"text": kony.i18n.getLocalizedString("i18n.accounts.amount"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxSortAmountAsc = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "60%",
"centerY": "50%",
"clipBounds": true,
"height": "80%",
"id": "flxSortAmountAsc",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_i0ea18370d53442e891526e350260641,
"skin": "slFbox",
"width": "10%",
"zIndex": 1
}, {}, {});
flxSortAmountAsc.setDefaultUnit(kony.flex.DP);
var imgSortAmountAsc = new kony.ui.Image2({
"centerX": "50%",
"centerY": "50%",
"id": "imgSortAmountAsc",
"isVisible": true,
"skin": "slImage",
"src": "map_arrow_straight_nonselected.png",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxSortAmountAsc.add(imgSortAmountAsc);
var flxSortAmountDesc = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "71%",
"centerY": "50%",
"clipBounds": true,
"height": "80%",
"id": "flxSortAmountDesc",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_f73c2bbf39bb4af68638e1f198787c59,
"skin": "slFbox",
"width": "10%",
"zIndex": 1
}, {}, {});
flxSortAmountDesc.setDefaultUnit(kony.flex.DP);
var imgSortAmountDesc = new kony.ui.Image2({
"centerX": "50%",
"centerY": "50%",
"id": "imgSortAmountDesc",
"isVisible": true,
"skin": "slImage",
"src": "map_arrow_down_nonselected.png",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxSortAmountDesc.add(imgSortAmountDesc);
flxAmount.add(lblSortAmount, flxSortAmountAsc, flxSortAmountDesc);
flxAccountDetailsSort.add(flxSortTitle, flxDate, flxAmount);
frmStandingInstructions.add(flxSIHeader, flxSIList, flxAccountDetailsSort);
};
function frmStandingInstructionsGlobalsAr() {
frmStandingInstructionsAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmStandingInstructionsAr,
"enableScrolling": false,
"enabledForIdleTimeout": true,
"id": "frmStandingInstructions",
"layoutType": kony.flex.FLOW_VERTICAL,
"needAppMenu": false,
"preShow": AS_Form_h5116efbf5f846729adaf1fd4c1c1587,
"skin": "sknmainGradient"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FLOW_VERTICAL,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_f796d777c179488580d21c084220b9dd,
"retainScrollPosition": false,
"titleBar": true,
"titleBarSkin": "slTitleBar",
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
