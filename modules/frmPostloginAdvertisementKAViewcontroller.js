kony = kony || {};
kony.rb = kony.rb || {};



kony.rb.frmPostloginAdvertisementKAViewcontroller = function() {
  var numOfAds = 0;
  var currentFlex = 1;
  var xOffset = 0;
  var nonInfeedAds = 0;
  var imageObjArray = [];
  var imageDownloadFailureCount = 0; 
  var gestIDs = [];
  var isSwipeDone = false;
};

kony.rb.frmPostloginAdvertisementKAViewcontroller.prototype.frmPostloginAdvertisementPreshow = function()
{
  KNYMetricsService.sendCustomMetrics("frmPostLoginAdvertisementKA", {"formName" : "frmPostLoginAdvertisementKA"});
  frmPostLoginAdvertisementKA.statusBarHidden = true;
  var navManager =  applicationManager.getNavManager();
  this.nonInfeedAds =  navManager.getCustomInfo("frmPostLoginAdvertisementKA");
  this.numOfAds = this.nonInfeedAds.length;
  var maxNumbAds = 5;
  if(this.numOfAds > maxNumbAds)
    {
      kony.print("PostLogin Ad's count exceeded 5");
      showFormOrderList();
    }
  this.isSwipeDone = false;
  this.xOffset = 0;
  this.currentFlex = 1;
  this.imageDownloadFailureCount = 0;
  this.imageObjArray = [];
  if(!this.gestIDs)
    {
      this.gestIDs = [];
    }
  disableUntilImageDownloads();
  removeGestureRecognisers();
  var date = new Date();
    for(var j = 1; j <= this.numOfAds; j++)
    {
      param=date.getTime();
      frmPostLoginAdvertisementKA["flxAd"+j+"KA"].left="0dp";
      frmPostLoginAdvertisementKA["flxAd"+j+"KA"].setVisibility(false);
      frmPostLoginAdvertisementKA["imgAd"+j+"KA"].src = getImageURLBasedOnDeviceType(this.nonInfeedAds[j-1].adImageURL)+"?Param="+param;
    }
  frmPostLoginAdvertisementKA.flxScrollContainerAdKA.setContentOffset({
    x: this.xOffset,
    y: 0
  }, true);
  
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();  
};

 
function onAllAdsDownloadFailure()
{
  frmPostLoginAdvertisementKA.imgAdDownloadFailureKA.src = "failuread.png";
  //addtime interval
  ShowLoadingScreen();//close screen
  showFormOrderList();
}
function alignFlexInScrollContainer(position,flxNumber)
{
  if(position === 1)
  {
    frmPostLoginAdvertisementKA["flxAd"+flxNumber+"KA"].setVisibility(true);
  }
  else
  {
    var leftVal = (position-1)*Number(kony.os.deviceInfo().screenWidth);
    frmPostLoginAdvertisementKA["flxAd"+flxNumber+"KA"].left = leftVal+"dp";
    frmPostLoginAdvertisementKA["flxAd"+flxNumber+"KA"].setVisibility(true);
  }
  frmPostLoginAdvertisementKA.flxAdDownloadFailureKA.setVisibility(false);
  var postLoginVC = applicationManager.getpostLoginAdsViewController();
  var noOfDownloadedAds = position;
  if(noOfDownloadedAds>1 && noOfDownloadedAds<=5)
  {
    if(noOfDownloadedAds === 2)
    {
      frmPostLoginAdvertisementKA.flxProgressButton1KA.setVisibility(true);
      frmPostLoginAdvertisementKA.flxProgressButton2KA.setVisibility(true);
      frmPostLoginAdvertisementKA.flxProgressButton1KA.left="46%";
      frmPostLoginAdvertisementKA.flxProgressButton1KA.skin = "sknFlxProgressDotActive";
      frmPostLoginAdvertisementKA.flxProgressButton2KA.skin = "sknFlxProgressDotInactive";
    }
    else if(noOfDownloadedAds === 3)
    {
      frmPostLoginAdvertisementKA.flxProgressButton3KA.setVisibility(true);
      frmPostLoginAdvertisementKA.flxProgressButton3KA.skin = "sknFlxProgressDotInactive";
      frmPostLoginAdvertisementKA.flxProgressButton1KA.left="43.5%";
    }
    else if(noOfDownloadedAds === 4)
    {
      frmPostLoginAdvertisementKA.flxProgressButton4KA.setVisibility(true);
      frmPostLoginAdvertisementKA.flxProgressButton4KA.skin = "sknFlxProgressDotInactive";
      frmPostLoginAdvertisementKA.flxProgressButton1KA.left="41%";
    }
    else
    {
      frmPostLoginAdvertisementKA.flxProgressButton5KA.setVisibility(true);
      frmPostLoginAdvertisementKA.flxProgressButton5KA.skin = "sknFlxProgressDotInactive";
      frmPostLoginAdvertisementKA.flxProgressButton1KA.left="38%";
    }
    frmPostLoginAdvertisementKA.flxProgressBarKA.forceLayout();
  }
  else
  {
    if(noOfDownloadedAds!==1)
      kony.print("There are more than 5 Ad's");
  }
}

function disableUntilImageDownloads()
{
  frmPostLoginAdvertisementKA.btnAdAction2KA.setVisibility(false);
  frmPostLoginAdvertisementKA.flxProgressBarKA.setVisibility(false);
  frmPostLoginAdvertisementKA.flxProgressButton1KA.setVisibility(false);
  frmPostLoginAdvertisementKA.flxProgressButton2KA.setVisibility(false);
  frmPostLoginAdvertisementKA.flxProgressButton3KA.setVisibility(false);
  frmPostLoginAdvertisementKA.flxProgressButton4KA.setVisibility(false);
  frmPostLoginAdvertisementKA.flxProgressButton5KA.setVisibility(false);
  frmPostLoginAdvertisementKA.flxProgressBarKA.forceLayout();
  frmPostLoginAdvertisementKA.flxAdDownloadFailureKA.setVisibility(true);
  frmPostLoginAdvertisementKA.imgAdDownloadFailureKA.src = "loader2.gif";
}

function removeGestureRecognisers()
{
 
  var postLoginVC = applicationManager.getpostLoginAdsViewController();
  if(postLoginVC.gestIDs.length!==0)
  {
    var  swipeGestureID = postLoginVC.gestIDs[0];
    var  tapGestureID = postLoginVC.gestIDs[1];
  	frmPostLoginAdvertisementKA.flxAdImageOnclickAreaKA.removeGestureRecognizer(swipeGestureID);
  	frmPostLoginAdvertisementKA.flxAdImageOnclickAreaKA.removeGestureRecognizer(tapGestureID);
    postLoginVC.gestIDs = [];
  }
}

function setGestureRecogniser()
{
  
  var postLoginVC = applicationManager.getpostLoginAdsViewController();
  if(postLoginVC.gestIDs.length === 0)
    {
      var swipeGestID = frmPostLoginAdvertisementKA.flxAdImageOnclickAreaKA.setGestureRecognizer(2, {
        fingers: 1,
        swipedistance: 20,
        swipevelocity: 60
      }, onAdSwipe);
      var tapGestID = frmPostLoginAdvertisementKA.flxAdImageOnclickAreaKA.setGestureRecognizer(1, {
        fingers: 1,
        taps:1
      }, onAdTap);
      postLoginVC.gestIDs[0]=swipeGestID;
      postLoginVC.gestIDs[1]=tapGestID;
     }
}
function enableAfterImageDownloads(adNumber)
{
  if(isAction2Present(adNumber))
  {
    frmPostLoginAdvertisementKA.btnAdAction2KA.setVisibility(true);
    frmPostLoginAdvertisementKA.btnAdAction2KA.text = getAction2Title(adNumber);
  }
  frmPostLoginAdvertisementKA.flxProgressBarKA.setVisibility(true);
}



function onAdSwipe(widget, gestureInfo, context) {
  var postLoginVC = applicationManager.getpostLoginAdsViewController();
  var adNum = postLoginVC.imageObjArray.length;
  var scWidth= Number(kony.os.deviceInfo().screenWidth);
  var currFlex = Number(postLoginVC.currentFlex);
  var xVal = Number(postLoginVC.xOffset);
  var adID = "postLoginAd"+(currAd+1);
  var isThereChange = false;
  if(!postLoginVC.isSwipeDone)
  {
    KNYMetricsService.sendCustomMetrics("frmPostLoginAdvertisementKA", {"adAction":"swipePostLoginAd"}); 
    postLoginVC.isSwipeDone = true;
  }
  if (gestureInfo.swipeDirection === 1) {
    if (currFlex>=1 && currFlex<adNum)
    {
      isThereChange = true;
      xVal = xVal + scWidth;
    }
  } else if (gestureInfo.swipeDirection === 2) {
    if (currFlex>1 && currFlex<=adNum) {
      isThereChange = true;
      xVal = xVal - scWidth;  
    } 
  }
    if(isThereChange)
    {
      frmPostLoginAdvertisementKA.flxScrollContainerAdKA.setContentOffset({
        x: xVal,
        y: 0
      }, true);
      currFlex = Math.floor(Number(xVal)/scWidth)+1;
      var currAd = postLoginVC.imageObjArray[currFlex-1];
      if (isAction2Present(currAd)) {
        frmPostLoginAdvertisementKA.btnAdAction2KA.setVisibility(true);
        frmPostLoginAdvertisementKA.btnAdAction2KA.text = getAction2Title(currAd);
      } else {
        frmPostLoginAdvertisementKA.btnAdAction2KA.setVisibility(false);
      }
      for (var j = 1; j <= adNum; j++) {
        if (j === currFlex){
          frmPostLoginAdvertisementKA["flxProgressButton" + j + "KA"].skin = "sknFlxProgressDotActive";
        }
        else{
          frmPostLoginAdvertisementKA["flxProgressButton" + j + "KA"].skin = "sknFlxProgressDotInactive";
        }
      }
      frmPostLoginAdvertisementKA.flxProgressBarKA.forceLayout();
      frmPostLoginAdvertisementKA.flxScrollContainerAdKA.forceLayout();
      applicationManager.getpostLoginAdsViewController().currentFlex = currFlex;
      applicationManager.getpostLoginAdsViewController().xOffset = xVal;
    }
}
function isAction2Present(adNumber) {
  var postLoginVC = applicationManager.getpostLoginAdsViewController();
  var nonInfeedAds = postLoginVC.nonInfeedAds;
  if (nonInfeedAds[adNumber-1].adAction2 && (nonInfeedAds[adNumber-1].adAction2 !== null || nonInfeedAds[adNumber-1].adAction2 !== ""))
    return true;
  else
    return false;
}

function getAction2Title(adNumber) {
  var postLoginVC = applicationManager.getpostLoginAdsViewController();
  var nonInfeedAds = postLoginVC.nonInfeedAds;
  var title = nonInfeedAds[adNumber-1].adTitle;
  return title;
}

kony.rb.frmPostloginAdvertisementKAViewcontroller.prototype.adActionButtonOnClick =function()
{
  ShowLoadingScreen();
  var postLoginVC = applicationManager.getpostLoginAdsViewController();
  var nonInfeedAds = postLoginVC.nonInfeedAds;
  var currFlex = postLoginVC.currentFlex;
  var currAd = postLoginVC.imageObjArray[currFlex-1];
  if(nonInfeedAds[currAd-1].adAction2)
  {
    kony.application.openURL(nonInfeedAds[currAd-1].adAction2);
  }
  else 
  {
    alert("Navigation URL is not there.");
  }
  KNYMetricsService.sendCustomMetrics("frmPostLoginAdvertisementKA", {"adAction" : "action2PostLoginAd"+currAd});
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen(); 
};

function onAdTap(widget, gestureInfo, context) {
  ShowLoadingScreen();
  var postLoginVC = applicationManager.getpostLoginAdsViewController();
  var nonInfeedAds = postLoginVC.nonInfeedAds;
  var currFlex = postLoginVC.currentFlex;
  var currAd = postLoginVC.imageObjArray[currFlex-1];
  if(nonInfeedAds[currAd-1].adAction1)
  {
    kony.application.openURL(nonInfeedAds[currAd-1].adAction1);
  }
  else 
  {
    alert("Navigation URL is not there.");
  }
  KNYMetricsService.sendCustomMetrics("frmPostLoginAdvertisementKA", {"adAction" : "action1PostLoginAd"+currAd}); 
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen(); 
}

kony.rb.frmPostloginAdvertisementKAViewcontroller.prototype.adCloseButtonOnClick =function()
{
  ShowLoadingScreen();
  var postLoginVC = applicationManager.getpostLoginAdsViewController();
  var currFlex = postLoginVC.currentFlex;
  var currAd = postLoginVC.imageObjArray[currFlex-1];
  KNYMetricsService.sendCustomMetrics("frmPostLoginAdvertisementKA", {"adAction" : "closePostLoginAd"+currAd});
  showFormOrderList();
};

kony.rb.frmPostloginAdvertisementKAViewcontroller.prototype.onAdImageDownloadComplete =function(issuccess,adNumber)
{
  var postLoginVC = applicationManager.getpostLoginAdsViewController();
    if(issuccess)
    {
      var i=postLoginVC.imageObjArray.length;
      alignFlexInScrollContainer(i+1,adNumber);
      if(i===0)
      {
        KNYMetricsService.sendCustomMetrics("frmPostLoginAdvertisementKA", {"adID" : "postLoginAd"+adNumber});
        setGestureRecogniser();
        enableAfterImageDownloads(adNumber);
      }
      postLoginVC.imageObjArray[i] = adNumber;
    }    
  else
  {
    postLoginVC.imageDownloadFailureCount++;
    if(postLoginVC.imageDownloadFailureCount === postLoginVC.numOfAds)
    {
      kony.print("All Ad's download failed in PostLoginAds");
      onAllAdsDownloadFailure();
    }
  }
};
