//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmPinEntryStep2Ar() {
frmPinEntryStep2.setDefaultUnit(kony.flex.DP);
var flxMainContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0dp",
"clipBounds": true,
"id": "flxMainContainer",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"skin": "CopysknBOJblueBG0dbfc60802b4c4a",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxMainContainer.setDefaultUnit(kony.flex.DP);
var hdrPinStep1 = new kony.ui.Label({
"centerX": "50%",
"id": "hdrPinStep1",
"isVisible": true,
"right": "29%",
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.pinLogin.setPinHeader"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "3%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var stepCountLabel = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "58%",
"clipBounds": true,
"height": "7.50%",
"id": "stepCountLabel",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"skin": "slFbox",
"top": "1%",
"width": "45%",
"zIndex": 1
}, {}, {});
stepCountLabel.setDefaultUnit(kony.flex.DP);
var StepLabel = new kony.ui.Label({
"height": "100%",
"id": "StepLabel",
"isVisible": true,
"right": "0%",
"skin": "skniconButtonLabel",
"text": kony.i18n.getLocalizedString("i18n.pinLogin.stepLabel"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var stepNumberFlex = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "23dp",
"id": "stepNumberFlex",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "2%",
"skin": "sknStepNumber",
"width": "23dp",
"zIndex": 1
}, {}, {});
stepNumberFlex.setDefaultUnit(kony.flex.DP);
var StepNumberCount = new kony.ui.Label({
"centerX": "45%",
"centerY": "45%",
"id": "StepNumberCount",
"isVisible": true,
"skin": "sknlblblue",
"text": "2",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
stepNumberFlex.add(StepNumberCount);
var totalStepsLabel = new kony.ui.Label({
"height": "100%",
"id": "totalStepsLabel",
"isVisible": true,
"right": "2%",
"skin": "skniconButtonLabel",
"text": kony.i18n.getLocalizedString("i18n.pinLogin.totalSteps"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
stepCountLabel.add( totalStepsLabel, stepNumberFlex,StepLabel);
var DidNotMatchLabel = new kony.ui.Label({
"centerX": "50%",
"id": "DidNotMatchLabel",
"isVisible": false,
"skin": "sknErrorMessageEC223BKA",
"text": kony.i18n.getLocalizedString("i18n.common.notmatc"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "-1%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var PinEntryLabel = new kony.ui.Label({
"centerX": "50%",
"id": "PinEntryLabel",
"isVisible": true,
"skin": "sknLblWhite100",
"text": kony.i18n.getLocalizedString("i18n.con.pin"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxProgressButtons = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "4%",
"id": "flxProgressButtons",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "25.00%",
"skin": "flexTransparent",
"top": "2%",
"width": "60%",
"zIndex": 1
}, {}, {});
flxProgressButtons.setDefaultUnit(kony.flex.DP);
var flxProgressButton1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "15dp",
"id": "flxProgressButton1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "sknFlxProgressButtonEmpty",
"top": "0%",
"width": "15dp",
"zIndex": 1
}, {}, {});
flxProgressButton1.setDefaultUnit(kony.flex.DP);
flxProgressButton1.add();
var flxProgressButton6 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "15dp",
"id": "flxProgressButton6",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "75%",
"skin": "sknFlxProgressButtonEmpty",
"top": "0%",
"width": "15dp",
"zIndex": 1
}, {}, {});
flxProgressButton6.setDefaultUnit(kony.flex.DP);
flxProgressButton6.add();
var flxProgressButton5 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "15dp",
"id": "flxProgressButton5",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "60%",
"skin": "sknFlxProgressButtonEmpty",
"top": "0%",
"width": "15dp",
"zIndex": 1
}, {}, {});
flxProgressButton5.setDefaultUnit(kony.flex.DP);
flxProgressButton5.add();
var flxProgressButton4 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "15dp",
"id": "flxProgressButton4",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "45%",
"skin": "sknFlxProgressButtonEmpty",
"top": "0%",
"width": "15dp",
"zIndex": 1
}, {}, {});
flxProgressButton4.setDefaultUnit(kony.flex.DP);
flxProgressButton4.add();
var flxProgressButton3 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "15dp",
"id": "flxProgressButton3",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "30%",
"skin": "sknFlxProgressButtonEmpty",
"top": "0%",
"width": "15dp",
"zIndex": 1
}, {}, {});
flxProgressButton3.setDefaultUnit(kony.flex.DP);
flxProgressButton3.add();
var flxProgressButton2 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "15dp",
"id": "flxProgressButton2",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "15%",
"skin": "sknFlxProgressButtonEmpty",
"top": "0%",
"width": "15dp",
"zIndex": 1
}, {}, {});
flxProgressButton2.setDefaultUnit(kony.flex.DP);
flxProgressButton2.add();
flxProgressButtons.add(flxProgressButton1, flxProgressButton6, flxProgressButton5, flxProgressButton4, flxProgressButton3, flxProgressButton2);
var flxDialPad = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "66%",
"id": "flxDialPad",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "flexTransparent",
"top": "2%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxDialPad.setDefaultUnit(kony.flex.DP);
var btnOne = new kony.ui.Button({
"centerX": "22%",
"focusSkin": "btnDialpadSkinOnFocus",
"height": "72dp",
"id": "btnOne",
"isVisible": true,
"right": "4%",
"onClick": AS_Button_d4a7be7142804fb88143b52347fed621,
"skin": "btnNumber",
"text": "1",
"top": "2%",
"width": "72dp",
"zIndex": 22
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnFour = new kony.ui.Button({
"centerX": "22%",
"focusSkin": "btnDialpadSkinOnFocus",
"height": "72dp",
"id": "btnFour",
"isVisible": true,
"right": "4%",
"onClick": AS_Button_b14cc58f415c482da55169c57df4f427,
"skin": "btnNumber",
"text": "4",
"top": "24%",
"width": "72dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnSeven = new kony.ui.Button({
"centerX": "22%",
"focusSkin": "btnDialpadSkinOnFocus",
"height": "72dp",
"id": "btnSeven",
"isVisible": true,
"right": "4%",
"onClick": AS_Button_h6aa8f0275a2448a8002421f4042fed1,
"skin": "btnNumber",
"text": "7",
"top": "45%",
"width": "72dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnZero = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "btnDialpadSkinOnFocus",
"height": "72dp",
"id": "btnZero",
"isVisible": true,
"right": "36%",
"onClick": AS_Button_g26b720583874a94a7e618f09e7a7a93,
"skin": "btnNumber",
"text": "0",
"top": "65%",
"width": "72dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnTwo = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "btnDialpadSkinOnFocus",
"height": "72dp",
"id": "btnTwo",
"isVisible": true,
"right": "36%",
"onClick": AS_Button_b77c401991cf4e9687c06eff46c99c78,
"skin": "btnNumber",
"text": "2",
"top": "2%",
"width": "72dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnFive = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "btnDialpadSkinOnFocus",
"height": "72dp",
"id": "btnFive",
"isVisible": true,
"right": "36%",
"onClick": AS_Button_bd414352519944a2a8b600a229bb1f0c,
"skin": "btnNumber",
"text": "5",
"top": "24%",
"width": "72dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnEight = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "btnDialpadSkinOnFocus",
"height": "72dp",
"id": "btnEight",
"isVisible": true,
"right": "36%",
"onClick": AS_Button_bbd5e94fa2f04c7ab665e216f984244d,
"skin": "btnNumber",
"text": "8",
"top": "45%",
"width": "72dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnThree = new kony.ui.Button({
"centerX": "78%",
"focusSkin": "btnDialpadSkinOnFocus",
"height": "72dp",
"id": "btnThree",
"isVisible": true,
"right": "67.97%",
"onClick": AS_Button_b2410f1b6f2046e6b613840a0a58c54b,
"left": "5.00%",
"skin": "btnNumber",
"text": "3",
"top": "2.00%",
"width": "72dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnSix = new kony.ui.Button({
"centerX": "78%",
"focusSkin": "btnDialpadSkinOnFocus",
"height": "72dp",
"id": "btnSix",
"isVisible": true,
"right": "68%",
"onClick": AS_Button_b32fd7fb9edd4d4286e90efda2dd586a,
"left": "10dp",
"skin": "btnNumber",
"text": "6",
"top": "24%",
"width": "72dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnNine = new kony.ui.Button({
"centerX": "77.97%",
"focusSkin": "btnDialpadSkinOnFocus",
"height": "72dp",
"id": "btnNine",
"isVisible": true,
"right": "68%",
"onClick": AS_Button_ca09e90591ce48499e35636768e0eda7,
"skin": "btnNumber",
"text": "9",
"top": "45.03%",
"width": "72dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxDialPad.add(btnOne, btnFour, btnSeven, btnZero, btnTwo, btnFive, btnEight, btnThree, btnSix, btnNine);
var flxbottomContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxbottomContainer",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": -50,
"width": "100%",
"zIndex": 1
}, {}, {});
flxbottomContainer.setDefaultUnit(kony.flex.DP);
var btnCancel = new kony.ui.Button({
"bottom": "15%",
"centerX": "25%",
"centerY": "50%",
"focusSkin": "sknprimaryAction",
"height": "80%",
"id": "btnCancel",
"isVisible": true,
"onClick": AS_Button_efd54815b1d742ff91b81e2a81405830,
"skin": "sknprimaryActionFcs",
"text": kony.i18n.getLocalizedString("i18n.common.cancelC"),
"width": "40%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnDelete = new kony.ui.Button({
"bottom": "15%",
"centerX": "75%",
"centerY": "50%",
"focusSkin": "sknprimaryActionFcs",
"height": "80%",
"id": "btnDelete",
"isVisible": true,
"onClick": AS_Button_h834e409c4344faa9e7ef5052d41c4d8,
"skin": "sknprimaryAction",
"text": kony.i18n.getLocalizedString("i18n.Messages.Delete"),
"width": "40%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxbottomContainer.add(btnCancel, btnDelete);
flxMainContainer.add(hdrPinStep1, stepCountLabel, DidNotMatchLabel, PinEntryLabel, flxProgressButtons, flxDialPad, flxbottomContainer);
frmPinEntryStep2.add(flxMainContainer);
};
function frmPinEntryStep2GlobalsAr() {
frmPinEntryStep2Ar = new kony.ui.Form2({
"addWidgets": addWidgetsfrmPinEntryStep2Ar,
"bounces": false,
"enableScrolling": false,
"enabledForIdleTimeout": true,
"id": "frmPinEntryStep2",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"preShow": AS_Form_e186b2d2bc564610bc8894c777744cd1,
"skin": "slFormCommon"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"retainScrollPosition": false,
"titleBar": false,
"titleBarSkin": "slTitleBar",
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
