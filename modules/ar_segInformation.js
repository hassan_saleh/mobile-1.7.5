//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function initializesegInformationAr() {
    flxInformationAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxInformation",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "flxBgBlueGradientRound8"
    }, {}, {});
    flxInformation.setDefaultUnit(kony.flex.DP);
    var flxBody = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxBody",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0%",
        "skin": "slFbox",
        "top": 0,
        "width": "84%",
        "zIndex": 1
    }, {}, {});
    flxBody.setDefaultUnit(kony.flex.DP);
    var lblHeader = new kony.ui.Label({
        "id": "lblHeader",
        "isVisible": true,
        "right": "6%",
        "maxNumberOfLines": 2,
        "skin": "sknLblWhike125",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "0",
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 2,0, 2],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDescription = new kony.ui.Label({
        "id": "lblDescription",
        "isVisible": true,
        "right": "6%",
        "maxNumberOfLines": 3,
        "skin": "CopysknLblWhike0f7876795165448",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
        "padding": [ 0, 2,0, 2],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBody.add(lblHeader, lblDescription);
    var lblSymbol = new kony.ui.Label({
        "id": "lblSymbol",
        "isVisible": true,
        "right": "84%",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.baclinfo"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "16%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 8,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgNews = new kony.ui.Image2({
        "id": "imgNews",
        "isVisible": false,
        "left": "30dp",
        "skin": "slImage",
        "src": "imagedrag.png",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    flxInformationAr.add(flxBody, lblSymbol, imgNews);
}
