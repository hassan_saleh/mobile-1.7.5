//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmConfirmCashWithDrawAr() {
frmConfirmCashWithDraw.setDefaultUnit(kony.flex.DP);
var titleBarWrapper = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "titleBarWrapper",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgheader",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
titleBarWrapper.setDefaultUnit(kony.flex.DP);
var androidTitleBar = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "androidTitleBar",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgheader",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
androidTitleBar.setDefaultUnit(kony.flex.DP);
var androidTitleLabel = new kony.ui.Label({
"centerY": "50%",
"id": "androidTitleLabel",
"isVisible": true,
"right": "55dp",
"skin": "sknnavBarTitle",
"text": kony.i18n.getLocalizedString("i18n.cashWithdraw.confirmWithdraw"),
"width": "70%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
androidTitleBar.add(androidTitleLabel);
var backButton = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "skntitleBarTextButtonFocus",
"height": "50dp",
"id": "backButton",
"isVisible": true,
"right": "0dp",
"minWidth": "50dp",
"onClick": AS_Button_b22780a80ee9408e802dec5d01c6c93e,
"skin": "skntitleBarTextButton",
"text": kony.i18n.getLocalizedString("i18n.cashWithdraw.cancel"),
"top": "0dp",
"width": "70dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,2, 0],
"paddingInPixel": false
}, {});
titleBarWrapper.add(androidTitleBar, backButton);
var FlexScrollContainerConfirmWithdraw = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": true,
"allowVerticalBounce": true,
"bottom": "0dp",
"bounces": false,
"clipBounds": true,
"enableScrolling": false,
"height": "100%",
"horizontalScrollIndicator": false,
"id": "FlexScrollContainerConfirmWithdraw",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "sknscrollBkg",
"verticalScrollIndicator": false,
"width": "100%",
"zIndex": 1
}, {}, {});
FlexScrollContainerConfirmWithdraw.setDefaultUnit(kony.flex.DP);
var FlexContainerWithdrawDetails = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "115dp",
"id": "FlexContainerWithdrawDetails",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
FlexContainerWithdrawDetails.setDefaultUnit(kony.flex.DP);
var innerFlexContainerWithdrawDetails = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "100%",
"id": "innerFlexContainerWithdrawDetails",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
innerFlexContainerWithdrawDetails.setDefaultUnit(kony.flex.DP);
var withdrawAmount = new kony.ui.Label({
"centerX": "50%",
"id": "withdrawAmount",
"isVisible": true,
"skin": "skndetailPageNumber",
"text": "$ 5006.00",
"top": "10dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var forCollectionLabel = new kony.ui.Label({
"centerX": "50%",
"id": "forCollectionLabel",
"isVisible": true,
"skin": "skndetailPageDate",
"text": kony.i18n.getLocalizedString("i18n.cashWithdraw.cashWithdrawalby"),
"top": "5dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var collectorName = new kony.ui.Label({
"centerX": "50%",
"id": "collectorName",
"isVisible": true,
"skin": "skn30363f110KA",
"text": "Self",
"top": "5dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var divider1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "divider1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"top": "10dp",
"width": "100%",
"zIndex": 1
}, {}, {});
divider1.setDefaultUnit(kony.flex.DP);
divider1.add();
innerFlexContainerWithdrawDetails.add(withdrawAmount, forCollectionLabel, collectorName, divider1);
FlexContainerWithdrawDetails.add(innerFlexContainerWithdrawDetails);
var fromFlxDetails = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "65dp",
"id": "fromFlxDetails",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
fromFlxDetails.setDefaultUnit(kony.flex.DP);
var fromLabel = new kony.ui.Label({
"id": "fromLabel",
"isVisible": true,
"right": "5%",
"skin": "sknsegmentHeaderText",
"text": kony.i18n.getLocalizedString("i18n.cashWithdraw.from"),
"top": "5dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var withdrawFrom = new kony.ui.Label({
"id": "withdrawFrom",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "Savings 2453",
"top": "30dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var divider2 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "divider2",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
divider2.setDefaultUnit(kony.flex.DP);
divider2.add();
fromFlxDetails.add(fromLabel, withdrawFrom, divider2);
var flexNotesDetails = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flexNotesDetails",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flexNotesDetails.setDefaultUnit(kony.flex.DP);
var lblNotes = new kony.ui.Label({
"id": "lblNotes",
"isVisible": true,
"right": "5%",
"skin": "sknsegmentHeaderText",
"text": kony.i18n.getLocalizedString("i18n.cashWithdraw.notes"),
"top": "13dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var transactionNotes = new kony.ui.Label({
"bottom": "15dp",
"id": "transactionNotes",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "Note details appear here. This can be a muliple line descripsion",
"top": "5dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var divider3 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "divider3",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
divider3.setDefaultUnit(kony.flex.DP);
divider3.add();
flexNotesDetails.add(lblNotes, transactionNotes, divider3);
var flxSecureCodeKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0dp",
"clipBounds": true,
"height": "315dp",
"id": "flxSecureCodeKA",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"width": "100%",
"zIndex": 1
}, {}, {});
flxSecureCodeKA.setDefaultUnit(kony.flex.DP);
var flxImportantKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "130dp",
"id": "flxImportantKA",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxImportantKA.setDefaultUnit(kony.flex.DP);
var lblImportant = new kony.ui.Label({
"centerX": "50%",
"id": "lblImportant",
"isVisible": true,
"skin": "sknErrorMessageEC223BKA",
"text": "Important",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "30dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblWarningDescription = new kony.ui.Label({
"id": "lblWarningDescription",
"isVisible": true,
"right": "10%",
"left": "10%",
"skin": "sknNumber",
"text": "Please carefully check the details above as you may not be able to recover an incorrect transaction",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "60dp",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxImportantKA.add(lblImportant, lblWarningDescription);
var flxCreateSecureCodeKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxCreateSecureCodeKA",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "15dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxCreateSecureCodeKA.setDefaultUnit(kony.flex.DP);
var CopylblNotes0b76aa8e8ea9249 = new kony.ui.Label({
"id": "CopylblNotes0b76aa8e8ea9249",
"isVisible": true,
"right": "5%",
"skin": "sknsegmentHeaderText",
"text": kony.i18n.getLocalizedString("i18n.cashWithdraw.createSecureCode"),
"top": "13dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyFlexContainerImportantNotice0ca16e307340a44 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "CopyFlexContainerImportantNotice0ca16e307340a44",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {}, {});
CopyFlexContainerImportantNotice0ca16e307340a44.setDefaultUnit(kony.flex.DP);
var CopylblWarningDescription0be3347b2d02844 = new kony.ui.Label({
"id": "CopylblWarningDescription0be3347b2d02844",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "You need to share this code with ",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblPersonNameKA = new kony.ui.Label({
"id": "lblPersonNameKA",
"isVisible": true,
"right": "1%",
"skin": "sknsegmentHeaderText",
"text": kony.i18n.getLocalizedString("i18n.cashWithdraw.createSecureCode"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyFlexContainerImportantNotice0ca16e307340a44.add( lblPersonNameKA,CopylblWarningDescription0be3347b2d02844);
var CopylblWarningDescription0fe6733c43c4946 = new kony.ui.Label({
"id": "CopylblWarningDescription0fe6733c43c4946",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "for collecting cash at ATM.",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var tbxCreateSecureCodeKA = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"centerX": "50%",
"focusSkin": "skngeneralTextField",
"height": "40dp",
"id": "tbxCreateSecureCodeKA",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "0dp",
"maxTextLength": 4,
"onTextChange": AS_TextField_g8eca4cb6b774d1ea7fc5614e89db38a,
"placeholder": kony.i18n.getLocalizedString("i18n.cashWithdraw.enterSecureCode"),
"secureTextEntry": true,
"skin": "skngeneralTextField",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"top": "10dp",
"width": "90%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,2, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
"placeholderSkin": "sknPlaceholderKA",
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var tbxReEnterSecureCodeKA = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"centerX": "50%",
"focusSkin": "skngeneralTextField",
"height": "40dp",
"id": "tbxReEnterSecureCodeKA",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "0dp",
"maxTextLength": 4,
"placeholder": kony.i18n.getLocalizedString("i18n.cashWithdraw.reenterSecureCode"),
"secureTextEntry": false,
"skin": "skngeneralTextField",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"top": "10dp",
"width": "90%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,2, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
"placeholderSkin": "sknPlaceholderKA",
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var lblInvalidSecureCode = new kony.ui.Label({
"id": "lblInvalidSecureCode",
"isVisible": false,
"right": "5%",
"skin": "sknInvalidCredKA",
"text": kony.i18n.getLocalizedString("i18n.cashWithdraw.Please Enter Secure Code"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "6dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxCreateSecureCodeKA.add(CopylblNotes0b76aa8e8ea9249, CopyFlexContainerImportantNotice0ca16e307340a44, CopylblWarningDescription0fe6733c43c4946, tbxCreateSecureCodeKA, tbxReEnterSecureCodeKA, lblInvalidSecureCode);
var btnConfirm = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "sknprimaryActionFocus",
"height": "42dp",
"id": "btnConfirm",
"isVisible": true,
"onClick": AS_Button_f15bea2e0d9f4148a1c94f596f9b8ae3,
"skin": "sknprimaryAction",
"text": kony.i18n.getLocalizedString("i18n.cashWithdraw.confirm"),
"top": "7%",
"width": "80%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnEditTranscation = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "sknsecondaryActionFocus",
"height": "35dp",
"id": "btnEditTranscation",
"isVisible": true,
"onClick": AS_Button_c7707ef7ea9a439f8440eadf7ba41716,
"skin": "sknsecondaryAction",
"text": kony.i18n.getLocalizedString("i18n.cashWithdraw.edit"),
"top": "10dp",
"width": "260dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxSecureCodeKA.add(flxImportantKA, flxCreateSecureCodeKA, btnConfirm, btnEditTranscation);
FlexScrollContainerConfirmWithdraw.add(FlexContainerWithdrawDetails, fromFlxDetails, flexNotesDetails, flxSecureCodeKA);
frmConfirmCashWithDraw.add(titleBarWrapper, FlexScrollContainerConfirmWithdraw);
};
function frmConfirmCashWithDrawGlobalsAr() {
frmConfirmCashWithDrawAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmConfirmCashWithDrawAr,
"bounces": false,
"enableScrolling": false,
"enabledForIdleTimeout": true,
"id": "frmConfirmCashWithDraw",
"layoutType": kony.flex.FLOW_VERTICAL,
"needAppMenu": false,
"preShow": AS_Form_df0cc29e6caf452bb000a6e32cc7549d,
"skin": "sknmainGradient"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FLOW_VERTICAL,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_f6a55ea9cf194c12a318ab2ac01354dc,
"retainScrollPosition": false,
"titleBar": true,
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
