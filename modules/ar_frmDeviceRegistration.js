//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmDeviceRegistrationAr() {
frmDeviceRegistration.setDefaultUnit(kony.flex.DP);
var flxheader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxheader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "s",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxheader.setDefaultUnit(kony.flex.DP);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_a520c6cbd057488590eeb3e2b0e091ba,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 1
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblBack0i5985fbe3b634e = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"centerY": "50%",
"height": "100%",
"id": "CopylblBack0i5985fbe3b634e",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBack.add(lblBackIcon, CopylblBack0i5985fbe3b634e);
var lblMyRegisteredDevices = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"height": "100%",
"id": "lblMyRegisteredDevices",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.DeviceSettings.myRegisteredDevices"),
"width": "65%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxheader.add(flxBack, lblMyRegisteredDevices);
var mainContent = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bottom": 0,
"bounces": true,
"clipBounds": true,
"enableScrolling": true,
"height": "91%",
"horizontalScrollIndicator": true,
"id": "mainContent",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "CopyslFbox0a80a39d4ea594c",
"top": "0dp",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
mainContent.setDefaultUnit(kony.flex.DP);
var flxLineSeperator = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "flxLineSeperator",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"top": "-1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxLineSeperator.setDefaultUnit(kony.flex.DP);
flxLineSeperator.add();
var flxDevice1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "50dp",
"id": "flxDevice1",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "flxBgBlueGradientRound8",
"top": "5dp",
"width": "88%",
"zIndex": 1
}, {}, {});
flxDevice1.setDefaultUnit(kony.flex.DP);
var lblDeviceTitle1 = new kony.ui.Label({
"centerY": "50%",
"id": "lblDeviceTitle1",
"isVisible": true,
"right": "5%",
"skin": "sknLblWhite128C",
"text": "This Device",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxThisDeviceSwitchOff = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxThisDeviceSwitchOff",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": "10%",
"skin": "sknflxGrey",
"top": "0dp",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxThisDeviceSwitchOff.setDefaultUnit(kony.flex.DP);
var CopyflxRoundDBlueOff0a5e3eddfada24b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "CopyflxRoundDBlueOff0a5e3eddfada24b",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknFlxDarkBlueRoundCornerGrey",
"top": "0dp",
"width": "25dp",
"zIndex": 1
}, {}, {});
CopyflxRoundDBlueOff0a5e3eddfada24b.setDefaultUnit(kony.flex.DP);
CopyflxRoundDBlueOff0a5e3eddfada24b.add();
var CopyflxNaveenbhaiKiLakeer0cb72155c61834c = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "12dp",
"id": "CopyflxNaveenbhaiKiLakeer0cb72155c61834c",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 10,
"skin": "sknLineDarkBlue",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
CopyflxNaveenbhaiKiLakeer0cb72155c61834c.setDefaultUnit(kony.flex.DP);
CopyflxNaveenbhaiKiLakeer0cb72155c61834c.add();
flxThisDeviceSwitchOff.add(CopyflxRoundDBlueOff0a5e3eddfada24b, CopyflxNaveenbhaiKiLakeer0cb72155c61834c);
var flxThisDeviceSwitchOn = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxThisDeviceSwitchOn",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_ic7e4cea21c74b4f86db0d81e3105511,
"left": "10%",
"skin": "sknflxyellow",
"top": "0dp",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxThisDeviceSwitchOn.setDefaultUnit(kony.flex.DP);
var CopyflxRoundDBlue0bbc73e3c0fb041 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "CopyflxRoundDBlue0bbc73e3c0fb041",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "sknFlxDarkBlueRoundCorner",
"top": "0dp",
"width": "25dp",
"zIndex": 1
}, {}, {});
CopyflxRoundDBlue0bbc73e3c0fb041.setDefaultUnit(kony.flex.DP);
CopyflxRoundDBlue0bbc73e3c0fb041.add();
var Copyflxlakeer0h4a3ac20cf6948 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "12dp",
"id": "Copyflxlakeer0h4a3ac20cf6948",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10dp",
"skin": "sknLineDarkBlue",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
Copyflxlakeer0h4a3ac20cf6948.setDefaultUnit(kony.flex.DP);
Copyflxlakeer0h4a3ac20cf6948.add();
flxThisDeviceSwitchOn.add(CopyflxRoundDBlue0bbc73e3c0fb041, Copyflxlakeer0h4a3ac20cf6948);
flxDevice1.add(lblDeviceTitle1, flxThisDeviceSwitchOff, flxThisDeviceSwitchOn);
var flxDevice2 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "50dp",
"id": "flxDevice2",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "flxBgBlueGradientRound8",
"top": "5dp",
"width": "88%",
"zIndex": 1
}, {}, {});
flxDevice2.setDefaultUnit(kony.flex.DP);
var lblDevice2 = new kony.ui.Label({
"centerY": "50%",
"id": "lblDevice2",
"isVisible": true,
"right": "5%",
"skin": "sknLblWhite128C",
"text": "Samsung Galaxy s6 Edge +",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "35dp",
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxDevice2SwitchOff = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxDevice2SwitchOff",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": "10%",
"skin": "sknflxGrey",
"top": "0dp",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxDevice2SwitchOff.setDefaultUnit(kony.flex.DP);
var CopyflxRoundDBlueOff0ce5ef83d127f4a = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "CopyflxRoundDBlueOff0ce5ef83d127f4a",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknFlxDarkBlueRoundCornerGrey",
"top": "0dp",
"width": "25dp",
"zIndex": 1
}, {}, {});
CopyflxRoundDBlueOff0ce5ef83d127f4a.setDefaultUnit(kony.flex.DP);
CopyflxRoundDBlueOff0ce5ef83d127f4a.add();
var CopyflxNaveenbhaiKiLakeer0bfdf563d8ec946 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "12dp",
"id": "CopyflxNaveenbhaiKiLakeer0bfdf563d8ec946",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 10,
"skin": "sknLineDarkBlue",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
CopyflxNaveenbhaiKiLakeer0bfdf563d8ec946.setDefaultUnit(kony.flex.DP);
CopyflxNaveenbhaiKiLakeer0bfdf563d8ec946.add();
flxDevice2SwitchOff.add(CopyflxRoundDBlueOff0ce5ef83d127f4a, CopyflxNaveenbhaiKiLakeer0bfdf563d8ec946);
var flxDevice2SwitchOn = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxDevice2SwitchOn",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_a9918f62b18f4ac8aa19f8df4762ae50,
"left": "10%",
"skin": "sknflxyellow",
"top": "0dp",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxDevice2SwitchOn.setDefaultUnit(kony.flex.DP);
var CopyflxRoundDBlue0d4529702fe1f45 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "CopyflxRoundDBlue0d4529702fe1f45",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "sknFlxDarkBlueRoundCorner",
"top": "0dp",
"width": "25dp",
"zIndex": 1
}, {}, {});
CopyflxRoundDBlue0d4529702fe1f45.setDefaultUnit(kony.flex.DP);
CopyflxRoundDBlue0d4529702fe1f45.add();
var Copyflxlakeer0b6d72011650145 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "12dp",
"id": "Copyflxlakeer0b6d72011650145",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10dp",
"skin": "sknLineDarkBlue",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
Copyflxlakeer0b6d72011650145.setDefaultUnit(kony.flex.DP);
Copyflxlakeer0b6d72011650145.add();
flxDevice2SwitchOn.add(CopyflxRoundDBlue0d4529702fe1f45, Copyflxlakeer0b6d72011650145);
flxDevice2.add(lblDevice2, flxDevice2SwitchOff, flxDevice2SwitchOn);
var flxDevice3 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "50dp",
"id": "flxDevice3",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "flxBgBlueGradientRound8",
"top": "5dp",
"width": "88%",
"zIndex": 1
}, {}, {});
flxDevice3.setDefaultUnit(kony.flex.DP);
var lblDevice3 = new kony.ui.Label({
"centerY": "50%",
"id": "lblDevice3",
"isVisible": true,
"right": "5%",
"skin": "sknLblWhite128C",
"text": "Apple iphone X",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "35dp",
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxDevice3SwitchOff = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxDevice3SwitchOff",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": "10%",
"skin": "sknflxGrey",
"top": "0dp",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxDevice3SwitchOff.setDefaultUnit(kony.flex.DP);
var CopyflxRoundDBlueOff0j17f2e6856d248 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "CopyflxRoundDBlueOff0j17f2e6856d248",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknFlxDarkBlueRoundCornerGrey",
"top": "0dp",
"width": "25dp",
"zIndex": 1
}, {}, {});
CopyflxRoundDBlueOff0j17f2e6856d248.setDefaultUnit(kony.flex.DP);
CopyflxRoundDBlueOff0j17f2e6856d248.add();
var CopyflxNaveenbhaiKiLakeer0e67cced9555148 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "12dp",
"id": "CopyflxNaveenbhaiKiLakeer0e67cced9555148",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 10,
"skin": "sknLineDarkBlue",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
CopyflxNaveenbhaiKiLakeer0e67cced9555148.setDefaultUnit(kony.flex.DP);
CopyflxNaveenbhaiKiLakeer0e67cced9555148.add();
flxDevice3SwitchOff.add(CopyflxRoundDBlueOff0j17f2e6856d248, CopyflxNaveenbhaiKiLakeer0e67cced9555148);
var flxDevice3SwitchOn = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxDevice3SwitchOn",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_d73bae8950a54661890c4e6d2bca55a5,
"left": "10%",
"skin": "sknflxyellow",
"top": "0dp",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxDevice3SwitchOn.setDefaultUnit(kony.flex.DP);
var CopyflxRoundDBlue0bc52785d9bc743 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "CopyflxRoundDBlue0bc52785d9bc743",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "sknFlxDarkBlueRoundCorner",
"top": "0dp",
"width": "25dp",
"zIndex": 1
}, {}, {});
CopyflxRoundDBlue0bc52785d9bc743.setDefaultUnit(kony.flex.DP);
CopyflxRoundDBlue0bc52785d9bc743.add();
var Copyflxlakeer0e50ca63e7dd044 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "12dp",
"id": "Copyflxlakeer0e50ca63e7dd044",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10dp",
"skin": "sknLineDarkBlue",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
Copyflxlakeer0e50ca63e7dd044.setDefaultUnit(kony.flex.DP);
Copyflxlakeer0e50ca63e7dd044.add();
flxDevice3SwitchOn.add(CopyflxRoundDBlue0bc52785d9bc743, Copyflxlakeer0e50ca63e7dd044);
flxDevice3.add(lblDevice3, flxDevice3SwitchOff, flxDevice3SwitchOn);
var segRegisteredDevices = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"centerX": "50%",
"data": [{
"lblDeviceTitle": "Samsung Galaxy S6"
}, {
"lblDeviceTitle": "OnePlus 3t"
}, {
"lblDeviceTitle": "iPhone 6s"
}],
"groupCells": false,
"id": "segRegisteredDevices",
"isVisible": true,
"right": "0dp",
"needPageIndicator": true,
"onRowClick": AS_Segment_af4054e07e9149c780af5df1c11d9b23,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "seg2Focus",
"rowSkin": "segDBlue",
"rowTemplate": flxDeviceDetailsEnabled,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "052b4900",
"separatorRequired": true,
"separatorThickness": 5,
"showScrollbars": false,
"top": "5%",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"CopyflxNaveenbhaiKiLakeer0cb72155c61834c": "CopyflxNaveenbhaiKiLakeer0cb72155c61834c",
"CopyflxRoundDBlue0bbc73e3c0fb041": "CopyflxRoundDBlue0bbc73e3c0fb041",
"CopyflxRoundDBlueOff0a5e3eddfada24b": "CopyflxRoundDBlueOff0a5e3eddfada24b",
"Copyflxlakeer0h4a3ac20cf6948": "Copyflxlakeer0h4a3ac20cf6948",
"flxDeviceDetailsEnabled": "flxDeviceDetailsEnabled",
"flxThisDeviceSwitchOff": "flxThisDeviceSwitchOff",
"flxThisDeviceSwitchOn": "flxThisDeviceSwitchOn",
"lblDeviceTitle": "lblDeviceTitle"
},
"width": "92%",
"zIndex": 1
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var lblLanguage = new kony.ui.Label({
"id": "lblLanguage",
"isVisible": false,
"right": "79dp",
"skin": "slLabel",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "93dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblDeviceInfo = new kony.ui.Label({
"id": "lblDeviceInfo",
"isVisible": false,
"right": "79dp",
"skin": "slLabel",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "93dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCustId = new kony.ui.Label({
"id": "lblCustId",
"isVisible": false,
"right": "79dp",
"skin": "slLabel",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "93dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblDeviceId = new kony.ui.Label({
"id": "lblDeviceId",
"isVisible": false,
"right": "79dp",
"skin": "slLabel",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "93dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblRegisterFlag = new kony.ui.Label({
"id": "lblRegisterFlag",
"isVisible": false,
"right": "79dp",
"skin": "slLabel",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "93dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblKSID = new kony.ui.Label({
"id": "lblKSID",
"isVisible": false,
"right": "100%",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "100%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblAppID = new kony.ui.Label({
"id": "lblAppID",
"isVisible": false,
"right": "100%",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "100%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblUserId = new kony.ui.Label({
"id": "lblUserId",
"isVisible": false,
"right": "100%",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "100%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
mainContent.add(flxLineSeperator, flxDevice1, flxDevice2, flxDevice3, segRegisteredDevices, lblLanguage, lblDeviceInfo, lblCustId, lblDeviceId, lblRegisterFlag, lblKSID, lblAppID, lblUserId);
frmDeviceRegistration.add(flxheader, mainContent);
};
function frmDeviceRegistrationGlobalsAr() {
frmDeviceRegistrationAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmDeviceRegistrationAr,
"enabledForIdleTimeout": true,
"id": "frmDeviceRegistration",
"layoutType": kony.flex.FLOW_VERTICAL,
"needAppMenu": false,
"skin": "sknSuccessBkg"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FLOW_VERTICAL,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_hfc6f74bddbe428280026aa9932ac3c5,
"retainScrollPosition": false,
"titleBar": true,
"titleBarSkin": "slTitleBar",
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
