//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function initializesegmanagepayeetmpltAr() {
    flxManagePayeeNormalAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "80dp",
        "id": "flxManagePayeeNormal",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknCopyslFbox07d05709853a74d"
    }, {}, {});
    flxManagePayeeNormal.setDefaultUnit(kony.flex.DP);
    var flxToAnimate = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxToAnimate",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "onClick": AS_FlexContainer_c64d866ecd01409a8745624b1c18e24b,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxToAnimate.setDefaultUnit(kony.flex.DP);
    var flxAnimate = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxAnimate",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "slFbox",
        "top": "0%",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    flxAnimate.setDefaultUnit(kony.flex.DP);
    var lblBillerType = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBillerType",
        "isVisible": false,
        "right": "80%",
        "skin": "lblsegtextsmall",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "18%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "23%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    flxDetails.setDefaultUnit(kony.flex.DP);
    var payeename = new kony.ui.Label({
        "centerY": "35%",
        "height": "32%",
        "id": "payeename",
        "isVisible": true,
        "right": "2%",
        "skin": "sknLblNextDisabled",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var payeenickname = new kony.ui.Label({
        "centerY": "56.35%",
        "height": "20dp",
        "id": "payeenickname",
        "isVisible": false,
        "right": "5.00%",
        "skin": "sknRegisterMobileBank",
        "top": 50,
        "width": "65%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var accountnumber = new kony.ui.Label({
        "centerY": "65%",
        "height": "30%",
        "id": "accountnumber",
        "isVisible": true,
        "right": "2%",
        "skin": "sknLblAccNumBiller",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var dueAmount = new kony.ui.Label({
        "centerY": "75%",
        "height": "30%",
        "id": "dueAmount",
        "isVisible": false,
        "right": "2%",
        "skin": "sknLblAccNumBiller",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxDetails.add(payeename, payeenickname, accountnumber, dueAmount);
    var flxIconContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "50dp",
        "id": "flxIconContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "5%",
        "skin": "slFbox",
        "width": "50dp",
        "zIndex": 1
    }, {}, {});
    flxIconContainer.setDefaultUnit(kony.flex.DP);
    var flxIcon1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "100%",
        "id": "flxIcon1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknFlxToIcon",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxIcon1.setDefaultUnit(kony.flex.DP);
    var lblInitial = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "100%",
        "id": "lblInitial",
        "isVisible": true,
        "skin": "sknLblFromIcon",
        "text": "GG",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblTick = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "100%",
        "id": "lblTick",
        "isVisible": true,
        "skin": "sknBOJttfwhitee150",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxIcon1.add(lblInitial, lblTick);
    flxIconContainer.add(flxIcon1);
    flxAnimate.add(lblBillerType, flxDetails, flxIconContainer);
    var flxButtonHolder = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxButtonHolder",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "86%",
        "skin": "slFbox",
        "top": "0%",
        "width": "13%",
        "zIndex": 1
    }, {}, {});
    flxButtonHolder.setDefaultUnit(kony.flex.DP);
    var btnDelete = new kony.ui.Button({
        "focusSkin": "CopysknSegmentDelete0h225a870b5144b",
        "height": "100%",
        "id": "btnDelete",
        "isVisible": true,
        "right": "0%",
        "onClick": AS_Button_ec163423670d46408b62777c1c108944,
        "skin": "CopysknSegmentDelete0h225a870b5144b",
        "text": "w",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    flxButtonHolder.add(btnDelete);
    var contactListDivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1%",
        "id": "contactListDivider",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "sknsegmentDivider",
        "top": "99%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    contactListDivider.setDefaultUnit(kony.flex.DP);
    contactListDivider.add();
    var btnEditBillerDetails = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "CopysknSegmentDelete0h225a870b5144b",
        "height": "100%",
        "id": "btnEditBillerDetails",
        "isVisible": true,
        "right": "71%",
        "onClick": AS_Button_e37b93afc1844a1b9ef6ae41cab8423c,
        "skin": "CopysknSegmentDelete0h225a870b5144b",
        "text": "0",
        "width": "13%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    var verticalDivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "40%",
        "id": "verticalDivider",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "85%",
        "skin": "sknsegmentDivider",
        "width": "1dp",
        "zIndex": 1
    }, {}, {});
    verticalDivider.setDefaultUnit(kony.flex.DP);
    verticalDivider.add();
    var btnBillsPayAccounts = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "CopysknSegmentDelete0h225a870b5144b",
        "height": "60%",
        "id": "btnBillsPayAccounts",
        "isVisible": true,
        "right": "4%",
        "onClick": AS_Button_c6eb27e074334d02bed90f4d7e2d0732,
        "skin": "CopysknSegmentDelete0h225a870b5144b",
        "width": "14%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    var lblBulkSelection = new kony.ui.Label({
        "height": "25%",
        "id": "lblBulkSelection",
        "isVisible": false,
        "right": "13%",
        "skin": "sknBulkPaymentSelection",
        "text": "r",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "14%",
        "width": "5%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxToAnimate.add(flxAnimate, flxButtonHolder, contactListDivider, btnEditBillerDetails, verticalDivider, btnBillsPayAccounts, lblBulkSelection);
    flxManagePayeeNormalAr.add(flxToAnimate);
}
