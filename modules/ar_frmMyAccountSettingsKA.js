//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmMyAccountSettingsKAAr() {
frmMyAccountSettingsKA.setDefaultUnit(kony.flex.DP);
var flxMain = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxMain",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknflxTransprnt",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxMain.setDefaultUnit(kony.flex.DP);
var flxSettingsHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxSettingsHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "CopyslFbox0f07559cdcd2e4e",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxSettingsHeader.setDefaultUnit(kony.flex.DP);
var flxTitle = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"centerY": "50%",
"clipBounds": true,
"height": "100%",
"id": "flxTitle",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"skin": "sknflxTransprnt",
"width": "100%",
"zIndex": 2
}, {}, {});
flxTitle.setDefaultUnit(kony.flex.DP);
var lblSettingsTitle = new kony.ui.Label({
"centerX": "50%",
"centerY": "45%",
"id": "lblSettingsTitle",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.myaccountsettings"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblAccNumber = new kony.ui.Label({
"centerY": "50%",
"id": "lblAccNumber",
"isVisible": false,
"skin": "lblAmountCurrency",
"text": " ***777",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxTitle.add(lblSettingsTitle, lblAccNumber);
var btnBack = new kony.ui.Button({
"focusSkin": "CopyslButtonGlossBlue0e73a02c4810645",
"height": "90%",
"id": "btnBack",
"isVisible": true,
"left": "0%",
"onClick": AS_Button_e36a8f2af41e4d5f9a345d1b4399891e,
"skin": "CopyslButtonGlossBlue0e73a02c4810645",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"top": "0.00%",
"width": "20%",
"zIndex": 4
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"displayText": true,
"padding": [2, 0, 0, 0],
"paddingInPixel": false
}, {});
var lblBack = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"centerY": "45%",
"id": "lblBack",
"isVisible": true,
"left": "8%",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 3
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxSettingsHeader.add(flxTitle, btnBack, lblBack);
var FlexScrollContainer0aaefdaacee1040 = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": true,
"clipBounds": true,
"enableScrolling": true,
"height": "91%",
"horizontalScrollIndicator": true,
"id": "FlexScrollContainer0aaefdaacee1040",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "sknflxScrollBlue",
"top": "9%",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
FlexScrollContainer0aaefdaacee1040.setDefaultUnit(kony.flex.DP);
var lblAddNickNameDesc = new kony.ui.Label({
"id": "lblAddNickNameDesc",
"isVisible": true,
"right": "5%",
"skin": "sknlblBodytxt",
"text": kony.i18n.getLocalizedString("i18n.accountSetting.chooseAccountsSettings"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "3%",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxLine = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "flxLine",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknflxLinewhiteOp",
"top": "3%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxLine.setDefaultUnit(kony.flex.DP);
flxLine.add();
var segChooseAccounts = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"data": [{
"Symbol": "o",
"lblACCHideShow": "Salary Account",
"lblAccName": "Salary Account",
"lblAccNickname": "Salary Account",
"lblAccountName": "Salary Account",
"lblAccountNumber": "***777",
"lblAmount": "2,7453 JOD",
"lblDefaultAccPayment": "Balance",
"lblDefaultAccTransfer": "Salary Account"
}, {
"Symbol": "o",
"lblACCHideShow": "Salary Account",
"lblAccName": "Salary Account",
"lblAccNickname": "Salary Account",
"lblAccountName": "Savings Account",
"lblAccountNumber": "***145",
"lblAmount": "2,7453 JOD",
"lblDefaultAccPayment": "Balance",
"lblDefaultAccTransfer": "Salary Account"
}, {
"Symbol": "o",
"lblACCHideShow": "Salary Account",
"lblAccName": "Salary Account",
"lblAccNickname": "Salary Account",
"lblAccountName": "Current Account",
"lblAccountNumber": "***0014",
"lblAmount": "2,7453 JOD",
"lblDefaultAccPayment": "Balance",
"lblDefaultAccTransfer": "Salary Account"
}],
"groupCells": false,
"id": "segChooseAccounts",
"isVisible": true,
"right": "0%",
"needPageIndicator": true,
"onRowClick": AS_Segment_bf63ae0736d5495a8572d59e7bb49193,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "sknsegAcc",
"rowSkin": "sknsegAcc",
"rowTemplate": flxSegChooseAccounts,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "ffffff50",
"separatorRequired": true,
"separatorThickness": 2,
"showScrollbars": false,
"top": "0%",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"Symbol": "Symbol",
"flxSegChooseAccounts": "flxSegChooseAccounts",
"lblACCHideShow": "lblACCHideShow",
"lblAccName": "lblAccName",
"lblAccNickname": "lblAccNickname",
"lblAccountName": "lblAccountName",
"lblAccountNumber": "lblAccountNumber",
"lblAmount": "lblAmount",
"lblDefaultAccPayment": "lblDefaultAccPayment",
"lblDefaultAccTransfer": "lblDefaultAccTransfer"
},
"width": "100%",
"zIndex": 1
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var LabelNoRecordsKA = new kony.ui.Label({
"id": "LabelNoRecordsKA",
"isVisible": false,
"right": "16%",
"skin": "skn383838LatoRegular107KA",
"text": kony.i18n.getLocalizedString("i18n.alerts.NoAccountsMsg"),
"top": "20dp",
"width": "65%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
FlexScrollContainer0aaefdaacee1040.add(lblAddNickNameDesc, flxLine, segChooseAccounts, LabelNoRecordsKA);
flxMain.add(flxSettingsHeader, FlexScrollContainer0aaefdaacee1040);
frmMyAccountSettingsKA.add(flxMain);
};
function frmMyAccountSettingsKAGlobalsAr() {
frmMyAccountSettingsKAAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmMyAccountSettingsKAAr,
"enabledForIdleTimeout": true,
"id": "frmMyAccountSettingsKA",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"preShow": AS_Form_b06f99cc19d64d84844d34bb8ac13dad,
"skin": "slFormCommon"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_ecbd0818e06c45e0a311713a226feb05,
"retainScrollPosition": false,
"titleBar": false,
"titleBarSkin": "slTitleBar",
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
