//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmCardStatementKAAr() {
frmCardStatementKA.setDefaultUnit(kony.flex.DP);
var titleBarAccountDetails = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "9%",
"id": "titleBarAccountDetails",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "s",
"width": "100%",
"zIndex": 1
}, {}, {});
titleBarAccountDetails.setDefaultUnit(kony.flex.DP);
var accountDetailsTitleBar = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "95%",
"id": "accountDetailsTitleBar",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "100%",
"zIndex": 2
}, {}, {});
accountDetailsTitleBar.setDefaultUnit(kony.flex.DP);
var lblheader = new kony.ui.Label({
"centerX": "50%",
"height": "90%",
"id": "lblheader",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.common.cardstatement"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_a755307036184755ac5fdb7f4b75b5df,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 20
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var CopylblBackIcon0cbedcd7fb2894e = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"centerY": "50%",
"id": "CopylblBackIcon0cbedcd7fb2894e",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBack = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"centerY": "50%",
"id": "lblBack",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBack.add(CopylblBackIcon0cbedcd7fb2894e, lblBack);
var flxFilterDone = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "90%",
"centerY": "50%",
"clipBounds": true,
"height": "80%",
"id": "flxFilterDone",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_e8d74629892c4db0a9ebf6f3e0255202,
"skin": "slFbox",
"width": "20%",
"zIndex": 1
}, {}, {});
flxFilterDone.setDefaultUnit(kony.flex.DP);
var Label0d6e6be1b179e44 = new kony.ui.Label({
"centerX": "49%",
"centerY": "50%",
"id": "Label0d6e6be1b179e44",
"isVisible": true,
"skin": "sknCarioSemiBold130GreenYellow",
"text": kony.i18n.getLocalizedString("i18n.overview.buttonDone"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [2, 1, 2, 1],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxFilterDone.add(Label0d6e6be1b179e44);
accountDetailsTitleBar.add(lblheader, flxBack, flxFilterDone);
titleBarAccountDetails.add(accountDetailsTitleBar);
var flxCardStatement = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": false,
"clipBounds": true,
"enableScrolling": false,
"height": "92%",
"horizontalScrollIndicator": true,
"id": "flxCardStatement",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "sknflxScrollBlue",
"top": "8%",
"verticalScrollIndicator": false,
"width": "100.00%",
"zIndex": 1
}, {}, {});
flxCardStatement.setDefaultUnit(kony.flex.DP);
var flxSegmentWrapper = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "100%",
"horizontalScrollIndicator": true,
"id": "flxSegmentWrapper",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "1dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "sknGradientbluetodark",
"top": "0%",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 2
}, {}, {});
flxSegmentWrapper.setDefaultUnit(kony.flex.DP);
var flxExtraIcons = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxExtraIcons",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 3
}, {}, {});
flxExtraIcons.setDefaultUnit(kony.flex.DP);
var lblSearch = new kony.ui.Label({
"centerY": "50%",
"id": "lblSearch",
"isVisible": true,
"right": "4%",
"skin": "CopyslLabel0j2197a197ee14d",
"text": "h",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "8dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var txtSearch = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"centerY": "50%",
"focusSkin": "sknCarioRegular125OPc50",
"height": "30dp",
"id": "txtSearch",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "8%",
"maxTextLength": 30,
"onDone": AS_TextField_efe4a90135394ed4b1d553c751818761,
"placeholder": kony.i18n.getLocalizedString("i18n.search.SearchTransactions"),
"secureTextEntry": false,
"skin": "sknCarioRegular125OPc50",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "13dp",
"width": "75%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"placeholderSkin": "sknPlaceHolder",
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var lblExport = new kony.ui.Label({
"height": "100%",
"id": "lblExport",
"isVisible": false,
"onTouchEnd": AS_Label_a7d225c2afcb450ab75711b0dd7214bd,
"left": "15%",
"skin": "CopyslLabel0i2b4c385eb5140",
"text": "l",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": "8%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblFilter0f7770a92cff247 = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "CopylblFilter0f7770a92cff247",
"isVisible": true,
"onTouchEnd": AS_Label_g6532a9992e946899565f1ca6712bc9f,
"left": "5%",
"skin": "CopyslLabel0b8f2bdd739144d",
"text": "T",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "8%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxLine = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "3%",
"id": "flxLine",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "4%",
"skin": "CopyslFbox0ab42d5f22ae345",
"top": "82%",
"width": "78%",
"zIndex": 2
}, {}, {});
flxLine.setDefaultUnit(kony.flex.DP);
flxLine.add();
flxExtraIcons.add(lblSearch, txtSearch, lblExport, CopylblFilter0f7770a92cff247, flxLine);
var CopyflxLine0j05a26a65ef04c = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopyflxLine0j05a26a65ef04c",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknFlxGreyLine",
"top": "0%",
"width": "100%",
"zIndex": 2
}, {}, {});
CopyflxLine0j05a26a65ef04c.setDefaultUnit(kony.flex.DP);
CopyflxLine0j05a26a65ef04c.add();
var accountDetailsTransactions = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "75%",
"id": "accountDetailsTransactions",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0%",
"width": "100%"
}, {}, {});
accountDetailsTransactions.setDefaultUnit(kony.flex.DP);
var transactionSegment = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"bottom": "5dp",
"data": [{
"lblCardStatementmerchant": "Payment to City of Austin",
"lblLastTransaction": "",
"transactionAmount": "123.123 JOD",
"transactionDate": "Oct 10, 2015 - Recurring Monthly",
"transactionName": "Payment to City of Austin"
}, {
"lblCardStatementmerchant": "Payment to City of Austin",
"lblLastTransaction": "",
"transactionAmount": "123.123 JOD",
"transactionDate": "Oct 10, 2015 - Recurring Monthly",
"transactionName": "Payment to City of Austin"
}, {
"lblCardStatementmerchant": "Payment to City of Austin",
"lblLastTransaction": "",
"transactionAmount": "123.123 JOD",
"transactionDate": "Oct 10, 2015 - Recurring Monthly",
"transactionName": "Payment to City of Austin"
}],
"groupCells": false,
"id": "transactionSegment",
"isVisible": true,
"right": "0%",
"needPageIndicator": true,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "sknsegAcc",
"rowSkin": "sknsegAcc",
"rowTemplate": CopyFlexContainer0a99898df0f2a41,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "ffffff4b",
"separatorRequired": true,
"separatorThickness": 1,
"showScrollbars": false,
"top": "0dp",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"CopyFlexContainer0a99898df0f2a41": "CopyFlexContainer0a99898df0f2a41",
"lblCardStatementmerchant": "lblCardStatementmerchant",
"lblLastTransaction": "lblLastTransaction",
"transactionAmount": "transactionAmount",
"transactionAmountJOD": "transactionAmountJOD",
"transactionDate": "transactionDate",
"transactionName": "transactionName"
},
"width": "100%"
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"dockSectionHeaders": true
});
var LabelNoRecordsKA = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "LabelNoRecordsKA",
"isVisible": false,
"right": "0dp",
"skin": "skn383838LatoRegular107KA",
"text": kony.i18n.getLocalizedString("i18n.cards.stmntnotrans"),
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
accountDetailsTransactions.add(transactionSegment, LabelNoRecordsKA);
flxSegmentWrapper.add(flxExtraIcons, CopyflxLine0j05a26a65ef04c, accountDetailsTransactions);
var flxAccountdetailsSortContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"focusSkin": "sknflxTransprnt",
"height": "84%",
"id": "flxAccountdetailsSortContainer",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknflxTransprnt",
"top": "100%",
"width": "100%",
"zIndex": 3
}, {}, {});
flxAccountdetailsSortContainer.setDefaultUnit(kony.flex.DP);
var flxAccountDetailsSort = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "30%",
"id": "flxAccountDetailsSort",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknWhiteBGroundedBorder",
"top": "70%",
"width": "100%",
"zIndex": 3
}, {}, {});
flxAccountDetailsSort.setDefaultUnit(kony.flex.DP);
var lblSortby = new kony.ui.Label({
"centerX": "50%",
"centerY": "10%",
"id": "lblSortby",
"isVisible": true,
"skin": "sknCairoBold120",
"text": kony.i18n.getLocalizedString("i18n.common.sortby"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxLine1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"centerY": "22%",
"clipBounds": true,
"height": "1%",
"id": "flxLine1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknGreyLine50Opc",
"top": 42,
"width": "80%",
"zIndex": 1
}, {}, {});
flxLine1.setDefaultUnit(kony.flex.DP);
flxLine1.add();
var lblSortDate = new kony.ui.Label({
"centerX": "40%",
"centerY": "40.00%",
"id": "lblSortDate",
"isVisible": true,
"skin": "sknCarioSemiBold130perDRKBluefont80Opc",
"text": kony.i18n.getLocalizedString("i18n.accounts.date"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxSortDateAsc = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "60%",
"centerY": "40%",
"clipBounds": true,
"height": "17%",
"id": "flxSortDateAsc",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_hc97617fc9e24c2eb00884546e9b6557,
"skin": "slFbox",
"width": "6%",
"zIndex": 1
}, {}, {});
flxSortDateAsc.setDefaultUnit(kony.flex.DP);
var imgSortDateAsc = new kony.ui.Image2({
"centerX": "50%",
"centerY": "50%",
"id": "imgSortDateAsc",
"isVisible": true,
"skin": "slImage",
"src": "map_arrow_straight_nonselected.png",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxSortDateAsc.add(imgSortDateAsc);
var flxSortDateDesc = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "68%",
"centerY": "40%",
"clipBounds": true,
"height": "17%",
"id": "flxSortDateDesc",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_g14e13003aad489b833b5d519a1cfd18,
"skin": "slFbox",
"width": "6%",
"zIndex": 1
}, {}, {});
flxSortDateDesc.setDefaultUnit(kony.flex.DP);
var imgSortDateDesc = new kony.ui.Image2({
"centerX": "50%",
"centerY": "50%",
"id": "imgSortDateDesc",
"isVisible": true,
"skin": "slImage",
"src": "map_arrow_down_nonselected.png",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxSortDateDesc.add(imgSortDateDesc);
var lblSortAmount = new kony.ui.Label({
"centerX": "40%",
"centerY": "65%",
"id": "lblSortAmount",
"isVisible": true,
"skin": "sknCarioSemiBold130perDRKBluefont80Opc",
"text": kony.i18n.getLocalizedString("i18n.accounts.amount"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxSortAmountAsc = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "60%",
"centerY": "65%",
"clipBounds": true,
"height": "17%",
"id": "flxSortAmountAsc",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_hfa77a6085aa4aedbe77048b08778d47,
"skin": "slFbox",
"width": "6%",
"zIndex": 1
}, {}, {});
flxSortAmountAsc.setDefaultUnit(kony.flex.DP);
var imgSortAmountAsc = new kony.ui.Image2({
"centerX": "50%",
"centerY": "50%",
"id": "imgSortAmountAsc",
"isVisible": true,
"skin": "slImage",
"src": "map_arrow_straight_nonselected.png",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxSortAmountAsc.add(imgSortAmountAsc);
var flxSortAmountDesc = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "68%",
"centerY": "65%",
"clipBounds": true,
"height": "17%",
"id": "flxSortAmountDesc",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_af5e298049d44b4eae188faf524bde68,
"skin": "slFbox",
"width": "6%",
"zIndex": 1
}, {}, {});
flxSortAmountDesc.setDefaultUnit(kony.flex.DP);
var imgSortAmountDesc = new kony.ui.Image2({
"centerX": "50%",
"centerY": "50%",
"id": "imgSortAmountDesc",
"isVisible": true,
"skin": "slImage",
"src": "map_arrow_down_nonselected.png",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxSortAmountDesc.add(imgSortAmountDesc);
flxAccountDetailsSort.add(lblSortby, flxLine1, lblSortDate, flxSortDateAsc, flxSortDateDesc, lblSortAmount, flxSortAmountAsc, flxSortAmountDesc);
flxAccountdetailsSortContainer.add(flxAccountDetailsSort);
flxCardStatement.add(flxSegmentWrapper, flxAccountdetailsSortContainer);
var flxCardsFilterScreen = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxCardsFilterScreen",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "10%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxCardsFilterScreen.setDefaultUnit(kony.flex.DP);
var Label0ad957630909846 = new kony.ui.Label({
"id": "Label0ad957630909846",
"isVisible": true,
"right": "8%",
"skin": "sknCarioRegular120White70Opc",
"text": kony.i18n.getLocalizedString("i18n.common.month"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var FlexContainer0d8cbd8f33ae045 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "6%",
"id": "FlexContainer0d8cbd8f33ae045",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10%",
"onClick": AS_FlexContainer_d93c3ffcdce44c8b89cc8835e4317d58,
"skin": "slFbox",
"top": "10%",
"width": "80%",
"zIndex": 1
}, {}, {});
FlexContainer0d8cbd8f33ae045.setDefaultUnit(kony.flex.DP);
var CopyFlexContainer0h1c28a2cf36142 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0%",
"clipBounds": true,
"height": "5%",
"id": "CopyFlexContainer0h1c28a2cf36142",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "skntextFieldDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0h1c28a2cf36142.setDefaultUnit(kony.flex.DP);
CopyFlexContainer0h1c28a2cf36142.add();
var lblCardDate = new kony.ui.Label({
"centerY": "50%",
"id": "lblCardDate",
"isVisible": true,
"right": "5%",
"skin": "sknCarioRegular120White70Opc",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblProfileRightIcon = new kony.ui.Label({
"centerY": "50%",
"id": "lblProfileRightIcon",
"isVisible": true,
"left": "5%",
"skin": "sknLblBoj145",
"text": "d",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
FlexContainer0d8cbd8f33ae045.add(CopyFlexContainer0h1c28a2cf36142, lblCardDate, lblProfileRightIcon);
var CopyFlexContainer0a6645c15cd5447 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "6%",
"id": "CopyFlexContainer0a6645c15cd5447",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10%",
"onClick": AS_FlexContainer_e26e3a8dbcdb4c7e92220d55e21b6c76,
"skin": "slFbox",
"top": "25%",
"width": "80%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0a6645c15cd5447.setDefaultUnit(kony.flex.DP);
var CopyFlexContainer0a1c986471e234b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0%",
"clipBounds": true,
"height": "5%",
"id": "CopyFlexContainer0a1c986471e234b",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "skntextFieldDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0a1c986471e234b.setDefaultUnit(kony.flex.DP);
CopyFlexContainer0a1c986471e234b.add();
var lblCardYear = new kony.ui.Label({
"centerY": "50%",
"id": "lblCardYear",
"isVisible": true,
"right": "5%",
"skin": "sknCarioRegular120White70Opc",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblProfileRightIcon0h068901a6f8143 = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblProfileRightIcon0h068901a6f8143",
"isVisible": true,
"left": "5%",
"skin": "sknLblBoj145",
"text": "d",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyFlexContainer0a6645c15cd5447.add(CopyFlexContainer0a1c986471e234b, lblCardYear, CopylblProfileRightIcon0h068901a6f8143);
var CopyLabel0e2acff81a7cc46 = new kony.ui.Label({
"id": "CopyLabel0e2acff81a7cc46",
"isVisible": true,
"right": "8%",
"skin": "sknCarioRegular120White70Opc",
"text": kony.i18n.getLocalizedString("i18n.filtertransaction.year"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "20%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxCardsFilterScreen.add(Label0ad957630909846, FlexContainer0d8cbd8f33ae045, CopyFlexContainer0a6645c15cd5447, CopyLabel0e2acff81a7cc46);
var flxCardsSegmentOption = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "91%",
"id": "flxCardsSegmentOption",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "flexTransparent",
"top": "100%",
"width": "100%",
"zIndex": 20000
}, {}, {});
flxCardsSegmentOption.setDefaultUnit(kony.flex.DP);
var FlexContainer0f5eb1fd3515243 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0%",
"clipBounds": true,
"height": "40%",
"id": "FlexContainer0f5eb1fd3515243",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknTransparentRounded",
"width": "100%",
"zIndex": 1
}, {}, {});
FlexContainer0f5eb1fd3515243.setDefaultUnit(kony.flex.DP);

var segCardsOptions = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0%",
"data": [{
"lblTitle": "Label",
"lblValue": "Label"
}, {
"lblTitle": "Label",
"lblValue": "Label"
}, {
"lblTitle": "Label",
"lblValue": "Label"
}],
"groupCells": false,
"height": "100%",
"id": "segCardsOptions",
"isVisible": true,
"right": "0%",
"needPageIndicator": true,
"onRowClick": AS_Segment_c243f75cc0c349de9071a6cc8bcce56e,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "sknBOJBlue80OPC",
"rowSkin": "sknBOJBlue80OPC",
"rowTemplate": CopyFBox0b35b83491a1346,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "072c4800",
"separatorRequired": true,
"separatorThickness": 1,
"showScrollbars": false,
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"lblTitle": "lblTitle",
"lblValue": "lblValue"
},
"width": "100%",
"zIndex": 1
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
FlexContainer0f5eb1fd3515243.add(segCardsOptions);
flxCardsSegmentOption.add(FlexContainer0f5eb1fd3515243);
var flxRetriveStatement = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "91%",
"id": "flxRetriveStatement",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "9%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxRetriveStatement.setDefaultUnit(kony.flex.DP);
var lblSetCustomDateRange = new kony.ui.Label({
"id": "lblSetCustomDateRange",
"isVisible": true,
"right": "5.00%",
"skin": "sknCarioRegular120White70Opc",
"text": kony.i18n.getLocalizedString("i18n.FilterTransaction.setcustomdaterange"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "4%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxCustomDateRange = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxCustomDateRange",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "10%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxCustomDateRange.setDefaultUnit(kony.flex.DP);
var lblDateFrom = new kony.ui.Label({
"height": "60%",
"id": "lblDateFrom",
"isVisible": true,
"right": "5%",
"skin": "sknCarioRegular120White70Opc",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "30%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyFlexContainer0d80598fcd6d44c = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "72%",
"clipBounds": true,
"height": "2%",
"id": "CopyFlexContainer0d80598fcd6d44c",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "43%",
"skin": "skntextFieldDivider",
"width": "5%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0d80598fcd6d44c.setDefaultUnit(kony.flex.DP);
CopyFlexContainer0d80598fcd6d44c.add();
var CopyFlexContainer0g5b3a6c46c784b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "1%",
"clipBounds": true,
"height": "2%",
"id": "CopyFlexContainer0g5b3a6c46c784b",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "skntextFieldDivider",
"width": "35%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0g5b3a6c46c784b.setDefaultUnit(kony.flex.DP);
CopyFlexContainer0g5b3a6c46c784b.add();
var CopyFlexContainer0a61f8d99e19649 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "1%",
"clipBounds": true,
"height": "2%",
"id": "CopyFlexContainer0a61f8d99e19649",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "50%",
"skin": "skntextFieldDivider",
"width": "35%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0a61f8d99e19649.setDefaultUnit(kony.flex.DP);
CopyFlexContainer0a61f8d99e19649.add();
var calDatefrom = new kony.ui.Calendar({
"calendarIcon": "tran.png",
"dateFormat": "MM/dd/yyyy",
"focusSkin": "sknCarioRegular",
"height": "60%",
"id": "calDatefrom",
"isVisible": true,
"right": "5%",
"onSelection": AS_Calendar_c7cdb7f5cb6548a3b1052ad5355e3d60,
"skin": "sknCarioRegular",
"top": "35%",
"validStartDate": [7, 2, 2018],
"viewConfig": {
"gridConfig": {
"gridCellSkin": "Copycalender0h05b9ba7ebfb45",
"gridSkin": "Copycalender0c4c48cb6d2174e",
"leftNavigationImage": "directions_turn_left.png",
"rightNavigationImage": "directions_turn_right.png"
}
},
"viewType": constants.CALENDAR_VIEW_TYPE_DEFAULT,
"width": "28%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var calDateTo = new kony.ui.Calendar({
"calendarIcon": "tran.png",
"dateFormat": "MM/dd/yyyy",
"focusSkin": "sknCarioRegular",
"height": "60%",
"id": "calDateTo",
"isVisible": true,
"right": "52%",
"onSelection": AS_Calendar_jf673df577164fdebe31173c2bf394ec,
"skin": "sknCarioRegular",
"top": "35%",
"validStartDate": [7, 2, 2018],
"viewConfig": {
"gridConfig": {
"gridCellSkin": "Copycalender0h05b9ba7ebfb45",
"gridSkin": "Copycalender0c4c48cb6d2174e",
"leftNavigationImage": "directions_turn_left.png",
"rightNavigationImage": "directions_turn_right.png"
}
},
"viewType": constants.CALENDAR_VIEW_TYPE_DEFAULT,
"width": "28%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var lblDateTo = new kony.ui.Label({
"height": "60%",
"id": "lblDateTo",
"isVisible": true,
"right": "52%",
"skin": "sknCarioRegular120White70Opc",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "30%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyLabel0jba95dfa466447 = new kony.ui.Label({
"id": "CopyLabel0jba95dfa466447",
"isVisible": true,
"right": "5.00%",
"skin": "sknCarioRegular90Opc70",
"text": kony.i18n.getLocalizedString("i18n.cashWithdraw.from"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyLabel0ccd1a4d962ec45 = new kony.ui.Label({
"id": "CopyLabel0ccd1a4d962ec45",
"isVisible": true,
"right": "52%",
"skin": "sknCarioRegular90Opc70",
"text": kony.i18n.getLocalizedString("i18n.common.ToC"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxCustomDateRange.add(lblDateFrom, CopyFlexContainer0d80598fcd6d44c, CopyFlexContainer0g5b3a6c46c784b, CopyFlexContainer0a61f8d99e19649, calDatefrom, calDateTo, lblDateTo, CopyLabel0jba95dfa466447, CopyLabel0ccd1a4d962ec45);
var lblDownloadPDFXLT = new kony.ui.Label({
"id": "lblDownloadPDFXLT",
"isVisible": true,
"right": "5%",
"skin": "sknCarioRegular120White70Opc",
"text": "Download Type",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "27%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxDownloadType = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "6%",
"id": "flxDownloadType",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"onClick": AS_FlexContainer_d54b5c2e532243e28e6e71a2c39b2d42,
"skin": "slFbox",
"top": "32%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxDownloadType.setDefaultUnit(kony.flex.DP);
var CopyFlexContainer0bc45e912af154e = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0%",
"clipBounds": true,
"height": "5%",
"id": "CopyFlexContainer0bc45e912af154e",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "skntextFieldDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0bc45e912af154e.setDefaultUnit(kony.flex.DP);
CopyFlexContainer0bc45e912af154e.add();
var lblDownloadType = new kony.ui.Label({
"centerY": "50%",
"id": "lblDownloadType",
"isVisible": true,
"right": "5%",
"skin": "sknCarioRegular120White70Opc",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblDownloadSelect = new kony.ui.Label({
"centerY": "50%",
"id": "lblDownloadSelect",
"isVisible": true,
"left": "5%",
"skin": "sknLblBoj145",
"text": "d",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxDownloadType.add(CopyFlexContainer0bc45e912af154e, lblDownloadType, lblDownloadSelect);
flxRetriveStatement.add(lblSetCustomDateRange, flxCustomDateRange, lblDownloadPDFXLT, flxDownloadType);
frmCardStatementKA.add(titleBarAccountDetails, flxCardStatement, flxCardsFilterScreen, flxCardsSegmentOption, flxRetriveStatement);
};
function frmCardStatementKAGlobalsAr() {
frmCardStatementKAAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmCardStatementKAAr,
"bounces": false,
"enableScrolling": false,
"enabledForIdleTimeout": true,
"id": "frmCardStatementKA",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"skin": "sknmainGradient"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_c5d81e379924459f9bfdfb96628d07b6,
"retainScrollPosition": false,
"titleBar": true,
"titleBarSkin": "slTitleBar",
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
