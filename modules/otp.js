var SmsReceiverObject = null;
/*
  Author : Vineeth Vishwanath
  Date : 22-01-18
  Desc : preShow for OTPform
  */
function preShowOTP() {
    frmNewUserOnboardVerificationKA.lblInvalidCredentialsKA.setVisibility(false);
    frmNewUserOnboardVerificationKA.flxNext.setEnabled(false);
    frmNewUserOnboardVerificationKA.lblNext.skin = sknLblNextDisabled;
    frmNewUserOnboardVerificationKA.txtOTP.text = "";
    frmNewUserOnboardVerificationKA.btnResendOTP.setVisibility(false);
     
    if (customerAccountDetails.profileDetails !== null) {
        frmNewUserOnboardVerificationKA.lblPhoneNum.text = customerAccountDetails.profileDetails.maskedSMSno;
    } else {
        frmNewUserOnboardVerificationKA.lblPhoneNum.text = "***** ****";
    }
  if(gblToOTPFrom == "UpdateSMSNumber"){
    kony.print("gblToOTPFromgblToOTPFrom--"+gblToOTPFrom);
    var countryCode = "";
    var MobNo = "";
     if(gblFromModule == "UpdateSMSNumber"){     
   countryCode = isEmpty(frmRegisterUser.txtCountryCode.text) ? "962" : frmRegisterUser.txtCountryCode.text;
        MobNo = countryCode + frmRegisterUser.txtMobileNumber.text;
    }else if(gblFromModule == "UpdateMobileNumber"){ 
      MobNo = frmRegisterUser.txtMobileNumber.text;
    }
    if(!isEmpty(MobNo))
    frmNewUserOnboardVerificationKA.lblPhoneNum.text = mask_MobileNumber(MobNo);
    else
    frmNewUserOnboardVerificationKA.lblPhoneNum.text = "***** ****";
  }
    gblMaxOTPLength = 6;
    frmNewUserOnboardVerificationKA.txtOTP.maxTextLength = gblMaxOTPLength + 1;

    frmNewUserOnboardVerificationKA.lblTxt1.text = "";
    frmNewUserOnboardVerificationKA.lblTxt2.text = "";
    frmNewUserOnboardVerificationKA.lblTxt3.text = "";
    frmNewUserOnboardVerificationKA.lblTxt4.text = "";
    frmNewUserOnboardVerificationKA.lblTxt5.text = "";
    frmNewUserOnboardVerificationKA.lblTxt6.text = "";
    for (var i = 1; i < 7; i++) {
        var lineName = "flxLine" + i;
        frmNewUserOnboardVerificationKA[lineName].skin = sknFlxGreyLine;
    }


    switch (gblToOTPFrom) {

        case "RegisterUser":
            frmNewUserOnboardVerificationKA.lblBack.text = geti18Value("i18n.deposit.back");
            frmNewUserOnboardVerificationKA.flxProgress1.setVisibility(true);
            frmNewUserOnboardVerificationKA.flxProgress2.setVisibility(true);
            break;

        case "ForgotPassword":
            frmNewUserOnboardVerificationKA.lblBack.text = geti18Value("i18n.deposit.back");
            frmNewUserOnboardVerificationKA.flxProgress1.setVisibility(true);
            frmNewUserOnboardVerificationKA.flxProgress2.setVisibility(true);
            break;

        case "SendMoney":
            frmNewUserOnboardVerificationKA.lblBack.text = geti18Value("i18n.deposit.back");
            frmNewUserOnboardVerificationKA.flxProgress1.setVisibility(false);
            frmNewUserOnboardVerificationKA.flxProgress2.setVisibility(false);
            break;

        case "ChangeUsername":
            frmNewUserOnboardVerificationKA.lblBack.text = geti18Value("i18n.deposit.back");
            frmNewUserOnboardVerificationKA.flxProgress1.setVisibility(false);
            frmNewUserOnboardVerificationKA.flxProgress2.setVisibility(false);
            break;

        case "ChangePassword":
            frmNewUserOnboardVerificationKA.lblBack.text = geti18Value("i18n.deposit.back");
            frmNewUserOnboardVerificationKA.flxProgress1.setVisibility(false);
            frmNewUserOnboardVerificationKA.flxProgress2.setVisibility(false);
            break;

        case "OwnAccounts":
            frmNewUserOnboardVerificationKA.lblBack.text = geti18Value("i18n.deposit.back");
            frmNewUserOnboardVerificationKA.flxProgress1.setVisibility(false);
            frmNewUserOnboardVerificationKA.flxProgress2.setVisibility(false);
            break;

        case "AddBene":
            frmNewUserOnboardVerificationKA.lblBack.text = geti18Value("i18n.deposit.back");
            frmNewUserOnboardVerificationKA.flxProgress1.setVisibility(false);
            frmNewUserOnboardVerificationKA.flxProgress2.setVisibility(false);
            break;

        case "jomo":
            frmNewUserOnboardVerificationKA.lblBack.text = geti18Value("i18n.deposit.back");
            frmNewUserOnboardVerificationKA.flxProgress1.setVisibility(false);
            frmNewUserOnboardVerificationKA.flxProgress2.setVisibility(false);
            break;

        case "regjp":
            frmNewUserOnboardVerificationKA.lblBack.text = geti18Value("i18n.deposit.back");
            frmNewUserOnboardVerificationKA.flxProgress1.setVisibility(false);
            frmNewUserOnboardVerificationKA.flxProgress2.setVisibility(false);
            break;

        case "web":
            frmNewUserOnboardVerificationKA.lblBack.text = geti18Value("i18n.deposit.back");
            frmNewUserOnboardVerificationKA.flxProgress1.setVisibility(false);
            frmNewUserOnboardVerificationKA.flxProgress2.setVisibility(false);
            break;

        case "AddBiller":
            frmNewUserOnboardVerificationKA.lblBack.text = geti18Value("i18n.deposit.back");
            frmNewUserOnboardVerificationKA.lblBack.text = geti18Value("i18n.deposit.back");
            frmNewUserOnboardVerificationKA.flxProgress1.setVisibility(false);
            frmNewUserOnboardVerificationKA.flxProgress2.setVisibility(false);
            break;

        case "DeleteBiller":
            frmNewUserOnboardVerificationKA.lblBack.text = geti18Value("i18n.deposit.back");
            frmNewUserOnboardVerificationKA.lblBack.text = geti18Value("i18n.deposit.back");
            frmNewUserOnboardVerificationKA.flxProgress1.setVisibility(false);
            frmNewUserOnboardVerificationKA.flxProgress2.setVisibility(false);
            break;

        case "PayBill":
            frmNewUserOnboardVerificationKA.lblBack.text = geti18Value("i18n.deposit.back");
            frmNewUserOnboardVerificationKA.lblBack.text = geti18Value("i18n.deposit.back");
            frmNewUserOnboardVerificationKA.flxProgress1.setVisibility(false);
            frmNewUserOnboardVerificationKA.flxProgress2.setVisibility(false);
            break;

        case "ConfirmPayBill":
            frmNewUserOnboardVerificationKA.lblBack.text = geti18Value("i18n.deposit.back");
            frmNewUserOnboardVerificationKA.flxProgress1.setVisibility(false);
            frmNewUserOnboardVerificationKA.flxProgress2.setVisibility(false);
            break;

        case "StopPaymentSI":
            frmNewUserOnboardVerificationKA.lblBack.text = geti18Value("i18n.deposit.back");
            frmNewUserOnboardVerificationKA.flxProgress1.setVisibility(false);
            frmNewUserOnboardVerificationKA.flxProgress2.setVisibility(false);
            break;

        default:
            frmNewUserOnboardVerificationKA.lblBack.text = geti18Value("i18n.deposit.back");
            frmNewUserOnboardVerificationKA.flxProgress1.setVisibility(false);
            frmNewUserOnboardVerificationKA.flxProgress2.setVisibility(false);
    }
}

// Auto Read SMS code START
function isAndroid() {
	var deviceName = kony.os.deviceInfo()["name"];
	kony.print("device name - " + kony.os.deviceInfo()["name"]);
	if(deviceName == "android" || deviceName == "BlackBerry" || deviceName == "blackberry"|| deviceName == "Blackberry" || deviceName == "blackBerry") {
		return deviceName == "android"|| deviceName == "BlackBerry" || deviceName == "blackberry"|| deviceName == "Blackberry" || deviceName == "blackBerry";
	}	
	//return kony.os.deviceInfo()["name"] == "android";
}
 function registerWithSMSListener() {
    if (isAndroid()) {
                if(SmsReceiverObject == null) {
                                                //Creates an object of class 'SmsReceiver'
                   				kony.print("inside register method null");
                                SmsReceiverObject = new AutoReadSMS.SmsReader(getOTPString,"BOJ"); 
                  //Invokes function 'registerLicense'
					//WrapperJar.registerLicense();
                }
                                //Invokes method 'register' on the object
                                SmsReceiverObject.registerListener();
      kony.print("inside register method invocation success");
    }
}

function deregisterWithSMSListener() {
    /*if (isAndroid()) {
                if(SmsReceiverObject == null) {
                                                //Creates an object of class 'SmsReceiver'
                                SmsReceiverObject = new com.smsOTP.SmsReceiver(getOTPString); 
                }
                                //Invokes method 'register' on the object
                                SmsReceiverObject.deregister();
    }*/
  // Aditya - Invoking de-register only if SmsReceiver Object is initialized
  if(isAndroid()){
    if(SmsReceiverObject != null){
      //Invokes method 'deregister' on the object
                                SmsReceiverObject.deregisterListener();
    }
  }
}

function getOTPString(Obj) {
                kony.print("getOTPString() ---> Obj="+JSON.stringify(Obj));
    //frmOTPValidation.txtOTP.text = Obj;
                var thenum = Obj.replace( /^\D+/g, '');
                kony.print("getOTPString() ---> thenum= "+thenum);
                var otprefno = "";
   /* if(gblRefNo!=null && gblRefNo!="" && contains(gblRefNo, "_")){
      otprefno = gblRefNo.split("_")[0];
    }else{
      otprefno = gblRefNo;
    }
  
                if(!isBlankOrNull(thenum) && thenum.indexOf(otprefno) == -1){
                                kony.print("In correct SMS is being stopped");
                                return;
                }*/
                var otpNum = thenum.substring(0, 6);
                kony.print("getOTPString() ---> otpNum= "+otpNum);
                
                //frmOTPValidation.txtOTP.text = otpNum;             
                kony.print("getOTPString() ---> otpNum="+otpNum+"; thenum="+thenum);
                gblOTPSMSAutoRead = false;
  				onTextChangeOTP2(otpNum);
}

function timerScanCard(){
    popupCommonAlert.dismiss();
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
}

function microBlinkCallBack(response){
  //alert("Hi There"+JSON.stringify(response));
  if(response !== null && response !== ""  ){
    var data = JSON.stringify(response);
     data =  data.replace(/['"]+/g, '');
    //alert("DATA :::"+data);
    if(data == "0"){
   	customAlertPopup(geti18nkey("i18n.maps.Info"), geti18Value("i18n.scan,CancelTimeOUT"), popupCommonAlertDimiss,"");
   // kony.timer.schedule("scanCard",timerScanCard, 3, false);
      return;
    }else if(data.length<10){
     customAlertPopup(geti18nkey("i18n.NUO.Error"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
    return;
    }
  }else{
    customAlertPopup(geti18nkey("i18n.NUO.Error"), geti18Value("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
    return;
  }
  
  if(isAndroid() && response !== null && response !== ""){
    response = response.replace(/\s/g, '');
  	frmRegisterUser.txtCardNum.text = response;  
  }else if(JSON.stringify(response) !== null && JSON.stringify(response) !== ""){
    response = JSON.stringify(response);
    //alert("Hi There judt"+JSON.stringify(response));
     response = response.replace(/\s/g, '');
    //alert("Hi There after"+JSON.stringify(response));
    response = response.replace(/['"]+/g, '');
    frmRegisterUser.txtCardNum.text = response;
    
  }else{
    response ="";
  }
  frmRegisterUser.txtCardNum.setFocus(true);
  frmRegisterUser.forceLayout();

  onTextChangeScanCard(response);
  frmRegisterUser.forceLayout();
  
}


// Auto Read SMS code STOP

/*
  Author : Vineeth Vishwanath
  Date : 22-01-18
  Desc : On click of next from otp form
  */
function nextFromOTP() {
    //   var enteredVal = frmNewUserOnboardVerificationKA.txtOTP.text;
    //   var OTPVal = "123456";
    //   if(enteredVal == OTPVal){
    kony.timer.cancel("mytimer12");
	kony.print("gblToOTPFrom ::"+gblToOTPFrom);
    switch (gblToOTPFrom) {
      
      case "updateBeneficiary":
        	serv_UPDATE_BENEFICIARY_DETAILS();
        break;
      case "RegisterUserNational":
         frmEnrolluserLandingKA.UserFlag.text = "";
         initialiseAndCallEnroll();
        break;
      case "RegisterUser":
            frmEnrolluserLandingKA.UserFlag.text = "";
            initialiseAndCallEnroll();
            break;
        
      case "UpdateSMSNumber":
        changeSMSNumberServiceCall();
        break;
        
        case "ForgotPassword":
            frmEnrolluserLandingKA.UserFlag.text = "";
            initialiseAndCallEnroll();
            break;

        case "ChangeUsername":
            frmEnrolluserLandingKA.UserFlag.text = "Update";
            callSave();
            break;

        case "ChangePassword":
            frmEnrolluserLandingKA.UserFlag.text = ""
            callSave();
            break;

        case "SendMoney":
            kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.success"), geti18Value("i18n.transfers.successSentMoney"), geti18Value("i18n.transfers.gotoDashPay"), "PaymentDashboard");
            break;

        case "regjp":
            kony.print("phonee:"+frmJoMoPayRegistration.lblPhoneConfirm.text);
            frmJoMoPayRegistration.forceLayout();
            var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var controller = INSTANCE.getFormController("frmJoMoPayRegistration");
        var controllerContextData = controller.getContextData()|| new kony.sdk.mvvm.NavigationObject();
        controllerContextData.setDataModel(null, kony.sdk.mvvm.OperationType.ADD, "form");
       controller.setContextData(controllerContextData);
		controller.performAction("saveData");
            break;

        case "web":
            performOwnTransfer();
            break;

        case "credit":
            payFromCreditCard();
            break;

        case "OwnAccounts":
            performOwnTransfer();
            break;

        case "ConfirmPayBill":
            performBillPayPostpaid();
            break;

        case "Pin":
            EnableDisablePinLogin();
            break;
        
        case "AddBene":
//             var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
//             var listController = INSTANCE.getFormController("frmAddExternalAccountKA");
//             listController.performAction("saveData");
      		serv_ADD_TRANSFER_BENEFICIARY();
            kony.boj.detailsForBene.flag = false;
            break;
        
      case "RetriveUsername":
        var langSelected = kony.store.getItem("langPrefObj");
        var mm="";
        if(langSelected == "en")
          mm=engSUNMmsg;
        else
          mm=araSUNMmsg;

        mm =mm.format(customerAccountDetails.profileDetails.title + " " + customerAccountDetails.profileDetails.first_name,userName);
		kony.print("The messege for the username is :: " + mm);
        callRequestUsrname(mm);
        break;   

        case "jomo":
            kony.print("INSTANCE creation");
            serv_PAYMENT_JOMOPAY();
            break;

        case "AddBiller":
            INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
            listController = INSTANCE.getFormController("frmAddNewPayeeKA");
            listController.performAction("saveData");
            break;

        case "DeleteBiller":
            INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
            listController = INSTANCE.getFormController("frmPayeeDetailsKA");
            listController.performAction("deleteData");
            break;

        case "PayBill":
            kony.boj.populateSuccessScreen("success.png", geti18nkey("i18n.common.paymSucc"), geti18nkey("i18n.common.ReferenceNumber")+" 974973", geti18nkey("i18n.common.CgtAD"), geti18nkey ("i18n.common.AccountDashboard"), geti18nkey("i18n.common.CgtB"), "ManagePayee");
            break;

        case "StopPaymentSI":
            //       alert("here");
            //       INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
            //       listController = INSTANCE.getFormController("frmConfirmStandingInstructions");
            //       listController.performAction("deleteData");
            var scopeObj = this;
            var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
            var options = {
                "access": "online",
                "objectName": "RBObjects"
            };
            var headers = {};
            var serviceName = "RBObjects";
            var modelObj = INSTANCE.getModel("StandingInstructions", serviceName, options);
            var queryParams = {
                "InstructionNum": standingIns,
                "custId": custid,
            };
            var dataObject = new kony.sdk.dto.DataObject("StandingInstructions", queryParams);

            var serviceOptions = {
                "dataObject": dataObject,
                "headers": headers
            };
            kony.print("Input params-->" + JSON.stringify(serviceOptions));
            modelObj.create(serviceOptions, standingInstructionDeleteSuccess, standingInstructionDeleteFailure);

            break;
      case "cardLimit":
      	serv_INTERNETANDMAILORDERLIMIT();
      	break;
      case "CLIQ":// hassan otp ips transfer
        TransferIPS();
        break;
        default:
            alert("Please set the global variable gblToOTPFrom");
    }
    //   }

    // //   else{
    // //     alert(geti18Value("i18n.transfers.incorrectOTP"));
    // //   }
    kony.timer.cancel("mytimer12");
}

function backFromOTP() {
  
  if(!isAndroid()){
    frmNewUserOnboardVerificationKA.loadCustomWidgetForm.resignKeyBoard();
  }
  
  kony.timer.cancel("mytime21");
  switch (gblToOTPFrom) {
      
    case "RetriveUsername" :
      frmRegisterUser.show();
      break;
      
    case "UpdateSMSNumber" :
      frmRegisterUser.show();
            break;
      
      case "RegisterUserNational":
      frmNewUserOnboardVerificationKA.destroy();
      frmRegisterUser.show();
      break;
      
    case "RegisterUser":
      frmNewUserOnboardVerificationKA.destroy();
      frmRegisterUser.show();
      break;

    case "Pin":
      onclickPinSet();
      break;
      
    case "ForgotPassword":
      frmNewUserOnboardVerificationKA.destroy();
      frmRegisterUser.show();
      break;

        case "SendMoney":
            frmNewTransferKA.show();
            break;

        case "OwnAccounts":
            frmNewTransferKA.show();
            break;

        case "AddBene":
            frmAddExternalAccountKA.show();
            break;
      
    	case "updateBeneficiary":
    		frmAddExternalAccountKA.show();
    		break;
      
        case "jomo":
            frmJoMoPayConfirmation.show();
            break;

        case "web":
            frmWebCharge.show();
            break;

        case "regjp":
            frmJoMoPayRegistration.show();
            break;

        case "credit":
            frmCreditCardPayment.show();
            break;

        case "AddBiller":
            frmAddNewPayeeKA.show();
            break;

        case "DeleteBiller":
            if (kony.boj.deletePayeeFrom === "ManagePayee")
                frmManagePayeeKA.show();
            else
                frmPayeeDetailsKA.show();
            break;

        case "PayBill":
            frmNewBillKA.show();
            break;

        case "ConfirmPayBill":
            frmConfirmPayBill.show();
            break;

        case "ChangeUsername":
      		if(gblforceflag){
              gblforceflag = true;
    		  gblFromModule = "ChangeUsername";
    		  gblToOTPFrom = "ChangeUsername";
    		  ShowLoadingScreen();
    		  initialiseAndCallEnroll();
            }else{
            frmSettingsKA.show();
            }
            break;

        case "ChangePassword":
            frmSettingsKA.show();
            break;

        case "StopPaymentSI":
            frmConfirmStandingInstructions.show();
            break;
    case "cardLimit":
    	frmEnableInternetTransactionKA.show();
    	break;
    case "CLIQ":
    	frmEPS.show();
    	break;
        default:
            alert("Please set the global variable gblToOTPFrom!!!!!");
    }
    kony.timer.cancel("mytimer12");
}

function onTextChangeOTP(otpStatusFlag) {
  //frmNewUserOnboardVerificationKA.flxLine2.skin = "sknFlxBlueLine";  
    isNextEnabled();
  var otpText = "";
  if(otpStatusFlag === true){
    otpText = "";
    frmNewUserOnboardVerificationKA.txtOTP.text = "";
    if(!isAndroid()){
      frmNewUserOnboardVerificationKA.loadCustomWidgetForm.clearOTPTextField();
      frmNewUserOnboardVerificationKA.loadCustomWidgetForm.resignKeyBoard();
    }
    //frmNewUserOnboardVerificationKA.txtOTP.setFocus(true);
    //if(isAndroid()){
   // registerWithSMSListener();
    //}
    return;
  }
  else
    otpText = frmNewUserOnboardVerificationKA.txtOTP.text;
   if(!isEmpty(otpText)){
    if(otpText.length === 7 && 
     frmNewUserOnboardVerificationKA.lblTxt1.text === "" && frmNewUserOnboardVerificationKA.lblTxt2.text === "" &&
     frmNewUserOnboardVerificationKA.lblTxt3.text === "" && frmNewUserOnboardVerificationKA.lblTxt4.text === "" &&
     frmNewUserOnboardVerificationKA.lblTxt5.text === "" && frmNewUserOnboardVerificationKA.lblTxt6.text === "")
    onTextChangeOTP2(otpText.substring(6));
  else
    onTextChangeOTP2(otpText);
  }
  else{
    if(kony.store.getItem("langPrefObj")==="en"){
      frmNewUserOnboardVerificationKA.lblTxt1.text = "";
      frmNewUserOnboardVerificationKA.flxLine1.skin = sknBlueflxLine;
      frmNewUserOnboardVerificationKA.flxLine2.skin = sknFlxGreyLine;
    }else{
      frmNewUserOnboardVerificationKA.lblTxt6.text = "";
      frmNewUserOnboardVerificationKA.flxLine6.skin = sknBlueflxLine;
      frmNewUserOnboardVerificationKA.flxLine5.skin = sknFlxGreyLine;
    }
  }
}

function onTextChangeOTP2(otpText){
  var langSelected = kony.store.getItem("langPrefObj");
  frmNewUserOnboardVerificationKA.txtOTP.text = otpText;

  if(langSelected == "en"){

    for (var i = 1; i <= otpText.length; i++) {
      var lblName = "lblTxt" + i;
      frmNewUserOnboardVerificationKA[lblName].text = otpText[i - 1];
    }

    for (var i = otpText.length + 1; i <= 6; i++) {
      var lblName = "lblTxt" + i;
      frmNewUserOnboardVerificationKA[lblName].text = "";
    }

    for (var i = 1; i <= 6; i++) {
      var lineName = "flxLine" + i;
      if (i == otpText.length + 1) {
        frmNewUserOnboardVerificationKA[lineName].skin = sknBlueflxLine;
      } else if (i <= otpText.length) {
        frmNewUserOnboardVerificationKA[lineName].skin = sknFlxGreenLine;
      } else {
        frmNewUserOnboardVerificationKA[lineName].skin = sknFlxGreyLine;
      }
    }

  }else{
    //Arabic layout

    try{
      for (var i = 1,j=6; i <= otpText.length; i++) {
        var lblName = "lblTxt" + j;
        j--;
        frmNewUserOnboardVerificationKA[lblName].text = otpText[i - 1];
      }

      for (var i = otpText.length + 1, j = 7-i; i <= 6; i++) {
        var lblName = "lblTxt" + j;
        frmNewUserOnboardVerificationKA[lblName].text = "";
      }
      for (var i = 1; i <= 6; i++) {
        var lineName = "flxLine" + i;
        if (i == 6 - otpText.length) {
          frmNewUserOnboardVerificationKA[lineName].skin = sknBlueflxLine;
        } else if (i > 6 - otpText.length) {
          frmNewUserOnboardVerificationKA[lineName].skin = sknFlxGreenLine;
        } else {
          frmNewUserOnboardVerificationKA[lineName].skin = sknFlxGreyLine;
        }
      }
    }catch(e){
      kony.print("Exception onTextChangeOTP2 :: "+ e);
	  exceptionLogCall("onTextChangeOTP2","UI ERROR","UI",e);
    }

  }
  if (frmNewUserOnboardVerificationKA.txtOTP.text.length == 6) {
    if(gblOTPSMSAutoRead)
    frmNewUserOnboardVerificationKA.txtOTP.setFocus(false);
    frmNewUserOnboardVerificationKA.flxNext.setFocus(true);
//     frmNewUserOnboardVerificationKA.btnResendOTP.setFocus(true);
    gblOTPSMSAutoRead = true;
    gotoOTPVerification();
  }
}






function registerWithSMSListener1() {
                kony.print("calling registerWithSMSListener");
    try{
      if (isAndroid()) {
          //if(SmsReceiverObject == null) {
              //Creates an object of class 'SmsReceiver'
              //SmsReceiverObject = new com.smsOTP.SmsReceiver(getOTPString); 
              // com.smsOTP.setSmsRetrieverCB(getOTPString);
               //com.smsOTP.startSmsRetriever();

      //     }
          //Invokes method 'register' on the object
          //SmsReceiverObject.register();
      }
    }catch(e){
      kony.print("error while calling registerWithSMSListener: " + e);
    }
}

function deregisterWithSMSListener1() {
                kony.print(" --> inside deregisterWithSMSListener");
    try{
         /*if (isAndroid()) {
              if(SmsReceiverObject == null) {
                  //Creates an object of class 'SmsReceiver'
                  SmsReceiverObject = new com.smsOTP.SmsReceiver(getOTPString); 
              }
              //Invokes method 'register' on the object
              SmsReceiverObject.deregister();
          }*/
        // Aditya - Invoking de-register only if SmsReceiver Object is initialized
        if(isAndroid()){
          //if(SmsReceiverObject != null){
            //Invokes method 'deregister' on the object
              //SmsReceiverObject.deregister();
             // com.smsOTP.stopSmsRetriever();
         // }
        }
    }catch(e){
      kony.print("error while calling deregisterWithSMSListener: " +e);
    }
}

