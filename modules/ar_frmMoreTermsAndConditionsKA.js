//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmMoreTermsAndConditionsKAAr() {
    frmMoreTermsAndConditionsKA.setDefaultUnit(kony.flex.DP);
    var androidTitleBar = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "androidTitleBar",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skntitleBarGradient",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    androidTitleBar.setDefaultUnit(kony.flex.DP);
    var androidTitleLabel = new kony.ui.Label({
        "centerY": "50%",
        "id": "androidTitleLabel",
        "isVisible": true,
        "right": "55dp",
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.common.termsAndConditions"),
        "width": "70%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var androidBack = new kony.ui.Button({
        "focusSkin": "sknandroidBackButtonFocus",
        "height": "50dp",
        "id": "androidBack",
        "isVisible": true,
        "right": "0dp",
        "onClick": AS_Button_9f72377dcc12445ba73e68d1a0926ee5,
        "skin": "sknandroidBackButton",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    androidTitleBar.add(androidTitleLabel, androidBack);
    var mainContent = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bottom": 0,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "horizontalScrollIndicator": true,
        "id": "mainContent",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknscrollBkgGray",
        "top": "50dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    mainContent.setDefaultUnit(kony.flex.DP);
    var rTxtTAndCKA = new kony.ui.RichText({
        "id": "rTxtTAndCKA",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknRichTxt100LR30363fKA",
        "text": " Kony By accessing the information provided at the Kony Mobile Banking site, you agree to the following terms and conditions. The material available on these sites has been produced by independent providers and are not the opinions or recommendations of kony Mobile Banking. Proin elit quam, feugiat quis sem eu, euismod feugiat lacus. Donec posuere sapien eu auctor lacinia. Quisque dictum augue nec auctor venenatis. Cras tincidunt tristique mauris, non tincidunt metus elementum quis.<br><br>Etiam nec pulvinar dui, eget eleifend felis. Proin bibendum molestie dolor. Aenean dictum pharetra mauris, ultrices pretium nunc imperdiet in. Cras et dui mollis, tempus velit placerat, sodales tellus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Quisque et venenatis sapien. Maecenas sem diam, lacinia in ipsum et, fermentum rutrum lacus.</br></br><br><br>Nulla dictum tincidunt turpis eu consequat. Sed adipiscing eros a nisi dictum mollis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vivamus urna lorem, porta sed congue ut, sollicitudin quis erat. Aliquam ac dolor id nulla lobortis adipiscing id quis mauris. Duis arcu nunc, mollis a quam vel, aliquam dignissim neque. Maecenas magna dui, dignissim quis felis sed, tincidunt molestie magna. Proin semper felis in dolor commodo, quis facilisis lorem facilisis.</br></br><br><br>Vivamus massa odio, dignissim ac ante at, euismod dignissim risus. Cras ut blandit lorem. Maecenas nisl quam, cursus nec aliquet facilisis, mattis id augue. Fusce semper odio et gravida interdum. Suspendisse quis lacus nulla. Nullam et nibh ligula. Nunc vitae nulla et arcu mollis iaculis. Vestibulum venenatis risus ut ligula lacinia malesuada. Nulla facilisis, arcu ac facilisis tincidunt, tortor est congue ligula, eu adipiscing quam ligula ut purus. Suspendisse at gravida dolor. Vestibulum eget odio in massa luctus imperdiet. Donec consequat orci a diam pellentesque mollis.</br></br><br><br>Fusce ac consectetur enim. Vivamus turpis tellus, malesuada ac erat vitae, pulvinar venenatis turpis. Quisque interdum blandit est id volutpat. Etiam in ipsum sagittis, accumsan ipsum vitae, tincidunt metus.</br></br>",
        "top": 0,
        "width": "100%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
        "padding": [ 2, 2,5, 0],
        "paddingInPixel": false
    }, {});
    var txtFaqKA = new kony.ui.TextArea2({
        "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
        "height": "120dp",
        "id": "txtFaqKA",
        "isVisible": false,
        "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
        "right": "37dp",
        "numberOfVisibleLines": 3,
        "skin": "slTextArea",
        "text": "TextArea2",
        "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
        "top": "0dp",
        "width": "260dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
        "padding": [ 2, 2,2, 2],
        "paddingInPixel": false
    }, {});
    mainContent.add(rTxtTAndCKA, txtFaqKA);
    frmMoreTermsAndConditionsKA.add(androidTitleBar, mainContent);
};
function frmMoreTermsAndConditionsKAGlobalsAr() {
    frmMoreTermsAndConditionsKAAr = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmMoreTermsAndConditionsKAAr,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmMoreTermsAndConditionsKA",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": true,
        "inTransitionConfig": {
            "formAnimation": 2
        },
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_efd7f60f5b0547fb8a41033e626f4801,
        "outTransitionConfig": {
            "formAnimation": 5
        },
        "retainScrollPosition": false,
        "titleBar": true,
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};
