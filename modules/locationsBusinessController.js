/**
* Business Controller for location Object
*/
kony = kony || {};
kony.rb = kony.rb || {};

kony.rb.locationsBusinessController = function(mfObjSvc){
  this.mfObjectServiceHandler = mfObjSvc;
};

/**
* function getPinAddress
* gets address from lat lon positions
*/
kony.rb.locationsBusinessController.prototype.getPinAddress = function(addressdata,presentationSuccessCallback, presentationErrorCallback) 
{ 
  if(addressdata) {
  var location= {"latitude":addressdata.lat,"longitude":addressdata.lon};
  this.mfObjectServiceHandler.customPost("getLocationAddress","Locations",location, {}, success, error);
  function success(response){
    var address={};  
    var addresslane1="";
    var addresslane2="";
    var state="";
    var data =response.formattedAddress;
    var arr=data.split(",");
    if(arr.length>=3){
      var statepostal=arr[arr.length-2].trim().split(" ");  
      var address={"country":arr[arr.length-1].trim(),"pincode":statepostal[statepostal.length-1],"city":arr[arr.length-3].trim()};
      var mid=Math.round((arr.length-3)/2);
    for(var i=0;i<statepostal.length-1;i++)
       {
         state=state+statepostal[i]+" ";
       }
   address["state"]=state.trim();
      for(var i=0;i<mid;i++)
      {
        addresslane1=addresslane1+arr[i]+",";
      }
      address["addresslane1"]=addresslane1.substring(0,addresslane1.length-1).trim();
      for(var j=mid;j<=arr.length-3;j++){
        addresslane2=addresslane2+arr[j]+",";
      }
      address["addresslane2"]=addresslane2.substring(0,addresslane2.length-1).trim();
      address["formattedAddress"]=data;
      presentationSuccessCallback(address);
    }
    else if(arr.length==2)
    {
    address["country"]=arr[1];
    address["state"]=arr[0];
    address["formattedAddress"]=data;
    presentationSuccessCallback(address);
    }  
    else if(arr.length==1 && arr[0]!=="" )
    {
    address["country"]=arr[0];
    address["formattedAddress"]=data;
    presentationSuccessCallback(address);
    }
    else{
      presentationSuccessCallback(response);
    }   
  }
	function error(err)
	{
	  presentationErrorCallback(err);
	}
  }
  else {
    kony.application.dismissLoadingScreen();
  }
};

kony.rb.locationsBusinessController.prototype.addressSuggestions = function(query1,presentationSuccessCallback, presentationErrorCallback)
{
     
     var feilds = {"query":query1};

    this.mfObjectServiceHandler.customPost("getAddressSuggestions","Locations",feilds ,{}, success, error);
    function success(res){
      
      presentationSuccessCallback(res);
      
	}
	function error(err)
	{
      presentationErrorCallback(err);
	}
 }; 