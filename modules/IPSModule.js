//Type your code here
var Alias_Type = "";
var gblWorkflowType = "";
function validate_IPS(val, widget){
	try{
    	var valid_DATA = false;
    	valid_DATA = isEmpty(frmIPSRegistration.lblAccountNumber.text)?false:true;
    	kony.print("valid_DATA ::"+valid_DATA);
    	valid_DATA = (valid_DATA && validate_IPS_ENTRED_VALUE(val, widget));
    	if(valid_DATA){
        	frmIPSRegistration.btnNext.skin = "jomopaynextEnabled";
        }else{
        	frmIPSRegistration.btnNext.skin = "jomopaynextDisabled";
        }
    }catch(e){
    	kony.print("Exception_validate_IPS ::"+e);
    }
}

function validate_IPS_ENTRED_VALUE(val, widget){
	try{
    	var isValid = false;
    	if(frmIPSRegistration.btnMobileNumber.skin === "sknOrangeBGRNDBOJ"){
        	if((val.match(/[0-9]/g)) && ((val.length >=11) && (val.length <= 15))){
            	isValid = true;
            }
        }else if(frmIPSRegistration.btnAlias.skin === "sknOrangeBGRNDBOJ"){
          widget.text=val.replace(/[a-z]/g,"");
        	if(val.match(/[^A-Z0-9]/g)){
              val = val.replace(/[^A-Z0-9]/g, "");
              widget.text = val;
              return;
            }
          
        	if(val.match(/[A-Z0-9]/g) && (val.length <= 10)&& (val.length >= 3)){
            	isValid = true;
            }
        }
    	return isValid;
    }catch(e){
    	kony.print("Exception_validate_IPS_ENTRED_VALUE ::"+e);
    }
}

function onSelect_REGISTRATION_TYPES(selection, widget){
	try{
    	switch(selection){
          case "MOBILE":
        	frmIPSRegistration.btnMobileNumber.skin = "sknOrangeBGRNDBOJ";
            frmIPSRegistration.btnAlias.skin = "slButtonBlueFocus";
            widget.text = "";
			widget.textInputMode = constants.TEXTBOX_INPUT_MODE_NUMERIC;
            frmIPSRegistration.lblAliasTitle.text=geti18Value("i18n.jomopay.mobiletype");
            frmIPSRegistration.LblCountryCodeHint.setVisibility(true);
            frmIPSRegistration.flxAliasHints.setVisibility(false);
         	break;
          case "ALIAS":
            frmIPSRegistration.btnMobileNumber.skin = "slButtonBlueFocus";
            frmIPSRegistration.btnAlias.skin = "sknOrangeBGRNDBOJ";
            widget.text = "";
            widget.textInputMode = constants.TEXTBOX_INPUT_MODE_ANY;
			widget.autoCapitalize = constants.TEXTBOX_AUTO_CAPITALIZE_ALL;
            frmIPSRegistration.lblAliasTitle.text=geti18Value("i18n.jomopay.aliastype");
            frmIPSRegistration.LblCountryCodeHint.setVisibility(false);
            frmIPSRegistration.flxAliasHints.setVisibility(true);
            break;
        }
        frmIPSRegistration.btnNext.skin = "jomopaynextDisabled";
    }catch(e){
    	kony.print("Exception_onSelect_REGISTRATION_TYPES ::"+e);
    }
}

function show_ACCOUNT_LIST(){
	try{
    	var fromAccounts = kony.retailBanking.globalData.accountsDashboardData.fromAccounts;
        gblTModule = "IPSRegistration";
        accountsScreenPreshow(fromAccounts);
        frmAccountDetailsScreen.lblHead.text = geti18Value("i18n.common.accountNumber");
        frmAccountDetailsScreen.show();
    }catch(e){
    	kony.print("Exception_show_ACCOUNT_LIST ::"+e);
    }
}

function set_ACCOUNT_LIST_IPS_REGISTRATION(dataSelected){
	try{
      	var name = dataSelected.accountName;
    	kony.print("account selected ::"+JSON.stringify(dataSelected));
    	frmIPSRegistration.lblAccountNumberTitle.isVisible=false;
        frmIPSRegistration.flxIcon1.isVisible=true;
    	frmIPSRegistration.lblAccountNumber.text = dataSelected.accountID;
    	if(!isEmpty(dataSelected.accNickName))
        	name = dataSelected.accNickName;
        frmIPSRegistration.lblAccountName1.text=name;
        frmIPSRegistration.lblIcon1.text= name.substring(0,2).toUpperCase();
    	kony.store.setItem("frmAccount",dataSelected);
    	validate_IPS(frmIPSRegistration.txtAlias.text, frmIPSRegistration.txtAlias);
    	frmIPSRegistration.show();
    }catch(e){
    	kony.print("Exception_set_ACCOUNT_LIST_IPS_REGISTRATION ::"+e);
    }
}

function navigate_TO_IPS_REGISTRATION_CONFIRMATION(){
	try{
      
    	frmIPSRegistration.lblAccountNumberConfirmation.text = frmIPSRegistration.lblAccountNumber.text;
    	if(frmIPSRegistration.btnMobileNumber.skin === "sknOrangeBGRNDBOJ"){
    		frmIPSRegistration.lblAliasTypeConfirmation.text = geti18Value("i18n.jomopay.mobiletype");
        	frmIPSRegistration.lblAliasConfirmationTitle.text = geti18Value("i18n.jomopay.mobiletype");
            Alias_Type = "MOBL";
        }else if(frmIPSRegistration.btnAlias.skin === "sknOrangeBGRNDBOJ"){
        	frmIPSRegistration.lblAliasTypeConfirmation.text = geti18Value("i18n.jomopay.aliastype");
        	frmIPSRegistration.lblAliasConfirmationTitle.text = geti18Value("i18n.jomopay.aliastype");
            Alias_Type = "ALIAS";
        }
    	frmIPSRegistration.lblAliasConfirmation.text = frmIPSRegistration.txtAlias.text;
    	frmIPSRegistration.btnNext.setVisibility(false);
    	frmIPSRegistration.flxBody.setVisibility(false);
    	frmIPSRegistration.flxConfirmation.setVisibility(true);
    }catch(e){
    	kony.print("Exception_navigate_TO_IPS_REGISTRATION_CONFIRMATION ::"+e);
    }
}

function navigate_BACK_IPS_REGISTRATION(){
	if(frmIPSRegistration.flxConfirmation.isVisible){
    	frmIPSRegistration.flxConfirmation.setVisibility(false);
    	frmIPSRegistration.btnNext.setVisibility(true);
    	frmIPSRegistration.flxBody.setVisibility(true);
    }else if(previous_FORM !== null){
    	previous_FORM.show();
        frmIPSRegistration.destroy();
    }else{
    	frmIPSHome.show();
    	frmIPSRegistration.destroy();
    }
}

function set_SELECTED_ACCOUNT_TO_CLIQ_TRANS(dataSelected){
	try{
    	kony.store.setItem("frmAccount",dataSelected);
    	frmEPS.lblAccountName1.text = dataSelected.accountName;
    	frmEPS.lblAccountNumber1.text = dataSelected.accountID;
    	frmEPS.lblIcon1.text = (dataSelected.accountName.substring(0,2)).toUpperCase();
    	frmEPS.lblSelectanAccount1.setVisibility(false);
    	frmEPS.flxIcon1.setVisibility(true);
        validateLblNext();
      	frmEPS.show();
      // to fix Accounts double choose to get lbl next valditae issue (Ahmad-OCT-5)
       //#ifdef iphone
      validateLblNext();
       //#endif 
      // to fix Accounts double choose to get lbl next valditae issue (Ahmad-OCT-5)
    	frmAccountDetailsScreen.destroy();
    }catch(e){
    	kony.print("Exception_set_SELECTED_ACCOUNT_TO_CLIQ_TRANS ::"+e);
    }
}

function get_ACCOUNTS_FOR_CLIQ(data){
	try{
    	var temp = [];
    	for(var i in data){
        	kony.print("subStringed Account ID ::"+data[i].accountID.substring(0,3));
        	if(data[i].accountID.substring(0,3) === "001"){
            	temp.push(data[i]);
            }
        }
    	return temp;
    }catch(e){
    	kony.print("Exception_get_ACCOUNTS_FOR_CLIQ ::"+e);
    }
}



function SetupRegIPSScreen(){
frmIPSRegistration.lblAccountNumberTitle.setVisibility(true);
frmIPSRegistration.btnAlias.skin = "sknOrangeBGRNDBOJ";
frmIPSRegistration.LblCountryCodeHint.setVisibility(false);
frmIPSRegistration.flxAliasHints.setVisibility(true);
frmIPSRegistration.lblAliasTitle.text=geti18Value("i18n.jomopay.aliastype"); 
frmIPSRegistration.show();
}

function show_IPS_BENEFICIARY_LIST(data){
	try{
    	var fav = [], dataNorm = [];
    	if(!isEmpty(data)){
          for(var i in data){
          	data[i].initial = data[i].nickName.substring(0,2).toUpperCase();
            data[i].icon = {
              backgroundColor: kony.boj.getBackGroundColour(data[i].initial)
            };
            if(data[i].isFav === "Y" || data[i].isFav === "y")
            	fav.push(data[i]);
            else
            	dataNorm.push(data[i]);
          }
          frmIPSManageBene.segIPSBene.widgetDataMap = { 
            											lbltmpTitle : "lbltmpTitle",
                                                        accountNumber :"benAcctNo",
                                                        BenificiaryName: "nickName",
                                                        BenificiaryFullName: "beneficiaryName",
                                                        lblInitial: "initial",
                                                        flxIcon1: "icon"
                                                      };
          if(!isEmpty(fav) && !isEmpty(dataNorm)){
            kony.print("favourite list ::"+JSON.stringify(fav));
            kony.print("Normal list ::"+JSON.stringify(dataNorm));
          	frmIPSManageBene.segIPSBene.data = [[
                                                    {lbltmpTitle: geti18Value("i18n.common.Favourite")},
                                                    fav
                                                  ],[
                                                  {lbltmpTitle: geti18Value("i18n.Bene.AllContacts")},
                                                  dataNorm
                                                  ]];
          }else if(!isEmpty(fav) && isEmpty(dataNorm)){
            kony.print("favourite list ::"+JSON.stringify(fav));
            frmIPSManageBene.segIPSBene.data = [[
                                                    {lbltmpTitle: geti18Value("i18n.common.Favourite")},
                                                    fav
                                                  ]];
          }else if(isEmpty(fav) && !isEmpty(dataNorm)){
            kony.print("Normal list ::"+JSON.stringify(dataNorm));
          	frmIPSManageBene.segIPSBene.data = [[
                                                  {lbltmpTitle: geti18Value("i18n.Bene.AllContacts")},
                                                  dataNorm
                                                  ]];
          }
        }
    	frmIPSManageBene.show();
       // added by ahmad to solve visibility issue for setting button and add alias button 
      frmIPSManageBene.btnGetAllAliases.setVisibility(true);
      frmIPSManageBene.btnAddAlias.setVisibility(false);
      // added by ahmad to solve visibility issue for setting button and add alias button 
    }catch(e){
    	kony.print("Exception_show_IPS_BENEFICIARY_LIST ::"+e);
    }
}

function show_SELECTED_BENEFICIARY_IPS_TRANSFER(data){
	try{
    	kony.print("selected Beneficiary Details ::"+JSON.stringify(data));
    }catch(e){
    	kony.print("Exception_show_SELECTED_BENEFICIARY_IPS_TRANSFER ::"+e);
    }
}