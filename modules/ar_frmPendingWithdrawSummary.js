//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmPendingWithdrawSummaryAr() {
frmPendingWithdrawSummary.setDefaultUnit(kony.flex.DP);
var titleBarWrapper = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "titleBarWrapper",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgheader",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
titleBarWrapper.setDefaultUnit(kony.flex.DP);
var androidTitleBar = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "androidTitleBar",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgheader",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
androidTitleBar.setDefaultUnit(kony.flex.DP);
var androidTitleLabel = new kony.ui.Label({
"centerY": "50%",
"id": "androidTitleLabel",
"isVisible": true,
"right": "55dp",
"skin": "sknnavBarTitle",
"text": "Withdraw Summary",
"width": "70%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var androidBack = new kony.ui.Button({
"focusSkin": "sknandroidBackButtonFocus",
"height": "50dp",
"id": "androidBack",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_a283e87c4e4343d0a0687f6a72fe2613,
"skin": "sknandroidBackButton",
"top": "0dp",
"width": "50dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
androidTitleBar.add(androidTitleLabel, androidBack);
titleBarWrapper.add(androidTitleBar);
var FlexScrollContainerConfirmWithdraw = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": true,
"allowVerticalBounce": true,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "92%",
"horizontalScrollIndicator": false,
"id": "FlexScrollContainerConfirmWithdraw",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "sknscrollBkg",
"verticalScrollIndicator": false,
"width": "100%",
"zIndex": 1
}, {}, {});
FlexScrollContainerConfirmWithdraw.setDefaultUnit(kony.flex.DP);
var FlexContainerWithdrawDetails = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "115dp",
"id": "FlexContainerWithdrawDetails",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
FlexContainerWithdrawDetails.setDefaultUnit(kony.flex.DP);
var innerFlexContainerWithdrawDetails = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "100%",
"id": "innerFlexContainerWithdrawDetails",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
innerFlexContainerWithdrawDetails.setDefaultUnit(kony.flex.DP);
var withdrawAmount = new kony.ui.Label({
"centerX": "50%",
"id": "withdrawAmount",
"isVisible": true,
"skin": "skndetailPageNumber",
"text": "$ 5006.00",
"top": "10dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var forCollectionLabel = new kony.ui.Label({
"centerX": "50%",
"id": "forCollectionLabel",
"isVisible": true,
"skin": "skndetailPageDate",
"text": "For collection by",
"top": "5dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var collectorName = new kony.ui.Label({
"centerX": "50%",
"id": "collectorName",
"isVisible": true,
"skin": "skn30363f110KA",
"text": "Self",
"top": "5dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var divider1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "divider1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"top": "10dp",
"width": "100%",
"zIndex": 1
}, {}, {});
divider1.setDefaultUnit(kony.flex.DP);
divider1.add();
innerFlexContainerWithdrawDetails.add(withdrawAmount, forCollectionLabel, collectorName, divider1);
FlexContainerWithdrawDetails.add(innerFlexContainerWithdrawDetails);
var fromFlxDetails = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "58dp",
"id": "fromFlxDetails",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
fromFlxDetails.setDefaultUnit(kony.flex.DP);
var fromLabel = new kony.ui.Label({
"id": "fromLabel",
"isVisible": true,
"right": "5%",
"skin": "sknsegmentHeaderText",
"text": "From:",
"top": "2dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var withdrawFrom = new kony.ui.Label({
"id": "withdrawFrom",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "Savings 2453",
"top": "27dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var divider2 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "divider2",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
divider2.setDefaultUnit(kony.flex.DP);
divider2.add();
fromFlxDetails.add(fromLabel, withdrawFrom, divider2);
var flxTransactionDetails = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "58dp",
"id": "flxTransactionDetails",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxTransactionDetails.setDefaultUnit(kony.flex.DP);
var lblTransactionID = new kony.ui.Label({
"id": "lblTransactionID",
"isVisible": true,
"right": "5%",
"skin": "sknsegmentHeaderText",
"text": "Transaction ID",
"top": "5dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopywithdrawFrom0ed9893a29e9c40 = new kony.ui.Label({
"id": "CopywithdrawFrom0ed9893a29e9c40",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "34657694556",
"top": "30dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copydivider0e5c6e772eade4e = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider0e5c6e772eade4e",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
Copydivider0e5c6e772eade4e.setDefaultUnit(kony.flex.DP);
Copydivider0e5c6e772eade4e.add();
flxTransactionDetails.add(lblTransactionID, CopywithdrawFrom0ed9893a29e9c40, Copydivider0e5c6e772eade4e);
var flexNotesDetails = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flexNotesDetails",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flexNotesDetails.setDefaultUnit(kony.flex.DP);
var lblNotes = new kony.ui.Label({
"id": "lblNotes",
"isVisible": true,
"right": "5%",
"skin": "sknsegmentHeaderText",
"text": "Notes:",
"top": "13dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var transactionNotes = new kony.ui.Label({
"bottom": "15dp",
"id": "transactionNotes",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "Note details appear here. This can be a muliple line descripsion",
"top": "5dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var divider3 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "divider3",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
divider3.setDefaultUnit(kony.flex.DP);
divider3.add();
flexNotesDetails.add(lblNotes, transactionNotes, divider3);
var FlexContainerConfirmandEditBtns = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "FlexContainerConfirmandEditBtns",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
FlexContainerConfirmandEditBtns.setDefaultUnit(kony.flex.DP);
var lblCashWithdrawCode = new kony.ui.Label({
"centerX": "50%",
"id": "lblCashWithdrawCode",
"isVisible": true,
"skin": "sknPendingLabel",
"text": "Withdraw cash code:",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "12dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblPassCode = new kony.ui.Label({
"centerX": "50%",
"id": "lblPassCode",
"isVisible": true,
"skin": "sknPassCodeLabel",
"text": "09889809",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblExpiryTime = new kony.ui.Label({
"centerX": "50%",
"id": "lblExpiryTime",
"isVisible": true,
"skin": "sknErrorMessageEC223BKA",
"text": "Expires in 21h:22m",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxShareOptions = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "40dp",
"id": "flxShareOptions",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"skin": "slFbox",
"top": "10dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxShareOptions.setDefaultUnit(kony.flex.DP);
var lblShareCode = new kony.ui.Label({
"centerX": "35%",
"centerY": "50%",
"id": "lblShareCode",
"isVisible": true,
"skin": "sknPendingLabel",
"text": "Share withdraw code",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var FlexContainer0cab5cb4a249945 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "80%",
"id": "FlexContainer0cab5cb4a249945",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "skncontainerBkgWhite",
"width": "10%",
"zIndex": 1
}, {}, {});
FlexContainer0cab5cb4a249945.setDefaultUnit(kony.flex.DP);
var imgMessageAck = new kony.ui.Image2({
"centerX": "50%",
"centerY": "50%",
"height": "100%",
"id": "imgMessageAck",
"isVisible": true,
"skin": "slImage",
"src": "mail_icon.png",
"width": "100%",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
FlexContainer0cab5cb4a249945.add(imgMessageAck);
var CopyFlexContainer0a08d08aacc6145 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "80%",
"id": "CopyFlexContainer0a08d08aacc6145",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "skncontainerBkgWhite",
"width": "10%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0a08d08aacc6145.setDefaultUnit(kony.flex.DP);
var imgMailAck = new kony.ui.Image2({
"centerX": "50%",
"centerY": "50%",
"height": "100%",
"id": "imgMailAck",
"isVisible": true,
"skin": "slImage",
"src": "messageack_icon.png",
"width": "100%",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
CopyFlexContainer0a08d08aacc6145.add(imgMailAck);
flxShareOptions.add( CopyFlexContainer0a08d08aacc6145, FlexContainer0cab5cb4a249945,lblShareCode);
var flxBottomAck = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "65dp",
"id": "flxBottomAck",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"skin": "slFbox",
"top": "20dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxBottomAck.setDefaultUnit(kony.flex.DP);
var flxWatchDemo = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "70%",
"id": "flxWatchDemo",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "9%",
"skin": "sknBottomFlex",
"top": "6%",
"width": "40%",
"zIndex": 1
}, {}, {});
flxWatchDemo.setDefaultUnit(kony.flex.DP);
var imgPlay = new kony.ui.Image2({
"centerY": "50%",
"height": "65%",
"id": "imgPlay",
"isVisible": true,
"right": "5dp",
"skin": "slImage",
"src": "play_blue.png",
"top": "12dp",
"width": "25%",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var lblWatchDemo = new kony.ui.Label({
"centerY": "50%",
"id": "lblWatchDemo",
"isVisible": true,
"right": "4%",
"left": "5dp",
"skin": "sknLabelPendingColors",
"text": "Watch demo video",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "80dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 5, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxWatchDemo.add( lblWatchDemo,imgPlay);
var flxFindATM = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "70%",
"id": "flxFindATM",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": 9,
"skin": "sknBottomFlex",
"top": "6%",
"width": "40%",
"zIndex": 1
}, {}, {});
flxFindATM.setDefaultUnit(kony.flex.DP);
var imgLocation = new kony.ui.Image2({
"centerY": "50%",
"height": "65%",
"id": "imgLocation",
"isVisible": true,
"right": "5dp",
"skin": "slImage",
"src": "placeholder_blue.png",
"top": "12dp",
"width": "25%",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var lblFindATM = new kony.ui.Label({
"centerY": "50%",
"id": "lblFindATM",
"isVisible": true,
"right": "4%",
"left": "5dp",
"skin": "sknLabelPendingColors",
"text": "Find near by ATM",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "75dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 10, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxFindATM.add( lblFindATM,imgLocation);
flxBottomAck.add( flxFindATM,flxWatchDemo);
var Copydivider0d9dbf93f2ab844 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider0d9dbf93f2ab844",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
Copydivider0d9dbf93f2ab844.setDefaultUnit(kony.flex.DP);
Copydivider0d9dbf93f2ab844.add();
var FlexContainer0be1121ad10c545 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "60dp",
"id": "FlexContainer0be1121ad10c545",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
FlexContainer0be1121ad10c545.setDefaultUnit(kony.flex.DP);
var lblCancelWithdraw = new kony.ui.Label({
"centerX": "50%",
"centerY": "48%",
"id": "lblCancelWithdraw",
"isVisible": true,
"skin": "sknLabelPendingColors",
"text": "Cancel Withdraw",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
FlexContainer0be1121ad10c545.add(lblCancelWithdraw);
FlexContainerConfirmandEditBtns.add(lblCashWithdrawCode, lblPassCode, lblExpiryTime, flxShareOptions, flxBottomAck, Copydivider0d9dbf93f2ab844, FlexContainer0be1121ad10c545);
FlexScrollContainerConfirmWithdraw.add(FlexContainerWithdrawDetails, fromFlxDetails, flxTransactionDetails, flexNotesDetails, FlexContainerConfirmandEditBtns);
frmPendingWithdrawSummary.add(titleBarWrapper, FlexScrollContainerConfirmWithdraw);
};
function frmPendingWithdrawSummaryGlobalsAr() {
frmPendingWithdrawSummaryAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmPendingWithdrawSummaryAr,
"bounces": false,
"enableScrolling": false,
"enabledForIdleTimeout": true,
"id": "frmPendingWithdrawSummary",
"layoutType": kony.flex.FLOW_VERTICAL,
"needAppMenu": false,
"skin": "sknmainGradient"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FLOW_VERTICAL,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"retainScrollPosition": false,
"titleBar": true,
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
