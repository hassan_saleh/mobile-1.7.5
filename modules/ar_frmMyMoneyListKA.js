//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmMyMoneyListKAAr() {
frmMyMoneyListKA.setDefaultUnit(kony.flex.DP);
var topWrapper = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "topWrapper",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"skin": "CopysknslFbox0b60a9222667f44",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
topWrapper.setDefaultUnit(kony.flex.DP);
var lblNumberKA = new kony.ui.Label({
"height": "20dp",
"id": "lblNumberKA",
"isVisible": false,
"right": "5dp",
"skin": "CopyslLabel0f7ca8453c24243",
"text": "18",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "13dp",
"width": "20dp",
"zIndex": 10
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxForTappingKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxForTappingKA",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"onTouchEnd": AS_FlexContainer_8bb76ba18725429783633f967893bdb2,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
flxForTappingKA.setDefaultUnit(kony.flex.DP);
flxForTappingKA.add();
var lblTitleLabel = new kony.ui.Label({
"height": "90%",
"id": "lblTitleLabel",
"isVisible": true,
"left": "20%",
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.common.statistics"),
"top": "0%",
"width": "60%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_j4ed910cdef6425fa733ea9eab143516,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 10
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"centerY": "50%",
"height": "100%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblBack0c15b1f41de0b41 = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"centerY": "50%",
"height": "100%",
"id": "CopylblBack0c15b1f41de0b41",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBack.add(lblBackIcon, CopylblBack0c15b1f41de0b41);
topWrapper.add(lblNumberKA, flxForTappingKA, lblTitleLabel, flxBack);
var mainContentSearch = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "100%",
"id": "mainContentSearch",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "skncontainerBkg",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
mainContentSearch.setDefaultUnit(kony.flex.DP);
var searchSegmentedControllerWrapper = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "56dp",
"id": "searchSegmentedControllerWrapper",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
searchSegmentedControllerWrapper.setDefaultUnit(kony.flex.DP);
var androidSegmentedController = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "56dp",
"id": "androidSegmentedController",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkg",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
androidSegmentedController.setDefaultUnit(kony.flex.DP);
var btnAccountsKA = new kony.ui.Button({
"focusSkin": "skntabSelected",
"height": "52dp",
"id": "btnAccountsKA",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_f11c52c881264104a131646191b0b963,
"skin": "skntabSelected",
"text": kony.i18n.getLocalizedString("i18n.my_money.btnAccounts"),
"top": "0dp",
"width": "33.30%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnSpendingKA = new kony.ui.Button({
"focusSkin": "skntabSelected",
"height": "52dp",
"id": "btnSpendingKA",
"isVisible": true,
"right": "33.30%",
"onClick": AS_Button_c263b57f9fb3479a955fbddbac988f97,
"skin": "skntabDeselected",
"text": kony.i18n.getLocalizedString("i18n.my_money.btnSpending"),
"top": "0dp",
"width": "33.40%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 6, 0,6, 0],
"paddingInPixel": false
}, {});
var btnBudgetKA = new kony.ui.Button({
"focusSkin": "skntabSelected",
"height": "52dp",
"id": "btnBudgetKA",
"isVisible": true,
"right": "66.70%",
"onClick": AS_Button_6b806ef30f994fe58d33f7bed63f5536,
"skin": "skntabDeselected",
"text": kony.i18n.getLocalizedString("i18n.my_money.btnBudget"),
"top": "0dp",
"width": "33.30%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 6, 0,6, 0],
"paddingInPixel": false
}, {});
var flxSelectedKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "5dp",
"clipBounds": true,
"height": "5dp",
"id": "flxSelectedKA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknCopyslFbox031e643cbc0cd4b",
"width": "33.30%",
"zIndex": 1
}, {}, {});
flxSelectedKA.setDefaultUnit(kony.flex.DP);
flxSelectedKA.add();
androidSegmentedController.add(btnAccountsKA, btnSpendingKA, btnBudgetKA, flxSelectedKA);
searchSegmentedControllerWrapper.add(androidSegmentedController);
var flxResultAccountsKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxResultAccountsKA",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "skncontainerBkg",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxResultAccountsKA.setDefaultUnit(kony.flex.DP);
var segAccountsKA = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"data": [{
"amountAccount1": "$00.00",
"amountcurrBal": "$00.00",
"amtOutsatndingBal": "$00.00",
"dummyAccountName": "Account Name",
"dummyAccountNumber": "",
"isPFMLabel": "",
"lblBankName": "",
"lblColorKA": "Label",
"nameAccount1": "Account Name",
"typeAccount": "Available Balance",
"typeKA": kony.i18n.getLocalizedString("i18n.deposit.availableBalance")
}, {
"amountAccount1": "$00.00",
"amountcurrBal": "$00.00",
"amtOutsatndingBal": "$00.00",
"dummyAccountName": "Account Name",
"dummyAccountNumber": "",
"isPFMLabel": "",
"lblBankName": "",
"lblColorKA": "Label",
"nameAccount1": "Account Name",
"typeAccount": "Available Balance",
"typeKA": kony.i18n.getLocalizedString("i18n.deposit.availableBalance")
}, {
"amountAccount1": "$00.00",
"amountcurrBal": "$00.00",
"amtOutsatndingBal": "$00.00",
"dummyAccountName": "Account Name",
"dummyAccountNumber": "",
"isPFMLabel": "",
"lblBankName": "",
"lblColorKA": "Label",
"nameAccount1": "Account Name",
"typeAccount": "Available Balance",
"typeKA": kony.i18n.getLocalizedString("i18n.deposit.availableBalance")
}],
"groupCells": false,
"height": "80%",
"id": "segAccountsKA",
"isVisible": true,
"right": "0dp",
"needPageIndicator": true,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowSkin": "seg2Normal",
"rowTemplate": yourAccount1,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "f7f7f700",
"separatorRequired": true,
"separatorThickness": 2,
"showScrollbars": true,
"top": "10dp",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"amountAccount1": "amountAccount1",
"amountcurrBal": "amountcurrBal",
"amtOutsatndingBal": "amtOutsatndingBal",
"colorAccount1": "colorAccount1",
"dummyAccountName": "dummyAccountName",
"dummyAccountNumber": "dummyAccountNumber",
"isPFMLabel": "isPFMLabel",
"lblBankName": "lblBankName",
"lblColorKA": "lblColorKA",
"nameAccount1": "nameAccount1",
"nameContainer": "nameContainer",
"typeAccount": "typeAccount",
"typeKA": "typeKA",
"yourAccount1": "yourAccount1"
},
"width": "90%",
"zIndex": 1
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var LabelNoRecordsKA = new kony.ui.Label({
"centerX": "50%",
"centerY": "35.519999999999996%",
"id": "LabelNoRecordsKA",
"isVisible": true,
"right": "17.52%",
"skin": "skn383838LatoRegular107KA",
"text": kony.i18n.getLocalizedString("i18n.MyMoney.noPFMAccounts"),
"top": "30dp",
"width": "65%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxResultAccountsKA.add(segAccountsKA, LabelNoRecordsKA);
var flxResultSpendingKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxResultSpendingKA",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "skncontainerBkg",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxResultSpendingKA.setDefaultUnit(kony.flex.DP);
var flxAllLabelsKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "40dp",
"id": "flxAllLabelsKA",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAllLabelsKA.setDefaultUnit(kony.flex.DP);
var flxMonthlylblKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "40dp",
"id": "flxMonthlylblKA",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0dp",
"onTouchStart": AS_FlexContainer_398db20872664c47929ed9d89aca85a6,
"skin": "slFbox",
"top": "0dp",
"width": "138dp",
"zIndex": 1
}, {}, {});
flxMonthlylblKA.setDefaultUnit(kony.flex.DP);
var resourcesLabel = new kony.ui.Label({
"height": "40dp",
"id": "resourcesLabel",
"isVisible": true,
"right": "15dp",
"skin": "sknaccountName",
"text": kony.i18n.getLocalizedString("i18n.my_money.monthlySpending"),
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxMonthlylblKA.add(resourcesLabel);
var flxJanMnthKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "40dp",
"id": "flxJanMnthKA",
"isVisible": false,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0dp",
"onClick": AS_FlexContainer_7ab4a4f3e3ca4c81b8fc17059da95f58,
"skin": "slFbox",
"top": "0dp",
"width": "70dp",
"zIndex": 1
}, {}, {});
flxJanMnthKA.setDefaultUnit(kony.flex.DP);
var lblMonthlySpendingKA = new kony.ui.Label({
"height": "40dp",
"id": "lblMonthlySpendingKA",
"isVisible": true,
"right": "0dp",
"skin": "sknaccountName",
"text": "> January",
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxJanMnthKA.add(lblMonthlySpendingKA);
var flxExpenditureKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "40dp",
"id": "flxExpenditureKA",
"isVisible": false,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {}, {});
flxExpenditureKA.setDefaultUnit(kony.flex.DP);
var lblExpenditureKA = new kony.ui.Label({
"height": "40dp",
"id": "lblExpenditureKA",
"isVisible": true,
"right": "0dp",
"skin": "sknaccountName",
"text": "> Shopping",
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxExpenditureKA.add(lblExpenditureKA);
flxAllLabelsKA.add( flxExpenditureKA, flxJanMnthKA,flxMonthlylblKA);
var flxSpendingOverviewKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "300dp",
"id": "flxSpendingOverviewKA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%"
}, {}, {});
flxSpendingOverviewKA.setDefaultUnit(kony.flex.DP);
var imgSpendingOverviewKA = new kony.ui.Image2({
"height": "100%",
"id": "imgSpendingOverviewKA",
"isVisible": false,
"right": "0dp",
"skin": "slImage",
"src": "spendingoverview.png",
"top": "0dp",
"width": "100%"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var flxSpendingOverviewKAParent = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "100%",
"id": "flxSpendingOverviewKAParent",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "1%",
"left": "1%",
"skin": "sknCopyslFbox07d05709853a74d",
"top": "10dp",
"width": "98%",
"zIndex": 1
}, {}, {});
flxSpendingOverviewKAParent.setDefaultUnit(kony.flex.DP);
var flxSpendingOverviewKAinner = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "93%",
"id": "flxSpendingOverviewKAinner",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "7%",
"left": "7%",
"skin": "sknCopyslFbox07d05709853a74d",
"top": "0dp",
"width": "86%",
"zIndex": 1
}, {}, {});
flxSpendingOverviewKAinner.setDefaultUnit(kony.flex.DP);
flxSpendingOverviewKAinner.add();
var LabelNoRecordsSpendingKA = new kony.ui.Label({
"centerX": "50%",
"centerY": "35.519999999999996%",
"id": "LabelNoRecordsSpendingKA",
"isVisible": true,
"right": "17.52%",
"skin": "skn383838LatoRegular107KA",
"text": "You do not have any records",
"top": "30dp",
"width": "65%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxSpendingOverviewKAParent.add(flxSpendingOverviewKAinner, LabelNoRecordsSpendingKA);
flxSpendingOverviewKA.add(imgSpendingOverviewKA, flxSpendingOverviewKAParent);
var flxMonthlySpendingKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "450dp",
"id": "flxMonthlySpendingKA",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%"
}, {}, {});
flxMonthlySpendingKA.setDefaultUnit(kony.flex.DP);
var CopyimgSpendingOverviewKA002e64031a2bc47 = new kony.ui.Image2({
"height": "100%",
"id": "CopyimgSpendingOverviewKA002e64031a2bc47",
"isVisible": false,
"right": "0dp",
"skin": "slImage",
"src": "spendingmonthly.png",
"top": "0dp",
"width": "100%"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var flxMonthlySpendingKAinner = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "60%",
"id": "flxMonthlySpendingKAinner",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"left": "0%",
"skin": "sknCopyslFbox07d05709853a74d",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxMonthlySpendingKAinner.setDefaultUnit(kony.flex.DP);
flxMonthlySpendingKAinner.add();
var flxScrollSegmentLegend = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": true,
"allowVerticalBounce": true,
"bounces": true,
"clipBounds": true,
"enableScrolling": true,
"height": "35%",
"horizontalScrollIndicator": true,
"id": "flxScrollSegmentLegend",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_HORIZONTAL,
"skin": "slFSbox",
"top": "0dp",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
flxScrollSegmentLegend.setDefaultUnit(kony.flex.DP);
var segLegendKA = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"data": [{
"CategoryName": "Payment to City of Austin",
"lblBoxKA": "",
"lblSepKA": "Label"
}, {
"CategoryName": "Payment to City of Austin",
"lblBoxKA": "",
"lblSepKA": "Label"
}, {
"CategoryName": "Payment to City of Austin",
"lblBoxKA": "",
"lblSepKA": "Label"
}],
"groupCells": false,
"height": "100%",
"id": "segLegendKA",
"isVisible": true,
"right": "0dp",
"needPageIndicator": true,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "seg2Focus",
"rowSkin": "seg2Normal",
"rowTemplate": CopyFlexContainer07124cb64fc4f45,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "ffffff00",
"separatorRequired": true,
"separatorThickness": 0,
"showScrollbars": false,
"top": "0dp",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"CategoryName": "CategoryName",
"CopyFlexContainer07124cb64fc4f45": "CopyFlexContainer07124cb64fc4f45",
"lblBoxKA": "lblBoxKA",
"lblSepKA": "lblSepKA"
},
"width": "100%"
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxScrollSegmentLegend.add(segLegendKA);
flxMonthlySpendingKA.add(CopyimgSpendingOverviewKA002e64031a2bc47, flxMonthlySpendingKAinner, flxScrollSegmentLegend);
var flxsegMonthlyDataKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "450dp",
"id": "flxsegMonthlyDataKA",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_1ab136cfcdc8490c84e57bbd9bdb10e5,
"skin": "slFbox",
"top": "0dp",
"width": "100%"
}, {}, {});
flxsegMonthlyDataKA.setDefaultUnit(kony.flex.DP);
var transactionSegment = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"data": [{
"CopyImage055f33fb7444246": "right_chevron_icon.png",
"CopytransactionName0e3bdeb3f5a7f48": "Shopping at Shoppers Stop",
"transactionAmount": "$155.00",
"transactionDate": "Jan16, 2016 "
}, {
"CopyImage055f33fb7444246": "right_chevron_icon.png",
"CopytransactionName0e3bdeb3f5a7f48": "Shopping at Lifestyle,Austin",
"transactionAmount": "$155.00",
"transactionDate": "Jan10, 2016 "
}, {
"CopyImage055f33fb7444246": "right_chevron_icon.png",
"CopytransactionName0e3bdeb3f5a7f48": "LG Electronics,Austin",
"transactionAmount": "$155.00",
"transactionDate": "Jan08, 2016 "
}, {
"CopyImage055f33fb7444246": "right_chevron_icon.png",
"CopytransactionName0e3bdeb3f5a7f48": "e-mart",
"transactionAmount": "$155.00",
"transactionDate": "Jan01, 2016 "
}],
"groupCells": false,
"id": "transactionSegment",
"isVisible": true,
"right": "0dp",
"needPageIndicator": true,
"onRowClick": AS_Segment_c2d7bca5adb943639b06db8cf59417e3,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "seg2Focus",
"rowSkin": "seg2Normal",
"rowTemplate": CopyFlexContainer0c9f1eddbc7f547,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "ffffff00",
"separatorRequired": true,
"separatorThickness": 0,
"showScrollbars": false,
"top": "0dp",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"CopyFlexContainer02080b70129e248": "CopyFlexContainer02080b70129e248",
"CopyImage055f33fb7444246": "CopyImage055f33fb7444246",
"CopytransactionName0e3bdeb3f5a7f48": "CopytransactionName0e3bdeb3f5a7f48",
"listDivider": "listDivider",
"transactionAmount": "transactionAmount",
"transactionDate": "transactionDate"
},
"width": "100%"
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var LabelNoRecordsTransactnKA = new kony.ui.Label({
"centerX": "50%",
"centerY": "35.519999999999996%",
"id": "LabelNoRecordsTransactnKA",
"isVisible": true,
"right": "17.52%",
"skin": "skn383838LatoRegular107KA",
"text": kony.i18n.getLocalizedString("i18n.common.NoTransactions"),
"top": "30dp",
"width": "65%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxsegMonthlyDataKA.add(transactionSegment, LabelNoRecordsTransactnKA);
flxResultSpendingKA.add(flxAllLabelsKA, flxSpendingOverviewKA, flxMonthlySpendingKA, flxsegMonthlyDataKA);
var flxResultBudgetKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxResultBudgetKA",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "skncontainerBkg",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxResultBudgetKA.setDefaultUnit(kony.flex.DP);
var flxImgBudgetKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "350dp",
"id": "flxImgBudgetKA",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "10dp",
"width": "100%"
}, {}, {});
flxImgBudgetKA.setDefaultUnit(kony.flex.DP);
var imgBudgetKA = new kony.ui.Image2({
"height": "100%",
"id": "imgBudgetKA",
"isVisible": false,
"right": "0dp",
"skin": "slImage",
"src": "budget_bar.png",
"top": "0dp",
"width": "100%"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var segBudgetKA = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"centerX": "50%",
"data": [{
"ImgCategoryKA": "budget_furnish.png",
"fullColorKA": "Home Furnishing",
"lblBelowLabelKA": "$400 of $500 used",
"lblPercentageValKA": "80%",
"restofColorKA": " "
}, {
"ImgCategoryKA": "budget_office.png",
"fullColorKA": "Painting",
"lblBelowLabelKA": "$380 of $410 used",
"lblPercentageValKA": "85%",
"restofColorKA": " "
}, {
"ImgCategoryKA": "budget_painting.png",
"fullColorKA": "Office Setup",
"lblBelowLabelKA": "$280 of $550 used",
"lblPercentageValKA": "60%",
"restofColorKA": " "
}, {
"ImgCategoryKA": "budget_printer.png",
"fullColorKA": "Printing Job",
"lblBelowLabelKA": "$370 of $380 used",
"lblPercentageValKA": "95%",
"restofColorKA": " "
}],
"groupCells": false,
"id": "segBudgetKA",
"isVisible": true,
"right": "0dp",
"needPageIndicator": true,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowTemplate": flxBudgetListKA,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorRequired": false,
"showScrollbars": false,
"top": "10dp",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"ImgCategoryKA": "ImgCategoryKA",
"flxBottomLevelKA": "flxBottomLevelKA",
"flxBudgetListKA": "flxBudgetListKA",
"flxTopLevelKA": "flxTopLevelKA",
"fullColorKA": "fullColorKA",
"lblBelowLabelKA": "lblBelowLabelKA",
"lblPercentageValKA": "lblPercentageValKA",
"restofColorKA": "restofColorKA"
},
"width": "90%",
"zIndex": 1
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var LabelNoBudgetRecordsKA = new kony.ui.Label({
"centerX": "50%",
"centerY": "35.519999999999996%",
"id": "LabelNoBudgetRecordsKA",
"isVisible": false,
"right": "17.52%",
"skin": "skn383838LatoRegular107KA",
"text": "You do not have any records",
"top": "30dp",
"width": "65%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxImgBudgetKA.add(imgBudgetKA, segBudgetKA, LabelNoBudgetRecordsKA);
flxResultBudgetKA.add(flxImgBudgetKA);
mainContentSearch.add(searchSegmentedControllerWrapper, flxResultAccountsKA, flxResultSpendingKA, flxResultBudgetKA);
var flxGraphs = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxGraphs",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "10%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxGraphs.setDefaultUnit(kony.flex.DP);
var flxTab = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxTab",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxTab.setDefaultUnit(kony.flex.DP);
var flxContent = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"centerY": "50%",
"clipBounds": true,
"height": "65%",
"id": "flxContent",
"isVisible": false,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0dp",
"skin": "slFboxOuterRing",
"top": "0dp",
"width": "88%",
"zIndex": 1
}, {}, {});
flxContent.setDefaultUnit(kony.flex.DP);
var btnSpendPerMonth = new kony.ui.Button({
"focusSkin": "slButtonWhiteTabFocus",
"height": "100%",
"id": "btnSpendPerMonth",
"isVisible": true,
"right": "0",
"onClick": AS_Button_acec4d7f9c894f25929f584d5d373058,
"skin": "slButtonWhiteTab",
"text": kony.i18n.getLocalizedString("i18n.my_money.montlyspending"),
"top": "0dp",
"width": "50%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnAllAccount = new kony.ui.Button({
"focusSkin": "slButtonWhiteTabFocus",
"height": "100%",
"id": "btnAllAccount",
"isVisible": true,
"right": "0",
"onClick": AS_Button_e45160893b7e4428b3bdc1dafff97403,
"skin": "slButtonWhiteTabDisabled",
"text": kony.i18n.getLocalizedString("i18n.overview.allAccounts"),
"top": "0dp",
"width": "50%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxContent.add( btnAllAccount,btnSpendPerMonth);
flxTab.add(flxContent);
var flxStatisticAccountSelection = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxStatisticAccountSelection",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 3
}, {}, {});
flxStatisticAccountSelection.setDefaultUnit(kony.flex.DP);
var flxMonth = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "80%",
"id": "flxMonth",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10%",
"skin": "slFbox",
"top": "0%",
"width": "30%",
"zIndex": 1
}, {}, {});
flxMonth.setDefaultUnit(kony.flex.DP);
var flxMonthHolder = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "4%",
"centerX": "50%",
"clipBounds": true,
"height": "60%",
"id": "flxMonthHolder",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_c7eee294b7574d42b5ace1009b3949c1,
"skin": "slFbox",
"width": "100%",
"zIndex": 2
}, {}, {});
flxMonthHolder.setDefaultUnit(kony.flex.DP);
var lblMonth = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblMonth",
"isVisible": true,
"right": "2%",
"maxNumberOfLines": 1,
"skin": "sknLblNextDisabled",
"text": "Month",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "85%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblArrowMonth = new kony.ui.Label({
"centerY": "50%",
"id": "lblArrowMonth",
"isVisible": true,
"right": "80%",
"skin": "sknBackIconDisabled",
"text": "d",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxMonthHolder.add(lblMonth, lblArrowMonth);
var flxUnderlineMonth = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "3%",
"id": "flxUnderlineMonth",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineMonth.setDefaultUnit(kony.flex.DP);
flxUnderlineMonth.add();
flxMonth.add(flxMonthHolder, flxUnderlineMonth);
var flxAccount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "80%",
"id": "flxAccount",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "45%",
"onClick": AS_FlexContainer_d417f271697b46a396136fb321522185,
"skin": "slFbox",
"top": "0%",
"width": "45%",
"zIndex": 1
}, {}, {});
flxAccount.setDefaultUnit(kony.flex.DP);
var flxAccountHolder = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "4%",
"centerX": "50%",
"clipBounds": true,
"height": "60%",
"id": "flxAccountHolder",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_b846ee30aa7c4402a70f25e14227cb38,
"skin": "slFbox",
"width": "100%",
"zIndex": 2
}, {}, {});
flxAccountHolder.setDefaultUnit(kony.flex.DP);
var lblAccount = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblAccount",
"isVisible": true,
"right": "2%",
"maxNumberOfLines": 1,
"skin": "sknLblNextDisabled",
"text": "Account",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "85%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblArrowAccount = new kony.ui.Label({
"centerY": "50%",
"id": "lblArrowAccount",
"isVisible": true,
"right": "85%",
"skin": "sknBackIconDisabled",
"text": "d",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxAccountHolder.add(lblAccount, lblArrowAccount);
var flxUnderlineAccount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "3%",
"id": "flxUnderlineAccount",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineAccount.setDefaultUnit(kony.flex.DP);
flxUnderlineAccount.add();
flxAccount.add(flxAccountHolder, flxUnderlineAccount);
flxStatisticAccountSelection.add(flxMonth, flxAccount);
var flxPieChart = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "80%",
"id": "flxPieChart",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "10%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxPieChart.setDefaultUnit(kony.flex.DP);
var lblAccountTitile = new kony.ui.Label({
"centerX": "50%",
"id": "lblAccountTitile",
"isVisible": false,
"skin": "CopyslLabel0j87bdb29c3144b",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxPieChart.add(lblAccountTitile);
var flxLineChart = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "80%",
"id": "flxLineChart",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "10%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxLineChart.setDefaultUnit(kony.flex.DP);
flxLineChart.add();
var lblNoChart = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "lblNoChart",
"isVisible": false,
"skin": "CopyslLabel0j87bdb29c3144b",
"text": kony.i18n.getLocalizedString("i18n.common.Notransaction"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxGraphs.add(flxTab, flxStatisticAccountSelection, flxPieChart, flxLineChart, lblNoChart);
frmMyMoneyListKA.add(topWrapper, mainContentSearch, flxGraphs);
};
function frmMyMoneyListKAGlobalsAr() {
frmMyMoneyListKAAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmMyMoneyListKAAr,
"bounces": true,
"enableScrolling": false,
"enabledForIdleTimeout": true,
"id": "frmMyMoneyListKA",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"postShow": AS_Form_a7e2147f56044352bd3617efe1168d9f,
"preShow": AS_Form_dc224ad09c6d4c558953826240e0efca,
"skin": "sknmainGradient"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"inTransitionConfig": {
"formAnimation": 0
},
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_52d450c42f434ba3bcd4fa7977a61b1d,
"outTransitionConfig": {
"formAnimation": 0
},
"retainScrollPosition": false,
"titleBar": true,
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
