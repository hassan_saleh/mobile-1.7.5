//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function initializetmpTransactionStatusAr() {
    flxTransactionStatusSegDetailsAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "105dp",
        "id": "flxTransactionStatusSegDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox"
    }, {}, {});
    flxTransactionStatusSegDetails.setDefaultUnit(kony.flex.DP);
    var flxIcon1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "50dp",
        "id": "flxIcon1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "2%",
        "skin": "sknFlxToIcon",
        "width": "50dp",
        "zIndex": 1
    }, {}, {});
    flxIcon1.setDefaultUnit(kony.flex.DP);
    var lblInitial = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "47%",
        "height": "80%",
        "id": "lblInitial",
        "isVisible": true,
        "skin": "sknLblFromIcon",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxIcon1.add(lblInitial);
    var flxTransactionStatusDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxTransactionStatusDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "16%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "84%",
        "zIndex": 1
    }, {}, {});
    flxTransactionStatusDetails.setDefaultUnit(kony.flex.DP);
    var lblAmount = new kony.ui.Label({
        "centerY": "50%",
        "height": "27%",
        "id": "lblAmount",
        "isVisible": true,
        "right": "4%",
        "maxNumberOfLines": 1,
        "skin": "sknLblNextDisabled",
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "84%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAccountNumber = new kony.ui.Label({
        "centerY": "75%",
        "height": "27%",
        "id": "lblAccountNumber",
        "isVisible": true,
        "right": "4%",
        "maxNumberOfLines": 1,
        "skin": "sknLblNextDisabled",
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "84%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBeneficiaryName = new kony.ui.Label({
        "centerY": "25%",
        "id": "lblBeneficiaryName",
        "isVisible": true,
        "right": "4%",
        "maxNumberOfLines": 1,
        "skin": "sknLblBack",
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "71%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDate = new kony.ui.Label({
        "centerY": "25%",
        "id": "lblDate",
        "isVisible": true,
        "maxNumberOfLines": 1,
        "left": "1%",
        "skin": "sknLblBack",
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "24%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblRightIcon = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblRightIcon",
        "isVisible": true,
        "left": "3%",
        "skin": "sknLblBoj100",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.more"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "15%",
        "zIndex": 10
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxTransactionStatusDetails.add(lblAmount, lblAccountNumber, lblBeneficiaryName, lblDate, lblRightIcon);
    var contactListDivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "2%",
        "id": "contactListDivider",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "top": "99%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    contactListDivider.setDefaultUnit(kony.flex.DP);
    contactListDivider.add();
    flxTransactionStatusSegDetailsAr.add(flxIcon1, flxTransactionStatusDetails, contactListDivider);
}
