//Type your code here
function accountInfo(flow, infoData) {
    kony.print("dashboardInfoScreen:::" + flow);
    kony.print("customerAccountDetails.currentIndex" + JSON.stringify(customerAccountDetails.currentIndex));
    kony.print("accountsDashboardData:::" + JSON.stringify(kony.retailBanking.globalData.accountsDashboardData));
  
  frmAccountInfoKA.lblListInfoInfo.skin = "lblListOn"; 
  frmAccountInfoKA.imgDiv.setVisibility(true);
  frmAccountInfoKA.lblAccountOptions.skin = "lblFontSettIconsFont2";
  frmAccountInfoKA.imgDivSett.setVisibility(false);
  frmAccountInfoKA.flxNextPaymentDate.setVisibility(false);
  //frmAccountInfoKA.flxLoanPostPoneIcon.setVisibility(false); hassan 29/12/2019
  frmAccountInfoKA.flxLoanPostPoneNextPayment.setVisibility(false); //hassan 29/12/2019
  frmAccountInfoKA.imgDivSett.src = "searcha.png";
  frmAccountInfoKA.flxLoanInfo.top = "37%";
  //frmAccountInfoKA.imgDiv.centerY = "38.7%";
  frmAccountInfoKA.flxAccountOptions.setVisibility(false);
    var langSelected1 = kony.store.getItem("langPrefObj");
    if (flow === "account") {
        kony.print("account:::");
        var index = parseInt(customerAccountDetails.currentIndex) || 0;

        var accountData = kony.retailBanking.globalData.accountsDashboardData.accountsData[index];
        kony.print("accountData::::-" + JSON.stringify(accountData));
        if (accountData !== null) {
            kony.print("Inside accountData:::");
           if (frmAccountsLandingKA.segAccountsKA.data.length <= 1) {
                frmAccountInfoKA.flxPageIndicator.setVisibility(false);
            } else {
                frmAccountInfoKA.flxPageIndicator.setVisibility(true);
                create_PageIndicatorInfo(frmAccountInfoKA, 50, frmAccountsLandingKA.segAccountsKA.data.length, customerAccountDetails.currentIndex);
            }
            frmAccountInfoKA.androidBack.onClick = function() {
                kony.print("Account infoo clik");
                frmAccountDetailKA.flxDealsScreen.setVisibility(false);
                frmAccountDetailKA.flxLoansScreen.setVisibility(false);
                frmAccountDetailKA.accountDetailsScrollContainer.setVisibility(true);
                frmAccountDetailKA.accountDetailsScrollContainer.left = "0%";
				kony.retailBanking.globalData.globals.accountDashboardFlowFlag = false;
              accountDashboardDataCall();
            };
            frmAccountInfoKA.flxAccountNameMain.top = "0%";
            frmAccountInfoKA.flxAmountmain.setVisibility(true);
            frmAccountInfoKA.flxTabsWrapper.setVisibility(true);
        	frmAccountInfoKA.lblListOnInfo.setVisibility(true);
          	if(kony.store.getItem("langPrefObj") === "en"){
              frmAccountInfoKA.lblListInfoInfo.left = "40%";
              frmAccountInfoKA.lblAccountOptions.right = "9%";
              frmAccountInfoKA.imgDiv.left = "40%";
              frmAccountInfoKA.imgDivSett.right = "9%";
            }else{
              frmAccountInfoKA.lblListInfoInfo.right = "40%";
              frmAccountInfoKA.lblAccountOptions.left = "9%";
              frmAccountInfoKA.imgDiv.right = "40%";
              frmAccountInfoKA.imgDivSett.left = "9%";
            }
            //frmAccountInfoKA.flxaccountdetailsfordeposits.top = "195Dp";
            frmAccountInfoKA.imgDiv.setVisibility(true);
            var status;
            if (accountData.favouriteStatus !== null && accountData.favouriteStatus == "A") {
                status = geti18Value("i18n.accounts.active");
            } else {
                status = geti18Value("i18n.accounts.dormant");
            }
          
           var acccName = "";
      if(isEmpty(accountData["AccNickName"])){
        acccName = accountData["accountName"]
        if(acccName !== null && acccName !==""  ){
          if(acccName.length > 30)											
            acccName = acccName.substring(0,22)+ "  ...";
          acccName = acccName +" "+ accountData["accountID"];
       }
      }
      else{
        acccName = accountData["AccNickName"] ;
         if(acccName !== null && acccName !==""  ){
          if(acccName.length > 40)											
            acccName = acccName.substring(0,40)+ "  ...";

          } 
      }
            frmAccountInfoKA.accountNumberLabel.text = acccName;
            frmAccountInfoKA.CopyaccountNumberLabel0e1c2ed08e25c42.text =formatamountwithCurrency(accountData["availableBalance"],accountData["currencyCode"]);
			frmAccountInfoKA.lblCurrency.text = isEmpty(accountData["currencyCode"])?  "" : accountData["currencyCode"];
            frmAccountInfoKA.CopyinterestEarnedLabel00b76261e56e741.text = "";//"12 Jan 2018";
            frmAccountInfoKA.CopyinterestRateLabel0b14fb4af446e45.text =isEmpty(accountData["branchName"])?  "" : accountData["branchName"];
            frmAccountInfoKA.CopyinterestRateLabel0h623a08258c745.text = status;
            var iban = isEmpty(accountData["iban"])?  "" : accountData["iban"];
            frmAccountInfoKA.CopyinterestRateLabel0b24dd6f4438b4a.text = ibanNumberFormat(iban);
            frmAccountInfoKA.CopyaccountNumberLabel036c71ac511b84d.text =isEmpty(accountData["accountID"])?  "" : accountData["accountID"];
            frmAccountInfoKA.CopyinterestRateLabel0dbfe2220200e49.text = isEmpty(accountData["currencyCode"])?  "" : accountData["currencyCode"];
            frmAccountInfoKA.CopyinterestRateLabel09f06e1600f0e43.text = formatamountwithCurrency(accountData["availableBalance"],accountData["currencyCode"]) + " "+accountData["currencyCode"];
			
          	if(accountData["isjomopayacc"] == "N"){
             frmAccountInfoKA.lblMobile.isVisible = false;
                }else if(accountData["isjomopayacc"] == "Y"){
              frmAccountInfoKA.lblMobile.isVisible = true;
             }

          if(langSelected1 === "ar" || langSelected1 === "ara"){
            frmAccountInfoKA.CopyaccountNumberLabel0e1c2ed08e25c42.text = accountData["currencyCode"] + "  "+ formatamountwithCurrency(accountData["availableBalance"],accountData["currencyCode"]);

          }
          else{
            frmAccountInfoKA.CopyaccountNumberLabel0e1c2ed08e25c42.text =formatamountwithCurrency(accountData["availableBalance"],accountData["currencyCode"]) + "  " +accountData["currencyCode"];


          }

            frmAccountInfoKA.lblLoanAccountInfo.text = kony.i18n.getLocalizedString("i18n.accounts.accounts");
            frmAccountInfoKA.lblLoanAccountInfo.setVisibility(true);
            frmAccountInfoKA.mainContent.skin = "sknAccountsInfo";

            frmAccountInfoKA.flxaccountdetailsfordeposits.setVisibility(true);
            frmAccountInfoKA.flxLoanInfo.setVisibility(false);
            frmAccountInfoKA.flxDeposits.setVisibility(false);

            frmAccountInfoKA.flxNextPayment.setVisibility(false);
            frmAccountInfoKA.flxLoanPaymentAmount.setVisibility(false);

            infoType = "account";
			kony.retailBanking.globalData.globals.accountDashboardFlowFlag = false;
            getOpenDateForAccountInfo(accountData["reference_no"].toString(),accountData["accountID"], accountData["branchNumber"]);
            
        } else {
            kony.print("No account data:");
        }
    } else if (flow === "deals") {
        kony.print("deals:::");
        frmAccountInfoKA.androidBack.onClick = function() {
            kony.print("deals infoo clik");
            frmAccountsLandingKA.flxDeals.setVisibility(true);
            frmAccountsLandingKA.flxAccountsMain.setVisibility(false);
            frmAccountsLandingKA.flxLoans.setVisibility(false);
            frmAccountsLandingKA.flxAccountsMain.zIndex = 1;
            frmAccountsLandingKA.flxDeals.zIndex = 1;
            frmAccountsLandingKA.flxLoans.zIndex = 1;
            frmAccountsLandingKA.forceLayout();
            accountDashboardDataCall();

        };
        frmAccountInfoKA.flxAccountNameMain.top = "2%";
        frmAccountInfoKA.flxAmountmain.setVisibility(false);
        frmAccountInfoKA.flxTabsWrapper.setVisibility(false);
        frmAccountInfoKA.imgDiv.setVisibility(false);
        //frmAccountInfoKA.accountNumberLabel.text = "Term deposit account ***654";
        frmAccountInfoKA.lblLoanAccountInfo.text = kony.i18n.getLocalizedString("i18n.accounts.deals");
        frmAccountInfoKA.lblLoanAccountInfo.setVisibility(true);
        frmAccountInfoKA.mainContent.skin = "sknDealsflxSrl";

        frmAccountInfoKA.flxaccountdetailsfordeposits.setVisibility(false);
        frmAccountInfoKA.flxLoanInfo.setVisibility(false);
        frmAccountInfoKA.flxDeposits.setVisibility(true);

        frmAccountInfoKA.lblPaymentAmount.text = kony.i18n.getLocalizedString("i18.deposit.Interestaccount");
        frmAccountInfoKA.lblNextPayment.text = kony.i18n.getLocalizedString("i18.deposit.Nextinterestdate");



        frmAccountInfoKA.flxNextPayment.setVisibility(true);
        frmAccountInfoKA.flxLoanPaymentAmount.setVisibility(true);

        //frmAccountInfoKA.androidBack.onClick
        infoType = "deal";
    	kony.print("Deals Response ::"+JSON.stringify(infoData));
		if (infoData !== null && infoData.length > 0) {
           var index1 = parseInt(DealsLoansCurrIndex) || 0;

        var dealsData = kony.retailBanking.globalData.accountsDashboardData.dealsData[index1];
            frmAccountInfoKA.CopylblLoanType0dc1c0e442dc34b.text =isEmpty(infoData[0]["Deposite amount"])? "" : (formatamountwithCurrency(infoData[0]["Deposite amount"],gblAccountsCurrencyType) + " " + gblAccountsCurrencyType);
            frmAccountInfoKA.lblLoanType1.text =isEmpty(infoData[0]["Deposite Type"])? "" : infoData[0]["Deposite Type"];
          	frmAccountInfoKA.lblTDDepositNumber.text =isEmpty(dealsData["reference_no"])? "" : dealsData["reference_no"];
            frmAccountInfoKA.CopyinterestEarnedLabel0daf983bec94d4c.text = getShortMonthDateFormat(infoData[0]["Maturity Date"]);
            frmAccountInfoKA.lblInterestDate1.text = getShortMonthDateFormat(infoData[0]["Interest date"]);
        	frmAccountInfoKA.lblNextInterestDate1.text = isEmpty(infoData[0]["interestAmount"])?"":(formatamountwithCurrency(infoData[0]["interestAmount"],gblAccountsCurrencyType) + " " + gblAccountsCurrencyType);
        	frmAccountInfoKA.lblInterestRateValue.text = isEmpty(infoData[0]["interestRate"])?"":infoData[0]["interestRate"];
            frmAccountInfoKA.lblNextPayment1.text = getShortMonthDateFormat(infoData[0]["Next interest date"]);
            frmAccountInfoKA.CopyaccountNumberLabel014bda555f8c040.text = isEmpty(infoData[0]["Interest account"])? "" : infoData[0]["Interest account"];
            frmAccountInfoKA.CopyaccountNumberLabel014bda555f8c040.skin = sknWhiteNew30Dep; 
            frmAccountInfoKA.CopyaccountNumberLabel014bda555f8c040.top = "27dp";
          //frmAccountInfoKA.accountNumberLabel.text = infoData[0]["Deposite name"] + " ***" + infoData[0]["Deposite number"].substring((infoData[0]["Deposite number"].length) - 3, infoData[0]["Deposite number"].length)
        }
		kony.retailBanking.globalData.globals.accountDashboardFlowFlag = false;
        frmAccountInfoKA.show();
    } else if (flow === "loans") {
        kony.print("loans:::");
        frmAccountInfoKA.androidBack.onClick = function() {
            kony.print("loans infoo clik");
            frmAccountsLandingKA.flxDeals.setVisibility(false);
            frmAccountsLandingKA.flxAccountsMain.setVisibility(false);
            frmAccountsLandingKA.flxLoans.setVisibility(true);
            frmAccountsLandingKA.flxAccountsMain.zIndex = 1;
            frmAccountsLandingKA.flxDeals.zIndex = 1;
            frmAccountsLandingKA.flxLoans.zIndex = 1;
            frmAccountsLandingKA.forceLayout();
            accountDashboardDataCall();
        };
    	frmAccountInfoKA.lblListInfoInfo.setVisibility(false);
  		frmAccountInfoKA.imgDiv.setVisibility(false);
    	frmAccountInfoKA.flxLoanInfo.top = "23%";
        frmAccountInfoKA.flxAccountNameMain.top = "2%";
        frmAccountInfoKA.flxAmountmain.setVisibility(false);
        frmAccountInfoKA.flxTabsWrapper.setVisibility(true);
    	frmAccountInfoKA.lblListOnInfo.setVisibility(false);
        frmAccountInfoKA.flxNextPaymentDate.setVisibility(true);
    	if(kony.retailBanking.globalData.accountsDashboardData.loansData[parseInt(DealsLoansCurrIndex)].producttype === "221"){
        	// frmAccountInfoKA.flxLoanPostPoneIcon.setVisibility(true); hassan 29/1/2019
            frmAccountInfoKA.flxLoanPostPoneNextPayment.setVisibility(true); //hassan 29/1/2019
        }
        /*hassan loan postpone*/
       /* if (kony.retailBanking.globalData.accountsDashboardData.loansData[parseInt(DealsLoansCurrIndex)].producttype === "221" ||
            kony.retailBanking.globalData.accountsDashboardData.loansData[parseInt(DealsLoansCurrIndex)].producttype === "225" ||  
            kony.retailBanking.globalData.accountsDashboardData.loansData[parseInt(DealsLoansCurrIndex)].producttype === "226"){
            frmAccountInfoKA.lblAccountOptions.setVisibility(true);
            if(kony.store.getItem("langPrefObj") === "en"){
              frmAccountInfoKA.lblListInfoInfo.left = "15%";
              frmAccountInfoKA.lblAccountOptions.right = "15%";
              frmAccountInfoKA.imgDiv.left = "15%";
              frmAccountInfoKA.imgDivSett.right = "15%";
          }else{
              frmAccountInfoKA.lblListInfoInfo.right = "15%";
              frmAccountInfoKA.lblAccountOptions.left = "15%";
              frmAccountInfoKA.imgDiv.right = "15%";
              frmAccountInfoKA.imgDivSett.left = "15%";
          } 
        }else{*/
            frmAccountInfoKA.lblAccountOptions.setVisibility(false);
            if(kony.store.getItem("langPrefObj") === "en"){
              frmAccountInfoKA.lblListInfoInfo.centerX = "50%";
              //frmAccountInfoKA.lblAccountOptions.right = "15%";
              frmAccountInfoKA.imgDiv.centerX = "50%";
              frmAccountInfoKA.imgDivSett.centerX = "50%";
          }else{
              frmAccountInfoKA.lblListInfoInfo.centerX = "50%";
              //frmAccountInfoKA.lblAccountOptions.left = "15%";
              frmAccountInfoKA.imgDiv.centerX = "50%";
              frmAccountInfoKA.imgDivSett.centerX = "50%";
          } 
        //}
        /*hassan loan postpone*/
      

    	frmAccountInfoKA.imgDiv.centerY = "39%";
    	frmAccountInfoKA.imgDivSett.src = "searchb.png";
        //frmAccountInfoKA.imgDiv.setVisibility(true);
        //frmAccountInfoKA.accountNumberLabel.text = "Car loan *** 454";
        frmAccountInfoKA.lblLoanAccountInfo.setVisibility(true);
        frmAccountInfoKA.lblLoanAccountInfo.text = kony.i18n.getLocalizedString("i18n.accounts.loans");
        frmAccountInfoKA.mainContent.skin = "sknscrollBkg";

        frmAccountInfoKA.flxaccountdetailsfordeposits.setVisibility(false);
        frmAccountInfoKA.flxLoanInfo.setVisibility(true);
        frmAccountInfoKA.flxDeposits.setVisibility(false);

        frmAccountInfoKA.lblPaymentAmount.text = kony.i18n.getLocalizedString("i18n.loans.paymentAmount");
        frmAccountInfoKA.lblNextPayment.text = kony.i18n.getLocalizedString("i18n.loans.nextPaymentOn");


        frmAccountInfoKA.flxNextPayment.setVisibility(true);
        frmAccountInfoKA.flxLoanPaymentAmount.setVisibility(true);

        //frmAccountInfoKA.androidBack.onClick
        infoType = "loan";
    	kony.print("Loans Response ::"+JSON.stringify(infoData));
    	if (infoData !== null && infoData.length > 0) {
          var index2 = parseInt(DealsLoansCurrIndex) || 0;

        var loansData = kony.retailBanking.globalData.accountsDashboardData.loansData[index2];
        	frmAccountInfoKA.lblLoanAmount.text = isEmpty(infoData[0]["Amount"])? "" : (formatamountwithCurrency(infoData[0]["Amount"],gblAccountsCurrencyType)+ " " + gblAccountsCurrencyType);
        	frmAccountInfoKA.lblInterestRateLoan.text =isEmpty(infoData[0]["Interest_rate"])? "" : infoData[0]["Interest_rate"];
          	frmAccountInfoKA.lblLoanAccNumber.text =isEmpty(loansData["reference_no"])? "" : loansData["reference_no"];
        	frmAccountInfoKA.lblFlatRate.text =isEmpty(infoData[0]["Flat_rate"])? "" : (formatamountwithCurrency(infoData[0]["Flat_rate"],gblAccountsCurrencyType)+ " " + gblAccountsCurrencyType);
          	frmAccountInfoKA.lblReducedInterestRate.text =isEmpty(infoData[0]["Reduced interest rate"])? "" : infoData[0]["Reduced interest rate"];
          	frmAccountInfoKA.CopyinterestRateLabel0bfae983d84904f.text =isEmpty(infoData[0]["Remaining_balance"])? "" : (formatamountwithCurrency(infoData[0]["Remaining_balance"],gblAccountsCurrencyType)+ " " + gblAccountsCurrencyType);
          	frmAccountInfoKA.lblStartingDateLoan.text = getShortMonthDateFormat(infoData[0]["Starting_Date"]);
          	frmAccountInfoKA.lblNextPayment1.text = getShortMonthDateFormat(infoData[0]["Next_payment_date"]);
        	frmAccountInfoKA.lblNextPaymentDate.text = getShortMonthDateFormat(infoData[0]["Next_payment_date"]);
          	frmAccountInfoKA.CopyaccountNumberLabel014bda555f8c040.text =isEmpty(infoData[0]["Payment_due"])? "" : (formatamountwithCurrency(infoData[0]["Payment_due"],gblAccountsCurrencyType)+ " " + gblAccountsCurrencyType);
             frmAccountInfoKA.CopyaccountNumberLabel014bda555f8c040.top = "24dp";
          frmAccountInfoKA.CopyaccountNumberLabel014bda555f8c040.skin = sknlblwhite40;
          kony.retailBanking.globalData.accountsDashboardData.loansData[index2].maturitydate = format_date(infoData[0]["Next_payment_date"], "DD/MM/YYYY");
        }
    	
		kony.retailBanking.globalData.globals.accountDashboardFlowFlag = false;
      frmAccountInfoKA.forceLayout();
        frmAccountInfoKA.show();
    }




}




function transactionMove() {
    kony.print("transactionMove:::");
    if (infoType == "account") {
       /* frmAccountDetailKA.flxDealsScreen.setVisibility(false);
        frmAccountDetailKA.flxLoansScreen.setVisibility(false);
        frmAccountDetailKA.accountDetailsScrollContainer.setVisibility(true);
        frmAccountDetailKA.accountDetailsScrollContainer.left = "0%";
        frmAccountDetailKA.show();*/
      var langSelected1 = kony.store.getItem("langPrefObj");
      frmAccountDetailKA.flxDealsScreen.setVisibility(false);
      frmAccountDetailKA.flxLoansScreen.setVisibility(false);
      frmAccountDetailKA.accountDetailsScrollContainer.setVisibility(true);
      frmAccountDetailKA.accountDetailsScrollContainer.left = "0%";
      kony.retailBanking.globalData.globals.accountDashboardFlowFlag = false;
      var index = parseInt(customerAccountDetails.currentIndex) || 0;
      var accountData = kony.retailBanking.globalData.accountsDashboardData.accountsData[index];
       if(langSelected1 === "ar" || langSelected1 === "ara"){
           frmAccountDetailKA.availableBalanceAmount.text = accountData["currencyCode"] + " " +formatamountwithCurrency(accountData["availableBalance"],accountData["currencyCode"]);
      		frmAccountDetailKA.availableBalanceAmountSecondary.text =  accountData["currencyCode"] + " " +formatamountwithCurrency(accountData["availableBalance"],accountData["currencyCode"]);
    
          }
          else{
            frmAccountDetailKA.availableBalanceAmount.text = formatamountwithCurrency(accountData["availableBalance"],accountData["currencyCode"])+ " " + accountData["currencyCode"];
      		frmAccountDetailKA.availableBalanceAmountSecondary.text =  formatamountwithCurrency(accountData["availableBalance"],accountData["currencyCode"])+ " " + accountData["currencyCode"];
      }

      var acccName = "";
      if(isEmpty(accountData["AccNickName"])){
        acccName = accountData["accountName"]
        if(acccName !== null && acccName !==""  ){
          if(acccName.length > 30)											
            acccName = acccName.substring(0,22)+ "  ...";
          acccName = acccName +" "+ accountData["accountID"];
       }
      }
      else{
        acccName = accountData["AccNickName"] ;
         if(acccName !== null && acccName !==""  ){
          if(acccName.length > 40)											
            acccName = acccName.substring(0,40)+ "  ...";

          } 
      }

      
      frmAccountDetailKA.lblavailbalance.text =acccName;
      frmAccountDetailKA.lblavailbalanceSecondary.text = acccName;
      create_PageIndicator(frmAccountDetailKA, 50, customerAccountDetails.length);
      fetch_TransactionList(kony.retailBanking.globalData.accountsDashboardData.accountsData[parseInt(customerAccountDetails.currentIndex)],{"toDATE":"","fromDATE":""});

           
    } else if (infoType == "deal") {
        frmAccountsLandingKA.show();
        dealsAnimation();
    } else if (infoType == "loan") {

        frmAccountsLandingKA.show();
        loansAnimation();
    }
}

function unclearedFunds() {

    // alert("in uncleared funds");
    if (flagUnclear === 0) {
        //alert("in uncleared funds 0");

        frmAccountDetailKA.flxExtra1.setVisibility(true);
        frmAccountDetailKA.flxExtra2.setVisibility(true);

        flagUnclear = 1;
    } else if (flagUnclear === 1) {
        // alert("in uncleared funds 1");

        frmAccountDetailKA.flxExtra1.setVisibility(false);
        frmAccountDetailKA.flxExtra2.setVisibility(false);

        flagUnclear = 0;
    }
}

function format_date(date, inputFormat, outputFormat){
	try{
    	outputFormat.replace("DD",date.substring(inputFormat.indexOf("D"), inputFormat.lastIndexOf("D")+1));
    	outputFormat.replace("MM",date.substring(inputFormat.indexOf("M"), inputFormat.lastIndexOf("M")+1));
    	outputFormat.replace("YYYY",date.substring(inputFormat.indexOf("Y"), inputFormat.lastIndexOf("Y")+1));
    	kony.print("formatted Date ::"+outputFormat);
    	return outputFormat;
    }catch(e){
    	kony.print("Exception_format_date ::"+e);
    }
}






function getOpenDateForAccountInfo(p_loan_ref_no,p_acc_No,branchNumber){
  kony.print("getOpenDateForAccountInfo:::");
  var langSelected1 = kony.store.getItem("langPrefObj");
    var langSelected = langSelected1.toUpperCase();
  var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
            var options = {
                "access": "online",
                "objectName": "RBObjects"
            };
            var headers = {};
            var serviceName = "RBObjects";
            var modelObj = INSTANCE.getModel("Accounts", serviceName, options);
            var dataObject = new kony.sdk.dto.DataObject("Accounts");
//             dataObject.addField("usr", "icbsserv");
//             dataObject.addField("pass", "icbsserv_1");
            dataObject.addField("custId", custid);
            dataObject.addField("p_loan_ref_no", p_loan_ref_no);
            dataObject.addField("p_acc_No", p_acc_No);
            dataObject.addField("p_Branch", branchNumber);
            dataObject.addField("p_acct_Type", "217");
            dataObject.addField("AccountFlag", "A");
            dataObject.addField("p_lan", langSelected);
            var serviceOptions = {
                "dataObject": dataObject,
                "headers": headers
            };
           
            kony.print("dataObjectLoans::" + JSON.stringify(dataObject));
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
            if (kony.sdk.isNetworkAvailable()) {
                modelObj.customVerb("getDetails", serviceOptions, getOpenDateForAccountInfoSuccess, getOpenDateForAccountInfoError);
            } else {
                kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
            }
}




function getOpenDateForAccountInfoSuccess(response){
  kony.print("getOpenDateForAccountInfoSuccess"+JSON.stringify(response));
  if (response.AccountDetail != undefined) {
  if (response.AccountDetail[0] != undefined) {
   frmAccountInfoKA.CopyinterestEarnedLabel00b76261e56e741.text = isEmpty(response.AccountDetail[0].openingdate) ? "" : getShortMonthDateFormat(response.AccountDetail[0].openingdate);
  }
  }
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    frmAccountInfoKA.show();
  
}


function getOpenDateForAccountInfoError(error){
  kony.print("getOpenDateForAccountInfoError"+JSON.stringify(error));
  kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
  frmAccountInfoKA.show();
   
}