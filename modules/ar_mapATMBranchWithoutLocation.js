//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function initializemapATMBranchWithoutLocationAr() {
    flxMapATMBranchWithoutLocationAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "280dp",
        "id": "flxMapATMBranchWithoutLocation",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "skin": "slFboxToolTipWithoutLocation",
        "width": "80%"
    }, {}, {});
    flxMapATMBranchWithoutLocation.setDefaultUnit(kony.flex.DP);
    var flxTopDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "86%",
        "id": "flxTopDetails",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0%",
        "skin": "slFbox",
        "top": "0%",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    flxTopDetails.setDefaultUnit(kony.flex.DP);
    var flxTop1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "17%",
        "id": "flxTop1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "1%",
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 5
    }, {}, {});
    flxTop1.setDefaultUnit(kony.flex.DP);
    var lblType = new kony.ui.Label({
        "centerY": "50%",
        "height": "90%",
        "id": "lblType",
        "isVisible": true,
        "right": "8%",
        "skin": "sknLblBlack100",
        "text": "B",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5%",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnClose = new kony.ui.Button({
        "centerY": "50%",
        "id": "btnClose",
        "isVisible": true,
        "left": "1%",
        "skin": "sknCloseBtndm",
        "top": "7dp",
        "width": "10%",
        "zIndex": 5
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    flxTop1.add(lblType, btnClose);
    var lblName = new kony.ui.Label({
        "id": "lblName",
        "isVisible": true,
        "right": "8%",
        "skin": "sknlblDblue145",
        "text": "Ras Al-Ain Branch",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "0%",
        "width": "78%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAddress = new kony.ui.Label({
        "id": "lblAddress",
        "isVisible": true,
        "right": "8%",
        "maxNumberOfLines": 1,
        "skin": "sknLblBlack100",
        "text": "Kondapur Kondapur ",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "0%",
        "width": "90%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblLat = new kony.ui.Label({
        "height": "15%",
        "id": "lblLat",
        "isVisible": false,
        "right": "8%",
        "maxNumberOfLines": 1,
        "skin": "sknLblBlack100",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "26%",
        "width": "84%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblLong = new kony.ui.Label({
        "height": "15%",
        "id": "lblLong",
        "isVisible": false,
        "right": "8%",
        "maxNumberOfLines": 1,
        "skin": "sknLblBlack100",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "36%",
        "width": "84%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblIcon1 = new kony.ui.Label({
        "height": "15%",
        "id": "lblIcon1",
        "isVisible": false,
        "right": "20dp",
        "maxNumberOfLines": 1,
        "skin": "sknLblBlack100",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "48%",
        "width": "30%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblIcon2 = new kony.ui.Label({
        "height": "15%",
        "id": "lblIcon2",
        "isVisible": false,
        "right": "20dp",
        "maxNumberOfLines": 1,
        "skin": "sknLblBlack100",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "65%",
        "width": "25%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnReserveQueue = new kony.ui.Button({
        "height": "15%",
        "id": "btnReserveQueue",
        "isVisible": false,
        "right": "20dp",
        "skin": "sknBtnBGWhiteBlueFace",
        "text": "Reserve Queue",
        "top": "48%",
        "width": "85%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    var flxBottom = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "20%",
        "id": "flxBottom",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "slFbox",
        "top": "5%",
        "width": "100%",
        "zIndex": 5
    }, {}, {});
    flxBottom.setDefaultUnit(kony.flex.DP);
    var btnDirections = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknBtnBGWhiteBlueFace",
        "height": "90%",
        "id": "btnDirections",
        "isVisible": true,
        "right": "8%",
        "skin": "sknBtnBGWhiteBlueFace",
        "text": kony.i18n.getLocalizedString("i18n.locateus.getDirections"),
        "top": "0%",
        "width": "85%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    var lblCarIcon = new kony.ui.Label({
        "centerY": "50%",
        "height": "90%",
        "id": "lblCarIcon",
        "isVisible": true,
        "right": "15%",
        "skin": "sknBOJttfDBlue145",
        "text": "U",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBottom.add(btnDirections, lblCarIcon);
    flxTopDetails.add(flxTop1, lblName, lblAddress, lblLat, lblLong, lblIcon1, lblIcon2, btnReserveQueue, flxBottom);
    flxMapATMBranchWithoutLocationAr.add(flxTopDetails);
}
