//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmSetLocatorDistanceFilterKAAr() {
    frmSetLocatorDistanceFilterKA.setDefaultUnit(kony.flex.DP);
    var androidTitleBar = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "androidTitleBar",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skntitleBarGradient",
        "top": "0dp",
        "width": "101%",
        "zIndex": 1
    }, {}, {});
    androidTitleBar.setDefaultUnit(kony.flex.DP);
    var androidTitleLabel = new kony.ui.Label({
        "centerY": "50%",
        "id": "androidTitleLabel",
        "isVisible": true,
        "right": "55dp",
        "skin": "sknnavBarTitle",
        "text": kony.i18n.getLocalizedString("i18n.search.advanceFilter"),
        "width": "64.16%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var androidBackButton = new kony.ui.Button({
        "focusSkin": "sknandroidBackButtonFocus",
        "height": "50dp",
        "id": "androidBackButton",
        "isVisible": true,
        "right": "0dp",
        "onClick": AS_Button_94027daf3beb4d8c8ecd644448f52962,
        "skin": "sknandroidBackButton",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    var btnAndroidDoneKA = new kony.ui.Button({
        "focusSkin": "sknRTButtonNormalKA",
        "height": "50dp",
        "id": "btnAndroidDoneKA",
        "isVisible": true,
        "onClick": AS_Button_3be9eafbda70496783cce641a183027c,
        "left": "12dp",
        "skin": "sknbtn",
        "text": kony.i18n.getLocalizedString("i18n.overview.buttonDone"),
        "top": "2dp",
        "width": "75dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    androidTitleBar.add(androidTitleLabel, androidBackButton, btnAndroidDoneKA);
    var mainContent = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "mainContent",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknscrollBkg",
        "top": "0dp",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    mainContent.setDefaultUnit(kony.flex.DP);
    var androidSettings = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "androidSettings",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    androidSettings.setDefaultUnit(kony.flex.DP);
    var iosInstruction = new kony.ui.Label({
        "centerX": "50%",
        "id": "iosInstruction",
        "isVisible": true,
        "right": "0%",
        "skin": "sknstandardTextBold",
        "text": kony.i18n.getLocalizedString("i18n.tranfer.SelectSearchRangeC"),
        "top": "10dp",
        "width": "70%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var LocatorDistanceSegmentAndroid = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "data": [{
            "imgicontick": "",
            "lblPageNameKA": "5 Miles"
        }, {
            "imgicontick": "",
            "lblPageNameKA": "10 Miles"
        }, {
            "imgicontick": "",
            "lblPageNameKA": "25 Miles"
        }, {
            "imgicontick": "check_blue.png",
            "lblPageNameKA": "50 Miles"
        }, {
            "imgicontick": "",
            "lblPageNameKA": "100 Miles"
        }],
        "groupCells": false,
        "id": "LocatorDistanceSegmentAndroid",
        "isVisible": true,
        "right": "0dp",
        "needPageIndicator": true,
        "onRowClick": AS_Segment_dedaa6e6fdf14e75aa7b03625b5549de,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowTemplate": container,
        "scrollingEvents": {},
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "10dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "container": "container",
            "imgicontick": "imgicontick",
            "lblPageNameKA": "lblPageNameKA"
        },
        "width": "100%"
    }, {
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    var segmentBorderBottomAndroid = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "segmentBorderBottomAndroid",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "top": "-1dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    segmentBorderBottomAndroid.setDefaultUnit(kony.flex.DP);
    segmentBorderBottomAndroid.add();
    androidSettings.add(iosInstruction, LocatorDistanceSegmentAndroid, segmentBorderBottomAndroid);
    mainContent.add(androidSettings);
    frmSetLocatorDistanceFilterKA.add(androidTitleBar, mainContent);
};
function frmSetLocatorDistanceFilterKAGlobalsAr() {
    frmSetLocatorDistanceFilterKAAr = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmSetLocatorDistanceFilterKAAr,
        "bounces": false,
        "enabledForIdleTimeout": true,
        "id": "frmSetLocatorDistanceFilterKA",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "needAppMenu": false,
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": true,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "retainScrollPosition": false,
        "titleBar": true,
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};
