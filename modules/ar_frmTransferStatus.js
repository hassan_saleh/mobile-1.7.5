//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmTransferStatusAr() {
frmTransferStatus.setDefaultUnit(kony.flex.DP);
var flxTransferStatusTop = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxTransferStatusTop",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"skin": "sknFlxSearch",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxTransferStatusTop.setDefaultUnit(kony.flex.DP);
var flxHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "34%",
"id": "flxHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxHeader.setDefaultUnit(kony.flex.DP);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_abc206f23580434a84e1b52f3ffd47f6,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 1
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBack = new kony.ui.Label({
"centerY": "50%",
"id": "lblBack",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBack.add(lblBackIcon, lblBack);
var lblTitle = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "lblTitle",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.common.transferstatus"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxHeader.add(flxBack, lblTitle);
var flxTab = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "22%",
"id": "flxTab",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "6%",
"skin": "slFboxOuterRing",
"top": "2%",
"width": "88%",
"zIndex": 1
}, {}, {});
flxTab.setDefaultUnit(kony.flex.DP);
var btnInwards = new kony.ui.Button({
"focusSkin": "slButtonWhiteTabFocus",
"height": "100%",
"id": "btnInwards",
"isVisible": true,
"right": "0%",
"onClick": AS_Button_g2072f5db2ef478595e6b416c684512e,
"skin": "slButtonWhiteTab",
"text": kony.i18n.getLocalizedString("i18n.transferstatus.inward"),
"top": "0%",
"width": "50%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 1],
"paddingInPixel": false
}, {});
var btnOutwards = new kony.ui.Button({
"focusSkin": "slButtonWhiteTabFocus",
"height": "100%",
"id": "btnOutwards",
"isVisible": true,
"right": "0%",
"onClick": AS_Button_d830a62d5d784c96a60ea69a43b08802,
"skin": "slButtonWhiteTabDisabled",
"text": kony.i18n.getLocalizedString("i18n.transferstatus.outward"),
"top": "0%",
"width": "50%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 1],
"paddingInPixel": false
}, {});
flxTab.add( btnOutwards,btnInwards);
var flxSearch = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "30%",
"id": "flxSearch",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "flexTransparent",
"top": "2%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxSearch.setDefaultUnit(kony.flex.DP);
var flxSearchHolder = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"centerY": "50%",
"clipBounds": true,
"height": "96%",
"id": "flxSearchHolder",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"skin": "slFbox",
"top": "0%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxSearchHolder.setDefaultUnit(kony.flex.DP);
var lblSearchImg = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblSearchImg",
"isVisible": true,
"right": "0%",
"skin": "sknSearchIcon",
"text": "h",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "8%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var tbxSearch = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"centerY": "50%",
"focusSkin": "sknTxtBoxSearch",
"height": "100%",
"id": "tbxSearch",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "0%",
"onTextChange": AS_TextField_e27da843a9024ef5bbd5b674f9bad259,
"placeholder": kony.i18n.getLocalizedString("i18n.transfer.searchPlh"),
"secureTextEntry": false,
"skin": "sknTxtBoxSearch",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "92%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
"placeholderSkin": "sknPlaceHolder",
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
flxSearchHolder.add( tbxSearch,lblSearchImg);
var flxUnderLineSearch = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0%",
"centerX": "50%",
"clipBounds": true,
"height": "3%",
"id": "flxUnderLineSearch",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"width": "90%",
"zIndex": 1
}, {}, {});
flxUnderLineSearch.setDefaultUnit(kony.flex.DP);
flxUnderLineSearch.add();
flxSearch.add(flxSearchHolder, flxUnderLineSearch);
flxTransferStatusTop.add(flxHeader, flxTab, flxSearch);
var flxTransferStatusBody = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "80%",
"id": "flxTransferStatusBody",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "20%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxTransferStatusBody.setDefaultUnit(kony.flex.DP);
var lblTransferHint = new kony.ui.Label({
"id": "lblTransferHint",
"isVisible": true,
"right": "2%",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.transferstatus.hint"),
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "0%",
"width": "96%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxDivider = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2dp",
"id": "flxDivider",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "11%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxDivider.setDefaultUnit(kony.flex.DP);
flxDivider.add();
var segTransferStatus = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"data": [{
"lblAccountNumber": "Account Number",
"lblAmount": "Account Number",
"lblBeneficiaryName": "Nick Name",
"lblDate": "11/11/2019",
"lblInitial": "BH",
"lblRightIcon": kony.i18n.getLocalizedString("i18n.appsettings.more")
}],
"groupCells": false,
"height": "89%",
"id": "segTransferStatus",
"isVisible": true,
"right": "0%",
"needPageIndicator": true,
"onRowClick": AS_Segment_gd17be748dc64f8ab6d613c1e64ffe22,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "slSegAllBene",
"rowSkin": "slSegAllBene",
"rowTemplate": flxTransactionStatusSegDetails,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorRequired": false,
"showScrollbars": false,
"top": "11%",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"contactListDivider": "contactListDivider",
"flxIcon1": "flxIcon1",
"flxTransactionStatusDetails": "flxTransactionStatusDetails",
"flxTransactionStatusSegDetails": "flxTransactionStatusSegDetails",
"lblAccountNumber": "lblAccountNumber",
"lblAmount": "lblAmount",
"lblBeneficiaryName": "lblBeneficiaryName",
"lblDate": "lblDate",
"lblInitial": "lblInitial",
"lblRightIcon": "lblRightIcon"
},
"width": "100%",
"zIndex": 1
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var lblNoRecors = new kony.ui.Label({
"centerX": "50%",
"centerY": "30%",
"id": "lblNoRecors",
"isVisible": false,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.common.noRecAvail"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxTransferStatusBody.add(lblTransferHint, flxDivider, segTransferStatus, lblNoRecors);
frmTransferStatus.add(flxTransferStatusTop, flxTransferStatusBody);
};
function frmTransferStatusGlobalsAr() {
frmTransferStatusAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmTransferStatusAr,
"bounces": false,
"enableScrolling": false,
"enabledForIdleTimeout": true,
"id": "frmTransferStatus",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": true,
"skin": "sknSuccessBkg"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"retainScrollPosition": false,
"titleBar": true,
"titleBarSkin": "slTitleBar",
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
