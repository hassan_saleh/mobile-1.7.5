//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function initializesegFAQRowAr() {
    flxFAQRowAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": false,
        "id": "flxFAQRow",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "skin": "flxBgBlueGradientRound8"
    }, {}, {});
    flxFAQRow.setDefaultUnit(kony.flex.DP);
    var flxQuestion = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxQuestion",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "slFbox",
        "top": "0px",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxQuestion.setDefaultUnit(kony.flex.DP);
    var lblQuestion = new kony.ui.Label({
        "id": "lblQuestion",
        "isVisible": true,
        "right": "8%",
        "skin": "sknLblWhike125",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "0",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 2,0, 5],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgSearch = new kony.ui.Image2({
        "height": "60px",
        "id": "imgSearch",
        "isVisible": true,
        "left": 20,
        "skin": "slImage",
        "src": "dropdownlist.png",
        "top": 10,
        "width": "20dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [ 0, 0,0, 2],
        "paddingInPixel": false
    }, {});
    flxQuestion.add(lblQuestion, imgSearch);
    var lblAnswer = new kony.ui.Label({
        "id": "lblAnswer",
        "isVisible": true,
        "right": "5%",
        "skin": "CopysknLblWhike0f7876795165448",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxFAQRowAr.add(flxQuestion, lblAnswer);
}
