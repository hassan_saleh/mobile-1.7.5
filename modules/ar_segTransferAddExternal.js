//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function initializesegTransferAddExternalAr() {
    Copycontainer03f00119dae464dAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "Copycontainer03f00119dae464d",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknyourAccountCard"
    }, {}, {});
    Copycontainer03f00119dae464d.setDefaultUnit(kony.flex.DP);
    var lblContact = new kony.ui.Label({
        "id": "lblContact",
        "isVisible": true,
        "right": "5%",
        "skin": "skn",
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblType = new kony.ui.Label({
        "id": "lblType",
        "isVisible": false,
        "right": "5%",
        "skin": "sknRegisterMobileBank",
        "top": 32,
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAccountNumberKA = new kony.ui.Label({
        "id": "lblAccountNumberKA",
        "isVisible": false,
        "right": "224dp",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "15dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblRowSeparator = new kony.ui.Label({
        "bottom": "0dp",
        "height": "1dp",
        "id": "lblRowSeparator",
        "isVisible": true,
        "right": "4%",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "96%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    Copycontainer03f00119dae464dAr.add(lblContact, lblType, lblAccountNumberKA, lblRowSeparator);
}
