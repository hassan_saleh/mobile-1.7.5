//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmJoMoPayAr() {
frmJoMoPay.setDefaultUnit(kony.flex.DP);
var flxJoMoPayHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxJoMoPayHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "s",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxJoMoPayHeader.setDefaultUnit(kony.flex.DP);
var lblJoMoPay = new kony.ui.Label({
"centerX": "50%",
"centerY": "45%",
"id": "lblJoMoPay",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.jomopay.jomopaypayment"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var btnNext = new kony.ui.Button({
"focusSkin": "jomopaynextDisabled",
"height": "100%",
"id": "btnNext",
"isVisible": true,
"onClick": AS_Button_ae3e65b833c744a88b0a6cbcfd3acabb,
"right": "0.00%",
"skin": "jomopaynextDisabled",
"text": kony.i18n.getLocalizedString("i18n.login.next"),
"top": "0.18%",
"width": "20%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_cf9f33c6b2144a22ae818763c190f30d,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 2
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblBack0i5985fbe3b634e = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"centerY": "50%",
"height": "100%",
"id": "CopylblBack0i5985fbe3b634e",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBack.add(lblBackIcon, CopylblBack0i5985fbe3b634e);
flxJoMoPayHeader.add(lblJoMoPay, btnNext, flxBack);
var flxJoMoPayTransaction = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "91%",
"id": "flxJoMoPayTransaction",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "CopyslFSbox0g5d481db55db49",
"top": "9%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxJoMoPayTransaction.setDefaultUnit(kony.flex.DP);
var flxTransfer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "12%",
"id": "flxTransfer",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "3%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxTransfer.setDefaultUnit(kony.flex.DP);
var lblTransferTypeStatic = new kony.ui.Label({
"id": "lblTransferTypeStatic",
"isVisible": true,
"right": "5%",
"skin": "sknLblCurr",
"text": kony.i18n.getLocalizedString("i18n.jomopay.transfertype"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "13%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxTransferType = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "45%",
"id": "flxTransferType",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "4%",
"skin": "slFbox",
"top": "38%",
"width": "96%",
"zIndex": 1
}, {}, {});
flxTransferType.setDefaultUnit(kony.flex.DP);
var lblTransferType = new kony.ui.Label({
"id": "lblTransferType",
"isVisible": true,
"right": "2%",
"skin": "sknTransferType",
"text": kony.i18n.getLocalizedString("i18n.jomopay.type"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "20%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var btnDropDown = new kony.ui.Button({
"focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
"height": "100%",
"id": "btnDropDown",
"isVisible": true,
"onClick": AS_Button_b781a2826fc745ad9fd4187686694f4b,
"left": "6%",
"skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
"text": "d",
"top": "2%",
"width": "94%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxTransferType.add(lblTransferType, btnDropDown);
var flxBorderTransferType = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "10%",
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxBorderTransferType",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "skntextFieldDividerGreen",
"width": "90%",
"zIndex": 1
}, {}, {});
flxBorderTransferType.setDefaultUnit(kony.flex.DP);
flxBorderTransferType.add();
flxTransfer.add(lblTransferTypeStatic, flxTransferType, flxBorderTransferType);
var flxAccount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "12%",
"id": "flxAccount",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "15%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAccount.setDefaultUnit(kony.flex.DP);
var lblAccountStaticText = new kony.ui.Label({
"height": "35%",
"id": "lblAccountStaticText",
"isVisible": true,
"right": "5%",
"skin": "sknLblCurr",
"text": kony.i18n.getLocalizedString("i18n.settings.account"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "13%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxAccountType = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50%",
"id": "flxAccountType",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "4%",
"skin": "slFbox",
"top": "37%",
"width": "96%",
"zIndex": 1
}, {}, {});
flxAccountType.setDefaultUnit(kony.flex.DP);
var btnDropDownAccount = new kony.ui.Button({
"focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
"height": "100%",
"id": "btnDropDownAccount",
"isVisible": true,
"onClick": AS_Button_b61f936dc80b4c2b89cea606fa60497c,
"left": "6%",
"skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
"text": "d",
"top": "2%",
"width": "94%",
"zIndex": 3
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var flxAccountNo = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxAccountNo",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "1%",
"skin": "slFbox",
"top": "5%",
"width": "80%",
"zIndex": 2
}, {}, {});
flxAccountNo.setDefaultUnit(kony.flex.DP);
var lblAccountType = new kony.ui.Label({
"height": "100%",
"id": "lblAccountType",
"isVisible": true,
"right": "0%",
"skin": "sknTransferType",
"text": "Current Account",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblAccountCode = new kony.ui.Label({
"height": "100%",
"id": "lblAccountCode",
"isVisible": true,
"right": "57%",
"skin": "lblsegtextsmall0b5a3b38d4be646",
"text": " ***2002",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxAccountNo.add(lblAccountType, lblAccountCode);
flxAccountType.add(btnDropDownAccount, flxAccountNo);
var flxBorderAccountType = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "5%",
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxBorderAccountType",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "skntextFieldDividerGreen",
"width": "90%",
"zIndex": 1
}, {}, {});
flxBorderAccountType.setDefaultUnit(kony.flex.DP);
flxBorderAccountType.add();
flxAccount.add(lblAccountStaticText, flxAccountType, flxBorderAccountType);
var flxJoMoPayType = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "12%",
"id": "flxJoMoPayType",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "28%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxJoMoPayType.setDefaultUnit(kony.flex.DP);
var flxJoMoPayTypeStaticText = new kony.ui.Label({
"id": "flxJoMoPayTypeStaticText",
"isVisible": true,
"right": "5%",
"skin": "sknLblCurr",
"text": kony.i18n.getLocalizedString("i18n.jomopay.jomopaytype"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "13%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxJoMoPay = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "45%",
"id": "flxJoMoPay",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "4%",
"skin": "slFbox",
"top": "38%",
"width": "96%",
"zIndex": 1
}, {}, {});
flxJoMoPay.setDefaultUnit(kony.flex.DP);
var lblType = new kony.ui.Label({
"id": "lblType",
"isVisible": true,
"right": "1%",
"skin": "sknTransferType",
"text": kony.i18n.getLocalizedString("i18n.jomopay.mobiletype"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "20%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var btnAliasDropDown = new kony.ui.Button({
"focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
"height": "100%",
"id": "btnAliasDropDown",
"isVisible": true,
"onClick": AS_Button_e71f09233b624895b90d0a671fab2e61,
"left": "5%",
"skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
"text": "d",
"top": "2%",
"width": "95%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxJoMoPay.add(lblType, btnAliasDropDown);
var flxBorderAliasType = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "10%",
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxBorderAliasType",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "skntextFieldDividerGreen",
"width": "90%",
"zIndex": 1
}, {}, {});
flxBorderAliasType.setDefaultUnit(kony.flex.DP);
flxBorderAliasType.add();
flxJoMoPayType.add(flxJoMoPayTypeStaticText, flxJoMoPay, flxBorderAliasType);
var flxBeneficiaryAlias = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "13%",
"id": "flxBeneficiaryAlias",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "41%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxBeneficiaryAlias.setDefaultUnit(kony.flex.DP);
var lblBeneficiaryStaticText = new kony.ui.Label({
"id": "lblBeneficiaryStaticText",
"isVisible": true,
"right": "0%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.jomopay.mobile"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "35%",
"width": "82%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxAlias = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "45%",
"id": "flxAlias",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "38%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAlias.setDefaultUnit(kony.flex.DP);
var txtPhoneNo = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "txtBox0b5a21c6c49d64b",
"height": "100%",
"id": "txtPhoneNo",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "1%",
"maxTextLength": 15,
"onDone": AS_TextField_d7ae597713984b5eb36c253f1b8d4104,
"onTextChange": AS_TextField_j359cd6e26024eb8aca6f22a6aa09272,
"onTouchEnd": AS_TextField_aed902da642c4309b1f59a07dabb139a,
"onTouchStart": AS_TextField_b57e5176b14a4e9591606105d07090af,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"top": "0%",
"width": "90%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
"onBeginEditing": AS_TextField_jea7df8c20a04520a9d9fd405b2d38a8,
"onEndEditing": AS_TextField_bbd722cbd4674b168807efe7d4de13a9,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var txtAliasType = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "txtBox0b5a21c6c49d64b",
"height": "100%",
"id": "txtAliasType",
"isVisible": false,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "1%",
"maxTextLength": 15,
"onDone": AS_TextField_f4f4107326a842cbbc0c602607588a1f,
"onTextChange": AS_TextField_e827efb40ea240978fe35ea34ccd7f57,
"onTouchEnd": AS_TextField_e43d42887bae4263a524080a5b6ad41b,
"onTouchStart": AS_TextField_d9ad5cacfef148819d347bda85297150,
"secureTextEntry": false,
"skin": "txtBox0b5a21c6c49d64b",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "0%",
"width": "90%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
"onBeginEditing": AS_TextField_c455a979d0fb4ac8917a9a10a35d9b78,
"onEndEditing": AS_TextField_j4077d4d546e481f903e918a067c5ec9,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var btnContactBook = new kony.ui.Button({
"focusSkin": "sknBtnContactList",
"height": "100%",
"id": "btnContactBook",
"isVisible": true,
"right": "92%",
"onClick": AS_Button_g72ce29dd91c4ccb9dc6629449749664,
"skin": "sknBtnContactList",
"text": "y",
"top": "0%",
"width": "8%",
"zIndex": 5
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxAlias.add(txtPhoneNo, txtAliasType, btnContactBook);
var flxBorderBenificiary = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "10%",
"clipBounds": true,
"height": "2%",
"id": "flxBorderBenificiary",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "skntextFieldDividerJomoPay",
"width": "100%",
"zIndex": 1
}, {}, {});
flxBorderBenificiary.setDefaultUnit(kony.flex.DP);
flxBorderBenificiary.add();
flxBeneficiaryAlias.add(lblBeneficiaryStaticText, flxAlias, flxBorderBenificiary);
var lblMobileIDHint = new kony.ui.Label({
"id": "lblMobileIDHint",
"isVisible": false,
"right": "5%",
"skin": "sknCairoRegular100White50OP",
"text": kony.i18n.getLocalizedString("i18n.jomopay.mobilecashidhint"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "54%",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "12%",
"id": "flxAmount",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "58%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAmount.setDefaultUnit(kony.flex.DP);
var lblAmountStaticText = new kony.ui.Label({
"id": "lblAmountStaticText",
"isVisible": true,
"right": "5%",
"skin": "sknlblanimated75",
"text": kony.i18n.getLocalizedString("i18n.accounts.amount"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "13%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var txtFieldAmount = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "txtBox0b5a21c6c49d64b",
"height": "45%",
"id": "txtFieldAmount",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DECIMAL,
"right": "5%",
"maxTextLength": 9,
"onDone": AS_TextField_f0797600ac284aa3ab9f5d23f3cc5fa4,
"onTextChange": AS_TextField_e6eb6e5b842b42f6a01a175ffc8cc746,
"onTouchStart": AS_TextField_j831f8dd430448d49a824b01f760b46d,
"placeholder": "0.000",
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"top": "40%",
"width": "90%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"onBeginEditing": AS_TextField_ba8e9b7f1a554ac788564b1f34bb4bb7,
"onEndEditing": AS_TextField_i319f55c90e64987809e0d624052414b,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxBorderAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "10%",
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxBorderAmount",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "skntextFieldDividerJomoPay",
"width": "90%",
"zIndex": 1
}, {}, {});
flxBorderAmount.setDefaultUnit(kony.flex.DP);
flxBorderAmount.add();
var lblCurrency = new kony.ui.Label({
"id": "lblCurrency",
"isVisible": true,
"right": "87%",
"skin": "lblAccountStaticText",
"text": "JOD",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "42%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxAmount.add(lblAmountStaticText, txtFieldAmount, flxBorderAmount, lblCurrency);
var lblIncorrect = new kony.ui.Label({
"bottom": "10%",
"id": "lblIncorrect",
"isVisible": false,
"left": "10%",
"skin": "sknInvalidCredKA",
"text": kony.i18n.getLocalizedString("i18n.jomopay.fillDetails"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxDescription = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxDescription",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "76%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxDescription.setDefaultUnit(kony.flex.DP);
var lblDescription = new kony.ui.Label({
"id": "lblDescription",
"isVisible": true,
"right": "5%",
"skin": "lblAccountStaticText",
"text": kony.i18n.getLocalizedString("i18n.jomopay.description"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var textAreaDescrip = new kony.ui.TextArea2({
"autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
"height": "75%",
"id": "textAreaDescrip",
"isVisible": true,
"keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
"right": "5%",
"numberOfVisibleLines": 3,
"skin": "sknTextAreaJomoPay",
"textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
"top": "30%",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 2, 2,2, 2],
"paddingInPixel": false
}, {});
flxDescription.add(lblDescription, textAreaDescrip);
var flxDescrip = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "12%",
"id": "flxDescrip",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "3%",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b3edf457af3b43",
"top": "72%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxDescrip.setDefaultUnit(kony.flex.DP);
var lblstaticdescription = new kony.ui.Label({
"id": "lblstaticdescription",
"isVisible": true,
"right": "2%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.jomopay.description"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": "86%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var txtDescription = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "txtBox0b5a21c6c49d64b",
"height": "45%",
"id": "txtDescription",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "2%",
"maxTextLength": 150,
"onDone": AS_TextField_e013c5c189214f11a3806466279302d2,
"onTextChange": AS_TextField_i05cf399f855467196971f73bfec6fbe,
"onTouchEnd": AS_TextField_ebad4acbe397482986551faac39ec772,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "40%",
"width": "90%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"onEndEditing": AS_TextField_e8b0ad14f0e64377b3b92971e1fa6f05,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxBorderDescription = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "10%",
"clipBounds": true,
"height": "2%",
"id": "flxBorderDescription",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "2%",
"skin": "skntextFieldDividerJomoPay",
"width": "90%",
"zIndex": 1
}, {}, {});
flxBorderDescription.setDefaultUnit(kony.flex.DP);
flxBorderDescription.add();
flxDescrip.add(lblstaticdescription, txtDescription, flxBorderDescription);
flxJoMoPayTransaction.add(flxTransfer, flxAccount, flxJoMoPayType, flxBeneficiaryAlias, lblMobileIDHint, flxAmount, lblIncorrect, flxDescription, flxDescrip);
var flxPopupOuter = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxPopupOuter",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"onClick": AS_FlexContainer_g14c9e38f3b04a03884164d429d4b7e5,
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxPopupOuter.setDefaultUnit(kony.flex.DP);
var flxInnerPopup = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"bounces": true,
"centerX": "50%",
"centerY": "50%",
"clipBounds": true,
"enableScrolling": true,
"horizontalScrollIndicator": true,
"id": "flxInnerPopup",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "CopyslFSbox0aabc4031db8b4f",
"verticalScrollIndicator": true,
"width": "90%",
"zIndex": 1
}, {}, {});
flxInnerPopup.setDefaultUnit(kony.flex.DP);
var segPopup = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"centerY": "50%",
"data": [{
"lblJPTransfer": "Government Account",
"lblJomopayType": "Label"
}, {
"lblJPTransfer": "Government Account",
"lblJomopayType": "Label"
}, {
"lblJPTransfer": "Government Account",
"lblJomopayType": "Label"
}, {
"lblJPTransfer": "Government Account",
"lblJomopayType": "Label"
}],
"groupCells": false,
"height": "85%",
"id": "segPopup",
"isVisible": true,
"right": "0%",
"needPageIndicator": true,
"onRowClick": AS_Segment_db112cf714c549f286cc83e0352af7af,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "seg2Focus",
"rowSkin": "seg2Normal",
"rowTemplate": flxJPPopup,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorRequired": true,
"showScrollbars": false,
"top": "0%",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"flxJPPopup": "flxJPPopup",
"lblJPTransfer": "lblJPTransfer",
"lblJomopayType": "lblJomopayType"
},
"width": "95%",
"zIndex": 20
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxInnerPopup.add(segPopup);
flxPopupOuter.add(flxInnerPopup);
frmJoMoPay.add(flxJoMoPayHeader, flxJoMoPayTransaction, flxPopupOuter);
};
function frmJoMoPayGlobalsAr() {
frmJoMoPayAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmJoMoPayAr,
"enabledForIdleTimeout": true,
"id": "frmJoMoPay",
"init": AS_Form_j5b5c1c4314e448dbe46985fb2cb238a,
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"postShow": AS_Form_hb7109ebaa124276b493423a431753ab,
"preShow": AS_Form_f1dae8d676644ca4a8b93d94df2b91a2,
"skin": "sknSuccessBkg"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_f747875fc5d6411397cfe1d2a0de7775,
"retainScrollPosition": false,
"titleBar": true,
"titleBarSkin": "slTitleBar",
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
