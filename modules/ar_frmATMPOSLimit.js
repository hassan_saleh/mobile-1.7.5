//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmATMPOSLimitAr() {
frmATMPOSLimit.setDefaultUnit(kony.flex.DP);
var flxHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "slFbox0b7d74518f07a4d",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxHeader.setDefaultUnit(kony.flex.DP);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "95%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_g8ec2b96eee34ab18a5c47f2bbbf59b4,
"skin": "slFbox",
"top": "0%",
"width": "18%",
"zIndex": 1
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"height": "100%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBack = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"height": "100%",
"id": "lblBack",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBack.add(lblBackIcon, lblBack);
var lblFormHeading = new kony.ui.Label({
"centerX": "50%",
"height": "100%",
"id": "lblFormHeading",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.atmpos.updatelimit"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": "65%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxHeader.add(flxBack, lblFormHeading);
var flxBody = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBody",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "10%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxBody.setDefaultUnit(kony.flex.DP);
var flxToggle = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "7%",
"id": "flxToggle",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "5%",
"skin": "sknBOJblueBG",
"top": "3%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxToggle.setDefaultUnit(kony.flex.DP);
var btnDaily = new kony.ui.Button({
"focusSkin": "sknBtnBGBlueWhite105Rd10",
"height": "100%",
"id": "btnDaily",
"isVisible": true,
"right": "0%",
"onClick": AS_Button_d03cc2209df14854bc9dbd020e5fd4d7,
"skin": "sknBtnBGWhiteBlue105Rd10",
"text": kony.i18n.getLocalizedString("i18n.transfers.Daily"),
"top": "0%",
"width": "33%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnWeekly = new kony.ui.Button({
"focusSkin": "sknBtnBGBlueWhite105Rd10",
"height": "100%",
"id": "btnWeekly",
"isVisible": true,
"right": "0%",
"onClick": AS_Button_d03cc2209df14854bc9dbd020e5fd4d7,
"skin": "sknBtnBGBlueWhite105Rd10",
"text": kony.i18n.getLocalizedString("i18n.transfers.WeeklyOnce"),
"top": "0%",
"width": "34%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnMonthly = new kony.ui.Button({
"focusSkin": "sknBtnBGBlueWhite105Rd10",
"height": "100%",
"id": "btnMonthly",
"isVisible": true,
"right": "0%",
"onClick": AS_Button_d03cc2209df14854bc9dbd020e5fd4d7,
"skin": "sknBtnBGBlueWhite105Rd10",
"text": kony.i18n.getLocalizedString("i18n.transfers.MonthlyOnce"),
"top": "0%",
"width": "33%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxToggle.add( btnMonthly, btnWeekly,btnDaily);
var flxWithdrawalPOSlimit = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "70%",
"id": "flxWithdrawalPOSlimit",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"skin": "slFbox",
"top": "10%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxWithdrawalPOSlimit.setDefaultUnit(kony.flex.DP);
var flxWithdrawalLimit = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "35%",
"id": "flxWithdrawalLimit",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "5%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxWithdrawalLimit.setDefaultUnit(kony.flex.DP);
var flxWithdrawalLimitEntry = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90dp",
"id": "flxWithdrawalLimitEntry",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "sknslFbox",
"top": "10%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxWithdrawalLimitEntry.setDefaultUnit(kony.flex.PERCENTAGE);
var txtWithdrawalLimit = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "23%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "50%",
"id": "txtWithdrawalLimit",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"onTextChange": AS_TextField_e12d687073e74031a9afc37b7710bba6,
"onTouchEnd": AS_TextField_c0d7475dffd44177b9ea0cad91e4a7f9,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"width": "100%",
"zIndex": 10
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var WithdrawalLimitBorder = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "WithdrawalLimitBorder",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "skntextFieldDivider",
"top": "73%",
"width": "95%",
"zIndex": 2
}, {}, {});
WithdrawalLimitBorder.setDefaultUnit(kony.flex.DP);
WithdrawalLimitBorder.add();
var lblWithdrawalLimitTitle = new kony.ui.Label({
"id": "lblWithdrawalLimitTitle",
"isVisible": true,
"right": "2%",
"skin": "sknCaiRegWhite50Op",
"text": kony.i18n.getLocalizedString("i18n.atmpos.atmlimit"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "28%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblWithdrawalLimitHint = new kony.ui.Label({
"bottom": "0%",
"id": "lblWithdrawalLimitHint",
"isVisible": true,
"left": "3%",
"skin": "sknLblSmallWhite",
"text": "Card limit is 500",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxWithdrawalLimitEntry.add(txtWithdrawalLimit, WithdrawalLimitBorder, lblWithdrawalLimitTitle, lblWithdrawalLimitHint);
flxWithdrawalLimit.add(flxWithdrawalLimitEntry);
var sliderATMLimit = new kony.ui.Slider({
"height": "45dp",
"id": "sliderATMLimit",
"isVisible": true,
"left": "5%",
"leftSkin": "slSliderLeftBlue",
"max": 100,
"min": 0,
"onSlide": AS_Slider_ba6b7476148346be8b0e3ddb48374e76,
"rightSkin": "slSliderRightBlue",
"selectedValue": 40,
"step": 1,
"thumbImage": "slider_android.png",
"top": "-3%",
"width": "90%",
"zIndex": 1
}, {}, {
"thickness": 15
});
var flxPOSLimit = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "35%",
"id": "flxPOSLimit",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "-5%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxPOSLimit.setDefaultUnit(kony.flex.DP);
var flxPOSLimitEntry = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90dp",
"id": "flxPOSLimitEntry",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "sknslFbox",
"top": "10%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxPOSLimitEntry.setDefaultUnit(kony.flex.PERCENTAGE);
var txtPOSLimit = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "23%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "50%",
"id": "txtPOSLimit",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"onTextChange": AS_TextField_e12d687073e74031a9afc37b7710bba6,
"onTouchEnd": AS_TextField_c0d7475dffd44177b9ea0cad91e4a7f9,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"width": "100%",
"zIndex": 10
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var POSLimitBorder = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "POSLimitBorder",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "skntextFieldDivider",
"top": "73%",
"width": "95%",
"zIndex": 2
}, {}, {});
POSLimitBorder.setDefaultUnit(kony.flex.DP);
POSLimitBorder.add();
var lblPOSLimitTitle = new kony.ui.Label({
"id": "lblPOSLimitTitle",
"isVisible": true,
"right": "2%",
"skin": "sknCaiRegWhite50Op",
"text": kony.i18n.getLocalizedString("i18n.atmpos.poslimit"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "28%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblPOSLimitHint = new kony.ui.Label({
"bottom": "0%",
"id": "lblPOSLimitHint",
"isVisible": true,
"left": "3%",
"skin": "sknLblSmallWhite",
"text": "Card limit is 500",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxPOSLimitEntry.add(txtPOSLimit, POSLimitBorder, lblPOSLimitTitle, lblPOSLimitHint);
flxPOSLimit.add(flxPOSLimitEntry);
var sliderPOSLimit = new kony.ui.Slider({
"height": "45dp",
"id": "sliderPOSLimit",
"isVisible": true,
"left": "5%",
"leftSkin": "slSliderLeftBlue",
"max": 100,
"min": 0,
"onSlide": AS_Slider_ba6b7476148346be8b0e3ddb48374e76,
"rightSkin": "slSliderRightBlue",
"selectedValue": 40,
"step": 1,
"thumbImage": "slider_android.png",
"top": "-3%",
"width": "90%",
"zIndex": 1
}, {}, {
"thickness": 15
});
flxWithdrawalPOSlimit.add(flxWithdrawalLimit, sliderATMLimit, flxPOSLimit, sliderPOSLimit);
var btnUpdateLimit = new kony.ui.Button({
"centerX": "50%",
"centerY": "90%",
"focusSkin": "slButtonWhiteFocus",
"height": "10%",
"id": "btnUpdateLimit",
"isVisible": true,
"onClick": AS_Button_g3dd2d9ed8a2424cb3172aa17f54f678,
"skin": "slButtonWhite",
"text": kony.i18n.getLocalizedString("i18n.atmpos.updatelimit"),
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxBody.add(flxToggle, flxWithdrawalPOSlimit, btnUpdateLimit);
frmATMPOSLimit.add(flxHeader, flxBody);
};
function frmATMPOSLimitGlobalsAr() {
frmATMPOSLimitAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmATMPOSLimitAr,
"allowHorizontalBounce": false,
"allowVerticalBounce": false,
"bounces": false,
"enabledForIdleTimeout": false,
"id": "frmATMPOSLimit",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": true,
"postShow": AS_Form_cb073edae53142e2873a65545b38bf17,
"skin": "sknmainGradient",
"verticalScrollIndicator": false
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"retainScrollPosition": false,
"titleBar": true,
"titleBarSkin": "slTitleBar",
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
