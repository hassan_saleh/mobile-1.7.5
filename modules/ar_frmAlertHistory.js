//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmAlertHistoryAr() {
frmAlertHistory.setDefaultUnit(kony.flex.DP);
var flxAlertHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxAlertHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "s",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAlertHeader.setDefaultUnit(kony.flex.DP);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_febead9dc46e42dfbc6c5d8ea48eeeec,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 1
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBack = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"centerY": "50%",
"id": "lblBack",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBack.add(lblBackIcon, lblBack);
var lblAlertHeader = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "lblAlertHeader",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.settings.alerthistory"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var btnDeleteAlertHistory = new kony.ui.Button({
"focusSkin": "CopysknSegmentDelete0h225a870b5144b",
"height": "96%",
"id": "btnDeleteAlertHistory",
"isVisible": true,
"onClick": AS_Button_ed3a4a7981f94c63ac67ab2cf256168e,
"right": "3%",
"skin": "CopysknSegmentDelete0h225a870b5144b",
"text": "w",
"top": "0%",
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
flxAlertHeader.add(flxBack, lblAlertHeader, btnDeleteAlertHistory);
var flxBody = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "91%",
"id": "flxBody",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknDetails",
"top": "9%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxBody.setDefaultUnit(kony.flex.DP);
var segDetails = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"data": [{
"lblAlertDetails": "",
"lblDate": ""
}],
"groupCells": false,
"height": "100%",
"id": "segDetails",
"isVisible": true,
"needPageIndicator": true,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "seg2Focus",
"rowSkin": "Copyseg0a99091fc3d474a",
"rowTemplate": flxAlertDetail,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "02acd850",
"separatorRequired": true,
"separatorThickness": 1,
"showScrollbars": false,
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"flxAlertDetail": "flxAlertDetail",
"lblAlertDetails": "lblAlertDetails",
"lblDate": "lblDate"
},
"width": "100%",
"zIndex": 1
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var LabelNoRecordsKA = new kony.ui.Label({
"centerX": "50%",
"centerY": "42%",
"id": "LabelNoRecordsKA",
"isVisible": false,
"skin": "skn383838LatoRegular107KA",
"text": "No Alerts to show",
"width": "65%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBody.add(segDetails, LabelNoRecordsKA);
frmAlertHistory.add(flxAlertHeader, flxBody);
};
function frmAlertHistoryGlobalsAr() {
frmAlertHistoryAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmAlertHistoryAr,
"bounces": false,
"enabledForIdleTimeout": true,
"id": "frmAlertHistory",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"skin": "sknSuccessBkg"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_a5564bdd3df845fe8b514b9f74da127e,
"retainScrollPosition": false,
"titleBar": true,
"titleBarSkin": "slTitleBar",
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
