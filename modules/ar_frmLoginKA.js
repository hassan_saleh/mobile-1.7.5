//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmLoginKAAr() {
frmLoginKA.setDefaultUnit(kony.flex.DP);
var apScrollEnable = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": true,
"allowVerticalBounce": false,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "100%",
"horizontalScrollIndicator": false,
"id": "apScrollEnable",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onScrolling": AS_FlexScrollContainer_2c28c2fc0ff644e9b70b3a1ef6a2bee0,
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_HORIZONTAL,
"skin": "sknTransperentcommon",
"top": "0dp",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
apScrollEnable.setDefaultUnit(kony.flex.DP);
var loginMainScreen = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "loginMainScreen",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": 0,
"skin": "sknslFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
loginMainScreen.setDefaultUnit(kony.flex.DP);
var logoHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": false,
"height": "15%",
"id": "logoHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"top": "0dp",
"width": "100%",
"zIndex": 100
}, {}, {});
logoHeader.setDefaultUnit(kony.flex.DP);
var imgLogo = new kony.ui.Image2({
"centerX": "25%",
"centerY": "50%",
"height": "80%",
"id": "imgLogo",
"isVisible": true,
"maxWidth": "40%",
"skin": "sknslImage",
"src": "logo.png",
"top": "10%",
"width": "50%",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
var logoContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100dp",
"id": "logoContainer",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"left": "0dp",
"skin": "sknslFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
logoContainer.setDefaultUnit(kony.flex.DP);
logoContainer.add();
var lblVersion = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Register"
},
"centerX": "90%",
"id": "lblVersion",
"isVisible": false,
"maxNumberOfLines": 1,
"skin": "CopysknRegisterMobileBank0h92a6e4066af49",
"text": "V 3.1",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "2%",
"width": "20%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 3, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
logoHeader.add(imgLogo, logoContainer, lblVersion);
var loginCardWrapper = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": false,
"height": "54%",
"id": "loginCardWrapper",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"skin": "sknslFbox",
"top": "20%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 200
}, {}, {});
loginCardWrapper.setDefaultUnit(kony.flex.DP);
var loginCard = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": false,
"height": "100%",
"id": "loginCard",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"skin": "skngenericCard",
"top": "0%",
"width": "100%",
"zIndex": 99
}, {}, {});
loginCard.setDefaultUnit(kony.flex.DP);
var usernameContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "18%",
"id": "usernameContainer",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknslFbox",
"top": "0%",
"width": "75%",
"zIndex": 1
}, {}, {});
usernameContainer.setDefaultUnit(kony.flex.PERCENTAGE);
var usernameTextField = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "0%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "usernameTextField",
"isVisible": false,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "2%",
"maxTextLength": 20,
"onDone": AS_TextField_05e969d7d53544a8b170674133f57f6b,
"onTextChange": AS_TextField_eec07e64210c45bdb69a6c2f04c442cc,
"onTouchEnd": AS_TextField_gde2330ca7cd4c42b5eb5dda0d1bc028,
"onTouchStart": AS_TextField_aa9aca97808947a396553184c773c344,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "110%",
"zIndex": 20
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
"onBeginEditing": AS_TextField_dfc9bf669f264c949603888b212615d8,
"onEndEditing": AS_TextField_j946ec6c3c5041d99332dc2944a5175d,
"placeholderSkin": "sknPlaceholderKA",
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var tbxusernameTextField = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "0%",
"focusSkin": "sknUnameNew",
"height": "60%",
"id": "tbxusernameTextField",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "2%",
"maxTextLength": 20,
"onDone": AS_TextField_ea78439fbfd542a28e95b59b1527a967,
"onTextChange": AS_TextField_c025464c9e294dfba1b449a4121e5e71,
"onTouchEnd": AS_TextField_a5c36e346cfd47e887200766cc5e770d,
"onTouchStart": AS_TextField_b474439d8f8a452bb73fd97ddfa5eae1,
"secureTextEntry": false,
"skin": "sknUnameNew",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "110%",
"zIndex": 20
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
"onBeginEditing": AS_TextField_a1d722421d37415198e51f9c8e8121b0,
"onEndEditing": AS_TextField_id67663a568a4093b17261ff51b5439f,
"placeholderSkin": "sknUnameNew",
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var borderBottom = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "3%",
"id": "borderBottom",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "skntextFieldDivider",
"top": "90%",
"width": "95%",
"zIndex": 2
}, {}, {});
borderBottom.setDefaultUnit(kony.flex.DP);
borderBottom.add();
var lblChangeUserName = new kony.ui.Label({
"id": "lblChangeUserName",
"isVisible": true,
"right": "0dp",
"skin": "sknNumber",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "7dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 99
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblUserName = new kony.ui.Label({
"id": "lblUserName",
"isVisible": true,
"right": "2%",
"skin": "sknCaiRegWhite50Op",
"text": kony.i18n.getLocalizedString("i18n.login.username"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblClose = new kony.ui.Label({
"height": "100%",
"id": "lblClose",
"isVisible": false,
"onTouchEnd": AS_Label_ac7dbf71f91a4306b4e64bbbd005d013,
"left": "2%",
"skin": "sknClose",
"text": "O",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "10%",
"width": "10%",
"zIndex": 20
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
usernameContainer.add(usernameTextField, tbxusernameTextField, borderBottom, lblChangeUserName, lblUserName, lblClose);
var passwordContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "18%",
"id": "passwordContainer",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknslFbox",
"top": "0%",
"width": "75%",
"zIndex": 1
}, {}, {});
passwordContainer.setDefaultUnit(kony.flex.DP);
var lblPassword = new kony.ui.Label({
"id": "lblPassword",
"isVisible": true,
"right": "2%",
"skin": "sknCaiRegWhite50Op",
"text": kony.i18n.getLocalizedString("i18n.login.password"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var passwordTextField = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "0%",
"focusSkin": "skntxtMasked",
"height": "60%",
"id": "passwordTextField",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "2%",
"maxTextLength": 20,
"onDone": AS_TextField_g90e239868f046658b662648076b22ea,
"onTextChange": AS_TextField_cd68fa69f76a4bd9966d8b41beea9fdb,
"onTouchEnd": AS_TextField_j3223466667d4767a05c94e8307e7f3b,
"onTouchStart": AS_TextField_a97a99d890794fdbae84264ec2f33229,
"secureTextEntry": true,
"skin": "skntxtMasked",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "110%",
"zIndex": 20
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"onBeginEditing": AS_TextField_j8f53be97bf04fdcbd55901c41740ccb,
"onEndEditing": AS_TextField_a47356be9f69448f9e26666a578d6223,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var lblClosePass = new kony.ui.Label({
"height": "100%",
"id": "lblClosePass",
"isVisible": false,
"onTouchEnd": AS_Label_ida4b61186bc4cc1b3007b80924f7d27,
"left": "2%",
"skin": "sknClose",
"text": "O",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "10%",
"width": "10%",
"zIndex": 20
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxbdrpwd = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "3%",
"id": "flxbdrpwd",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "skntextFieldDivider",
"top": "90%",
"width": "95%",
"zIndex": 2
}, {}, {});
flxbdrpwd.setDefaultUnit(kony.flex.DP);
flxbdrpwd.add();
var lblShowPass = new kony.ui.Label({
"height": "100%",
"id": "lblShowPass",
"isVisible": false,
"onTouchEnd": AS_Label_d0dbc00abaa841258ba6ccd6d4dabbef,
"left": "10%",
"skin": "sknBOJFont3SIZE120",
"text": "A",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "10%",
"width": "10%",
"zIndex": 20
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
passwordContainer.add(lblPassword, passwordTextField, lblClosePass, flxbdrpwd, lblShowPass);
var lblInvalidCredentialsKA = new kony.ui.Label({
"centerX": "50%",
"id": "lblInvalidCredentialsKA",
"isVisible": false,
"skin": "sknInvalidCredKA",
"text": kony.i18n.getLocalizedString("i18n.login.invalidCredentials"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "3%",
"width": "86%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var signInButton = new kony.ui.Button({
"centerX": "50.03%",
"focusSkin": "sknprimaryAction",
"height": "16%",
"id": "signInButton",
"isVisible": true,
"onClick": AS_Button_5be6fadb298145d7b15d8be4b19e6dc9,
"skin": "sknprimaryAction",
"text": kony.i18n.getLocalizedString("i18n.login.loginBOJ"),
"top": "8%",
"width": "75%",
"zIndex": 5
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
 var btnCancelLabibaLogin = new kony.ui.Button({
        "centerX": "50.03%",
        "focusSkin": "sknprimaryAction",
        "height": "15%",
        "id": "btnCancelLabibaLogin",
        "isVisible": false,
        "onClick": cancel_labiba_login_process,
        "skin": "sknprimaryAction",
        "text": kony.i18n.getLocalizedString("i18n.login.CancelChatbotLogin"),
        "top": "5%",
        "width": "70%",
        "zIndex": 5
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});  
var flxRegisterMobileBanking = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"focusSkin": "flexTransparent",
"height": "20%",
"id": "flxRegisterMobileBanking",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"onClick": AS_FlexContainer_e9fd6c868a2343f2b998b2cd5abb8bc7,
"skin": "flexTransparent",
"top": "2%",
"width": "100%",
"zIndex": 50
}, {}, {});
flxRegisterMobileBanking.setDefaultUnit(kony.flex.DP);
var lblRegisterMobileBanking = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Register"
},
"centerX": "50%",
"id": "lblRegisterMobileBanking",
"isVisible": true,
"right": "0%",
"skin": "lblsknWhite50transparent",
"text": kony.i18n.getLocalizedString("i18n.login.registerForMobileBank"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxLine = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2dp",
"id": "flxLine",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "0dp",
"width": "75%",
"zIndex": 1
}, {}, {});
flxLine.setDefaultUnit(kony.flex.DP);
flxLine.add();
var CopyflxForgotP0b319deed1ebf4f = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "35%",
"id": "CopyflxForgotP0b319deed1ebf4f",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "20%",
"width": "100%",
"zIndex": 201
}, {}, {});
CopyflxForgotP0b319deed1ebf4f.setDefaultUnit(kony.flex.DP);
var Copyforgotpassword0d579a4c549f649 = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "sknsecactionOnboarding",
"id": "Copyforgotpassword0d579a4c549f649",
"isVisible": true,
"onClick": AS_Button_h855b56f0df641149151a6d17595ce9e,
"skin": "sknsecactionOnboarding",
"text": kony.i18n.getLocalizedString("i18n.onboarding.icon"),
"top": 0,
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 202
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
CopyflxForgotP0b319deed1ebf4f.add(Copyforgotpassword0d579a4c549f649);
flxRegisterMobileBanking.add(lblRegisterMobileBanking, flxLine, CopyflxForgotP0b319deed1ebf4f);
var flxForgotP = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0.08%",
"clipBounds": true,
"height": "10%",
"id": "flxForgotP",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"right": "0.00%",
"top": 0,
"width": "100%",
"zIndex": 201
}, {}, {});
flxForgotP.setDefaultUnit(kony.flex.DP);
var forgotpassword = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "sknsecondaryActionFocus424242KA",
"id": "forgotpassword",
"isVisible": true,
"onClick": AS_Button_hac60c00e8d14eecb5e8a6e5f0c36b36,
"skin": "sknsecaction1",
"text": kony.i18n.getLocalizedString("i18n.common.ForUnamPwd"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 202
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxForgotP.add(forgotpassword);
var flxSwitchLanguage = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxSwitchLanguage",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "2.50%",
"skin": "slFbox",
"top": "-1%",
"width": "95%",
"zIndex": 201
}, {}, {});
flxSwitchLanguage.setDefaultUnit(kony.flex.DP); 
var btnSwitchLanguage = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "sknsecondaryActionFocus424242KA",
"id": "btnSwitchLanguage",
"isVisible": true,
"onClick": AS_Button_cbc225278b204f47afcb29d0de51ae9b,
"skin": "sknsecaction1",
"text": kony.i18n.getLocalizedString("i18n.common.switchlanguage"),
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 202
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxSwitchLanguage.add(btnSwitchLanguage);
loginCard.add(usernameContainer, passwordContainer, lblInvalidCredentialsKA, signInButton, btnCancelLabibaLogin, flxRegisterMobileBanking, flxForgotP, flxSwitchLanguage);
loginCardWrapper.add(loginCard);
var flxPayAppLink = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "6%",
"id": "flxPayAppLink",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "2.50%",
"skin": "slFbox",
"top": "75%",
"width": "95%",
"zIndex": 201
}, {}, {});
flxPayAppLink.setDefaultUnit(kony.flex.DP);
var btnPayyAppLink = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "sknsecondaryActionFocus424242KA",
"id": "btnPayyAppLink",
"isVisible": true,
"onClick": AS_Button_a27fd86b10c84934ba606e4493d4cf32,
"skin": "sknsecaction1",
"text": "BOJ Pay app ",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 202
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxPayAppLink.add(btnPayyAppLink);
var utilityLinks = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "15%",
"id": "utilityLinks",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "40%",
"skin": "sknslFbox",
"top": "80%",
"width": "100%",
"zIndex": 100
}, {}, {});
utilityLinks.setDefaultUnit(kony.flex.DP);
var contactLink = new kony.ui.Button({
"centerY": "49.52%",
"focusSkin": "sknsecondaryActionFocus",
"height": "60dp",
"id": "contactLink",
"isVisible": true,
"right": "3%",
"onClick": AS_Button_328c114136424552afae15f35f5f3837,
"skin": "sknsecondaryAction",
"text": "E",
"width": "60dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var atmLink = new kony.ui.Button({
"centerY": "50.00%",
"focusSkin": "sknsecondaryActionFocus",
"height": "60dp",
"id": "atmLink",
"isVisible": true,
"onClick": AS_Button_99012586285d4090870332cef101ce50,
"left": "55%",
"skin": "sknsecondaryAction",
"text": "B",
"width": "60dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnPinLogin = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "sknsecondaryActionFocus",
"height": "60dp",
"id": "btnPinLogin",
"isVisible": true,
"right": "55%",
"onClick": AS_Button_ie729bbbcca24784a8c1aa45dfff67f7,
"skin": "sknsecondaryAction",
"text": "3",
"width": "60dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnTouchID = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "sknsecondaryActionFocus",
"height": "60dp",
"id": "btnTouchID",
"isVisible": false,
"onClick": AS_Button_bcc85b20219b45e8be7e1a01dbe48752,
"left": "3%",
"skin": "sknsecondaryAction",
"text": "2",
"width": "60dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnFaceID = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "sknfaceidfffff",
"height": "60dp",
"id": "btnFaceID",
"isVisible": true,
"onClick": AS_Button_jb8907903ed843b8b0c1256427d875ee,
"left": "3%",
"skin": "sknfaceidfffff",
"text": "C",
"width": "60dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
utilityLinks.add(contactLink, atmLink, btnPinLogin, btnTouchID, btnFaceID);
var tbxwithcross = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"height": "0%",
"id": "tbxwithcross",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "0%",
"secureTextEntry": false,
"skin": "CopyslTextBox0h7dc1619bbf24d",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "0%",
"width": "0%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var tbxwithoutcross = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"height": "0%",
"id": "tbxwithoutcross",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "0%",
"secureTextEntry": false,
"skin": "CopyslTextBox0df8a5668534249",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "0%",
"width": "0%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
loginMainScreen.add(logoHeader, loginCardWrapper, flxPayAppLink, utilityLinks, tbxwithcross, tbxwithoutcross);
var flxtouchIdAndrd = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxtouchIdAndrd",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_e65d84d3608345028bc9babd5377f420,
"skin": "sknTouch",
"top": "0dp",
"width": "100%",
"zIndex": 5
}, {}, {});
flxtouchIdAndrd.setDefaultUnit(kony.flex.DP);
var flxAndrdTouchAlert = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "60%",
"id": "flxAndrdTouchAlert",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknAndrdTouchId",
"top": "20%",
"width": "80%",
"zIndex": 1
}, {}, {});
flxAndrdTouchAlert.setDefaultUnit(kony.flex.DP);
var flxTIdheader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "20%",
"id": "flxTIdheader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "5%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxTIdheader.setDefaultUnit(kony.flex.DP);
var lblHeaderLine1 = new kony.ui.Label({
"centerX": "49.45%",
"id": "lblHeaderLine1",
"isVisible": true,
"skin": "skn383838LatoRegular107KA",
"text": kony.i18n.getLocalizedString("i18n.touchId.SignIn"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "11dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblHeaderLine2 = new kony.ui.Label({
"centerX": "50%",
"id": "lblHeaderLine2",
"isVisible": false,
"skin": "skn383838LatoRegular107KA",
"text": kony.i18n.getLocalizedString("i18n.for.boj"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxTIdheader.add(lblHeaderLine1, lblHeaderLine2);
var flxTIdImage = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "25%",
"id": "flxTIdImage",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "30%",
"width": "28%",
"zIndex": 1
}, {}, {});
flxTIdImage.setDefaultUnit(kony.flex.DP);
var imgMainImg = new kony.ui.Image2({
"centerX": "50.00%",
"centerY": "50%",
"height": "80%",
"id": "imgMainImg",
"isVisible": true,
"skin": "slImage",
"src": "touch_id_icon.png",
"width": "100%",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxTIdImage.add(imgMainImg);
var flxCloseTId = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "50dp",
"id": "flxCloseTId",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxCloseTId.setDefaultUnit(kony.flex.DP);
var imgCloseTouchId = new kony.ui.Image2({
"centerY": "50%",
"height": "38dp",
"id": "imgCloseTouchId",
"isVisible": true,
"onTouchStart": AS_Image_bb169652645c4ce1abfca9550bbad8ab,
"right": "2%",
"skin": "slImage",
"src": "close.png",
"top": "0dp",
"width": "38dp",
"zIndex": 5
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
flxCloseTId.add(imgCloseTouchId);
var lblTIdMainText = new kony.ui.Label({
"centerX": "50%",
"id": "lblTIdMainText",
"isVisible": true,
"skin": "skn383838LatoRegular107KA",
"text": kony.i18n.getLocalizedString("i18n.login.touch.Text"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "70%",
"width": "95%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxAndrdTouchAlert.add(flxTIdheader, flxTIdImage, flxCloseTId, lblTIdMainText);
var flxTouchIdtryAgain = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "60%",
"id": "flxTouchIdtryAgain",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknAndrdTouchId",
"top": "20%",
"width": "80%",
"zIndex": 1
}, {}, {});
flxTouchIdtryAgain.setDefaultUnit(kony.flex.DP);
var flxTidRetryHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "20%",
"id": "flxTidRetryHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "5%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxTidRetryHeader.setDefaultUnit(kony.flex.DP);
var lblTIdHeader3 = new kony.ui.Label({
"centerX": "49.45%",
"id": "lblTIdHeader3",
"isVisible": true,
"skin": "skn383838LatoRegular107KA",
"text": kony.i18n.getLocalizedString("i18n.touchId.SignIn"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "11dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblTHeader4 = new kony.ui.Label({
"centerX": "50%",
"id": "lblTHeader4",
"isVisible": true,
"skin": "skn383838LatoRegular107KA",
"text": kony.i18n.getLocalizedString("i18n.for.boj"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxTidRetryHeader.add(lblTIdHeader3, lblTHeader4);
var flxCloseTTryagain = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "50dp",
"id": "flxCloseTTryagain",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxCloseTTryagain.setDefaultUnit(kony.flex.DP);
var imgCloseTt = new kony.ui.Image2({
"height": "38dp",
"id": "imgCloseTt",
"isVisible": true,
"onTouchStart": AS_Image_bb169652645c4ce1abfca9550bbad8ab,
"right": "2%",
"skin": "slImage",
"src": "close.png",
"top": "0dp",
"width": "38dp",
"zIndex": 5
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
flxCloseTTryagain.add(imgCloseTt);
var flxTAgainImg = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "25%",
"id": "flxTAgainImg",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "30%",
"width": "28%",
"zIndex": 1
}, {}, {});
flxTAgainImg.setDefaultUnit(kony.flex.DP);
var imgT = new kony.ui.Image2({
"centerX": "50.00%",
"centerY": "50%",
"height": "80%",
"id": "imgT",
"isVisible": true,
"skin": "slImage",
"src": "touch_id_icon.png",
"width": "100%",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxTAgainImg.add(imgT);
var lblTryAgain = new kony.ui.Label({
"centerX": "50%",
"id": "lblTryAgain",
"isVisible": true,
"maxNumberOfLines": 2,
"skin": "sknInvalidCredKA",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "55%",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblTagianText = new kony.ui.Label({
"centerX": "50%",
"id": "lblTagianText",
"isVisible": true,
"skin": "skn383838LatoRegular107KA",
"text": kony.i18n.getLocalizedString("i18n.login.touch.Text"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "70%",
"width": "95%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxTouchIdtryAgain.add(flxTidRetryHeader, flxCloseTTryagain, flxTAgainImg, lblTryAgain, lblTagianText);
var flxlogo = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": false,
"height": "15%",
"id": "flxlogo",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 100
}, {}, {});
flxlogo.setDefaultUnit(kony.flex.DP);
var CopyimgLogo0db1e9b6a3c814f = new kony.ui.Image2({
"centerX": "25%",
"centerY": "50%",
"height": "80%",
"id": "CopyimgLogo0db1e9b6a3c814f",
"isVisible": true,
"maxWidth": "40%",
"skin": "sknslImage",
"src": "logo.png",
"top": "10%",
"width": "50%",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
flxlogo.add(CopyimgLogo0db1e9b6a3c814f);
flxtouchIdAndrd.add(flxAndrdTouchAlert, flxTouchIdtryAgain, flxlogo);
var loginMainPINScreen = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50.00%",
"centerY": "49.98%",
"clipBounds": false,
"height": "100%",
"id": "loginMainPINScreen",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": 0,
"skin": "CopysknAndrdTouchId0jc168d327f7a47",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
loginMainPINScreen.setDefaultUnit(kony.flex.DP);
var PinEntryLabel = new kony.ui.Label({
"centerX": "50%",
"id": "PinEntryLabel",
"isVisible": true,
"skin": "sknLblWhite100",
"text": kony.i18n.getLocalizedString("i18n.common.EnterPin"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "2%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var pinReTry = new kony.ui.Label({
"centerX": "50%",
"id": "pinReTry",
"includeFontPadding": false,
"isVisible": true,
"maxNumberOfLines": 2,
"skin": "CopysknInvalidCredKA0g10f8d36a5e842",
"text": " ",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "0.50%",
"width": "95%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxProgressButtons = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "4%",
"id": "flxProgressButtons",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "25%",
"skin": "skncontainerBkgheader",
"top": "2%",
"width": "60%",
"zIndex": 1
}, {}, {});
flxProgressButtons.setDefaultUnit(kony.flex.DP);
var flxProgressButton1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "51%",
"clipBounds": true,
"height": "15dp",
"id": "flxProgressButton1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "6%",
"skin": "sknFlxProgressButtonEmpty",
"top": "0%",
"width": "15dp",
"zIndex": 1
}, {}, {});
flxProgressButton1.setDefaultUnit(kony.flex.DP);
flxProgressButton1.add();
var flxProgressButton6 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "15dp",
"id": "flxProgressButton6",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "81%",
"skin": "sknFlxProgressButtonEmpty",
"top": "0%",
"width": "15dp",
"zIndex": 1
}, {}, {});
flxProgressButton6.setDefaultUnit(kony.flex.DP);
flxProgressButton6.add();
var flxProgressButton5 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "15dp",
"id": "flxProgressButton5",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "66%",
"skin": "sknFlxProgressButtonEmpty",
"top": "0%",
"width": "15dp",
"zIndex": 1
}, {}, {});
flxProgressButton5.setDefaultUnit(kony.flex.DP);
flxProgressButton5.add();
var flxProgressButton4 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "15dp",
"id": "flxProgressButton4",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "51%",
"skin": "sknFlxProgressButtonEmpty",
"top": "0%",
"width": "15dp",
"zIndex": 1
}, {}, {});
flxProgressButton4.setDefaultUnit(kony.flex.DP);
flxProgressButton4.add();
var flxProgressButton3 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "15dp",
"id": "flxProgressButton3",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "36%",
"skin": "sknFlxProgressButtonEmpty",
"top": "0%",
"width": "15dp",
"zIndex": 1
}, {}, {});
flxProgressButton3.setDefaultUnit(kony.flex.DP);
flxProgressButton3.add();
var flxProgressButton2 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "15dp",
"id": "flxProgressButton2",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "21%",
"skin": "sknFlxProgressButtonEmpty",
"top": "0%",
"width": "15dp",
"zIndex": 1
}, {}, {});
flxProgressButton2.setDefaultUnit(kony.flex.DP);
flxProgressButton2.add();
flxProgressButtons.add(flxProgressButton1, flxProgressButton6, flxProgressButton5, flxProgressButton4, flxProgressButton3, flxProgressButton2);
var flxDialPad = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "66%",
"id": "flxDialPad",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "skncontainerBkgheader",
"top": "2%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxDialPad.setDefaultUnit(kony.flex.DP);
var btnOne = new kony.ui.Button({
"centerX": "22%",
"focusSkin": "btnDialpadSkinOnFocus",
"height": "75dp",
"id": "btnOne",
"isVisible": true,
"right": "7%",
"onClick": AS_Button_5ec3ae74bdaf45e59ec4701e84828451,
"skin": "btnNumber",
"text": "1",
"top": "2%",
"width": "75dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnFour = new kony.ui.Button({
"centerX": "22%",
"focusSkin": "btnDialpadSkinOnFocus",
"height": "75dp",
"id": "btnFour",
"isVisible": true,
"right": "7%",
"onClick": AS_Button_53decc07f347491c93521520794d177c,
"skin": "btnNumber",
"text": "4",
"top": "27.50%",
"width": "75dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnSeven = new kony.ui.Button({
"centerX": "22%",
"focusSkin": "btnDialpadSkinOnFocus",
"height": "75dp",
"id": "btnSeven",
"isVisible": true,
"right": "7%",
"onClick": AS_Button_9b55ca3239a34d3683602ff569e3c92a,
"skin": "btnNumber",
"text": "7",
"top": "52.50%",
"width": "75dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnZero = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "btnDialpadSkinOnFocus",
"height": "75dp",
"id": "btnZero",
"isVisible": true,
"onClick": AS_Button_33aa4862773945b4beb9adf0c05e4609,
"skin": "btnNumber",
"text": "0",
"top": "77.50%",
"width": "75dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnTwo = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "btnDialpadSkinOnFocus",
"height": "75dp",
"id": "btnTwo",
"isVisible": true,
"onClick": AS_Button_22217b3760214733b960c4365e49f74b,
"skin": "btnNumber",
"text": "2",
"top": "2%",
"width": "75dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnFive = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "btnDialpadSkinOnFocus",
"height": "75dp",
"id": "btnFive",
"isVisible": true,
"onClick": AS_Button_4aa5fc82a93145a4b707109a79ac12bf,
"skin": "btnNumber",
"text": "5",
"top": "27.50%",
"width": "75dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnEight = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "btnDialpadSkinOnFocus",
"height": "75dp",
"id": "btnEight",
"isVisible": true,
"onClick": AS_Button_2b503b1cb0f4476f8741722dc3d9a9c4,
"skin": "btnNumber",
"text": "8",
"top": "52.50%",
"width": "75dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnThree = new kony.ui.Button({
"centerX": "78%",
"focusSkin": "btnDialpadSkinOnFocus",
"height": "75dp",
"id": "btnThree",
"isVisible": true,
"onClick": AS_Button_2b6802318f874b53abd83abdb115149a,
"left": "7%",
"skin": "btnNumber",
"text": "3",
"top": "1.96%",
"width": "75dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnSix = new kony.ui.Button({
"centerX": "78%",
"focusSkin": "btnDialpadSkinOnFocus",
"height": "75dp",
"id": "btnSix",
"isVisible": true,
"onClick": AS_Button_a4639a31d035446a9e66e7e2423ed89a,
"left": "7%",
"skin": "btnNumber",
"text": "6",
"top": "27.50%",
"width": "75dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnNine = new kony.ui.Button({
"centerX": "78%",
"focusSkin": "btnDialpadSkinOnFocus",
"height": "75dp",
"id": "btnNine",
"isVisible": true,
"onClick": AS_Button_034b32bce6284183bdc7842ae8376d9b,
"left": "7%",
"skin": "btnNumber",
"text": "9",
"top": "52.46%",
"width": "75dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxDialPad.add(btnOne, btnFour, btnSeven, btnZero, btnTwo, btnFive, btnEight, btnThree, btnSix, btnNine);
var flxUtilbtns = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "10%",
"id": "flxUtilbtns",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknslFbox",
"top": "0%",
"width": "100%"
}, {}, {});
flxUtilbtns.setDefaultUnit(kony.flex.DP);
var btnCancel = new kony.ui.Button({
"focusSkin": "sknBtn4cc7e6x",
"height": "90%",
"id": "btnCancel",
"isVisible": true,
"right": "5%",
"onClick": AS_Button_13469d8209c84ca6b7f8647821063d72,
"skin": "sknprimaryActionFcs",
"text": kony.i18n.getLocalizedString("i18n.common.cancelC"),
"top": "5%",
"width": "40%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var clearLink = new kony.ui.Button({
"focusSkin": "sknBtn4cc7e6x",
"height": "90%",
"id": "clearLink",
"isVisible": true,
"right": "55%",
"onClick": AS_Button_2bebe10c50834570887b1bb9692f4413,
"skin": "sknprimaryAction",
"text": kony.i18n.getLocalizedString("i18n.pinLogin.clear"),
"top": "5%",
"width": "40%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxUtilbtns.add(btnCancel, clearLink);
loginMainPINScreen.add(PinEntryLabel, pinReTry, flxProgressButtons, flxDialPad, flxUtilbtns);
var loginfacialAuthScreen = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "0%",
"centerY": "0%",
"clipBounds": true,
"height": "0%",
"id": "loginfacialAuthScreen",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"skin": "sknAndrdTouchId",
"width": "0%",
"zIndex": 50
}, {}, {});
loginfacialAuthScreen.setDefaultUnit(kony.flex.DP);
var logInFacialAuthScreen1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"centerY": "50%",
"clipBounds": true,
"height": "100%",
"id": "logInFacialAuthScreen1",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"skin": "sknAndrdTouchId",
"width": "100%",
"zIndex": 50
}, {}, {});
logInFacialAuthScreen1.setDefaultUnit(kony.flex.DP);
var CopyLabel0bb54a46fada348 = new kony.ui.Label({
"centerX": "50%",
"id": "CopyLabel0bb54a46fada348",
"isVisible": true,
"skin": "sknBtnBlue",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "10%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyImage0d1f28bec43d04c = new kony.ui.Image2({
"centerX": "50%",
"height": "40%",
"id": "CopyImage0d1f28bec43d04c",
"isVisible": false,
"skin": "slImage",
"src": "cameraface.png",
"top": "15%",
"width": "80%",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyButton0baa14173445846 = new kony.ui.Button({
"bottom": "25%",
"centerX": "50%",
"focusSkin": "sknsecondaryActionFocus",
"height": "60dp",
"id": "CopyButton0baa14173445846",
"isVisible": false,
"onClick": AS_Button_c53e4c172e5f4bb991b5237cfe9a6695,
"skin": "sknsecondaryAction",
"top": 10,
"width": "74%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyButton0i6a3444a98ef40 = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "sknsecondaryActionFocus",
"height": "40dp",
"id": "CopyButton0i6a3444a98ef40",
"isVisible": false,
"onClick": AS_Button_e67835aa99604eaab264e203778ca033,
"skin": "sknsecondaryAction",
"top": "-129dp",
"width": "74%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
logInFacialAuthScreen1.add(CopyLabel0bb54a46fada348, CopyImage0d1f28bec43d04c, CopyButton0baa14173445846, CopyButton0i6a3444a98ef40);
var loginFacialAuthScreen2 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"centerY": "50%",
"clipBounds": true,
"height": "100%",
"id": "loginFacialAuthScreen2",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"skin": "sknAndrdTouchId",
"width": "100%",
"zIndex": 50
}, {}, {});
loginFacialAuthScreen2.setDefaultUnit(kony.flex.DP);
var Image0d3b64fe00b8a44 = new kony.ui.Image2({
"centerX": "50%",
"height": "50dp",
"id": "Image0d3b64fe00b8a44",
"isVisible": false,
"skin": "slImage",
"src": "closeicon.png",
"top": "30%",
"width": "50dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var lblTimedOut = new kony.ui.Label({
"centerX": "50%",
"id": "lblTimedOut",
"isVisible": false,
"right": "0%",
"skin": "sknonboardingHeader",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "3%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lbldesc = new kony.ui.Label({
"centerX": "50%",
"id": "lbldesc",
"isVisible": false,
"right": "0dp",
"skin": "sknonboardingText",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "2%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblDesc1 = new kony.ui.Label({
"centerX": "50%",
"height": "5%",
"id": "lblDesc1",
"isVisible": false,
"skin": "sknonboardingText",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var btnRetry = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "sknsecondaryActionFocus",
"height": "9%",
"id": "btnRetry",
"isVisible": false,
"skin": "sknprimaryAction",
"top": "20%",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnErrorCancel = new kony.ui.Button({
"centerX": "49.92%",
"focusSkin": "sknsecondaryActionFocus",
"height": "45dp",
"id": "btnErrorCancel",
"isVisible": false,
"skin": "sknsecondaryAction",
"top": "0%",
"width": "74%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
loginFacialAuthScreen2.add(Image0d3b64fe00b8a44, lblTimedOut, lbldesc, lblDesc1, btnRetry, btnErrorCancel);
loginfacialAuthScreen.add(logInFacialAuthScreen1, loginFacialAuthScreen2);
apScrollEnable.add(loginMainScreen, flxtouchIdAndrd, loginMainPINScreen, loginfacialAuthScreen);
var apScrollDisable = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": true,
"allowVerticalBounce": false,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"focusSkin": "sknflxAccounts",
"height": "100%",
"horizontalScrollIndicator": false,
"id": "apScrollDisable",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "-100%",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_HORIZONTAL,
"skin": "sknflxAccounts",
"top": "0dp",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1000
}, {}, {});
apScrollDisable.setDefaultUnit(kony.flex.DP);
var accountPreviewScreen = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "accountPreviewScreen",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"left": 0,
"skin": "sknslFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
accountPreviewScreen.setDefaultUnit(kony.flex.DP);
var flxHeaderDashboard = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "7%",
"id": "flxHeaderDashboard",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 9999
}, {}, {});
flxHeaderDashboard.setDefaultUnit(kony.flex.DP);
var flxDisabe = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxDisabe",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "55%",
"onClick": AS_FlexContainer_jb28c050cc2740d58d6b0f06aa21f286,
"right": "0%",
"skin": "slFbox",
"top": "0%",
"width": "45%",
"zIndex": 1
}, {}, {});
flxDisabe.setDefaultUnit(kony.flex.DP);
var Button0c0ff90fa1c134f = new kony.ui.Button({
"focusSkin": "BtnNotificationMail",
"height": "50dp",
"id": "Button0c0ff90fa1c134f",
"isVisible": false,
"left": "10dp",
"skin": "BtnNotificationMail",
"text": "Y",
"top": "0dp",
"width": "42dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
var CopyButton0e1741010299c42 = new kony.ui.Button({
"focusSkin": "btnUser",
"height": "50dp",
"id": "CopyButton0e1741010299c42",
"isVisible": false,
"left": "5dp",
"skin": "btnUser",
"text": "F",
"top": "0dp",
"width": "40dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
var btnDisableFeatire = new kony.ui.Button({
"focusSkin": "btnDisableQB0a189e7774f9c4f",
"height": "100%",
"id": "btnDisableFeatire",
"isVisible": true,
"left": "0%",
"onClick": AS_Button_a0fb44d6d63c4d2b991d78e3da3eb26e,
"skin": "btnDisableQB",
"text": kony.i18n.getLocalizedString("i18n.common.Disablethisfeature"),
"top": "0%",
"width": "86%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
var lblBackIcon = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"height": "100%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0%",
"right": "0%",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.o"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": "14%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxDisabe.add(Button0c0ff90fa1c134f, CopyButton0e1741010299c42, btnDisableFeatire, lblBackIcon);
var Image0c5157670502748 = new kony.ui.Image2({
"height": "30dp",
"id": "Image0c5157670502748",
"isVisible": false,
"left": "15dp",
"skin": "slImage",
"src": "logo03.png",
"top": "5dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
flxHeaderDashboard.add(flxDisabe, Image0c5157670502748);
var accountPreviewCard = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "75%",
"id": "accountPreviewCard",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknflxTransprnt",
"top": "0%",
"width": "100%",
"zIndex": 9999
}, {}, {});
accountPreviewCard.setDefaultUnit(kony.flex.DP);
var lblAcntPreview = new kony.ui.Label({
"height": "10%",
"id": "lblAcntPreview",
"isVisible": true,
"right": "16dp",
"skin": "sknlblwhitecario",
"text": kony.i18n.getLocalizedString("i18n.accounts.accounts"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 10000
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lbltimeStampKA = new kony.ui.Label({
"centerX": "50%",
"id": "lbltimeStampKA",
"isVisible": false,
"right": 0,
"skin": "sknaccountAvailableBalanceLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "2dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxAccountsSegment = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bottom": "0%",
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "85%",
"horizontalScrollIndicator": true,
"id": "flxAccountsSegment",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"pagingEnabled": true,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "5%",
"verticalScrollIndicator": false,
"width": "100%",
"zIndex": 1
}, {}, {});
flxAccountsSegment.setDefaultUnit(kony.flex.DP);
var segAccountPreviewKA = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"data": [
[{
"CopylblIncommingRing0ac2c9e9c52d147": "L",
"Label0bed5640cdb4b46": "Label",
"amountAccount1": "2,254.00",
"amountcurrBal": "$00.00",
"amtOutsatndingBal": "$00.00",
"btnNav": "L",
"dummyAccountName": "",
"dummyAccountNumber": "***154",
"isPFMLabel": "",
"lblAccNumber": "***154",
"lblAccountID": "Label",
"lblBankName": "",
"lblColorKA": "",
"lblCurrency": "JOD",
"lblIncommingRing": "L",
"lblMobile": "S",
"lblRowSeparator": "",
"nameAccount1": "RichText",
"nameAccount12": "Salary account",
"typeAccount": "Available Balance",
"typeKA": ""
},
[{
"Button0hf394843f2a54f": "o",
"lblTransAmount": "-130.201 JOD",
"lblTransDate": "Today, 5:00 PM",
"lblTransId": "Label",
"lblTransName": "Jardon"
}, {
"Button0hf394843f2a54f": "o",
"lblTransAmount": "-130.201 JOD",
"lblTransDate": "Today, 5:00 PM",
"lblTransId": "",
"lblTransName": "Jardon gas"
}]
]
],
"groupCells": false,
"height": "100%",
"id": "segAccountPreviewKA",
"isVisible": true,
"right": "0dp",
"needPageIndicator": true,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "seg2Normal",
"rowSkin": "seg2Normal",
"rowTemplate": flxTrans,
"scrollingEvents": {},
"sectionHeaderTemplate": yourAccount1,
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "f7f7f764",
"separatorRequired": true,
"separatorThickness": 0,
"showScrollbars": true,
"top": "0dp",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"Button0hf394843f2a54f": "Button0hf394843f2a54f",
"CopylblIncommingRing0ac2c9e9c52d147": "CopylblIncommingRing0ac2c9e9c52d147",
"Label0bed5640cdb4b46": "Label0bed5640cdb4b46",
"amountAccount1": "amountAccount1",
"amountcurrBal": "amountcurrBal",
"amtOutsatndingBal": "amtOutsatndingBal",
"btnNav": "btnNav",
"btnNav1": "btnNav1",
"dummyAccountName": "dummyAccountName",
"dummyAccountNumber": "dummyAccountNumber",
"flxLine": "flxLine",
"flxSet": "flxSet",
"flxTrans": "flxTrans",
"flxamount": "flxamount",
"isPFMLabel": "isPFMLabel",
"lblAccNumber": "lblAccNumber",
"lblAccountID": "lblAccountID",
"lblBankName": "lblBankName",
"lblColorKA": "lblColorKA",
"lblCurrency": "lblCurrency",
"lblIncommingRing": "lblIncommingRing",
"lblMobile": "lblMobile",
"lblNoData": "lblNoData",
"lblRowSeparator": "lblRowSeparator",
"lblTransAmount": "lblTransAmount",
"lblTransDate": "lblTransDate",
"lblTransId": "lblTransId",
"lblTransName": "lblTransName",
"nameAccount1": "nameAccount1",
"nameAccount12": "nameAccount12",
"nameContainer": "nameContainer",
"typeAccount": "typeAccount",
"typeKA": "typeKA",
"yourAccount1": "yourAccount1"
},
"width": "100%",
"zIndex": 1
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxAccountsSegment.add(segAccountPreviewKA);
var flxNodata = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50.00%",
"clipBounds": true,
"height": "250dp",
"id": "flxNodata",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skngenericCard",
"top": "20dp",
"width": "90%",
"zIndex": 1
}, {}, {});
flxNodata.setDefaultUnit(kony.flex.DP);
var lblNodatafetched = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "lblNodatafetched",
"isVisible": true,
"right": 0,
"skin": "sknErrorMessageEC223BKA",
"text": "The app is offline",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblErrorData = new kony.ui.Label({
"centerX": "50%",
"centerY": "58%",
"id": "lblErrorData",
"isVisible": true,
"right": 0,
"skin": "sknErrorMessageEC223BKA",
"text": "The app is offline",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxNodata.add(lblNodatafetched, lblErrorData);
var lblinstructionKA = new kony.ui.Label({
"bottom": 0,
"centerX": "50%",
"id": "lblinstructionKA",
"isVisible": false,
"right": 0,
"skin": "sknaccountAvailableBalanceLabel",
"text": kony.i18n.getLocalizedString("i18n.account.accountPreviewCanBeDisabled"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var LabelNoRecordsKA = new kony.ui.Label({
"centerX": "50%",
"centerY": "15.50%",
"id": "LabelNoRecordsKA",
"isVisible": false,
"skin": "skn383838LatoRegular107KA",
"text": kony.i18n.getLocalizedString("i18n.alerts.NoAccountsMsg"),
"width": "65%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
accountPreviewCard.add(lblAcntPreview, lbltimeStampKA, flxAccountsSegment, flxNodata, lblinstructionKA, LabelNoRecordsKA);
var CopyaccountPreviewButton0e6f20970abfd40 = new kony.ui.Button({
"centerX": "50.03%",
"focusSkin": "sknprimaryAction",
"height": "9%",
"id": "CopyaccountPreviewButton0e6f20970abfd40",
"isVisible": true,
"onClick": AS_Button_67a0d29f0fd947068f4afea2c5778d2c,
"skin": "sknprimaryAction",
"text": kony.i18n.getLocalizedString("i18n.login.loginToMobileBank"),
"top": "3%",
"width": "75%",
"zIndex": 9
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
 var flxIconMove = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "flxIconMove",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "75%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_dccde2f492b5488180aef45913449974,
        "skin": "slFbox",
        "top": "4%",
        "width": "60dp",
        "zIndex": 1000
    }, {
        "retainFlexPositionProperties": true,
        "retainContentAlignment": false,
        "retainFlowHorizontalAlignment": false
    }, {});
    flxIconMove.setDefaultUnit(kony.flex.DP);
    var chatbotIcon = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "150%",
        "id": "chatbotIcon",
        "isVisible": true,
        "skin": "slImage",
        "src": "chatbotico2.png",
        "width": "150%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxIconMove.add(chatbotIcon);  
accountPreviewScreen.add(flxHeaderDashboard, accountPreviewCard, CopyaccountPreviewButton0e6f20970abfd40);
apScrollDisable.add(accountPreviewScreen);
frmLoginKA.add(apScrollEnable, apScrollDisable, flxIconMove);
};
function frmLoginKAGlobalsAr() {
frmLoginKAAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmLoginKAAr,
"bounces": false,
"enableScrolling": false,
"enabledForIdleTimeout": false,
"id": "frmLoginKA",
"init": AS_Form_e5b00d71e5f040f883b75f4dfe934ce0,
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"onHide": AS_Form_74464c2c52764e5d850c5a24347dece8,
"postShow": AS_Form_43ed594f831e4c9ebca6b1228100b93b,
"preShow": AS_Form_733a752c59cd457298be1e214ef75bd5,
"skin": "sknLoginMainPage"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"inTransitionConfig": {
"formAnimation": 0
},
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_8d8ea179414745f485866a4050a6693d,
"retainScrollPosition": false,
"titleBar": true,
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
