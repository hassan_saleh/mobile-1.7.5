//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:46 EEST 2020
function initializeCopyFBox0b31d439f0d864bAr() {
    CopyFBox0b31d439f0d864bAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "CopyFBox0b31d439f0d864b",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "width": "100%"
    }, {
        "containerWeight": 100
    }, {});
    CopyFBox0b31d439f0d864bAr.setDefaultUnit(kony.flex.DP);
    var moreListLabel = new kony.ui.Label({
        "centerY": "50%",
        "id": "moreListLabel",
        "isVisible": true,
        "right": "10%",
        "text": "Label",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "hExpand": true,
        "margin": [ 1, 1,1, 1],
        "marginInPixel": false,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false
    });
    var moreListDivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "moreListDivider",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "top": "49dp",
        "width": "95%",
        "zIndex": 1
    }, {
        "containerWeight": 100
    }, {});
    moreListDivider.setDefaultUnit(kony.flex.DP);
    moreListDivider.add();
    CopyFBox0b31d439f0d864bAr.add(moreListLabel, moreListDivider);
}
