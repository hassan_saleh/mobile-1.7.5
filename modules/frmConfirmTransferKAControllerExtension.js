/*
 * Controller Extension class for frmConfirmTransferKA
 * Developer can edit the existing methods or can add new methods if required
 *
 */
kony = kony || {};
kony.sdk = kony.sdk || {};
kony.sdk.mvvm = kony.sdk.mvvm || {};
kony.sdk.mvvm.v2 = kony.sdk.mvvm.v2 || {};


/**
 * Creates a new Form Controller Extension.
 * @class frmConfirmTransferKAControllerExtension
 * @param {Object} controllerObj - Form Controller.
 */
kony.sdk.mvvm.frmConfirmTransferKAControllerExtension = Class(kony.sdk.mvvm.BankingAppControllerExtension, {
    constructor: function(controllerObj) {
        this.$class.$super.call(this, controllerObj);
    },
    /** 
     * This method is an entry point for all fetch related flows. Developer can edit.
     * Default implementation fetches data for the form based on form config 
     * @memberof frmConfirmTransferKAControllerExtension#
     */
    fetchData: function() {

        try {
            var scopeObj = this;
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen(" ");
            //Issue in Fetching Data And Fetch Also Not required for this page
            //  this.$class.$superp.fetchData.call(this, success, error); 
            scopeObj.getController().processData([]);
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("Error in fetchData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_FETCH_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_FETCH_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

        function success(response) {
            kony.sdk.mvvm.log.info("success fetching data ", response);
            scopeObj.getController().processData(response);
        }

        function error(err) {
            //Error fetching data
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("In fetchData errorcallback in controller extension ", err);
            var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_FETCH_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_FETCH_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    /** 
     * This method processes fetched data. Developer can edit.
     * Default implementation processes the provided data to required format for bind.
     * @param {Object} data - fetched data. (Default : data map, group id as key and records array as value)
     * @memberof frmConfirmTransferKAControllerExtension#
     * @returns {Object} - processed data
     */
    processData: function(data) {
        try {
            var scopeObj = this;
            var processedData = this.$class.$superp.processData.call(this, data);
            this.getController().bindData(processedData);
            return processedData;
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("Error in processData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_PROCESSDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_PROCESSDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    /** 
     * This method binds the processed data to the form. Developer can edit.
     * Default implementation binds the data to widgets in the form.
     * @param {Object} data - processed data.(Default : data map for each group, widget id as key and widget data as value)
     * @memberof frmConfirmTransferKAControllerExtension#
     */
    bindData: function(data) {

        try {
            var scopeObj = this;
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen(" ");
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();

            // Get newTransfer transaction object from navigation data and perform the bind
            bindNewTransferTransactionObjectToConfirmTransferForm();
            this.getController().showForm();
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("Error in fetchData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_FETCH_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_FETCH_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    /** 
     * This method is entry point for save flow. Developer can edit.
     * Default implementation saves the entity record from the data of widgets defined in form config 
     * @memberof frmConfirmTransferKAControllerExtension#
     */
    saveData: function() {
        // 	var confirmTransferSaveData= function (response){
        //       if(response["status"]=="success"){
        //       kony.print("Perf Log: Saving of transaction service call - Start");
        //     /*frmSuccessFormKA.successIcon.isVisible = false;
        //     frmSuccessFormKA.show();
        //     successFormPreShow(); */
        //     var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        //     //var listController = INSTANCE.getFormController("frmNewTransferKA");
        //     //var transferformModel = listController.getFormModel();
        //     try {
        //       var serviceName = "RBObjects";
        //       var options ={
        //         "access": "online",
        //         "objectName": "RBObjects"
        //       };
        //       var scopeObj = this;
        //       var formmodel = this.getController().getFormModel();
        //       var dataMap = this.getRecordsDataMap.call(this);
        //       var amount = formmodel.getViewAttributeByProperty("transactionAmount","text"); 
        //       var parsedAmount = parseFloat(amount.replace(/[^0-9-.]/g, ''));

        //       var receivedNavObject = getCustomInfoObject("frmConfirmTransferKA", "newTransferTransactionData")
        //       var frequencyType = getFrequencyType(receivedNavObject.transferFrequency);

        //       dataMap[0].amount = parsedAmount;
        //       dataMap[0].scheduledDate = kony.retailBanking.util.formatingDate.getDBDateTimeFormat(receivedNavObject.transferDate,"00:00");
        //       if(receivedNavObject.recurrenceNumberSelectedFlag !==0){
        //          dataMap[0].numberOfRecurrences = receivedNavObject.recurrenceNumberOfTimes;
        //       }
        //       if(receivedNavObject.recurrenceDateRangeSelectedFlag !==0){
        //         dataMap[0].frequencyStartDate = kony.retailBanking.util.formatingDate.getDBDateTimeFormat(receivedNavObject.fromRecurrenceCalDate,"00:00");
        //         dataMap[0].frequencyEndDate = kony.retailBanking.util.formatingDate.getDBDateTimeFormat(receivedNavObject.toRecurrenceCalDate,"00:00");
        //       }

        //       dataMap[0].frequencyType = frequencyType;
        //       dataMap[0].transactionType = receivedNavObject.transferTransactionType;
        //       dataMap[0].transactionId = receivedNavObject.transferTransactionID;
        //       if(receivedNavObject.fromAccountType){
        //         dataMap[0].fromAccountType = receivedNavObject.fromAccountType;
        //       }
        //       if(receivedNavObject.toAccountType){
        //         dataMap[0].toAccountType = receivedNavObject.toAccountType;
        //       }

        //       var contexData = this.getController() && this.getController().getContextData();
        //       var formConfig = this.getController() && this.getController().getConfig();
        //       var formEntity = formConfig && formConfig.getEntity();
        //       var dataObject = new kony.sdk.dto.DataObject(formEntity,dataMap[0]);
        //       var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        //       var modelObj = INSTANCE.getModel(formEntity,serviceName,options); 
        //       var requestOptions = contexData.getRequestOptions();
        //       var primaryKeyArr = modelObj.getValueForProperty("primaryKey");
        //       var recordsDataMap = scopeObj.getRecordsDataMap();
        //       var operation = contexData.getOperationType();
        //       if (!requestOptions) {
        //         requestOptions = {}
        //       }
        //       requestOptions["dataObject"] = dataObject;
        //       function primaryValuesExist(record, primaryKeyArr) {
        //         var result = false;
        //         for (var index in primaryKeyArr) {
        //           if (record.hasOwnProperty(primaryKeyArr[index])) {
        //             result = true
        //           }
        //         }
        //         return result
        //       }                                     
        //       if (isSettingsFlagEnabled("isFaceEnrolled"))
        //       {
        //             var BC = applicationManager.getTransactionBusinessController();
        //             BC.faceIDCall(recordsDataMap,presentationSuccessCallback, presentationErrorCallback);
        //             function presentationSuccessCallback(res)
        //             {
        //                    if(res.authenticationRequired === "true")
        //                    {
        //                       ShowLoadingScreen();
        //                       intializeFacialAuth();
        //                       FaceAuth_initialize2();  
        //                       FaceAuth_verify1();
        //                       function FaceAuth_verify1(){
        //                       faceIdService.verify({
        //                       onSuccess : function(){
        //                                frmSuccessFormKA.successIcon.isVisible = false;
        //                                frmSuccessFormKA.show();
        //                                successFormPreShow();
        //                               if (operation && operation === kony.sdk.mvvm.OperationType.ADD) {
        //                                 modelObj.create(requestOptions, success, error);
        //                               } 
        //                               else if(primaryValuesExist(recordsDataMap, primaryKeyArr))
        //                              {
        //                                modelObj.update(requestOptions, success, error);
        //                              }
        //                              else
        //                              {
        //                                modelObj.update(requestOptions, success, error);
        //                              }                                          
        //                            },
        //                     onFailed : function(e){showErrorFaceauth(e);}
        //                       });
        //                       }                               
        //                   }
        //                   else
        //                   {
        //                       frmSuccessFormKA.successIcon.isVisible = false;
        //                       frmSuccessFormKA.show();
        //                       successFormPreShow();
        //                       if (operation && operation === kony.sdk.mvvm.OperationType.ADD)
        //                       {
        //                         modelObj.create(requestOptions, success, error);
        //                       } 
        //                       else if(primaryValuesExist(recordsDataMap, primaryKeyArr))
        //                       {
        //                         modelObj.update(requestOptions, success, error);
        //                       }
        //                       else{
        //                         modelObj.update(requestOptions, success, error);
        //                       }
        //                   }  
        //             }
        //           function presentationErrorCallback(err)
        //           {
        //             kony.print("error");
        //           }
        //      // alert(res);     
        //       // this.$class.$superp.saveRecords.call(this,dataMap, success, error);
        //       }
        //       else
        //       {
        //                 frmSuccessFormKA.successIcon.isVisible = false;
        //                 frmSuccessFormKA.show();
        //                 successFormPreShow();
        //                 if (operation && operation === kony.sdk.mvvm.OperationType.ADD)
        //                 {
        //                   modelObj.create(requestOptions, success, error);
        //                 } 
        //                 else if(primaryValuesExist(recordsDataMap, primaryKeyArr))
        //                 {
        //                   modelObj.update(requestOptions, success, error);
        //                 }
        //                 else{
        //                   modelObj.update(requestOptions, success, error);
        //                 }
        //          }  
        //        this.$class.$superp.saveRecords.call(this,dataMap, success, error);

        //     } catch (err) {
        //       var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, err);
        //       kony.sdk.mvvm.log.error(exception.toString());
        //     }

        try {
            var scopeObj = this;
            kony.print("Inside Save data of confirmtransfer-->" + JSON.stringify(scopeObj));
            this.$class.$superp.saveData.call(this, success, error);
        } catch (err) {
          
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
            //alert("Exception "+ err);
        }

        //Logging Info
        //     var logObj = [
        //     "", //frmAccount,0
        //     "", //frmAccountBr,1
        //     "", //frmAccountCurr,2
        //     "", //toAccount,3
        //     "", //toAccountBr,4

        //     "", //billtype,5
        //     "", //billerno,6
        //     "", //billername,7
        //     "", //servicetype,8

        //     "", //jomopaytype,9
        //     "", //jomopaytypeval,10
        //     "", //transfercurr,11

        //     "", //transferamt,12
        //     "", //transferType,13
        //     "", //recurenceType,14
        //     "", //recurenceTxnDate,15

        //     "", // reference number16
        //   	"", //status,17
        //     "", // statuscomments,18
        //     "", //description19

        // ];

        logObj[0] = frmConfirmTransferKA.fromAccountNumberKA.text;
        logObj[1] = frmConfirmTransferKA.frmAccBranchCode.text;
        if (gblTModule != "web") {
            logObj[2] = frmNewTransferKA.lblFromAccCurr.text;
            logObj[19] = frmNewTransferKA.txtDesc.text;
        } else {
            logObj[2] = frmWebCharge.lblCurrencyCode.text;
            logObj[19] = "";
        }
        logObj[3] = frmConfirmTransferKA.toAccountNumberKA.text;
        logObj[4] = frmConfirmTransferKA.toAccBranchCode.text;
        logObj[11] = frmConfirmTransferKA.lblTransCur.text;
        logObj[12] = frmConfirmTransferKA.transactionAmount.text;

        if (gblTModule == "own") {
            logObj[13] = "TR_OWN";
        } else if (gblTModule == "web") {
            logObj[13] = "TOPUP";
        } else {
            if (gblSelectedBene.benType == "ITB") {
                logObj[13] = "SM_INT";
            } else if (gblSelectedBene.benType == "DTB") {
                logObj[13] = "SM_DOM";
            } else {
                logObj[13] = "SM_BOJ";
            }
        }

        logObj[14] = frmConfirmTransferKA.lblReccurrenceValue.text;
        logObj[15] = frmConfirmTransferKA.hiddenLblFromDate.text;

        function success(res) {
            //Successfully created record
            // alert(res);
            // 	  kony.print("Perf Log: Saving of transaction service call - End");
            //       kony.sdk.mvvm.log.info("success saving record ", res);
            //       var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
            //       var listController = INSTANCE.getFormController("frmSuccessFormKA");
            //       var navObject = new kony.sdk.mvvm.NavigationObject();
            //       if(res.referenceId !==undefined)
            //         navObject.setCustomInfo("TransactionID","Reference ID:"+res.referenceId);
            //       else
            //         navObject.setCustomInfo("TransactionID","Reference ID:"+res.transactionId);

            //       // kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            //       listController.loadDataAndShowForm(navObject);
            // 	   kony.print("Perf Log: Confirm form -- End");

            // 	 var amount = frmNewTransferKA.amountTextField.text;
            //      var data = externalAccData;       
            //      if(data.isInternationalAccount && data.isInternationalAccount === "true"){
            //         KNYMetricsService.sendCustomMetrics("frmSuccessFormKA", {"TransferInternational":"InternationalTransfer", "paymentType":"Transfer Payment", "TransferStatus":"TransferPayment", "TransferAverageAmount": parseInt(amount)});
            //     }else{
            //        KNYMetricsService.sendCustomMetrics("frmSuccessFormKA", {"paymentType":"Transfer Payment", "TransferStatus":"TransferPayment", "TransferAverageAmount": parseInt(amount)});
            //     }
            if (!isEmpty(res)) {
                kony.print("Success " + res);
                //alert("Success "+JSON.stringify(res));
                kony.application.dismissLoadingScreen();
                kony.sdk.mvvm.log.info("success saving record confirm transfer", res);
                kony.sdk.mvvm.log.info("success saving record confirm transfer", JSON.stringify(res));
                if ((res.ErrorCode === "00000"|| res.ErrorCode == 0) && !isEmpty(res.referenceId)) {
                    //customAlertPopup("Transaction Reference Id",res.referenceId,popupCommonAlertDimiss,"");
                    if (gblTModule != "web") {
                        if (frmConfirmTransferKA.lblReccurrenceValue.text === geti18Value("i18n.Transfers.Instant"))
                        	if(gblSelectedBene.benType === "DTB" || gblSelectedBene.benType === "ITB")
                            	kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.success"), geti18Value("i18n.transfer.INTNATsuccessmsg"), geti18Value("i18n.Bene.GotoAccDash"), geti18nkey("i18n.common.AccountDashboard"), geti18Value("i18n.Transfer.AnotherTransaction"), "MakeAnotherTransfer", "", "", geti18Value("i18n.common.ReferenceId") + " " + res.referenceId);
                            else
                            	kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.success"), geti18Value("i18n.transfers.successTransfer"), geti18Value("i18n.Bene.GotoAccDash"), geti18nkey("i18n.common.AccountDashboard"), geti18Value("i18n.Transfer.AnotherTransaction"), "MakeAnotherTransfer", "", "", geti18Value("i18n.common.ReferenceId") + " " + res.referenceId);
                        else
                            kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.success"), geti18Value("i18n.transfers.SISucc"), geti18Value("i18n.Bene.GotoAccDash"), geti18nkey("i18n.common.AccountDashboard"), geti18Value("i18n.Transfer.AnotherTransaction"), "MakeAnotherTransfer", "", "", geti18Value("i18n.common.ReferenceId") + " " + res.referenceId);
                    } else {
                        kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.success"), geti18Value("i18n.Transfer.succTrans"), geti18Value("i18n.Bene.GotoAccDash"), geti18nkey("i18n.common.AccountDashboard"), geti18Value("i18n.cards.gotoCards"), "Cards", "", "", geti18Value("i18n.common.ReferenceId") + " " + res.referenceId);
                    	if(gblTModule === "web"){
                        	var webtotalamnt = frmConfirmTransferKA.lblAfterBal.text;
                        	kony.retailBanking.globalData.prCardLandinList[customerAccountDetails.currentIndex].card_balance = webtotalamnt.substring(0,(webtotalamnt.length)-4);
                        	frmManageCardsKA.lblSpendCreditLimit.text = webtotalamnt;
                        }
                    }

                    //Logging INfo
                    logObj[16] = res.referenceId;
                    logObj[17] = "SUCCESS";
                    logObj[18] = "Transaction Successful";

                    frmCongratulations.show();
                } else {

                    var Message = getErrorMessage(res.ErrorCode);
                    //alert("Message ::" + Message);
                    if (isEmpty(Message)) {
                        Message = getServiceErrorMessage(res.ErrorCode,"frmConfTraConExtSuc");
                    }
                    //alert("Message after ::" + Message);
                     customAlertPopup(geti18Value("i18n.Bene.Failed"), Message, popupCommonAlertDimiss, "");
                    if (gblTModule != "web") {
                        kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), Message, geti18Value("i18n.Transfer.AnotherTransaction"), "MakeAnotherTransfer", geti18Value("i18n.Bene.GotoAccDash"), geti18nkey("i18n.common.AccountDashboard"));
                    } else {
                        kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), Message, geti18Value("i18n.Bene.GotoAccDash"), geti18nkey("i18n.common.AccountDashboard"), geti18Value("i18n.cards.gotoCards"), "Cards", "");
                    }

                    logObj[16] = "";
                    logObj[17] = "FAILURE";
                    logObj[18] = res.ErrorCode + " " + Message;
                    frmCongratulations.show();
                }

            } else {
                logObj[16] = "";
                logObj[17] = "FAILURE";
                logObj[18] = "";
                customAlertPopup(geti18Value("i18n.Bene.Failed"),
                    geti18Value("i18n.common.somethingwentwrong"),
                    popupCommonAlertDimiss, "");
                kony.application.dismissLoadingScreen();
            }
          gblLaunchModeOBJ.lauchMode = false;
            loggerCall();
          
        }

        function error(err) {
            //alert("err "+err);
                 if (!isEmpty(err)) {
                //alert("err "+JSON.stringify(err));
               /**** customAlertPopup("",
                    geti18Value("i18n.common.somethingwentwrong"),
                    popupCommonAlertDimiss, "");*****/
                kony.application.dismissLoadingScreen();
                kony.sdk.mvvm.log.info("err saving record confirm transfer", err);
                kony.sdk.mvvm.log.info("err saving record confirm transfer", JSON.stringify(err));
            } else {
                /****customAlertPopup("",
                    geti18Value("i18n.common.somethingwentwrong"),
                    popupCommonAlertDimiss, "");****/
                kony.application.dismissLoadingScreen();
            }
			if (gblTModule != "web") {
              kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"), geti18Value("i18n.Transfer.AnotherTransaction"), "MakeAnotherTransfer", geti18Value("i18n.Bene.GotoAccDash"), geti18nkey("i18n.common.AccountDashboard"));
            } else {
              kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"), geti18Value("i18n.Bene.GotoAccDash"), geti18nkey("i18n.common.AccountDashboard"), geti18Value("i18n.cards.gotoCards"), "Cards", "");
            }
            logObj[16] = "";
            logObj[17] = "FAILURE";
            logObj[18] = "";
           gblLaunchModeOBJ.lauchMode = false;
            loggerCall();
            //       //Handle error case
            //      frmSuccessFormKA.successText.text = "Transaction Failed";
            //      frmSuccessFormKA.successTitle.text = "Sorry for the inconvenience.Please try again after some time.";
            //      frmSuccessFormKA.processing.text = "Transaction Processing...";
            //      errorFormPostShow();
            //       kony.sdk.mvvm.log.error("In saveData errorcallback in controller extension ", err);
            //       var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, err);
            //       kony.sdk.mvvm.log.error(exception.toString());
            //       var amount = frmNewTransferKA.amountTextField.text;
            //       var fromAccNumber = frmConfirmTransferKA.fromAccNumberKA.text;
            //       var toAccNumber = frmConfirmTransferKA.fromAccountNumberKA.text;
            //       var fromAccName = frmConfirmTransferKA.transactionFrom.text;
            //       var toAccName = frmConfirmTransferKA.transactionName.text;
            //       KNYMetricsService.sendCustomMetrics("frmSuccessFormKA", {"TransferAmount":amount.toString(),"TransferFromAccName":fromAccName,"TransferFromAccNum":fromAccNumber.toString(),"TransferToAccName":toAccName,"TransferToAccNum":toAccNumber.toString()});
        }
    },

    //     else{
    //       alert(response["status"]);
    //     }
    //   }

    //     var serviceChargesfortransfers = function(resp){
    //        if(resp["status"]=="success")
    //        		secondFactorAuth(this,confirmTransferSaveData);
    //         else 
    //           	alert(resp["status"]+ ": "+ resp["msg"]);
    //     }
    //     serviceCharges(this,serviceChargesfortransfers);
    //   },


    /** 
     * This method is entry point for delete/remove flow. Developer can edit.
     * Default implementation deletes the entity record displayed in form (primary keys are needed)
     * @memberof frmConfirmTransferKAControllerExtension#
     */
    deleteData: function() {
        try {
            var scopeObj = this;
            this.$class.$superp.deleteData.call(this, success, error);
        } catch (err) {
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

        function success(res) {
            //Successfully deleting record
            kony.sdk.mvvm.log.info("success deleting record " + JSON.stringify(res));
        }

        function error(err) {
            //Handle error case
            kony.sdk.mvvm.log.error("In deleteData errorcallback in controller extension ", err);
            var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    /** 
     * This method shows form.
     * @memberof frmConfirmTransferKAControllerExtension#
     */
    showForm: function() {
        try {
            var formmodel = this.getController().getFormModel();
            formmodel.showView();
        } catch (e) {
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SHOWFORM_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SHOWFORM_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    }
});