function setSegmentData(formName)
{
  switch(formName){
    case "frmAccountsLandingKA" :
      popupData = [
        {"lblMenuItem" : i18n_openNewAccount}
      ];
      frmAccountsLandingKA.flxNavSegment1.height="100dp";
      if(kony.retailBanking.globalData.deviceInfo.isIphone())
        frmAccountsLandingKA.flxNavSegment1.centerY="87%";
      else
        frmAccountsLandingKA.flxNavSegment1.centerY="90%";
      frmAccountsLandingKA.segAccountOverViewNav1KA.removeAll();
      frmAccountsLandingKA.segAccountOverViewNav1KA.setData(popupData);
      if(kony.retailBanking.globalData.globals.configPreferences.newAccount==="OFF"){
      hide_openNewAccount(i18n_openNewAccount);
        frmAccountsLandingKA.flxNavSegment1.height="50dp";
   }
      break;
    case "frmAccountInfoKA" :
      var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
      var controller = INSTANCE.getFormController("frmAccountDetailKA");
      var controllerContextData = controller.getContextData();
      if( controllerContextData && controllerContextData.getCustomInfo("selectedAccountObj")){
        var accountData = controllerContextData.getCustomInfo("selectedAccountObj");
        if(accountData["accountType"] == "Checking" || accountData["accountType"] == "Savings"){
          popupData = [
            {"lblMenuItem" : i18n_reorderCheck},
            {"lblMenuItem" : i18n_changeAccountSettings}
          ];
          	frmAccountInfoKA.flxInnerAccntInfoNavOptKA.height="150dp";
  	    	frmAccountInfoKA.flxInnerAccntInfoNavOptKA.centerY="85%";
          	frmAccountInfoKA.segAccntInfoNavOptKA.removeAll();
        	frmAccountInfoKA.segAccntInfoNavOptKA.setData(popupData);
        }
        else{
          	frmAccountInfoKA.segAccntInfoNavOptKA.removeAll();
        	frmAccountInfoKA.flxInnerAccntInfoNavOptKA.height="50dp";
  	    	frmAccountInfoKA.flxInnerAccntInfoNavOptKA.centerY="95%";
        }
        
    }
      break;
	  case "frmAccountInfoEditKA" :
      var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
      var controller = INSTANCE.getFormController("frmAccountDetailKA");
      var controllerContextData = controller.getContextData();
      if( controllerContextData && controllerContextData.getCustomInfo("selectedAccountObj")){
        var accountData = controllerContextData.getCustomInfo("selectedAccountObj");
        if(accountData["accountType"] == "Checking" || accountData["accountType"] == "Savings"){
          popupData = [
            {"lblMenuItem" : i18n_reorderCheck},
            {"lblMenuItem" : i18n_changeAccountSettings}
          ];
          	frmAccountInfoEditKA.copyflxInnerAccntInfoNavOptKA.height="150dp";
  	    	frmAccountInfoEditKA.copyflxInnerAccntInfoNavOptKA.centerY="85%";
         	frmAccountInfoEditKA.copysegAccntInfoNavOptKA.removeAll();
        	frmAccountInfoEditKA.copysegAccntInfoNavOptKA.setData(popupData);
        }
        else{
          	frmAccountInfoEditKA.copysegAccntInfoNavOptKA.removeAll();
          	frmAccountInfoEditKA.copyflxInnerAccntInfoNavOptKA.height="50dp";
  	    	frmAccountInfoEditKA.copyflxInnerAccntInfoNavOptKA.centerY="95%";
        }
       
    }
      break;
    case "frmDepositPayLandingKA" :
      if(kony.retailBanking.globalData.deviceInfo.isIphone())
      	frmDepositPayLandingKA.flxNavSegment1.centerY = "92%";
      else
        frmDepositPayLandingKA.flxNavSegment1.centerY = "95%";
      frmDepositPayLandingKA.flxCheckDepositNavOptKA.isVisible=true;
  }
}


function accountLandingPopup()
{
  frmAccountsLandingKA.flxAccntLandingNavOptKA.isVisible=false;
  var record = frmAccountsLandingKA.segAccountOverViewNav1KA.data[frmAccountsLandingKA.segAccountOverViewNav1KA.selectedRowIndex[1]].lblMenuItem
  switch(record){
    case i18n_openNewAccount : 
      navigateToPickAProduct();
      break;
    case i18n_WithdrawCash:
      var vc = new kony.rb.frmAccountsLandingKAViewController();
      vc.onSelectWithdrawCash(); 
  }
}


function accountInfoEditPopup()
{
  frmAccountInfoEditKA.copyflxAccountInfoNavOptKA.isVisible=false;
  var record = frmAccountInfoEditKA.copysegAccntInfoNavOptKA.data[frmAccountInfoEditKA.copysegAccntInfoNavOptKA.selectedRowIndex[1]].lblMenuItem;
  switch(record){
    case i18n_editNickname : 
      navigateToAccInfoEdit("frmAccountInfoKA","frmAccountInfoEditKA");
      break;
    case i18n_reorderCheck :
      newCheckReOrderPreShow();
      break;

  }
}

function accountInfoPopup()
{
  frmAccountInfoKA.flxAccountInfoNavOptKA.isVisible=false;
  var record = frmAccountInfoKA.segAccntInfoNavOptKA.data[frmAccountInfoKA.segAccntInfoNavOptKA.selectedRowIndex[1]].lblMenuItem;
  switch(record){
    case i18n_editNickname : 
      navigateToAccInfoEdit("frmAccountInfoKA","frmAccountInfoEditKA");
      break;
    case i18n_reorderCheck :
      newCheckReOrderPreShow();
      break;
  }
}

function navOptForAccountDetails(selectedRecord)
{
 var accountType = selectedRecord["accountType"];
  var flagsupportsCashWithdrawal=selectedRecord["supportCardlessCash"];
  var acc = 1;
  kony.retailBanking.globalData.globals.nav_Object = selectedRecord;
  frmAccountDetailKA.segAccountDetailNav1KA.isVisible = false;
  switch(accountType)
    {
      case "CreditCard" :
        popupData = [
        {"lblMenuItem" : i18n_transferMoney},
        {"lblMenuItem" : i18n_payABill},
        {"lblMenuItem" : i18n_payAPerson},
        {"lblMenuItem" : i18n_ManageCard}
      ];
        
        frmAccountDetailKA.flxNavSegment1.height="300dp";
  		frmAccountDetailKA.flxNavSegment1.centerY="70%";
        break;
      case "Savings":
        popupData = [
        {"lblMenuItem" : i18n_payABill},
        {"lblMenuItem" : i18n_reorderCheck}
      ];
        frmAccountDetailKA.flxNavSegment1.height="200dp";
  		frmAccountDetailKA.flxNavSegment1.centerY="80%";
        break;
      case "Checking":
        popupData = [
        {"lblMenuItem" : i18n_transferMoney},
        {"lblMenuItem" : i18n_payAPerson},
        {"lblMenuItem" : i18n_reorderCheck}
      ];
        frmAccountDetailKA.flxNavSegment1.height="250dp";
  		frmAccountDetailKA.flxNavSegment1.centerY="75%";
        break;
        case "Deposit":
        popupData = [];
        frmAccountDetailKA.flxNavSegment1.height="50dp";
  		frmAccountDetailKA.flxNavSegment1.centerY="90%";
        break;
      
        default : acc = 0;
        frmAccountDetailKA.flxNavSegment1.height="100dp";
  		frmAccountDetailKA.flxNavSegment1.centerY="90%";
        
    }
  if(acc)
  {
    if(flagsupportsCashWithdrawal==="1")
          {
            popupData.unshift({"lblMenuItem" : i18n_WithdrawCash});
          }
    frmAccountDetailKA.segAccountDetailNav1KA.isVisible = true;
  	frmAccountDetailKA.segAccountDetailNav1KA.removeAll();
  	frmAccountDetailKA.segAccountDetailNav1KA.setData(popupData);
  }
}

function navOptTransfer(number)
{
 // frmAccountDetailKA.flxAccountDetailNavOptKA.isVisible = false;
  var record=frmAccountDetailKA["MoreHiddenKey"+number].text;
  //var record = frmAccountDetailKA.segAccountDetailNav1KA.data[frmAccountDetailKA.segAccountDetailNav1KA.selectedRowIndex[1]].lblMenuItem;
  switch(record)
  {
    case i18n_payAPerson :
      navigateToNewPayPerson("InitialLanding", null);
      break;
    case i18n_payABill :
      BillPayfromForm="NewBillPay";
    navigateToNewBillPayForm("InitialLanding", null);
      break;
    case i18n_transferMoney :
      navigateToNewTransferForm("InitialLanding", null);
      break;
    case i18n_ManageCard :
      var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
      var controller = INSTANCE.getFormController("frmManageCardsKA");
      var navigationObject = new kony.sdk.mvvm.NavigationObject();
      navigationObject.setRequestOptions("segCardsKA",{"headers":{"session_token":kony.retailBanking.globalData.session_token}});
      controller.performAction("navigateTo",["frmManageCardsKA",navigationObject]);
      break;
    case i18n_reorderCheck :
      newCheckReOrderPreShow();
      break;
       case i18n_WithdrawCash:
      ShowLoadingScreen();
     var vc = new kony.rb.frmAccountDetailKAViewController();
      vc.onSelectCashWithdraw(frmAccountsLandingKA.segAccountsKA.selectedItems[0].lblAccountID); 
      break;
    case i18n_accountDetails:
       var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var frmName = kony.application.getCurrentForm().id;
    var listController = INSTANCE.getFormController(frmName);
    listController.performAction("navigateToAccountsInfo");
      break;
//     case i18n_accountStatements:
//       firstTimeVisitAccountTermsForm();
//       break;
  }
}