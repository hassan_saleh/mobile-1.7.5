//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmEnrolluserLandingKAAr() {
frmEnrolluserLandingKA.setDefaultUnit(kony.flex.DP);
var imgBack = new kony.ui.Image2({
"centerX": "50%",
"centerY": "50%",
"height": "100%",
"id": "imgBack",
"isVisible": false,
"skin": "slImage",
"src": "bg04.png",
"width": "100%",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var flxmainform = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxmainform",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxmainform.setDefaultUnit(kony.flex.DP);
var flxHead = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "12%",
"id": "flxHead",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxHead.setDefaultUnit(kony.flex.DP);
var flxHeadTOp = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "80%",
"id": "flxHeadTOp",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxHeadTOp.setDefaultUnit(kony.flex.DP);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "86%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_ebc4485fae454f7984b20fc84c1d4ab4,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 1
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"centerY": "50%",
"height": "100%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0%",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblBack0i5985fbe3b634e = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"centerY": "50%",
"height": "100%",
"id": "CopylblBack0i5985fbe3b634e",
"isVisible": true,
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBack.add(lblBackIcon, CopylblBack0i5985fbe3b634e);
var lblheader = new kony.ui.Label({
"height": "100%",
"id": "lblheader",
"isVisible": true,
"left": "20%",
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.registration.credentials"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": "60%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxNext = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxNext",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 1
}, {}, {});
flxNext.setDefaultUnit(kony.flex.DP);
var lblNext = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Next Page"
},
"height": "100%",
"id": "lblNext",
"isVisible": true,
"left": "0dp",
"onTouchEnd": AS_Label_e85e01824c7e4fb383689d2d25e8f5ea,
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.login.next"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxNext.add(lblNext);
flxHeadTOp.add(flxBack, lblheader, flxNext);
var flxProgress1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "11%",
"id": "flxProgress1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "65%",
"skin": "CopysknFlxProgress0fd0051ac013643",
"width": "100%",
"zIndex": 2
}, {}, {});
flxProgress1.setDefaultUnit(kony.flex.DP);
flxProgress1.add();
var flxProgress2 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "11%",
"id": "flxProgress2",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknFlxProgress",
"width": "100%",
"zIndex": 1
}, {}, {});
flxProgress2.setDefaultUnit(kony.flex.DP);
flxProgress2.add();
var lbCloseChange = new kony.ui.Label({
"centerX": "92.00%",
"height": "100%",
"id": "lbCloseChange",
"isVisible": false,
"onTouchEnd": AS_Label_i239b1551a444d51bb873749a0442bc7,
"skin": "sknCloseConfirm",
"text": "O",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "-14dp",
"width": "10%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxHead.add(flxHeadTOp, flxProgress1, flxProgress2, lbCloseChange);
var mainContent = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0dp",
"clipBounds": true,
"height": "92%",
"id": "mainContent",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkg",
"top": "50dp",
"width": "100%",
"zIndex": 1
}, {}, {});
mainContent.setDefaultUnit(kony.flex.DP);
var bottomContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0dp",
"clipBounds": true,
"height": "100%",
"id": "bottomContainer",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkg",
"top": "-0.01%",
"width": "100%",
"zIndex": 1
}, {}, {});
bottomContainer.setDefaultUnit(kony.flex.DP);
var tabsWrapper = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "12%",
"id": "tabsWrapper",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
tabsWrapper.setDefaultUnit(kony.flex.DP);
var btnuserdetails = new kony.ui.Button({
"focusSkin": "skntabSelected",
"height": "100%",
"id": "btnuserdetails",
"isVisible": true,
"right": "33.00%",
"onClick": AS_Button_c11204deb8c14301bee2383f69bc1383,
"skin": "skntabDeselected",
"text": kony.i18n.getLocalizedString("i18n.login.btnUserDetails"),
"top": "0dp",
"width": "33%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 1,0, 0],
"paddingInPixel": false
}, {});
var btnterms = new kony.ui.Button({
"focusSkin": "skntabSelected",
"height": "100%",
"id": "btnterms",
"isVisible": true,
"right": "66%",
"onClick": AS_Button_8e16bee4635047e9be529c0ec03b5e8b,
"skin": "skntabDeselected",
"text": kony.i18n.getLocalizedString("i18n.login.btnSecurity"),
"top": "0dp",
"width": "33%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 1,0, 0],
"paddingInPixel": false
}, {});
var btnbasicinfo = new kony.ui.Button({
"focusSkin": "skntabSelected",
"height": "100%",
"id": "btnbasicinfo",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_8c79404b03704c189ae9c6c932014173,
"skin": "skntabSelected",
"text": kony.i18n.getLocalizedString("i18n.login.btnBasicInfo"),
"top": "0dp",
"width": "33%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 1,0, 0],
"paddingInPixel": false
}, {});
var tabDeselectedIndicator = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "tabDeselectedIndicator",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknCopyslFbox00fe82e6f7cc84b",
"top": "90%",
"width": "100%"
}, {}, {});
tabDeselectedIndicator.setDefaultUnit(kony.flex.DP);
tabDeselectedIndicator.add();
var tabSelectedIndicator = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "tabSelectedIndicator",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknCopyslFbox03fbc1653617542",
"top": "90%",
"width": "33%"
}, {}, {});
tabSelectedIndicator.setDefaultUnit(kony.flex.DP);
tabSelectedIndicator.add();
var CopylistDivider0c9d0c3b0715a44 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0dp",
"clipBounds": true,
"height": "1dp",
"id": "CopylistDivider0c9d0c3b0715a44",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
CopylistDivider0c9d0c3b0715a44.setDefaultUnit(kony.flex.DP);
CopylistDivider0c9d0c3b0715a44.add();
tabsWrapper.add(btnuserdetails, btnterms, btnbasicinfo, tabDeselectedIndicator, tabSelectedIndicator, CopylistDivider0c9d0c3b0715a44);
var enrollListsContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "88%",
"id": "enrollListsContainer",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknslFbox",
"top": "12%",
"width": "300%",
"zIndex": 1
}, {}, {});
enrollListsContainer.setDefaultUnit(kony.flex.DP);
var flexbasicinfo = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flexbasicinfo",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "33.30%",
"zIndex": 1
}, {}, {});
flexbasicinfo.setDefaultUnit(kony.flex.DP);
var lblaccountype = new kony.ui.Label({
"height": "32dp",
"id": "lblaccountype",
"isVisible": true,
"right": "5%",
"skin": "sknsectionHeaderLabel",
"text": kony.i18n.getLocalizedString("i18n.login.accounType"),
"top": "3%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lstboxaccountype = new kony.ui.ListBox({
"height": "40dp",
"id": "lstboxaccountype",
"isVisible": true,
"right": "2.50%",
"masterData": [["lb1", kony.i18n.getLocalizedString("i18n.login.frmEnrolluserLandingKA.ListBox03b5234b26e6743.lb1")],["lb2", kony.i18n.getLocalizedString("i18n.login.frmEnrolluserLandingKA.ListBox03b5234b26e6743.lb2")],["lb3", kony.i18n.getLocalizedString("i18n.login.frmEnrolluserLandingKA.ListBox03b5234b26e6743.lb3")],["lb4", kony.i18n.getLocalizedString("i18n.login.frmEnrolluserLandingKA.ListBox03b5234b26e6743.lb4")]],
"selectedKey": "lb3",
"selectedKeyValue": ["lb3", null],
"top": "10.80%",
"width": "270dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"applySkinsToPopup": true,
"dropDownImage": "dropdown.png",
"viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
});
var CopyFlexContainer06af0074ae94f40 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "1dp",
"clipBounds": true,
"height": "1dp",
"id": "CopyFlexContainer06af0074ae94f40",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "skntextFieldDivider",
"top": "19%",
"width": "90%",
"zIndex": 1
}, {}, {});
CopyFlexContainer06af0074ae94f40.setDefaultUnit(kony.flex.DP);
CopyFlexContainer06af0074ae94f40.add();
var enrolluserinfocontainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "250dp",
"id": "enrolluserinfocontainer",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "20%",
"width": "100%",
"zIndex": 1
}, {}, {});
enrolluserinfocontainer.setDefaultUnit(kony.flex.DP);
var lblacntnumber = new kony.ui.Label({
"height": "32dp",
"id": "lblacntnumber",
"isVisible": true,
"right": "5.00%",
"skin": "sknsectionHeaderLabel",
"text": kony.i18n.getLocalizedString("i18n.common.accountNumber"),
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxaccntnumber = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "36dp",
"id": "flxaccntnumber",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"left": "5%",
"skin": "sknslFbox",
"top": "2dp",
"width": "90%",
"zIndex": 1
}, {}, {});
flxaccntnumber.setDefaultUnit(kony.flex.DP);
var tbxaccntnumber = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "skngeneralTextFieldFocus",
"height": 32,
"id": "tbxaccntnumber",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
"right": "0dp",
"placeholder": kony.i18n.getLocalizedString("i18n.common.enterAccountNumber"),
"left": 0,
"secureTextEntry": false,
"skin": "skngeneralTextField",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"top": "0dp"
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var CopyFlexContainer09fbd523e9a6c43 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "1dp",
"clipBounds": true,
"height": "1dp",
"id": "CopyFlexContainer09fbd523e9a6c43",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skntextFieldDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyFlexContainer09fbd523e9a6c43.setDefaultUnit(kony.flex.DP);
CopyFlexContainer09fbd523e9a6c43.add();
flxaccntnumber.add(tbxaccntnumber, CopyFlexContainer09fbd523e9a6c43);
var flxssn = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "80dp",
"id": "flxssn",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"left": "5%",
"skin": "sknslFbox",
"top": "0%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxssn.setDefaultUnit(kony.flex.DP);
var lblssnumber = new kony.ui.Label({
"height": "32dp",
"id": "lblssnumber",
"isVisible": true,
"right": "0%",
"skin": "sknsectionHeaderLabel",
"text": kony.i18n.getLocalizedString("i18n.common.socialSecurityNumber"),
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var tbxssnnumber = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "skngeneralTextFieldFocus",
"height": 38,
"id": "tbxssnnumber",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD,
"right": "0dp",
"placeholder": kony.i18n.getLocalizedString("i18n.login.enterSSN"),
"left": 0,
"secureTextEntry": false,
"skin": "skngeneralTextField",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"top": "32dp"
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var FlexContainer0da5c3167759a45 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "1dp",
"clipBounds": true,
"height": "1dp",
"id": "FlexContainer0da5c3167759a45",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skntextFieldDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
FlexContainer0da5c3167759a45.setDefaultUnit(kony.flex.DP);
FlexContainer0da5c3167759a45.add();
var lblssnumbeErr = new kony.ui.Label({
"height": "32dp",
"id": "lblssnumbeErr",
"isVisible": false,
"left": "5%",
"skin": "sknD0021BFLatoRegular",
"text": "Invalid SSN",
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxssn.add(lblssnumber, tbxssnnumber, FlexContainer0da5c3167759a45, lblssnumbeErr);
var lbldob = new kony.ui.Label({
"height": "32dp",
"id": "lbldob",
"isVisible": true,
"right": "5.00%",
"skin": "sknsectionHeaderLabel",
"text": kony.i18n.getLocalizedString("i18n.login.dateOfBirth"),
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxdob = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "38dp",
"id": "flxdob",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"left": "5%",
"skin": "sknslFbox",
"top": "6dp",
"width": "90%",
"zIndex": 1
}, {}, {});
flxdob.setDefaultUnit(kony.flex.DP);
var calDob = new kony.ui.Calendar({
"calendarIcon": "dropdown.png",
"dateFormat": "dd/MMM/yyyy",
"height": "40dp",
"id": "calDob",
"isVisible": true,
"right": "0dp",
"placeholder": "DD/MMM/YYYY",
"top": "0dp",
"viewConfig": {},
"viewType": constants.CALENDAR_VIEW_TYPE_DEFAULT,
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyFlexContainer0f3582dde3eac40 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "1dp",
"clipBounds": true,
"height": "1dp",
"id": "CopyFlexContainer0f3582dde3eac40",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skntextFieldDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0f3582dde3eac40.setDefaultUnit(kony.flex.DP);
CopyFlexContainer0f3582dde3eac40.add();
var lblDobKA = new kony.ui.Label({
"height": "32dp",
"id": "lblDobKA",
"isVisible": false,
"right": "0%",
"skin": "sknsectionHeaderLabel",
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxdob.add(calDob, CopyFlexContainer0f3582dde3eac40, lblDobKA);
enrolluserinfocontainer.add(lblacntnumber, flxaccntnumber, flxssn, lbldob, flxdob);
var btnnextbasicinfo = new kony.ui.Button({
"focusSkin": "sknprimaryActionFocus",
"height": "40dp",
"id": "btnnextbasicinfo",
"isVisible": true,
"right": "72dp",
"onClick": AS_Button_d205fd16650f45fab5c8bc755bdab631,
"skin": "sknprimaryAction",
"text": kony.i18n.getLocalizedString("i18n.login.next"),
"top": "78.77%",
"width": "60%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btncancelnextinfo = new kony.ui.Button({
"focusSkin": "sknsecondaryActionFocus",
"height": "35dp",
"id": "btncancelnextinfo",
"isVisible": true,
"right": "70dp",
"onClick": AS_Button_5924a25173914bee9891c526b61c7095,
"skin": "sknsecondaryAction",
"text": kony.i18n.getLocalizedString("i18n.common.cancel"),
"top": "89%",
"width": "60%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flexbasicinfo.add(lblaccountype, lstboxaccountype, CopyFlexContainer06af0074ae94f40, enrolluserinfocontainer, btnnextbasicinfo, btncancelnextinfo);
var flxuserdetails = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxuserdetails",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "33.30%",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "33.30%",
"zIndex": 1
}, {}, {});
flxuserdetails.setDefaultUnit(kony.flex.DP);
var flxuserdetailssub = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxuserdetailssub",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxuserdetailssub.setDefaultUnit(kony.flex.DP);
var flxbottombtns = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxbottombtns",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "80%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxbottombtns.setDefaultUnit(kony.flex.DP);
var btnnextuserdetails = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "sknprimaryActionFocus",
"height": "40dp",
"id": "btnnextuserdetails",
"isVisible": true,
"right": "57dp",
"onClick": AS_Button_3f79fe5f9b354b35a72e79011b895ff7,
"skin": "sknprimaryAction",
"text": kony.i18n.getLocalizedString("i18n.login.next"),
"top": "5dp",
"width": "60%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btncanceluserdetails = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "sknsecondaryActionFocus",
"height": "35dp",
"id": "btncanceluserdetails",
"isVisible": true,
"right": "57dp",
"onClick": AS_Button_79d12dd0fd8f4a17b085d793cce11eb5,
"skin": "sknsecondaryAction",
"text": kony.i18n.getLocalizedString("i18n.common.cancel"),
"top": "46%",
"width": "60%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxbottombtns.add(btnnextuserdetails, btncanceluserdetails);
var flxscrolluserdetails = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": true,
"allowVerticalBounce": true,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "80%",
"horizontalScrollIndicator": true,
"id": "flxscrolluserdetails",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "0dp",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
flxscrolluserdetails.setDefaultUnit(kony.flex.DP);
var flxuname = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "36%",
"id": "flxuname",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "skncontainerBkgheader",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {}, {});
flxuname.setDefaultUnit(kony.flex.DP);
var CopyLabel0c929b1e27d9b44 = new kony.ui.Label({
"height": "32dp",
"id": "CopyLabel0c929b1e27d9b44",
"isVisible": true,
"right": "2%",
"skin": "sknsectionHeaderLabel",
"text": kony.i18n.getLocalizedString("i18n.login.usenamePlh"),
"top": "7%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var tbxUsernameKAOld = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "skngeneralTextFieldFocus",
"height": 32,
"id": "tbxUsernameKAOld",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "2%",
"placeholder": kony.i18n.getLocalizedString("i18n.login.enterUsernamePlh"),
"secureTextEntry": false,
"skin": "skngeneralTextField",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "63%",
"width": "260dp"
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var CopyLabel03b3c824e8bd443ontainer0d38e2928c7fe4e = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "1dp",
"clipBounds": true,
"height": "1dp",
"id": "CopyLabel03b3c824e8bd443ontainer0d38e2928c7fe4e",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "2%",
"skin": "skntextFieldDivider",
"top": "89%",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyLabel03b3c824e8bd443ontainer0d38e2928c7fe4e.setDefaultUnit(kony.flex.DP);
CopyLabel03b3c824e8bd443ontainer0d38e2928c7fe4e.add();
var lblinstrn = new kony.ui.Label({
"height": "50dp",
"id": "lblinstrn",
"isVisible": true,
"right": "2%",
"skin": "sknRegisterMobileBank",
"text": kony.i18n.getLocalizedString("i18n.login.note"),
"top": "28%",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblUsernameErr = new kony.ui.Label({
"height": "32dp",
"id": "lblUsernameErr",
"isVisible": false,
"left": "2%",
"skin": "sknD0021BFLatoRegular",
"text": "Invalid Username",
"top": "7%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxuname.add(CopyLabel0c929b1e27d9b44, tbxUsernameKAOld, CopyLabel03b3c824e8bd443ontainer0d38e2928c7fe4e, lblinstrn, lblUsernameErr);
var flxpwd = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50%",
"id": "flxpwd",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "sknslFbox",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {}, {});
flxpwd.setDefaultUnit(kony.flex.DP);
var CopyLabel038c9dce3f8b243 = new kony.ui.Label({
"height": "32dp",
"id": "CopyLabel038c9dce3f8b243",
"isVisible": true,
"right": "2%",
"skin": "sknsectionHeaderLabel",
"text": kony.i18n.getLocalizedString("i18n.login.passwordPlh"),
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var tbxPasswordKAOld = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "skngeneralTextFieldFocus",
"height": 32,
"id": "tbxPasswordKAOld",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "2%",
"maxTextLength": 24,
"placeholder": kony.i18n.getLocalizedString("i18n.login.enterPasswordPlh"),
"secureTextEntry": true,
"skin": "skngeneralTextField",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "50%",
"width": "260dp"
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var CopyLabel03bbaee3179c947 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "1dp",
"clipBounds": true,
"height": "1dp",
"id": "CopyLabel03bbaee3179c947",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "2%",
"skin": "skntextFieldDivider",
"top": "65%",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyLabel03bbaee3179c947.setDefaultUnit(kony.flex.DP);
CopyLabel03bbaee3179c947.add();
var Copylblinstrn0c26faacb159b4f = new kony.ui.Label({
"height": "50dp",
"id": "Copylblinstrn0c26faacb159b4f",
"isVisible": true,
"right": "2%",
"skin": "sknRegisterMobileBank",
"text": kony.i18n.getLocalizedString("i18n.login.note"),
"top": "40dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var tbxConfrmPwdKAOld = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "skngeneralTextFieldFocus",
"height": 32,
"id": "tbxConfrmPwdKAOld",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "2%",
"maxTextLength": 24,
"placeholder": kony.i18n.getLocalizedString("i18n.login.reEnterPasswordPlh"),
"secureTextEntry": true,
"skin": "skngeneralTextField",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "70%",
"width": "260dp"
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var CopyLabel0123e1dc1949143 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "1dp",
"clipBounds": true,
"height": "1dp",
"id": "CopyLabel0123e1dc1949143",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "2%",
"skin": "skntextFieldDivider",
"top": "85%",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyLabel0123e1dc1949143.setDefaultUnit(kony.flex.DP);
CopyLabel0123e1dc1949143.add();
var lblPwdErr = new kony.ui.Label({
"height": "32dp",
"id": "lblPwdErr",
"isVisible": false,
"left": "2%",
"skin": "sknD0021BFLatoRegular",
"text": "Invalid Password",
"top": "7%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxpwd.add(CopyLabel038c9dce3f8b243, tbxPasswordKAOld, CopyLabel03bbaee3179c947, Copylblinstrn0c26faacb159b4f, tbxConfrmPwdKAOld, CopyLabel0123e1dc1949143, lblPwdErr);
var flxemail = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "30%",
"id": "flxemail",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "sknslFbox",
"top": "-10dp",
"width": "90%",
"zIndex": 1
}, {}, {});
flxemail.setDefaultUnit(kony.flex.DP);
var CopyLabel092d7258cc7d340 = new kony.ui.Label({
"height": "32dp",
"id": "CopyLabel092d7258cc7d340",
"isVisible": true,
"right": "2%",
"skin": "sknsectionHeaderLabel",
"text": kony.i18n.getLocalizedString("i18n.common.emailHy"),
"top": "7%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var tbxEmailKA = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "skngeneralTextFieldFocus",
"height": 32,
"id": "tbxEmailKA",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_EMAIL,
"right": "2%",
"placeholder": kony.i18n.getLocalizedString("i18n.login.enterEmailHy"),
"secureTextEntry": false,
"skin": "skngeneralTextField",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "30%",
"width": "260dp"
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var CopyLabel0cee284b2ad414d = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "1dp",
"clipBounds": true,
"height": "1dp",
"id": "CopyLabel0cee284b2ad414d",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "2%",
"skin": "skntextFieldDivider",
"top": "60%",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyLabel0cee284b2ad414d.setDefaultUnit(kony.flex.DP);
CopyLabel0cee284b2ad414d.add();
var lblEmailErr = new kony.ui.Label({
"height": "32dp",
"id": "lblEmailErr",
"isVisible": false,
"left": "2%",
"skin": "sknD0021BFLatoRegular",
"text": "Invalid Email",
"top": "7%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxemail.add(CopyLabel092d7258cc7d340, tbxEmailKA, CopyLabel0cee284b2ad414d, lblEmailErr);
var flxphone = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "30%",
"id": "flxphone",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "sknslFbox",
"top": "-40dp",
"width": "90%",
"zIndex": 1
}, {}, {});
flxphone.setDefaultUnit(kony.flex.DP);
var CopyLabel0b7e45def03a04a = new kony.ui.Label({
"height": "32dp",
"id": "CopyLabel0b7e45def03a04a",
"isVisible": true,
"right": "2%",
"skin": "sknsectionHeaderLabel",
"text": kony.i18n.getLocalizedString("i18n.common.phoneNumber"),
"top": "7%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var tbxPhoneNumberKA = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "skngeneralTextFieldFocus",
"height": "40dp",
"id": "tbxPhoneNumberKA",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD,
"right": "-1%",
"placeholder": kony.i18n.getLocalizedString("i18n.login.enterPhoneNumberPlh"),
"secureTextEntry": false,
"skin": "skngeneralTextField",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"top": "30%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var lblLineKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "1dp",
"clipBounds": true,
"height": "1dp",
"id": "lblLineKA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "6dp",
"skin": "skntextFieldDivider",
"top": "75dp",
"width": "100%",
"zIndex": 1
}, {}, {});
lblLineKA.setDefaultUnit(kony.flex.DP);
lblLineKA.add();
var lblPhErr = new kony.ui.Label({
"height": "32dp",
"id": "lblPhErr",
"isVisible": false,
"left": "2%",
"skin": "sknD0021BFLatoRegular",
"text": "Invalid Phone Number",
"top": "7%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxphone.add(CopyLabel0b7e45def03a04a, tbxPhoneNumberKA, lblLineKA, lblPhErr);
flxscrolluserdetails.add(flxuname, flxpwd, flxemail, flxphone);
flxuserdetailssub.add(flxbottombtns, flxscrolluserdetails);
flxuserdetails.add(flxuserdetailssub);
var flxtsecurity = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxtsecurity",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "66.60%",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "33.30%",
"zIndex": 1
}, {}, {});
flxtsecurity.setDefaultUnit(kony.flex.DP);
var lblsubheading = new kony.ui.Label({
"height": "30dp",
"id": "lblsubheading",
"isVisible": true,
"right": "7%",
"skin": "sknstandardTextBold",
"text": kony.i18n.getLocalizedString("i18n.settings.chooseanswersecretquestions"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxscrollquestions = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": true,
"allowVerticalBounce": true,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "66%",
"horizontalScrollIndicator": true,
"id": "flxscrollquestions",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "sknscrollBkgWhite",
"top": "50dp",
"verticalScrollIndicator": false,
"width": "100%",
"zIndex": 1
}, {}, {});
flxscrollquestions.setDefaultUnit(kony.flex.DP);
var flxquestion1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxquestion1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxquestion1.setDefaultUnit(kony.flex.DP);
var lblquestion1 = new kony.ui.Label({
"height": "22dp",
"id": "lblquestion1",
"isVisible": true,
"right": "20dp",
"skin": "sknsectionHeaderLabel",
"text": kony.i18n.getLocalizedString("i18n.enroll.question1"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5dp",
"width": "100dp"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lstboxquestn1 = new kony.ui.ListBox({
"height": "40dp",
"id": "lstboxquestn1",
"isVisible": true,
"right": "17dp",
"masterData": [["lb1", "Select a quesiton here.."],["lb2", "What is your first vehicle colour?"],["lb3", "What is your first mobile number?"],["Key233", "What is your favourite food?"],["Key857", "Who is your first teacher?"],["Key293", "Who is your best friend?"]],
"selectedKey": "lb1",
"selectedKeyValue": ["lb1", "Select a quesiton here.."],
"skin": "sknlistboxquestions",
"top": "28dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"applySkinsToPopup": true,
"dropDownImage": "dropdown.png",
"viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
});
var CopylblLineKA0f48b291a4b8541 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "1dp",
"clipBounds": true,
"height": "1dp",
"id": "CopylblLineKA0f48b291a4b8541",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "20dp",
"skin": "skntextFieldDivider",
"top": "64dp",
"width": "85%",
"zIndex": 1
}, {}, {});
CopylblLineKA0f48b291a4b8541.setDefaultUnit(kony.flex.DP);
CopylblLineKA0f48b291a4b8541.add();
var tbxAnswer1 = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "skngeneralTextFieldFocus",
"height": "40dp",
"id": "tbxAnswer1",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "12dp",
"placeholder": kony.i18n.getLocalizedString("i18n.common.answerFieldPlaceholder"),
"secureTextEntry": false,
"skin": "skntxtbxanswer",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "66dp",
"width": "260dp",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var CopylblLineKA09883b62c62974b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "1dp",
"clipBounds": true,
"height": "1dp",
"id": "CopylblLineKA09883b62c62974b",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "20dp",
"skin": "skntextFieldDivider",
"top": "100dp",
"width": "85%",
"zIndex": 1
}, {}, {});
CopylblLineKA09883b62c62974b.setDefaultUnit(kony.flex.DP);
CopylblLineKA09883b62c62974b.add();
flxquestion1.add(lblquestion1, lstboxquestn1, CopylblLineKA0f48b291a4b8541, tbxAnswer1, CopylblLineKA09883b62c62974b);
var flxquestion2 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxquestion2",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "105dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxquestion2.setDefaultUnit(kony.flex.DP);
var lblquestion2 = new kony.ui.Label({
"height": "22dp",
"id": "lblquestion2",
"isVisible": true,
"right": "20dp",
"skin": "sknsectionHeaderLabel",
"text": kony.i18n.getLocalizedString("i18n.enroll.question2"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5dp",
"width": "100dp"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lstboxquestn2 = new kony.ui.ListBox({
"height": "40dp",
"id": "lstboxquestn2",
"isVisible": true,
"right": "17dp",
"masterData": [["lb1", "Select a quesiton here.."],["lb2", "What is your first vehicle colour?"],["lb3", "What is your first mobile number?"],["Key233", "What is your favourite food?"],["Key857", "Who is your first teacher?"],["Key293", "Who is your best friend?"]],
"selectedKey": "lb1",
"selectedKeyValue": ["lb1", "Select a quesiton here.."],
"skin": "sknlistboxquestions",
"top": "28dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"applySkinsToPopup": true,
"dropDownImage": "dropdown.png",
"viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
});
var CopylblLineKA077ab14ab13414e = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "1dp",
"clipBounds": true,
"height": "1dp",
"id": "CopylblLineKA077ab14ab13414e",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "20dp",
"skin": "skntextFieldDivider",
"top": "64dp",
"width": "85%",
"zIndex": 1
}, {}, {});
CopylblLineKA077ab14ab13414e.setDefaultUnit(kony.flex.DP);
CopylblLineKA077ab14ab13414e.add();
var tbxAnswer2 = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "skngeneralTextFieldFocus",
"height": "40dp",
"id": "tbxAnswer2",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "12dp",
"placeholder": kony.i18n.getLocalizedString("i18n.common.answerFieldPlaceholder"),
"secureTextEntry": false,
"skin": "skntxtbxanswer",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "66dp",
"width": "260dp",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var CopylblLineKA075b26173b69849 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "1dp",
"clipBounds": true,
"height": "1dp",
"id": "CopylblLineKA075b26173b69849",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "20dp",
"skin": "skntextFieldDivider",
"top": "100dp",
"width": "85%",
"zIndex": 1
}, {}, {});
CopylblLineKA075b26173b69849.setDefaultUnit(kony.flex.DP);
CopylblLineKA075b26173b69849.add();
flxquestion2.add(lblquestion2, lstboxquestn2, CopylblLineKA077ab14ab13414e, tbxAnswer2, CopylblLineKA075b26173b69849);
var flxquestion3 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxquestion3",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "210dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxquestion3.setDefaultUnit(kony.flex.DP);
var lblquestion3 = new kony.ui.Label({
"height": "22dp",
"id": "lblquestion3",
"isVisible": true,
"right": "20dp",
"skin": "sknsectionHeaderLabel",
"text": kony.i18n.getLocalizedString("i18n.enroll.question3"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5dp",
"width": "100dp"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lstboxquestn3 = new kony.ui.ListBox({
"height": "40dp",
"id": "lstboxquestn3",
"isVisible": true,
"right": "17dp",
"masterData": [["lb1", "Select a quesiton here.."],["lb2", "What is your first vehicle colour?"],["lb3", "What is your first mobile number?"],["Key233", "What is your favourite food?"],["Key857", "Who is your first teacher?"],["Key293", "Who is your best friend?"]],
"selectedKey": "lb1",
"selectedKeyValue": ["lb1", "Select a quesiton here.."],
"skin": "sknlistboxquestions",
"top": "28dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"applySkinsToPopup": true,
"dropDownImage": "dropdown.png",
"viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
});
var CopylblLineKA0094e603337474c = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "1dp",
"clipBounds": true,
"height": "1dp",
"id": "CopylblLineKA0094e603337474c",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "20dp",
"skin": "skntextFieldDivider",
"top": "64dp",
"width": "85%",
"zIndex": 1
}, {}, {});
CopylblLineKA0094e603337474c.setDefaultUnit(kony.flex.DP);
CopylblLineKA0094e603337474c.add();
var tbxAnswer3 = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "skngeneralTextFieldFocus",
"height": "40dp",
"id": "tbxAnswer3",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "12dp",
"placeholder": kony.i18n.getLocalizedString("i18n.common.answerFieldPlaceholder"),
"secureTextEntry": false,
"skin": "skntxtbxanswer",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "66dp",
"width": "260dp",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var CopylblLineKA0b98c3838f27c40 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "1dp",
"clipBounds": true,
"height": "1dp",
"id": "CopylblLineKA0b98c3838f27c40",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "20dp",
"skin": "skntextFieldDivider",
"top": "100dp",
"width": "85%",
"zIndex": 1
}, {}, {});
CopylblLineKA0b98c3838f27c40.setDefaultUnit(kony.flex.DP);
CopylblLineKA0b98c3838f27c40.add();
flxquestion3.add(lblquestion3, lstboxquestn3, CopylblLineKA0094e603337474c, tbxAnswer3, CopylblLineKA0b98c3838f27c40);
var flxquestion4 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxquestion4",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "315dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxquestion4.setDefaultUnit(kony.flex.DP);
var lblquestion4 = new kony.ui.Label({
"height": "22dp",
"id": "lblquestion4",
"isVisible": true,
"right": "17dp",
"skin": "sknsectionHeaderLabel",
"text": kony.i18n.getLocalizedString("i18n.enroll.question4"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5dp",
"width": "100dp"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lstboxquestn4 = new kony.ui.ListBox({
"height": "40dp",
"id": "lstboxquestn4",
"isVisible": true,
"right": "17dp",
"masterData": [["lb1", "Select a quesiton here.."],["lb2", "What is your first vehicle colour?"],["lb3", "What is your first mobile number?"],["Key233", "What is your favourite food?"],["Key857", "Who is your first teacher?"],["Key293", "Who is your best friend?"]],
"selectedKey": "lb1",
"selectedKeyValue": ["lb1", "Select a quesiton here.."],
"skin": "sknlistboxquestions",
"top": "28dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"applySkinsToPopup": true,
"dropDownImage": "dropdown.png",
"viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
});
var CopylblLineKA081389386ccb44a = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "1dp",
"clipBounds": true,
"height": "1dp",
"id": "CopylblLineKA081389386ccb44a",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "20dp",
"skin": "skntextFieldDivider",
"top": "64dp",
"width": "85%",
"zIndex": 1
}, {}, {});
CopylblLineKA081389386ccb44a.setDefaultUnit(kony.flex.DP);
CopylblLineKA081389386ccb44a.add();
var tbxAnswer4 = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "skngeneralTextFieldFocus",
"height": "40dp",
"id": "tbxAnswer4",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "12dp",
"placeholder": kony.i18n.getLocalizedString("i18n.common.answerFieldPlaceholder"),
"secureTextEntry": false,
"skin": "skntxtbxanswer",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "66dp",
"width": "260dp",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var CopylblLineKA0812b64794bc248 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "1dp",
"clipBounds": true,
"height": "1dp",
"id": "CopylblLineKA0812b64794bc248",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "20dp",
"skin": "skntextFieldDivider",
"top": "100dp",
"width": "85%",
"zIndex": 1
}, {}, {});
CopylblLineKA0812b64794bc248.setDefaultUnit(kony.flex.DP);
CopylblLineKA0812b64794bc248.add();
flxquestion4.add(lblquestion4, lstboxquestn4, CopylblLineKA081389386ccb44a, tbxAnswer4, CopylblLineKA0812b64794bc248);
var flxquestion5 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxquestion5",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "420dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxquestion5.setDefaultUnit(kony.flex.DP);
var lblquestion5 = new kony.ui.Label({
"height": "22dp",
"id": "lblquestion5",
"isVisible": true,
"right": "20dp",
"skin": "sknsectionHeaderLabel",
"text": kony.i18n.getLocalizedString("i18n.enroll.question5"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5dp",
"width": "100dp"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lstboxquestn5 = new kony.ui.ListBox({
"height": "40dp",
"id": "lstboxquestn5",
"isVisible": true,
"right": "17dp",
"masterData": [["lb1", "Select a quesiton here.."],["lb2", "What is your first vehicle colour?"],["lb3", "What is your first mobile number?"],["Key233", "What is your favourite food?"],["Key857", "Who is your first teacher?"],["Key293", "Who is your best friend?"]],
"selectedKey": "lb1",
"selectedKeyValue": ["lb1", "Select a quesiton here.."],
"skin": "sknlistboxquestions",
"top": "28dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"applySkinsToPopup": true,
"dropDownImage": "dropdown.png",
"viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
});
var CopylblLineKA0d90b2a62526845 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "1dp",
"clipBounds": true,
"height": "1dp",
"id": "CopylblLineKA0d90b2a62526845",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "20dp",
"skin": "skntextFieldDivider",
"top": "64dp",
"width": "85%",
"zIndex": 1
}, {}, {});
CopylblLineKA0d90b2a62526845.setDefaultUnit(kony.flex.DP);
CopylblLineKA0d90b2a62526845.add();
var tbxAnswer5 = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "skngeneralTextFieldFocus",
"height": "40dp",
"id": "tbxAnswer5",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "12dp",
"placeholder": kony.i18n.getLocalizedString("i18n.common.answerFieldPlaceholder"),
"secureTextEntry": false,
"skin": "skntxtbxanswer",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "66dp",
"width": "260dp",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var CopylblLineKA09897f6223ce64a = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "1dp",
"clipBounds": true,
"height": "1dp",
"id": "CopylblLineKA09897f6223ce64a",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "20dp",
"skin": "skntextFieldDivider",
"top": "100dp",
"width": "85%",
"zIndex": 1
}, {}, {});
CopylblLineKA09897f6223ce64a.setDefaultUnit(kony.flex.DP);
CopylblLineKA09897f6223ce64a.add();
flxquestion5.add(lblquestion5, lstboxquestn5, CopylblLineKA0d90b2a62526845, tbxAnswer5, CopylblLineKA09897f6223ce64a);
flxscrollquestions.add(flxquestion1, flxquestion2, flxquestion3, flxquestion4, flxquestion5);
var flxbottombuttons = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bottom": "0dp",
"bounces": false,
"clipBounds": true,
"enableScrolling": false,
"height": 105,
"horizontalScrollIndicator": true,
"id": "flxbottombuttons",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "sknscrollBkgWhite",
"verticalScrollIndicator": false,
"width": "100%",
"zIndex": 1
}, {}, {});
flxbottombuttons.setDefaultUnit(kony.flex.DP);
var btnaccept = new kony.ui.Button({
"centerX": "50.00%",
"focusSkin": "sknprimaryActionFocus",
"height": "40dp",
"id": "btnaccept",
"isVisible": true,
"right": "5dp",
"onClick": AS_Button_245362f65ec44b849f9e57d2ff9aba2a,
"skin": "sknprimaryAction",
"text": kony.i18n.getLocalizedString("i18n.login.btnEnroll"),
"top": "20%",
"width": "60%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnCancel = new kony.ui.Button({
"centerX": "50.00%",
"focusSkin": "sknsecondaryActionFocus",
"height": "30dp",
"id": "btnCancel",
"isVisible": true,
"right": "57dp",
"onClick": AS_Button_a13714471d684162a87cc1ddf4338c79,
"skin": "sknsecondaryAction",
"text": kony.i18n.getLocalizedString("i18n.common.cancel"),
"top": 5,
"width": "60%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxbottombuttons.add(btnaccept, btnCancel);
flxtsecurity.add(lblsubheading, flxscrollquestions, flxbottombuttons);
enrollListsContainer.add(flexbasicinfo, flxuserdetails, flxtsecurity);
bottomContainer.add(tabsWrapper, enrollListsContainer);
mainContent.add(bottomContainer);
var flxUserPass = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": false,
"centerX": "50%",
"clipBounds": true,
"enableScrolling": true,
"height": "88%",
"horizontalScrollIndicator": true,
"id": "flxUserPass",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "CopyslFSbox0f277ffde8c2b44",
"top": "12%",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
flxUserPass.setDefaultUnit(kony.flex.DP);
var lblTitle = new kony.ui.Label({
"centerX": "50%",
"id": "lblTitle",
"isVisible": true,
"skin": "sknLblTitle",
"text": kony.i18n.getLocalizedString("i18n.registration.registerTitle"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "7%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxUsername = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxUsername",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "5%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxUsername.setDefaultUnit(kony.flex.DP);
var lblUsername = new kony.ui.Label({
"id": "lblUsername",
"isVisible": true,
"right": "2%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.login.username"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var tbxUsernameKA = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxUsernameKA",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": null,
"onDone": AS_TextField_e15c7c7bd578455698e45647494ae953,
"onTextChange": AS_TextField_g8c4558d61504f45929cdd7509164e76,
"onTouchEnd": AS_TextField_d31b3d8898b145f590b35bf2b135d102,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
"onEndEditing": AS_TextField_j3d7047f81944175aef75ea8e83400c5,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineUsername = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineUsername",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineUsername.setDefaultUnit(kony.flex.DP);
flxUnderlineUsername.add();
var lblClose = new kony.ui.Label({
"height": "50%",
"id": "lblClose",
"isVisible": false,
"right": "90%",
"onTouchEnd": AS_Label_j85bff53082b4bd5b77b68db6e7fc7e0,
"skin": "sknClose",
"text": "O",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": "10%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxUsername.add(lblUsername, tbxUsernameKA, flxUnderlineUsername, lblClose);
var flxUsernameValidation = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "30%",
"id": "flxUsernameValidation",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"skin": "slFbox",
"top": "2%",
"width": "95%",
"zIndex": 1
}, {}, {});
flxUsernameValidation.setDefaultUnit(kony.flex.DP);
var CopyflxCondition0b324925061a144 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "16%",
"id": "CopyflxCondition0b324925061a144",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxCondition0b324925061a144.setDefaultUnit(kony.flex.DP);
var lblConditionCheck6 = new kony.ui.Label({
"height": "100%",
"id": "lblConditionCheck6",
"isVisible": true,
"right": "0%",
"skin": "lblTick",
"text": "r",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCondition6 = new kony.ui.Label({
"height": "100%",
"id": "lblCondition6",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyflxCondition0b324925061a144.add(lblConditionCheck6, lblCondition6);
var CopyflxCondition0e77c4054b89f4a = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "16%",
"id": "CopyflxCondition0e77c4054b89f4a",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxCondition0e77c4054b89f4a.setDefaultUnit(kony.flex.DP);
var lblConditionCheck7 = new kony.ui.Label({
"height": "100%",
"id": "lblConditionCheck7",
"isVisible": false,
"right": "0%",
"skin": "lblTick",
"text": "r",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCondition7 = new kony.ui.Label({
"height": "100%",
"id": "lblCondition7",
"isVisible": true,
"right": "0%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.common.uname2"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyflxCondition0e77c4054b89f4a.add(lblConditionCheck7, lblCondition7);
var CopyflxCondition0dca65c03fb0343 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "16%",
"id": "CopyflxCondition0dca65c03fb0343",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxCondition0dca65c03fb0343.setDefaultUnit(kony.flex.DP);
var lblConditionCheck8 = new kony.ui.Label({
"height": "100%",
"id": "lblConditionCheck8",
"isVisible": false,
"right": "0%",
"skin": "lblTick",
"text": "r",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCondition8 = new kony.ui.Label({
"height": "100%",
"id": "lblCondition8",
"isVisible": true,
"right": "0%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.common.uname3"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyflxCondition0dca65c03fb0343.add(lblConditionCheck8, lblCondition8);
var CopyflxCondition0bb39ff0ea27c42 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "16%",
"id": "CopyflxCondition0bb39ff0ea27c42",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxCondition0bb39ff0ea27c42.setDefaultUnit(kony.flex.DP);
var lblConditionCheck9 = new kony.ui.Label({
"height": "100%",
"id": "lblConditionCheck9",
"isVisible": false,
"right": "0%",
"skin": "lblTick",
"text": "r",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCondition9 = new kony.ui.Label({
"height": "100%",
"id": "lblCondition9",
"isVisible": true,
"right": "0%",
"skin": "sknLblNextDisabled",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyflxCondition0bb39ff0ea27c42.add(lblConditionCheck9, lblCondition9);
var CopyflxCondition0g5a2cbe6e57248 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "16%",
"id": "CopyflxCondition0g5a2cbe6e57248",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxCondition0g5a2cbe6e57248.setDefaultUnit(kony.flex.DP);
var lblConditionCheck10 = new kony.ui.Label({
"height": "100%",
"id": "lblConditionCheck10",
"isVisible": true,
"right": "0%",
"skin": "lblTick",
"text": "r",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCondition10 = new kony.ui.Label({
"height": "100%",
"id": "lblCondition10",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyflxCondition0g5a2cbe6e57248.add(lblConditionCheck10, lblCondition10);
var lblAllowededSpecialCharactersuname = new kony.ui.Label({
"height": "35%",
"id": "lblAllowededSpecialCharactersuname",
"isVisible": true,
"right": "0%",
"skin": "sknLblNextDisabled",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxUsernameValidation.add(CopyflxCondition0b324925061a144, CopyflxCondition0e77c4054b89f4a, CopyflxCondition0dca65c03fb0343, CopyflxCondition0bb39ff0ea27c42, CopyflxCondition0g5a2cbe6e57248, lblAllowededSpecialCharactersuname);
var lblInvalidCredentialsKA = new kony.ui.Label({
"id": "lblInvalidCredentialsKA",
"isVisible": false,
"left": "12%",
"skin": "sknInvalidCredKA",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "2%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxPassword = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxPassword",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "-1%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxPassword.setDefaultUnit(kony.flex.DP);
var lblPassword = new kony.ui.Label({
"id": "lblPassword",
"isVisible": true,
"right": "2%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.registration.createYourPassword"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var tbxPasswordKA = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter Password"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxPasswordKA",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"onDone": AS_TextField_g7aac657578b42b2a48160c604f9046e,
"onTextChange": AS_TextField_d692480d02be416faf4ea8ea5cbdef29,
"onTouchEnd": AS_TextField_d748b1bee4474d59b1a1ad30befe143c,
"secureTextEntry": true,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
"onEndEditing": AS_TextField_b1325229fcb14190b124c5a6eb8d6d5f,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlinePassword = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlinePassword",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlinePassword.setDefaultUnit(kony.flex.DP);
flxUnderlinePassword.add();
var flxPasswordProgress = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxPasswordProgress",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxPasswordProgress.setDefaultUnit(kony.flex.DP);
flxPasswordProgress.add();
var CopylblClose0bb2f50619b5543 = new kony.ui.Label({
"height": "50%",
"id": "CopylblClose0bb2f50619b5543",
"isVisible": false,
"right": "90%",
"onTouchEnd": AS_Label_d812f8ed21ef45e39d841fabd462571e,
"skin": "sknClose",
"text": "O",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": "10%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblShowPass = new kony.ui.Label({
"height": "100%",
"id": "lblShowPass",
"isVisible": false,
"onTouchEnd": AS_Label_ebe608dae7fc458fb6520a3b6df97a62,
"left": "10%",
"skin": "sknBOJFont3SIZE120",
"text": "A",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "10%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxPassword.add(lblPassword, tbxPasswordKA, flxUnderlinePassword, flxPasswordProgress, CopylblClose0bb2f50619b5543, lblShowPass);
var flxPasswordValidation = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "30%",
"id": "flxPasswordValidation",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"skin": "slFbox",
"top": "2%",
"width": "95%",
"zIndex": 1
}, {}, {});
flxPasswordValidation.setDefaultUnit(kony.flex.DP);
var flxCondition1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "16%",
"id": "flxCondition1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxCondition1.setDefaultUnit(kony.flex.DP);
var lblConditionCheck1 = new kony.ui.Label({
"height": "100%",
"id": "lblConditionCheck1",
"isVisible": true,
"right": "0%",
"skin": "lblTick",
"text": "r",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCondition1 = new kony.ui.Label({
"height": "100%",
"id": "lblCondition1",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxCondition1.add(lblConditionCheck1, lblCondition1);
var flxCondition2 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "16%",
"id": "flxCondition2",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxCondition2.setDefaultUnit(kony.flex.DP);
var lblConditionCheck2 = new kony.ui.Label({
"height": "100%",
"id": "lblConditionCheck2",
"isVisible": true,
"right": "0%",
"skin": "lblTick",
"text": "r",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCondition2 = new kony.ui.Label({
"height": "100%",
"id": "lblCondition2",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxCondition2.add(lblConditionCheck2, lblCondition2);
var flxCondition3 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "16%",
"id": "flxCondition3",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxCondition3.setDefaultUnit(kony.flex.DP);
var lblConditionCheck3 = new kony.ui.Label({
"height": "100%",
"id": "lblConditionCheck3",
"isVisible": true,
"right": "0%",
"skin": "lblTick",
"text": "r",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCondition3 = new kony.ui.Label({
"height": "100%",
"id": "lblCondition3",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxCondition3.add(lblConditionCheck3, lblCondition3);
var flxCondition4 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "16%",
"id": "flxCondition4",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxCondition4.setDefaultUnit(kony.flex.DP);
var lblConditionCheck4 = new kony.ui.Label({
"height": "100%",
"id": "lblConditionCheck4",
"isVisible": true,
"right": "0%",
"skin": "lblTick",
"text": "r",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCondition4 = new kony.ui.Label({
"height": "100%",
"id": "lblCondition4",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxCondition4.add(lblConditionCheck4, lblCondition4);
var flxCondition5 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "16%",
"id": "flxCondition5",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxCondition5.setDefaultUnit(kony.flex.DP);
var lblConditionCheck5 = new kony.ui.Label({
"height": "100%",
"id": "lblConditionCheck5",
"isVisible": true,
"right": "0%",
"skin": "lblTick",
"text": "r",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCondition5 = new kony.ui.Label({
"height": "100%",
"id": "lblCondition5",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxCondition5.add(lblConditionCheck5, lblCondition5);
var lblAllowededSpecialCharacterspwd = new kony.ui.Label({
"height": "20%",
"id": "lblAllowededSpecialCharacterspwd",
"isVisible": true,
"right": "0%",
"skin": "sknLblNextDisabled",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxPasswordValidation.add(flxCondition1, flxCondition2, flxCondition3, flxCondition4, flxCondition5, lblAllowededSpecialCharacterspwd);
var flxConfirmPassword = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxConfirmPassword",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxConfirmPassword.setDefaultUnit(kony.flex.DP);
var lblConfirmPassword = new kony.ui.Label({
"id": "lblConfirmPassword",
"isVisible": true,
"right": "2%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.registration.repeatYourPassword"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var tbxConfrmPwdKA = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Confirm your password and click next"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxConfrmPwdKA",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"onDone": AS_TextField_d73fbb049c8848d184432e4d2c245e79,
"onTextChange": AS_TextField_f958340f0288419582fe0ba29281bd4b,
"onTouchEnd": AS_TextField_ha10c8a825c5479693b5046341776c30,
"secureTextEntry": true,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"onEndEditing": AS_TextField_c5ac0110dbba405c94d1862a4e8642fa,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineConfirmPassword = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineConfirmPassword",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineConfirmPassword.setDefaultUnit(kony.flex.DP);
flxUnderlineConfirmPassword.add();
var CopylblClose0dcca487a6bc243 = new kony.ui.Label({
"height": "50%",
"id": "CopylblClose0dcca487a6bc243",
"isVisible": false,
"right": "90%",
"onTouchEnd": AS_Label_ec35aa25f14e4695b414b8847906f669,
"skin": "sknClose",
"text": "O",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": "10%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblShowConfirmPassword = new kony.ui.Label({
"height": "100%",
"id": "lblShowConfirmPassword",
"isVisible": false,
"onTouchEnd": AS_Label_ic5f7f513a12488a9f5cbee38b1d7758,
"left": "9.97%",
"skin": "sknBOJFont3SIZE120",
"text": "A",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "10.00%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxConfirmPassword.add(lblConfirmPassword, tbxConfrmPwdKA, flxUnderlineConfirmPassword, CopylblClose0dcca487a6bc243, lblShowConfirmPassword);
var lblInvalidPass = new kony.ui.Label({
"id": "lblInvalidPass",
"isVisible": true,
"left": "12%",
"skin": "sknInvalidCredKA",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "2%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxSpace = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2%",
"id": "flxSpace",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxSpace.setDefaultUnit(kony.flex.DP);
flxSpace.add();
var lblUname = new kony.ui.Label({
"height": "0%",
"id": "lblUname",
"isVisible": true,
"right": "0dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "0%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblPwd = new kony.ui.Label({
"height": "0%",
"id": "lblPwd",
"isVisible": true,
"right": "0dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "0%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCPwd = new kony.ui.Label({
"height": "0%",
"id": "lblCPwd",
"isVisible": true,
"right": "0dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "0%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxForgotusername = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "28%",
"id": "flxForgotusername",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "2%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxForgotusername.setDefaultUnit(kony.flex.DP);
var forgotpassword = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "CopysknsecactionUnderLine0b4be1f14a59741",
"height": "30%",
"id": "forgotpassword",
"isVisible": false,
"right": "0%",
"onClick": AS_Button_h93aa74feb2146829486185a3cdce107,
"skin": "sknsecactionUnderLine",
"text": kony.i18n.getLocalizedString("i18n.common.dontusername"),
"top": "0%",
"width": "100%",
"zIndex": 5
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var lblDontKnowUsername = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Click here if that's not your number"
},
"centerX": "50%",
"height": "25%",
"id": "lblDontKnowUsername",
"isVisible": true,
"right": "0%",
"onTouchEnd": AS_Label_bc5da0260e7d48eeb402a9cb5811f3d4,
"skin": "sknlblDNUN",
"text": kony.i18n.getLocalizedString("i18n.common.dontusername"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "2%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 1, 0,1, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxCallCallCenter = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "45%",
"id": "flxCallCallCenter",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"skin": "slFbox",
"top": "20%",
"width": "100%",
"zIndex": 5
}, {}, {});
flxCallCallCenter.setDefaultUnit(kony.flex.DP);
var lblNotYourNum = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Click here if that's not your number"
},
"id": "lblNotYourNum",
"isVisible": true,
"right": "0%",
"onTouchEnd": AS_Label_cb5ecd0073264ab09f5d319e2e65f99b,
"skin": "CopysknLblWhite0fbfcdb6497644a",
"text": kony.i18n.getLocalizedString("i18n.common.con"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblContactCenterNumber = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Nearest Branch"
},
"id": "lblContactCenterNumber",
"isVisible": true,
"right": "25%",
"onTouchEnd": AS_Label_afd5e54c81354558870586861ef45f5f,
"skin": "CopysknLblWhite0a7ef733745d04d",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5%",
"width": "50%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxCallCallCenter.add(lblNotYourNum, lblContactCenterNumber);
flxForgotusername.add(forgotpassword, lblDontKnowUsername, flxCallCallCenter);
flxUserPass.add(lblTitle, flxUsername, flxUsernameValidation, lblInvalidCredentialsKA, flxPassword, flxPasswordValidation, flxConfirmPassword, lblInvalidPass, flxSpace, lblUname, lblPwd, lblCPwd, flxForgotusername);
var flxNote = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0%",
"clipBounds": true,
"height": "15%",
"id": "flxNote",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "CopyslFbox0c11c20e1f2d54b",
"width": "100%",
"zIndex": 1
}, {}, {});
flxNote.setDefaultUnit(kony.flex.DP);
var lblNote = new kony.ui.Label({
"bottom": "2%",
"centerX": "50%",
"id": "lblNote",
"isVisible": true,
"skin": "CopysknLblNextDisabled0d6dfa37815734d",
"text": kony.i18n.getLocalizedString("i18n.common.DisableFeature"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "94%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxNote.add(lblNote);
flxmainform.add(flxHead, mainContent, flxUserPass, flxNote);
var lblLanguage = new kony.ui.Label({
"centerX": "70%",
"height": "0%",
"id": "lblLanguage",
"isVisible": false,
"right": "0%",
"skin": "sknnavBarTitle",
"top": "0dp",
"width": "0%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblFlag = new kony.ui.Label({
"centerX": "0%",
"height": "0%",
"id": "lblFlag",
"isVisible": false,
"right": "0%",
"skin": "sknnavBarTitle",
"top": "0%",
"width": "0%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblIpAdd = new kony.ui.Label({
"centerX": "10%",
"height": "0%",
"id": "lblIpAdd",
"isVisible": false,
"right": "0%",
"skin": "sknnavBarTitle",
"top": "10%",
"width": "0%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblDevId = new kony.ui.Label({
"centerX": "20%",
"height": "0%",
"id": "lblDevId",
"isVisible": false,
"right": "0%",
"skin": "sknnavBarTitle",
"top": "20%",
"width": "0%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblIEMI = new kony.ui.Label({
"centerX": "30%",
"height": "0%",
"id": "lblIEMI",
"isVisible": false,
"right": "0%",
"skin": "sknnavBarTitle",
"top": "30%",
"width": "0%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var UserFlag = new kony.ui.Label({
"centerX": "10%",
"height": "0%",
"id": "UserFlag",
"isVisible": false,
"right": "0%",
"skin": "sknnavBarTitle",
"top": "10%",
"width": "0%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
frmEnrolluserLandingKA.add(imgBack, flxmainform, lblLanguage, lblFlag, lblIpAdd, lblDevId, lblIEMI, UserFlag);
};
function frmEnrolluserLandingKAGlobalsAr() {
frmEnrolluserLandingKAAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmEnrolluserLandingKAAr,
"bounces": false,
"enableScrolling": true,
"enabledForIdleTimeout": true,
"id": "frmEnrolluserLandingKA",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"postShow": AS_Form_ac1757ccde8846a18b3616d01cc5d533,
"preShow": AS_Form_2171e0d469284db9ad7bbff510491c86,
"skin": "CopysknmainGradient0ea10498c60da48"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"inTransitionConfig": {
"formAnimation": 0
},
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_359d5f9a8a1c4b529755fedce4d6fa9e,
"retainScrollPosition": false,
"titleBar": true,
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
