//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmDeviceDeRegistrationKAAr() {
    frmDeviceDeRegistrationKA.setDefaultUnit(kony.flex.DP);
    var touchFeature = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "touchFeature",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    touchFeature.setDefaultUnit(kony.flex.DP);
    var FlexContainer00a3c07fadcbf4b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "FlexContainer00a3c07fadcbf4b",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "skin": "sknslFbox",
        "top": "60dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer00a3c07fadcbf4b.setDefaultUnit(kony.flex.DP);
    var Label07b268452752c4c = new kony.ui.Label({
        "id": "Label07b268452752c4c",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknonboardingHeader",
        "text": kony.i18n.getLocalizedString("i18n.login.deRegisterDevice"),
        "top": "0dp",
        "width": "100%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyLabel0d00f815bcc7c41 = new kony.ui.Label({
        "centerX": "50%",
        "id": "CopyLabel0d00f815bcc7c41",
        "isVisible": true,
        "right": "20%",
        "skin": "sknonboardingText",
        "text": kony.i18n.getLocalizedString("i18n.login.doYouWantDeviceDeRegistration.CopyLabel0d00f815bcc7c41"),
        "top": "10dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDeviceIdKA = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblDeviceIdKA",
        "isVisible": false,
        "right": "20%",
        "skin": "sknonboardingText",
        "text": kony.i18n.getLocalizedString("i18n.common.deviceId"),
        "top": "10dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyLabel0ebedff6d09854f = new kony.ui.Label({
        "centerX": "50%",
        "id": "CopyLabel0ebedff6d09854f",
        "isVisible": true,
        "right": "20%",
        "skin": "sknonboardingText",
        "text": kony.i18n.getLocalizedString("i18n.login.registerItBack"),
        "top": "10dp",
        "width": "65%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var enableTouchID = new kony.ui.Button({
        "centerX": "50.00%",
        "focusSkin": "sknprimaryActionFocus",
        "height": "42dp",
        "id": "enableTouchID",
        "isVisible": true,
        "onClick": AS_Button_82cf85d2d482430aa892d427e06de671,
        "skin": "sknprimaryAction",
        "text": kony.i18n.getLocalizedString("i18n.login.cDeviceDeRegistration"),
        "top": "60dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    var noThanks = new kony.ui.Button({
        "centerX": "50.00%",
        "focusSkin": "sknsecondaryActionFocus",
        "height": "35dp",
        "id": "noThanks",
        "isVisible": true,
        "onClick": AS_Button_2d6ad6ab37d6469cbcf51eac04405f94,
        "skin": "sknsecondaryAction",
        "text": kony.i18n.getLocalizedString("i18n.common.noThanks"),
        "top": "4dp",
        "width": "260dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    FlexContainer00a3c07fadcbf4b.add(Label07b268452752c4c, CopyLabel0d00f815bcc7c41, lblDeviceIdKA, CopyLabel0ebedff6d09854f, enableTouchID, noThanks);
    touchFeature.add(FlexContainer00a3c07fadcbf4b);
    frmDeviceDeRegistrationKA.add(touchFeature);
};
function frmDeviceDeRegistrationKAGlobalsAr() {
    frmDeviceDeRegistrationKAAr = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmDeviceDeRegistrationKAAr,
        "bounces": false,
        "enableScrolling": true,
        "enabledForIdleTimeout": true,
        "id": "frmDeviceDeRegistrationKA",
        "layoutType": kony.flex.FLOW_VERTICAL,
        "needAppMenu": false,
        "pagingEnabled": false,
        "skin": "sknSuccessBkg"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "inTransitionConfig": {
            "formAnimation": 0
        },
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_9bcc68e3b6504befbe23d8cec83fc520,
        "outTransitionConfig": {
            "formAnimation": 0
        },
        "retainScrollPosition": false,
        "titleBar": true,
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};
