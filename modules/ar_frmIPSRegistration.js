//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmIPSRegistrationAr() {
frmIPSRegistration.setDefaultUnit(kony.flex.DP);
var flxHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "s",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxHeader.setDefaultUnit(kony.flex.DP);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_hfbe8ef2027d46c19b6ea012d1ba531e,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 1
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblBack0h2fe661f120d45 = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblBack0h2fe661f120d45",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBack.add(lblBackIcon, CopylblBack0h2fe661f120d45);
var lblIPSRegistrationTitle = new kony.ui.Label({
"height": "90%",
"id": "lblIPSRegistrationTitle",
"isVisible": true,
"left": "20%",
"minHeight": "90%",
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.IPS.registerforIPS"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": "60%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var btnNext = new kony.ui.Button({
"focusSkin": "jomopaynextDisabled",
"height": "90%",
"id": "btnNext",
"isVisible": true,
"onClick": AS_Button_c67378e555b64860b9e50a7638f7e289,
"right": "0%",
"skin": "jomopaynextDisabled",
"text": kony.i18n.getLocalizedString("i18n.login.next"),
"top": "0%",
"width": "20%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
flxHeader.add(flxBack, lblIPSRegistrationTitle, btnNext);
var flxBody = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBody",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "10%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxBody.setDefaultUnit(kony.flex.DP);
var flxAccountNumber = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "13%",
"id": "flxAccountNumber",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAccountNumber.setDefaultUnit(kony.flex.DP);
var lblAccountNumberTitle = new kony.ui.Label({
"id": "lblAccountNumberTitle",
"isVisible": true,
"right": "5%",
"skin": "sknLblWhike125",
"text": kony.i18n.getLocalizedString("i18n.Transfer.selectAcc"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "35%",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblAccountNumber = new kony.ui.Label({
"height": "45%",
"id": "lblAccountNumber",
"isVisible": true,
"right": "15%",
"skin": "sknTransferType",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "45%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxBorderAccountNumber = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2%",
"id": "flxBorderAccountNumber",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "skntextFieldDivider",
"top": "86%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxBorderAccountNumber.setDefaultUnit(kony.flex.DP);
flxBorderAccountNumber.add();
var btnAccountNumber = new kony.ui.Button({
"focusSkin": "CopyslButtonGlossBlue0cec99ae0f6784a",
"height": "50%",
"id": "btnAccountNumber",
"isVisible": true,
"right": "0%",
"onClick": AS_Button_b5539ce362fe4e19a40ffb0e8320fcd8,
"skin": "CopyslButtonGlossBlue0cec99ae0f6784a",
"text": kony.i18n.getLocalizedString("i18n.common.reverseback"),
"top": "35%",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var flxIcon1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "45dp",
"id": "flxIcon1",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "1%",
"skin": "sknFlxToIcon",
"top": "10%",
"width": "45dp",
"zIndex": 100
}, {}, {});
flxIcon1.setDefaultUnit(kony.flex.DP);
var lblIcon1 = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"height": "80%",
"id": "lblIcon1",
"isVisible": true,
"right": "19dp",
"onTouchEnd": AS_Label_e258f2739b1a4ce0a772fe869d2de4fe,
"skin": "sknLblFromIcon",
"text": "BH",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "13dp",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxIcon1.add(lblIcon1);
var lblAccountName1 = new kony.ui.Label({
"bottom": "0%",
"height": "40%",
"id": "lblAccountName1",
"isVisible": true,
"right": "15%",
"skin": "sknLblWhike125",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "10%",
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxAccountNumber.add(lblAccountNumberTitle, lblAccountNumber, flxBorderAccountNumber, btnAccountNumber, flxIcon1, lblAccountName1);
var lblSplit1 = new kony.ui.Label({
"height": "2dp",
"id": "lblSplit1",
"isVisible": true,
"right": "0dp",
"skin": "lblsknToandFromAccLine",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "15%",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxAliasType = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxAliasType",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "16%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxAliasType.setDefaultUnit(kony.flex.DP);
var lblAliasTypeTitle = new kony.ui.Label({
"id": "lblAliasTypeTitle",
"isVisible": true,
"right": "0%",
"skin": "sknLblWhike125",
"text": kony.i18n.getLocalizedString("i18.CLIQ.selectRegType"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "10%",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var FlexContainer0ia570f0c4eb44e = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "35%",
"id": "FlexContainer0ia570f0c4eb44e",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "45%",
"width": "100%",
"zIndex": 1
}, {}, {});
FlexContainer0ia570f0c4eb44e.setDefaultUnit(kony.flex.DP);
var btnMobileNumber = new kony.ui.Button({
"centerX": "75%",
"focusSkin": "sknOrangeBGRNDBOJ",
"height": "90%",
"id": "btnMobileNumber",
"isVisible": true,
"right": "0%",
"onClick": AS_Button_d5bdddf90b68441f9306ca6944e82cf9,
"skin": "slButtonBlueFocus",
"text": kony.i18n.getLocalizedString("i18n.jomopay.mobiletype"),
"top": "0%",
"width": "45%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnAlias = new kony.ui.Button({
"centerX": "25%",
"focusSkin": "sknOrangeBGRNDBOJ",
"height": "90%",
"id": "btnAlias",
"isVisible": true,
"right": "50%",
"onClick": AS_Button_d5bdddf90b68441f9306ca6944e82cf9,
"skin": "slButtonBlueFocus",
"text": kony.i18n.getLocalizedString("i18n.jomopay.aliastype"),
"top": "0%",
"width": "45%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
FlexContainer0ia570f0c4eb44e.add(btnMobileNumber, btnAlias);
flxAliasType.add(lblAliasTypeTitle, FlexContainer0ia570f0c4eb44e);
var lblSplit2 = new kony.ui.Label({
"height": "2dp",
"id": "lblSplit2",
"isVisible": true,
"right": "0dp",
"skin": "lblsknToandFromAccLine",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "35%",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxAlias = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "13%",
"id": "flxAlias",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "35%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxAlias.setDefaultUnit(kony.flex.DP);
var txtAlias = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_ALL,
"focusSkin": "sknTxtBox",
"height": "50%",
"id": "txtAlias",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "0%",
"onTextChange": onTextChangeAliasIPSRegistration,
"onTouchEnd": AS_TextField_OnTouchEndTxtAliasIPSREG,
"secureTextEntry": false,
"skin": "txtBox0b5a21c6c49d64b",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "45%",
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
"onEndEditing": AS_TextField_OnEndEditingTxtAliasIPSREg,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var lblAliasTitle = new kony.ui.Label({
"id": "lblAliasTitle",
"isVisible": true,
"right": "2%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.jomopay.mobiletype"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "55%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxBorderAlias = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2%",
"id": "flxBorderAlias",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "skntextFieldDivider",
"top": "86%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxBorderAlias.setDefaultUnit(kony.flex.DP);
flxBorderAlias.add();
flxAlias.add(txtAlias, lblAliasTitle, flxBorderAlias);
var LblCountryCodeHint = new kony.ui.Label({
"id": "LblCountryCodeHint",
"isVisible": true,
"right": "5%",
"skin": "latoRegular24px",
"text": kony.i18n.getLocalizedString("i18n.jomopay.mobilenumberhint"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "47%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
      var flxAliasHints = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxAliasHints",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "5%",
        "skin": "slFbox",
        "top": "47%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxAliasHints.setDefaultUnit(kony.flex.DP);
    var lblAliasMinVal = new kony.ui.Label({
        "id": "lblAliasMinVal",
        "isVisible": true,
        "right": "0dp",
        "skin": "latoRegular24px",
        "text": kony.i18n.getLocalizedString("i18n.CLIQ.AliasMinVal"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAliasMaxVal = new kony.ui.Label({
        "id": "lblAliasMaxVal",
        "isVisible": true,
        "right": "0dp",
        "skin": "latoRegular24px",
        "text": kony.i18n.getLocalizedString("i18n.CLIQ.AliasMaxVal"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "3px",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAliasNumVal = new kony.ui.Label({
        "id": "lblAliasNumVal",
        "isVisible": true,
        "right": 0,
        "skin": "latoRegular24px",
        "text": kony.i18n.getLocalizedString("i18n.CLIQ.AliasNumVal"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "3px",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAliasCaseVal = new kony.ui.Label({
        "id": "lblAliasCaseVal",
        "isVisible": true,
        "right": 0,
        "skin": "latoRegular24px",
        "text": kony.i18n.getLocalizedString("i18n.CLIQ.AliasCaseVal"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "3px",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAliasHints.add(lblAliasMinVal, lblAliasMaxVal, lblAliasNumVal, lblAliasCaseVal);

flxBody.add(flxAccountNumber, lblSplit1, flxAliasType, lblSplit2, flxAlias, LblCountryCodeHint, flxAliasHints);
var flxConfirmation = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxConfirmation",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"skin": "slFbox",
"top": "10%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxConfirmation.setDefaultUnit(kony.flex.DP);
var flxAccountNumberConfirmation = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxAccountNumberConfirmation",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "0%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxAccountNumberConfirmation.setDefaultUnit(kony.flex.DP);
var lblAccountNumberConfirmationTitle = new kony.ui.Label({
"id": "lblAccountNumberConfirmationTitle",
"isVisible": true,
"right": "0%",
"skin": "sknlblBodytxt",
"text": kony.i18n.getLocalizedString("i18n.common.accountNumber"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblAccountNumberConfirmation = new kony.ui.Label({
"id": "lblAccountNumberConfirmation",
"isVisible": true,
"right": "0%",
"skin": "sknNumber",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "50%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxAccountNumberConfirmation.add(lblAccountNumberConfirmationTitle, lblAccountNumberConfirmation);
var flxAliasTypeConfirmation = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxAliasTypeConfirmation",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "1%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxAliasTypeConfirmation.setDefaultUnit(kony.flex.DP);
var lblAliasTypeConfirmationTitle = new kony.ui.Label({
"id": "lblAliasTypeConfirmationTitle",
"isVisible": true,
"right": "0%",
"skin": "sknlblBodytxt",
"text": kony.i18n.getLocalizedString("i18n.common.type"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblAliasTypeConfirmation = new kony.ui.Label({
"id": "lblAliasTypeConfirmation",
"isVisible": true,
"right": "0%",
"skin": "sknNumber",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "50%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxAliasTypeConfirmation.add(lblAliasTypeConfirmationTitle, lblAliasTypeConfirmation);
var flxAliasConfirmation = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxAliasConfirmation",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "1%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxAliasConfirmation.setDefaultUnit(kony.flex.DP);
var lblAliasConfirmationTitle = new kony.ui.Label({
"id": "lblAliasConfirmationTitle",
"isVisible": true,
"right": "0%",
"skin": "sknlblBodytxt",
"text": kony.i18n.getLocalizedString("i18n.common.accountNumber"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblAliasConfirmation = new kony.ui.Label({
"id": "lblAliasConfirmation",
"isVisible": true,
"right": "0%",
"skin": "sknNumber",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "50%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxAliasConfirmation.add(lblAliasConfirmationTitle, lblAliasConfirmation);
var btnConfirm = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "slButtonWhiteFocus",
"height": "10%",
"id": "btnConfirm",
"isVisible": true,
"onClick": AS_Button_dd3a17c7a39548498bd44bb6b70716c6,
"skin": "slButtonWhite",
"text": kony.i18n.getLocalizedString("i18n.Bene.Confirm"),
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxConfirmation.add(flxAccountNumberConfirmation, flxAliasTypeConfirmation, flxAliasConfirmation, btnConfirm);
frmIPSRegistration.add(flxHeader, flxBody, flxConfirmation);
};
function frmIPSRegistrationGlobalsAr() {
frmIPSRegistrationAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmIPSRegistrationAr,
"bounces": false,
"enabledForIdleTimeout": true,
"id": "frmIPSRegistration",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": true,
"skin": "slFormCommon",
"verticalScrollIndicator": false
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"retainScrollPosition": false,
"titleBar": true,
"titleBarSkin": "slTitleBar",
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
