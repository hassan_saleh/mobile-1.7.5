//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmEnterPersonalDetailsKAAr() {
frmEnterPersonalDetailsKA.setDefaultUnit(kony.flex.DP);
var overview = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "overview",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
overview.setDefaultUnit(kony.flex.DP);
var titleBarAccountInfo = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "85dp",
"id": "titleBarAccountInfo",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
titleBarAccountInfo.setDefaultUnit(kony.flex.DP);
var flxAndroidTittleBarKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "53dp",
"id": "flxAndroidTittleBarKA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAndroidTittleBarKA.setDefaultUnit(kony.flex.DP);
var titleBarLabel = new kony.ui.Label({
"centerX": "50%",
"id": "titleBarLabel",
"isVisible": true,
"skin": "skniconButtonLabel",
"text": kony.i18n.getLocalizedString("i18n.common.openinganAccount"),
"top": "12dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var androidBack = new kony.ui.Button({
"focusSkin": "sknandroidBackButtonFocus",
"height": "50dp",
"id": "androidBack",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_db45a6551c264b6ca449718fba67072a,
"skin": "sknandroidBackButton",
"top": "0dp",
"width": "50dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxAndroidTittleBarKA.add(titleBarLabel, androidBack);
var lblPickAProductKA = new kony.ui.Label({
"centerX": "50%",
"id": "lblPickAProductKA",
"isVisible": true,
"skin": "skniconButtonLabel",
"text": kony.i18n.getLocalizedString("i18n.settings.enterPersonalDetails"),
"top": "53dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxTransitionKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50.00%",
"clipBounds": true,
"height": "20dp",
"id": "flxTransitionKA",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"skin": "slFbox",
"top": "35dp",
"width": "200dp",
"zIndex": 1
}, {}, {});
flxTransitionKA.setDefaultUnit(kony.flex.DP);
var flx1KA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "5dp",
"id": "flx1KA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": 0,
"skin": "sknNewAccount78a0c8KA",
"top": 0,
"width": "20dp"
}, {}, {});
flx1KA.setDefaultUnit(kony.flex.DP);
flx1KA.add();
var flx2KA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "5dp",
"id": "flx2KA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5dp",
"skin": "sknNewAccount78a0c8KA",
"top": "0dp",
"width": "20dp"
}, {}, {});
flx2KA.setDefaultUnit(kony.flex.DP);
flx2KA.add();
var flx3KA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "49.44%",
"clipBounds": true,
"height": "5dp",
"id": "flx3KA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5dp",
"skin": "sknNewAccount78a0c8KA",
"top": 0,
"width": "20dp"
}, {}, {});
flx3KA.setDefaultUnit(kony.flex.DP);
flx3KA.add();
var flx4KA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "5dp",
"id": "flx4KA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5dp",
"skin": "sknNewAccount78a0c8KA",
"top": 0,
"width": "20dp"
}, {}, {});
flx4KA.setDefaultUnit(kony.flex.DP);
flx4KA.add();
var flx5KA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "5dp",
"id": "flx5KA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5dp",
"skin": "sknNewAccount78a0c8KA",
"top": 0,
"width": "20dp"
}, {}, {});
flx5KA.setDefaultUnit(kony.flex.DP);
flx5KA.add();
var flx6KA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "5dp",
"id": "flx6KA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5dp",
"skin": "skncontainerBkgWhite",
"top": 0,
"width": "20dp"
}, {}, {});
flx6KA.setDefaultUnit(kony.flex.DP);
flx6KA.add();
var flx7KA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "5dp",
"id": "flx7KA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5dp",
"skin": "sknNewAccount78a0c8KA",
"top": 0,
"width": "20dp"
}, {}, {});
flx7KA.setDefaultUnit(kony.flex.DP);
flx7KA.add();
var flx8KA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "5dp",
"id": "flx8KA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5dp",
"skin": "sknNewAccount78a0c8KA",
"top": 0,
"width": "20dp"
}, {}, {});
flx8KA.setDefaultUnit(kony.flex.DP);
flx8KA.add();
flxTransitionKA.add( flx8KA, flx7KA, flx6KA, flx5KA, flx4KA, flx3KA, flx2KA,flx1KA);
titleBarAccountInfo.add(flxAndroidTittleBarKA, lblPickAProductKA, flxTransitionKA);
var mainContent = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bottom": "0dp",
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"horizontalScrollIndicator": true,
"id": "mainContent",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "sknscrollBkgGray",
"top": "85dp",
"verticalScrollIndicator": false,
"width": "100%",
"zIndex": 1
}, {}, {});
mainContent.setDefaultUnit(kony.flex.DP);
var challengeQuestionContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "75dp",
"id": "challengeQuestionContainer",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
challengeQuestionContainer.setDefaultUnit(kony.flex.DP);
var lblQuestion = new kony.ui.Label({
"height": "32dp",
"id": "lblQuestion",
"isVisible": true,
"right": "5%",
"skin": "skn",
"text": kony.i18n.getLocalizedString("i18n.common.name"),
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var answerFIeldWrapper = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "38dp",
"id": "answerFIeldWrapper",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"left": "5%",
"skin": "sknslFbox",
"top": "35dp",
"width": "90%",
"zIndex": 1
}, {}, {});
answerFIeldWrapper.setDefaultUnit(kony.flex.DP);
var answerField = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_WORDS,
"focusSkin": "skngeneralTextFieldFocus",
"height": 35,
"id": "answerField",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "0dp",
"placeholder": kony.i18n.getLocalizedString("i18n.settings.enterYourNameHere"),
"left": 0,
"secureTextEntry": false,
"skin": "skngeneralTextField",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "0dp"
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
"placeholderSkin": "sknPlaceholderKA",
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var listDivider = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "listDivider",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"top": "34dp",
"width": "100%",
"zIndex": 1
}, {}, {});
listDivider.setDefaultUnit(kony.flex.DP);
listDivider.add();
answerFIeldWrapper.add(answerField, listDivider);
challengeQuestionContainer.add(lblQuestion, answerFIeldWrapper);
var CopychallengeQuestionContainer0d0f6eb35317e4d = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "75dp",
"id": "CopychallengeQuestionContainer0d0f6eb35317e4d",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopychallengeQuestionContainer0d0f6eb35317e4d.setDefaultUnit(kony.flex.DP);
var CopylblQuestion07aa9e7e2abe442 = new kony.ui.Label({
"height": "32dp",
"id": "CopylblQuestion07aa9e7e2abe442",
"isVisible": true,
"right": "5%",
"skin": "skn",
"text": kony.i18n.getLocalizedString("i18n.common.dateOfBirth"),
"top": "5dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyanswerFIeldWrapper0117ea71aa7d743 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "38dp",
"id": "CopyanswerFIeldWrapper0117ea71aa7d743",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"left": "5%",
"skin": "sknslFbox",
"top": "35dp",
"width": "90%",
"zIndex": 1
}, {}, {});
CopyanswerFIeldWrapper0117ea71aa7d743.setDefaultUnit(kony.flex.DP);
var CalendarDOB = new kony.ui.Calendar({
"calendarIcon": "dropdown.png",
"dateComponents": [11, 3, 2016, 0, 0, 0],
"dateFormat": "dd/MM/yyyy",
"day": 11,
"formattedDate": "11/03/2016",
"height": "40dp",
"hour": 0,
"id": "CalendarDOB",
"isVisible": true,
"right": "0dp",
"minutes": 0,
"month": 3,
"placeholder": "Enter DOB",
"seconds": 0,
"skin": "skndobDatepicker",
"top": "0dp",
"viewConfig": {},
"viewType": constants.CALENDAR_VIEW_TYPE_DEFAULT,
"width": "260dp",
"year": 2016,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopylistDivider02e2fa644c3d642 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopylistDivider02e2fa644c3d642",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"top": "36dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopylistDivider02e2fa644c3d642.setDefaultUnit(kony.flex.DP);
CopylistDivider02e2fa644c3d642.add();
CopyanswerFIeldWrapper0117ea71aa7d743.add(CalendarDOB, CopylistDivider02e2fa644c3d642);
CopychallengeQuestionContainer0d0f6eb35317e4d.add(CopylblQuestion07aa9e7e2abe442, CopyanswerFIeldWrapper0117ea71aa7d743);
var CopychallengeQuestionContainer0103ed4f67f2345 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "110dp",
"id": "CopychallengeQuestionContainer0103ed4f67f2345",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopychallengeQuestionContainer0103ed4f67f2345.setDefaultUnit(kony.flex.DP);
var CopylblQuestion0b378c51dd1f041 = new kony.ui.Label({
"height": "32dp",
"id": "CopylblQuestion0b378c51dd1f041",
"isVisible": true,
"right": "5%",
"skin": "skn",
"text": kony.i18n.getLocalizedString("i18n.common.phonenumber"),
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyanswerFIeldWrapper0082ca72944f541 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "75dp",
"id": "CopyanswerFIeldWrapper0082ca72944f541",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "5%",
"left": "5%",
"skin": "sknslFbox",
"top": "35dp",
"width": "90%",
"zIndex": 1
}, {}, {});
CopyanswerFIeldWrapper0082ca72944f541.setDefaultUnit(kony.flex.DP);
var phoneone = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "skngeneralTextFieldFocus",
"height": 35,
"id": "phoneone",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD,
"right": "0dp",
"placeholder": kony.i18n.getLocalizedString("i18n.settings.enterHomePhNumber"),
"left": 0,
"secureTextEntry": false,
"skin": "skngeneralTextField",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"top": "0dp"
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
"placeholderSkin": "sknPlaceholderKA",
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var CopylistDivider05fbced4bf29d42 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopylistDivider05fbced4bf29d42",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopylistDivider05fbced4bf29d42.setDefaultUnit(kony.flex.DP);
CopylistDivider05fbced4bf29d42.add();
var phonetwo = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "skngeneralTextFieldFocus",
"height": 35,
"id": "phonetwo",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD,
"right": "0dp",
"placeholder": kony.i18n.getLocalizedString("i18n.settings.enterYourMobileNumberPlh"),
"left": -10,
"secureTextEntry": false,
"skin": "skngeneralTextField",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"top": "0dp"
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
"placeholderSkin": "sknPlaceholderKA",
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var CopylistDivider0fa5b2ba001c740 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopylistDivider0fa5b2ba001c740",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopylistDivider0fa5b2ba001c740.setDefaultUnit(kony.flex.DP);
CopylistDivider0fa5b2ba001c740.add();
CopyanswerFIeldWrapper0082ca72944f541.add(phoneone, CopylistDivider05fbced4bf29d42, phonetwo, CopylistDivider0fa5b2ba001c740);
CopychallengeQuestionContainer0103ed4f67f2345.add(CopylblQuestion0b378c51dd1f041, CopyanswerFIeldWrapper0082ca72944f541);
var CopychallengeQuestionContainer075e50955f8ec44 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "75dp",
"id": "CopychallengeQuestionContainer075e50955f8ec44",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopychallengeQuestionContainer075e50955f8ec44.setDefaultUnit(kony.flex.DP);
var CopylblQuestion0b5ad63f640ef48 = new kony.ui.Label({
"height": "32dp",
"id": "CopylblQuestion0b5ad63f640ef48",
"isVisible": true,
"right": "4.97%",
"skin": "skn",
"text": kony.i18n.getLocalizedString("i18n.manage_payee.eMailHy"),
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyanswerFIeldWrapper05ccf9db0a51344 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "38dp",
"id": "CopyanswerFIeldWrapper05ccf9db0a51344",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"left": "5%",
"skin": "sknslFbox",
"top": "35dp",
"width": "90%",
"zIndex": 1
}, {}, {});
CopyanswerFIeldWrapper05ccf9db0a51344.setDefaultUnit(kony.flex.DP);
var email = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "skngeneralTextFieldFocus",
"height": 35,
"id": "email",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_EMAIL,
"right": "0dp",
"placeholder": kony.i18n.getLocalizedString("i18n.settings.enterYourEmailHerePlh"),
"secureTextEntry": false,
"skin": "skngeneralTextField",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "0dp",
"width": "260dp"
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
"placeholderSkin": "sknPlaceholderKA",
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var CopylistDivider083f14b480af54c = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopylistDivider083f14b480af54c",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"top": "34dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopylistDivider083f14b480af54c.setDefaultUnit(kony.flex.DP);
CopylistDivider083f14b480af54c.add();
CopyanswerFIeldWrapper05ccf9db0a51344.add(email, CopylistDivider083f14b480af54c);
var lblEmailErr = new kony.ui.Label({
"height": "32dp",
"id": "lblEmailErr",
"isVisible": false,
"left": "4.97%",
"skin": "sknD0021BFLatoRegular",
"text": "Invalid E-Mail",
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopychallengeQuestionContainer075e50955f8ec44.add(CopylblQuestion0b5ad63f640ef48, CopyanswerFIeldWrapper05ccf9db0a51344, lblEmailErr);
var CopychallengeQuestionContainer05574a04a683345 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "75dp",
"id": "CopychallengeQuestionContainer05574a04a683345",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopychallengeQuestionContainer05574a04a683345.setDefaultUnit(kony.flex.DP);
var CopylblQuestion07ba1e81d885647 = new kony.ui.Label({
"height": "32dp",
"id": "CopylblQuestion07ba1e81d885647",
"isVisible": true,
"right": "4.97%",
"skin": "skn",
"text": kony.i18n.getLocalizedString("i18n.common.address"),
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyanswerFIeldWrapper0dfc9a456ea9746 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "38dp",
"id": "CopyanswerFIeldWrapper0dfc9a456ea9746",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"left": "5%",
"skin": "sknslFbox",
"top": "35dp",
"width": "90%",
"zIndex": 1
}, {}, {});
CopyanswerFIeldWrapper0dfc9a456ea9746.setDefaultUnit(kony.flex.DP);
var address = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_WORDS,
"focusSkin": "skngeneralTextFieldFocus",
"height": 35,
"id": "address",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "0dp",
"placeholder": kony.i18n.getLocalizedString("i18n.settings.enterYourPostalAddressPlh"),
"left": 0,
"secureTextEntry": false,
"skin": "skngeneralTextField",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "0dp"
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
"placeholderSkin": "sknPlaceholderKA",
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var CopylistDivider0bd9631a4464f4a = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopylistDivider0bd9631a4464f4a",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"top": "34dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopylistDivider0bd9631a4464f4a.setDefaultUnit(kony.flex.DP);
CopylistDivider0bd9631a4464f4a.add();
CopyanswerFIeldWrapper0dfc9a456ea9746.add(address, CopylistDivider0bd9631a4464f4a);
CopychallengeQuestionContainer05574a04a683345.add(CopylblQuestion07ba1e81d885647, CopyanswerFIeldWrapper0dfc9a456ea9746);
var CopychallengeQuestionContainer0716edd868fd246 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "75dp",
"id": "CopychallengeQuestionContainer0716edd868fd246",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopychallengeQuestionContainer0716edd868fd246.setDefaultUnit(kony.flex.DP);
var CopylblQuestion09fc42e2c3a9b41 = new kony.ui.Label({
"height": "32dp",
"id": "CopylblQuestion09fc42e2c3a9b41",
"isVisible": true,
"right": "4.97%",
"skin": "skn",
"text": kony.i18n.getLocalizedString("i18n.common.socialSecurityNumber"),
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyanswerFIeldWrapper0112aea60b64346 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "38dp",
"id": "CopyanswerFIeldWrapper0112aea60b64346",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"left": "5%",
"skin": "sknslFbox",
"top": "35dp",
"width": "90%",
"zIndex": 1
}, {}, {});
CopyanswerFIeldWrapper0112aea60b64346.setDefaultUnit(kony.flex.DP);
var ssn = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"focusSkin": "skngeneralTextFieldFocus",
"height": 35,
"id": "ssn",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DECIMAL,
"right": "0dp",
"placeholder": kony.i18n.getLocalizedString("i18n.common.enterYouSocialSecirutyNumber"),
"left": 0,
"secureTextEntry": false,
"skin": "skngeneralTextField",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"top": "0dp"
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
"placeholderSkin": "sknPlaceholderKA",
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var CopylistDivider001bfe075d9bb48 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "CopylistDivider001bfe075d9bb48",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"top": "34dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopylistDivider001bfe075d9bb48.setDefaultUnit(kony.flex.DP);
CopylistDivider001bfe075d9bb48.add();
CopyanswerFIeldWrapper0112aea60b64346.add(ssn, CopylistDivider001bfe075d9bb48);
var lblSSNErr = new kony.ui.Label({
"height": "32dp",
"id": "lblSSNErr",
"isVisible": false,
"left": "4.97%",
"skin": "sknD0021BFLatoRegular",
"text": "Invalid SSN",
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopychallengeQuestionContainer0716edd868fd246.add(CopylblQuestion09fc42e2c3a9b41, CopyanswerFIeldWrapper0112aea60b64346, lblSSNErr);
var enableTouchID = new kony.ui.Button({
"centerX": "50.00%",
"focusSkin": "sknprimaryActionFocus",
"height": "42dp",
"id": "enableTouchID",
"isVisible": true,
"onClick": AS_Button_4b3ee9c658354600a5eb36eb7b1a1553,
"skin": "sknprimaryAction",
"text": kony.i18n.getLocalizedString("i18n.settings.cProceed"),
"top": "25dp",
"width": "60%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var noThanks = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "sknsecondaryActionFocus",
"height": "35dp",
"id": "noThanks",
"isVisible": true,
"onClick": AS_Button_5230f5be38dc49379ba3a0e10bba474d,
"skin": "sknsecondaryAction",
"text": kony.i18n.getLocalizedString("i18n.common.cancel"),
"top": "4dp",
"width": "60%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
mainContent.add(challengeQuestionContainer, CopychallengeQuestionContainer0d0f6eb35317e4d, CopychallengeQuestionContainer0103ed4f67f2345, CopychallengeQuestionContainer075e50955f8ec44, CopychallengeQuestionContainer05574a04a683345, CopychallengeQuestionContainer0716edd868fd246, enableTouchID, noThanks);
overview.add(titleBarAccountInfo, mainContent);
frmEnterPersonalDetailsKA.add(overview);
};
function frmEnterPersonalDetailsKAGlobalsAr() {
frmEnterPersonalDetailsKAAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmEnterPersonalDetailsKAAr,
"bounces": true,
"enableScrolling": true,
"enabledForIdleTimeout": true,
"id": "frmEnterPersonalDetailsKA",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"preShow": AS_Form_35e9775962fb4a0c8012cc069ceab941,
"skin": "sknmainGradient"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"inTransitionConfig": {
"formAnimation": 0
},
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_e01d93db2e5f49908dd240043cc063ff,
"outTransitionConfig": {
"formAnimation": 0
},
"retainScrollPosition": false,
"titleBar": true,
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
