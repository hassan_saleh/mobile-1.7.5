//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:46 EEST 2020
function initializeCopyFBox02b498f6048be4eAr() {
    CopyFBox02b498f6048be4eAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100dp",
        "id": "CopyFBox02b498f6048be4e",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "width": "100%"
    }, {
        "containerWeight": 100
    }, {});
    CopyFBox02b498f6048be4eAr.setDefaultUnit(kony.flex.DP);
    var informationListDivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "informationListDivider",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "top": "99dp",
        "width": "95%",
        "zIndex": 1
    }, {
        "containerWeight": 100
    }, {});
    informationListDivider.setDefaultUnit(kony.flex.DP);
    informationListDivider.add();
    var informationListIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "28dp",
        "id": "informationListIcon",
        "isVisible": true,
        "right": "5%",
        "skin": "sknslImage",
        "src": "imagedrag.png",
        "top": "0dp",
        "width": "28dp"
    }, {
        "containerWeight": 100,
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "margin": [ 0, 0,0, 0],
        "marginInPixel": false,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {});
    var informationListLabel = new kony.ui.Label({
        "id": "informationListLabel",
        "isVisible": true,
        "right": "18%",
        "skin": "skn",
        "text": "Label",
        "top": "22dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "hExpand": true,
        "margin": [ 1, 1,1, 1],
        "marginInPixel": false,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false
    });
    var addressLine1 = new kony.ui.Label({
        "id": "addressLine1",
        "isVisible": true,
        "right": "18%",
        "skin": "sknlocatorListAddress",
        "text": "Label",
        "top": "47dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "hExpand": true,
        "margin": [ 1, 1,1, 1],
        "marginInPixel": false,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false
    });
    var addressLine2 = new kony.ui.Label({
        "id": "addressLine2",
        "isVisible": true,
        "right": "18%",
        "skin": "sknlocatorListAddress",
        "text": "Label",
        "top": "63dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "hExpand": true,
        "margin": [ 1, 1,1, 1],
        "marginInPixel": false,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false
    });
    var distanceLabel = new kony.ui.Label({
        "centerY": "50%",
        "id": "distanceLabel",
        "isVisible": true,
        "left": "10%",
        "skin": "skn",
        "text": "Label",
        "top": "39dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "hExpand": true,
        "margin": [ 1, 1,1, 1],
        "marginInPixel": false,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false
    });
    CopyFBox02b498f6048be4eAr.add(informationListDivider, informationListIcon, informationListLabel, addressLine1, addressLine2, distanceLabel);
}
