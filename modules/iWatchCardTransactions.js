function iWatchserv_getCrCardTransactions(data, dateOPTIONS) {
  kony.print("Iwatch :: Inside iWatchserv_getCrCardTransactions for :: "+ JSON.stringify(data));
  try {
    if (kony.sdk.isNetworkAvailable()) {
      var month = "";
      var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
      var options = {
        "access": "online",
        "objectName": "RBObjects"
      };
      objectService = kony.sdk.getCurrentInstance().getObjectService("RBObjects",options);
      var date1 = new Date();
      var curdate = "";
      var splitDATE = "";
      if (dateOPTIONS.toDATE !== "" && dateOPTIONS.toDATE !== null) {
        splitDATE = dateOPTIONS.toDATE.split("-");
        curdate = splitDATE[2] + "" + splitDATE[1] + "" + splitDATE[0];
      } else
        curdate = (date1.getDate() < 10 ? "0" + date1.getDate() : date1.getDate()) + "" + ((date1.getMonth() + 1) < 10 ? "0" + (date1.getMonth() + 1) : (date1.getMonth() + 1)) + "" + date1.getFullYear();
      var headers = {};
      var serviceName = "RBObjects";
      var dataObject = new kony.sdk.dto.DataObject("Cards");

      dataObject.addField("custId", iWatchcustid);
      dataObject.addField("p_trn_nbr", "45");
      dataObject.addField("DateTo", curdate);
      var monthval = (parseInt(date1.getMonth()) - 2) < 0 ? 11 : (parseInt(date1.getMonth()) - 1);

      date1.setDate(date1.getDate() - 60);
      if (dateOPTIONS.fromDATE !== "" && dateOPTIONS.fromDATE !== null) {
        splitDATE = "";
        splitDATE = dateOPTIONS.fromDATE.split("-");
        curdate = splitDATE[2] + "" + splitDATE[1] + "" + splitDATE[0];
      } else
        curdate = "01" + ((date1.getMonth() + 1) < 10 ? "0" + (date1.getMonth() + 1) : (date1.getMonth() + 1)) + date1.getFullYear();
      dataObject.addField("DateFrom", curdate);
      dataObject.addField("CardNum", data.card_num);
      var serviceOptions = {
        "dataObject": dataObject,
        "headers": headers
      };
      kony.print("Iwatch :: iWatchserv_getCrCardTransactions dataObject ::" + JSON.stringify(dataObject));

      objectService.customVerb("getCardTransHistory", serviceOptions, iWatchserv_successgetCardTransactions, iWatchfetch_CARDTRANSACTIONFAILURE);
    }else{
      kony.print("Iwatch ::Exception_serv_getCrCardTransactions :: No Internet" );
      return_watchrequest({"cardTransactions": [],"isUserLoggedIn": true, "isNetworkAvailable": false,"isRetry": true});
    }
  } catch (e) {
    kony.print("Iwatch ::Exception_serv_getCrCardTransactions ::" + e);
    return_watchrequest({"cardTransactions": [],"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": false});
  }
}


function iWatchfetch_CARDTRANSACTIONFAILURE(err) {
  kony.print("Iwatch :: iWatchfetch_CARDTRANSACTIONFAILURE Failed ::" + JSON.stringify(err));
  return_watchrequest({"cardTransactions": [],"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": false});
}


function iWatchserv_successgetCardTransactions(response) {
  kony.print("Iwatch :: iWatchserv_successgetCardTransactions response for card transaction ::" + JSON.stringify(response));
  var temp = [];
  var data = "";
  var currencyCode = "";
  var skin = "";
  var category = "";
  kony.retailBanking.globalData.accountsTransactionList = {
    "transaction": temp
  };

  if (response.Transactions !== undefined && response.Transactions !== null) {
    data = response.Transactions;
    currencyCode = "JOD";
    for (var i in kony.retailBanking.globalData.accountsDashboardData.accountsData) {
      if (kony.retailBanking.globalData.accountsDashboardData.accountsData[i].accountID === CARDLIST_DETAILS.cardNumber) {
        currencyCode = kony.retailBanking.globalData.accountsDashboardData.accountsData[i].currencyCode;
        break;
      }
    }
    for (var j in data) {
      if (data[j].txnType === "D")
        data[j].amount = "- " + data[j].amount;
      else if (data[j].txnType === "C")
        data[j].amount = "+ " + data[j].amount;
    }
  } else if (response.History !== undefined && response.History !== null) {
    for (var i = 0; i < response.History.length; i++) {
      response.History[i].description = response.History[i].merchant_name_location;
      response.History[i].amount = response.History[i].trn_amt;
    }
    data = response.History;
    currencyCode = "JOD";
  }
  kony.print("Iwatch ::Currency ::" + currencyCode);
  for (var j in data) {
    var transDate = data[j].trn_date || data[j].transactionDate;
    if (!isEmpty(transDate)) {
      transDate = transDate.split('-');
      transDate = transDate[2] + "/" + transDate[1] + "/" + transDate[0];
    }
    data[j].transactionDate = transDate;

  }
  if (data !== "" && data !== null) {
    for (var i = 0; i < data.length; i++) {
      skin = "";

      if (data[i].trn_ccy !== undefined && data[i].trn_ccy !== null) {
        kony.print("Iwatch ::Transaction Currency ::" + data[i].trn_ccy);
        currencyCode = getISOCurrency(data[i].trn_ccy);
      }
      if (data[i].atmLoc !== undefined && data[i].atmLoc !== null) {
        data[i].description = data[i].description + " - " + data[i].atmLoc;
      }
      if (data[i].amount.indexOf("-") > -1) {
        data[i].amount = data[i].amount.substring(0, 1) + " " + data[i].amount.substring(1, data[i].amount.length);
        category = "debit";
      } else {
        category = "credit";
      }
      kony.print("Iwatch ::Parsed Amount ::" + data[i].amount);
      temp.push({
        "transactionDate": {
          "text": data[i].transactionDate,
          "isVisible": true
        },
        "description": {
          "text": data[i].description,
          "isVisible": true
        },
        "amount": {
          "text": data[i].amount + " " + currencyCode,
          "isVisible": true
        },
        "lblLastTransaction": {
          "text": "",
          "isVisible": true
        },
        "category": category
      });
    }
    kony.retailBanking.globalData.accountsTransactionList = {
      "transaction": temp
    };

    var data_WATCH = [];
    var length_WATCHDATA = (kony.retailBanking.globalData.accountsTransactionList.transaction.length > 5) ? 5 : kony.retailBanking.globalData.accountsTransactionList.transaction.length;
    for (var l = 0; l < length_WATCHDATA; l++) {
      data_WATCH.push(kony.retailBanking.globalData.accountsTransactionList.transaction[l]);
      data_WATCH[l].trn_ccy = getISOCurrency(data_WATCH[l].trn_ccy);
    }
    return_watchrequest({"cardTransactions": data_WATCH,"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": true});
    return;
  } else {
    return_watchrequest({"cardTransactions": [],"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": true});
    return;
  }
}

function iWatchserv_FETCHDEBITCARDTRANSACTION(data, dateOPTIONS) {
  try {
    kony.print("Iwatch ::iWatchserv_FETCHDEBITCARDTRANSACTION Card Data ::" + JSON.stringify(data));
    var curDate = new Date();
    var month = "",
        date = "";
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
      "access": "online",
      "objectName": "RBObjects"
    };
    var headers = {}; 
    objectService = kony.sdk.getCurrentInstance().getObjectService("RBObjects",options);
    var dataObject = new kony.sdk.dto.DataObject("Transactions");
    dataObject.addField("custId", iWatchcustid);
    month = ((curDate.getMonth() + 1) > 9) ? (curDate.getMonth() + 1) : "0" + (curDate.getMonth() + 1);
    date = ((curDate.getDate() > 9) ? (curDate.getDate()) : "0" + curDate.getDate());
    if (dateOPTIONS.toDATE !== "" && dateOPTIONS.toDATE !== null) {
      dataObject.addField("p_toDate", dateOPTIONS.toDATE);
    } else
      dataObject.addField("p_toDate", curDate.getFullYear() + "-" + month + "-" + date);

    curDate.setDate(curDate.getDate() - 60);
    month = ((curDate.getMonth() + 1) > 9) ? (curDate.getMonth() + 1) : "0" + (curDate.getMonth() + 1);

    if (dateOPTIONS.fromDATE !== "" && dateOPTIONS.fromDATE !== null) {
      dataObject.addField("p_fromDate", dateOPTIONS.fromDATE);
    } else
      dataObject.addField("p_fromDate", curDate.getFullYear() + "-" + month + "-01");
    dataObject.addField("p_debit_card_no", data.card_number);
    var serviceOptions = {
      "dataObject": dataObject,
      "headers": headers
    };
    kony.print("Iwatch ::Debit Card ::" + JSON.stringify(dataObject));
    if (kony.sdk.isNetworkAvailable()) {
      objectService.customVerb("getDbCardTxnList", serviceOptions, function(res) {
        kony.print("Iwatch ::Success ::" + JSON.stringify(res));
        iWatchserv_successgetCardTransactions(res);
      }, iWatchfetch_CARDTRANSACTIONFAILURE);
    }else{
      kony.print("Iwatch ::iWatchserv_FETCHDEBITCARDTRANSACTION  :: No internet");
      return_watchrequest({"cardTransactions": [],"isUserLoggedIn": true, "isNetworkAvailable": false,"isRetry": true});
    }
  } catch (e) {
    kony.print("Iwatch ::iWatchserv_FETCHDEBITCARDTRANSACTION Exception_serv_FETCHDEBITCARDTRANSACTION ::" + e);
    return_watchrequest({"cardTransactions": [],"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": false});
  }
}


function iWatchserv_getCardTransactions(data, dateOPTIONS) {
  try {
    kony.print("Iwatch ::iWatchserv_getCardTransactions Card Data ::" + JSON.stringify(data));
    var queryParams = {
      "custId": iWatchcustid,
      "accountNumber": data.card_acc_num,
      "p_debit_card_no": data.card_number,
      "p_no_Of_Txn": "5",
      "p_Branch": data.card_acc_branch,
      "p_toDate": ((dateOPTIONS.toDATE !== "") && (dateOPTIONS.toDATE !== null)) ? dateOPTIONS.toDATE : "",
      "p_fromDate": ((dateOPTIONS.fromDATE !== "") && (dateOPTIONS.fromDATE !== null)) ? dateOPTIONS.fromDATE : "",
      "lang": "eng"
    };
    kony.print("Iwatch ::iWatchserv_getCardTransactions card transactions parameters ::" + JSON.stringify(queryParams));
    var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJGetTransactionDetails");
    if (kony.sdk.isNetworkAvailable()) {
      appMFConfiguration.invokeOperation("prGetTransactionsDetails", {}, queryParams, iWatchserv_successgetCardTransactions, iWatchfetch_CARDTRANSACTIONFAILURE);
    }else{
      kony.print("Iwatch ::iWatchserv_getCardTransactions Exception_serv_getCardTransactions :: No internet"); 
      return_watchrequest({"cardTransactions": [],"isUserLoggedIn": true, "isNetworkAvailable": false,"isRetry": true});
    }
  } catch (e) {
    kony.print("Iwatch ::iWatchserv_getCardTransactions Exception_serv_getCardTransactions ::" + e);
    return_watchrequest({"cardTransactions": [],"isUserLoggedIn": true, "isNetworkAvailable": true,"isRetry": false});
  }
}