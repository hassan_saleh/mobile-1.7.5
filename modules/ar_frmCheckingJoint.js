//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmCheckingJointAr() {
    frmCheckingJoint.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "8%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var lblCheckingAccount = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblCheckingAccount",
        "isVisible": true,
        "skin": "sknnavBarTitle",
        "text": "Checking Account - Joint",
        "width": "70%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgCancelKA = new kony.ui.Image2({
        "centerY": "50%",
        "height": "45%",
        "id": "imgCancelKA",
        "isVisible": true,
        "right": "4%",
        "onTouchStart": AS_Image_c1a2c39535ce4bcbbf40e75e1d31e716,
        "skin": "slImage",
        "src": "cancel.png",
        "width": "5%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    flxHeader.add(lblCheckingAccount, imgCancelKA);
    var FlxMainContainer = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "92%",
        "horizontalScrollIndicator": true,
        "id": "FlxMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknscrollBkgWhite",
        "top": "8%",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlxMainContainer.setDefaultUnit(kony.flex.DP);
    var searchSegmentedControllerWrapper = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "56dp",
        "id": "searchSegmentedControllerWrapper",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    searchSegmentedControllerWrapper.setDefaultUnit(kony.flex.DP);
    var tabsWrapper = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "tabsWrapper",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkgheader",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    tabsWrapper.setDefaultUnit(kony.flex.DP);
    var btnFeaturesKA = new kony.ui.Button({
        "focusSkin": "skntabSelected",
        "height": "100%",
        "id": "btnFeaturesKA",
        "isVisible": true,
        "right": "0dp",
        "onClick": AS_Button_d8ae087aa2f44a94aff6ace43c413a04,
        "skin": "skntabSelected",
        "text": kony.i18n.getLocalizedString("i18n.opening_account.btnFeatures"),
        "top": "0dp",
        "width": "33.33%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 1,0, 0],
        "paddingInPixel": false
    }, {});
    var btnChargesKA = new kony.ui.Button({
        "focusSkin": "skntabSelected",
        "height": "100%",
        "id": "btnChargesKA",
        "isVisible": true,
        "right": "33.33%",
        "onClick": AS_Button_dc37fde1644340758707bec52a8032b3,
        "skin": "sknbtnbgf9f9f9B1pxe2e2e2",
        "text": kony.i18n.getLocalizedString("i18n.opening_account.btnCharges"),
        "top": "0dp",
        "width": "33.33%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 1,0, 0],
        "paddingInPixel": false
    }, {});
    var btnInfoKA = new kony.ui.Button({
        "focusSkin": "skntabSelected",
        "height": "100%",
        "id": "btnInfoKA",
        "isVisible": true,
        "right": "66.66%",
        "onClick": AS_Button_cd131e274b4b4060b305f992c113351e,
        "skin": "sknbtnbgf9f9f9B1pxe2e2e2",
        "text": kony.i18n.getLocalizedString("i18n.opening_account.btnInfo"),
        "top": "0dp",
        "width": "33.33%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 1,0, 0],
        "paddingInPixel": false
    }, {});
    var tabDeselectedIndicator1KA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "tabDeselectedIndicator1KA",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "right": "33.33%",
        "skin": "sknCopyslFbox03fbc1653617542",
        "top": "0dp",
        "width": "33.33%"
    }, {}, {});
    tabDeselectedIndicator1KA.setDefaultUnit(kony.flex.DP);
    tabDeselectedIndicator1KA.add();
    var tabDeselectedIndicator2KA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "tabDeselectedIndicator2KA",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "right": "66.66%",
        "skin": "sknCopyslFbox03fbc1653617542",
        "top": "0dp",
        "width": "33.33%"
    }, {}, {});
    tabDeselectedIndicator2KA.setDefaultUnit(kony.flex.DP);
    tabDeselectedIndicator2KA.add();
    var tabSelectedIndicator = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "tabSelectedIndicator",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "sknCopyslFbox03fbc1653617542",
        "top": "0dp",
        "width": "33.33%"
    }, {}, {});
    tabSelectedIndicator.setDefaultUnit(kony.flex.DP);
    tabSelectedIndicator.add();
    var line1KA = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "2dp",
        "id": "line1KA",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    line1KA.setDefaultUnit(kony.flex.DP);
    line1KA.add();
    tabsWrapper.add(btnFeaturesKA, btnChargesKA, btnInfoKA, tabDeselectedIndicator1KA, tabDeselectedIndicator2KA, tabSelectedIndicator, line1KA);
    searchSegmentedControllerWrapper.add(tabsWrapper);
    var flxContent = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90.50%",
        "id": "flxContent",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "skin": "slFbox",
        "top": "56dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxContent.setDefaultUnit(kony.flex.DP);
    var RichTextInfoMain = new kony.ui.RichText({
        "id": "RichTextInfoMain",
        "isVisible": true,
        "right": "0dp",
        "skin": "rctTxtMessage",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 2, 2,5, 0],
        "paddingInPixel": false
    }, {});
    var RichTextInfo1 = new kony.ui.RichText({
        "id": "RichTextInfo1",
        "isVisible": false,
        "right": "0dp",
        "skin": "sknRichTxt100LR30363fKA",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 2, 2,5, 0],
        "paddingInPixel": false
    }, {});
    var RichTextInfo2 = new kony.ui.RichText({
        "id": "RichTextInfo2",
        "isVisible": false,
        "right": "0dp",
        "skin": "sknRichTxt100LR30363fKA",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 2, 2,5, 0],
        "paddingInPixel": false
    }, {});
    flxContent.add(RichTextInfoMain, RichTextInfo1, RichTextInfo2);
    FlxMainContainer.add(searchSegmentedControllerWrapper, flxContent);
    frmCheckingJoint.add(flxHeader, FlxMainContainer);
};
function frmCheckingJointGlobalsAr() {
    frmCheckingJointAr = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmCheckingJointAr,
        "bounces": false,
        "enabledForIdleTimeout": true,
        "id": "frmCheckingJoint",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "preShow": AS_Form_gdfe29d0dcd5424c9ac734db89e85194,
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_e1112daf5e714bd18d7e8ecf9c532b89,
        "retainScrollPosition": false,
        "titleBar": false,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};
