/*
 * Controller Extension class for frmCardsLandingKA
 * Developer can edit the existing methods or can add new methods if required
 *
 */

kony = kony || {};
kony.sdk = kony.sdk || {};
kony.sdk.mvvm = kony.sdk.mvvm || {};
kony.sdk.mvvm.v2 = kony.sdk.mvvm.v2 || {};

/**
 * Creates a new Form Controller Extension.
 * @class frmCardsLandingKAControllerExtension
 * @param {Object} controllerObj - Form Controller.
 */
kony.sdk.mvvm.frmCardsLandingKAControllerExtension = Class(kony.sdk.mvvm.BankingAppControllerExtension, {
    constructor: function(controllerObj) {
        this.$class.$super.call(this, controllerObj);
    },
    /** 
     * This method is an entry point for all fetch related flows. Developer can edit.
     * Default implementation fetches data for the form based on form config 
     * @memberof frmCardsLandingKAControllerExtension#
     */
    fetchData: function() {
        try {
            var scopeObj = this;
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen(" ");
            this.$class.$superp.fetchData.call(this, success, error);
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("Error in fetchData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_FETCH_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_FETCH_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

        function success(response) {
            kony.sdk.mvvm.log.info("success fetching data ", response);
        	kony.print("Fetch Card Details ::"+JSON.stringify(response));
        	if(response._raw_response_.segCardsLanding.opstatus !== 0 || (response._raw_response_.segCardsLanding.errmsg !== "" && response._raw_response_.segCardsLanding.errmsg !== undefined))
              customAlertPopup(geti18Value("i18n.maps.Info"), geti18nkey("i18n.cards.unabletoretrivecreditcard"),popupCommonAlertDimiss,"");
//           getDWCardListDetails( this, response);
        	this.getController().processData(response);
        }

        function error(err) {
            //Error fetching data
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("In fetchData errorcallback in controller extension ", err);
//         	getDWCardListDetails( this, []);
          		gblCardReorder = false;
  				gblCardAddNickname = false;
            var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_FETCH_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_FETCH_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        	this.getController().processData();
        }
    },
    /** 
     * This method processes fetched data. Developer can edit.
     * Default implementation processes the provided data to required format for bind.
     * @param {Object} data - fetched data. (Default : data map, group id as key and records array as value)
     * @memberof frmCardsLandingKAControllerExtension#
     * @returns {Object} - processed data
     */
    processData: function(data) {
        try {
          var scopeObj = this;
          var processedData = this.$class.$superp.processData.call(this, data);
          var card_limit = 0;
          kony.retailBanking.globalData.isCreditCardAvailable = false;
          kony.retailBanking.globalData.isDebitCardAvailable = false;
          kony.retailBanking.globalData.isWebchargeCardAvailable = false;
          kony.print("ProcessData "+JSON.stringify(data));
          kony.print("gblCardReorder ::"+gblCardReorder +"  gblCardAddNickname ::"+gblCardAddNickname+" is_CARDPAYMENTSUCCESS ::"+is_CARDPAYMENTSUCCESS);

          kony.retailBanking.globalData.prCardLandinList = [];
          kony.retailBanking.globalData.prCreditCardPayList =[];
          if(!isEmpty(data._raw_response_.segCardsLanding.records[0].AllCardList)){
            kony.retailBanking.globalData.prCreditCardPayList = data._raw_response_.segCardsLanding.records[0].AllCardList;
          }
          if(!isEmpty(kony.retailBanking.globalData.prCreditCardPayList) && kony.retailBanking.globalData.prCreditCardPayList.length >0){
            for(var j in kony.retailBanking.globalData.prCreditCardPayList){
              kony.retailBanking.globalData.prCreditCardPayList[j].isWearablePrepaid = false;
              if(kony.retailBanking.globalData.prCreditCardPayList[j].cardTypeFlag === "C"){
                card_limit = parseInt(kony.retailBanking.globalData.prCreditCardPayList[j].card_limit);
                if(card_limit > 0)
                	kony.retailBanking.globalData.isCreditCardAvailable = true;
                else if(card_limit == 0){
//                 	kony.retailBanking.globalData.isWebchargeCardAvailable = true;
                	kony.retailBanking.globalData.prCreditCardPayList[j].isWearablePrepaid = true;
                	kony.retailBanking.globalData.prCreditCardPayList[j].cardTypeFlag = "W";
                }
              }
              if(kony.retailBanking.globalData.prCreditCardPayList[j].cardTypeFlag === "D"){
                kony.retailBanking.globalData.isDebitCardAvailable = true;
              }
              if(kony.retailBanking.globalData.prCreditCardPayList[j].cardTypeFlag === "W"){
              	kony.retailBanking.globalData.isWebchargeCardAvailable = true;
              }
              if(kony.retailBanking.globalData.prCreditCardPayList[j].showHideFlag === "T" || kony.retailBanking.globalData.prCreditCardPayList[j].showHideFlag === "Y")
                kony.retailBanking.globalData.prCardLandinList.push(kony.retailBanking.globalData.prCreditCardPayList[j]);
            }
          }
          var tempdata = [];
          if(kony.retailBanking.globalData.prCardLandinList.length > 0){
            tempdata = process_CardListDetails(kony.retailBanking.globalData.prCardLandinList);
          }
          var navigationObject = this.getController() && this.getController().getContextData();
          navigationObject.setCustomInfo("segCardsLanding",tempdata);
          if(gblCardReorder || gblCardAddNickname){             
            callGetCardReorderServiceCall();
          }else if(!is_CARDPAYMENTSUCCESS){
            this.getController().bindData(processedData);
            return processedData;
          }else{
            if(tempdata.length > 0)
              get_cardDataList();
            else{
              is_CARDPAYMENTSUCCESS = false;
              frmWebCharge.destroy();
              frmWebCharge.txtFieldAmount.text = "";
              frmCreditCardPayment.destroy();
              frmCreditCardPayment.txtFieldAmount.text = "";
              this.getController().bindData(processedData);
              return processedData;
            }
          }
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            gblCardReorder = false;
  			gblCardAddNickname = false;
            kony.sdk.mvvm.log.error("Error in processData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_PROCESSDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_PROCESSDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    /** 
     * This method binds the processed data to the form. Developer can edit.
sou     * @param {Object} data - processed data.(Default : data map for each group, widget id as key and widget data as value)
     * @memberof frmCardsLandingKAControllerExtension#
     */
    bindData: function(data) {
        try {
            var formmodel = this.getController().getFormModel();
            formmodel.clear();
            kony.print("bindData data::"+JSON.stringify(data));
        	var navigationObject = this.getController() && this.getController().getContextData();
        	var cardData = navigationObject.getCustomInfo("segCardsLanding");
            kony.print("accData segAccountsKA:::::::" + JSON.stringify(cardData));
        	if(cardData.length > 0){
              data["segCardsLanding"]["segCardsLanding"].setData(cardData);
              var widgetSetData = formmodel.getViewAttributeByProperty("segCardsLanding", "setData");
              widgetSetData.setData = cardData;
              formmodel.setViewAttributeByProperty('segCardsLanding', "setData", widgetSetData);
              formmodel.setViewAttributeByProperty('segCardsLanding', "isVisible", true);
              formmodel.setViewAttributeByProperty("LabelNoRecordsKA","isVisible",false);
            }else{
            	formmodel.setViewAttributeByProperty('segCardsLanding', "isVisible", false);
            	formmodel.setViewAttributeByProperty("LabelNoRecordsKA","isVisible",true);
            	if(hasCasaAccounts){
                	customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.common.systemload"), function(){kony.sdk.mvvm.LogoutAction();popupCommonAlertDimiss();}, "");
                }
            }
        	formmodel.setViewAttributeByProperty('flxScrollMain',"isVisible",true);
        	formmodel.setViewAttributeByProperty('flxApplyNewCards',"isVisible",false);
        	formmodel.setViewAttributeByProperty('btnAllCards',"skin","btnAccounts");
        	formmodel.setViewAttributeByProperty('btnApplyCards',"skin","sknTransparentBGRNDFontBOJ");
        	formmodel.setViewAttributeByProperty('btnApplyCards',"isVisible",true);
        	formmodel.setViewAttributeByProperty('lblBack',"text",geti18Value("i18n.accounts.accounts"));
        	//
            	set_UI_FOR_CASA_USER(frmCardsLandingKA);
            //
            kony.print("bindData data::"+JSON.stringify(data));
        	this.getController().getFormModel().formatUI();
            this.$class.$superp.bindData.call(this, data);
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            this.getController().showForm();
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("Error in bindData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_BINDDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_BINDDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

    },
    /** 
     * This method is entry point for save flow. Developer can edit.
     * Default implementation saves the entity record from the data of widgets defined in form config 
     * @memberof frmCardsLandingKAControllerExtension#
     */
    saveData: function() {
        try {
            var scopeObj = this;
            this.$class.$superp.saveData.call(this, success, error);
        } catch (err) {
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

        function success(res) {
            //Successfully created record
            kony.sdk.mvvm.log.info("success saving record ", res);
        }

        function error(err) {
            //Handle error case
            kony.sdk.mvvm.log.error("In saveData errorcallback in controller extension ", err);
            var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

    },
    /** 
     * This method is entry point for delete/remove flow. Developer can edit.
     * Default implementation deletes the entity record displayed in form (primary keys are needed)
     * @memberof frmCardsLandingKAControllerExtension#
     */
    deleteData: function() {
        try {
            var scopeObj = this;
            this.$class.$superp.deleteData.call(this, success, error);
        } catch (err) {
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

        function success(res) {
            //Successfully deleting record
            kony.sdk.mvvm.log.info("success deleting record " + JSON.stringify(res));
        }

        function error(err) {
            //Handle error case
            kony.sdk.mvvm.log.error("In deleteData errorcallback in controller extension ", err);
            var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    /** 
     * This method shows form.
     * @memberof frmCardsLandingKAControllerExtension#
     */
    showForm: function() {
        try {
            var formmodel = this.getController().getFormModel();
            formmodel.showView();
        } catch (e) {
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SHOWFORM_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SHOWFORM_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    }
});