//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmUserSettingsMyProfileKAAr() {
frmUserSettingsMyProfileKA.setDefaultUnit(kony.flex.DP);
var flxHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "s",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxHeader.setDefaultUnit(kony.flex.DP);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_b53e7b129d034a25b7b81d2730acb461,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 2
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblBack0e1581dd5473c4d = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"centerY": "50%",
"id": "CopylblBack0e1581dd5473c4d",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBack.add(lblBackIcon, CopylblBack0e1581dd5473c4d);
var lblheadertext = new kony.ui.Label({
"height": "100%",
"id": "lblheadertext",
"isVisible": true,
"left": "20%",
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.appsettings.Profile"),
"top": "0%",
"width": "60%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxHeader.add(flxBack, lblheadertext);
var mainContent = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "91%",
"horizontalScrollIndicator": true,
"id": "mainContent",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "sknConfirmTransfer",
"top": "9%",
"verticalScrollIndicator": false,
"width": "100%",
"zIndex": 1
}, {}, {});
mainContent.setDefaultUnit(kony.flex.DP);
var androidSettings = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "androidSettings",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
androidSettings.setDefaultUnit(kony.flex.DP);
var loginSettingsSegmentAndroid = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"data": [{
"HiddenLbl": "",
"imgicontick": "right_chevron_icon.png",
"lblPageNameKA": kony.i18n.getLocalizedString("i18n.settings.myProfile.ChangeUsername")
}, {
"HiddenLbl": "",
"imgicontick": "right_chevron_icon.png",
"lblPageNameKA": kony.i18n.getLocalizedString("i18n.settings.myprofile.ChangePassword")
}, {
"HiddenLbl": "",
"imgicontick": "right_chevron_icon.png",
"lblPageNameKA": kony.i18n.getLocalizedString("i18n.settings.myprofile.PersonalDetails")
}],
"groupCells": false,
"id": "loginSettingsSegmentAndroid",
"isVisible": false,
"right": "0dp",
"needPageIndicator": true,
"onRowClick": AS_Segment_0ebdd5674f3a4f5b9779918359f33cd1,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowTemplate": container,
"scrollingEvents": {},
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "64646400",
"separatorRequired": false,
"separatorThickness": 0,
"showScrollbars": false,
"top": "0dp",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"container": "container",
"imgicontick": "imgicontick",
"lblPageNameKA": "lblPageNameKA"
},
"width": "100%"
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyLabel0b42349f737df43 = new kony.ui.Label({
"id": "CopyLabel0b42349f737df43",
"isVisible": true,
"right": "7%",
"skin": "sknCarioRegular130149164",
"text": kony.i18n.getLocalizedString("i18n.myprofile.FirstName"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblFirstName = new kony.ui.Label({
"height": "33dp",
"id": "lblFirstName",
"isVisible": true,
"right": "7%",
"skin": "sknCarioRegular255255255Per120",
"text": "John",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "-12px",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var segmentBorderBottomAndroid = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2px",
"id": "segmentBorderBottomAndroid",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknBlueLineSeparator023356",
"top": "-1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
segmentBorderBottomAndroid.setDefaultUnit(kony.flex.DP);
segmentBorderBottomAndroid.add();
var Label0f1634615bdd144 = new kony.ui.Label({
"id": "Label0f1634615bdd144",
"isVisible": true,
"right": "7%",
"skin": "sknCarioRegular130149164",
"text": kony.i18n.getLocalizedString("i18n.myprofile.CustomerSegment"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCustomerSegmet = new kony.ui.Label({
"height": "33dp",
"id": "lblCustomerSegmet",
"isVisible": true,
"right": "7%",
"skin": "sknCarioRegular255255255Per120",
"text": "WISAM+",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "-12px",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopysegmentBorderBottomAndroid0a046ebf4199d49 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2px",
"id": "CopysegmentBorderBottomAndroid0a046ebf4199d49",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknBlueLineSeparator023356",
"top": "-1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopysegmentBorderBottomAndroid0a046ebf4199d49.setDefaultUnit(kony.flex.DP);
CopysegmentBorderBottomAndroid0a046ebf4199d49.add();
var CopyLabel0bed1199cc28146 = new kony.ui.Label({
"id": "CopyLabel0bed1199cc28146",
"isVisible": false,
"right": "6.93%",
"skin": "sknCarioRegular130149164",
"text": kony.i18n.getLocalizedString("i18n.myprofile.LastName"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblLastName = new kony.ui.Label({
"id": "lblLastName",
"isVisible": false,
"right": "7%",
"skin": "sknCarioRegular255255255Per120",
"text": "Doe",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "-12px",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopysegmentBorderBottomAndroid0e222c15b4a3546 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2px",
"id": "CopysegmentBorderBottomAndroid0e222c15b4a3546",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknBlueLineSeparator023356",
"top": "-1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopysegmentBorderBottomAndroid0e222c15b4a3546.setDefaultUnit(kony.flex.DP);
CopysegmentBorderBottomAndroid0e222c15b4a3546.add();
var flxSmsNumber = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "flxSmsNumber",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxSmsNumber.setDefaultUnit(kony.flex.DP);
var CopyLabel0j8008ed4b2dc42 = new kony.ui.Label({
"id": "CopyLabel0j8008ed4b2dc42",
"isVisible": true,
"right": "7.00%",
"skin": "sknCarioRegular130149164",
"text": kony.i18n.getLocalizedString("i18n.myprofile.SMSNumber"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var btnEdit = new kony.ui.Button({
"focusSkin": "sknBtnContactList",
"height": "40dp",
"id": "btnEdit",
"isVisible": true,
"onClick": AS_Button_j3cdada32d3e4eacafe1a82af0549c2f,
"left": "10dp",
"skin": "sknBtnContactList",
"text": "0",
"top": "5dp",
"width": "45dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxSmsNumber.add(CopyLabel0j8008ed4b2dc42, btnEdit);
var lblSMSNumber = new kony.ui.Label({
"height": "33dp",
"id": "lblSMSNumber",
"isVisible": true,
"right": "7%",
"skin": "sknCarioRegular255255255Per120",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "-12px",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopysegmentBorderBottomAndroid0d219f012594d42 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2px",
"id": "CopysegmentBorderBottomAndroid0d219f012594d42",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknBlueLineSeparator023356",
"top": "-1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopysegmentBorderBottomAndroid0d219f012594d42.setDefaultUnit(kony.flex.DP);
CopysegmentBorderBottomAndroid0d219f012594d42.add();
var CopyLabel0i58f4b23272c4e = new kony.ui.Label({
"id": "CopyLabel0i58f4b23272c4e",
"isVisible": true,
"right": "7%",
"skin": "sknCarioRegular130149164",
"text": kony.i18n.getLocalizedString("i18n.myprofile.MobileNumber"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblMobileNumber = new kony.ui.Label({
"height": "33dp",
"id": "lblMobileNumber",
"isVisible": true,
"right": "7%",
"skin": "sknCarioRegular255255255Per120",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "-12px",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopysegmentBorderBottomAndroid0aa09484e57e54a = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2px",
"id": "CopysegmentBorderBottomAndroid0aa09484e57e54a",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknBlueLineSeparator023356",
"top": "-1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopysegmentBorderBottomAndroid0aa09484e57e54a.setDefaultUnit(kony.flex.DP);
CopysegmentBorderBottomAndroid0aa09484e57e54a.add();
var CopyLabel0ae05a5f8bdd547 = new kony.ui.Label({
"id": "CopyLabel0ae05a5f8bdd547",
"isVisible": true,
"right": "7%",
"skin": "sknCarioRegular130149164",
"text": kony.i18n.getLocalizedString("i18n.myprofile.LandLineNumber"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblLandLineNumber = new kony.ui.Label({
"height": "33dp",
"id": "lblLandLineNumber",
"isVisible": true,
"right": "7%",
"skin": "sknCarioRegular255255255Per120",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "-12px",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopysegmentBorderBottomAndroid0g81b854421c345 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2px",
"id": "CopysegmentBorderBottomAndroid0g81b854421c345",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknBlueLineSeparator023356",
"top": "-1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopysegmentBorderBottomAndroid0g81b854421c345.setDefaultUnit(kony.flex.DP);
CopysegmentBorderBottomAndroid0g81b854421c345.add();
var CopyLabel0ed1699cf2f324c = new kony.ui.Label({
"id": "CopyLabel0ed1699cf2f324c",
"isVisible": true,
"right": "7%",
"skin": "sknCarioRegular130149164",
"text": kony.i18n.getLocalizedString("i18n.myprofile.EmailAdderss"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblEmailAddress = new kony.ui.Label({
"height": "33dp",
"id": "lblEmailAddress",
"isVisible": true,
"right": "7%",
"skin": "sknCarioRegular255255255Per120",
"text": "john.doe@boj.com",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "-12px",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopysegmentBorderBottomAndroid0c415b220f0084f = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2px",
"id": "CopysegmentBorderBottomAndroid0c415b220f0084f",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknBlueLineSeparator023356",
"top": "-1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopysegmentBorderBottomAndroid0c415b220f0084f.setDefaultUnit(kony.flex.DP);
CopysegmentBorderBottomAndroid0c415b220f0084f.add();
var CopyLabel0b1e784b4b1364f = new kony.ui.Label({
"id": "CopyLabel0b1e784b4b1364f",
"isVisible": true,
"right": "7.44%",
"skin": "sknCarioRegular130149164",
"text": kony.i18n.getLocalizedString("i18n.myprofile.AddressLine1"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblAddressLine01 = new kony.ui.Label({
"height": "33dp",
"id": "lblAddressLine01",
"isVisible": true,
"right": "7%",
"skin": "sknCarioRegular255255255Per120",
"text": "7380 Madiha Appartments",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "-12px",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopysegmentBorderBottomAndroid0bab0b7b3cf6242 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2px",
"id": "CopysegmentBorderBottomAndroid0bab0b7b3cf6242",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknBlueLineSeparator023356",
"top": "-1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopysegmentBorderBottomAndroid0bab0b7b3cf6242.setDefaultUnit(kony.flex.DP);
CopysegmentBorderBottomAndroid0bab0b7b3cf6242.add();
var CopyLabel0i3a5453815b74f = new kony.ui.Label({
"id": "CopyLabel0i3a5453815b74f",
"isVisible": true,
"right": "7%",
"skin": "sknCarioRegular130149164",
"text": kony.i18n.getLocalizedString("i18n.myprofile.AddressLine2"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblAddressLine02 = new kony.ui.Label({
"height": "33dp",
"id": "lblAddressLine02",
"isVisible": true,
"right": "7%",
"skin": "sknCarioRegular255255255Per120",
"text": "West Sand Lake Road",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "-12px",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopysegmentBorderBottomAndroid0d29b652b34754b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2px",
"id": "CopysegmentBorderBottomAndroid0d29b652b34754b",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknBlueLineSeparator023356",
"top": "-1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopysegmentBorderBottomAndroid0d29b652b34754b.setDefaultUnit(kony.flex.DP);
CopysegmentBorderBottomAndroid0d29b652b34754b.add();
var CopyLabel0ad1697272f774a = new kony.ui.Label({
"id": "CopyLabel0ad1697272f774a",
"isVisible": true,
"right": "7%",
"skin": "sknCarioRegular130149164",
"text": kony.i18n.getLocalizedString("i18n.myprofile.City"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblProfileCity = new kony.ui.Label({
"height": "33dp",
"id": "lblProfileCity",
"isVisible": true,
"right": "7%",
"skin": "sknCarioRegular255255255Per120",
"text": "Amman",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "-12px",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopysegmentBorderBottomAndroid0jc62e4fdea0b41 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2px",
"id": "CopysegmentBorderBottomAndroid0jc62e4fdea0b41",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknBlueLineSeparator023356",
"top": "-1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopysegmentBorderBottomAndroid0jc62e4fdea0b41.setDefaultUnit(kony.flex.DP);
CopysegmentBorderBottomAndroid0jc62e4fdea0b41.add();
var CopyLabel0da6f3083eb0546 = new kony.ui.Label({
"id": "CopyLabel0da6f3083eb0546",
"isVisible": true,
"right": "7.00%",
"skin": "sknCarioRegular130149164",
"text": kony.i18n.getLocalizedString("i18n.myprofile.Country"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblProfileCountry = new kony.ui.Label({
"height": "33dp",
"id": "lblProfileCountry",
"isVisible": true,
"right": "7%",
"skin": "sknCarioRegular255255255Per120",
"text": "Jordan",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "-12px",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
androidSettings.add(loginSettingsSegmentAndroid, CopyLabel0b42349f737df43, lblFirstName, segmentBorderBottomAndroid, Label0f1634615bdd144, lblCustomerSegmet, CopysegmentBorderBottomAndroid0a046ebf4199d49, CopyLabel0bed1199cc28146, lblLastName, CopysegmentBorderBottomAndroid0e222c15b4a3546, flxSmsNumber, lblSMSNumber, CopysegmentBorderBottomAndroid0d219f012594d42, CopyLabel0i58f4b23272c4e, lblMobileNumber, CopysegmentBorderBottomAndroid0aa09484e57e54a, CopyLabel0ae05a5f8bdd547, lblLandLineNumber, CopysegmentBorderBottomAndroid0g81b854421c345, CopyLabel0ed1699cf2f324c, lblEmailAddress, CopysegmentBorderBottomAndroid0c415b220f0084f, CopyLabel0b1e784b4b1364f, lblAddressLine01, CopysegmentBorderBottomAndroid0bab0b7b3cf6242, CopyLabel0i3a5453815b74f, lblAddressLine02, CopysegmentBorderBottomAndroid0d29b652b34754b, CopyLabel0ad1697272f774a, lblProfileCity, CopysegmentBorderBottomAndroid0jc62e4fdea0b41, CopyLabel0da6f3083eb0546, lblProfileCountry);
mainContent.add(androidSettings);
var flxProfileEdit = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": false,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "100%",
"horizontalScrollIndicator": true,
"id": "flxProfileEdit",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "8%",
"verticalScrollIndicator": false,
"width": "100%",
"zIndex": 1
}, {}, {});
flxProfileEdit.setDefaultUnit(kony.flex.DP);
var CopyuserSettingsLabel0af5ce2f7e9cf48 = new kony.ui.Label({
"id": "CopyuserSettingsLabel0af5ce2f7e9cf48",
"isVisible": false,
"right": "8%",
"skin": "sknCarioSemiBold120White",
"text": "Phone",
"top": "2%",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxPrimaryPhoneNo = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxPrimaryPhoneNo",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "8%",
"skin": "sknslFbox",
"top": "0%",
"width": "75%",
"zIndex": 1
}, {}, {});
flxPrimaryPhoneNo.setDefaultUnit(kony.flex.PERCENTAGE);
var txtPrimaryPhoneNo = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "1%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "txtPrimaryPhoneNo",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"onTouchEnd": AS_TextField_h46760a3d8f54a2cb27a9334db597312,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "90%",
"zIndex": 20
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"placeholderSkin": "sknPlaceholderKA",
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var borderBottom = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "5%",
"id": "borderBottom",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "skntextFieldDivider",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
borderBottom.setDefaultUnit(kony.flex.DP);
borderBottom.add();
var lblPrimaryPhoneNo = new kony.ui.Label({
"id": "lblPrimaryPhoneNo",
"isVisible": true,
"right": "5%",
"skin": "sknCaiRegWhite50Op",
"text": "Primary Phone No.",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "35%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxPrimaryPhoneNo.add(txtPrimaryPhoneNo, borderBottom, lblPrimaryPhoneNo);
var flxSecondaryPhoneNo = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxSecondaryPhoneNo",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "8%",
"skin": "sknslFbox",
"top": "1%",
"width": "75%",
"zIndex": 1
}, {}, {});
flxSecondaryPhoneNo.setDefaultUnit(kony.flex.PERCENTAGE);
var txtSecondaryPhoneNo = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "1%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "txtSecondaryPhoneNo",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"onTouchEnd": AS_TextField_gc6bca232df948a594ad3893c77789c0,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "90%",
"zIndex": 20
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"placeholderSkin": "sknPlaceholderKA",
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var CopyborderBottom0d9349761e6944f = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "5%",
"id": "CopyborderBottom0d9349761e6944f",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "skntextFieldDivider",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
CopyborderBottom0d9349761e6944f.setDefaultUnit(kony.flex.DP);
CopyborderBottom0d9349761e6944f.add();
var lblSecondaryPhoneNo = new kony.ui.Label({
"id": "lblSecondaryPhoneNo",
"isVisible": true,
"right": "5%",
"skin": "sknCaiRegWhite50Op",
"text": "Secondary Phone No.",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "35%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxSecondaryPhoneNo.add(txtSecondaryPhoneNo, CopyborderBottom0d9349761e6944f, lblSecondaryPhoneNo);
var CopyuserSettingsLabel0hd99db2190ef44 = new kony.ui.Label({
"id": "CopyuserSettingsLabel0hd99db2190ef44",
"isVisible": true,
"right": "8%",
"skin": "sknCarioSemiBold120White",
"text": "Email",
"top": "6%",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxPrimaryEmail = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxPrimaryEmail",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "8%",
"skin": "sknslFbox",
"top": "0%",
"width": "75%",
"zIndex": 1
}, {}, {});
flxPrimaryEmail.setDefaultUnit(kony.flex.PERCENTAGE);
var txtPrimaryEmail = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "1%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "txtPrimaryEmail",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"onTouchEnd": AS_TextField_cae44c40e5cb40ffbb8818574a92f8ec,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "90%",
"zIndex": 20
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"placeholderSkin": "sknPlaceholderKA",
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var CopyborderBottom0ha841f72d3dd4b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "5%",
"id": "CopyborderBottom0ha841f72d3dd4b",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "skntextFieldDivider",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
CopyborderBottom0ha841f72d3dd4b.setDefaultUnit(kony.flex.DP);
CopyborderBottom0ha841f72d3dd4b.add();
var lblPrimaryEmail = new kony.ui.Label({
"id": "lblPrimaryEmail",
"isVisible": true,
"right": "5%",
"skin": "sknCaiRegWhite50Op",
"text": "Primary Email",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "35%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxPrimaryEmail.add(txtPrimaryEmail, CopyborderBottom0ha841f72d3dd4b, lblPrimaryEmail);
var flxSecondaryEmail = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxSecondaryEmail",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "8%",
"skin": "sknslFbox",
"top": "1%",
"width": "75%",
"zIndex": 1
}, {}, {});
flxSecondaryEmail.setDefaultUnit(kony.flex.PERCENTAGE);
var txtSecondaryEmail = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "1%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "txtSecondaryEmail",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"onTouchEnd": AS_TextField_b39498cb169b42c7a087ac598883d047,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "90%",
"zIndex": 20
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"placeholderSkin": "sknPlaceholderKA",
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var CopyborderBottom0f6eacdc8d58243 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "5%",
"id": "CopyborderBottom0f6eacdc8d58243",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "skntextFieldDivider",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
CopyborderBottom0f6eacdc8d58243.setDefaultUnit(kony.flex.DP);
CopyborderBottom0f6eacdc8d58243.add();
var lblSecondaryEmail = new kony.ui.Label({
"id": "lblSecondaryEmail",
"isVisible": true,
"right": "5%",
"skin": "sknCaiRegWhite50Op",
"text": "Secondary Email",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "35%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxSecondaryEmail.add(txtSecondaryEmail, CopyborderBottom0f6eacdc8d58243, lblSecondaryEmail);
var CopyuserSettingsLabel0c94a6316a25d49 = new kony.ui.Label({
"id": "CopyuserSettingsLabel0c94a6316a25d49",
"isVisible": true,
"right": "8%",
"skin": "sknCarioSemiBold120White",
"text": "Payee Address",
"top": "6%",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxAddressLine1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxAddressLine1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "8%",
"skin": "sknslFbox",
"top": "0%",
"width": "75%",
"zIndex": 1
}, {}, {});
flxAddressLine1.setDefaultUnit(kony.flex.PERCENTAGE);
var txtAddressLine1 = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "1%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "txtAddressLine1",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"onTouchEnd": AS_TextField_h11caa4ec8244f38be5ddf79340edde1,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "90%",
"zIndex": 20
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"placeholderSkin": "sknPlaceholderKA",
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var CopyborderBottom0j71ce24f68564b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "5%",
"id": "CopyborderBottom0j71ce24f68564b",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "skntextFieldDivider",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
CopyborderBottom0j71ce24f68564b.setDefaultUnit(kony.flex.DP);
CopyborderBottom0j71ce24f68564b.add();
var lblAddressLine1 = new kony.ui.Label({
"id": "lblAddressLine1",
"isVisible": true,
"right": "5%",
"skin": "sknCaiRegWhite50Op",
"text": "Line 1",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "35%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxAddressLine1.add(txtAddressLine1, CopyborderBottom0j71ce24f68564b, lblAddressLine1);
var flxAddressLine2 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxAddressLine2",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "8%",
"skin": "sknslFbox",
"top": "1%",
"width": "75%",
"zIndex": 1
}, {}, {});
flxAddressLine2.setDefaultUnit(kony.flex.PERCENTAGE);
var txtAddressLine2 = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "1%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "txtAddressLine2",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"onTouchEnd": AS_TextField_b147e66dd4bb44b28fd6c1d569d3e1e9,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "90%",
"zIndex": 20
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"placeholderSkin": "sknPlaceholderKA",
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var CopyborderBottom0gfdd39ea47f945 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "5%",
"id": "CopyborderBottom0gfdd39ea47f945",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "skntextFieldDivider",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
CopyborderBottom0gfdd39ea47f945.setDefaultUnit(kony.flex.DP);
CopyborderBottom0gfdd39ea47f945.add();
var lblAddressLine2 = new kony.ui.Label({
"id": "lblAddressLine2",
"isVisible": true,
"right": "5%",
"skin": "sknCaiRegWhite50Op",
"text": "Line 2",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "35%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxAddressLine2.add(txtAddressLine2, CopyborderBottom0gfdd39ea47f945, lblAddressLine2);
var flxCity = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxCity",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "8%",
"skin": "sknslFbox",
"top": "0%",
"width": "75%",
"zIndex": 1
}, {}, {});
flxCity.setDefaultUnit(kony.flex.PERCENTAGE);
var txtCity = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "1%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "txtCity",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"onTouchEnd": AS_TextField_f6a1e19aa5ab4f8595dfba98e2bfe6e4,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "90%",
"zIndex": 20
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"placeholderSkin": "sknPlaceholderKA",
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var CopyborderBottom0h6f61094faba4b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "5%",
"id": "CopyborderBottom0h6f61094faba4b",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "skntextFieldDivider",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
CopyborderBottom0h6f61094faba4b.setDefaultUnit(kony.flex.DP);
CopyborderBottom0h6f61094faba4b.add();
var lblCity = new kony.ui.Label({
"id": "lblCity",
"isVisible": true,
"right": "5%",
"skin": "sknCaiRegWhite50Op",
"text": "City",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "35%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxCity.add(txtCity, CopyborderBottom0h6f61094faba4b, lblCity);
var flxState = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxState",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "8%",
"skin": "sknslFbox",
"top": "1%",
"width": "75%",
"zIndex": 1
}, {}, {});
flxState.setDefaultUnit(kony.flex.PERCENTAGE);
var txtState = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "1%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "txtState",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"onTouchEnd": AS_TextField_bea7af23e8fe4318aa64634e1feb3fd1,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "90%",
"zIndex": 20
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"placeholderSkin": "sknPlaceholderKA",
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var CopyborderBottom0c512f90595e64e = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "5%",
"id": "CopyborderBottom0c512f90595e64e",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "skntextFieldDivider",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
CopyborderBottom0c512f90595e64e.setDefaultUnit(kony.flex.DP);
CopyborderBottom0c512f90595e64e.add();
var lblState = new kony.ui.Label({
"id": "lblState",
"isVisible": true,
"right": "5%",
"skin": "sknCaiRegWhite50Op",
"text": "State",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "35%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxState.add(txtState, CopyborderBottom0c512f90595e64e, lblState);
flxProfileEdit.add(CopyuserSettingsLabel0af5ce2f7e9cf48, flxPrimaryPhoneNo, flxSecondaryPhoneNo, CopyuserSettingsLabel0hd99db2190ef44, flxPrimaryEmail, flxSecondaryEmail, CopyuserSettingsLabel0c94a6316a25d49, flxAddressLine1, flxAddressLine2, flxCity, flxState);
frmUserSettingsMyProfileKA.add(flxHeader, mainContent, flxProfileEdit);
};
function frmUserSettingsMyProfileKAGlobalsAr() {
frmUserSettingsMyProfileKAAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmUserSettingsMyProfileKAAr,
"bounces": false,
"enableScrolling": true,
"enabledForIdleTimeout": true,
"id": "frmUserSettingsMyProfileKA",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"skin": "sknmainGradient",
"verticalScrollIndicator": false
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": true,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_660213d662b74d749bdeee9771831aef,
"retainScrollPosition": false,
"titleBar": true,
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
