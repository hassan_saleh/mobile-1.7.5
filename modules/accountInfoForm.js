// Sets titlebar and form to appropriate color
function accountsInfoPreShow() {
  if (x === "Checking") {
    //frmAccountInfoKA.skin = sknaccountCheckingBkg;
    frmAccountInfoKA.titleBarAccountInfo.skin = sknaccountTypeChecking;
  } else if (x === "Savings") {
    // frmAccountInfoKA.skin = sknaccountSavingsBkg;
    frmAccountInfoKA.titleBarAccountInfo.skin = sknaccountTypeSavings;
  } else if (x === "CreditCard") {
    // frmAccountInfoKA.skin = sknaccountCreditBkg;
    frmAccountInfoKA.titleBarAccountInfo.skin = sknaccountTypeCredit;
  }

  frmAccountInfoKA.successIcon.opacity = 0;
  frmAccountInfoKA.successImage.opacity = 0;
}


function accountsStatementPreShow() {
  if (x === "Checking") {
    frmacntstatementsKA.skin = sknaccountCheckingBkg;
    frmacntstatementsKA.titleBarAccountInfo.skin = sknaccountTypeChecking;
  } else if (x === "Savings") {
    frmacntstatementsKA.skin = sknaccountSavingsBkg;
    frmacntstatementsKA.titleBarAccountInfo.skin = sknaccountTypeSavings;
  } else if (x === "CreditCard") {
    frmacntstatementsKA.skin = sknaccountCreditBkg;
    frmacntstatementsKA.titleBarAccountInfo.skin = sknaccountTypeCredit;
  } else if (x === "Deposit") {
    frmacntstatementsKA.skin = sknaccountDepositBkg;
    frmacntstatementsKA.titleBarAccountInfo.skin = sknaccounttypeDeposit;
  } else if (x == "Mortgage") {
    frmacntstatementsKA.skin = sknaccountMortageBkg;
    frmacntstatementsKA.titleBarAccountInfo.skin = sknaccounttypemortage;
  }

}

function accountsStatementDetailsPreShow() {
  if (x === "Checking") {
    frmacntstatementdetailsKA.skin = sknaccountCheckingBkg;
    frmacntstatementdetailsKA.titleBarAccountInfo.skin = sknaccountTypeChecking;
  } else if (x === "Savings") {
    frmacntstatementdetailsKA.skin = sknaccountSavingsBkg;
    frmacntstatementdetailsKA.titleBarAccountInfo.skin = sknaccountTypeSavings;
  } else if (x === "CreditCard") {
    frmacntstatementdetailsKA.skin = sknaccountCreditBkg;
    frmacntstatementdetailsKA.titleBarAccountInfo.skin = sknaccountTypeCredit;
  }

}

// called onEndEdit frmAccountInfoKA.accountNicknameTextfield
function accountNicknameEntered() {
  frmAccountInfoKA.saveNickname.isVisible = true;
}

function saveAccountNickname() {
  frmAccountInfoKA.saveNickname.isVisible = false;

  frmAccountInfoKA.successIcon.opacity = 1;

  var transformSuccess1 = kony.ui.makeAffineTransform();
  transformSuccess1.scale(0.6, 0.6);
  var transformSuccess2 = kony.ui.makeAffineTransform();
  transformSuccess2.scale(0.75, 0.75);
  var transformSuccess3 = kony.ui.makeAffineTransform();
  transformSuccess3.scale(1.1, 1.1);
  var transformSuccess4 = kony.ui.makeAffineTransform();
  transformSuccess4.scale(1.0, 1.0);

  frmAccountInfoKA.successIcon.animate(
    kony.ui.createAnimation({
      "0": {
        "transform": transformSuccess1,
        "stepConfig": {
          "timingFunction": easeIn
        }
      },
      "15": {
        "transform": transformSuccess2,
        "stepConfig": {
          "timingFunction": easeIn
        }
      },
      "30": {
        "transform": transformSuccess1,
        "stepConfig": {
          "timingFunction": easeIn
        }
      },
      "45": {
        "transform": transformSuccess2,
        "stepConfig": {
          "timingFunction": easeIn
        }
      },
      "60": {
        "transform": transformSuccess1,
        "stepConfig": {
          "timingFunction": easeIn
        }
      },
      "80": {
        "transform": transformSuccess3,
        "stepConfig": {
          "timingFunction": easeIn
        }
      },
      "100": {
        "transform": transformSuccess4,
        "stepConfig": {
          "timingFunction": easeIn
        }
      }
    }), {
      "fillMode": forwards,
      "duration": 4.0,
      "delay": 0
    }, {
      "animationStart": function() {},
      "animationEnd": function() {}
    }
  );

  frmAccountInfoKA.successImage.animate(
    kony.ui.createAnimation({
      "0": {
        "transform": transformSuccess1,
        "opacity": 0,
        "stepConfig": {
          "timingFunction": easeIn
        }
      },
      "60": {
        "transform": transformSuccess3,
        "opacity": 0.8,
        "stepConfig": {
          "timingFunction": easeIn
        }
      },
      "100": {
        "transform": transformSuccess4,
        "opacity": 1,
        "stepConfig": {
          "timingFunction": easeIn
        }
      }
    }), {
      "fillMode": forwards,
      "duration": 1.0,
      "delay": 2.6
    }, {
      "animationEnd": function() {}
    }
  );

}

function accountInfoSorting(widget, sortby, type) {
  try {

    var currentform = kony.application.getCurrentForm();
    currentform.lblNoResult.isVisible = false;
    kony.print("accountInfoSorting :: BEGIN" + widget.src + " " + type + " " + sortby);
    currentform.imgSortDateAsc.src = "map_arrow_straight_nonselected.png";
    currentform.imgSortDateDesc.src = "map_arrow_down_nonselected.png";
    currentform.imgSortAmountAsc.src = "map_arrow_straight_nonselected.png";
    currentform.imgSortAmountDesc.src = "map_arrow_down_nonselected.png";

    if (widget.src == "map_arrow_straight_nonselected.png")
      widget.src = "map_arrow_straight.png";
    else if (widget.src == "map_arrow_straight.png")
      widget.src = "map_arrow_straight_nonselected.png";
    else if (widget.src == "map_arrow_down_nonselected.png")
      widget.src = "map_arrow_down.png";
    else if (widget.src == "map_arrow_down.png")
      widget.src = "map_arrow_down_nonselected.png";

    //if ((currentform.accountDetailsScrollContainer.isVisible == true) && (currentform.accountDetailsScrollContainer.left == "0dp" || currentform.accountDetailsScrollContainer.left == "0%")) {
    if(sortby.trim() == "Date"){
      date_sort(currentform.transactionSegment, sortby, type);
    }else if(sortby.trim() == "Amount"){
      amount_Sort(currentform.transactionSegment, sortby, type);
    }
    //}
    kony.print("accountInfoSorting :: END");
  } catch (e) {
    kony.print("Exception_accountInfoSorting ::" + e);
    exceptionLogCall("accountInfoSorting","UI ERROR","UI",e);
  }
}

function date_sort(segData, sortby, type) {
  try {
    kony.print("Date_sort ::" + segData.data);
    var Data = segData.data;
    var temp = "";
    var value1 = "";
    var value2 = "";

    if (sortby.trim() == "Date") {
      temp = "";
      for (var l = 0; l < Data.length; l++) {
        for (var k = l + 1; k < Data.length; k++) {
          value1 = Data[l].transactionDate.text.substring(3,5)+"/"+Data[l].transactionDate.text.substring(0,2)+"/"+Data[l].transactionDate.text.substring(6,Data[l].transactionDate.text.length);
          value2 = Data[k].transactionDate.text.substring(3,5)+"/"+Data[k].transactionDate.text.substring(0,2)+"/"+Data[k].transactionDate.text.substring(6,Data[k].transactionDate.text.length);
          kony.print("Date ::"+value1+""+value2);
          if ((new Date(value1) > new Date(value2)) && (type == "ascending")) {
            kony.print("inside asc");
            temp = Data[l];
            Data[l] = Data[k];
            Data[k] = temp;
          } else if ((new Date(value1) < new Date(value2)) && (type == "descending")) {
            kony.print("inside desc");
            temp = Data[l];
            Data[l] = Data[k];
            Data[k] = temp;
          }
        }
      }
    }
    kony.print("Sorting Result " + JSON.stringify(Data));
    kony.print("length of the data ::" + Data.length);
    if (Data.length <= 1) {
      kony.application.getCurrentForm().lblExport.setEnabled(false);
      kony.application.getCurrentForm().CopylblFilter0f7770a92cff247.setEnabled(true);
    } else {
      kony.application.getCurrentForm().lblExport.setEnabled(true);
      kony.application.getCurrentForm().CopylblFilter0f7770a92cff247.setEnabled(true);
    }
    segData.removeAll();
    segData.setData(Data);
    animatePopup_Sort();
  } catch (e) {
    kony.print("Exception_date_sort ::" + e);
    exceptionLogCall("date_sort","UI ERROR","UI",e);
  }
}

function amount_Sort(segData, sortby, type){
  try{
    kony.print("amountDate_sort ::" + segData.data);
    var Data = segData.data;
    var temp = "";
    var value1 = "";
    var value2 = "";

    if (sortby.trim() == "Amount") {
      temp = "";
      for (var i = 0; i < Data.length; i++) {
        for (var j = i + 1; j < Data.length; j++) {
          if(Data[i].amount.text.match('\[\+\-]') !== null)
            value1 = Data[i].amount.text.substring(0,1)+""+Data[i].amount.text.substring(2,(Data[i].amount.text.length)-4);
          else
            value1 = Data[i].amount.text.substring(0,(Data[i].amount.text.length)-4);
          value1 = parseFloat(value1.replace(/,/g,""));
          if(Data[j].amount.text.match('\[\+\-]') !== null)
            value2 = Data[j].amount.text.substring(0,1)+""+Data[j].amount.text.substring(2,(Data[j].amount.text.length)-4);
          else
            value2 = Data[j].amount.text.substring(0,(Data[j].amount.text.length)-4);
          value2 = parseFloat(value2.replace(/,/g,""));
          if (((value1) > (value2)) && (type == "ascending")) {
            temp = Data[i];
            Data[i] = Data[j];
            Data[j] = temp;
          } else if (((value1) < (value2)) && (type == "descending")) {
            temp = Data[i];
            Data[i] = Data[j];
            Data[j] = temp;
          }
        }
      }
    }
    kony.print("Sorting Result " + JSON.stringify(Data));
    kony.print("length of the data ::" + Data.length);
    if (Data.length <= 1) {
      kony.application.getCurrentForm().lblExport.setEnabled(false);
      kony.application.getCurrentForm().CopylblFilter0f7770a92cff247.setEnabled(true);
    } else {
      kony.application.getCurrentForm().lblExport.setEnabled(true);
      kony.application.getCurrentForm().CopylblFilter0f7770a92cff247.setEnabled(true);
    }
    segData.removeAll();
    segData.setData(Data);
    animatePopup_Sort();
  }catch(e){
    kony.print("Exception_amount_Sort ::"+e);
    exceptionLogCall("amount_Sort","UI ERROR","UI",e);
    animatePopup_Sort();
  }
}
function searchTransation() {
  try {
    var isValid = false;
    var mode = "",data = "";
    filter_OPTIONS = {
      "transactionAmount": {
        "min": null,
        "max": null
      },
      "days": null,
      "Mode": {
        "credit": null,
        "debit": null,
        "card": null,
        "all": null
      },
      "checknumber": {
        "from": null,
        "to": null
      },
      "other": {
        "from": null,
        "to": null
      },
      "year": null,
      "transactionName": null
    };
    var currentForm = kony.application.getCurrentForm();
    if (currentForm.id == "frmFilterTransaction") {
      kony.application.getPreviousForm().txtSearch.text = "";
      frmFilterTransaction.flxFilterDone.setEnabled(false);
      if (frmFilterTransaction.txtAmountRangeFrom.text !== "" && frmFilterTransaction.txtAmountRangeFrom.text !== null) {
        if ((frmFilterTransaction.txtAmountRangeTo.text === "" || frmFilterTransaction.txtAmountRangeTo.text === null) ||
            frmFilterTransaction.txtAmountRangeTo.text === "0.000" || frmFilterTransaction.txtAmountRangeTo.text === "0.00" &&
            frmFilterTransaction.txtAmountRangeTo.text === 0.000 || frmFilterTransaction.txtAmountRangeTo.text === 0.00) {
          toastMsg.showToastMsg("Check Amount Fields", 5000);
          frmFilterTransaction.flxFilterDone.setEnabled(true);
          return;
        } else if (frmFilterTransaction.txtAmountRangeTo.text !== "" && frmFilterTransaction.txtAmountRangeTo.text !== null) {
          filter_OPTIONS.transactionAmount = {
            "min": frmFilterTransaction.txtAmountRangeFrom.text || null,
            "max": frmFilterTransaction.txtAmountRangeTo.text || null
          };
          if(parseFloat(filter_OPTIONS.transactionAmount.min) > parseFloat(filter_OPTIONS.transactionAmount.max)){
            customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.filter.amountvalidation"), popupCommonAlertDimiss, null, "", "");
            frmFilterTransaction.flxFilterDone.setEnabled(true);
            return;
          }
          isValid = true;
        }
      }
      if ((frmFilterTransaction.lblIncommingTick.isVisible === true) && (frmFilterTransaction.lblOutGoingTick.isVisible === true)){
        filter_OPTIONS.Mode.all = "all";
        isValid = true;
      }else {
        if (frmFilterTransaction.lblIncommingTick.isVisible === true){
          filter_OPTIONS.Mode.credit = "credit";
          isValid = true;
        }else if (frmFilterTransaction.lblOutGoingTick.isVisible === true){
          filter_OPTIONS.Mode.debit = "debit";
          isValid = true;
        }else if (frmFilterTransaction.lblCardTick.isVisible === true){
          filter_OPTIONS.Mode.card = "Card";
          isValid = true;
        }
      }
      if ((frmFilterTransaction.lblDateRange.text != "Pick a date") && (frmFilterTransaction.lblDateRange.text != "")) {
        var year = new Date().getFullYear();
        var month = (new Date().getMonth() + 1) + "";
        var date = new Date();
        var fromDATE = "";
        var toDATE = date.getFullYear()+"-"+((date.getMonth()+1)>9?(date.getMonth()+1):"0"+(date.getMonth()+1))+"-"+(date.getDate()>9?date.getDate():"0"+date.getDate());
        if (frmFilterTransaction.lblDateRange.text == geti18Value("i18n.FilterTransaction.last30days")) {
          filter_OPTIONS.days = 30;
          date.setDate(date.getDate()-30);
          fromDATE = date.getFullYear()+"-"+((date.getMonth()+1)>9?(date.getMonth()+1):"0"+(date.getMonth()+1))+"-"+(date.getDate()>9?date.getDate():"0"+date.getDate());
          if(kony.application.getPreviousForm().id === "frmAccountDetailKA")
            fetch_TransactionList(kony.retailBanking.globalData.accountsDashboardData.accountsData[parseInt(customerAccountDetails.currentIndex)],
                                  {"toDATE":toDATE,"fromDATE":fromDATE});
          else if(kony.application.getPreviousForm().id === "frmManageCardsKA"){
            filter_OPTIONS.other.from = fromDATE;
            filter_OPTIONS.other.to = toDATE;
            get_cardDataList();
          }
          isValid = false;
          return;
        } else if (frmFilterTransaction.lblDateRange.text == geti18Value("i18n.FilterTransaction.last60days")) {
          filter_OPTIONS.days = 60;
          date.setDate(date.getDate()-60);
          fromDATE = date.getFullYear()+"-"+((date.getMonth()+1)>9?(date.getMonth()+1):"0"+(date.getMonth()+1))+"-"+(date.getDate()>9?date.getDate():"0"+date.getDate());
          if(kony.application.getPreviousForm().id === "frmAccountDetailKA")
            fetch_TransactionList(kony.retailBanking.globalData.accountsDashboardData.accountsData[parseInt(customerAccountDetails.currentIndex)],
                                  {"toDATE":toDATE,"fromDATE":fromDATE});
          else if(kony.application.getPreviousForm().id === "frmManageCardsKA"){
            filter_OPTIONS.other.from = fromDATE;
            filter_OPTIONS.other.to = toDATE;
            get_cardDataList();
          }
          isValid = false;
          return;
        } else if (frmFilterTransaction.lblDateRange.text == geti18Value("i18n.FilterTransaction.last90days")) {
          filter_OPTIONS.days = 90;
          date.setDate(date.getDate()-90);
          fromDATE = date.getFullYear()+"-"+((date.getMonth()+1)>9?(date.getMonth()+1):"0"+(date.getMonth()+1))+"-"+(date.getDate()>9?date.getDate():"0"+date.getDate());
          if(kony.application.getPreviousForm().id === "frmAccountDetailKA")
            fetch_TransactionList(kony.retailBanking.globalData.accountsDashboardData.accountsData[parseInt(customerAccountDetails.currentIndex)],
                                  {"toDATE":toDATE,"fromDATE":fromDATE});
          else if(kony.application.getPreviousForm().id === "frmManageCardsKA"){
            filter_OPTIONS.other.from = fromDATE;
            filter_OPTIONS.other.to = toDATE;
            get_cardDataList();
          }
          isValid = false;
          return;
        } else if (frmFilterTransaction.lblDateRange.text == list_Month[month]) {
          filter_OPTIONS.days = "thismonth";
          isValid = true;
        } else if (frmFilterTransaction.lblDateRange.text == geti18Value("i18n.filtertransaction.year") + " " + year) {
          filter_OPTIONS.year = year + "";
          isValid = true;
        } else {//option.other.from //option.other.to
          var frmDATE = frmFilterTransaction.lblDateRange.text.substring(0, frmFilterTransaction.lblDateRange.text.indexOf("-")).split("/");
          var tDATE = frmFilterTransaction.lblDateRange.text.substring(frmFilterTransaction.lblDateRange.text.indexOf("-") + 1, frmFilterTransaction.lblDateRange.text.length).split("/");
          fromDATE = frmDATE[2]+"-"+(parseInt(frmDATE[1])>9?frmDATE[1]:"0"+frmDATE[1])+"-"+(parseInt(frmDATE[0])>9?frmDATE[0]:"0"+frmDATE[0]);
          toDATE = tDATE[2]+"-"+(parseInt(tDATE[1])>9?tDATE[1]:"0"+tDATE[1])+"-"+(parseInt(tDATE[0])>9?tDATE[0]:"0"+tDATE[0]);
          kony.print("formDATE ::"+fromDATE+" :: toDATE ::"+toDATE);
          if(kony.application.getPreviousForm().id === "frmAccountDetailKA")
            fetch_TRANSACTIONLIST(kony.retailBanking.globalData.accountsDashboardData.accountsData[parseInt(customerAccountDetails.currentIndex)],
                                  {"toDATE":toDATE,"fromDATE":fromDATE});
          else if(kony.application.getPreviousForm().id === "frmManageCardsKA"){
            filter_OPTIONS.other.from = fromDATE;
            filter_OPTIONS.other.to = toDATE;
            get_cardDataList();
          }
          isValid = false;
          return;
        }
      }
      if(data === ""){
        data = kony.retailBanking.globalData.accountsTransactionList.transaction;//kony.application.getPreviousForm().transactionSegment.data;
      }
      if (isValid) {
        filterAccountTransactions(filter_OPTIONS, kony.application.getPreviousForm().transactionSegment, data, kony.application.getPreviousForm());
      }else if(kony.application.getPreviousForm().id === "frmAccountDetailKA"){
        fetch_TransactionList(kony.retailBanking.globalData.accountsDashboardData.accountsData[parseInt(customerAccountDetails.currentIndex)],
                              {"toDATE":"","fromDATE":""});
        frmFilterTransaction.flxFilterDone.setEnabled(true);
        return;
      }else if(kony.application.getPreviousForm().id === "frmManageCardsKA"){
        filter_OPTIONS.other.from = null;
        filter_OPTIONS.other.to = null;
        get_cardDataList();
        frmFilterTransaction.flxFilterDone.setEnabled(true);
        return;
      }
      isValid = false;
      frmFilterTransaction.flxFilterDone.setEnabled(true);
      kony.application.getPreviousForm().show();
    } else if(currentForm.txtSearch.text !== null && currentForm.txtSearch.text !== ""){
      filter_OPTIONS.transactionName = currentForm.txtSearch.text.trim();
      filterAccountTransactions(filter_OPTIONS, currentForm.transactionSegment, currentForm.transactionSegment.data, currentForm);
    }else{
      currentForm.lblNoResult.isVisible = false;
      frmAccountDetailKA.LabelNoRecordsKA.setVisibility(false);
      currentForm.transactionSegment.isVisible = true;
      currentForm.lblExport.setEnabled(true);
      currentForm.CopylblFilter0f7770a92cff247.setEnabled(true);
      if(currentForm.id === "frmAccountDetailKA" || currentForm.id === "frmManageCardsKA")
        data = kony.retailBanking.globalData.accountsTransactionList.transaction;
      currentForm.transactionSegment.removeAll();
      customerAccountDetails.indicatorIndex = 0;
      customerAccountDetails.cardTransactionIndex = 0;
      if(data !== "" && data !== null)
        currentForm.transactionSegment.setData(data);
      frmFilterTransaction.flxFilterDone.setEnabled(true);
      currentForm.show();
    }
  } catch (e) {
    kony.print("Exception_searchTransation ::" + e);

  }

}

var no = true;

function filterAccountTransactions(field, segment, segData, form) {
  try {
    kony.print("filterAccountTransactions ::" + JSON.stringify(segData));
    var result = [];
    var temp = segData;
    if ((field.transactionName !== undefined) && (field.transactionName !== null)) {
      var data = "";
      for (var i = 0; i < temp.length; i++) {
        if(temp[i].description.text !== undefined){
          data = temp[i].description.text.toLowerCase();
          kony.print("data ::"+data+" :: transactionName :: "+field.transactionName.toLowerCase()+" :: Index :: "+data.indexOf(field.transactionName));
          if (data.indexOf(field.transactionName.toLowerCase()) > -1) {
            result.push(temp[i]);
          }
        }
      }
      if(result.length > 0)
        result.push({"lblLastTransaction":{"text":"End of search results.","isVisible":true},
                     "description":{"text":"","isVisible":false},
                     "amount":{"text":"","isVisible":false},
                     "category":"",
                     "transactionDate":{"text":"","isVisible":false}});
    }
    if (((field.transactionAmount.min !== undefined) && (field.transactionAmount.min !== null) && ((field.transactionAmount.min !== "0.000") || (field.transactionAmount.min !== "0.00")))) {
      var temp = result.length > 0 ? result : segData;
      result = [];
      var min = "";
      var max = "";
      var value = "";
      for (var i = 0; i < temp.length; i++) {
        min = (parseFloat(field.transactionAmount.min).toFixed(3))*1;
        max = (parseFloat(field.transactionAmount.max).toFixed(3))*1;
        if(temp[i].amount.text.match('\[\+\-]') !== null)
          value = temp[i].amount.text.substring(2,(temp[i].amount.text.length)-4);
        else
          value = temp[i].amount.text.substring(0,(temp[i].amount.text.length)-4);
        value = (parseFloat(value.replace(/,/g,"")).toFixed(3))*1;
        kony.print("Value "+value+" min "+min+" max "+max);
        kony.print("min <= value"+(min <= value)+"value <= max"+(value <= max));
        if (min <= value) {
          if (value <= max) {
            result.push(temp[i]);
          }
        }
      }
    }

    if ((field.other.from != null) && (field.other.to != undefined)) {
      kony.print("field.other");
      var temp = result.length > 0 ? result : segData;
      result = [];
      var value1 = "";
      for (var i = 0; i < temp.length; i++) {
        value1 = temp[i].transactionDate.text.substring(3,5)+"/"+temp[i].transactionDate.text.substring(0,2)+"/"+temp[i].transactionDate.text.substring(6,temp[i].transactionDate.text.length);
        if ((new Date(field.other.from) <= new Date(value1)) && (new Date(value1) <= new Date(field.other.to))) {
          result.push(temp[i]);
        }
      }
    }

    if ((field.year != null) && (field.year != undefined)) {
      kony.print("field.year");
      var temp = result.length > 0 ? result : segData;
      result = [];
      for (var i = 0; i < temp.length; i++) {
        if (field.year == temp[i].transactionDate.text.substring(6, (temp[i].transactionDate.text.length))) {
          result.push(temp[i]);
        }
      }
    }

    if ((field.days != null) && (field.days != undefined)) {
      kony.print("field.days");
      var temp = result.length > 0 ? result : segData;
      result = [];
      var dateValue = "";
      var today = new Date();
      if (field.days == "thismonth") {
        for (var i = 0; i < temp.length; i++) {
          kony.print("thismonth ::" + today.getMonth() + 1 + " " + parseInt(temp[i].date.text.substring(0, 2)));
          if (today.getMonth() + 1 == parseInt(temp[i].date.text.substring(0, 2))) {
            result.push(temp[i]);
          }
        }
      } else if (field.days == "lastmonth") {
        today.setMonth(today.getMonth() - 1);
        for (var i = 0; i < temp.length; i++) {
          if (parseInt(today.getMonth() + 1) == parseInt(temp[i].date.text.substring(0, 2))) {
            result.push(temp[i]);
          }
        }
      } else {
        today.setDate(today.getDate() - parseInt(field.days));
        for (var i = 0; i < temp.length; i++) {
          dateValue = temp[i].transactionDate.text.substring(3,5)+"/"+temp[i].transactionDate.text.substring(0,2)+"/"+temp[i].transactionDate.text.substring(6,temp[i].transactionDate.text.length)
          kony.print("temp ::" + dateValue + "" + today);
          if (new Date(dateValue) >= today) {
            result.push(temp[i]);
          }
        }
      }
    }

    if ((field.Mode.all != null) || (field.Mode.credit != null) || (field.Mode.debit != null) || (field.Mode.card != null)) {
      kony.print("field.Mode");
      var temp = result.length > 0 ? result : segData;
      result = [];

      for (var i = 0; i < temp.length; i++) {
        kony.print("temp[i] ::"+temp[i]);
        kony.print("temp[i].category == field.Mode.credit ::"+temp[i].category == field.Mode.credit);
        if(temp[i].category === "credit" || temp[i].category === "debit"){
          if(field.Mode.credit !== null && (field.Mode.credit === "credit")){
            if(temp[i].amount.text.indexOf("+") > -1){
              result.push(temp[i]);
            }
          }else if(field.Mode.debit !== null && (field.Mode.debit === "debit")){
            if(temp[i].amount.text.indexOf("-") > -1){
              result.push(temp[i]);
            }
          }else if(field.Mode.all !== null && (field.Mode.all === "all")){
            result.push(temp[i]);
          }
        }else if ((field.Mode.credit != null) && (temp[i].category == field.Mode.credit)) {
          result.push(temp[i]);
        } else if ((field.Mode.debit != null) && (temp[i].category == field.Mode.debit)) {
          result.push(temp[i]);
        } else if ((field.Mode.card != null) && (temp[i].category == field.Mode.card)) {
          result.push(temp[i]);
        } else if ((field.Mode.all != null) && (field.Mode.all == "all")) {
          result.push(temp[i]);
        }
      }
    }
    // 		if(((field.checknumber.from != null) && (field.checknumber.from != undefined)) && ((field.checknumber.to != null) && (field.checknumber.to != undefined))){
    // 			var temp = result.length > 0? result:segData;
    // 			result = [];
    // 			for(var i=0; i<temp.length; i++){
    // 				if((temp[i].checknumber.from == field.checknumber.from) || ((temp[i].checknumber.to == field.checknumber.to))){
    // 					result.push(temp[i]);
    // 				}
    // 			}
    // 		}
    if (result.length <= 1) {
      form.lblExport.setEnabled(false);
      form.CopylblFilter0f7770a92cff247.setEnabled(true);
    } else {
      form.lblExport.setEnabled(true);
      form.CopylblFilter0f7770a92cff247.setEnabled(true);
    }
    if(form.id !== "frmManageCardsKA")
      form.LabelNoRecordsKA.isVisible = false;
    else
      form.lblNoResult.isVisible = false;
    kony.print("result ::" + JSON.stringify(result));
    if (result.length > 0) {
      form.transactionSegment.removeAll();
      form.lblExport.setEnabled(true);
      form.transactionSegment.isVisible = true;
      form.transactionSegment.setData(result);
      frmFilterTransaction.flxFilterDone.setEnabled(true);
      return result;
    } else {
      if(form.id !== "frmManageCardsKA"){
        form.LabelNoRecordsKA.text = kony.i18n.getLocalizedString("i18n.search.Noresultsfound");
        form.LabelNoRecordsKA.isVisible = true;
      }else{
        form.lblNoResult.text = kony.i18n.getLocalizedString("i18n.search.Noresultsfound");
        form.lblNoResult.isVisible = true;
      }
      /*if((field.transactionName != null) && (field.transactionName != undefined)){
        		form.LabelNoRecordsKA.text = kony.i18n.getLocalizedString("i18n.search.noSearchResults");
            }*/
      //             form.LabelNoRecordsKA.isVisible = true;
      //           	form.transactionSegment.removeAll();
      form.transactionSegment.isVisible = false; 
      frmFilterTransaction.flxFilterDone.setEnabled(true);
      return [];
    }
  } catch (e) {
    kony.print("Exception at filterAccountTransactions::" + e);
  }
}

function calendar_DateRange(field, target, flow) {
  try {
    kony.print("calendar_DateRange ::"+field);
    if(flow == "Cards"){
      if (target == "From") {
        frmFilterTransaction.lblDateFromCards.text = field.dateComponents[0] + "/" + field.dateComponents[1] + "/" + field.dateComponents[2];
      } else if (target == "To") {
        frmFilterTransaction.lblDateToCards.text = field.dateComponents[0] + "/" + field.dateComponents[1] + "/" + field.dateComponents[2];
      }
    }else{
      if (target == "From") {
        frmFilterTransaction.lblDateFrom.text = field.dateComponents[0] + "/" + field.dateComponents[1] + "/" + field.dateComponents[2];
        frmFilterTransaction.calDateTo.validStartDate = [field.dateComponents[0],field.dateComponents[1],field.dateComponents[2]];
        var date = new Date(field.dateComponents[1]+"/"+field.dateComponents[0]+"/"+field.dateComponents[2]);
        var curDate = new Date();
        date.setDate(date.getDate()+60);
        kony.print("date comparission ::"+(date>curDate));
        if(date > curDate)
          frmFilterTransaction.calDateTo.validEndDate = [curDate.getDate(),curDate.getMonth()+1,curDate.getFullYear()];
        else
          frmFilterTransaction.calDateTo.validEndDate = [date.getDate(),date.getMonth()+1,date.getFullYear()];
      } else if (target == "To") {
        frmFilterTransaction.lblDateTo.text = field.dateComponents[0] + "/" + field.dateComponents[1] + "/" + field.dateComponents[2];
      }
      if ((frmFilterTransaction.lblDateFrom.text != null) && (frmFilterTransaction.lblDateFrom.text != "")) {
        if ((frmFilterTransaction.lblDateTo.text != null) && (frmFilterTransaction.lblDateTo.text != "")) {
          frmFilterTransaction.lblDateRange.text = frmFilterTransaction.lblDateFrom.text + "-" + frmFilterTransaction.lblDateTo.text;
          frmFilterTransaction.lblLast30days.isVisible = false;
          //                 frmFilterTransaction.lblThisMonth.isVisible = false;
          frmFilterTransaction.lblLast90days.isVisible = false;
          frmFilterTransaction.lblCurrentYear.isVisible = false;
        }
      }
    }
  } catch (e) {
    kony.print("Exception_calendar_DateRange ::" + e);
  }
}

function setTimePeriod(source, value) {
  try {
    frmFilterTransaction.lblLast30days.isVisible = false;
    //         frmFilterTransaction.lblThisMonth.isVisible = false;
    frmFilterTransaction.lblLast90days.isVisible = false;
    frmFilterTransaction.lblCurrentYear.isVisible = false;
    frmFilterTransaction.lblOthersDate.isVisible = false;
    if (source.isVisible == false) {
      source.isVisible = true;
      if(value == kony.i18n.getLocalizedString("i18n.FilterTransaction.Others")){
        frmFilterTransaction.lblSetCustomDateRange.isVisible = true;	
        frmFilterTransaction.CopyFlexContainer0f90c1916338042.isVisible = true;
      }else{
        frmFilterTransaction.lblDateRange.text = value;
        frmFilterTransaction.CopyFlexContainer0f90c1916338042.isVisible = false;
        frmFilterTransaction.lblSetCustomDateRange.isVisible = false;
      }
      frmFilterTransaction.lblDateFrom.text = "";
      frmFilterTransaction.lblDateTo.text = "";
    } else {
      source.isVisible = false;
      frmFilterTransaction.lblDateRange.text = kony.i18n.getLocalizedString("i18n.FilterTransaction.pickadate"); //"Pick a date"
    }
  } catch (e) {
    kony.print("Exception_setTimePeriod ::"+e);
  }
}

function create_PageIndicator(form, centerX, totalAcc) {
  kony.print("create_PageIndicator ::" + centerX + " " + totalAcc);
  try {
    form.flxPageIndicator.removeAll();
    if(totalAcc > 1){
      centerX = Math.round((centerX*2)/totalAcc)-10;
      // centerX = (8*(totalAcc)/2)+((13*(totalAcc-1))/2);
      //kony.print("CenterX before ::"+centerX);
      //centerX = (50-centerX)+4;
      kony.print("create_PageIndicator ::left " + centerX);
      form.flxPageIndicator.padding = [0, 0, 0, 0];
      for (var i = 0; i < totalAcc; i++) {
        //#ifdef android
        centerX = centerX + 13;
        //#endif
        //#ifdef iphone
        centerX = centerX + 18;
        //#endif

        kony.print("create_PageIndicator ::left " + centerX);
        var flex = new kony.ui.FlexContainer({
          "autogrowMode": kony.flex.AUTOGROW_NONE,
          "centerX": centerX+"%",
          "centerY": "50%",
          "clipBounds": true,
          "height": "20%",
          "id": "flxPageIndicator" + (i),
          "isVisible": true,
          "layoutType": kony.flex.FREE_FORM,
          "skin": "sknPageIndicatorFlex",
          "width": "8%",
          "zIndex": 1
        }, {}, {});
        form.flxPageIndicator.add(flex);
      }
    }
    //     	form["flxPageIndicator"+customerAccountDetails.currentIndex].setFocus();
    form["flxPageIndicator"+customerAccountDetails.currentIndex].skin = "sknPageIndicatorFlexnoOpc";
  } catch (e) {
    kony.print("Exception in create_PageIndicator ::" + e);
  }
}

var list_Month = {
  "1": "January",
  "2": "February",
  "3": "March",
  "4": "April",
  "5": "May",
  "6": "June",
  "7": "July",
  "8": "August",
  "9": "September",
  "10": "October",
  "11": "November",
  "12": "December"
};
var clearForm = {
  filterTransactions: function() {
    frmFilterTransaction.lblLast30days.isVisible = false;
    frmFilterTransaction.lblThisMonth.isVisible = false;
    frmFilterTransaction.lblLast90days.isVisible = false;
    frmFilterTransaction.lblCurrentYear.isVisible = false;
    frmFilterTransaction.lblOthersDate.isVisible = false;
    frmFilterTransaction.CopyFlexContainer0f90c1916338042.isVisible = false;
    frmFilterTransaction.lblSetCustomDateRange.isVisible = false;
    frmFilterTransaction.lblDateFrom.text = "";
    frmFilterTransaction.lblDateTo.text = "";
    frmFilterTransaction.lblDateRange.text = kony.i18n.getLocalizedString("i18n.FilterTransaction.pickadate");
    if (frmFilterTransaction.flxFilterTransaction.isVisible === true) {
      frmFilterTransaction.txtAmountRangeFrom.text = "";
      frmFilterTransaction.txtAmountRangeTo.text = "";
      frmFilterTransaction.lblIncommingTick.isVisible = false;
      frmFilterTransaction.lblOutGoingTick.isVisible = false;
      frmFilterTransaction.lblCardTick.isVisible = false;
    }
  }
};
var customerAccountDetails = {
  "currentIndex": null,
  "data": null,
  "length": null,
  "indicatorIndex": null,
  "isTransactionEmpty":false,
  "unClearedTransaction":false,
  "cardTransactionIndex":0,
  "profileDetails":null
};

function gesture_AccountAnimation() {
  try {
    kony.print("gesture_AccountAnimation BEGIN");
    var setupTblTap = {
      fingers: 1,
      swipedistance: 60,
      swipevelocity: 60
    };
    frmAccountDetailKA.accountBalanceOverview.setGestureRecognizer(2, setupTblTap, animation_AccountTransaction);
    frmAccountDetailKA.accountDetailsTransactions.setGestureRecognizer(2, setupTblTap, animation_AccountTransaction);
    frmAccountDetailKA.flxAccountdetailsSortContainer.setGestureRecognizer(1, {fingers:1,taps:1}, animatePopup_Sort);

    function animation_AccountTransaction(myWidget, gestureInfo) {
      kony.print("animation_AccountTransaction ::" + JSON.stringify(gestureInfo));
      try {
        if(frmAccountDetailKA.txtSearch.text == "" || frmAccountDetailKA.txtSearch.text == null)
          frmAccountDetailKA.txtSearch.text = "";
        if (gestureInfo.swipeDirection == 1) {
          kony.print("gestureInfo.swipeDirection ::" + gestureInfo.swipeDirection);
          kony.print("(parseInt(customerAccountDetails.currentIndex)+1) < (parseInt(customerAccountDetails.length)) ::" + customerAccountDetails.currentIndex + " " + customerAccountDetails.length);
          if ((parseInt(customerAccountDetails.currentIndex) + 1) < (parseInt(customerAccountDetails.length))) {
            customerAccountDetails.currentIndex = parseInt(customerAccountDetails.currentIndex) + 1;
          } else if ((parseInt(customerAccountDetails.currentIndex) + 1) == (parseInt(customerAccountDetails.length))) {
            return;
          }
          var indicator1 = "";
          var indicator2 = "";
          var indicator3 = ""
          kony.print("customerAccountDetails.currentIndex ::" + customerAccountDetails.currentIndex);
          if ((parseInt(customerAccountDetails.currentIndex)+1) < (parseInt(customerAccountDetails.length))) {
            kony.print("inside if");
            indicator1 = "flxPageIndicator" + (parseInt(customerAccountDetails.currentIndex)).toString();
            indicator2 = "flxPageIndicator" + (parseInt(customerAccountDetails.currentIndex) + 1).toString();
            indicator3 = "flxPageIndicator" + (parseInt(customerAccountDetails.currentIndex) - 1).toString();
            //                         customerAccountDetails.indicatorIndex = parseInt(customerAccountDetails.indicatorIndex) + 1;
          } else if ((parseInt(customerAccountDetails.currentIndex)+1) == (parseInt(customerAccountDetails.length))) {
            kony.print("inside else");
            //                         customerAccountDetails.indicatorIndex = 1;
            indicator1 = "flxPageIndicator" + (parseInt(customerAccountDetails.length)-1).toString();
            indicator2 = "flxPageIndicator0";
            indicator3 = "flxPageIndicator"+(parseInt(customerAccountDetails.length)-2).toString();;
          }
          kony.print("page Indicator ::"+indicator1+" "+indicator2+" "+indicator3);
          frmAccountDetailKA[indicator1].skin = "sknPageIndicatorFlexnoOpc";
          frmAccountDetailKA[indicator2].skin = "sknPageIndicatorFlex";
          frmAccountDetailKA[indicator3].skin = "sknPageIndicatorFlex";
          frmAccountDetailKA[indicator1].setFocus();
          if (frmAccountDetailKA.flxAccountDetailsSecondary.left == "100%" && frmAccountDetailKA.flxAccountDetailsSecondary.isVisible == false) {
            var AccName = customerAccountDetails.data[parseInt(customerAccountDetails.currentIndex)][0].nickName.text.toString();
            var accountName = AccName.substring(AccName.lastIndexOf("<label>")+1,AccName.lastIndexOf("</label>"))
            frmAccountDetailKA.lblavailbalanceSecondary.text = accountName.substring(6,accountName.length);
            frmAccountDetailKA.availableBalanceAmountSecondary.text = customerAccountDetails.data[parseInt(customerAccountDetails.currentIndex)][0].availableBalance; //+ " " + customerAccountDetails.data[parseInt(customerAccountDetails.currentIndex)][0].currencyCode;
            frmAccountDetailKA.flxAccountDetailsSecondary.isVisible = true;
            frmAccountDetailKA.flxAccountDetailsSecondary.left = "0%";
            frmAccountDetailKA.FlexContainer04dfa3182c5f34e.isVisible = false;
            frmAccountDetailKA.FlexContainer04dfa3182c5f34e.left = "100%";
          } else if (frmAccountDetailKA.FlexContainer04dfa3182c5f34e.left == "100%" && frmAccountDetailKA.FlexContainer04dfa3182c5f34e.isVisible == false) {
            frmAccountDetailKA.flxAccountDetailsSecondary.isVisible = false;
            frmAccountDetailKA.flxAccountDetailsSecondary.left = "100%";
            frmAccountDetailKA.FlexContainer04dfa3182c5f34e.isVisible = true;
            frmAccountDetailKA.FlexContainer04dfa3182c5f34e.left = "0%";
            var AccName = customerAccountDetails.data[parseInt(customerAccountDetails.currentIndex)][0].nickName.text.toString();
            var accountName = AccName.substring(AccName.lastIndexOf("<label>")+1,AccName.lastIndexOf("</label>"))
            frmAccountDetailKA.lblavailbalance.text = accountName.substring(6,accountName.length);
            frmAccountDetailKA.availableBalanceAmount.text = customerAccountDetails.data[parseInt(customerAccountDetails.currentIndex)][0].availableBalance; //+ " " + customerAccountDetails.data[parseInt(customerAccountDetails.currentIndex)][0].currencyCode;
          }
          clearForm.filterTransactions();
          fetch_TransactionList(kony.retailBanking.globalData.accountsDashboardData.accountsData[parseInt(customerAccountDetails.currentIndex)],{"toDATE":"","fromDATE":""});
        } else if (gestureInfo.swipeDirection == 2) {
          kony.print("gestureInfo.swipeDirection ::" + gestureInfo.swipeDirection);
          kony.print("(parseInt(customerAccountDetails.currentIndex)+1) < (parseInt(customerAccountDetails.length)) ::" + customerAccountDetails.currentIndex + " " + customerAccountDetails.length);
          if ((parseInt(customerAccountDetails.currentIndex) == 0)) {
            return;
          } else {
            customerAccountDetails.currentIndex = parseInt(customerAccountDetails.currentIndex) - 1;
          }
          var indicator1 = "";
          var indicator2 = "";
          var indicator3 = "";
          if (parseInt(customerAccountDetails.currentIndex) == 0) {
            kony.print("inside if");
            indicator1 = "flxPageIndicator0";
            indicator2 = "flxPageIndicator" + (parseInt(customerAccountDetails.length)-1).toString();
            indicator3 = "flxPageIndicator1";
            //                         customerAccountDetails.indicatorIndex = parseInt(customerAccountDetails.length);
          } else {
            indicator1 = "flxPageIndicator" + (parseInt(customerAccountDetails.currentIndex)).toString();
            indicator2 = "flxPageIndicator" + (parseInt(customerAccountDetails.currentIndex) - 1).toString();
            indicator3 = "flxPageIndicator" + (parseInt(customerAccountDetails.currentIndex)+1).toString();
            //                         customerAccountDetails.indicatorIndex = parseInt(customerAccountDetails.indicatorIndex) - 1;
          }
          kony.print("page Indicator ::"+indicator1+" "+indicator2+" "+indicator3);
          frmAccountDetailKA[indicator1].skin = "sknPageIndicatorFlexnoOpc";
          frmAccountDetailKA[indicator2].skin = "sknPageIndicatorFlex";
          frmAccountDetailKA[indicator3].skin = "sknPageIndicatorFlex";
          frmAccountDetailKA[indicator1].setFocus();
          kony.print("customerAccountDetails.currentIndex ::" + customerAccountDetails.currentIndex);
          kony.print("customerAccountDetails.data ::" + customerAccountDetails.data[parseInt(customerAccountDetails.currentIndex)]);
          if (frmAccountDetailKA.flxAccountDetailsSecondary.left == "100%" && frmAccountDetailKA.flxAccountDetailsSecondary.isVisible == false) {
            var AccName = customerAccountDetails.data[parseInt(customerAccountDetails.currentIndex)][0].nickName.text.toString();
            var accountName = AccName.substring(AccName.lastIndexOf("<label>")+1,AccName.lastIndexOf("</label>"))
            frmAccountDetailKA.lblavailbalanceSecondary.text = accountName.substring(6,accountName.length);
            frmAccountDetailKA.availableBalanceAmountSecondary.text = customerAccountDetails.data[parseInt(customerAccountDetails.currentIndex)][0].availableBalance; // + " " + customerAccountDetails.data[parseInt(customerAccountDetails.currentIndex)][0].currencyCode;
            frmAccountDetailKA.flxAccountDetailsSecondary.isVisible = true;
            frmAccountDetailKA.flxAccountDetailsSecondary.left = "0%";
            frmAccountDetailKA.FlexContainer04dfa3182c5f34e.isVisible = false;
            frmAccountDetailKA.FlexContainer04dfa3182c5f34e.left = "100%";
          } else if (frmAccountDetailKA.FlexContainer04dfa3182c5f34e.left == "100%" && frmAccountDetailKA.FlexContainer04dfa3182c5f34e.isVisible == false) {
            frmAccountDetailKA.flxAccountDetailsSecondary.isVisible = false;
            frmAccountDetailKA.flxAccountDetailsSecondary.left = "100%";
            frmAccountDetailKA.FlexContainer04dfa3182c5f34e.isVisible = true;
            frmAccountDetailKA.FlexContainer04dfa3182c5f34e.left = "0%";
            var AccName = customerAccountDetails.data[parseInt(customerAccountDetails.currentIndex)][0].nickName.text.toString();
            var accountName = AccName.substring(AccName.lastIndexOf("<label>")+1,AccName.lastIndexOf("</label>"))
            frmAccountDetailKA.lblavailbalance.text = accountName.substring(6,accountName.length);
            frmAccountDetailKA.availableBalanceAmount.text = customerAccountDetails.data[parseInt(customerAccountDetails.currentIndex)][0].availableBalance; //+ " " + customerAccountDetails.data[parseInt(customerAccountDetails.currentIndex)][0].currencyCode;
          }
          clearForm.filterTransactions();
          fetch_TransactionList(kony.retailBanking.globalData.accountsDashboardData.accountsData[parseInt(customerAccountDetails.currentIndex)],{"toDATE":"","fromDATE":""});
        }
        //alert(kony.retailBanking.globalData.accountsDashboardData.accountsData[parseInt(customerAccountDetails.currentIndex)]);
      } catch (e) {
        kony.print("Animation Exception ::" + e)
      }
    }
    kony.print("gesture_AccountAnimation END");
  } catch (e) {
    kony.print("Exception_gesture_AccountAnimation ::" + e);
  }
}

function set_DateRange(){
  try{
    kony.print("Inside setDateRange");
    var fromDate = [1,1,1960];
    var date_Today = new Date();
    if(fromAccounts === true){
      frmFilterTransaction.lblLast90daystext.text = geti18Value("i18n.FilterTransaction.last60days");
      fromDate = [1,1,1960];
    }else if(fromCards == true){
      frmFilterTransaction.lblLast90daystext.text = geti18Value("i18n.FilterTransaction.last60days");
      date_Today.setDate(date_Today.getDate()-60);
      fromDate = [date_Today.getDate(),parseInt(date_Today.getMonth())+1,date_Today.getFullYear()];
    }
    date_Today.setDate(date_Today.getDate()+60);
    frmFilterTransaction.calDatefrom.validStartDate = fromDate;
    frmFilterTransaction.calDatefrom.validEndDate = [date_Today.getDate(),date_Today.getMonth()+1,date_Today.getFullYear()];
    frmFilterTransaction.calDateTo.validStartDate = fromDate;
    frmFilterTransaction.calDateTo.validEndDate = [date_Today.getDate(),date_Today.getMonth()+1,date_Today.getFullYear()];
  }catch(e){
    kony.print("Exception_set_DateRange ::"+e);
  }
}

function textbox_AmountOnly(field){
  try{
    field.text = field.text.replace(/[^0-9\.]/g,"");
  }catch(e){
    kony.print("Exception_textbox_AmountOnly ::"+e);
  }
}

function accountTransactionseg(){
  try{
    var currentForm = kony.application.getCurrentForm();
    var index = 0;
    if(currentForm.id === "frmManageCardsKA")
      index = customerAccountDetails.cardTransactionIndex;
    else
      index = customerAccountDetails.indicatorIndex;
    currentForm.imgSortDateAsc.src = "map_arrow_straight_nonselected.png";
    currentForm.imgSortDateDesc.src = "map_arrow_down_nonselected.png";
    currentForm.imgSortAmountAsc.src = "map_arrow_straight_nonselected.png";
    currentForm.imgSortAmountDesc.src = "map_arrow_down_nonselected.png";
    if(index <= 3 && customerAccountDetails.unClearedTransaction === false){
      index = parseInt(index) +1;
      var temp = [];
      var segData = "";
      for(i = 0; i <= index;i++){
        segData = kony.retailBanking.globalData.accountsTransactionList.transaction[i];
        for(var j=0; j<segData.length;j++){
          temp.push(kony.retailBanking.globalData.accountsTransactionList.transaction[i][j]);
        }
      }
      if(index === (kony.retailBanking.globalData.accountsTransactionList.transaction.length)-1){
        temp.push({"lblLastTransaction":{"text":"For more details please login to online banking","isVisible":true},
                   "description":{"text":"","isVisible":false},
                   "amount":{"text":"","isVisible":false},
                   "category":"",
                   "transactionDate":{"text":"","isVisible":false}});
      }
      kony.print("temp ::"+JSON.stringify(temp));
      currentForm.transactionSegment.widgetDataMap = {"transactionName":"description","transactionAmount":"amount","transactionDate":"transactionDate","lblLastTransaction":"lblLastTransaction"};
      currentForm.transactionSegment.setData(temp);
      if(currentForm.id === "frmManageCardsKA")
        customerAccountDetails.cardTransactionIndex = index;
      else
        customerAccountDetails.indicatorIndex = index;
      //         	clearForm.filterTransactions();
    }
  }catch(e){
    kony.print("Exception_accountTransactionseg ::"+e);
  }
}

function check_AmountDecimal(field,currency,textline){
  try{
    kony.print("check_AmountDecimal ::"+currency);
    //
    if(field.text != null && field.text != ""){
      if(currency == "JOD"){
        field.text = (1*field.text).toFixed(3);
      }else field.text = (1*field.text).toFixed(2);
      textline.skin = "sknFlxGreenLine";
    }
  }catch(e){
    kony.print("Exception_check_AmountDecimal ::"+e);
  }
}

function validate_FILTERDATESELECTION(){
  try{
    if(frmFilterTransaction.CopyFlexContainer0f90c1916338042.isVisible === false){
      return true;
    }
    var fromDate = frmFilterTransaction.lblDateFrom.text.split("/");
    var toDate = frmFilterTransaction.lblDateTo.text.split("/");
    if(new Date(fromDate[1]+"/"+fromDate[0]+"/"+fromDate[2]) <= new Date(toDate[1]+"/"+toDate[0]+"/"+toDate[2]))
      return true;
    return false;
  }catch(e){
    kony.print("Exception_validate_FILTERDATESELECTION ::"+e);
  }
}

function validate_FILTEROPTIONS(data){
  try{
    if((data.transactionAmount.min !== null && data.transactionAmount.max !== null && 
        data.transactionAmount.min !== "0.000" && data.transactionAmount.max !== "0.000" &&
        data.transactionAmount.min !== "0.00" && data.transactionAmount.max !== "0.00") ||
       data.days !== null || data.Mode.credit !== null || data.Mode.debit !== null || data.Mode.card !== null || 
       data.Mode.all !== null || (data.other.from !== null && data.other.to !== null) ||
       data.year !== null || data.transactionName !== null)
    {
      return true;
    }else{
      return false;
    }
  }catch(e){
    kony.print("Exception_"+e);
  }
}



function hideSortpopupinAccountsDetailsScreen(){
  frmAccountDetailKA.flxAccountdetailsSortContainer.top = "100%"
  frmAccountDetailKA.imgSortDateAsc.src = "map_arrow_straight_nonselected.png";
  frmAccountDetailKA.imgSortDateDesc.src = "map_arrow_down_nonselected.png";
  frmAccountDetailKA.imgSortAmountAsc.src = "map_arrow_straight_nonselected.png";
  frmAccountDetailKA.imgSortAmountDesc.src = "map_arrow_down_nonselected.png";
}