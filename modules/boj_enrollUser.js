var usernameFlag = false;
var passwordFlag = false;
var confrmPasswordFlag = false;

kony = kony || {};
kony.boj = kony.boj || {};

function initialiseAndCallEnroll() {
  var appContext = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
  try {
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    frmEnrolluserLandingKA.show();
  } catch (err) {
    kony.print("In initialiseAndCallEnroll err" + err);
	fetchApplicationPropertiesWithoutLoading();
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
	exceptionLogCall("initialiseAndCallEnroll","UI ERROR","UI",err);
  }
}


kony.boj.preShowEnrollUser = function() {
    try {
    	if (!isLoggedIn) {
        	fetchApplicationPropertiesWithoutLoading();
        }
        frmEnrolluserLandingKA.lblLanguage.text = "";
        frmEnrolluserLandingKA.lblFlag.text = "";
        frmEnrolluserLandingKA.lblUname.text = "";
        frmEnrolluserLandingKA.tbxUsernameKA.text = "";
        frmEnrolluserLandingKA.lblPwd.text = "";
        frmEnrolluserLandingKA.tbxPasswordKA.text = "";
        frmEnrolluserLandingKA.lblCPwd.text = "";
        frmEnrolluserLandingKA.tbxConfrmPwdKA.text = "";
      if(gblPayAppFlow && !isEmpty(gblLaunchModeOBJ.appFlow)){
        frmEnrolluserLandingKA.lblDontKnowUsername.setVisibility(false);
      }else{
          frmEnrolluserLandingKA.lblDontKnowUsername.setVisibility(true);
        }
        frmEnrolluserLandingKA.lblInvalidCredentialsKA.setVisibility(false);
        frmEnrolluserLandingKA.lblInvalidPass.setVisibility(false);
        frmEnrolluserLandingKA.lblClose.setVisibility(false);
        frmEnrolluserLandingKA.CopylblClose0bb2f50619b5543.setVisibility(false);
        frmEnrolluserLandingKA.CopylblClose0dcca487a6bc243.setVisibility(false);
        kony.boj.resetRegisterUsernameSkin();
        var langSelected = kony.store.getItem("langPrefObj");
        var Language = langSelected.toUpperCase();
        frmEnrolluserLandingKA.lblLanguage.text = Language;
        frmEnrolluserLandingKA.flxPasswordValidation.setVisibility(false);
        if (gblFromModule == "RegisterUser") {
//             alert("I Am Register");
            frmEnrolluserLandingKA.flxProgress1.setVisibility(true);
            frmEnrolluserLandingKA.flxProgress2.setVisibility(true);
            frmEnrolluserLandingKA.flxUsername.setVisibility(true);
            frmEnrolluserLandingKA.flxPassword.setVisibility(true);
            frmEnrolluserLandingKA.flxConfirmPassword.setVisibility(true);
            frmEnrolluserLandingKA.lblFlag.text = "R";
            frmEnrolluserLandingKA.lblTitle.text = geti18Value("i18n.registration.registerTitle");
            frmEnrolluserLandingKA.lblheader.text = geti18Value("i18n.registration.credentials");
            usernameFlag = false;
            passwordFlag = false;
            confrmPasswordFlag = false;
        } else if (gblFromModule == "ChangeUsername") {
            frmEnrolluserLandingKA.flxProgress1.setVisibility(false);
            frmEnrolluserLandingKA.flxProgress2.setVisibility(false);
            frmEnrolluserLandingKA.lblTitle.text = geti18Value("i18n.settings.myProfile.ChangeUsername");
            frmEnrolluserLandingKA.lblheader.text = geti18Value("i18n.settings.myProfile.ChangeUsername");
            frmEnrolluserLandingKA.flxUsername.setVisibility(true);
            frmEnrolluserLandingKA.flxPassword.setVisibility(false);
            frmEnrolluserLandingKA.flxConfirmPassword.setVisibility(false);
            usernameFlag = false;
            passwordFlag = true;
            confrmPasswordFlag = true;

        } else if (gblFromModule == "ChangePassword") {
            frmEnrolluserLandingKA.flxProgress1.setVisibility(false);
            frmEnrolluserLandingKA.flxProgress2.setVisibility(false);
            frmEnrolluserLandingKA.lblTitle.text = geti18Value("i18n.settings.myprofile.ChangePassword");
            frmEnrolluserLandingKA.lblheader.text = geti18Value("i18n.settings.myprofile.ChangePassword");
            frmEnrolluserLandingKA.flxUsername.setVisibility(false);
            frmEnrolluserLandingKA.flxPassword.setVisibility(true);
            frmEnrolluserLandingKA.flxConfirmPassword.setVisibility(true);
            frmEnrolluserLandingKA.flxForgotusername.isVisible = false;
            usernameFlag = true;
            passwordFlag = false;
            confrmPasswordFlag = false;
        } else { //ForgotPassword flow
            frmEnrolluserLandingKA.flxProgress1.setVisibility(true);
            frmEnrolluserLandingKA.flxProgress2.setVisibility(true);
            frmEnrolluserLandingKA.lblFlag.text = "F";
            frmEnrolluserLandingKA.lblheader.text = geti18Value("i18n.login.forgotPassword");
            if (gblReqField == "Username") {
                frmEnrolluserLandingKA.lblTitle.text = geti18Value("i18n.login.enterUsernamePlh");
                frmEnrolluserLandingKA.flxUsername.setVisibility(true);
                frmEnrolluserLandingKA.flxPassword.setVisibility(false);
                frmEnrolluserLandingKA.flxConfirmPassword.setVisibility(false);
                usernameFlag = false;
                passwordFlag = true;
                confrmPasswordFlag = true;
              if(isLoggedIn){
                frmEnrolluserLandingKA.flxForgotusername.setVisibility(false);
              }else{
                frmEnrolluserLandingKA.flxForgotusername.setVisibility(true);
              }
            } else {
                frmEnrolluserLandingKA.flxUsername.setVisibility(false);
                frmEnrolluserLandingKA.flxPassword.setVisibility(true);
                frmEnrolluserLandingKA.flxConfirmPassword.setVisibility(true);
                frmEnrolluserLandingKA.lblTitle.text = geti18Value("i18n.registration.createYourPassword");
                usernameFlag = true;
                passwordFlag = false;
                confrmPasswordFlag = false;
            }
        }
      if (frmEnrolluserLandingKA.lblheader.text === kony.i18n.getLocalizedString("i18n.login.forgotPassword") || frmEnrolluserLandingKA.lblheader.text === geti18Value("i18n.settings.myprofile.ChangePassword")) {
        frmEnrolluserLandingKA.flxNote.isVisible = true;
        if(isLoggedIn)
        frmEnrolluserLandingKA.flxForgotusername.isVisible = false;
        else{
        frmEnrolluserLandingKA.flxForgotusername.isVisible = true;  
        if (gblReqField !== "Username")
        frmEnrolluserLandingKA.flxForgotusername.isVisible = false;  
        }
        
      } else {
        frmEnrolluserLandingKA.flxNote.isVisible = false;
        frmEnrolluserLandingKA.flxForgotusername.isVisible = false;
      }
    } catch (err) {
        kony.print("err in kony.boj.preShowEnrollUser :: " + err);
		exceptionLogCall("kony.boj.preShowEnrollUser","UI ERROR","UI",err);
    }

};


kony.boj.resetRegisterUsernameSkin = function() {
    frmEnrolluserLandingKA.lblConditionCheck1.skin = "lblTick";
    frmEnrolluserLandingKA.lblCondition1.skin = "sknLblNextDisabled";
    frmEnrolluserLandingKA.lblConditionCheck2.skin = "lblTick";
    frmEnrolluserLandingKA.lblCondition2.skin = "sknLblNextDisabled";
    frmEnrolluserLandingKA.lblConditionCheck3.skin = "lblTick";
    frmEnrolluserLandingKA.lblCondition3.skin = "sknLblNextDisabled";
    frmEnrolluserLandingKA.lblConditionCheck4.skin = "lblTick";
    frmEnrolluserLandingKA.lblCondition4.skin = "sknLblNextDisabled";
    frmEnrolluserLandingKA.lblConditionCheck5.skin = "lblTick";
    frmEnrolluserLandingKA.lblCondition5.skin = "sknLblNextDisabled";
  
    frmEnrolluserLandingKA.lblConditionCheck6.skin = "lblTick";
    frmEnrolluserLandingKA.lblCondition6.skin = "sknLblNextDisabled";
    frmEnrolluserLandingKA.lblConditionCheck7.skin = "lblTick";
    frmEnrolluserLandingKA.lblCondition7.skin = "sknLblNextDisabled";
    frmEnrolluserLandingKA.lblConditionCheck8.skin = "lblTick";
    frmEnrolluserLandingKA.lblCondition8.skin = "sknLblNextDisabled";
    frmEnrolluserLandingKA.lblConditionCheck9.skin = "lblTick";
    frmEnrolluserLandingKA.lblCondition9.skin = "sknLblNextDisabled";
    frmEnrolluserLandingKA.lblConditionCheck10.skin = "lblTick";
    frmEnrolluserLandingKA.lblCondition10.skin = "sknLblNextDisabled";

    frmEnrolluserLandingKA.tbxUsernameKA.text = "";
    frmEnrolluserLandingKA.flxUnderlineUsername.skin = "sknFlxGreyLine";

    frmEnrolluserLandingKA.tbxPasswordKA.text = "";
    frmEnrolluserLandingKA.flxUnderlinePassword.skin = "sknFlxGreyLine";

    frmEnrolluserLandingKA.tbxConfrmPwdKA.text = "";
    frmEnrolluserLandingKA.flxUnderlineConfirmPassword.skin = "sknFlxGreyLine";

    frmEnrolluserLandingKA.lblNext.skin = "sknLblNextDisabled";
    frmEnrolluserLandingKA.flxProgress1.left = 65 + "%";
};


function callSave() {
    if (kony.sdk.isNetworkAvailable()) {
        try {
            var appContext = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
            var frmEnrolluserLandingKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmEnrolluserLandingKAConfig);
            var frmEnrolluserLandingKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmEnrolluserLandingKAController", appContext, frmEnrolluserLandingKAModelConfigObj);
            var frmEnrolluserLandingKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmEnrolluserLandingKAControllerExtension", frmEnrolluserLandingKAControllerObj);
            frmEnrolluserLandingKAControllerObj.setControllerExtensionObject(frmEnrolluserLandingKAControllerExtObj);
            var frmEnrolluserLandingKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmEnrolluserLandingKAFormModel", frmEnrolluserLandingKAControllerObj);
            var frmEnrolluserLandingKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmEnrolluserLandingKAFormModelExtension", frmEnrolluserLandingKAFormModelObj);
            frmEnrolluserLandingKAFormModelObj.setFormModelExtensionObj(frmEnrolluserLandingKAFormModelExtObj);
            appContext.setFormController("frmEnrolluserLandingKA", frmEnrolluserLandingKAControllerObj);

            var langSelected = kony.store.getItem("langPrefObj");
          var Language = langSelected.toUpperCase();
          frmEnrolluserLandingKA.lblLanguage.text = Language;
          var devID = deviceid;
          var imei = "";
          if (kony.boj.imeiPermissionFlag)
            imei = kony.os.deviceInfo().uid;
          else
            imei = "";
          
          var ipAddress = getDeviceIP();
          frmEnrolluserLandingKA.lblIEMI.text = imei;
          frmEnrolluserLandingKA.lblDevId.text = devID;
          frmEnrolluserLandingKA.lblIpAdd.text = ipAddress;
          
          var prevForm = kony.application.getPreviousForm().id;

            if (frmEnrolluserLandingKA.tbxUsernameKA.text !== "" && frmEnrolluserLandingKA.tbxUsernameKA.text !== null)
                frmEnrolluserLandingKA.lblUname.text = frmEnrolluserLandingKA.tbxUsernameKA.text;
            else if (gblFromModule == "ChangePassword") {
                frmEnrolluserLandingKA.lblUname.text = "";
            } else
                frmEnrolluserLandingKA.lblUname.text = gblUsername;

            frmEnrolluserLandingKA.lblPwd.text = frmEnrolluserLandingKA.tbxPasswordKA.text;
            frmEnrolluserLandingKA.lblCPwd.text = frmEnrolluserLandingKA.tbxConfrmPwdKA.text;

            if (gblFromModule == "RegisterUser" || gblToOTPFrom == "RegisterUserNational"){
//                 alert(gblToOTPFrom);
                frmEnrolluserLandingKA.lblFlag.text = "R";
            }
            else if (gblFromModule == "ChangeUsername")
                frmEnrolluserLandingKA.lblFlag.text = "U";
            else if (gblFromModule == "ChangePassword")
                frmEnrolluserLandingKA.lblFlag.text = "P";
            else
                frmEnrolluserLandingKA.lblFlag.text = "F";

            kony.print(frmEnrolluserLandingKA.lblPwd.text + "-----pwd uname------  " + frmEnrolluserLandingKA.lblUname.text + " -----cpwd----     " + frmEnrolluserLandingKA.lblCPwd.text + "            " + frmEnrolluserLandingKA.lblFlag.text + "--flag and lang -" + frmEnrolluserLandingKA.lblLanguage.text);

            var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
            var controller = INSTANCE.getFormController("frmEnrolluserLandingKA");
            var controllerContextData = new kony.sdk.mvvm.NavigationObject();
            controllerContextData.setDataModel(null, kony.sdk.mvvm.OperationType.ADD, "form");
            controller.setContextData(controllerContextData);
            controller.performAction("saveData");
        } catch (e) {
            kony.print("Err in :: " + e);
            initialisePreLoginForms();
            exceptionLogCall("callSave:boj_enrollUser","UI ERROR","UI",e);
            kony.application.dismissLoadingScreen();

        }
    } else {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
    }
}


function OnclicknextOnuserSCreen() {
    if (frmEnrolluserLandingKA.lblNext.skin == "sknLblNextEnabled") {
        if (kony.sdk.isNetworkAvailable()) {
            ShowLoadingScreen();
            if (frmEnrolluserLandingKA.lblNext.skin == "sknLblNextEnabled") {
              frmEnrolluserLandingKA.flxUsernameValidation.isVisible = false;
              frmEnrolluserLandingKA.flxPasswordValidation.isVisible = false;
              
                if (gblFromModule == "ChangeUsername") {
                    frmEnrolluserLandingKA.UserFlag.text = "C";
                    callSave();
                } else if (gblFromModule == "ChangePassword") {
                    if (!isLoggedIn) {
                        callRequestOTP();
                    } else {
                        callSave();
                    }
                } else {
                    callSave();
                }
            }
        } else {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
        }
    } else {
        kony.print("Do nothing");
    }
    frmEnrolluserLandingKA.lblNext.skin = "sknLblNextDisabled";
}

function enrolluserPostShow() {
    try {
        frmEnrolluserLandingKA.lblCondition1.text = geti18Value("i18n.common.firsttext1") + " " + kony.boj.util.passwordValidation.minPasswordLength + " " + geti18Value("i18n.FP.char");
        frmEnrolluserLandingKA.lblCondition2.text = geti18Value("i18n.common.firsttext2") + " " + kony.boj.util.passwordValidation.minUpperCase + " " + geti18Value("i18n.FP.Uchar");
        frmEnrolluserLandingKA.lblCondition3.text = geti18Value("i18n.common.firsttext3") + " " + kony.boj.util.passwordValidation.minLowerCase + " " + geti18Value("i18n.FP.Lchar");
        frmEnrolluserLandingKA.lblCondition4.text = geti18Value("i18n.common.firsttext4") + " " + kony.boj.util.passwordValidation.minDigit + " " + geti18Value("i18n.FP.digits");
        frmEnrolluserLandingKA.lblCondition5.text = geti18Value("i18n.common.firsttext5") + " " + kony.boj.util.passwordValidation.minSpecial + " " + geti18Value("i18n.FP.SChar");
    	jpdata = "( ";
    	for(var i in kony.boj.util.passwordValidation.validCharacters){
        	jpdata =  (i==0)?jpdata+""+kony.boj.util.passwordValidation.validCharacters[i]:jpdata+","+kony.boj.util.passwordValidation.validCharacters[i];
        }
    	jpdata = jpdata+" )";
		frmEnrolluserLandingKA.lblAllowededSpecialCharacterspwd.text = geti18Value("i18n.common.allowedspecialcharacters")+" "+jpdata;
    	kony.print("specila characters ::"+frmEnrolluserLandingKA.lblAllowededSpecialCharacterspwd.text);
		jpdata = "";
      
      
        frmEnrolluserLandingKA.lblCondition6.text = geti18Value("i18n.common.usernamefirsttext") + " " + kony.boj.util.usernameValidation.minUsernameLength + " " + geti18Value("i18n.FP.char");
        //frmEnrolluserLandingKA.lblCondition7.text = geti18Value("i18n.common.firsttext2") + " " + kony.boj.util.usernameValidation.minUpperCase + " " + geti18Value("i18n.FP.Uchar");
        //frmEnrolluserLandingKA.lblCondition8.text = geti18Value("i18n.common.firsttext3") + " " + kony.boj.util.usernameValidation.minLowerCase + " " + geti18Value("i18n.FP.Lchar");
        //frmEnrolluserLandingKA.lblCondition9.text = geti18Value("i18n.common.firsttext4") + " " + kony.boj.util.usernameValidation.minDigit + " " + geti18Value("i18n.FP.digits");
        //frmEnrolluserLandingKA.lblCondition10.text = geti18Value("i18n.common.firsttext5") + " " + kony.boj.util.usernameValidation.minSpecial + " " + geti18Value("i18n.FP.SChar");
    	jpdata = "( ";
    	for(var i in kony.boj.util.usernameValidation.validCharacters){
        	jpdata =  (i==0)?jpdata+""+kony.boj.util.usernameValidation.validCharacters[i]:jpdata+","+kony.boj.util.usernameValidation.validCharacters[i];
        }
    	jpdata = jpdata+" )";
		frmEnrolluserLandingKA.lblAllowededSpecialCharactersuname.text = geti18Value("i18n.common.uname4")+" "+jpdata;
    	kony.print("specila characters ::"+frmEnrolluserLandingKA.lblAllowededSpecialCharactersuname.text);
		jpdata = "";
      
      
      
      
      
      
        animateLabel("DOWN", "lblUsername", "");
        animateLabel("DOWN", "lblPassword", "");
        animateLabel("DOWN", "lblConfirmPassword", "");

        frmEnrolluserLandingKA.tbxUsernameKA.maxTextLength = (kony.boj.util.usernameValidation.maxUsernameLength);
        frmEnrolluserLandingKA.tbxPasswordKA.maxTextLength = (kony.boj.util.passwordValidation.maxPasswordLength);
        frmEnrolluserLandingKA.tbxConfrmPwdKA.maxTextLength = (kony.boj.util.passwordValidation.maxPasswordLength);
      
      
    } catch (err) {
        exceptionLogCall("enrolluserPostShow","UI ERROR","UI",err);
    }
}
function fetchForget_Username_OrPass_AccessibilityProps(){
  frmEnrolluserLandingKA.lblDontKnowUsername.accessibilityConfig = {
	
		"a11yValue": geti18Value("i18n.common.dontusername"),
		"a11yLabel": "",
		"a11yHint": ""
		};
  frmEnrolluserLandingKA.lblNotYourNum.accessibilityConfig = {
	
		"a11yValue": geti18Value("i18n.common.con"),
		"a11yLabel": "",
		"a11yHint": ""
		};
  frmEnrolluserLandingKA.flxBack.accessibilityConfig = {
	
		"a11yValue": geti18Value("i18n.deposit.back"),
		"a11yLabel": "",
		"a11yHint": ""
		};
  frmEnrolluserLandingKA.tbxUsernameKA.accessibilityConfig = {
	
		"a11yValue": geti18Value("i18n.login.usernameC"),
		"a11yLabel": "",
		"a11yHint": ""
		};
  frmEnrolluserLandingKA.lblContactCenterNumber.accessibilityConfig = {
	
		"a11yValue": frmEnrolluserLandingKA.lblContactCenterNumber.text,
		"a11yLabel": "",
		"a11yHint": ""
		};
  frmEnrolluserLandingKA.lblNext.accessibilityConfig = {
	
		"a11yValue": geti18Value("i18n.login.next"),
		"a11yLabel": "",
		"a11yHint": ""
		};
}