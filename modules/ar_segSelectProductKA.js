//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function initializesegSelectProductKAAr() {
    flxSegMainAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "120dp",
        "id": "flxSegMain",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox"
    }, {}, {});
    flxSegMain.setDefaultUnit(kony.flex.DP);
    var flxSegMain2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxSegMain2",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkgGrayf7f7f7",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSegMain2.setDefaultUnit(kony.flex.DP);
    var lblAccName = new kony.ui.Label({
        "id": "lblAccName",
        "isVisible": true,
        "right": "2%",
        "skin": "sknonboardingHeader107",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblData1 = new kony.ui.Label({
        "id": "lblData1",
        "isVisible": true,
        "right": "2%",
        "skin": "sknRegularFormlblfornuo",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "30%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblData2 = new kony.ui.Label({
        "id": "lblData2",
        "isVisible": true,
        "right": "2%",
        "skin": "sknRegularFormlblfornuo1",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "45%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnViewDetailsKA = new kony.ui.Button({
        "focusSkin": "sknSecondaryActionWithBorderFocus90KA",
        "height": "20%",
        "id": "btnViewDetailsKA",
        "isVisible": true,
        "right": "2%",
        "skin": "sknsecondaryAction1KA",
        "text": kony.i18n.getLocalizedString("i18n.NUO.ViewDetails"),
        "top": "66%",
        "width": "30%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    flxSegMain2.add(lblAccName, lblData1, lblData2, btnViewDetailsKA);
    var flxDivider1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "5dp",
        "id": "flxDivider1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkg",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxDivider1.setDefaultUnit(kony.flex.DP);
    flxDivider1.add();
    var imgUntick = new kony.ui.Image2({
        "centerY": "50%",
        "height": "35dp",
        "id": "imgUntick",
        "isVisible": true,
        "right": "85%",
        "skin": "slImage",
        "src": "inactive1.png",
        "width": "35dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    flxSegMainAr.add(flxSegMain2, flxDivider1, imgUntick);
}
