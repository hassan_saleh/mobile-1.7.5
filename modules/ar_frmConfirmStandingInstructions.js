//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmConfirmStandingInstructionsAr() {
frmConfirmStandingInstructions.setDefaultUnit(kony.flex.DP);
var flxHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"skin": "skncontainerBkg",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxHeader.setDefaultUnit(kony.flex.DP);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_e43e7f227ceb496c849ca8ca75147213,
"skin": "slFbox",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var CopylblHeaderBackIcon0e57343f2e2274c = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblHeaderBackIcon0e57343f2e2274c",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblHeaderBack0j7f57eb046594b = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblHeaderBack0j7f57eb046594b",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBack.add(CopylblHeaderBackIcon0e57343f2e2274c, CopylblHeaderBack0j7f57eb046594b);
var CopylblHeaderTitle0de254f31c2b44e = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "CopylblHeaderTitle0de254f31c2b44e",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.Transfer.ConfirmDet"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblClose = new kony.ui.Label({
"centerX": "94%",
"height": "100%",
"id": "lblClose",
"isVisible": false,
"skin": "sknCloseConfirm",
"text": "O",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "10%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxHeader.add(flxBack, CopylblHeaderTitle0de254f31c2b44e, lblClose);
var flxMain = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": true,
"clipBounds": true,
"enableScrolling": true,
"height": "91%",
"horizontalScrollIndicator": true,
"id": "flxMain",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "0dp",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
flxMain.setDefaultUnit(kony.flex.DP);
var flxImpDetails = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "25%",
"id": "flxImpDetails",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxImpDetails.setDefaultUnit(kony.flex.DP);
var CopyflxIcon0d4c897823fde4f = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "CopyflxIcon0d4c897823fde4f",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxRoundContainer",
"top": "5%",
"width": "70dp",
"zIndex": 1
}, {}, {});
CopyflxIcon0d4c897823fde4f.setDefaultUnit(kony.flex.DP);
var initialsCategory = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"height": "80%",
"id": "initialsCategory",
"isVisible": true,
"skin": "sknLblFromIcon",
"text": "BH",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyflxIcon0d4c897823fde4f.add(initialsCategory);
var lblName = new kony.ui.Label({
"centerX": "50%",
"id": "lblName",
"isVisible": true,
"skin": "sknBeneTitle",
"text": "Bernard Hermann",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "3%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblID = new kony.ui.Label({
"centerX": "50%",
"id": "lblID",
"isVisible": true,
"right": "0dp",
"skin": "sknLblNextDisabled",
"text": "9845654568",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxImpDetails.add(CopyflxIcon0d4c897823fde4f, lblName, lblID);
var flxFromAcc = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "15%",
"id": "flxFromAcc",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxFromAcc.setDefaultUnit(kony.flex.DP);
var lblFrom = new kony.ui.Label({
"height": "35%",
"id": "lblFrom",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.filtertransaction.to"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblFromAcc = new kony.ui.Label({
"height": "35%",
"id": "lblFromAcc",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "Savings Account ****5614",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBeneficiaryName = new kony.ui.Label({
"height": "35%",
"id": "lblBeneficiaryName",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "Hassan Al Saleh",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxFromAcc.add(lblFrom, lblFromAcc, lblBeneficiaryName);
var flxAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxAmount",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "2%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAmount.setDefaultUnit(kony.flex.DP);
var CopylblConfirmBNTitle0b7f4f5e29e9346 = new kony.ui.Label({
"height": "50%",
"id": "CopylblConfirmBNTitle0b7f4f5e29e9346",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.billsPay.Amount"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblAmount = new kony.ui.Label({
"height": "50%",
"id": "lblAmount",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "1243 JOD",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxAmount.add(CopylblConfirmBNTitle0b7f4f5e29e9346, lblAmount);
var flxScheduledDate = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxScheduledDate",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "2%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxScheduledDate.setDefaultUnit(kony.flex.DP);
var CopylblConfirmBankNameTitle0gb348fcf67e64f = new kony.ui.Label({
"height": "50%",
"id": "CopylblConfirmBankNameTitle0gb348fcf67e64f",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.Transfer.StartDate"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "42%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblScheduledDate = new kony.ui.Label({
"height": "50%",
"id": "lblScheduledDate",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "10 Feb 2018",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "42%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var FlexContainer0b57728c8c75b42 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlexContainer0b57728c8c75b42",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "50%",
"skin": "slFbox",
"top": "-100%",
"width": "50%",
"zIndex": 100
}, {}, {});
FlexContainer0b57728c8c75b42.setDefaultUnit(kony.flex.DP);
var CopylblConfirmBankNameTitle0hc62d89e8ae446 = new kony.ui.Label({
"height": "50%",
"id": "CopylblConfirmBankNameTitle0hc62d89e8ae446",
"isVisible": true,
"right": "0%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.Transfer.EndDate"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": "42%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblScheduledEndDate = new kony.ui.Label({
"height": "50%",
"id": "lblScheduledEndDate",
"isVisible": true,
"right": "0%",
"skin": "sknLblBack",
"text": "10 Feb 2018",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": "42%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
FlexContainer0b57728c8c75b42.add(CopylblConfirmBankNameTitle0hc62d89e8ae446, lblScheduledEndDate);
flxScheduledDate.add(CopylblConfirmBankNameTitle0gb348fcf67e64f, lblScheduledDate, FlexContainer0b57728c8c75b42);
var flxSIType = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxSIType",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "2%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxSIType.setDefaultUnit(kony.flex.DP);
var lblSIFrequencyTitle = new kony.ui.Label({
"height": "50%",
"id": "lblSIFrequencyTitle",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.confirmdetails.frequency"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "42%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblSIFrequency = new kony.ui.Label({
"height": "50%",
"id": "lblSIFrequency",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "Daily",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "42%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var FlexContainer0f04e78af304147 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlexContainer0f04e78af304147",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "50%",
"skin": "slFbox",
"top": "-100%",
"width": "50%",
"zIndex": 100
}, {}, {});
FlexContainer0f04e78af304147.setDefaultUnit(kony.flex.DP);
var lblPeriodTitle = new kony.ui.Label({
"height": "50%",
"id": "lblPeriodTitle",
"isVisible": true,
"right": "0%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.common.PeriodC"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": "42%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblPeriodValue = new kony.ui.Label({
"height": "50%",
"id": "lblPeriodValue",
"isVisible": true,
"right": "0%",
"skin": "sknLblBack",
"text": "1",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": "42%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
FlexContainer0f04e78af304147.add(lblPeriodTitle, lblPeriodValue);
flxSIType.add(lblSIFrequencyTitle, lblSIFrequency, FlexContainer0f04e78af304147);
var flxReferenceID = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxReferenceID",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "2%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxReferenceID.setDefaultUnit(kony.flex.DP);
var CopylblConfirmCountryTitle0fcd86eed51d946 = new kony.ui.Label({
"height": "50%",
"id": "CopylblConfirmCountryTitle0fcd86eed51d946",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.common.ReferenceId"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblReferenceID = new kony.ui.Label({
"height": "50%",
"id": "lblReferenceID",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "1234567890",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxReferenceID.add(CopylblConfirmCountryTitle0fcd86eed51d946, lblReferenceID);
var flxFee = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxFee",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "2%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxFee.setDefaultUnit(kony.flex.DP);
var CopylblConfirmCountryTitle0a095687a3dab49 = new kony.ui.Label({
"height": "50%",
"id": "CopylblConfirmCountryTitle0a095687a3dab49",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.confirmdetails.fee"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBillerNumber = new kony.ui.Label({
"height": "50%",
"id": "lblBillerNumber",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "0.060 JOD",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxFee.add(CopylblConfirmCountryTitle0a095687a3dab49, lblBillerNumber);
var flxDescription = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxDescription",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "2%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxDescription.setDefaultUnit(kony.flex.DP);
var lblDescriptionHead = new kony.ui.Label({
"height": "50%",
"id": "lblDescriptionHead",
"isVisible": true,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.Transfer.Desc"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblDescription = new kony.ui.Label({
"height": "50%",
"id": "lblDescription",
"isVisible": true,
"right": "8%",
"skin": "sknLblBack",
"text": "Some Random Description",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxDescription.add(lblDescriptionHead, lblDescription);
var flxStopPayment = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxStopPayment",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "5%",
"width": "100%",
"zIndex": 100
}, {}, {});
flxStopPayment.setDefaultUnit(kony.flex.DP);
var CopybtnConfirm0afe340a6460347 = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "slButtonWhiteFocus",
"height": "100%",
"id": "CopybtnConfirm0afe340a6460347",
"isVisible": true,
"onClick": AS_Button_gde4fcb460514ad6a48c3fded7829831,
"skin": "slButtonWhite",
"text": kony.i18n.getLocalizedString("i18n.bills.StoppedPayment"),
"top": "0%",
"width": "80%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopybtnEditTranscation0cdd2ff91db6b45 = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "sknsecondaryActionFocus",
"height": "35dp",
"id": "CopybtnEditTranscation0cdd2ff91db6b45",
"isVisible": false,
"skin": "sknsecondaryAction",
"text": "Edit",
"top": "5dp",
"width": "260dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxStopPayment.add(CopybtnConfirm0afe340a6460347, CopybtnEditTranscation0cdd2ff91db6b45);
var CopyflxAmount0h42ee0066fa841 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "CopyflxAmount0h42ee0066fa841",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFbox",
"top": "2%",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxAmount0h42ee0066fa841.setDefaultUnit(kony.flex.DP);
var CopylblConfirmEmailTitle0e56c42a5adca42 = new kony.ui.Label({
"height": "50%",
"id": "CopylblConfirmEmailTitle0e56c42a5adca42",
"isVisible": false,
"right": "8%",
"skin": "sknLblNextDisabled",
"text": "Issue Date",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblConfirmEmail0caa6a6f404754d = new kony.ui.Label({
"height": "50%",
"id": "CopylblConfirmEmail0caa6a6f404754d",
"isVisible": false,
"right": "8%",
"skin": "sknLblBack",
"text": "1 JOD",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyflxAmount0h42ee0066fa841.add(CopylblConfirmEmailTitle0e56c42a5adca42, CopylblConfirmEmail0caa6a6f404754d);
flxMain.add(flxImpDetails, flxFromAcc, flxAmount, flxScheduledDate, flxSIType, flxReferenceID, flxFee, flxDescription, flxStopPayment, CopyflxAmount0h42ee0066fa841);
var flxService = new kony.ui.FlexContainer({
"clipBounds": true,
"height": "220dp",
"id": "flxService",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 100
}, {}, {});
flxService.setDefaultUnit(kony.flex.DP);
var lblDebitAcc = new kony.ui.Label({
"id": "lblDebitAcc",
"isVisible": true,
"right": "73dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "71dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCustID = new kony.ui.Label({
"id": "lblCustID",
"isVisible": true,
"right": "83dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "81dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblfcdb = new kony.ui.Label({
"id": "lblfcdb",
"isVisible": true,
"right": "93dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "91dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblUsr = new kony.ui.Label({
"id": "lblUsr",
"isVisible": true,
"right": "103dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "101dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblPass = new kony.ui.Label({
"id": "lblPass",
"isVisible": true,
"right": "113dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "111dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblStatus = new kony.ui.Label({
"id": "lblStatus",
"isVisible": true,
"right": "93dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "91dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblTransactionType = new kony.ui.Label({
"id": "lblTransactionType",
"isVisible": false,
"right": "103dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "101dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblNarrative = new kony.ui.Label({
"id": "lblNarrative",
"isVisible": false,
"right": "113dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "111dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCrAcc = new kony.ui.Label({
"id": "lblCrAcc",
"isVisible": false,
"right": "123dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "121dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblPeriod = new kony.ui.Label({
"id": "lblPeriod",
"isVisible": false,
"right": "133dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "131dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblInstructionNumber = new kony.ui.Label({
"id": "lblInstructionNumber",
"isVisible": false,
"right": "143dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "141dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCreBranchCode = new kony.ui.Label({
"id": "lblCreBranchCode",
"isVisible": false,
"right": "153dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "151dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblHiddenAmnt = new kony.ui.Label({
"id": "lblHiddenAmnt",
"isVisible": false,
"right": "163dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "161dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblEndDate = new kony.ui.Label({
"id": "lblEndDate",
"isVisible": false,
"right": "173dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "171dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblStartDate = new kony.ui.Label({
"id": "lblStartDate",
"isVisible": false,
"right": "173dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "171dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var segSIDetails = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"groupCells": false,
"height": "120dp",
"id": "segSIDetails",
"isVisible": false,
"right": "151dp",
"needPageIndicator": true,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "seg2Focus",
"rowSkin": "seg2Normal",
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"sectionHeaderTemplate": flxtmpSIList,
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "64646400",
"separatorRequired": true,
"separatorThickness": 1,
"showScrollbars": false,
"top": "92dp",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"CopyflxTAmount0bc17ffab355945": "CopyflxTAmount0bc17ffab355945",
"CopylblAmount0a2751be25d5944": "CopylblAmount0a2751be25d5944",
"flxIcon1": "flxIcon1",
"flxTAmount": "flxTAmount",
"flxUserDetails": "flxUserDetails",
"flxtmpSIList": "flxtmpSIList",
"lblAmount": "lblAmount",
"lblInitial": "lblInitial",
"lblTransactionDesc": "lblTransactionDesc",
"lblTransactiondate": "lblTransactiondate"
},
"width": "100%",
"zIndex": 1
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxService.add(lblDebitAcc, lblCustID, lblfcdb, lblUsr, lblPass, lblStatus, lblTransactionType, lblNarrative, lblCrAcc, lblPeriod, lblInstructionNumber, lblCreBranchCode, lblHiddenAmnt, lblEndDate, lblStartDate, segSIDetails);
frmConfirmStandingInstructions.add(flxHeader, flxMain, flxService);
};
function frmConfirmStandingInstructionsGlobalsAr() {
frmConfirmStandingInstructionsAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmConfirmStandingInstructionsAr,
"enabledForIdleTimeout": true,
"id": "frmConfirmStandingInstructions",
"layoutType": kony.flex.FLOW_VERTICAL,
"needAppMenu": false,
"skin": "sknmainGradient"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FLOW_VERTICAL,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_f09fff65acb74315a159097d62e5ef49,
"retainScrollPosition": false,
"titleBar": true,
"titleBarSkin": "slTitleBar",
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
