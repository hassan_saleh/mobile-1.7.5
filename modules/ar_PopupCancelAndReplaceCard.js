//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsPopupCancelAndReplaceCardAr() {
var HBxTitleCancelCard = new kony.ui.Box({
"id": "HBxTitleCancelCard",
"isVisible": true,
"orientation": constants.BOX_LAYOUT_HORIZONTAL,
"position": constants.BOX_POSITION_AS_NORMAL,
"skin": "sknWhiteBGHBX"
}, {
"containerWeight": 100,
"layoutType": constants.CONTAINER_LAYOUT_BOX,
"margin": [ 0, 0,0, 0],
"marginInPixel": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false,
"percent": true,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_TOP_RIGHT
}, {});
var lblCardCancelDesc = new kony.ui.Label({
"id": "lblCardCancelDesc",
"isVisible": true,
"skin": "sknlblblue",
"text": "Cancel Card",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
}
}, {
"containerWeight": 100,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"hExpand": true,
"margin": [ 1, 3,35, 3],
"marginInPixel": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_CENTER
}, {
"textCopyable": false
});
HBxTitleCancelCard.add(lblCardCancelDesc);
var HBxTitle = new kony.ui.Box({
"id": "HBxTitle",
"isVisible": true,
"orientation": constants.BOX_LAYOUT_HORIZONTAL,
"position": constants.BOX_POSITION_AS_NORMAL,
"skin": "slHbox"
}, {
"containerWeight": 100,
"layoutType": constants.CONTAINER_LAYOUT_BOX,
"margin": [ 0, 0,0, 0],
"marginInPixel": false,
"padding": [ 0, 5,0, 5],
"paddingInPixel": false,
"percent": true,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_CENTER
}, {});
var lblCardCancelTitle = new kony.ui.Label({
"id": "lblCardCancelTitle",
"isVisible": true,
"skin": "sknlblblue",
"text": kony.i18n.getLocalizedString("i18n.cardcancel.title"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
}
}, {
"containerWeight": 100,
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"hExpand": true,
"margin": [ 1, 1,6, 1],
"marginInPixel": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_CENTER
}, {
"textCopyable": false
});
HBxTitle.add(lblCardCancelTitle);
var HBoxCacelCard = new kony.ui.Box({
"id": "HBoxCacelCard",
"isVisible": true,
"orientation": constants.BOX_LAYOUT_HORIZONTAL,
"position": constants.BOX_POSITION_AS_NORMAL,
"skin": "slHbox"
}, {
"containerWeight": 100,
"layoutType": constants.CONTAINER_LAYOUT_BOX,
"margin": [ 0, 0,0, 1],
"marginInPixel": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false,
"percent": true,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_TOP_RIGHT
}, {});
var RdBtnCancelCard = new kony.ui.RadioButtonGroup({
"id": "RdBtnCancelCard",
"isVisible": true,
"masterData": [["rbg1", kony.i18n.getLocalizedString("i18n.cardcancel.cardcancel")]],
"onSelection": AS_RadioButtonGroup_j2cf187be0a842a29e5ce8ba9f90c12f,
"selectedKey": "rbg1",
"selectedKeyValue": ["rbg1", "Card Cancel"],
"skin": "sknRDBlue"
}, {
"containerWeight": 100,
"hExpand": true,
"itemOrientation": constants.RADIOGROUP_ITEM_ORIENTATION_VERTICAL,
"margin": [ 2, 5,2, 2],
"marginInPixel": false,
"padding": [ 1, 1,1, 1],
"paddingInPixel": false,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_CENTER
}, {
"tickedImage": "radioselected.png",
"untickedImage": "radiononselected.png"
});
HBoxCacelCard.add(RdBtnCancelCard);
var HBxCardCancelReason = new kony.ui.Box({
"id": "HBxCardCancelReason",
"isVisible": true,
"orientation": constants.BOX_LAYOUT_HORIZONTAL,
"position": constants.BOX_POSITION_AS_NORMAL,
"skin": "sknHbxDropdown"
}, {
"containerWeight": 100,
"layoutType": constants.CONTAINER_LAYOUT_BOX,
"margin": [ 7, 6,7, 0],
"marginInPixel": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false,
"percent": true,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_TOP_RIGHT
}, {});
var lstbxCardCancelReason = new kony.ui.ListBox({
"focusSkin": "slListBoxNew",
"id": "lstbxCardCancelReason",
"isVisible": true,
"masterData": [["lb1", "Reason for card cancel"],["lb2", "Card cancel reason1"],["Key735", "Card cancel reason2"]],
"selectedKey": "lb1",
"selectedKeyValue": ["lb1", "Reason for card cancel"],
"skin": "slListBoxNew"
}, {
"containerWeight": 100,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"hExpand": true,
"margin": [ 0, 0,0, 0],
"marginInPixel": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_CENTER
}, {
"applySkinsToPopup": true,
"dropDownImage": "dropdownlist.png",
"viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
});
HBxCardCancelReason.add(lstbxCardCancelReason);
var HBxCardDeliveryBranch = new kony.ui.Box({
"id": "HBxCardDeliveryBranch",
"isVisible": false,
"orientation": constants.BOX_LAYOUT_HORIZONTAL,
"position": constants.BOX_POSITION_AS_NORMAL,
"skin": "sknHbxDropdown"
}, {
"containerWeight": 100,
"layoutType": constants.CONTAINER_LAYOUT_BOX,
"margin": [ 7, 8,7, 0],
"marginInPixel": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false,
"percent": true,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_TOP_RIGHT
}, {});
var lstbxCardDeliveryBranch = new kony.ui.ListBox({
"id": "lstbxCardDeliveryBranch",
"isVisible": true,
"masterData": [["lb1", "Select delivery branch for the card"],["lb2", "Branch1"],["Key938", "Branch2"]],
"selectedKey": "lb1",
"selectedKeyValue": ["lb1", "Select delivery branch for the card"],
"skin": "slListBoxNew"
}, {
"containerWeight": 100,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"hExpand": true,
"margin": [ 0, 0,0, 0],
"marginInPixel": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_CENTER
}, {
"applySkinsToPopup": true,
"dropDownImage": "dropdownlist.png",
"viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
});
HBxCardDeliveryBranch.add(lstbxCardDeliveryBranch);
var HBxSubmit = new kony.ui.Box({
"id": "HBxSubmit",
"isVisible": true,
"orientation": constants.BOX_LAYOUT_HORIZONTAL,
"position": constants.BOX_POSITION_AS_NORMAL,
"skin": "slHbox"
}, {
"containerWeight": 100,
"layoutType": constants.CONTAINER_LAYOUT_BOX,
"margin": [ 3, 10,3, 0],
"marginInPixel": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false,
"percent": true,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_TOP_RIGHT
}, {});
var btnSubmit = new kony.ui.Button({
"id": "btnSubmit",
"isVisible": true,
"onClick": AS_Button_ac2f6245e35943fea1d1a7ae55592a7c,
"skin": "sknCancleBtn",
"text": "Submit"
}, {
"containerWeight": 100,
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"hExpand": true,
"margin": [ 6, 4,6, 6],
"marginInPixel": false,
"padding": [ 3, 3,3, 3],
"paddingInPixel": false,
"vExpand": false,
"widgetAlignment": constants.WIDGET_ALIGN_CENTER
}, {});
HBxSubmit.add(btnSubmit);
PopupCancelAndReplaceCard.add(HBxTitleCancelCard, HBxTitle, HBoxCacelCard, HBxCardCancelReason, HBxCardDeliveryBranch, HBxSubmit);
};
function PopupCancelAndReplaceCardGlobalsAr() {
PopupCancelAndReplaceCardAr = new kony.ui.Popup({
"addWidgets": addWidgetsPopupCancelAndReplaceCardAr,
"id": "PopupCancelAndReplaceCard",
"isModal": true,
"skin": "sknWTBGWTBRDRND",
"transparencyBehindThePopup": 25
}, {
"containerWeight": 90,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"windowSoftInputMode": constants.POPUP_ADJUST_PAN
});
};
