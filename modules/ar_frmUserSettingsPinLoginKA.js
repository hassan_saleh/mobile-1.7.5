//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmUserSettingsPinLoginKAAr() {
    frmUserSettingsPinLoginKA.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFlxHeaderImg",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var lblPinLoginHeader = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblPinLoginHeader",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.PINLogin"),
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnBack = new kony.ui.Button({
        "focusSkin": "CopyslButtonGlossBlue0e73a02c4810645",
        "height": "90%",
        "id": "btnBack",
        "isVisible": true,
        "left": "2%",
        "onClick": AS_Button_a4e715a360c945c7a10bcb1c4666163b,
        "skin": "CopyslButtonGlossBlue0e73a02c4810645",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblBack = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Login Screen"
        },
        "height": "90%",
        "id": "lblBack",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxHeader.add(lblPinLoginHeader, btnBack, lblBack);
    var flxBody = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBody",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "skin": "slFbox",
        "top": "9%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBody.setDefaultUnit(kony.flex.DP);
    var lblPinBody = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblPinBody",
        "isVisible": true,
        "skin": "sknstandardTextBold",
        "text": kony.i18n.getLocalizedString("i18n.pinPage.bodyText"),
        "top": "3%",
        "width": "90%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxAlterAuthPinLoginContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "20%",
        "id": "flxAlterAuthPinLoginContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxAlterAuthPinLoginContainer.setDefaultUnit(kony.flex.DP);
    var flxAlterAuthPinLogin = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "40%",
        "id": "flxAlterAuthPinLogin",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknFlxLightGreyColor",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxAlterAuthPinLogin.setDefaultUnit(kony.flex.DP);
    var lblActivateDeactivateText = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblActivateDeactivateText",
        "isVisible": true,
        "right": "3%",
        "skin": "sknWhiteChanged",
        "text": kony.i18n.getLocalizedString("i18n.acivate.pinlogin"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "79%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lbl = new kony.ui.Label({
        "id": "lbl",
        "isVisible": true,
        "right": "5%",
        "skin": "sknlblTouchIdsmall",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "37dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAlterAuthPinLogin.add(lblActivateDeactivateText, lbl);
    var flxSwitchOffTouchLogin = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25%",
        "id": "flxSwitchOffTouchLogin",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_c376c1c740df40bcb9b9bd5514e6918c,
        "left": "8%",
        "skin": "sknflxGrey",
        "width": "12%",
        "zIndex": 1
    }, {}, {});
    flxSwitchOffTouchLogin.setDefaultUnit(kony.flex.DP);
    var CopyflxRoundDBlue0hfee6fe5ed994b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "CopyflxRoundDBlue0hfee6fe5ed994b",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "sknFlxDarkBlueRoundCornerGrey",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {}, {});
    CopyflxRoundDBlue0hfee6fe5ed994b.setDefaultUnit(kony.flex.DP);
    CopyflxRoundDBlue0hfee6fe5ed994b.add();
    var Copyflxlakeer0a0654737f4504c = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "12dp",
        "id": "Copyflxlakeer0a0654737f4504c",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "10dp",
        "skin": "sknLineDarkBlue",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    Copyflxlakeer0a0654737f4504c.setDefaultUnit(kony.flex.DP);
    Copyflxlakeer0a0654737f4504c.add();
    flxSwitchOffTouchLogin.add(CopyflxRoundDBlue0hfee6fe5ed994b, Copyflxlakeer0a0654737f4504c);
    var flxSwitchOnTouchLogin = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25%",
        "id": "flxSwitchOnTouchLogin",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_b2ea3c7cce6c491193ac09de228ab942,
        "left": "8%",
        "skin": "sknflxyellow",
        "width": "12%",
        "zIndex": 1
    }, {}, {});
    flxSwitchOnTouchLogin.setDefaultUnit(kony.flex.DP);
    var CopyflxRoundDBlueOff0j3222bd94cbc49 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "CopyflxRoundDBlueOff0j3222bd94cbc49",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "sknFlxDarkBlueRoundCorner",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 2
    }, {}, {});
    CopyflxRoundDBlueOff0j3222bd94cbc49.setDefaultUnit(kony.flex.DP);
    CopyflxRoundDBlueOff0j3222bd94cbc49.add();
    var CopyflxNaveenbhaiKiLakeer0ee7fe15950b44b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "12dp",
        "id": "CopyflxNaveenbhaiKiLakeer0ee7fe15950b44b",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "10dp",
        "skin": "sknLineDarkBlue",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    CopyflxNaveenbhaiKiLakeer0ee7fe15950b44b.setDefaultUnit(kony.flex.DP);
    CopyflxNaveenbhaiKiLakeer0ee7fe15950b44b.add();
    flxSwitchOnTouchLogin.add(CopyflxRoundDBlueOff0j3222bd94cbc49, CopyflxNaveenbhaiKiLakeer0ee7fe15950b44b);
    flxAlterAuthPinLoginContainer.add(flxAlterAuthPinLogin, flxSwitchOffTouchLogin, flxSwitchOnTouchLogin);
    var btnSetResetPin = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknprimaryActionFcs",
        "height": "9%",
        "id": "btnSetResetPin",
        "isVisible": false,
        "maxHeight": 50,
        "onClick": AS_Button_g30c3e69c1b3478e86eac894cc65da1a,
        "skin": "sknprimaryAction",
        "text": "Reset PIN",
        "top": "55%",
        "width": "75%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    flxBody.add(lblPinBody, flxAlterAuthPinLoginContainer, btnSetResetPin);
    frmUserSettingsPinLoginKA.add(flxHeader, flxBody);
};
function frmUserSettingsPinLoginKAGlobalsAr() {
    frmUserSettingsPinLoginKAAr = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmUserSettingsPinLoginKAAr,
        "allowHorizontalBounce": false,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmUserSettingsPinLoginKA",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "preShow": AS_Form_j90df7e9f14e4a0e95d40d78c2117993,
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": true,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_fbe6842601814da9b8b3619742765083,
        "retainScrollPosition": false,
        "titleBar": true,
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};
