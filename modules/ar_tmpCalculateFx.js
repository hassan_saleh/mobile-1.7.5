//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function initializetmpCalculateFxAr() {
    flxTmpCalculateFxAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "85dp",
        "id": "flxTmpCalculateFx",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox"
    }, {}, {});
    flxTmpCalculateFx.setDefaultUnit(kony.flex.DP);
    var flxFlag = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "50dp",
        "id": "flxFlag",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "5%",
        "skin": "sknflxroundflagBorder",
        "width": "50dp",
        "zIndex": 1
    }, {}, {});
    flxFlag.setDefaultUnit(kony.flex.DP);
    var imgFlag = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "100%",
        "id": "imgFlag",
        "isVisible": true,
        "skin": "slImage",
        "src": "bahrainflag.png",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    flxFlag.add(imgFlag);
    var lblConvertedValue = new kony.ui.Label({
        "id": "lblConvertedValue",
        "isVisible": true,
        "right": "22%",
        "skin": "sknlblWhitecarioRegular135",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "16%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCurrency = new kony.ui.Label({
        "id": "lblCurrency",
        "isVisible": true,
        "right": "22%",
        "skin": "sknlblWhitecariolight115",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblActualValue = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblActualValue",
        "isVisible": true,
        "right": "58%",
        "skin": "sknlblWhitecarioRegular100",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblEqual = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblEqual",
        "isVisible": true,
        "right": "70%",
        "skin": "sknlblWhitecarioRegular135",
        "text": "=",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblEqualentValue = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblEqualentValue",
        "isVisible": true,
        "right": "76%",
        "skin": "sknlblWhitecarioRegular100",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxTmpCalculateFxAr.add(flxFlag, lblConvertedValue, lblCurrency, lblActualValue, lblEqual, lblEqualentValue);
}
