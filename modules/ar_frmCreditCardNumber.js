//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmCreditCardNumberAr() {
frmCreditCardNumber.setDefaultUnit(kony.flex.DP);
var flxCreditCardNumber = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxCreditCardNumber",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxCreditCardNumber.setDefaultUnit(kony.flex.DP);
var flxCreditCardNumberHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxCreditCardNumberHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "s",
"top": "0%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxCreditCardNumberHeader.setDefaultUnit(kony.flex.DP);
var lblCreditCardNumber = new kony.ui.Label({
"centerX": "50%",
"centerY": "45%",
"id": "lblCreditCardNumber",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.creditcard.cardnumber"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_b70de31ff4984a2ba116f1f68c5de504,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 2
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBack = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"centerY": "50%",
"id": "lblBack",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBack.add(lblBackIcon, lblBack);
flxCreditCardNumberHeader.add(lblCreditCardNumber, flxBack);
var segCreditCardNumber = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"data": [{
"lblCardNum": "Label",
"lblCardType": "234567890",
"lblHiddenCardNum": "4321 56** **** 3456"
}, {
"lblCardNum": "Label",
"lblCardType": "234567890",
"lblHiddenCardNum": "4321 56** **** 3786"
}, {
"lblCardNum": "Label",
"lblCardType": "234567890",
"lblHiddenCardNum": "5261 56** **** 3456"
}],
"groupCells": false,
"height": "90.22%",
"id": "segCreditCardNumber",
"isVisible": true,
"right": "0%",
"needPageIndicator": true,
"onRowClick": AS_Segment_de6903682b94416b8895ebbcb0c906e9,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "seg2Focus",
"rowSkin": "Copyseg0eab44f6fee814d",
"rowTemplate": flxCardNumber,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "1e415b00",
"separatorRequired": true,
"separatorThickness": 1,
"showScrollbars": false,
"top": "10%",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"flxCardNumber": "flxCardNumber",
"flxCardNumberContainer": "flxCardNumberContainer",
"lblCardHolderName": "lblCardHolderName",
"lblCardNum": "lblCardNum",
"lblCardType": "lblCardType",
"lblHiddenCardNum": "lblHiddenCardNum"
},
"width": "100%",
"zIndex": 2
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxCreditCardNumber.add(flxCreditCardNumberHeader, segCreditCardNumber);
frmCreditCardNumber.add(flxCreditCardNumber);
};
function frmCreditCardNumberGlobalsAr() {
frmCreditCardNumberAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmCreditCardNumberAr,
"allowVerticalBounce": false,
"bounces": false,
"enabledForIdleTimeout": true,
"id": "frmCreditCardNumber",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"preShow": AS_Form_dde91e2022fd43b1b5f4e79aa08e9fb9,
"skin": "slFormCommon"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_d3dbaa8b103d4f1c8fa39d8192161a8a,
"retainScrollPosition": false,
"titleBar": true,
"titleBarSkin": "slTitleBar",
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
