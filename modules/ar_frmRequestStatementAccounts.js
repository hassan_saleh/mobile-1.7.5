//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmRequestStatementAccountsAr() {
frmRequestStatementAccounts.setDefaultUnit(kony.flex.DP);
var flxHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0b7d74518f07a4d",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxHeader.setDefaultUnit(kony.flex.DP);
var lblFormHeading = new kony.ui.Label({
"centerX": "50%",
"centerY": "45%",
"height": "100%",
"id": "lblFormHeading",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.accounts.requestStatement"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxNext = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "100%",
"id": "flxNext",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_d2461ce347684a128aae6c81311a8dba,
"right": "2%",
"skin": "slFbox",
"top": "5%",
"width": "18%",
"zIndex": 2
}, {}, {});
flxNext.setDefaultUnit(kony.flex.DP);
var lblNext = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Next Page"
},
"height": "90%",
"id": "lblNext",
"isVisible": true,
"left": "0dp",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.login.next"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxNext.add(lblNext);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 1
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"height": "100%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBack = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"height": "100%",
"id": "lblBack",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var btn = new kony.ui.Button({
"focusSkin": "slButtonGlossRed",
"height": "0%",
"id": "btn",
"isVisible": true,
"left": "0%",
"skin": "btnBack0b71f859656c647",
"top": "0%",
"width": "0%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
flxBack.add(lblBackIcon, lblBack, btn);
flxHeader.add(lblFormHeading, flxNext, flxBack);
var flxScrllMain = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "91%",
"horizontalScrollIndicator": true,
"id": "flxScrllMain",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "9%",
"verticalScrollIndicator": false,
"width": "100%",
"zIndex": 1
}, {}, {});
flxScrllMain.setDefaultUnit(kony.flex.DP);
var flxAccountNumber = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "13%",
"id": "flxAccountNumber",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"onClick": AS_FlexContainer_ef3d6e8f16734ee9832b58103b1c3d3a,
"skin": "slFbox0f0c75590716b41",
"top": "3%",
"width": "100%",
"zIndex": 100
}, {}, {});
flxAccountNumber.setDefaultUnit(kony.flex.DP);
var lblAccountNumber = new kony.ui.Label({
"height": "35%",
"id": "lblAccountNumber",
"isVisible": true,
"right": "5%",
"skin": "sknLblCurr",
"text": kony.i18n.getLocalizedString("i18n.common.accountNumber"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblNumber = new kony.ui.Label({
"bottom": "0%",
"height": "48%",
"id": "lblNumber",
"isVisible": true,
"right": "5%",
"onTouchEnd": AS_Label_afeefa94cac342979c854345abc52ace,
"skin": "sknLblWhike125",
"text": "Account Number",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "28%",
"width": "88%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblNumber1 = new kony.ui.Label({
"bottom": "0%",
"centerX": "60%",
"height": "48%",
"id": "lblNumber1",
"isVisible": false,
"onTouchEnd": AS_Label_afeefa94cac342979c854345abc52ace,
"skin": "sknLblWhike125",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "38%",
"width": "95%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblLine4 = new kony.ui.Label({
"centerX": "50%",
"height": "2%",
"id": "lblLine4",
"isVisible": true,
"skin": "lblLine0h8b99d8a85f649",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "77%",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxEdit = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50%",
"id": "flxEdit",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"skin": "slFbox",
"top": "15dp",
"width": "53dp",
"zIndex": 2
}, {}, {});
flxEdit.setDefaultUnit(kony.flex.DP);
var lblEdit = new kony.ui.Label({
"height": "100%",
"id": "lblEdit",
"isVisible": true,
"right": "0dp",
"onTouchEnd": AS_Label_cb7119370d9d4d2fb3b0beed26c5db40,
"skin": "sknLblEditDimmed",
"text": kony.i18n.getLocalizedString("i18n.common.backReverse"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxEdit.add(lblEdit);
flxAccountNumber.add(lblAccountNumber, lblNumber, lblNumber1, lblLine4, flxEdit);
var lblFromTitle = new kony.ui.Label({
"id": "lblFromTitle",
"isVisible": true,
"right": "5%",
"skin": "sknLblCurr",
"text": kony.i18n.getLocalizedString("i18n.common.fromc"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "3%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxFrom = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "13%",
"id": "flxFrom",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0f0c75590716b41",
"top": "2%",
"width": "100%",
"zIndex": 100
}, {}, {});
flxFrom.setDefaultUnit(kony.flex.DP);
var lblFrom = new kony.ui.Label({
"height": "35%",
"id": "lblFrom",
"isVisible": true,
"right": "5%",
"skin": "sknLblCurr",
"text": kony.i18n.getLocalizedString("i18n.common.fromc"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblFromDate = new kony.ui.Label({
"bottom": "0%",
"centerX": "50%",
"height": "48%",
"id": "lblFromDate",
"isVisible": true,
"skin": "sknLblWhike125",
"text": "From",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "28%",
"width": "95%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblLine2 = new kony.ui.Label({
"centerX": "50%",
"height": "2%",
"id": "lblLine2",
"isVisible": true,
"skin": "lblLine0h8b99d8a85f649",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "77%",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyflxEdit0f4588dd13b5546 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50%",
"id": "CopyflxEdit0f4588dd13b5546",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "2%",
"skin": "slFbox",
"top": "15dp",
"width": "53dp",
"zIndex": 1
}, {}, {});
CopyflxEdit0f4588dd13b5546.setDefaultUnit(kony.flex.DP);
var CopylblEdit0d6f6ef441ddc4e = new kony.ui.Label({
"height": "100%",
"id": "CopylblEdit0d6f6ef441ddc4e",
"isVisible": true,
"right": "0dp",
"skin": "sknLblEditDimmed",
"text": "T",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyflxEdit0f4588dd13b5546.add(CopylblEdit0d6f6ef441ddc4e);
var CalFrom = new kony.ui.Calendar({
"calendarIcon": "tran.png",
"dateComponents": [22, 11, 2018, 0, 0, 0],
"dateFormat": "MM/dd/yyyy",
"day": 22,
"focusSkin": "sknCarioRegular",
"formattedDate": "11/22/2018",
"height": "50%",
"hour": 0,
"id": "CalFrom",
"isVisible": true,
"right": "84.51%",
"minutes": 0,
"month": 11,
"onSelection": AS_Calendar_h74d563d409144b382240b5fef7ffd4e,
"seconds": 0,
"skin": "sknCarioRegular",
"top": "28%",
"viewType": constants.CALENDAR_VIEW_TYPE_DEFAULT,
"width": "12.49%",
"year": 2018,
"zIndex": 10
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxFrom.add(lblFrom, lblFromDate, lblLine2, CopyflxEdit0f4588dd13b5546, CalFrom);
var flxTo = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "13%",
"id": "flxTo",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "slFbox0f0c75590716b41",
"top": "2%",
"width": "100%",
"zIndex": 100
}, {}, {});
flxTo.setDefaultUnit(kony.flex.DP);
var lblTo = new kony.ui.Label({
"height": "35%",
"id": "lblTo",
"isVisible": true,
"right": "5%",
"skin": "sknLblCurr",
"text": kony.i18n.getLocalizedString("i18n.common.To"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblToDate = new kony.ui.Label({
"bottom": "0%",
"centerX": "50%",
"height": "48%",
"id": "lblToDate",
"isVisible": true,
"skin": "sknLblWhike125",
"text": "To",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "28%",
"width": "95%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblLine3 = new kony.ui.Label({
"centerX": "50%",
"height": "2%",
"id": "lblLine3",
"isVisible": true,
"skin": "lblLine0h8b99d8a85f649",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "77%",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyflxEdit0i01d4fe87ac647 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50%",
"id": "CopyflxEdit0i01d4fe87ac647",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "2%",
"skin": "slFbox",
"top": "15dp",
"width": "53dp",
"zIndex": 1
}, {}, {});
CopyflxEdit0i01d4fe87ac647.setDefaultUnit(kony.flex.DP);
var CopylblEdit0eb4fd815c50649 = new kony.ui.Label({
"height": "100%",
"id": "CopylblEdit0eb4fd815c50649",
"isVisible": true,
"right": "0dp",
"skin": "sknLblEditDimmed",
"text": "T",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyflxEdit0i01d4fe87ac647.add(CopylblEdit0eb4fd815c50649);
var CalTo = new kony.ui.Calendar({
"calendarIcon": "tran.png",
"dateComponents": ["14", "11", "2018"],
"dateFormat": "MM/dd/yyyy",
"day": 14,
"focusSkin": "sknCarioRegular",
"formattedDate": "11/14/2018",
"height": "50%",
"hour": 0,
"id": "CalTo",
"isVisible": true,
"right": "81.33%",
"minutes": 0,
"month": 11,
"onSelection": AS_Calendar_i83644f2115f4f0f9e735cf87f556d5e,
"seconds": 0,
"skin": "sknCarioRegular",
"top": "28%",
"viewType": constants.CALENDAR_VIEW_TYPE_DEFAULT,
"width": "16.00%",
"year": 2018,
"zIndex": 10
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxTo.add(lblTo, lblToDate, lblLine3, CopyflxEdit0i01d4fe87ac647, CalTo);
var flxFromContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "13%",
"id": "flxFromContainer",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 100
}, {}, {});
flxFromContainer.setDefaultUnit(kony.flex.DP);
var CopyflxBranch0c09769ba62da4e = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "CopyflxBranch0c09769ba62da4e",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "4%",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"onClick": AS_FlexContainer_bd6a58d54ec54b5b8553aebfefae7932,
"skin": "slFbox0f0c75590716b41",
"top": "1%",
"width": "40%",
"zIndex": 100
}, {}, {});
CopyflxBranch0c09769ba62da4e.setDefaultUnit(kony.flex.DP);
var lblYearFrom = new kony.ui.Label({
"height": "35%",
"id": "lblYearFrom",
"isVisible": true,
"right": "5%",
"skin": "sknLblCurr",
"text": kony.i18n.getLocalizedString("i18n.filtertransaction.year"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblYearFromValue = new kony.ui.Label({
"bottom": "0%",
"centerX": "50%",
"height": "48%",
"id": "lblYearFromValue",
"isVisible": true,
"skin": "sknLblWhike125",
"text": kony.i18n.getLocalizedString("i18n.filtertransaction.year"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "28%",
"width": "95%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblLineFY = new kony.ui.Label({
"centerX": "50%",
"height": "2%",
"id": "lblLineFY",
"isVisible": true,
"skin": "lblLine0h8b99d8a85f649",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "77%",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyflxEdit0a68ea5dea48344 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50%",
"id": "CopyflxEdit0a68ea5dea48344",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"skin": "slFbox",
"top": "15dp",
"width": "40dp",
"zIndex": 1
}, {}, {});
CopyflxEdit0a68ea5dea48344.setDefaultUnit(kony.flex.DP);
var CopylblEdit0j481c77f0cba48 = new kony.ui.Label({
"height": "100%",
"id": "CopylblEdit0j481c77f0cba48",
"isVisible": true,
"right": "0dp",
"skin": "sknLblEditDimmed",
"text": kony.i18n.getLocalizedString("i18n.common.backReverse"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyflxEdit0a68ea5dea48344.add(CopylblEdit0j481c77f0cba48);
CopyflxBranch0c09769ba62da4e.add(lblYearFrom, lblYearFromValue, lblLineFY, CopyflxEdit0a68ea5dea48344);
var CopyflxBranch0i21c7346d5554b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "CopyflxBranch0i21c7346d5554b",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "52%",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"onClick": AS_FlexContainer_c105de35dcfa47e4953a274cca5cb3d3,
"skin": "slFbox0f0c75590716b41",
"top": "1%",
"width": "40%",
"zIndex": 100
}, {}, {});
CopyflxBranch0i21c7346d5554b.setDefaultUnit(kony.flex.DP);
var lblMonthFrom = new kony.ui.Label({
"height": "35%",
"id": "lblMonthFrom",
"isVisible": true,
"right": "5%",
"skin": "sknLblCurr",
"text": kony.i18n.getLocalizedString("i18n.common.month"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblMonthFromValue = new kony.ui.Label({
"bottom": "0%",
"centerX": "50%",
"height": "48%",
"id": "lblMonthFromValue",
"isVisible": true,
"skin": "sknLblWhike125",
"text": kony.i18n.getLocalizedString("i18n.common.month"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "28%",
"width": "95%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblLineFM = new kony.ui.Label({
"centerX": "50%",
"height": "2%",
"id": "lblLineFM",
"isVisible": true,
"skin": "lblLine0h8b99d8a85f649",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "77%",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyflxEdit0b8438875f3a14b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50%",
"id": "CopyflxEdit0b8438875f3a14b",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"skin": "slFbox",
"top": "15dp",
"width": "40dp",
"zIndex": 1
}, {}, {});
CopyflxEdit0b8438875f3a14b.setDefaultUnit(kony.flex.DP);
var CopylblEdit0e208d59f086449 = new kony.ui.Label({
"height": "100%",
"id": "CopylblEdit0e208d59f086449",
"isVisible": true,
"right": "0dp",
"skin": "sknLblEditDimmed",
"text": kony.i18n.getLocalizedString("i18n.common.backReverse"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyflxEdit0b8438875f3a14b.add(CopylblEdit0e208d59f086449);
CopyflxBranch0i21c7346d5554b.add(lblMonthFrom, lblMonthFromValue, lblLineFM, CopyflxEdit0b8438875f3a14b);
flxFromContainer.add(CopyflxBranch0c09769ba62da4e, CopyflxBranch0i21c7346d5554b);
var lblToTitle = new kony.ui.Label({
"id": "lblToTitle",
"isVisible": true,
"right": "5%",
"skin": "sknLblCurr",
"text": kony.i18n.getLocalizedString("i18n.common.To"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "3%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxToContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "13%",
"id": "flxToContainer",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 100
}, {}, {});
flxToContainer.setDefaultUnit(kony.flex.DP);
var CopyflxBranch0a011c2467ce84e = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "CopyflxBranch0a011c2467ce84e",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "4%",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"onClick": AS_FlexContainer_b32c6059d6014401bd57f020787d3968,
"skin": "slFbox0f0c75590716b41",
"top": "1%",
"width": "40%",
"zIndex": 100
}, {}, {});
CopyflxBranch0a011c2467ce84e.setDefaultUnit(kony.flex.DP);
var lblYearTo = new kony.ui.Label({
"height": "35%",
"id": "lblYearTo",
"isVisible": true,
"right": "5%",
"skin": "sknLblCurr",
"text": kony.i18n.getLocalizedString("i18n.filtertransaction.year"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblYearToValue = new kony.ui.Label({
"bottom": "0%",
"centerX": "50%",
"height": "48%",
"id": "lblYearToValue",
"isVisible": true,
"skin": "sknLblWhike125",
"text": kony.i18n.getLocalizedString("i18n.filtertransaction.year"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "28%",
"width": "95%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblLineTY = new kony.ui.Label({
"centerX": "50%",
"height": "2%",
"id": "lblLineTY",
"isVisible": true,
"skin": "lblLine0h8b99d8a85f649",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "77%",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyflxEdit0hb0b687ec41346 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50%",
"id": "CopyflxEdit0hb0b687ec41346",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"skin": "slFbox",
"top": "15dp",
"width": "40dp",
"zIndex": 1
}, {}, {});
CopyflxEdit0hb0b687ec41346.setDefaultUnit(kony.flex.DP);
var CopylblEdit0aa2a9da5454c43 = new kony.ui.Label({
"height": "100%",
"id": "CopylblEdit0aa2a9da5454c43",
"isVisible": true,
"right": "0dp",
"skin": "sknLblEditDimmed",
"text": kony.i18n.getLocalizedString("i18n.common.backReverse"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyflxEdit0hb0b687ec41346.add(CopylblEdit0aa2a9da5454c43);
CopyflxBranch0a011c2467ce84e.add(lblYearTo, lblYearToValue, lblLineTY, CopyflxEdit0hb0b687ec41346);
var CopyflxBranch0e78ace8f76eb41 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "CopyflxBranch0e78ace8f76eb41",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "52%",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"onClick": AS_FlexContainer_ecdccdab719f44509bb637db78c72e99,
"skin": "slFbox0f0c75590716b41",
"top": "1%",
"width": "40%",
"zIndex": 100
}, {}, {});
CopyflxBranch0e78ace8f76eb41.setDefaultUnit(kony.flex.DP);
var lblMonthTo = new kony.ui.Label({
"height": "35%",
"id": "lblMonthTo",
"isVisible": true,
"right": "5%",
"skin": "sknLblCurr",
"text": kony.i18n.getLocalizedString("i18n.common.month"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblMonthToValue = new kony.ui.Label({
"bottom": "0%",
"centerX": "50%",
"height": "48%",
"id": "lblMonthToValue",
"isVisible": true,
"skin": "sknLblWhike125",
"text": kony.i18n.getLocalizedString("i18n.common.month"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "28%",
"width": "95%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblLineTM = new kony.ui.Label({
"centerX": "50%",
"height": "2%",
"id": "lblLineTM",
"isVisible": true,
"skin": "lblLine0h8b99d8a85f649",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "77%",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyflxEdit0caabf91b982442 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50%",
"id": "CopyflxEdit0caabf91b982442",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"skin": "slFbox",
"top": "15dp",
"width": "40dp",
"zIndex": 1
}, {}, {});
CopyflxEdit0caabf91b982442.setDefaultUnit(kony.flex.DP);
var CopylblEdit0jcc6b334e4164f = new kony.ui.Label({
"height": "100%",
"id": "CopylblEdit0jcc6b334e4164f",
"isVisible": true,
"right": "0dp",
"skin": "sknLblEditDimmed",
"text": kony.i18n.getLocalizedString("i18n.common.backReverse"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyflxEdit0caabf91b982442.add(CopylblEdit0jcc6b334e4164f);
CopyflxBranch0e78ace8f76eb41.add(lblMonthTo, lblMonthToValue, lblLineTM, CopyflxEdit0caabf91b982442);
flxToContainer.add(CopyflxBranch0a011c2467ce84e, CopyflxBranch0e78ace8f76eb41);
var lblModeOfBranch = new kony.ui.Label({
"id": "lblModeOfBranch",
"isVisible": true,
"right": "5%",
"skin": "sknLblCurr",
"text": kony.i18n.getLocalizedString("i18n.accounts.deliveryMode"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "3%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxRadioDeliveryMode = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "70dp",
"id": "flxRadioDeliveryMode",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "0%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxRadioDeliveryMode.setDefaultUnit(kony.flex.DP);
var RadioBtnAccCards = new kony.ui.RadioButtonGroup({
"centerY": "65%",
"height": "60%",
"id": "RadioBtnAccCards",
"isVisible": false,
"right": "2%",
"masterData": [["2", kony.i18n.getLocalizedString("i18n.billsPay.Cards")],["1", kony.i18n.getLocalizedString("i18n.billsPay.Accounts")]],
"onSelection": AS_RadioButtonGroup_d1c7211917384d56b761b94b289b4fdb,
"selectedKey": "1",
"selectedKeyValue": ["1", "Accounts"],
"skin": "CopyslRadioButtonGroup0b4da25f7d5c746",
"top": "17dp",
"width": "90%",
"zIndex": 1
}, {
"itemOrientation": constants.RADIOGROUP_ITEM_ORIENTATION_HORIZONTAL,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var lblBranch = new kony.ui.Label({
"centerY": "50%",
"id": "lblBranch",
"isVisible": true,
"right": "15%",
"skin": "sknTransferType",
"text": kony.i18n.getLocalizedString("i18n.locateus.branch"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "20%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var btnMail = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "sknRDOWhiteBRDBOJFont",
"id": "btnMail",
"isVisible": true,
"right": "48%",
"onClick": AS_Button_f8e4740106f5460a933d4f839a7c13e7,
"skin": "sknRDOWhiteBRDBOJFont",
"text": "s",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var lblMailAddress = new kony.ui.Label({
"centerY": "50%",
"id": "lblMailAddress",
"isVisible": true,
"right": "58%",
"skin": "sknTransferType",
"text": kony.i18n.getLocalizedString("i18n.accounts.mailAddress"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var btnBranch = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "sknRDOWhiteBRDBOJFont",
"id": "btnBranch",
"isVisible": true,
"right": "5%",
"onClick": AS_Button_ja2875f63f7c4ebd93502e6fb2f68092,
"skin": "sknRDOWhiteBRDBOJFont",
"text": "t",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxRadioDeliveryMode.add(RadioBtnAccCards, lblBranch, btnMail, lblMailAddress, btnBranch);
var flxBranch = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "13%",
"id": "flxBranch",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"onClick": AS_FlexContainer_j32fe18e3b934aeb88d60f65a07dd91d,
"skin": "slFbox0f0c75590716b41",
"top": "2%",
"width": "100%",
"zIndex": 100
}, {}, {});
flxBranch.setDefaultUnit(kony.flex.DP);
var lblCurrHead = new kony.ui.Label({
"height": "35%",
"id": "lblCurrHead",
"isVisible": true,
"right": "5%",
"skin": "sknLblCurr",
"text": kony.i18n.getLocalizedString("i18n.Map.Branchname"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBranchValue = new kony.ui.Label({
"bottom": "0%",
"centerX": "50%",
"height": "48%",
"id": "lblBranchValue",
"isVisible": true,
"skin": "sknLblWhike125",
"text": kony.i18n.getLocalizedString("i18n.Map.Branchname"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "28%",
"width": "95%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lbllinee1 = new kony.ui.Label({
"centerX": "50%",
"height": "2%",
"id": "lbllinee1",
"isVisible": true,
"skin": "lblLine0h8b99d8a85f649",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "77%",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyflxEdit0e3e34b1bf78049 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50%",
"id": "CopyflxEdit0e3e34b1bf78049",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"skin": "slFbox",
"top": "15dp",
"width": "53dp",
"zIndex": 1
}, {}, {});
CopyflxEdit0e3e34b1bf78049.setDefaultUnit(kony.flex.DP);
var CopylblEdit0b4a3f2847e3d44 = new kony.ui.Label({
"height": "100%",
"id": "CopylblEdit0b4a3f2847e3d44",
"isVisible": true,
"right": "0dp",
"skin": "sknLblEditDimmed",
"text": kony.i18n.getLocalizedString("i18n.common.backReverse"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyflxEdit0e3e34b1bf78049.add(CopylblEdit0b4a3f2847e3d44);
flxBranch.add(lblCurrHead, lblBranchValue, lbllinee1, CopyflxEdit0e3e34b1bf78049);
var flxAddressMail = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "70dp",
"id": "flxAddressMail",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxAddressMail.setDefaultUnit(kony.flex.DP);
var lblAddressMail = new kony.ui.Label({
"id": "lblAddressMail",
"isVisible": true,
"right": "2%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.common.address"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var txtAddressMail = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "txtAddressMail",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": 75,
"onDone": AS_TextField_becbec46852b42c1b8564238ffe6bd02,
"onTextChange": AS_TextField_d2c6e07db35f442da0a8216872d756cf,
"onTouchEnd": AS_TextField_ed09446bab5f4124865def3096aafbbf,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
"onEndEditing": AS_TextField_i83910330d114f72a4a7876cd49666c3,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlineAddressMail = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlineAddressMail",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlineAddressMail.setDefaultUnit(kony.flex.DP);
flxUnderlineAddressMail.add();
flxAddressMail.add(lblAddressMail, txtAddressMail, flxUnderlineAddressMail);
var lblFromService = new kony.ui.Label({
"height": "1px",
"id": "lblFromService",
"isVisible": false,
"right": "100%",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1px",
"width": "1px",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblToService = new kony.ui.Label({
"height": "1px",
"id": "lblToService",
"isVisible": false,
"right": "100%",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1px",
"width": "1px",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBranchCode = new kony.ui.Label({
"height": "1px",
"id": "lblBranchCode",
"isVisible": false,
"right": "100%",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1px",
"width": "1px",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblAccBranch = new kony.ui.Label({
"height": "1px",
"id": "lblAccBranch",
"isVisible": false,
"right": "100%",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1px",
"width": "1px",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblFromDateHidee = new kony.ui.Label({
"height": "0px",
"id": "lblFromDateHidee",
"isVisible": true,
"right": "0dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "0px",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblToDateHidee = new kony.ui.Label({
"height": "0px",
"id": "lblToDateHidee",
"isVisible": true,
"right": "0dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "0px",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxScrllMain.add(flxAccountNumber, lblFromTitle, flxFrom, flxTo, flxFromContainer, lblToTitle, flxToContainer, lblModeOfBranch, flxRadioDeliveryMode, flxBranch, flxAddressMail, lblFromService, lblToService, lblBranchCode, lblAccBranch, lblFromDateHidee, lblToDateHidee);
frmRequestStatementAccounts.add(flxHeader, flxScrllMain);
};
function frmRequestStatementAccountsGlobalsAr() {
frmRequestStatementAccountsAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmRequestStatementAccountsAr,
"enabledForIdleTimeout": true,
"id": "frmRequestStatementAccounts",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"skin": "slFormCommon"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"retainScrollPosition": false,
"titleBar": false,
"titleBarSkin": "slTitleBar",
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
