//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmCurrencyAr() {
frmCurrency.setDefaultUnit(kony.flex.DP);
var flxHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "14%",
"id": "flxHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "CopyslFbox0e46317489b7742",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxHeader.setDefaultUnit(kony.flex.DP);
var flxSearch = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0%",
"clipBounds": true,
"height": "49%",
"id": "flxSearch",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknFlxSearch",
"width": "100%",
"zIndex": 2
}, {}, {});
flxSearch.setDefaultUnit(kony.flex.DP);
var flxSearchHolder = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"centerY": "50%",
"clipBounds": true,
"height": "96%",
"id": "flxSearchHolder",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"skin": "slFbox",
"top": "0%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxSearchHolder.setDefaultUnit(kony.flex.DP);
var lblSearchImg = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblSearchImg",
"isVisible": true,
"right": "0%",
"skin": "sknSearchIcon",
"text": "h",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "8%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var tbxSearch = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"centerY": "50%",
"focusSkin": "sknTxtBoxSearch",
"height": "100%",
"id": "tbxSearch",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "0%",
"onDone": AS_TextField_a342c4af65c346e7af7cb96f412e05c9,
"onTextChange": AS_TextField_hbd74757e819405bb57337a5c5630d32,
"placeholder": kony.i18n.getLocalizedString("i18n.transfer.searchPlh"),
"secureTextEntry": false,
"skin": "sknTxtBoxSearch",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "92%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
"placeholderSkin": "sknPlaceHolder",
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var lblClose = new kony.ui.Label({
"height": "100%",
"id": "lblClose",
"isVisible": false,
"right": "-10%",
"onTouchEnd": AS_Label_a96d27113db84dc8b7d314979e71cc9e,
"skin": "sknClose",
"text": "O",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "10%",
"zIndex": 3
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxSearchHolder.add( lblClose, tbxSearch,lblSearchImg);
var flxUnderLineSearch = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "3%",
"id": "flxUnderLineSearch",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "90%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxUnderLineSearch.setDefaultUnit(kony.flex.DP);
flxUnderLineSearch.add();
flxSearch.add(flxSearchHolder, flxUnderLineSearch);
var flxHeadMain1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50%",
"id": "flxHeadMain1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxHeadMain1.setDefaultUnit(kony.flex.DP);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "0%",
"onTouchStart": AS_FlexContainer_cb725390932b40afa3cc68d2dc940393,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 1
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBack = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "login screen"
},
"centerY": "51.84%",
"id": "lblBack",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBack.add(lblBackIcon, lblBack);
var btnSearch = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "slIconFocus",
"height": "100%",
"id": "btnSearch",
"isVisible": false,
"right": "0%",
"skin": "slIcon",
"text": "h",
"top": "0dp",
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
var lblTitle = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"height": "100%",
"id": "lblTitle",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18.Transfer.chooseCurrency"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxHeadMain1.add(flxBack, btnSearch, lblTitle);
flxHeader.add(flxSearch, flxHeadMain1);
var flxCurrency = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "86%",
"id": "flxCurrency",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknFlxBackgrounf",
"top": "-1%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxCurrency.setDefaultUnit(kony.flex.DP);
var segCurrency = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"data": [{
"CopylblCurrency0c4373e11aa8641": "",
"CopylblDescriptionAr0ca1638a7c3ee4b": "",
"imgCountryIcon": "",
"imgTickIcon": "",
"lblCurrCode": "",
"lblCurrency": "",
"lblDecimal": "",
"lblDescriptionAr": "",
"lblDescriptionEn": "",
"lblMobileCountry": ""
}],
"groupCells": false,
"height": "100%",
"id": "segCurrency",
"isVisible": true,
"right": "0dp",
"needPageIndicator": true,
"onRowClick": AS_Segment_fe32acaba15741509c67abb3e766d4f8,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "seg2Focus",
"rowSkin": "Copyseg0e6ede8359e2f45",
"rowTemplate": tmpFlxCurrency,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_SINGLE_SELECT_BEHAVIOR,
"selectionBehaviorConfig": {
"imageIdentifier": "imgTickIcon",
"selectedStateImage": "tick.png",
"unselectedStateImage": "tran.png"
},
"separatorColor": "023a6900",
"separatorRequired": true,
"separatorThickness": 1,
"showScrollbars": false,
"top": "0dp",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"CopylblCurrency0c4373e11aa8641": "CopylblCurrency0c4373e11aa8641",
"CopylblDescriptionAr0ca1638a7c3ee4b": "CopylblDescriptionAr0ca1638a7c3ee4b",
"flxCountryIcon": "flxCountryIcon",
"flxCurrencydetails": "flxCurrencydetails",
"imgCountryIcon": "imgCountryIcon",
"imgTickIcon": "imgTickIcon",
"lblCurrCode": "lblCurrCode",
"lblCurrency": "lblCurrency",
"lblDecimal": "lblDecimal",
"lblDescriptionAr": "lblDescriptionAr",
"lblDescriptionEn": "lblDescriptionEn",
"lblMobileCountry": "lblMobileCountry",
"tmpFlxCurrency": "tmpFlxCurrency"
},
"width": "100%",
"zIndex": 1
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxCurrency.add(segCurrency);
frmCurrency.add(flxHeader, flxCurrency);
};
function frmCurrencyGlobalsAr() {
frmCurrencyAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmCurrencyAr,
"enabledForIdleTimeout": true,
"id": "frmCurrency",
"layoutType": kony.flex.FLOW_VERTICAL,
"needAppMenu": false,
"preShow": AS_Form_baa193bbe45e4e41bf0e36abc4258a68,
"skin": "sknSuccessBkg"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FLOW_VERTICAL,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_e19a8a0da531422685eb2d7dc36c4075,
"retainScrollPosition": false,
"titleBar": true,
"titleBarSkin": "slTitleBar",
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
