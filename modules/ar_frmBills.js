//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmBillsAr() {
frmBills.setDefaultUnit(kony.flex.DP);
var flxHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxHeader",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"left": "0dp",
"skin": "slFlxHeaderImg",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxHeader.setDefaultUnit(kony.flex.DP);
var flxTop = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxTop",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxTop.setDefaultUnit(kony.flex.DP);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_eecc5906245b42c2b21f664d28da79bd,
"skin": "slFbox",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBack = new kony.ui.Label({
"centerY": "50%",
"id": "lblBack",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBack.add(lblBackIcon, lblBack);
var lblTitle = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"height": "70%",
"id": "lblTitle",
"isVisible": true,
"maxNumberOfLines": 1,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.billsPay.PostPaid"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblClose = new kony.ui.Label({
"centerY": "50%",
"id": "lblClose",
"isVisible": true,
"left": "90%",
"onTouchEnd": AS_FlexContainer_eecc5906245b42c2b21f664d28da79bd,
"right": 0,
"skin": "sknPostpaidClose",
"text": "O",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxTop.add(flxBack, lblTitle, lblClose);
flxHeader.add(flxTop);
var flxMain = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": true,
"clipBounds": true,
"enableScrolling": true,
"height": "90%",
"horizontalScrollIndicator": true,
"id": "flxMain",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0.08%",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "0.00%",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
flxMain.setDefaultUnit(kony.flex.DP);
var flxBiller = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "18%",
"id": "flxBiller",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxBiller.setDefaultUnit(kony.flex.DP);
var flxIcon1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "flxIcon1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "sknFlxToIcon",
"top": "20%",
"width": "50dp",
"zIndex": 1
}, {}, {});
flxIcon1.setDefaultUnit(kony.flex.DP);
var lblInitial = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"height": "80%",
"id": "lblInitial",
"isVisible": true,
"skin": "sknLblFromIcon",
"text": "BH",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxIcon1.add(lblInitial);
var flxDetail = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxDetail",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "2%",
"skin": "slFbox",
"top": "0dp",
"width": "60%",
"zIndex": 1
}, {}, {});
flxDetail.setDefaultUnit(kony.flex.DP);
var lblName = new kony.ui.Label({
"id": "lblName",
"isVisible": true,
"right": "0dp",
"skin": "slBillerDetailMain",
"text": "Nick Name",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "15%",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblType = new kony.ui.Label({
"id": "lblType",
"isVisible": true,
"right": "0dp",
"skin": "slBillerType",
"text": "Prepaid",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblAccountNumber = new kony.ui.Label({
"id": "lblAccountNumber",
"isVisible": true,
"right": "0dp",
"skin": "sknLblAccNumBiller",
"text": "48574839485",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxDetail.add(lblName, lblType, lblAccountNumber);
var btnDelete = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "sknBtnBack",
"height": "50%",
"id": "btnDelete",
"isVisible": false,
"right": "0dp",
"skin": "sknBtnBack",
"text": "w",
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxBiller.add( btnDelete, flxDetail,flxIcon1);
var flxAmounttoPay = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "70dp",
"id": "flxAmounttoPay",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxAmounttoPay.setDefaultUnit(kony.flex.DP);
var CopylblUnderline0gd2e5381440849 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "CopylblUnderline0gd2e5381440849",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
CopylblUnderline0gd2e5381440849.setDefaultUnit(kony.flex.DP);
CopylblUnderline0gd2e5381440849.add();
var CopylblAmount0e814d8334df04b = new kony.ui.Label({
"height": "40%",
"id": "CopylblAmount0e814d8334df04b",
"isVisible": true,
"right": "0%",
"skin": "sknLblSmall",
"text": kony.i18n.getLocalizedString("i18n.bills.AmountToPay"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "4%",
"width": "60%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyLabel0f9c9c65c610040 = new kony.ui.Label({
"bottom": "4%",
"centerX": "50%",
"height": "60%",
"id": "CopyLabel0f9c9c65c610040",
"isVisible": true,
"skin": "sknLblBack",
"text": "100 JOD",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxAmounttoPay.add(CopylblUnderline0gd2e5381440849, CopylblAmount0e814d8334df04b, CopyLabel0f9c9c65c610040);
var flxDenomination = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "70dp",
"id": "flxDenomination",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxDenomination.setDefaultUnit(kony.flex.DP);
var CopytbxPaymentMode0i4ecde4ad00143 = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "CopytbxPaymentMode0i4ecde4ad00143",
"isVisible": false,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": null,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var CopyflxUnderlinePaymentMode0a280b3b95eca4f = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "CopyflxUnderlinePaymentMode0a280b3b95eca4f",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
CopyflxUnderlinePaymentMode0a280b3b95eca4f.setDefaultUnit(kony.flex.DP);
CopyflxUnderlinePaymentMode0a280b3b95eca4f.add();
var CopyflxPaymentModeTypeHolder0c5063e5b4bbd4b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "4%",
"centerX": "50%",
"clipBounds": true,
"height": "60%",
"id": "CopyflxPaymentModeTypeHolder0c5063e5b4bbd4b",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"width": "100%",
"zIndex": 2
}, {}, {});
CopyflxPaymentModeTypeHolder0c5063e5b4bbd4b.setDefaultUnit(kony.flex.DP);
var CopylblPaymentMode0dab2bf4586514d = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "CopylblPaymentMode0dab2bf4586514d",
"isVisible": true,
"right": "2%",
"maxNumberOfLines": 1,
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.billsPay.Denomination"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "85%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblArrowPaymentMode0eded606d0a6e43 = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblArrowPaymentMode0eded606d0a6e43",
"isVisible": true,
"right": "90%",
"skin": "sknBackIconDisabled",
"text": "o",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyflxPaymentModeTypeHolder0c5063e5b4bbd4b.add(CopylblPaymentMode0dab2bf4586514d, CopylblArrowPaymentMode0eded606d0a6e43);
flxDenomination.add(CopytbxPaymentMode0i4ecde4ad00143, CopyflxUnderlinePaymentMode0a280b3b95eca4f, CopyflxPaymentModeTypeHolder0c5063e5b4bbd4b);
var flxDueAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "70dp",
"id": "flxDueAmount",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxDueAmount.setDefaultUnit(kony.flex.DP);
var CopylblUnderline0ee4cc246b9414f = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "CopylblUnderline0ee4cc246b9414f",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
CopylblUnderline0ee4cc246b9414f.setDefaultUnit(kony.flex.DP);
CopylblUnderline0ee4cc246b9414f.add();
var CopylblAmount0d113905789c54b = new kony.ui.Label({
"height": "40%",
"id": "CopylblAmount0d113905789c54b",
"isVisible": true,
"right": "0%",
"skin": "sknLblSmall",
"text": kony.i18n.getLocalizedString("i18n.bills.DueAmount"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "4%",
"width": "60%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var dueAmount = new kony.ui.Label({
"bottom": "4%",
"centerX": "50%",
"height": "60%",
"id": "dueAmount",
"isVisible": true,
"skin": "sknLblBack",
"text": "100 JOD",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxDueAmount.add(CopylblUnderline0ee4cc246b9414f, CopylblAmount0d113905789c54b, dueAmount);
var flxFee = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "70dp",
"id": "flxFee",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxFee.setDefaultUnit(kony.flex.DP);
var CopylblUnderline0b471e15b753447 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "CopylblUnderline0b471e15b753447",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
CopylblUnderline0b471e15b753447.setDefaultUnit(kony.flex.DP);
CopylblUnderline0b471e15b753447.add();
var CopylblAmount0g0aae2d6dfcf41 = new kony.ui.Label({
"height": "40%",
"id": "CopylblAmount0g0aae2d6dfcf41",
"isVisible": true,
"right": "0%",
"skin": "sknLblSmall",
"text": kony.i18n.getLocalizedString("i18n.bills.FeeAmount"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "4%",
"width": "60%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblFee = new kony.ui.Label({
"bottom": "4%",
"centerX": "50%",
"height": "60%",
"id": "lblFee",
"isVisible": true,
"skin": "sknLblBack",
"text": "100 JOD",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxFee.add(CopylblUnderline0b471e15b753447, CopylblAmount0g0aae2d6dfcf41, lblFee);
var flxMinimumAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "70dp",
"id": "flxMinimumAmount",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxMinimumAmount.setDefaultUnit(kony.flex.DP);
var CopylblUnderline0b0cbe98610e145 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "CopylblUnderline0b0cbe98610e145",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
CopylblUnderline0b0cbe98610e145.setDefaultUnit(kony.flex.DP);
CopylblUnderline0b0cbe98610e145.add();
var CopylblAmount0a1087682002a43 = new kony.ui.Label({
"height": "40%",
"id": "CopylblAmount0a1087682002a43",
"isVisible": true,
"right": "0%",
"skin": "sknLblSmall",
"text": kony.i18n.getLocalizedString("i18n.bills.MinimumAmount"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "4%",
"width": "60%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var minAmount = new kony.ui.Label({
"bottom": "4%",
"centerX": "50%",
"height": "60%",
"id": "minAmount",
"isVisible": true,
"skin": "sknLblBack",
"text": "50 JOD",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxMinimumAmount.add(CopylblUnderline0b0cbe98610e145, CopylblAmount0a1087682002a43, minAmount);
var flxMaxAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "70dp",
"id": "flxMaxAmount",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxMaxAmount.setDefaultUnit(kony.flex.DP);
var CopylblUnderline0d7783036f95f49 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "CopylblUnderline0d7783036f95f49",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
CopylblUnderline0d7783036f95f49.setDefaultUnit(kony.flex.DP);
CopylblUnderline0d7783036f95f49.add();
var CopylblAmount0j38f26fb347840 = new kony.ui.Label({
"height": "40%",
"id": "CopylblAmount0j38f26fb347840",
"isVisible": true,
"right": "0%",
"skin": "sknLblSmall",
"text": kony.i18n.getLocalizedString("i18n.bills.MaximumAmount"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "4%",
"width": "60%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var maxAmount = new kony.ui.Label({
"bottom": "4%",
"centerX": "50%",
"height": "60%",
"id": "maxAmount",
"isVisible": true,
"skin": "sknLblBack",
"text": "1000 JOD",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxMaxAmount.add(CopylblUnderline0d7783036f95f49, CopylblAmount0j38f26fb347840, maxAmount);
var flxTotalAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "70dp",
"id": "flxTotalAmount",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxTotalAmount.setDefaultUnit(kony.flex.DP);
var CopylblUnderline0f31f9b0e5f874b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "CopylblUnderline0f31f9b0e5f874b",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
CopylblUnderline0f31f9b0e5f874b.setDefaultUnit(kony.flex.DP);
CopylblUnderline0f31f9b0e5f874b.add();
var CopylblAmount0a7a932d2c0274f = new kony.ui.Label({
"height": "40%",
"id": "CopylblAmount0a7a932d2c0274f",
"isVisible": true,
"right": "0%",
"skin": "sknLblSmall",
"text": kony.i18n.getLocalizedString("i18n.bills.TotalAmount"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "4%",
"width": "60%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyLabel0ia49bbb23e7542 = new kony.ui.Label({
"bottom": "4%",
"centerX": "50%",
"height": "60%",
"id": "CopyLabel0ia49bbb23e7542",
"isVisible": true,
"skin": "sknLblBack",
"text": "1000 JOD",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxTotalAmount.add(CopylblUnderline0f31f9b0e5f874b, CopylblAmount0a7a932d2c0274f, CopyLabel0ia49bbb23e7542);
var flxIssueDate = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "70dp",
"id": "flxIssueDate",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxIssueDate.setDefaultUnit(kony.flex.DP);
var CopylblUnderline0h42214aa95cf47 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "CopylblUnderline0h42214aa95cf47",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
CopylblUnderline0h42214aa95cf47.setDefaultUnit(kony.flex.DP);
CopylblUnderline0h42214aa95cf47.add();
var CopylblAmount0ac5f672e94704f = new kony.ui.Label({
"height": "40%",
"id": "CopylblAmount0ac5f672e94704f",
"isVisible": true,
"right": "0%",
"skin": "sknLblSmall",
"text": kony.i18n.getLocalizedString("i18n.bills.IssueDate"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "4%",
"width": "60%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var issueDate = new kony.ui.Label({
"bottom": "4%",
"centerX": "50%",
"height": "60%",
"id": "issueDate",
"isVisible": true,
"skin": "sknLblBack",
"text": "10/02/2018",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxIssueDate.add(CopylblUnderline0h42214aa95cf47, CopylblAmount0ac5f672e94704f, issueDate);
var flxDueDate = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "70dp",
"id": "flxDueDate",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxDueDate.setDefaultUnit(kony.flex.DP);
var CopylblUnderline0ffe9b584dc6740 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "CopylblUnderline0ffe9b584dc6740",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
CopylblUnderline0ffe9b584dc6740.setDefaultUnit(kony.flex.DP);
CopylblUnderline0ffe9b584dc6740.add();
var CopylblAmount0b12d96472f3047 = new kony.ui.Label({
"height": "40%",
"id": "CopylblAmount0b12d96472f3047",
"isVisible": true,
"right": "0%",
"skin": "sknLblSmall",
"text": kony.i18n.getLocalizedString("i18n.bills.DueDate"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "4%",
"width": "60%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var dueDate = new kony.ui.Label({
"bottom": "4%",
"centerX": "50%",
"height": "60%",
"id": "dueDate",
"isVisible": true,
"skin": "sknLblBack",
"text": "10/02/2018",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxDueDate.add(CopylblUnderline0ffe9b584dc6740, CopylblAmount0b12d96472f3047, dueDate);
var flxRadioAccCardsSelection = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "70dp",
"id": "flxRadioAccCardsSelection",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxRadioAccCardsSelection.setDefaultUnit(kony.flex.DP);
var RadioBtnAccCards = new kony.ui.RadioButtonGroup({
"centerY": "65%",
"height": "60%",
"id": "RadioBtnAccCards",
"isVisible": false,
"right": "2%",
"masterData": [["2", kony.i18n.getLocalizedString("i18n.billsPay.Cards")],["1", kony.i18n.getLocalizedString("i18n.billsPay.Accounts")]],
"onSelection": AS_RadioButtonGroup_da3c8ac9ba9f465e811cefe16eaa7724,
"selectedKey": "1",
"selectedKeyValue": ["1", "Accounts"],
"skin": "CopyslRadioButtonGroup0b4da25f7d5c746",
"top": "17dp",
"width": "90%",
"zIndex": 1
}, {
"itemOrientation": constants.RADIOGROUP_ITEM_ORIENTATION_HORIZONTAL,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var lblBillsPayAccounts = new kony.ui.Label({
"centerY": "50%",
"id": "lblBillsPayAccounts",
"isVisible": true,
"right": "15%",
"skin": "sknTransferType",
"text": kony.i18n.getLocalizedString("i18n.billsPay.Accounts"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var btnBillsPayCards = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "sknRDOWhiteBRDBOJFont",
"id": "btnBillsPayCards",
"isVisible": false,
"right": "55%",
"onClick": AS_Button_e7043ed396ff42a7af392cc5d734ab9f,
"skin": "sknRDOWhiteBRDBOJFont",
"text": "s",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var lblBillsPayCards = new kony.ui.Label({
"centerY": "50%",
"id": "lblBillsPayCards",
"isVisible": false,
"right": "65%",
"skin": "sknTransferType",
"text": kony.i18n.getLocalizedString("i18n.billsPay.Cards"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var btnBillsPayAccounts = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "sknRDOWhiteBRDBOJFont",
"id": "btnBillsPayAccounts",
"isVisible": true,
"right": "5%",
"onClick": AS_Button_bda7f1237fc744beae5127652088db16,
"skin": "sknRDOWhiteBRDBOJFont",
"text": "t",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxRadioAccCardsSelection.add(RadioBtnAccCards, lblBillsPayAccounts, btnBillsPayCards, lblBillsPayCards, btnBillsPayAccounts);
var flxPaymentMode = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "70dp",
"id": "flxPaymentMode",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "0%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxPaymentMode.setDefaultUnit(kony.flex.DP);
var tbxPaymentMode = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxPaymentMode",
"isVisible": false,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"maxTextLength": null,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "100%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var flxUnderlinePaymentMode = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderlinePaymentMode",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "95%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxUnderlinePaymentMode.setDefaultUnit(kony.flex.DP);
flxUnderlinePaymentMode.add();
var flxPaymentModeTypeHolder = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "4%",
"centerX": "50%",
"clipBounds": true,
"height": "60%",
"id": "flxPaymentModeTypeHolder",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_j90b35c5a4524e17a41dc98e0d4a00b2,
"skin": "slFbox",
"width": "100%",
"zIndex": 2
}, {}, {});
flxPaymentModeTypeHolder.setDefaultUnit(kony.flex.DP);
var lblPaymentMode = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblPaymentMode",
"isVisible": true,
"right": "2%",
"maxNumberOfLines": 1,
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.billsPay.Accounts"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "85%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblArrowPaymentMode = new kony.ui.Label({
"centerY": "50%",
"id": "lblArrowPaymentMode",
"isVisible": true,
"right": "90%",
"skin": "sknBackIconDisabled",
"text": "o",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxPaymentModeTypeHolder.add(lblPaymentMode, lblArrowPaymentMode);
flxPaymentMode.add(tbxPaymentMode, flxUnderlinePaymentMode, flxPaymentModeTypeHolder);
var flxAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90dp",
"id": "flxAmount",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "0%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxAmount.setDefaultUnit(kony.flex.DP);
var tbxAmount = new kony.ui.TextBox2({
"accessibilityConfig": {
"a11yLabel": "Enter your Username"
},
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "4%",
"centerX": "50%",
"focusSkin": "sknTxtBox",
"height": "60%",
"id": "tbxAmount",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DECIMAL,
"maxTextLength": 13,
"onDone": AS_TextField_h004f54d77c84f35b3e36d2809aa27f3,
"onTextChange": AS_TextField_i1377425d0c344f1b4a3d571a1f60ee6,
"onTouchStart": AS_TextField_f317c3e80a974cc2891a50fa44508762,
"secureTextEntry": false,
"skin": "sknTxtBox",
"textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
"width": "100%",
"zIndex": 2
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
"onEndEditing": AS_TextField_d2b8bcb35ad047ce8e8f04135eaad3ff,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var lblUnderline = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "lblUnderline",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "80%",
"width": "100%",
"zIndex": 1
}, {}, {});
lblUnderline.setDefaultUnit(kony.flex.DP);
lblUnderline.add();
var lblAmount = new kony.ui.Label({
"id": "lblAmount",
"isVisible": true,
"right": "2%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.billsPay.Amount"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxConversionAmt = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"id": "flxConversionAmt",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "68%",
"zIndex": 1
}, {}, {});
flxConversionAmt.setDefaultUnit(kony.flex.DP);
var lblVal = new kony.ui.Label({
"id": "lblVal",
"isVisible": true,
"right": "20dp",
"skin": "sknLblCurr",
"text": "0.000 JOD",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblFromCurr = new kony.ui.Label({
"id": "lblFromCurr",
"isVisible": true,
"right": "55%",
"skin": "sknLblCurr",
"text": "1 JOD",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "10dp",
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblToCurr = new kony.ui.Label({
"id": "lblToCurr",
"isVisible": true,
"right": "73%",
"skin": "sknLblCurr",
"text": "0.746464 JOD",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblEquals = new kony.ui.Label({
"id": "lblEquals",
"isVisible": true,
"right": "69%",
"skin": "sknLblCurr",
"text": "=",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxConversionAmt.add(lblVal, lblFromCurr, lblToCurr, lblEquals);
var lblCurrencyCode = new kony.ui.Label({
"id": "lblCurrencyCode",
"isVisible": true,
"right": "93%",
"skin": "sknLblNextDisabled",
"text": "JOD",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxAmount.add(tbxAmount, lblUnderline, lblAmount, flxConversionAmt, lblCurrencyCode);
var btnPayNow = new kony.ui.Button({
"centerX": "50.05%",
"focusSkin": "slButtonWhiteFocus",
"height": "9%",
"id": "btnPayNow",
"isVisible": true,
"onClick": AS_Button_eebc6476b6564ca0940c8118512a7b0d,
"skin": "slButtonWhite",
"text": kony.i18n.getLocalizedString("i18n.cards.paynow"),
"top": "10%",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyflxAmount0if2f406f50b24d = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "70dp",
"id": "CopyflxAmount0if2f406f50b24d",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "2%",
"width": "90%",
"zIndex": 1
}, {}, {});
CopyflxAmount0if2f406f50b24d.setDefaultUnit(kony.flex.DP);
CopyflxAmount0if2f406f50b24d.add();
var lblHiddenDueAmnt = new kony.ui.Label({
"id": "lblHiddenDueAmnt",
"isVisible": false,
"right": "72dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "19dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblHiddenAmnt = new kony.ui.Label({
"id": "lblHiddenAmnt",
"isVisible": false,
"right": "72dp",
"skin": "slLabel",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "19dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxMain.add(flxBiller, flxAmounttoPay, flxDenomination, flxDueAmount, flxFee, flxMinimumAmount, flxMaxAmount, flxTotalAmount, flxIssueDate, flxDueDate, flxRadioAccCardsSelection, flxPaymentMode, flxAmount, btnPayNow, CopyflxAmount0if2f406f50b24d, lblHiddenDueAmnt, lblHiddenAmnt);
frmBills.add(flxHeader, flxMain);
};
function frmBillsGlobalsAr() {
frmBillsAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmBillsAr,
"enabledForIdleTimeout": true,
"id": "frmBills",
"layoutType": kony.flex.FLOW_VERTICAL,
"needAppMenu": false,
"postShow": AS_Form_f042000a7fbd4d6ea21f76117fc2e17f,
"preShow": AS_Form_b815194793774694a3b6d2ab21a12cf8,
"skin": "sknmainGradient"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FLOW_VERTICAL,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_b63d28f27b8a48e6b1d1a639991f4c35,
"retainScrollPosition": false,
"titleBar": true,
"titleBarSkin": "slTitleBar",
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
