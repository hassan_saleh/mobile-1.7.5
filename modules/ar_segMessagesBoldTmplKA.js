//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function initializesegMessagesBoldTmplKAAr() {
flxSegMsgAr = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100dp",
"id": "flxSegMsg",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknCopyslFbox07d05709853a74d"
}, {}, {});
flxSegMsg.setDefaultUnit(kony.flex.DP);
var flxSegMsgSwipe = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100dp",
"id": "flxSegMsgSwipe",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "168%",
"zIndex": 1
}, {}, {});
flxSegMsgSwipe.setDefaultUnit(kony.flex.DP);
var flxmainkA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100dp",
"id": "flxmainkA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "59.50%",
"zIndex": 1
}, {}, {});
flxmainkA.setDefaultUnit(kony.flex.DP);
var lblTitleKA = new kony.ui.Label({
"id": "lblTitleKA",
"isVisible": true,
"right": "5%",
"maxNumberOfLines": 1,
"skin": "sknMessageTitleBoldKA",
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "2%",
"width": "90%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxDescChevronContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "25dp",
"id": "flxDescChevronContainer",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0",
"skin": "slFbox",
"top": "25dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxDescChevronContainer.setDefaultUnit(kony.flex.DP);
var lblDescKA = new kony.ui.Label({
"id": "lblDescKA",
"isVisible": true,
"right": "5.00%",
"maxNumberOfLines": 1,
"skin": "CopyslLabel031d27909a26c4a",
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "0",
"width": "80%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxDescChevronContainer.add(lblDescKA);
var lblTimestampKA = new kony.ui.Label({
"id": "lblTimestampKA",
"isVisible": true,
"right": "5.00%",
"skin": "sknasOfTimeLabelBlack",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "50dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblMessageIdKA = new kony.ui.Label({
"id": "lblMessageIdKA",
"isVisible": false,
"right": "204dp",
"skin": "slLabel",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "67dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblMessageStatusKA = new kony.ui.Label({
"id": "lblMessageStatusKA",
"isVisible": false,
"right": "0dp",
"skin": "slLabel",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxmainkA.add(lblTitleKA, flxDescChevronContainer, lblTimestampKA, lblMessageIdKA, lblMessageStatusKA);
var btnDeleteKA = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "sknbtnDeleteKA",
"height": "100dp",
"id": "btnDeleteKA",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_b222ca6786d94a03a967807f5eb2b909,
"skin": "sknbtnDeleteKA",
"text": "Delete",
"width": "100dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnReplyKA = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "sknbtnReplyKA",
"height": "100dp",
"id": "btnReplyKA",
"isVisible": true,
"onClick": AS_Button_0ed5f01ff78a46aca3361ae45969e15f,
"skin": "sknbtnReplyKA",
"text": "Reply",
"top": "0dp",
"width": "100dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": false,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxSegMsgSwipe.add( btnReplyKA, btnDeleteKA,flxmainkA);
flxSegMsgAr.add(flxSegMsgSwipe);
}
