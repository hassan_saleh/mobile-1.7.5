//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmCardLinkedAccountsAr() {
frmCardLinkedAccounts.setDefaultUnit(kony.flex.DP);
var flxHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "s",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxHeader.setDefaultUnit(kony.flex.DP);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_h47fec9a10404014a2ad7d3094e36ce1,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 1
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblBack0h2fe661f120d45 = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblBack0h2fe661f120d45",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBack.add(lblBackIcon, CopylblBack0h2fe661f120d45);
var lblWebCharge = new kony.ui.Label({
"height": "90%",
"id": "lblWebCharge",
"isVisible": true,
"left": "20%",
"minHeight": "90%",
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.debitcardlinkedacc.linkedacc"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": "60%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var btnNext = new kony.ui.Button({
"focusSkin": "jomopaynextDisabled",
"height": "90%",
"id": "btnNext",
"isVisible": true,
"onClick": AS_Button_dc52f72c706648c08e3a41820daf1760,
"right": "0%",
"skin": "jomopaynextDisabled",
"text": kony.i18n.getLocalizedString("i18n.login.next"),
"top": "0%",
"width": "20%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
flxHeader.add(flxBack, lblWebCharge, btnNext);
var flxBody = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": true,
"clipBounds": true,
"enableScrolling": true,
"height": "90%",
"horizontalScrollIndicator": true,
"id": "flxBody",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "10%",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
flxBody.setDefaultUnit(kony.flex.DP);
var lblLinked = new kony.ui.Label({
"id": "lblLinked",
"isVisible": false,
"left": "10%",
"skin": "sknSIDate",
"text": kony.i18n.getLocalizedString("i18n.debitcardlinkedacc.linked"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "14%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblDefault = new kony.ui.Label({
"id": "lblDefault",
"isVisible": false,
"right": "81%",
"skin": "sknSIDate",
"text": kony.i18n.getLocalizedString("i18n.debitcardlinkedacc.default"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblLinkedAccountsTitle = new kony.ui.Label({
"height": "30dp",
"id": "lblLinkedAccountsTitle",
"isVisible": false,
"right": "20%",
"minHeight": "90%",
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.debitcardlinkedacc.linkedacc"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": "60%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblHint = new kony.ui.Label({
"id": "lblHint",
"isVisible": false,
"right": "5%",
"skin": "sknSIDate",
"text": kony.i18n.getLocalizedString("i18n.linkedaccount.defaultaccounthint"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "-2%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxLine = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2dp",
"id": "flxLine",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "skntextFieldDivider",
"top": "0%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxLine.setDefaultUnit(kony.flex.DP);
flxLine.add();
var segCardLinkedAccounts = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"data": [{
"lblAccountName": "Label",
"lblAccountNickName": "Label",
"lblAccountNumber": "Label",
"lblDefaultAccounts": "s",
"lblLinkAccounts": "q"
}, {
"lblAccountName": "Label",
"lblAccountNickName": "Label",
"lblAccountNumber": "Label",
"lblDefaultAccounts": "s",
"lblLinkAccounts": "q"
}, {
"lblAccountName": "Label",
"lblAccountNickName": "Label",
"lblAccountNumber": "Label",
"lblDefaultAccounts": "s",
"lblLinkAccounts": "q"
}],
"groupCells": false,
"id": "segCardLinkedAccounts",
"isVisible": true,
"right": "5%",
"needPageIndicator": true,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "sknsegAcc",
"rowSkin": "sknsegAcc",
"rowTemplate": flxtemplateLinkedAccounts,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorRequired": false,
"separatorThickness": 0,
"showScrollbars": false,
"top": "0%",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"Copydivider0c263b496ea8345": "Copydivider0c263b496ea8345",
"flxAccountDetails": "flxAccountDetails",
"flxAccountLinkOptions": "flxAccountLinkOptions",
"flxDefaultAccounts": "flxDefaultAccounts",
"flxDivider": "flxDivider",
"flxILineDisabled": "flxILineDisabled",
"flxIcon1": "flxIcon1",
"flxInnerDisabled": "flxInnerDisabled",
"flxInnerEnabled": "flxInnerEnabled",
"flxLineEnabled": "flxLineEnabled",
"flxLinkAccounts": "flxLinkAccounts",
"flxLinkedAccountsDisabled": "flxLinkedAccountsDisabled",
"flxLinkedAccountsEnable": "flxLinkedAccountsEnable",
"flxtemplateLinkedAccounts": "flxtemplateLinkedAccounts",
"lblAccountName": "lblAccountName",
"lblAccountNickName": "lblAccountNickName",
"lblAccountNumber": "lblAccountNumber",
"lblDefaultAccounts": "lblDefaultAccounts",
"lblLinkAccounts": "lblLinkAccounts",
"lblTick": "lblTick"
},
"width": "90%",
"zIndex": 1
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"dockSectionHeaders": true
});
var lblDefaultAccountsTitle = new kony.ui.Label({
"height": "30dp",
"id": "lblDefaultAccountsTitle",
"isVisible": true,
"right": "20%",
"minHeight": "90%",
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.linkcard.defaultaccounts"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "2%",
"width": "60%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxDefaultAccount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "13%",
"id": "flxDefaultAccount",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "5%",
"skin": "slFbox",
"top": "0%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxDefaultAccount.setDefaultUnit(kony.flex.DP);
var lblDefaultAccountTitle = new kony.ui.Label({
"height": "35%",
"id": "lblDefaultAccountTitle",
"isVisible": true,
"right": "0%",
"skin": "sknLblCurr",
"text": kony.i18n.getLocalizedString("i18n.linkcard.defaultaccounts"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "8%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var btnDefaultAccountIcon = new kony.ui.Button({
"centerY": "60%",
"focusSkin": "sknBtnForwardDimmed",
"height": "50%",
"id": "btnDefaultAccountIcon",
"isVisible": true,
"right": "0%",
"onClick": AS_Button_afbce03e0a2240de9f3749791eab21c1,
"skin": "sknBtnForwardDimmed",
"text": kony.i18n.getLocalizedString("i18n.common.baclinfo"),
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var lblDefaultAccount = new kony.ui.Label({
"centerY": "60%",
"id": "lblDefaultAccount",
"isVisible": true,
"right": "0%",
"skin": "sknLblWhike125",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblLine = new kony.ui.Label({
"centerX": "50%",
"height": "2%",
"id": "lblLine",
"isVisible": true,
"skin": "lblLine0h8b99d8a85f649",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "80%",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxDefaultAccount.add(lblDefaultAccountTitle, btnDefaultAccountIcon, lblDefaultAccount, lblLine);
flxBody.add(lblLinked, lblDefault, lblLinkedAccountsTitle, lblHint, flxLine, segCardLinkedAccounts, lblDefaultAccountsTitle, flxDefaultAccount);
frmCardLinkedAccounts.add(flxHeader, flxBody);
};
function frmCardLinkedAccountsGlobalsAr() {
frmCardLinkedAccountsAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmCardLinkedAccountsAr,
"bounces": false,
"enabledForIdleTimeout": false,
"id": "frmCardLinkedAccounts",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": true,
"skin": "slFormCommon",
"verticalScrollIndicator": false
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"retainScrollPosition": false,
"titleBar": true,
"titleBarSkin": "slTitleBar",
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
