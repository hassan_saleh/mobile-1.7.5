//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function initializetmpDeviceRegstrationEnabledAr() {
    flxDeviceDetailsEnabledAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "50dp",
        "id": "flxDeviceDetailsEnabled",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "flxBgBlueGradientRound8"
    }, {}, {});
    flxDeviceDetailsEnabled.setDefaultUnit(kony.flex.DP);
    var lblDeviceTitle = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblDeviceTitle",
        "isVisible": true,
        "right": "5%",
        "maxNumberOfLines": 1,
        "skin": "sknLblWhite128C",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "1dp",
        "width": "73%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxThisDeviceSwitchOff = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxThisDeviceSwitchOff",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "sknflxWhiteOpacity60",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxThisDeviceSwitchOff.setDefaultUnit(kony.flex.DP);
    var CopyflxRoundDBlueOff0a5e3eddfada24b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "CopyflxRoundDBlueOff0a5e3eddfada24b",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "sknFlxDarkBlueRoundCornerGrey",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {}, {});
    CopyflxRoundDBlueOff0a5e3eddfada24b.setDefaultUnit(kony.flex.DP);
    CopyflxRoundDBlueOff0a5e3eddfada24b.add();
    var CopyflxNaveenbhaiKiLakeer0cb72155c61834c = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "14dp",
        "id": "CopyflxNaveenbhaiKiLakeer0cb72155c61834c",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 10,
        "skin": "sknLineDarkBlueOpacity60",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    CopyflxNaveenbhaiKiLakeer0cb72155c61834c.setDefaultUnit(kony.flex.DP);
    CopyflxNaveenbhaiKiLakeer0cb72155c61834c.add();
    flxThisDeviceSwitchOff.add(CopyflxRoundDBlueOff0a5e3eddfada24b, CopyflxNaveenbhaiKiLakeer0cb72155c61834c);
    var flxThisDeviceSwitchOn = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "flxThisDeviceSwitchOn",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "skin": "sknflxyellow",
        "top": "0dp",
        "width": "44dp",
        "zIndex": 1
    }, {}, {});
    flxThisDeviceSwitchOn.setDefaultUnit(kony.flex.DP);
    var CopyflxRoundDBlue0bbc73e3c0fb041 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "25dp",
        "id": "CopyflxRoundDBlue0bbc73e3c0fb041",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "sknFlxDarkBlueRoundCorner",
        "top": "0dp",
        "width": "25dp",
        "zIndex": 1
    }, {}, {});
    CopyflxRoundDBlue0bbc73e3c0fb041.setDefaultUnit(kony.flex.DP);
    CopyflxRoundDBlue0bbc73e3c0fb041.add();
    var Copyflxlakeer0h4a3ac20cf6948 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "14dp",
        "id": "Copyflxlakeer0h4a3ac20cf6948",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "10dp",
        "skin": "sknLineDarkBlue",
        "top": "0dp",
        "width": "2dp",
        "zIndex": 1
    }, {}, {});
    Copyflxlakeer0h4a3ac20cf6948.setDefaultUnit(kony.flex.DP);
    Copyflxlakeer0h4a3ac20cf6948.add();
    flxThisDeviceSwitchOn.add(CopyflxRoundDBlue0bbc73e3c0fb041, Copyflxlakeer0h4a3ac20cf6948);
    flxDeviceDetailsEnabledAr.add(lblDeviceTitle, flxThisDeviceSwitchOff, flxThisDeviceSwitchOn);
}
