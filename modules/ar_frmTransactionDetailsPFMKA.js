//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmTransactionDetailsPFMKAAr() {
    frmTransactionDetailsPFMKA.setDefaultUnit(kony.flex.DP);
    var titleBarWrapper = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "titleBarWrapper",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkgheader",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    titleBarWrapper.setDefaultUnit(kony.flex.DP);
    var androidTitleBar = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "androidTitleBar",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkgheader",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    androidTitleBar.setDefaultUnit(kony.flex.DP);
    var androidTitleLabel = new kony.ui.Label({
        "centerY": "50%",
        "id": "androidTitleLabel",
        "isVisible": true,
        "right": "55dp",
        "skin": "sknnavBarTitle",
        "text": kony.i18n.getLocalizedString("i18n.common.transactionDetails"),
        "width": "70%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var androidBack = new kony.ui.Button({
        "focusSkin": "sknandroidBackButtonFocus",
        "height": "50dp",
        "id": "androidBack",
        "isVisible": true,
        "right": "0dp",
        "onClick": AS_Button_1e1ee39f1c7e4d09ab114b2225e94bbf,
        "skin": "sknandroidBackButton",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    androidTitleBar.add(androidTitleLabel, androidBack);
    var btnEditKA = new kony.ui.Button({
        "focusSkin": "sknbtn",
        "height": "50dp",
        "id": "btnEditKA",
        "isVisible": false,
        "left": "10dp",
        "skin": "sknbtn",
        "text": "Edit",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    titleBarWrapper.add(androidTitleBar, btnEditKA);
    var mainContent = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "bottom": 0,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "horizontalScrollIndicator": true,
        "id": "mainContent",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknscrollBkg",
        "top": "50dp",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    mainContent.setDefaultUnit(kony.flex.DP);
    var FlexContainer042b0725667e643 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "FlexContainer042b0725667e643",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer042b0725667e643.setDefaultUnit(kony.flex.DP);
    var FlexContainer0f615b714fea843 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "150dp",
        "id": "FlexContainer0f615b714fea843",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexContainer0f615b714fea843.setDefaultUnit(kony.flex.DP);
    var transactionAmount = new kony.ui.Label({
        "centerX": "50%",
        "id": "transactionAmount",
        "isVisible": true,
        "skin": "skndetailPageNumber",
        "text": "$ 5006.00",
        "top": "30dp",
        "width": "100%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var transactionDate = new kony.ui.Label({
        "centerX": "50%",
        "id": "transactionDate",
        "isVisible": true,
        "skin": "skndetailPageDate",
        "text": "Bill Payment to",
        "top": "3dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var transactionName = new kony.ui.Label({
        "centerX": "50%",
        "id": "transactionName",
        "isVisible": true,
        "skin": "sknNumber",
        "text": "Direct Energy 5445",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblP2PContactKA = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblP2PContactKA",
        "isVisible": false,
        "skin": "skndetailPageDate",
        "text": "+91 9848492905",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FlexContainer0f615b714fea843.add(transactionAmount, transactionDate, transactionName, lblP2PContactKA);
    var divider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "1dp",
        "id": "divider",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    divider.setDefaultUnit(kony.flex.DP);
    divider.add();
    var transactionId = new kony.ui.Label({
        "id": "transactionId",
        "isVisible": false,
        "right": "8dp",
        "skin": "slLabel",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "11dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var transactionType = new kony.ui.Label({
        "id": "transactionType",
        "isVisible": false,
        "right": "3dp",
        "skin": "slLabel",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "50dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FlexContainer042b0725667e643.add(FlexContainer0f615b714fea843, divider, transactionId, transactionType);
    var additionalDetails1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "75dp",
        "id": "additionalDetails1",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    additionalDetails1.setDefaultUnit(kony.flex.DP);
    var Label0e7a26666ebf141 = new kony.ui.Label({
        "id": "Label0e7a26666ebf141",
        "isVisible": true,
        "right": "5%",
        "skin": "sknsegmentHeaderText",
        "text": kony.i18n.getLocalizedString("i18n.common.fromc"),
        "top": "20dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var transactionFrom = new kony.ui.Label({
        "id": "transactionFrom",
        "isVisible": true,
        "right": "5%",
        "skin": "sknNumber",
        "text": "Savings 2453",
        "top": "40dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var divider2 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "1dp",
        "id": "divider2",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    divider2.setDefaultUnit(kony.flex.DP);
    divider2.add();
    var lblseletedIndex = new kony.ui.Label({
        "id": "lblseletedIndex",
        "isVisible": false,
        "right": "251dp",
        "skin": "slLabel",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "26dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    additionalDetails1.add(Label0e7a26666ebf141, transactionFrom, divider2, lblseletedIndex);
    var flxSchedule = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "75dp",
        "id": "flxSchedule",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSchedule.setDefaultUnit(kony.flex.DP);
    var lblTransactionDateKA = new kony.ui.Label({
        "id": "lblTransactionDateKA",
        "isVisible": true,
        "right": "5%",
        "skin": "sknsegmentHeaderText",
        "text": kony.i18n.getLocalizedString("i18n.common.scheduledForC"),
        "top": "20dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblTransactionDateValueKA = new kony.ui.Label({
        "id": "lblTransactionDateValueKA",
        "isVisible": true,
        "right": "5%",
        "skin": "sknNumber",
        "text": "05-20-2016",
        "top": "40dp",
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var Copydivider058b92c2528494d = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "1dp",
        "id": "Copydivider058b92c2528494d",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    Copydivider058b92c2528494d.setDefaultUnit(kony.flex.DP);
    Copydivider058b92c2528494d.add();
    flxSchedule.add(lblTransactionDateKA, lblTransactionDateValueKA, Copydivider058b92c2528494d);
    var flxReccurrence = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "75dp",
        "id": "flxReccurrence",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxReccurrence.setDefaultUnit(kony.flex.DP);
    var lblReccurrenceLabelKA = new kony.ui.Label({
        "id": "lblReccurrenceLabelKA",
        "isVisible": true,
        "right": "5%",
        "skin": "sknsegmentHeaderText",
        "text": kony.i18n.getLocalizedString("i18n.transfers.RecurrenceC"),
        "top": "20dp",
        "width": "95%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblReccurrenceValueKA = new kony.ui.Label({
        "id": "lblReccurrenceValueKA",
        "isVisible": true,
        "right": "5%",
        "skin": "sknNumber",
        "text": "Once a Month",
        "top": "40dp",
        "width": "45%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblReccurrenceNumberKA = new kony.ui.Label({
        "id": "lblReccurrenceNumberKA",
        "isVisible": false,
        "left": "5%",
        "skin": "sknNumber",
        "text": "Next 5 Times",
        "top": "40dp",
        "width": "45%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var Copydivider0d794f3c52b2a4f = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "1dp",
        "id": "Copydivider0d794f3c52b2a4f",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    Copydivider0d794f3c52b2a4f.setDefaultUnit(kony.flex.DP);
    Copydivider0d794f3c52b2a4f.add();
    flxReccurrence.add(lblReccurrenceLabelKA, lblReccurrenceValueKA, lblReccurrenceNumberKA, Copydivider0d794f3c52b2a4f);
    var flxNotes = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxNotes",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxNotes.setDefaultUnit(kony.flex.DP);
    var CopynotesWrapper03de47f39b8bc46 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "CopynotesWrapper03de47f39b8bc46",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "skin": "skncontainerBkgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopynotesWrapper03de47f39b8bc46.setDefaultUnit(kony.flex.DP);
    var lblNotesLabelKA = new kony.ui.Label({
        "id": "lblNotesLabelKA",
        "isVisible": true,
        "right": "5%",
        "skin": "sknsegmentHeaderText",
        "text": kony.i18n.getLocalizedString("i18n.common.notesc"),
        "top": "20dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var transactionNotes = new kony.ui.Label({
        "bottom": "20dp",
        "id": "transactionNotes",
        "isVisible": true,
        "right": "5%",
        "skin": "sknNumber",
        "text": "Note details appear here. This can be a muliple line descripsion",
        "top": "10dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var Copydivider06e184b7d586e4a = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "1dp",
        "id": "Copydivider06e184b7d586e4a",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    Copydivider06e184b7d586e4a.setDefaultUnit(kony.flex.DP);
    Copydivider06e184b7d586e4a.add();
    CopynotesWrapper03de47f39b8bc46.add(lblNotesLabelKA, transactionNotes, Copydivider06e184b7d586e4a);
    var repeatTransactionContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "repeatTransactionContainer",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skncontainerBkgWhite",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    repeatTransactionContainer.setDefaultUnit(kony.flex.DP);
    var repeatTransactionButton = new kony.ui.Button({
        "centerX": "50.00%",
        "focusSkin": "sknsecondaryActionFocus",
        "height": "60dp",
        "id": "repeatTransactionButton",
        "isVisible": true,
        "skin": "sknsecondaryAction",
        "text": kony.i18n.getLocalizedString("i18n.transfer.repeatTransaction"),
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    var Copydivider0b2398672408341 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "1dp",
        "id": "Copydivider0b2398672408341",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    Copydivider0b2398672408341.setDefaultUnit(kony.flex.DP);
    Copydivider0b2398672408341.add();
    repeatTransactionContainer.add(repeatTransactionButton, Copydivider0b2398672408341);
    flxNotes.add(CopynotesWrapper03de47f39b8bc46, repeatTransactionContainer);
    mainContent.add(FlexContainer042b0725667e643, additionalDetails1, flxSchedule, flxReccurrence, flxNotes);
    frmTransactionDetailsPFMKA.add(titleBarWrapper, mainContent);
};
function frmTransactionDetailsPFMKAGlobalsAr() {
    frmTransactionDetailsPFMKAAr = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmTransactionDetailsPFMKAAr,
        "allowHorizontalBounce": false,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmTransactionDetailsPFMKA",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "inTransitionConfig": {
            "formAnimation": 2
        },
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_13fab0c525a647b8a1279d6af7ee63ff,
        "outTransitionConfig": {
            "formAnimation": 5
        },
        "retainScrollPosition": false,
        "titleBar": true,
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};
