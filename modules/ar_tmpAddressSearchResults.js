//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function initializetmpAddressSearchResultsAr() {
    flxAddressSearchResultsAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxAddressSearchResults",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox"
    }, {}, {});
    flxAddressSearchResults.setDefaultUnit(kony.flex.DP);
    var lblSearchResults = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblSearchResults",
        "isVisible": true,
        "right": "5%",
        "skin": "sknlblF9c9c9c",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": "95%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lineFirstNameKA = new kony.ui.Label({
        "bottom": "1%",
        "centerX": "50%",
        "height": "1dp",
        "id": "lineFirstNameKA",
        "isVisible": true,
        "right": "5%",
        "skin": "sknLineEDEDEDKA",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAddressSearchResultsAr.add(lblSearchResults, lineFirstNameKA);
}
