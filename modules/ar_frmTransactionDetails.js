//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmTransactionDetailsAr() {
    frmTransactionDetails.setDefaultUnit(kony.flex.DP);
    var flxTransactionDetailsHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "8%",
        "id": "flxTransactionDetailsHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "skin": "sknBlueBGheader",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTransactionDetailsHeader.setDefaultUnit(kony.flex.DP);
    var lblJoMoPay = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblJoMoPay",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": "Confirm Details",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnBack = new kony.ui.Button({
        "centerX": "5%",
        "centerY": "50%",
        "focusSkin": "CopyslButtonGlossBlue0e73a02c4810645",
        "height": "50%",
        "id": "btnBack",
        "isVisible": true,
        "onClick": AS_Button_e6aec81914a34c2d8219b98e431aaffe,
        "skin": "CopyslButtonGlossBlue0e73a02c4810645",
        "text": "j",
        "width": "8%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblBack = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Login Screen"
        },
        "centerY": "50%",
        "id": "lblBack",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxTransactionDetailsHeader.add(lblJoMoPay, btnBack, lblBack);
    var flxTransactionDetailsBody = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxTransactionDetailsBody",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "slFbox",
        "top": "10%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTransactionDetailsBody.setDefaultUnit(kony.flex.DP);
    var lblNameShort = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "6%",
        "height": "12%",
        "id": "lblNameShort",
        "isVisible": true,
        "skin": "sknRoundedNOBG",
        "text": "EW",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "19%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblName = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "15%",
        "id": "lblName",
        "isVisible": true,
        "skin": "sknCarioRegular255255255Per120",
        "text": "Bernard Herrmann",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblreferenceNo = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "18%",
        "id": "lblreferenceNo",
        "isVisible": true,
        "skin": "sknCarioRegular130149164",
        "text": "DE36147942711520577565",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxTransactionDetailsContent = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": false,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "80%",
        "horizontalScrollIndicator": true,
        "id": "flxTransactionDetailsContent",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0%",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "20%",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTransactionDetailsContent.setDefaultUnit(kony.flex.DP);
    var Label0f1634615bdd144 = new kony.ui.Label({
        "id": "Label0f1634615bdd144",
        "isVisible": true,
        "right": "5%",
        "skin": "sknCarioRegular130149164",
        "text": "From",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5px",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyLabel0f1d2d508761f4c = new kony.ui.Label({
        "id": "CopyLabel0f1d2d508761f4c",
        "isVisible": true,
        "right": "5%",
        "skin": "sknCarioRegular255255255Per120",
        "text": "Savings account ****5614",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "-12px",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxAmount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxAmount",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "slFbox",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxAmount.setDefaultUnit(kony.flex.DP);
    var CopyLabel0fe3e4b76713144 = new kony.ui.Label({
        "id": "CopyLabel0fe3e4b76713144",
        "isVisible": true,
        "right": "5%",
        "skin": "sknCarioRegular130149164",
        "text": "Amount",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0px",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyLabel0j676f80f9a204f = new kony.ui.Label({
        "id": "CopyLabel0j676f80f9a204f",
        "isVisible": true,
        "right": "5%",
        "skin": "sknCarioRegular255255255Per120",
        "text": "123.000 JOD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "22dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyLabel0a29b690e67794d = new kony.ui.Label({
        "id": "CopyLabel0a29b690e67794d",
        "isVisible": true,
        "left": "10%",
        "skin": "sknCarioRegular255255255Per120",
        "text": "173.49 USD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "22dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxAmount.add(CopyLabel0fe3e4b76713144, CopyLabel0j676f80f9a204f, CopyLabel0a29b690e67794d);
    var CopyLabel0if18b04692db45 = new kony.ui.Label({
        "id": "CopyLabel0if18b04692db45",
        "isVisible": true,
        "right": "5%",
        "skin": "sknCarioRegular130149164",
        "text": "Frequency",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyLabel0j60c845661e946 = new kony.ui.Label({
        "id": "CopyLabel0j60c845661e946",
        "isVisible": true,
        "right": "5%",
        "skin": "sknCarioRegular255255255Per120",
        "text": "Monthly",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "-12px",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyLabel0f5624eecf1a24a = new kony.ui.Label({
        "id": "CopyLabel0f5624eecf1a24a",
        "isVisible": true,
        "right": "5%",
        "skin": "sknCarioRegular130149164",
        "text": "Recurrances",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyLabel0e3f3848b97c04c = new kony.ui.Label({
        "id": "CopyLabel0e3f3848b97c04c",
        "isVisible": true,
        "right": "5%",
        "skin": "sknCarioRegular255255255Per120",
        "text": "10",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "-12px",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyflxAmount0ia2541c374fc46 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "CopyflxAmount0ia2541c374fc46",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "slFbox",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyflxAmount0ia2541c374fc46.setDefaultUnit(kony.flex.DP);
    var CopyLabel0dfc75d0e4cf843 = new kony.ui.Label({
        "id": "CopyLabel0dfc75d0e4cf843",
        "isVisible": true,
        "right": "5%",
        "skin": "sknCarioRegular130149164",
        "text": "Start Date",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0px",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyLabel0bc135f67b07c49 = new kony.ui.Label({
        "id": "CopyLabel0bc135f67b07c49",
        "isVisible": true,
        "right": "65%",
        "skin": "sknCarioRegular130149164",
        "text": "End Date",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0px",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyLabel0hbaeeddfaec740 = new kony.ui.Label({
        "id": "CopyLabel0hbaeeddfaec740",
        "isVisible": true,
        "right": "5%",
        "skin": "sknCarioRegular255255255Per120",
        "text": "14 Feb 2018",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "22dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyLabel0id23ea7cb4534b = new kony.ui.Label({
        "id": "CopyLabel0id23ea7cb4534b",
        "isVisible": true,
        "right": "65%",
        "skin": "sknCarioRegular255255255Per120",
        "text": "14 Feb 2018",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "22dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopyflxAmount0ia2541c374fc46.add(CopyLabel0dfc75d0e4cf843, CopyLabel0bc135f67b07c49, CopyLabel0hbaeeddfaec740, CopyLabel0id23ea7cb4534b);
    var CopyflxAmount0d73d550f2aef46 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "CopyflxAmount0d73d550f2aef46",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "slFbox",
        "top": "5dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyflxAmount0d73d550f2aef46.setDefaultUnit(kony.flex.DP);
    var CopyLabel0a7744ac50b954a = new kony.ui.Label({
        "id": "CopyLabel0a7744ac50b954a",
        "isVisible": true,
        "right": "5%",
        "skin": "sknCarioRegular130149164",
        "text": "Scheduled Date",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0px",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyLabel0f0fa260c96d246 = new kony.ui.Label({
        "id": "CopyLabel0f0fa260c96d246",
        "isVisible": true,
        "right": "5%",
        "skin": "sknCarioRegular255255255Per120",
        "text": "14 Feb 2018",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "22dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyLabel0ge25fae29d434a = new kony.ui.Label({
        "id": "CopyLabel0ge25fae29d434a",
        "isVisible": true,
        "right": "75%",
        "skin": "sknBOJFont255255255Per120",
        "text": "1",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35px",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopyflxAmount0d73d550f2aef46.add(CopyLabel0a7744ac50b954a, CopyLabel0f0fa260c96d246, CopyLabel0ge25fae29d434a);
    var CopyLabel0a139f112f21640 = new kony.ui.Label({
        "id": "CopyLabel0a139f112f21640",
        "isVisible": true,
        "right": "5%",
        "skin": "sknCarioRegular130149164",
        "text": "Purpose of transfer",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyLabel0e5bc9f40211145 = new kony.ui.Label({
        "id": "CopyLabel0e5bc9f40211145",
        "isVisible": true,
        "right": "5%",
        "skin": "sknCarioRegular255255255Per120",
        "text": "8932983739297",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "-12px",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyLabel0g36a9fb192a742 = new kony.ui.Label({
        "id": "CopyLabel0g36a9fb192a742",
        "isVisible": true,
        "right": "5%",
        "skin": "sknCarioRegular130149164",
        "text": "Fee",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyLabel0fee3fd571e4848 = new kony.ui.Label({
        "id": "CopyLabel0fee3fd571e4848",
        "isVisible": true,
        "right": "5%",
        "skin": "sknCarioRegular255255255Per120",
        "text": "00.060 JOD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "-12px",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyLabel0a8521288980a42 = new kony.ui.Label({
        "id": "CopyLabel0a8521288980a42",
        "isVisible": true,
        "right": "5%",
        "skin": "sknCarioRegular130149164",
        "text": "Description",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyLabel0c1ba526abe7747 = new kony.ui.Label({
        "id": "CopyLabel0c1ba526abe7747",
        "isVisible": true,
        "right": "5%",
        "skin": "sknCarioRegular255255255Per120",
        "text": "lorem ipsum….",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "-12px",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var Button0dedb1d35bd4c4b = new kony.ui.Button({
        "bottom": 0,
        "centerX": "50%",
        "centerY": "10%",
        "focusSkin": "sknprimaryActionFcs",
        "height": "10%",
        "id": "Button0dedb1d35bd4c4b",
        "isVisible": true,
        "onClick": AS_Button_e75b018148194370b044537315afece2,
        "skin": "sknprimaryAction",
        "text": "Stop Payment",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    var lblEmptySpace = new kony.ui.Label({
        "id": "lblEmptySpace",
        "isVisible": true,
        "right": "5%",
        "skin": "sknCarioRegular255255255Per120",
        "text": " ",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "-12px",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxTransactionDetailsContent.add(Label0f1634615bdd144, CopyLabel0f1d2d508761f4c, flxAmount, CopyLabel0if18b04692db45, CopyLabel0j60c845661e946, CopyLabel0f5624eecf1a24a, CopyLabel0e3f3848b97c04c, CopyflxAmount0ia2541c374fc46, CopyflxAmount0d73d550f2aef46, CopyLabel0a139f112f21640, CopyLabel0e5bc9f40211145, CopyLabel0g36a9fb192a742, CopyLabel0fee3fd571e4848, CopyLabel0a8521288980a42, CopyLabel0c1ba526abe7747, Button0dedb1d35bd4c4b, lblEmptySpace);
    flxTransactionDetailsBody.add(lblNameShort, lblName, lblreferenceNo, flxTransactionDetailsContent);
    frmTransactionDetails.add(flxTransactionDetailsHeader, flxTransactionDetailsBody);
};
function frmTransactionDetailsGlobalsAr() {
    frmTransactionDetailsAr = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmTransactionDetailsAr,
        "enabledForIdleTimeout": false,
        "id": "frmTransactionDetails",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknSuccessBkg"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};
