//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmShowAllBeneficiaryAr() {
frmShowAllBeneficiary.setDefaultUnit(kony.flex.DP);
var flxHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "23%",
"id": "flxHeader",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "slFlxHeaderImg",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxHeader.setDefaultUnit(kony.flex.DP);
var flxTop = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "34%",
"id": "flxTop",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxTop.setDefaultUnit(kony.flex.DP);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_ba48cb1d0c4b4458aef0f8cdcf79aad6,
"skin": "slFbox",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBack = new kony.ui.Label({
"centerY": "50%",
"id": "lblBack",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBack.add(lblBackIcon, lblBack);
var lblTitle = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "lblTitle",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.Bene.SendMoney"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxIcons = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxIcons",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "75%",
"skin": "slFbox",
"top": "0dp",
"width": "25%",
"zIndex": 1
}, {}, {});
flxIcons.setDefaultUnit(kony.flex.DP);
var btnTransactionStatus = new kony.ui.Button({
"focusSkin": "slButtonWhiteFocus",
"height": "100%",
"id": "btnTransactionStatus",
"isVisible": false,
"left": 0,
"skin": "slIcon",
"text": "A",
"top": 0,
"width": "48%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
var btnSearch = new kony.ui.Button({
"focusSkin": "slIconFocus",
"height": "100%",
"id": "btnSearch",
"isVisible": false,
"left": "0dp",
"onClick": AS_Button_df69320aef2d4221b035b01aa5725562,
"skin": "slIcon",
"text": "h",
"top": "0dp",
"width": "50%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
var btnAddBeneficiary = new kony.ui.Button({
"focusSkin": "slIconFocus",
"height": "100%",
"id": "btnAddBeneficiary",
"isVisible": true,
"left": "50%",
"onClick": AS_Button_g816a95180f74f5187a0722e93ebaae2,
"skin": "slIcon",
"text": "u",
"top": "0dp",
"width": "50%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
flxIcons.add(btnTransactionStatus, btnSearch, btnAddBeneficiary);
flxTop.add(flxBack, lblTitle, flxIcons);
var flxTab = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "28%",
"id": "flxTab",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxTab.setDefaultUnit(kony.flex.DP);
var flxContent = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"centerY": "50%",
"clipBounds": true,
"height": "65%",
"id": "flxContent",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0dp",
"skin": "slFboxOuterRing",
"top": "0dp",
"width": "88%",
"zIndex": 1
}, {}, {});
flxContent.setDefaultUnit(kony.flex.DP);
var btnAll = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "slButtonWhiteTabFocus",
"height": "100%",
"id": "btnAll",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_icfdb0e4c4424163885f5e73c75f7d06,
"skin": "slButtonWhiteTab",
"text": kony.i18n.getLocalizedString("i18n.Bene.All"),
"top": "0dp",
"width": "18%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnBOJ = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "slButtonWhiteTabFocus",
"height": "100%",
"id": "btnBOJ",
"isVisible": true,
"onClick": AS_Button_h12c7d60d8a54de5a53e2fa0abb40fa7,
"skin": "slButtonWhiteTabDisabled",
"text": kony.i18n.getLocalizedString("i18n.Bene.BOJ"),
"top": "0dp",
"width": "18%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnDomestic = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "slButtonWhiteTabFocus",
"height": "100%",
"id": "btnDomestic",
"isVisible": true,
"onClick": AS_Button_b1d62137c69140809a577d4b788b93f2,
"skin": "slButtonWhiteTabDisabled",
"text": kony.i18n.getLocalizedString("i18n.externalaccount.Domestic"),
"top": "0dp",
"width": "30%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnInternational = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "slButtonWhiteTabFocus",
"height": "98%",
"id": "btnInternational",
"isVisible": true,
"onClick": AS_Button_cd85e9f2430a46a69f35f3755ccec06a,
"skin": "slButtonWhiteTabDisabled",
"text": kony.i18n.getLocalizedString("i18n.transfer.International"),
"width": "34%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxContent.add( btnInternational, btnDomestic, btnBOJ,btnAll);
flxTab.add(flxContent);
var flxSearch = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "28%",
"id": "flxSearch",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknFlxSearch",
"top": "0dp",
"width": "100%",
"zIndex": 2
}, {}, {});
flxSearch.setDefaultUnit(kony.flex.DP);
var flxSearchHolder = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"centerY": "50%",
"clipBounds": true,
"height": "96%",
"id": "flxSearchHolder",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"skin": "slFbox",
"top": "0%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxSearchHolder.setDefaultUnit(kony.flex.DP);
var lblSearchImg = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblSearchImg",
"isVisible": true,
"right": "0%",
"skin": "sknSearchIcon",
"text": "h",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "8%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var tbxSearch = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"centerY": "50%",
"focusSkin": "sknTxtBoxSearch",
"height": "100%",
"id": "tbxSearch",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "0%",
"onDone": AS_TextField_c583e1e8e21749189d04388a9d25f095,
"onTextChange": AS_TextField_ce9eae382cf0441ca3ccab3ba442f279,
"placeholder": kony.i18n.getLocalizedString("i18n.transfer.searchPlh"),
"secureTextEntry": false,
"skin": "sknTxtBoxSearch",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "92%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
"placeholderSkin": "sknPlaceHolder",
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
flxSearchHolder.add( tbxSearch,lblSearchImg);
var flxUnderLineSearch = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "2%",
"id": "flxUnderLineSearch",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "92%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxUnderLineSearch.setDefaultUnit(kony.flex.DP);
flxUnderLineSearch.add();
flxSearch.add(flxSearchHolder, flxUnderLineSearch);
flxHeader.add(flxTop, flxTab, flxSearch);
var flxScrollMain = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": true,
"allowVerticalBounce": true,
"bottom": "0dp",
"bounces": true,
"clipBounds": true,
"enableScrolling": true,
"height": "79%",
"horizontalScrollIndicator": true,
"id": "flxScrollMain",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_HORIZONTAL,
"skin": "sknscrollBenf",
"top": "-2%",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 2
}, {}, {});
flxScrollMain.setDefaultUnit(kony.flex.DP);
flxScrollMain.add();
var segAllBeneficiary = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"data": [
[{
"lbltmpTitle": "Favourite"
},
[{
"BenificiaryFullName": "",
"BenificiaryName": "",
"accountNumber": "",
"btnDelete": "",
"btnEditBeneficiary": "Button",
"btnFav": "",
"lblInitial": ""
}]
],
[{
"lbltmpTitle": "All"
},
[{
"BenificiaryFullName": "",
"BenificiaryName": "",
"accountNumber": "",
"btnDelete": "",
"btnEditBeneficiary": "Button",
"btnFav": "",
"lblInitial": ""
}]
]
],
"groupCells": false,
"height": "79%",
"id": "segAllBeneficiary",
"isVisible": true,
"right": "0dp",
"needPageIndicator": true,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "seg2Focus",
"rowSkin": "slSegAllBene",
"rowTemplate": flxSeg,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"sectionHeaderTemplate": flxTmpHeader,
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorRequired": false,
"separatorThickness": 0,
"showScrollbars": true,
"top": "21%",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"BenificiaryFullName": "BenificiaryFullName",
"BenificiaryName": "BenificiaryName",
"accountNumber": "accountNumber",
"btnDelete": "btnDelete",
"btnEditBeneficiary": "btnEditBeneficiary",
"btnFav": "btnFav",
"contactListDivider": "contactListDivider",
"flxAnimateBenf": "flxAnimateBenf",
"flxButtonHolder": "flxButtonHolder",
"flxDetails": "flxDetails",
"flxIcon1": "flxIcon1",
"flxSeg": "flxSeg",
"flxTmpHeader": "flxTmpHeader",
"flxUnderline": "flxUnderline",
"lblInitial": "lblInitial",
"lbltmpTitle": "lbltmpTitle"
},
"width": "100%",
"zIndex": 2
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var lblStatus = new kony.ui.Label({
"centerX": "50%",
"centerY": "40%",
"id": "lblStatus",
"isVisible": false,
"skin": "sknBeneTitle",
"text": "No Beneficiary added in All",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
frmShowAllBeneficiary.add(flxHeader, flxScrollMain, segAllBeneficiary, lblStatus);
};
function frmShowAllBeneficiaryGlobalsAr() {
frmShowAllBeneficiaryAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmShowAllBeneficiaryAr,
"enabledForIdleTimeout": true,
"id": "frmShowAllBeneficiary",
"init": AS_Form_c3d01c76f39d4b95b62eee6ea2e63e94,
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"postShow": AS_Form_f79a33c994f446bf8b7b6fb5716f7c36,
"preShow": AS_Form_c3bc0f0d8a5b43569cb469fa1aecc2e9,
"skin": "sknSuccessBkg"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_a4382862c43a4e98a50eaf98a61306c7,
"retainScrollPosition": false,
"titleBar": true,
"titleBarSkin": "slTitleBar",
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
