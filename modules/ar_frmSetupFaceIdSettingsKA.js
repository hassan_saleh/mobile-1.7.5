//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmSetupFaceIdSettingsKAAr() {
    frmSetupFaceIdSettingsKA.setDefaultUnit(kony.flex.DP);
    var titleBarAccountDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "titleBarAccountDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "skntitleBarGradient",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    titleBarAccountDetails.setDefaultUnit(kony.flex.DP);
    var touchIDTitleLabel = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "touchIDTitleLabel",
        "isVisible": true,
        "skin": "sknnavBarTitle",
        "text": "Face ID",
        "width": "70%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var backButton = new kony.ui.Button({
        "focusSkin": "sknleftBackButtonFocus",
        "height": "50dp",
        "id": "backButton",
        "isVisible": true,
        "right": "0dp",
        "onClick": AS_Button_bdd873113f6548b1aeb6cb1b851b8bd6,
        "skin": "sknleftBackButtonNormal",
        "top": "0dp",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    var lblBack = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Login Screen"
        },
        "centerY": "50%",
        "id": "lblBack",
        "isVisible": true,
        "right": "10%",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    titleBarAccountDetails.add(touchIDTitleLabel, backButton, lblBack);
    var flxMain = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxMain",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0%",
        "skin": "slFbox",
        "top": "50dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMain.setDefaultUnit(kony.flex.DP);
    var mainContent = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": false,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "92%",
        "horizontalScrollIndicator": true,
        "id": "mainContent",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "right": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "sknscrollBkg",
        "top": "0dp",
        "verticalScrollIndicator": false,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    mainContent.setDefaultUnit(kony.flex.DP);
    var Label013c1b33ed19c4a = new kony.ui.Label({
        "centerX": "50%",
        "id": "Label013c1b33ed19c4a",
        "isVisible": true,
        "right": "10%",
        "skin": "sknstandardTextBold",
        "text": "You can simplify your Login by setting up a FACE ID",
        "top": "45dp",
        "width": "60%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var Button074ef6a5b805c40 = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "sknprimaryActionFocus",
        "height": "50dp",
        "id": "Button074ef6a5b805c40",
        "isVisible": true,
        "right": "45dp",
        "onClick": AS_Button_dc4c1b07ec1b478a96d02c428258a3a7,
        "skin": "sknprimaryAction",
        "text": "SET UP FACE ID...",
        "top": "50dp",
        "width": "260dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    mainContent.add(Label013c1b33ed19c4a, Button074ef6a5b805c40);
    flxMain.add(mainContent);
    frmSetupFaceIdSettingsKA.add(titleBarAccountDetails, flxMain);
};
function frmSetupFaceIdSettingsKAGlobalsAr() {
    frmSetupFaceIdSettingsKAAr = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmSetupFaceIdSettingsKAAr,
        "bounces": false,
        "enabledForIdleTimeout": false,
        "id": "frmSetupFaceIdSettingsKA",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_b60f6443499446d2b8b3a311097a8b84,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};
