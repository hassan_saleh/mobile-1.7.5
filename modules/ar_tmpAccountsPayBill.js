//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function initializetmpAccountsPayBillAr() {
    yourAccountAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "focusSkin": "sknyourAccountCardFocus",
        "height": "62dp",
        "id": "yourAccount",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "5%",
        "skin": "sknyourAccountCard",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    yourAccount.setDefaultUnit(kony.flex.DP);
    var colorAccount1 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "100%",
        "id": "colorAccount1",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "sknCopyslFbox02562de9b22264b",
        "top": "0dp",
        "width": "6dp"
    }, {}, {});
    colorAccount1.setDefaultUnit(kony.flex.DP);
    colorAccount1.add();
    var nameContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "nameContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "60%",
        "zIndex": 1
    }, {}, {});
    nameContainer.setDefaultUnit(kony.flex.DP);
    var nameAccount1 = new kony.ui.Label({
        "id": "nameAccount1",
        "isVisible": true,
        "right": "15dp",
        "maxWidth": "90%",
        "skin": "sknaccountName",
        "top": "12dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var dummyAccountName = new kony.ui.Label({
        "id": "dummyAccountName",
        "isVisible": false,
        "right": "25dp",
        "maxWidth": "90%",
        "skin": "sknaccountName",
        "top": "22dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var dummyAccountNumber = new kony.ui.Label({
        "id": "dummyAccountNumber",
        "isVisible": false,
        "right": "35dp",
        "maxWidth": "90%",
        "skin": "sknaccountName",
        "top": "32dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblColorKA = new kony.ui.Label({
        "height": "100%",
        "id": "lblColorKA",
        "isVisible": true,
        "right": "0dp",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "6dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var isPFMLabel = new kony.ui.Label({
        "id": "isPFMLabel",
        "isVisible": false,
        "left": "12dp",
        "skin": "sknaccountAmount",
        "top": "12dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBankName = new kony.ui.Label({
        "id": "lblBankName",
        "isVisible": true,
        "right": "15dp",
        "maxWidth": "90%",
        "skin": "sknsegmentHeaderText",
        "top": "29dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    nameContainer.add(nameAccount1, dummyAccountName, dummyAccountNumber, lblColorKA, isPFMLabel, lblBankName);
    var amountAccount1 = new kony.ui.Label({
        "id": "amountAccount1",
        "isVisible": true,
        "left": "12dp",
        "skin": "sknaccountAmount",
        "top": "12dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var amtOutsatndingBal = new kony.ui.Label({
        "id": "amtOutsatndingBal",
        "isVisible": false,
        "left": "12dp",
        "skin": "sknaccountAmount",
        "text": "$00.00",
        "top": "12dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var amountcurrBal = new kony.ui.Label({
        "id": "amountcurrBal",
        "isVisible": false,
        "left": "12dp",
        "skin": "sknaccountAmount",
        "text": "$00.00",
        "top": "12dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var typeAccount = new kony.ui.Label({
        "bottom": "12dp",
        "id": "typeAccount",
        "isVisible": false,
        "left": "12dp",
        "skin": "sknaccountAvailableBalanceLabel",
        "text": "Available Balance",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var typeKA = new kony.ui.Label({
        "bottom": "2dp",
        "centerX": "50%",
        "id": "typeKA",
        "isVisible": false,
        "left": "0dp",
        "skin": "sknaccountAvailableBalanceLabel",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var typeAccountBalanceView = new kony.ui.Label({
        "bottom": "12dp",
        "id": "typeAccountBalanceView",
        "isVisible": true,
        "left": "12dp",
        "skin": "sknaccountAvailableBalanceLabel",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    yourAccountAr.add(colorAccount1, nameContainer, amountAccount1, amtOutsatndingBal, amountcurrBal, typeAccount, typeKA, typeAccountBalanceView);
}
