//Type your code here

function callMyProfileData(){
//  var appContext = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
var controller = INSTANCE.getFormController("frmUserSettingsMyProfileKA");
var navObject = new kony.sdk.mvvm.NavigationObject();
navObject.setRequestOptions("form", {
  "headers": {
    "session_token": kony.retailBanking.globalData.session_token
  },
  "queryParams": {
    "custId": custid,
    "language": kony.store.getItem("langPrefObj")=="en"?"eng":"ara"
  }
});
controller.loadDataAndShowForm(navObject);
}



function callConfirmUpdateMobileNumber(msg,action){
  kony.print("callConfirmUpdateMobileNumber::");
  customAlertPopup(geti18Value("i18n.cards.Confirmation"), msg, action,  popupCommonAlertDimiss, geti18Value("i18n.common.Yess"), geti18Value("i18n.comon.Noo"));          

}


function changeSMSNumberServiceCall(){
  kony.print("changeSMSNumberServiceCall::");
  popupCommonAlertDimiss();
  if(kony.sdk.isNetworkAvailable()){
    kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var options = {
      "access": "online",
      "objectName": "RBObjects"
    };
    var countryCode = "962";
    var  creditCardNum = "";
    var FlowFlag = "P";
    var logType = "CHNGPH";
    var smsno = "";
    smsno = frmRegisterUser.txtMobileNumber.text;
    if(gblFromModule == "UpdateSMSNumber"){   
      FlowFlag = "P";
      logType = "CHNGSMS";
   countryCode = isEmpty(frmRegisterUser.txtCountryCode.text) ? "962" : frmRegisterUser.txtCountryCode.text;
    }else{
      logType = "CHNGPH";
      FlowFlag = "C";
      creditCardNum = CARDLIST_DETAILS.cardNumber;
      countryCode = isEmpty(frmRegisterUser.txtCountryCode.text) ? "962" : frmRegisterUser.txtCountryCode.text;
      smsno = countryCode+""+smsno;
    }
    
    var headers = {};
    var serviceName = "RBObjects";
    var modelObj = INSTANCE.getModel("Cards", serviceName, options);
    var dataObject = new kony.sdk.dto.DataObject("Cards"); 
    dataObject.addField("creditCardNum",creditCardNum);
    dataObject.addField("CardNum",frmRegisterUser.txtCardNum.text);
    dataObject.addField("pin",frmRegisterUser.txtPIN.text);
	dataObject.addField("countryCode",countryCode);
    dataObject.addField("custId",custid);
    dataObject.addField("cardkey","");
     dataObject.addField("FlowFlag", FlowFlag);

    dataObject.addField("smsno",smsno);
    dataObject.addField("usr","");
    dataObject.addField("pass","");
    //Logger
    logObj[0] = isEmpty(creditCardNum) ? customerAccountDetails.profileDetails.smsno : creditCardNum.substring(0,4)+" "+creditCardNum.substring(4,6)+"** **** "+creditCardNum.substring(creditCardNum.length-4,creditCardNum.length); //frmAccount,
    logObj[3] = smsno; //toAccount,
    logObj[13] = logType; //transferType,
    var serviceOptions = {"dataObject":dataObject,"headers":headers};

    kony.print("UpdateMobile Number Input params-->"+ JSON.stringify(serviceOptions) );
    modelObj.customVerb("changeSMSNo",serviceOptions, changeSMSNumberServiceCallSuccessCallback,changeSMSNumberServiceCallErrorCallback);
  }else {
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
  }
}




function changeSMSNumberServiceCallSuccessCallback(response){
  kony.print("changeSMSNumberServiceCallSuccessCallback::"+JSON.stringify(response));
   if(!isEmpty(response)){
    if(response.opstatus === 0)
    {if(!isEmpty(response.result)){
      if(response.result === "00000"){
         
      gblBeforeUpdateSMSNumber = true;
      var langSelected = kony.store.getItem("langPrefObj");
      var Language = langSelected.toUpperCase();
      var MSG = "";
      kony.print("gblProfileCustmerName ::"+gblProfileCustmerName);
      if(Language == "EN"){
        MSG = gblEN_CNGSMS_MSG_AFTR.format(gblProfileCustmerName,frmRegisterUser.txtMobileNumber.text);
      }else{
        MSG = gblAR_CNGSMS_MSG_AFTR.format(gblProfileCustmerName,frmRegisterUser.txtMobileNumber.text);
      }    
      callRequestUsrname(MSG); 
        
      logObj[16] = response.result; 
      logObj[17] = "SUCCESS";
      logObj[18] = MSG;
      }else{
		logObj[16] = response.result; 
        logObj[17] = "FAILURE";
        logObj[18] =  "";
        if(response.result === "00101")
        customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.common.invalidPin"),popupCommonAlertDimiss, "");
        else
          customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"),popupCommonAlertDimiss, "");          
        kony.application.dismissLoadingScreen();
      }
    }else{
      logObj[16] =""; 
        logObj[17] = "FAILURE";
        logObj[18] =  "";
      customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"),popupCommonAlertDimiss, "");
      kony.application.dismissLoadingScreen();
    }
    }else{
      logObj[16] =""; 
        logObj[17] = "FAILURE";
        logObj[18] =  "";
      customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"),popupCommonAlertDimiss, "");
      kony.application.dismissLoadingScreen();
    }
     
  }else{
    logObj[16] =""; 
        logObj[17] = "FAILURE";
        logObj[18] =  "";
    customAlertPopup(geti18Value("i18n.Bene.Failed"),geti18Value("i18n.common.somethingwentwrong"),popupCommonAlertDimiss, "");
    kony.application.dismissLoadingScreen();
  }
   loggerCall();
}

function changeSMSNumberServiceCallErrorCallback(response)
{
    kony.print("changeSMSNumberServiceCallErrorCallback -----"+JSON.stringify(response));
  logObj[17] = "FAILURE";
  gblUpdateOldMobileNumber = "";
  if(!isEmpty(response.result)){
    logObj[16] = response.result; 
        logObj[17] = "FAILURE";
        logObj[18] =  "";
  }
    customAlertPopup(geti18Value("i18n.Bene.Failed"),  geti18Value("i18n.common.somethingwentwrong"),popupCommonAlertDimiss, "");
   loggerCall();
    kony.application.dismissLoadingScreen();
}

function callPreSMSSendAction(){
  kony.print("callPreSMSSendAction::");
  gblBeforeUpdateSMSNumber = false;
  var langSelected = kony.store.getItem("langPrefObj");
  kony.print("langSelected"+langSelected);
  var lang = langSelected.toUpperCase();
  var MSG = "";
  kony.print("gblProfileCustmerName ::"+gblProfileCustmerName);
  if(lang == "EN"){
    MSG = gblEN_CNGSMS_MSG_BFR.format(gblProfileCustmerName,frmRegisterUser.txtMobileNumber.text);
  }else{
    MSG = gblAR_CNGSMS_MSG_BFR.format(gblProfileCustmerName,frmRegisterUser.txtMobileNumber.text);
  }
  callRequestUsrname(MSG);
}





function callProfileServiceforCustName(){
 
  
  if(isEmpty(gblProfileCustmerName)){
     kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
    	var queryParams = {"custId":custid,
                       "language":kony.store.getItem("langPrefObj")=="en"?"eng":"ara"};
        var appMFConfiguration = kony.sdk.getCurrentInstance().getIntegrationService("BOJGetProfileData");
        appMFConfiguration.invokeOperation("prGetCustDetails", {},queryParams,function(response){kony.print("Success ::"+JSON.stringify(response));
                                                                                                 if (response !== undefined && response.custData !== undefined && response.custData[0] !== null) {
                                                                                                   	response.custData[0].maskedSMSno = mask_MobileNumber(response.custData[0].smsno);
                                                                                                   	customerAccountDetails.profileDetails = response.custData[0];
                                                                                                   kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
                                                                                                  	gblProfileCustmerName = customerAccountDetails.profileDetails.first_name;
                                                                                                 }	},function(err){kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen(); kony.print("Failed to get status ::"+err);
                                                                                                               //customAlertPopup("",  geti18Value("i18n.common.unabletoretirvesmsno"),popupCommonAlertDimiss, "");
                                                                                                                   });
    }
}




function appendPhoneNumberPlus(aa){
  kony.print("len"+aa.length);
if(aa.length == 1){
if(aa.indexOf("+") < 0){
aa = "+" + aa;
}else{
aa = aa.replaceAll("+","");
}
}else if(aa.length == 5){
  if(aa.indexOf(" ") < 0){
    aa = aa.substring(0,4);
    kony.print("aaa"+aa);
  }else{
    aa = aa.substring(0,4)+ " " + aa.substring(4,5);
  }
}
kony.print(aa);
  return aa;
}

function validate_CHANGE_MOBILE_SMS_NUMBER(){
	try{
    	var isValid = true;
    	if(isEmpty(frmRegisterUser.txtCountryCode.text)){
        	frmRegisterUser.flxLine2.skin = "sknFlxOrangeLine";
        	isValid = false;
        }else{
          frmRegisterUser.flxLine2.skin = "sknFlxGreenLine";
        }
    	if(isEmpty(frmRegisterUser.txtMobileNumber.text)){
        	frmRegisterUser.CopyflxLine0a4214bb8a7c146.skin = "sknFlxOrangeLine";
        	isValid = false;
        }else{
          frmRegisterUser.CopyflxLine0a4214bb8a7c146.skin = "sknFlxGreenLine";  
        }
    	
    	return isValid;
    }catch(e){
    	kony.print("Exception_validate_CHANGE_MOBILE_SMS_NUMBER ::"+e);
    }
}