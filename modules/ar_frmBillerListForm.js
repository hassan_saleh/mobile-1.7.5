//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmBillerListFormAr() {
frmBillerListForm.setDefaultUnit(kony.flex.DP);
var flxHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "15%",
"id": "flxHeader",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "CopyslFbox0e46317489b7742",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxHeader.setDefaultUnit(kony.flex.DP);
var flxTop = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "55%",
"id": "flxTop",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxTop.setDefaultUnit(kony.flex.DP);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_c775a9db04754ed08d53536b5d8138ad,
"skin": "slFbox",
"width": "20%",
"zIndex": 1
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBack = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "login screen"
},
"centerY": "50%",
"id": "lblBack",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBack.add(lblBackIcon, lblBack);
var lblTitle = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "lblTitle",
"isVisible": true,
"left": "144dp",
"maxNumberOfLines": 1,
"skin": "lblAmountCurrency",
"text": "Service Type",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "60%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxTop.add(flxBack, lblTitle);
var flxSearch = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "45%",
"id": "flxSearch",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknFlxSearch",
"top": "0dp",
"width": "100%",
"zIndex": 2
}, {}, {});
flxSearch.setDefaultUnit(kony.flex.DP);
var flxSearchHolder = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"centerY": "50%",
"clipBounds": true,
"height": "96%",
"id": "flxSearchHolder",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"skin": "slFbox",
"top": "0%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxSearchHolder.setDefaultUnit(kony.flex.DP);
var lblSearchImg = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblSearchImg",
"isVisible": true,
"right": "0%",
"skin": "sknSearchIcon",
"text": "h",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "8%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var tbxSearch = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"centerY": "50%",
"focusSkin": "sknTxtBoxSearch",
"height": "100%",
"id": "tbxSearch",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "0%",
"onDone": AS_TextField_afb8098dd86646adb2bd2750f8645807,
"onTextChange": AS_TextField_e6ec3d5b87ae434fa632b2ce56460c57,
"placeholder": kony.i18n.getLocalizedString("i18n.transfer.searchPlh"),
"secureTextEntry": false,
"skin": "sknTxtBoxSearch",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "92%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
"placeholderSkin": "sknPlaceHolder",
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
flxSearchHolder.add( tbxSearch,lblSearchImg);
var flxUnderLineSearch = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "3%",
"id": "flxUnderLineSearch",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknFlxGreyLine",
"top": "90%",
"width": "90%",
"zIndex": 1
}, {}, {});
flxUnderLineSearch.setDefaultUnit(kony.flex.DP);
flxUnderLineSearch.add();
flxSearch.add(flxSearchHolder, flxUnderLineSearch);
flxHeader.add(flxTop, flxSearch);
var flxBiller = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "85%",
"id": "flxBiller",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknFlxBackgrounf",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxBiller.setDefaultUnit(kony.flex.DP);
var segDetails = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"data": [{
"lblData": "Mix Voucher",
"lblInitial": ""
}, {
"lblData": "Cancer Institute",
"lblInitial": ""
}, {
"lblData": "Univeristy",
"lblInitial": ""
}],
"groupCells": false,
"height": "96%",
"id": "segDetails",
"isVisible": true,
"right": "0dp",
"needPageIndicator": true,
"onRowClick": AS_Segment_h7206b918d694d1a9eca61b86efd1a18,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "seg2Focus",
"rowSkin": "Copyseg0e6ede8359e2f45",
"rowTemplate": CopyFlexContainer0cb881afe06de49,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "023a6900",
"separatorRequired": true,
"separatorThickness": 1,
"showScrollbars": false,
"top": "0dp",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"CopyFlexContainer0cb881afe06de49": "CopyFlexContainer0cb881afe06de49",
"flxIcon1": "flxIcon1",
"lblData": "lblData",
"lblInitial": "lblInitial"
},
"width": "100%",
"zIndex": 1
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxBiller.add(segDetails);
frmBillerListForm.add(flxHeader, flxBiller);
};
function frmBillerListFormGlobalsAr() {
frmBillerListFormAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmBillerListFormAr,
"enableScrolling": false,
"enabledForIdleTimeout": true,
"id": "frmBillerListForm",
"layoutType": kony.flex.FLOW_VERTICAL,
"needAppMenu": false,
"preShow": AS_Form_i297d32c182e45ccbf1ab62d369ad215,
"skin": "sknSuccessBkg"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FLOW_VERTICAL,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_f84c83bd42114fd79e1977eef0196902,
"retainScrollPosition": false,
"titleBar": true,
"titleBarSkin": "slTitleBar",
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
