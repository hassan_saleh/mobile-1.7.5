//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmEnlargeAdKAAr() {
    frmEnlargeAdKA.setDefaultUnit(kony.flex.DP);
    var flxEnlargeAd = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxEnlargeAd",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "sknWhiteDivider100",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxEnlargeAd.setDefaultUnit(kony.flex.DP);
    var imgEnlargeAd = new kony.ui.Image2({
        "centerX": "50%",
        "height": "86.26%",
        "id": "imgEnlargeAd",
        "imageWhenFailed": "failedimagered.png",
        "imageWhileDownloading": "loader2.gif",
        "isVisible": false,
        "onDownloadComplete": AS_Image_j1941e9965114749842b77ebd4bfc3ae,
        "skin": "slImage",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    var imgCloseEnlargedAd = new kony.ui.Image2({
        "height": "35dp",
        "id": "imgCloseEnlargedAd",
        "isVisible": true,
        "onTouchStart": AS_Image_e16d087d9bee49499c7a7df64ea5a8a8,
        "left": "12dp",
        "skin": "slImage",
        "src": "close_ad.png",
        "top": "12dp",
        "width": "35dp",
        "zIndex": 20
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    var flxImgOnClickArea = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "80%",
        "id": "flxImgOnClickArea",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "onTouchStart": AS_FlexContainer_e6c1ac34c7664974a1a5ff407eb9b47e,
        "skin": "slFbox",
        "top": "47dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxImgOnClickArea.setDefaultUnit(kony.flex.DP);
    flxImgOnClickArea.add();
    var imgFailedEnlargedAdKA = new kony.ui.Image2({
        "height": "86.20%",
        "id": "imgFailedEnlargedAdKA",
        "isVisible": true,
        "right": "0dp",
        "skin": "slImage",
        "src": "loader2.gif",
        "top": "0dp",
        "width": "100%",
        "zIndex": 10
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    flxEnlargeAd.add(imgEnlargeAd, imgCloseEnlargedAd, flxImgOnClickArea, imgFailedEnlargedAdKA);
    var flxAdAction = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": 0,
        "clipBounds": true,
        "height": "94dp",
        "id": "flxAdAction",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "sknWhiteDivider100",
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    flxAdAction.setDefaultUnit(kony.flex.DP);
    var btnAdAction = new kony.ui.Button({
        "bottom": "35dp",
        "centerX": "50%",
        "focusSkin": "sknBtnAdAction2FocusKA",
        "height": "42dp",
        "id": "btnAdAction",
        "isVisible": true,
        "onClick": AS_Button_ceee1218143e47c8ba20dc8637f1b574,
        "skin": "sknBtnAdAction2KA",
        "text": "Sample Ad Action",
        "width": "75%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    flxAdAction.add(btnAdAction);
    frmEnlargeAdKA.add(flxEnlargeAd, flxAdAction);
};
function frmEnlargeAdKAGlobalsAr() {
    frmEnlargeAdKAAr = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmEnlargeAdKAAr,
        "enabledForIdleTimeout": true,
        "id": "frmEnlargeAdKA",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "preShow": AS_Form_b4fc4c8f91964230be818a95e5337d1b,
        "skin": "slForm"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_h942d7c1984a41c0bd7682ff5709b51b,
        "retainScrollPosition": false,
        "titleBar": false,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};
