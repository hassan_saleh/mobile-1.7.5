//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function initializetempCurrencySectionHeaderAr() {
    flxHeadreAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "8%",
        "id": "flxHeadre",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknFlxBackgrounf"
    }, {}, {});
    flxHeadre.setDefaultUnit(kony.flex.DP);
    var lblSection = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblSection",
        "isVisible": true,
        "right": "15dp",
        "skin": "sknLblWhike150",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "6dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblLine0i25ba22a14b94a = new kony.ui.Label({
        "height": "1.50%",
        "id": "CopylblLine0i25ba22a14b94a",
        "isVisible": true,
        "right": "0dp",
        "skin": "lblsknToandFromAccLine",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "97%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxHeadreAr.add(lblSection, CopylblLine0i25ba22a14b94a);
}
