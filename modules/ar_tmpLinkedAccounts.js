//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function initializetmpLinkedAccountsAr() {
flxtemplateLinkedAccountsAr = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "15%",
"id": "flxtemplateLinkedAccounts",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox"
}, {}, {});
flxtemplateLinkedAccounts.setDefaultUnit(kony.flex.DP);
var flxAccountDetails = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"centerY": "50%",
"clipBounds": true,
"id": "flxAccountDetails",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"skin": "slFbox",
"width": "80%",
"zIndex": 1
}, {}, {});
flxAccountDetails.setDefaultUnit(kony.flex.DP);
var lblAccountName = new kony.ui.Label({
"id": "lblAccountName",
"isVisible": true,
"right": "5%",
"skin": "sknSIDate",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblAccountNumber = new kony.ui.Label({
"id": "lblAccountNumber",
"isVisible": true,
"right": "5%",
"skin": "CopyslLabel0e2302468ce1241",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblAccountNickName = new kony.ui.Label({
"id": "lblAccountNickName",
"isVisible": false,
"right": "5%",
"skin": "sknSIDate",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxAccountDetails.add(lblAccountName, lblAccountNumber, lblAccountNickName);
var flxAccountLinkOptions = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxAccountLinkOptions",
"isVisible": false,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "60%",
"skin": "slFbox",
"top": "0%",
"width": "40%",
"zIndex": 1
}, {}, {});
flxAccountLinkOptions.setDefaultUnit(kony.flex.DP);
var flxLinkAccounts = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25%",
"id": "flxLinkAccounts",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "12%",
"onClick": AS_FlexContainer_jb7a4cf3da704ce3a08694cac170d323,
"skin": "slFbox",
"width": "20%",
"zIndex": 1
}, {}, {});
flxLinkAccounts.setDefaultUnit(kony.flex.DP);
var lblLinkAccounts = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "lblLinkAccounts",
"isVisible": true,
"skin": "sknBOJttfwhitee",
"text": "q",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxLinkAccounts.add(lblLinkAccounts);
var flxDivider = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "35%",
"id": "flxDivider",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "20%",
"skin": "sknwhiteDivider",
"width": "1%",
"zIndex": 1
}, {}, {});
flxDivider.setDefaultUnit(kony.flex.DP);
flxDivider.add();
var flxDefaultAccounts = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "30%",
"id": "flxDefaultAccounts",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "20%",
"onClick": AS_FlexContainer_d565feb9c5704fd49be75a6888e80da2,
"skin": "slFbox",
"width": "20%",
"zIndex": 1
}, {}, {});
flxDefaultAccounts.setDefaultUnit(kony.flex.DP);
var lblDefaultAccounts = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "lblDefaultAccounts",
"isVisible": true,
"skin": "sknBOJttfwhitee",
"text": "s",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxDefaultAccounts.add(lblDefaultAccounts);
flxAccountLinkOptions.add( flxDefaultAccounts, flxDivider,flxLinkAccounts);
var flxLinkedAccountsEnable = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"focusSkin": "sknflxyellow",
"height": "25dp",
"id": "flxLinkedAccountsEnable",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_jb7a4cf3da704ce3a08694cac170d323,
"left": "5%",
"skin": "sknflxyellow",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxLinkedAccountsEnable.setDefaultUnit(kony.flex.DP);
var flxInnerEnabled = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "45%",
"clipBounds": true,
"height": "25dp",
"id": "flxInnerEnabled",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "sknFlxDarkBlueRoundCorner",
"top": "0dp",
"width": "25dp",
"zIndex": 1
}, {}, {});
flxInnerEnabled.setDefaultUnit(kony.flex.DP);
flxInnerEnabled.add();
var flxLineEnabled = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "45%",
"clipBounds": true,
"height": "14dp",
"id": "flxLineEnabled",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10dp",
"skin": "sknLineDarkBlue",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
flxLineEnabled.setDefaultUnit(kony.flex.DP);
flxLineEnabled.add();
flxLinkedAccountsEnable.add(flxInnerEnabled, flxLineEnabled);
var flxLinkedAccountsDisabled = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"focusSkin": "sknflxWhiteOpacity60",
"height": "25dp",
"id": "flxLinkedAccountsDisabled",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_jb7a4cf3da704ce3a08694cac170d323,
"left": "5%",
"skin": "sknflxWhiteOpacity60",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxLinkedAccountsDisabled.setDefaultUnit(kony.flex.DP);
var flxInnerDisabled = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "45%",
"clipBounds": true,
"height": "25dp",
"id": "flxInnerDisabled",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknFlxDarkBlueRoundCornerGrey",
"top": "0dp",
"width": "25dp",
"zIndex": 1
}, {}, {});
flxInnerDisabled.setDefaultUnit(kony.flex.DP);
flxInnerDisabled.add();
var flxILineDisabled = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "45%",
"clipBounds": true,
"height": "14dp",
"id": "flxILineDisabled",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 10,
"skin": "sknLineDarkBlueOpacity60",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
flxILineDisabled.setDefaultUnit(kony.flex.DP);
flxILineDisabled.add();
flxLinkedAccountsDisabled.add(flxInnerDisabled, flxILineDisabled);
var Copydivider0c263b496ea8345 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "2dp",
"id": "Copydivider0c263b496ea8345",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
Copydivider0c263b496ea8345.setDefaultUnit(kony.flex.DP);
Copydivider0c263b496ea8345.add();
var flxIcon1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "50dp",
"id": "flxIcon1",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": "5%",
"skin": "sknFlxToIcon",
"width": "50dp",
"zIndex": 1
}, {}, {});
flxIcon1.setDefaultUnit(kony.flex.DP);
var lblTick = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"height": "80%",
"id": "lblTick",
"isVisible": true,
"skin": "sknBOJttfwhitee150",
"text": "r",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxIcon1.add(lblTick);
flxtemplateLinkedAccountsAr.add(flxAccountDetails, flxAccountLinkOptions, flxLinkedAccountsEnable, flxLinkedAccountsDisabled, Copydivider0c263b496ea8345, flxIcon1);
}
