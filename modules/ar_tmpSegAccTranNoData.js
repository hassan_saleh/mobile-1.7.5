//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function initializetmpSegAccTranNoDataAr() {
    flxNoDataAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "63dp",
        "id": "flxNoData",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "right": "0dp",
        "skin": "flxsegBg",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxNoData.setDefaultUnit(kony.flex.DP);
    var lblNoData = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblNoData",
        "isVisible": true,
        "right": "15dp",
        "skin": "lblSegName",
        "text": kony.i18n.getLocalizedString("i18n.accounts.noTransaction"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "43%",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxNoDataAr.add(lblNoData);
}
