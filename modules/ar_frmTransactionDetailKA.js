//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmTransactionDetailKAAr() {
frmTransactionDetailKA.setDefaultUnit(kony.flex.DP);
var topWrapper = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "topWrapper",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
topWrapper.setDefaultUnit(kony.flex.DP);
var androidTitleBar = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "androidTitleBar",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
androidTitleBar.setDefaultUnit(kony.flex.DP);
var androidTitleLabel = new kony.ui.Label({
"centerY": "50%",
"id": "androidTitleLabel",
"isVisible": true,
"right": "55dp",
"skin": "sknnavBarTitle",
"text": kony.i18n.getLocalizedString("i18n.common.transactionDetails"),
"width": "70%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var androidBack = new kony.ui.Button({
"focusSkin": "sknandroidBackButtonFocus",
"height": "50dp",
"id": "androidBack",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_a9ea2e47f8be4c128901233a7754b91d,
"skin": "sknandroidBackButton",
"top": "0dp",
"width": "50dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
androidTitleBar.add(androidTitleLabel, androidBack);
topWrapper.add(androidTitleBar);
var mainContent = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bottom": 0,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "100%",
"horizontalScrollIndicator": true,
"id": "mainContent",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "sknscrollBkg",
"top": "0dp",
"verticalScrollIndicator": false,
"width": "100%",
"zIndex": 1
}, {}, {});
mainContent.setDefaultUnit(kony.flex.DP);
var FlexContainer042b0725667e643 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "145dp",
"id": "FlexContainer042b0725667e643",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
FlexContainer042b0725667e643.setDefaultUnit(kony.flex.DP);
var FlexContainer0f615b714fea843 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "100%",
"id": "FlexContainer0f615b714fea843",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "80%",
"zIndex": 1
}, {}, {});
FlexContainer0f615b714fea843.setDefaultUnit(kony.flex.DP);
var transactionAmount = new kony.ui.Label({
"centerX": "50%",
"id": "transactionAmount",
"isVisible": true,
"skin": "skndetailPageNumber",
"text": "$500.00",
"top": "30dp",
"width": "80%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var dummytransactionAmount = new kony.ui.Label({
"centerX": "50%",
"id": "dummytransactionAmount",
"isVisible": false,
"skin": "skndetailPageNumber",
"text": "$500.00",
"top": "30dp",
"width": "80%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var transactionName = new kony.ui.Label({
"centerX": "50%",
"id": "transactionName",
"isVisible": true,
"skin": "sknNumber",
"text": "Payment to City of Austin",
"top": "5dp",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var transactionDate = new kony.ui.Label({
"centerX": "50%",
"id": "transactionDate",
"isVisible": true,
"skin": "skndetailPageDate",
"text": "Jan 29, 2016",
"top": "3dp",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var dummytransactionDate = new kony.ui.Label({
"centerX": "50%",
"id": "dummytransactionDate",
"isVisible": false,
"skin": "skndetailPageDate",
"text": "Jan 29, 2016",
"top": "3dp",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
FlexContainer0f615b714fea843.add(transactionAmount, dummytransactionAmount, transactionName, transactionDate, dummytransactionDate);
var divider = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "divider",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
divider.setDefaultUnit(kony.flex.DP);
divider.add();
FlexContainer042b0725667e643.add(FlexContainer0f615b714fea843, divider);
var additionalDetails1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "80dp",
"id": "additionalDetails1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
additionalDetails1.setDefaultUnit(kony.flex.DP);
var Label0e7a26666ebf141 = new kony.ui.Label({
"id": "Label0e7a26666ebf141",
"isVisible": true,
"right": "5%",
"skin": "sknsegmentHeaderText",
"text": kony.i18n.getLocalizedString("i18n.common.fromc"),
"top": "20dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var transactionFrom = new kony.ui.Label({
"id": "transactionFrom",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "Savings Account 3465",
"top": "40dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var divider2 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "divider2",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
divider2.setDefaultUnit(kony.flex.DP);
divider2.add();
additionalDetails1.add(Label0e7a26666ebf141, transactionFrom, divider2);
var CopynotesWrapper03de47f39b8bc46 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "80dp",
"id": "CopynotesWrapper03de47f39b8bc46",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopynotesWrapper03de47f39b8bc46.setDefaultUnit(kony.flex.DP);
var CopyLabel07f3ee643ff4e47 = new kony.ui.Label({
"id": "CopyLabel07f3ee643ff4e47",
"isVisible": true,
"right": "5%",
"skin": "sknsegmentHeaderText",
"text": kony.i18n.getLocalizedString("i18n.common.notesc"),
"top": "20dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var transactionNotes = new kony.ui.Label({
"id": "transactionNotes",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "Note Details appear here.This can be a multiple line description",
"top": "40dp",
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copydivider06e184b7d586e4a = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider06e184b7d586e4a",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
Copydivider06e184b7d586e4a.setDefaultUnit(kony.flex.DP);
Copydivider06e184b7d586e4a.add();
CopynotesWrapper03de47f39b8bc46.add(CopyLabel07f3ee643ff4e47, transactionNotes, Copydivider06e184b7d586e4a);
var CopynotesWrapper0accf5ab04b6940 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "85dp",
"id": "CopynotesWrapper0accf5ab04b6940",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopynotesWrapper0accf5ab04b6940.setDefaultUnit(kony.flex.DP);
var CopyLabel000cf6571804e4e = new kony.ui.Label({
"id": "CopyLabel000cf6571804e4e",
"isVisible": true,
"right": "5%",
"skin": "sknsegmentHeaderText",
"text": kony.i18n.getLocalizedString("i18n.my_money.mapToACategory"),
"top": "20dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var hiddencategoryIdKA = new kony.ui.Label({
"id": "hiddencategoryIdKA",
"isVisible": false,
"right": "15%",
"skin": "sknsegmentHeaderText",
"text": "Map to a category",
"top": "30dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copydivider0828a7be62f8a4a = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider0828a7be62f8a4a",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
Copydivider0828a7be62f8a4a.setDefaultUnit(kony.flex.DP);
Copydivider0828a7be62f8a4a.add();
var ListBox001b478bb90954f = new kony.ui.ListBox({
"height": "40dp",
"id": "ListBox001b478bb90954f",
"isVisible": true,
"right": "20dp",
"masterData": [["lb1", kony.i18n.getLocalizedString("i18n.my_money.frmTransactionDetailKA.ListBox001b478bb90954f.lb1")],["lb2", kony.i18n.getLocalizedString("i18n.my_money.frmTransactionDetailKA.ListBox001b478bb90954f.lb2")],["lb3", kony.i18n.getLocalizedString("i18n.my_money.frmTransactionDetailKA.ListBox001b478bb90954f.lb3")],["Key197", kony.i18n.getLocalizedString("i18n.my_money.frmTransactionDetailKA.ListBox001b478bb90954f.Key197")]],
"selectedKey": "lb1",
"selectedKeyValue": ["lb1", null],
"top": "40dp",
"width": "260dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"applySkinsToPopup": true,
"dropDownImage": "dropdown.png",
"viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
});
CopynotesWrapper0accf5ab04b6940.add(CopyLabel000cf6571804e4e, hiddencategoryIdKA, Copydivider0828a7be62f8a4a, ListBox001b478bb90954f);
var preferredAccountEnable = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "preferredAccountEnable",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
preferredAccountEnable.setDefaultUnit(kony.flex.DP);
var CheckBoxGroup0aebf676b3db246 = new kony.ui.CheckBoxGroup({
"height": "35dp",
"id": "CheckBoxGroup0aebf676b3db246",
"isVisible": true,
"right": "5%",
"masterData": [["true", "Include in analysis"]],
"selectedKeyValues": [
["true", "Include in analysis"]
],
"selectedKeys": ["true"],
"skin": "sknCopyslCheckBoxGroup0f2b152e3c3d441",
"top": "7dp",
"width": "80%",
"zIndex": 1
}, {
"itemOrientation": constants.CHECKBOX_ITEM_ORIENTATION_VERTICAL,
"padding": [ 0, 0,4, 0],
"paddingInPixel": false
}, {
"tickedImage": "checkbox_selected.png",
"untickedImage": "checkbox_unselected.png"
});
var hiddenIncludeInAnalysis = new kony.ui.Label({
"height": "32dp",
"id": "hiddenIncludeInAnalysis",
"isVisible": false,
"right": "15%",
"skin": "sknsectionHeaderLabel",
"text": kony.i18n.getLocalizedString("i18n.my_money.includeInAnalysis"),
"top": "20dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copydivider00b787fb89f2841 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider00b787fb89f2841",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
Copydivider00b787fb89f2841.setDefaultUnit(kony.flex.DP);
Copydivider00b787fb89f2841.add();
preferredAccountEnable.add(CheckBoxGroup0aebf676b3db246, hiddenIncludeInAnalysis, Copydivider00b787fb89f2841);
var CopypreferredAccountEnable03b3617d7d0094f = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "CopypreferredAccountEnable03b3617d7d0094f",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopypreferredAccountEnable03b3617d7d0094f.setDefaultUnit(kony.flex.DP);
var CopyCheckBoxGroup0d9fa019582e647 = new kony.ui.CheckBoxGroup({
"height": "35dp",
"id": "CopyCheckBoxGroup0d9fa019582e647",
"isVisible": true,
"right": "5%",
"masterData": [["true", "Map this merchant always"]],
"selectedKeyValues": [
["true", "Map this merchant always"]
],
"selectedKeys": ["true"],
"skin": "sknCopyslCheckBoxGroup0f2b152e3c3d441",
"top": "7dp",
"width": "80%",
"zIndex": 1
}, {
"itemOrientation": constants.CHECKBOX_ITEM_ORIENTATION_VERTICAL,
"padding": [ 0, 0,4, 0],
"paddingInPixel": false
}, {
"tickedImage": "checkbox_selected.png",
"untickedImage": "checkbox_unselected.png"
});
var hiddenMaptoMerchant = new kony.ui.Label({
"height": "32dp",
"id": "hiddenMaptoMerchant",
"isVisible": false,
"right": "15%",
"skin": "sknsectionHeaderLabel",
"text": kony.i18n.getLocalizedString("i18n.my_money.mapThisMerchantAlways"),
"top": "20dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copydivider09e23dd99550542 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider09e23dd99550542",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
Copydivider09e23dd99550542.setDefaultUnit(kony.flex.DP);
Copydivider09e23dd99550542.add();
CopypreferredAccountEnable03b3617d7d0094f.add(CopyCheckBoxGroup0d9fa019582e647, hiddenMaptoMerchant, Copydivider09e23dd99550542);
var signInButton = new kony.ui.Button({
"centerX": "50.00%",
"focusSkin": "sknprimaryActionFocus",
"height": "38dp",
"id": "signInButton",
"isVisible": true,
"onClick": AS_Button_d8d9cafbbc444d47af671a8d5632ef9c,
"skin": "sknprimaryAction",
"text": kony.i18n.getLocalizedString("i18n.my_money.map"),
"top": "14dp",
"width": "80%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
mainContent.add(FlexContainer042b0725667e643, additionalDetails1, CopynotesWrapper03de47f39b8bc46, CopynotesWrapper0accf5ab04b6940, preferredAccountEnable, CopypreferredAccountEnable03b3617d7d0094f, signInButton);
var flxHeaderTransaferedStatusDetails = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "8%",
"id": "flxHeaderTransaferedStatusDetails",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "sknFlxSearch",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxHeaderTransaferedStatusDetails.setDefaultUnit(kony.flex.DP);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_d2ad7610b0154381a21ac4a56f336fcc,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 1
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBack = new kony.ui.Label({
"centerY": "50%",
"id": "lblBack",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBack.add(lblBackIcon, lblBack);
var lblTitle = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "lblTitle",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.common.transferstatusdetails"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxHeaderTransaferedStatusDetails.add(flxBack, lblTitle);
var flxTransferedStatusDetailsBody = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "92%",
"horizontalScrollIndicator": true,
"id": "flxTransferedStatusDetailsBody",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "0%",
"verticalScrollIndicator": false,
"width": "100%",
"zIndex": 1
}, {}, {});
flxTransferedStatusDetailsBody.setDefaultUnit(kony.flex.DP);
var lblPayeeNameTitle = new kony.ui.Label({
"id": "lblPayeeNameTitle",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.transfer.payeeNameC"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "2%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblPayeeName = new kony.ui.Label({
"id": "lblPayeeName",
"isVisible": true,
"right": "5%",
"skin": "sknLblBack",
"text": "RAEID WAFA HASAN ABD RABU\r",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxAccounts = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxAccounts",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "1%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAccounts.setDefaultUnit(kony.flex.DP);
var lblPayeeAccountTitle = new kony.ui.Label({
"id": "lblPayeeAccountTitle",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.common.payeeaccount"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "2%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblPayeeAccount = new kony.ui.Label({
"id": "lblPayeeAccount",
"isVisible": true,
"right": "5%",
"skin": "sknLblBack",
"text": "0013010050133000",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxAccounts.add(lblPayeeAccountTitle, lblPayeeAccount);
var lblCreditAccountTitle = new kony.ui.Label({
"id": "lblCreditAccountTitle",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": "PayeeAccount",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCreditAccount = new kony.ui.Label({
"id": "lblCreditAccount",
"isVisible": true,
"right": "5%",
"skin": "sknLblBack",
"text": "0013010050133000",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblReferenceIdTitle = new kony.ui.Label({
"id": "lblReferenceIdTitle",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.common.ReferenceNumber"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblReferenceId = new kony.ui.Label({
"id": "lblReferenceId",
"isVisible": true,
"right": "5%",
"skin": "sknLblBack",
"text": "632481",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxAmount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxAmount",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "1%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAmount.setDefaultUnit(kony.flex.DP);
var lblAmountTitle = new kony.ui.Label({
"id": "lblAmountTitle",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.accounts.amount"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "2%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblAmount = new kony.ui.Label({
"id": "lblAmount",
"isVisible": true,
"right": "5%",
"skin": "sknLblBack",
"text": "928.703 JOD",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblFeesTitle = new kony.ui.Label({
"id": "lblFeesTitle",
"isVisible": true,
"right": "55%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.opening_account.fees"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "2%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblFees = new kony.ui.Label({
"id": "lblFees",
"isVisible": true,
"right": "55%",
"skin": "sknLblBack",
"text": "5 JOD",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxAmount.add(lblAmountTitle, lblAmount, lblFeesTitle, lblFees);
var flxDate = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "10%",
"id": "flxDate",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "1%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxDate.setDefaultUnit(kony.flex.DP);
var lblTransactionDateTitle = new kony.ui.Label({
"id": "lblTransactionDateTitle",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.common.transactiondate"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "2%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblTransactionDate = new kony.ui.Label({
"id": "lblTransactionDate",
"isVisible": true,
"right": "5%",
"skin": "sknLblBack",
"text": "2013-10-07",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblTransactionEffectedDateTitle = new kony.ui.Label({
"id": "lblTransactionEffectedDateTitle",
"isVisible": true,
"right": "55%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.common.effecteddate"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "2%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblTransactionEffectedDate = new kony.ui.Label({
"id": "lblTransactionEffectedDate",
"isVisible": true,
"right": "55%",
"skin": "sknLblBack",
"text": "2013-10-07",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxDate.add(lblTransactionDateTitle, lblTransactionDate, lblTransactionEffectedDateTitle, lblTransactionEffectedDate);
var lblPurposeTitle = new kony.ui.Label({
"id": "lblPurposeTitle",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.confirmdetails.purposeoftransfer"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblPurpose = new kony.ui.Label({
"id": "lblPurpose",
"isVisible": true,
"right": "5%",
"skin": "sknLblBack",
"text": "Internal Transaction",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBankTitle = new kony.ui.Label({
"id": "lblBankTitle",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.Bene.Bankname"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "2%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBank = new kony.ui.Label({
"id": "lblBank",
"isVisible": true,
"right": "5%",
"skin": "sknLblBack",
"text": "AL RAJHI BANK",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBranchTitle = new kony.ui.Label({
"id": "lblBranchTitle",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.Map.Branchname"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "2%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBranch = new kony.ui.Label({
"id": "lblBranch",
"isVisible": true,
"right": "5%",
"skin": "sknLblBack",
"text": "JORDAN BRANCH",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxBankAddress = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxBankAddress",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"skin": "slFbox",
"top": "1%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxBankAddress.setDefaultUnit(kony.flex.DP);
var lblBankAddressTitle = new kony.ui.Label({
"id": "lblBankAddressTitle",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.Bene.BankAddress"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "2%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBankAddress1 = new kony.ui.Label({
"id": "lblBankAddress1",
"isVisible": true,
"right": "5%",
"skin": "sknLblBack",
"text": "JORDAN BRANCH",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "3%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBankAddress2 = new kony.ui.Label({
"id": "lblBankAddress2",
"isVisible": true,
"right": "5%",
"skin": "sknLblBack",
"text": "JORDAN BRANCH",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBankAddress3 = new kony.ui.Label({
"id": "lblBankAddress3",
"isVisible": true,
"right": "5%",
"skin": "sknLblBack",
"text": "JORDAN BRANCH",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBankAddress.add(lblBankAddressTitle, lblBankAddress1, lblBankAddress2, lblBankAddress3);
var flxBeneficiaryAddress = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxBeneficiaryAddress",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"skin": "slFbox",
"top": "1%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxBeneficiaryAddress.setDefaultUnit(kony.flex.DP);
var lblBeneficaryAddressTitle = new kony.ui.Label({
"id": "lblBeneficaryAddressTitle",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.Bene.BeneficiaryAddress"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "2%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBeneficaryAddress1 = new kony.ui.Label({
"id": "lblBeneficaryAddress1",
"isVisible": true,
"right": "5%",
"skin": "sknLblBack",
"text": "JORDAN BRANCH",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "3%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBeneficaryAddress2 = new kony.ui.Label({
"id": "lblBeneficaryAddress2",
"isVisible": true,
"right": "5%",
"skin": "sknLblBack",
"text": "JORDAN BRANCH",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBeneficaryAddress3 = new kony.ui.Label({
"id": "lblBeneficaryAddress3",
"isVisible": true,
"right": "5%",
"skin": "sknLblBack",
"text": "JORDAN BRANCH",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBeneficiaryAddress.add(lblBeneficaryAddressTitle, lblBeneficaryAddress1, lblBeneficaryAddress2, lblBeneficaryAddress3);
var lblRemarksTitle = new kony.ui.Label({
"id": "lblRemarksTitle",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.common.remarks"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblRemarks = new kony.ui.Label({
"id": "lblRemarks",
"isVisible": true,
"right": "5%",
"skin": "sknLblBack",
"text": "AL RAJHI BANK , JORDAN BRANCH",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblNotesTitle = new kony.ui.Label({
"id": "lblNotesTitle",
"isVisible": true,
"right": "5%",
"skin": "sknLblNextDisabled",
"text": kony.i18n.getLocalizedString("i18n.newCard.notes"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "1%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblNotes = new kony.ui.Label({
"id": "lblNotes",
"isVisible": true,
"right": "5%",
"skin": "sknLblBack",
"text": "AL RAJHI BANK , JORDAN BRANCH",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxTransferedStatusDetailsBody.add(lblPayeeNameTitle, lblPayeeName, flxAccounts, lblCreditAccountTitle, lblCreditAccount, lblReferenceIdTitle, lblReferenceId, flxAmount, flxDate, lblPurposeTitle, lblPurpose, lblBankTitle, lblBank, lblBranchTitle, lblBranch, flxBankAddress, flxBeneficiaryAddress, lblRemarksTitle, lblRemarks, lblNotesTitle, lblNotes);
frmTransactionDetailKA.add(topWrapper, mainContent, flxHeaderTransaferedStatusDetails, flxTransferedStatusDetailsBody);
};
function frmTransactionDetailKAGlobalsAr() {
frmTransactionDetailKAAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmTransactionDetailKAAr,
"bounces": false,
"enableScrolling": true,
"enabledForIdleTimeout": true,
"id": "frmTransactionDetailKA",
"layoutType": kony.flex.FLOW_VERTICAL,
"needAppMenu": false,
"skin": "sknmainGradient"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FLOW_VERTICAL,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"inTransitionConfig": {
"formAnimation": 0
},
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_158cb54078bf442b9c605e1a9bbb4cb7,
"outTransitionConfig": {
"formAnimation": 0
},
"retainScrollPosition": false,
"titleBar": true,
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
