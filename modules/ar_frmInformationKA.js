//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmInformationKAAr() {
frmInformationKA.setDefaultUnit(kony.flex.DP);
var flxHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "s",
"top": "0dp",
"width": "100%",
"zIndex": 2
}, {}, {});
flxHeader.setDefaultUnit(kony.flex.DP);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_af10ade971ff4f5aa1a02ebf442edf55,
"skin": "slFbox",
"top": "0%",
"width": "18%",
"zIndex": 1
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblBack0i5985fbe3b634e = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"centerY": "50%",
"height": "100%",
"id": "CopylblBack0i5985fbe3b634e",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBack.add(lblBackIcon, CopylblBack0i5985fbe3b634e);
var lblInfoHeader = new kony.ui.Label({
"centerX": "50%",
"height": "90%",
"id": "lblInfoHeader",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.common.infonsupp"),
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblVersionCode = new kony.ui.Label({
"centerY": "45%",
"height": "100%",
"id": "lblVersionCode",
"isVisible": false,
"onTouchEnd": AS_Label_i2f16c11f5b84d65ab3e41c0d2ab67bc,
"right": "1%",
"skin": "sknLblWhiteCairo105",
"text": "V ",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "12%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxHeader.add(flxBack, lblInfoHeader, lblVersionCode);
var searchInformation = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "5%",
"id": "searchInformation",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "CopysknBOJblueBG0jbd7ebd48db74e",
"top": "4%",
"width": "95%"
}, {}, {});
searchInformation.setDefaultUnit(kony.flex.DP);
var btnNews = new kony.ui.Button({
"focusSkin": "sknBtnBGWhiteBlue105Rd10",
"height": "100%",
"id": "btnNews",
"isVisible": true,
"right": "0%",
"onClick": AS_Button_j4b6098d4bf54cfd9f94c42bf01e06cb,
"skin": "sknBtnBGWhiteBlue105Rd10",
"text": kony.i18n.getLocalizedString("i18n.info.newsSegment.news"),
"top": "0%",
"width": "33%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnContacts = new kony.ui.Button({
"focusSkin": "sknBtnBGBlueWhite105Rd10",
"height": "100%",
"id": "btnContacts",
"isVisible": true,
"right": "33.50%",
"onClick": AS_Button_ha6bc39e6b094af48c63cc551324dfcb,
"skin": "sknBtnBGBlueWhite105Rd10",
"text": kony.i18n.getLocalizedString("i18n.info.ContactUs"),
"top": "0dp",
"width": "33%",
"zIndex": 3
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnFAQ = new kony.ui.Button({
"focusSkin": "sknBtnBGBlueWhite105Rd10",
"height": "100%",
"id": "btnFAQ",
"isVisible": true,
"right": "67%",
"onClick": AS_Button_bca81f5826f1480bb1d7443a48538e49,
"skin": "sknBtnBGBlueWhite105Rd10",
"text": kony.i18n.getLocalizedString("i18n.opening_account.faq"),
"top": "0dp",
"width": "33%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
searchInformation.add(btnNews, btnContacts, btnFAQ);
var flcContentSivaram = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "80%",
"horizontalScrollIndicator": true,
"id": "flcContentSivaram",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "sknFlxBGBlue",
"top": "2%",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 2
}, {}, {});
flcContentSivaram.setDefaultUnit(kony.flex.DP);
var flxNewsSivaram = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "100%",
"horizontalScrollIndicator": true,
"id": "flxNewsSivaram",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "0%",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
flxNewsSivaram.setDefaultUnit(kony.flex.DP);
var lblOffersHeader = new kony.ui.Label({
"id": "lblOffersHeader",
"isVisible": false,
"right": "8%",
"skin": "sknLblWhike125",
"text": kony.i18n.getLocalizedString("i18n.common.Offers"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "0%",
"width": "84%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 2,0, 2],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxInnerNews = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "53%",
"id": "flxInnerNews",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknBOJblueBG",
"top": "2%",
"width": "100%",
"zIndex": 2
}, {}, {});
flxInnerNews.setDefaultUnit(kony.flex.DP);
var segOffersKA = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"data": [
[{
"lblHeaderKA": "Example"
},
[{
"imgNews": "yellowcard.png",
"lblDescription": "Get 5% Cash Back on your purchase when using Bank of Jordon Credit Card",
"lblHeader": "5% CashBack",
"lblSymbol": "Label"
}, {
"imgNews": "card_affluent_debit.png",
"lblDescription": "Apply for a loan to buy a taxi with a registration licence or to renew your taxi",
"lblHeader": "Taxi Loan-Renew your car",
"lblSymbol": "Label"
}, {
"imgNews": "boj_card_elite_debit_v4_01.png",
"lblDescription": "Get 10% Cash Back on your purchase when using Bank of Jordon Credit Card",
"lblHeader": "10% CashBack",
"lblSymbol": "Label"
}, {
"imgNews": "card_gold.png",
"lblDescription": "Apply for a loan to buy a taxi with a registration licence or to renew your taxi",
"lblHeader": "Taxi Loan-Renew your car",
"lblSymbol": "Label"
}, {
"imgNews": "card_silver.png",
"lblDescription": "Get 15% Cash Back on your purchase when using Bank of Jordon Credit Card",
"lblHeader": "15% CashBack",
"lblSymbol": "Label"
}]
],
[{
"lblHeaderKA": "Example"
},
[{
"imgNews": "yellowcard.png",
"lblDescription": "Get 5% Cash Back on your purchase when using Bank of Jordon Credit Card",
"lblHeader": "5% CashBack",
"lblSymbol": "Label"
}, {
"imgNews": "card_affluent_debit.png",
"lblDescription": "Apply for a loan to buy a taxi with a registration licence or to renew your taxi",
"lblHeader": "Taxi Loan-Renew your car",
"lblSymbol": "Label"
}, {
"imgNews": "boj_card_elite_debit_v4_01.png",
"lblDescription": "Get 10% Cash Back on your purchase when using Bank of Jordon Credit Card",
"lblHeader": "10% CashBack",
"lblSymbol": "Label"
}, {
"imgNews": "card_gold.png",
"lblDescription": "Apply for a loan to buy a taxi with a registration licence or to renew your taxi",
"lblHeader": "Taxi Loan-Renew your car",
"lblSymbol": "Label"
}, {
"imgNews": "card_silver.png",
"lblDescription": "Get 15% Cash Back on your purchase when using Bank of Jordon Credit Card",
"lblHeader": "15% CashBack",
"lblSymbol": "Label"
}]
]
],
"groupCells": false,
"id": "segOffersKA",
"isVisible": false,
"right": "0dp",
"needPageIndicator": true,
"onRowClick": AS_Segment_g57b7174d98a42f890b5b8d737ef0f14,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "seg2Focus",
"rowSkin": "Copyseg0f8a7b3f4fc9546",
"rowTemplate": flxInformation,
"scrollingEvents": {},
"sectionHeaderSkin": "CopysliPhoneSegmentHeader0e27d37c2968140",
"sectionHeaderTemplate": Copycontainer087f3a0994b334a,
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "052c4900",
"separatorRequired": true,
"separatorThickness": 2,
"showScrollbars": false,
"top": "3%",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"Copycontainer087f3a0994b334a": "Copycontainer087f3a0994b334a",
"contactListDivider": "contactListDivider",
"flxBody": "flxBody",
"flxInformation": "flxInformation",
"imgNews": "imgNews",
"lblDescription": "lblDescription",
"lblHeader": "lblHeader",
"lblHeaderKA": "lblHeaderKA",
"lblSymbol": "lblSymbol"
},
"width": "100%",
"zIndex": 1
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxInnerNews.add(segOffersKA);
var btnMoreOfferNews = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "sknBtnBGBlueWhite105Rd10",
"height": "5%",
"id": "btnMoreOfferNews",
"isVisible": false,
"right": "0dp",
"onClick": AS_Button_bcbee2b6fce0413481515a49daf386e6,
"skin": "SKNsHOWmORE",
"text": "Show More",
"top": "2%",
"width": "60%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 6, 0,6, 0],
"paddingInPixel": false
}, {});
var lblBankNews = new kony.ui.Label({
"id": "lblBankNews",
"isVisible": false,
"right": "8%",
"skin": "sknLblWhike125",
"text": kony.i18n.getLocalizedString("i18n.common.BankNews"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "5%",
"width": "84%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 2,0, 2],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxInnerBankNews = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "30%",
"id": "flxInnerBankNews",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "10dp",
"width": "100%",
"zIndex": 2
}, {}, {});
flxInnerBankNews.setDefaultUnit(kony.flex.DP);
var segBankNewsKA = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"data": [
[{
"lblHeaderKA": "Example"
},
[{
"imgNews": "bg.png",
"lblDescription": "Open your savings account in Bank Of Jordon and you have a grreat chance to be the lucky winner of our grand monthly prize",
"lblHeader": "A Quarter million JD ",
"lblSymbol": "Label"
}]
],
[{
"lblHeaderKA": "Example"
},
[{
"imgNews": "bg.png",
"lblDescription": "Open your savings account in Bank Of Jordon and you have a grreat chance to be the lucky winner of our grand monthly prize",
"lblHeader": "A Quarter million JD ",
"lblSymbol": "Label"
}]
]
],
"groupCells": false,
"id": "segBankNewsKA",
"isVisible": true,
"right": "0dp",
"needPageIndicator": true,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "seg2Focus",
"rowSkin": "seg2Normal",
"rowTemplate": flxInformation,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"sectionHeaderTemplate": Copycontainer087f3a0994b334a,
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "052c4900",
"separatorRequired": true,
"separatorThickness": 2,
"showScrollbars": false,
"top": "0%",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"Copycontainer087f3a0994b334a": "Copycontainer087f3a0994b334a",
"contactListDivider": "contactListDivider",
"flxBody": "flxBody",
"flxInformation": "flxInformation",
"imgNews": "imgNews",
"lblDescription": "lblDescription",
"lblHeader": "lblHeader",
"lblHeaderKA": "lblHeaderKA",
"lblSymbol": "lblSymbol"
},
"width": "100%",
"zIndex": 1
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxInnerBankNews.add(segBankNewsKA);
var btnMoreBankNews = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "sknBtnBGBlueWhite105Rd10",
"height": "5%",
"id": "btnMoreBankNews",
"isVisible": false,
"right": "0dp",
"skin": "SKNsHOWmORE",
"text": "Show 3 more",
"top": "2%",
"width": "60%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 6, 0,6, 0],
"paddingInPixel": false
}, {});
var flxNewsAndBankInfo = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "100%",
"horizontalScrollIndicator": true,
"id": "flxNewsAndBankInfo",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "0%",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
flxNewsAndBankInfo.setDefaultUnit(kony.flex.DP);
flxNewsAndBankInfo.add();
flxNewsSivaram.add(lblOffersHeader, flxInnerNews, btnMoreOfferNews, lblBankNews, flxInnerBankNews, btnMoreBankNews, flxNewsAndBankInfo);
var flxContactUsSivaram = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "100%",
"horizontalScrollIndicator": true,
"id": "flxContactUsSivaram",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "sknflxScrollBlue",
"top": "0%",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 2
}, {}, {});
flxContactUsSivaram.setDefaultUnit(kony.flex.DP);
var contactSegmentList = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"centerX": "50.00%",
"data": [{
"imgicontick": "",
"lblNameKA": ""
}],
"groupCells": false,
"id": "contactSegmentList",
"isVisible": false,
"needPageIndicator": true,
"onRowClick": AS_Segment_if528ed9752d494293d0da1b4aad1b41,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "seg2Focus",
"rowSkin": "seg2Normal",
"rowTemplate": CopyFlexContainer0b2b1c26ffbf74f,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "64646400",
"separatorRequired": false,
"separatorThickness": 1,
"showScrollbars": false,
"top": "0dp",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"CopyFlexContainer0b2b1c26ffbf74f": "CopyFlexContainer0b2b1c26ffbf74f",
"contactListDivider": "contactListDivider",
"imgicontick": "imgicontick",
"lblNameKA": "lblNameKA"
},
"width": "100%"
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var flxCallUS = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "75dp",
"id": "flxCallUS",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_a3b7a477c4db474c8a336530a9f9cf5f,
"skin": "flxBgBlueGradientRound8",
"top": "20dp",
"width": "88%",
"zIndex": 1
}, {}, {});
flxCallUS.setDefaultUnit(kony.flex.DP);
var lblCallUS = new kony.ui.Label({
"height": "35dp",
"id": "lblCallUS",
"isVisible": true,
"right": "5%",
"skin": "sknLblWhite128CR",
"text": kony.i18n.getLocalizedString("i18n.common.callus24"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "2dp",
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblMobile = new kony.ui.Label({
"id": "lblMobile",
"isVisible": true,
"right": "5%",
"skin": "sknLblWhite128C",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "35dp",
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblMobileIcon = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblMobileIcon",
"isVisible": true,
"left": "2dp",
"skin": "sknBOJttf170Yellow",
"text": "X",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "25%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxCallUS.add(lblCallUS, lblMobile, lblMobileIcon);
var flxSendUS = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "75dp",
"id": "flxSendUS",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_g8db948c50404fc5a00f2cc609ab2aa0,
"skin": "flxBgBlueGradientRound8",
"top": "20dp",
"width": "88%",
"zIndex": 1
}, {}, {});
flxSendUS.setDefaultUnit(kony.flex.DP);
var lblSendus = new kony.ui.Label({
"height": "35dp",
"id": "lblSendus",
"isVisible": true,
"right": "5%",
"skin": "sknLblWhite128CR",
"text": kony.i18n.getLocalizedString("i18n.common.sendusanemail"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "2dp",
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblEmail = new kony.ui.Label({
"id": "lblEmail",
"isVisible": true,
"right": "5%",
"skin": "sknLblWhite128C",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "35dp",
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblEmailIcon = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblEmailIcon",
"isVisible": true,
"left": "2dp",
"skin": "sknBOJttf170Yellow",
"text": "Y",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "25%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxSendUS.add(lblSendus, lblEmail, lblEmailIcon);
var flxVisitBranch = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "75dp",
"id": "flxVisitBranch",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_c318ff4ce3844b58be8589e28fd6aeba,
"skin": "flxBgBlueGradientRound8",
"top": "20dp",
"width": "88%",
"zIndex": 1
}, {}, {});
flxVisitBranch.setDefaultUnit(kony.flex.DP);
var lblVisitBranch = new kony.ui.Label({
"centerY": "50%",
"height": "35dp",
"id": "lblVisitBranch",
"isVisible": true,
"right": "5%",
"skin": "sknLblWhite128CR",
"text": kony.i18n.getLocalizedString("i18n.common.VisitBrn/atm"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "2dp",
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBranchIcon = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblBranchIcon",
"isVisible": true,
"left": "2dp",
"skin": "sknBOJttf170Yellow",
"text": "B",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "25%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxVisitBranch.add(lblVisitBranch, lblBranchIcon);
var flxHomepage = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "75dp",
"id": "flxHomepage",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_ef03458b73b84837ac1ebb70d703cca1,
"skin": "flxBgBlueGradientRound8",
"top": "20dp",
"width": "88%",
"zIndex": 1
}, {}, {});
flxHomepage.setDefaultUnit(kony.flex.DP);
var lblHomePage = new kony.ui.Label({
"centerY": "50%",
"height": "35dp",
"id": "lblHomePage",
"isVisible": true,
"right": "5%",
"skin": "sknLblWhite128CR",
"text": kony.i18n.getLocalizedString("i18n.common.gotohomepage"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "2dp",
"width": "70%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblHomepageIcon = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblHomepageIcon",
"isVisible": true,
"left": "2dp",
"skin": "sknBOJttf170Yellow",
"text": "Z",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "25%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxHomepage.add(lblHomePage, lblHomepageIcon);
var lblFollowUs = new kony.ui.Label({
"centerX": "50%",
"height": "33dp",
"id": "lblFollowUs",
"isVisible": true,
"right": "0dp",
"skin": "sknLblWhite128CR",
"text": "Follow Us",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": "100dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxFollowUs = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0dp",
"clipBounds": true,
"height": "48dp",
"id": "flxFollowUs",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "0dp",
"skin": "slFbox",
"width": "100%",
"zIndex": 1
}, {}, {});
flxFollowUs.setDefaultUnit(kony.flex.DP);
var lblFb = new kony.ui.Label({
"height": "45dp",
"id": "lblFb",
"isVisible": true,
"left": "10%",
"onTouchEnd": AS_Label_gc7fd0fdf5534f2081480769d7efe06e,
"skin": "sknBOJFont22S205BRD1",
"text": "H",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "2dp",
"width": "12%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblTwitter = new kony.ui.Label({
"height": "45dp",
"id": "lblTwitter",
"isVisible": true,
"left": "5%",
"onTouchEnd": AS_Label_d531aa289383419db0ce9d4652290743,
"skin": "sknBOJFont22S205BRD1",
"text": "T",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "2dp",
"width": "12%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblLinkedIn = new kony.ui.Label({
"height": "45dp",
"id": "lblLinkedIn",
"isVisible": true,
"left": "5%",
"onTouchEnd": AS_Label_db2e313fbb524871afa2029f6bf7e354,
"skin": "sknBOJFont22S205BRD1",
"text": "M",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "2dp",
"width": "12%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblYoutube = new kony.ui.Label({
"height": "45dp",
"id": "lblYoutube",
"isVisible": true,
"left": "5%",
"onTouchEnd": AS_Label_h5fe92477a3a47bbac91fa121e90efea,
"skin": "sknBOJFont22S205BRD1",
"text": "U",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "2dp",
"width": "12%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblInstagram = new kony.ui.Label({
"height": "45dp",
"id": "lblInstagram",
"isVisible": true,
"left": "5%",
"onTouchEnd": AS_Label_i0a600fccf2c4408a0e9d1d6ada231dc,
"skin": "sknBOJFont22S205BRD1",
"text": "J",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "2dp",
"width": "12%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxFollowUs.add(lblFb, lblTwitter, lblLinkedIn, lblYoutube, lblInstagram);
var lblVersion = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Register"
},
"id": "lblVersion",
"isVisible": true,
"maxNumberOfLines": 1,
"onTouchEnd": AS_Label_e3226fe1c534423c84e441cbced6d69d,
"left": "3%",
"skin": "sknLblWhiteCairo105",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "3%",
"width": "20%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 3, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxContactUsSivaram.add(contactSegmentList, flxCallUS, flxSendUS, flxVisitBranch, flxHomepage, lblFollowUs, flxFollowUs, lblVersion);
var flxFAQInfo = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "100%",
"horizontalScrollIndicator": true,
"id": "flxFAQInfo",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "100%",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "0%",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 2
}, {}, {});
flxFAQInfo.setDefaultUnit(kony.flex.DP);
var mainContent = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bottom": 0,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"horizontalScrollIndicator": true,
"id": "mainContent",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "sknscrollBkgGray",
"top": "50dp",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
mainContent.setDefaultUnit(kony.flex.DP);
var rTxtPrivacyPolicyKA = new kony.ui.RichText({
"id": "rTxtPrivacyPolicyKA",
"isVisible": true,
"right": "0dp",
"skin": "sknRichTxt100LR30363fKA",
"text": "<br><b>111What is bill payment?</b></br>\n\n<br>Etiam nec pulvinar dui, eget eleifend felis. Proin bibendum molestie dolor. Aenean dictum pharetra mauris, ultrices pretium nunc imperdiet in.</br>\n\n<b><br>What is remote deposit capture?</br></b>\n\n<br>Nulla dictum tincidunt turpis eu consequat. Sed adipiscing eros a nisi dictum mollis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vivamus urna lorem, porta sed congue ut, sollicitudin quis erat.</br>\n\n<b><br>What is pay a person?</br></b>\n\n<br>Vivamus massa odio, dignissim ac ante at, euismod dignissim risus. Cras ut blandit lorem. Maecenas nisl quam, cursus nec aliquet facilisis, mattis id augue. Fusce semper odio et gravida interdum. Suspendisse quis lacus nulla. Nullam et nibh ligula. Nunc vitae nulla et arcu mollis iaculis. Vestibulum venenatis risus ut ligula lacinia malesuada.</br>",
"top": "0dp",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 2, 2,5, 0],
"paddingInPixel": false
}, {});
var txtFaqKA = new kony.ui.TextArea2({
"autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
"height": "120dp",
"id": "txtFaqKA",
"isVisible": false,
"keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
"right": "37dp",
"numberOfVisibleLines": 3,
"skin": "slTextArea",
"text": "TextArea2",
"textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
"top": "0dp",
"width": "260dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 2, 2,2, 2],
"paddingInPixel": false
}, {});
mainContent.add(rTxtPrivacyPolicyKA, txtFaqKA);
var flxFAQMainContent = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bottom": 0,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "100%",
"horizontalScrollIndicator": true,
"id": "flxFAQMainContent",
"isVisible": false,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "CopysknscrollBkgGray0ia243a1bed8b4f",
"top": "0dp",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
flxFAQMainContent.setDefaultUnit(kony.flex.DP);
var flxSearch = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "20%",
"id": "flxSearch",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxSearch.setDefaultUnit(kony.flex.DP);
var txtSearchFaq = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"centerX": "50%",
"centerY": "50%",
"height": "40dp",
"id": "txtSearchFaq",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": 0,
"placeholder": "Search for Keyword",
"secureTextEntry": false,
"skin": "sknTbxTrans",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "23dp",
"width": "260dp",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
"placeholderSkin": "sknPlaceHolder",
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var imgFaqSearch = new kony.ui.Image2({
"centerY": "50%",
"height": "25dp",
"id": "imgFaqSearch",
"isVisible": true,
"right": "5%",
"skin": "sknslImage",
"src": "search.png",
"width": "25dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxSearch.add(txtSearchFaq, imgFaqSearch);
var lblHeader = new kony.ui.Label({
"id": "lblHeader",
"isVisible": false,
"right": "8%",
"skin": "sknLblWhike125",
"text": "Access to Mobile Banking",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "0%",
"width": "50%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 2,0, 2],
"paddingInPixel": false
}, {
"textCopyable": false
});
var FlexContainer0dcf0398082d044 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "40%",
"id": "FlexContainer0dcf0398082d044",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
FlexContainer0dcf0398082d044.setDefaultUnit(kony.flex.DP);
var segFaq = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"data": [
[{
"lblHeaderKA": "Example"
},
[{
"imgSearch": "dropdownlist.png",
"lblAnswer": "This Device",
"lblQuestion": "This Device "
}, {
"imgSearch": "dropdownlist.png",
"lblAnswer": "This Device",
"lblQuestion": "This Device "
}, {
"imgSearch": "dropdownlist.png",
"lblAnswer": "This Device",
"lblQuestion": "This Device "
}, {
"imgSearch": "dropdownlist.png",
"lblAnswer": "This Device",
"lblQuestion": "This Device "
}, {
"imgSearch": "dropdownlist.png",
"lblAnswer": "This Device",
"lblQuestion": "This Device "
}, {
"imgSearch": "dropdownlist.png",
"lblAnswer": "This Device",
"lblQuestion": "This Device "
}, {
"imgSearch": "dropdownlist.png",
"lblAnswer": "This Device",
"lblQuestion": "This Device "
}, {
"imgSearch": "dropdownlist.png",
"lblAnswer": "This Device",
"lblQuestion": "This Device "
}]
],
[{
"lblHeaderKA": "Example"
},
[{
"imgSearch": "dropdownlist.png",
"lblAnswer": "This Device",
"lblQuestion": "This Device "
}, {
"imgSearch": "dropdownlist.png",
"lblAnswer": "This Device",
"lblQuestion": "This Device "
}, {
"imgSearch": "dropdownlist.png",
"lblAnswer": "This Device",
"lblQuestion": "This Device "
}, {
"imgSearch": "dropdownlist.png",
"lblAnswer": "This Device",
"lblQuestion": "This Device "
}, {
"imgSearch": "dropdownlist.png",
"lblAnswer": "This Device",
"lblQuestion": "This Device "
}, {
"imgSearch": "dropdownlist.png",
"lblAnswer": "This Device",
"lblQuestion": "This Device "
}, {
"imgSearch": "dropdownlist.png",
"lblAnswer": "This Device",
"lblQuestion": "This Device "
}, {
"imgSearch": "dropdownlist.png",
"lblAnswer": "This Device",
"lblQuestion": "This Device "
}]
]
],
"groupCells": false,
"id": "segFaq",
"isVisible": true,
"right": "0dp",
"needPageIndicator": true,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "seg2Focus",
"rowSkin": "seg2Normal",
"rowTemplate": flxFAQRow,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"sectionHeaderTemplate": Copycontainer087f3a0994b334a,
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "64646400",
"separatorRequired": true,
"separatorThickness": 2,
"showScrollbars": false,
"top": "0dp",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"Copycontainer087f3a0994b334a": "Copycontainer087f3a0994b334a",
"contactListDivider": "contactListDivider",
"flxFAQRow": "flxFAQRow",
"flxQuestion": "flxQuestion",
"imgSearch": "imgSearch",
"lblAnswer": "lblAnswer",
"lblHeaderKA": "lblHeaderKA",
"lblQuestion": "lblQuestion"
},
"width": "100%",
"zIndex": 1
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
FlexContainer0dcf0398082d044.add(segFaq);
var CopybtnMoreQuestions0d027edd44dda43 = new kony.ui.Button({
"centerX": "50%",
"focusSkin": "sknBtnBGBlueWhite105Rd10",
"height": "5%",
"id": "CopybtnMoreQuestions0d027edd44dda43",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_e38ef84b3ee3450cb0998d4115a80423,
"skin": "SKNsHOWmORE",
"text": kony.i18n.getLocalizedString("i18n.common.shw3More"),
"top": "2%",
"width": "60%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 6, 0,6, 0],
"paddingInPixel": false
}, {});
var CopylblHeader0g881a3cb2ac144 = new kony.ui.Label({
"id": "CopylblHeader0g881a3cb2ac144",
"isVisible": false,
"right": "8%",
"skin": "sknLblWhike125",
"text": "Authorization",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "0%",
"width": "50%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 2,0, 2],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyFlexContainer0g4a81c2ff16e44 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "40%",
"id": "CopyFlexContainer0g4a81c2ff16e44",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "10dp",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyFlexContainer0g4a81c2ff16e44.setDefaultUnit(kony.flex.DP);
var CopysegFaq0fbe87da754cd47 = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"data": [
[{
"lblHeaderKA": "Example"
},
[{
"imgSearch": "dropdownlist.png",
"lblAnswer": "Bill Payment",
"lblQuestion": "How to activate the username ?"
}, {
"imgSearch": "dropdownlist.png",
"lblAnswer": "Yes",
"lblQuestion": "How to unlock my profile?"
}]
],
[{
"lblHeaderKA": "Example"
},
[{
"imgSearch": "dropdownlist.png",
"lblAnswer": "Bill Payment",
"lblQuestion": "How to activate the username ?"
}, {
"imgSearch": "dropdownlist.png",
"lblAnswer": "Yes",
"lblQuestion": "How to unlock my profile?"
}]
]
],
"groupCells": false,
"id": "CopysegFaq0fbe87da754cd47",
"isVisible": true,
"right": "0dp",
"needPageIndicator": true,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "seg2Focus",
"rowSkin": "seg2Normal",
"rowTemplate": flxFAQRow,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"sectionHeaderTemplate": Copycontainer087f3a0994b334a,
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "64646400",
"separatorRequired": true,
"separatorThickness": 3,
"showScrollbars": false,
"top": "0dp",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"Copycontainer087f3a0994b334a": "Copycontainer087f3a0994b334a",
"contactListDivider": "contactListDivider",
"flxFAQRow": "flxFAQRow",
"flxQuestion": "flxQuestion",
"imgSearch": "imgSearch",
"lblAnswer": "lblAnswer",
"lblHeaderKA": "lblHeaderKA",
"lblQuestion": "lblQuestion"
},
"width": "100%",
"zIndex": 1
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
CopyFlexContainer0g4a81c2ff16e44.add(CopysegFaq0fbe87da754cd47);
var btnMoreQuestions = new kony.ui.Button({
"centerX": "49%",
"focusSkin": "sknBtnBGBlueWhite105Rd10",
"height": "5%",
"id": "btnMoreQuestions",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_cf93aa7cd5ff4c2cb6fd6c1afd3c22cd,
"skin": "SKNsHOWmORE",
"text": kony.i18n.getLocalizedString("i18n.common.shw3More"),
"top": "2%",
"width": "60%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 6, 0,6, 0],
"paddingInPixel": false
}, {});
flxFAQMainContent.add(flxSearch, lblHeader, FlexContainer0dcf0398082d044, CopybtnMoreQuestions0d027edd44dda43, CopylblHeader0g881a3cb2ac144, CopyFlexContainer0g4a81c2ff16e44, btnMoreQuestions);
var flxSearchInfo = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "15%",
"horizontalScrollIndicator": true,
"id": "flxSearchInfo",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "0%",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
flxSearchInfo.setDefaultUnit(kony.flex.DP);
var searchContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "90%",
"id": "searchContainer",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknslFbox",
"top": "0%",
"width": "93%",
"zIndex": 1
}, {}, {});
searchContainer.setDefaultUnit(kony.flex.PERCENTAGE);
var searchField = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"bottom": "3%",
"focusSkin": "CopyslTextBox0c1d969ae9edf4b",
"height": "60%",
"id": "searchField",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "2%",
"maxTextLength": 20,
"onDone": AS_TextField_c0f47c11021346eda1f4faa7c146a1a6,
"placeholder": kony.i18n.getLocalizedString("i18n.common.searchText"),
"secureTextEntry": false,
"skin": "CopyslTextBox0c1d969ae9edf4b",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"width": "97%",
"zIndex": 20
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"placeholderSkin": "sknPlaceHolder",
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var borderBottom = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0%",
"centerX": "50%",
"clipBounds": true,
"height": "1dp",
"id": "borderBottom",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "skntextFieldDivider",
"width": "96%",
"zIndex": 2
}, {}, {});
borderBottom.setDefaultUnit(kony.flex.DP);
borderBottom.add();
var lblChangeUserName = new kony.ui.Label({
"id": "lblChangeUserName",
"isVisible": false,
"right": "0dp",
"skin": "sknNumber",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "7dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 99
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var noThanks = new kony.ui.Button({
"centerX": "88%",
"focusSkin": "sknsecondaryActionFocus",
"height": "26dp",
"id": "noThanks",
"isVisible": false,
"onClick": AS_Button_ff54b83f5de64bbbb1df4c97f0dc7bd7,
"skin": "sknaccountFilterButton",
"text": kony.i18n.getLocalizedString("i18n.pinLogin.change"),
"top": "0dp",
"width": "70dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var lblUserName = new kony.ui.Label({
"id": "lblUserName",
"isVisible": false,
"right": "2%",
"skin": "sknCaiRegWhite50Op",
"text": kony.i18n.getLocalizedString("i18n.login.username"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblClose = new kony.ui.Label({
"height": "100%",
"id": "lblClose",
"isVisible": true,
"onTouchEnd": AS_Label_c7b957b0c31843eb84b1a1d56f7b282a,
"left": "0%",
"skin": "sknClose",
"text": "O",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "10%",
"width": "14%",
"zIndex": 23
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
searchContainer.add(searchField, borderBottom, lblChangeUserName, noThanks, lblUserName, lblClose);
flxSearchInfo.add(searchContainer);
var flxFAQ = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "100%",
"horizontalScrollIndicator": true,
"id": "flxFAQ",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "0%",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
flxFAQ.setDefaultUnit(kony.flex.DP);
flxFAQ.add();
var flxFAQSearch = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "100%",
"horizontalScrollIndicator": true,
"id": "flxFAQSearch",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "0%",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
flxFAQSearch.setDefaultUnit(kony.flex.DP);
var segSearchedFAQ = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"data": [{
"imgSearch": "dropdownlist.png",
"lblAnswer": "",
"lblQuestion": "This Device "
}, {
"imgSearch": "dropdownlist.png",
"lblAnswer": "",
"lblQuestion": "This Device "
}, {
"imgSearch": "dropdownlist.png",
"lblAnswer": "",
"lblQuestion": "This Device "
}],
"groupCells": false,
"height": "100%",
"id": "segSearchedFAQ",
"isVisible": true,
"right": "13dp",
"needPageIndicator": true,
"onRowClick": AS_Segment_d6d4bc3b6e7e442b9f9d2a902ba925da,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "seg2Focus",
"rowSkin": "seg0b1b665cbd23642",
"rowTemplate": flxFAQRow,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "052c4900",
"separatorRequired": true,
"separatorThickness": 2,
"showScrollbars": false,
"top": "48dp",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"flxFAQRow": "flxFAQRow",
"flxQuestion": "flxQuestion",
"imgSearch": "imgSearch",
"lblAnswer": "lblAnswer",
"lblQuestion": "lblQuestion"
},
"width": "90%",
"zIndex": 1
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxFAQSearch.add(segSearchedFAQ);
flxFAQInfo.add(mainContent, flxFAQMainContent, flxSearchInfo, flxFAQ, flxFAQSearch);
flcContentSivaram.add(flxNewsSivaram, flxContactUsSivaram, flxFAQInfo);
frmInformationKA.add(flxHeader, searchInformation, flcContentSivaram);
};
function frmInformationKAGlobalsAr() {
frmInformationKAAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmInformationKAAr,
"bounces": false,
"enableScrolling": false,
"enabledForIdleTimeout": true,
"id": "frmInformationKA",
"init": AS_Form_haf7f511de1148e1b9cc2690e6727386,
"layoutType": kony.flex.FLOW_VERTICAL,
"needAppMenu": false,
"postShow": AS_Form_dcf7811eb4334e99aca48222cc2f6eb3,
"preShow": AS_Form_c178a8a53fd64652a31293d1a9547e45,
"skin": "sknmainGradient"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FLOW_VERTICAL,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": true,
"inTransitionConfig": {
"formAnimation": 2
},
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_db91454e3da441969b5a1201e65d0203,
"outTransitionConfig": {
"formAnimation": 5
},
"retainScrollPosition": false,
"titleBar": true,
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
