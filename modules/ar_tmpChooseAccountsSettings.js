//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function initializetmpChooseAccountsSettingsAr() {
    flxSegChooseAccountsAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "95dp",
        "id": "flxSegChooseAccounts",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknflxtmpAccountDetailsScreen"
    }, {}, {});
    flxSegChooseAccounts.setDefaultUnit(kony.flex.DP);
    var lblAccountName = new kony.ui.Label({
        "id": "lblAccountName",
        "isVisible": true,
        "right": "8%",
        "skin": "CopylblSegName0f5b732ec6b8142",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35dp",
        "width": "78%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var Symbol = new kony.ui.Label({
        "centerY": "50%",
        "id": "Symbol",
        "isVisible": true,
        "left": "4%",
        "skin": "sknBackIconDisabled",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.more"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAccountNumber = new kony.ui.Label({
        "id": "lblAccountNumber",
        "isVisible": true,
        "right": "8%",
        "skin": "lblsegtextsmall0b5a3b38d4be646",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "17dp",
        "width": "40%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDefaultAccPayment = new kony.ui.Label({
        "height": "45%",
        "id": "lblDefaultAccPayment",
        "isVisible": false,
        "left": "5%",
        "skin": "CopylblSegName0f5b732ec6b8142",
        "text": "Balance",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5%",
        "width": "40%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAmount = new kony.ui.Label({
        "height": "45%",
        "id": "lblAmount",
        "isVisible": false,
        "left": "3%",
        "skin": "Copylblsegtextsmall0a16789105f8b49",
        "text": "2,7453 JOD",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "5%",
        "width": "40%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblDefaultAccTransfer = new kony.ui.Label({
        "id": "lblDefaultAccTransfer",
        "isVisible": false,
        "right": "100%",
        "skin": "CopylblSegName0f5b732ec6b8142",
        "text": "Salary Account",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblACCHideShow = new kony.ui.Label({
        "id": "lblACCHideShow",
        "isVisible": false,
        "right": "100%",
        "skin": "CopylblSegName0f5b732ec6b8142",
        "text": "Salary Account",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAccNickname = new kony.ui.Label({
        "id": "lblAccNickname",
        "isVisible": false,
        "right": "100%",
        "skin": "CopylblSegName0f5b732ec6b8142",
        "text": "Salary Account",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAccName = new kony.ui.Label({
        "id": "lblAccName",
        "isVisible": false,
        "right": "100%",
        "skin": "CopylblSegName0f5b732ec6b8142",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxSegChooseAccountsAr.add(lblAccountName, Symbol, lblAccountNumber, lblDefaultAccPayment, lblAmount, lblDefaultAccTransfer, lblACCHideShow, lblAccNickname, lblAccName);
}
