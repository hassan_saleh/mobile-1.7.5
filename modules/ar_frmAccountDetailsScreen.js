//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:46 EEST 2020
function addWidgetsfrmAccountDetailsScreenAr() {
frmAccountDetailsScreen.setDefaultUnit(kony.flex.DP);
var flxMain = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "100%",
"id": "flxMain",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "CopyslFSbox0a724e40393f744",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxMain.setDefaultUnit(kony.flex.DP);
var flx = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flx",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"masterType": constants.MASTER_TYPE_USERWIDGET,
"skin": "s",
"top": "0%",
"width": "100%",
"zIndex": 10
}, {}, {});
flx.setDefaultUnit(kony.flex.DP);
var lblHead = new kony.ui.Label({
"centerX": "50.00%",
"centerY": "50%",
"height": "100%",
"id": "lblHead",
"isVisible": true,
"left": "30%",
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18.Transfer.sendmoneyto"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "20%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_iac662718cce4814ab90c37a01c81ae3,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 1
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBack = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"centerY": "50%",
"id": "lblBack",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBack.add(lblBackIcon, lblBack);
flx.add(lblHead, flxBack);
var segFre = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"data": [{
"Symbol": "B",
"imgIcon": "imagedrag.png",
"lbl3": "Balance",
"lblAccountName": "Salary Account",
"lblAccountNumber": "***00114",
"lblAmount": "2,7453 JOD"
}, {
"Symbol": "B",
"imgIcon": "imagedrag.png",
"lbl3": "Balance",
"lblAccountName": "Salary Account",
"lblAccountNumber": "***00114",
"lblAmount": "2,7453 JOD"
}, {
"Symbol": "B",
"imgIcon": "imagedrag.png",
"lbl3": "Balance",
"lblAccountName": "Salary Account",
"lblAccountNumber": "***00114",
"lblAmount": "2,7453 JOD"
}, {
"Symbol": "B",
"imgIcon": "imagedrag.png",
"lbl3": "Balance",
"lblAccountName": "Salary Account",
"lblAccountNumber": "***00114",
"lblAmount": "2,7453 JOD"
}, {
"Symbol": "B",
"imgIcon": "imagedrag.png",
"lbl3": "Balance",
"lblAccountName": "Salary Account",
"lblAccountNumber": "***00114",
"lblAmount": "2,7453 JOD"
}, {
"Symbol": "B",
"imgIcon": "imagedrag.png",
"lbl3": "Balance",
"lblAccountName": "Salary Account",
"lblAccountNumber": "***00114",
"lblAmount": "2,7453 JOD"
}, {
"Symbol": "B",
"imgIcon": "imagedrag.png",
"lbl3": "Balance",
"lblAccountName": "Salary Account",
"lblAccountNumber": "***00114",
"lblAmount": "2,7453 JOD"
}],
"groupCells": false,
"height": "91%",
"id": "segFre",
"isVisible": true,
"right": "0%",
"needPageIndicator": true,
"onRowClick": AS_Segment_ef182e9bd69845269f08bc53f75c0a0b,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "seg0a4ef05dd3f1c4f",
"rowSkin": "seg0b1b665cbd23642",
"rowTemplate": flxAccountDetailsScreenTemplate,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader0d39ff84c48da4e",
"selectionBehavior": constants.SEGUI_SINGLE_SELECT_BEHAVIOR,
"separatorColor": "ffffff5a",
"separatorRequired": true,
"separatorThickness": 1,
"showScrollbars": false,
"top": "9%",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"Symbol": "Symbol",
"flxAccountDetailsScreenTemplate": "flxAccountDetailsScreenTemplate",
"flxContents": "flxContents",
"imgIcon": "imgIcon",
"lbl3": "lbl3",
"lblAccountName": "lblAccountName",
"lblAccountNumber": "lblAccountNumber",
"lblAmount": "lblAmount"
},
"width": "100%",
"zIndex": 10,
"enableReordering": false
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxMain.add(flx, segFre);
frmAccountDetailsScreen.add(flxMain);
};
function frmAccountDetailsScreenGlobalsAr() {
frmAccountDetailsScreenAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmAccountDetailsScreenAr,
"enabledForIdleTimeout": true,
"id": "frmAccountDetailsScreen",
"layoutType": kony.flex.FREE_FORM,
"needAppMenu": false,
"skin": "slFormCommon"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FREE_FORM,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceMenu": AS_Form_a95e76c015d94d549ce9ccdb5503220e,
"retainScrollPosition": false,
"titleBar": true,
"titleBarSkin": "slTitleBar",
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
