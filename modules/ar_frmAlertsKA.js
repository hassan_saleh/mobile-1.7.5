//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmAlertsKAAr() {
frmAlertsKA.setDefaultUnit(kony.flex.DP);
var mainContent = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "100%",
"horizontalScrollIndicator": true,
"id": "mainContent",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "sknscrollBkg",
"top": "0dp",
"verticalScrollIndicator": false,
"width": "100%",
"zIndex": 1
}, {}, {});
mainContent.setDefaultUnit(kony.flex.DP);
var TextFieldFlex = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "TextFieldFlex",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "2dp",
"width": "100%",
"zIndex": 1
}, {}, {});
TextFieldFlex.setDefaultUnit(kony.flex.DP);
var RichText024982103203046 = new kony.ui.RichText({
"centerX": "50.00%",
"height": "50dp",
"id": "RichText024982103203046",
"isVisible": true,
"right": "0dp",
"left": 0,
"skin": "sknslRichTextInstructions",
"text": "These Alerts will help you to manage your accounts efficiently. More secured service is our intention.",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
TextFieldFlex.add(RichText024982103203046);
var switchFlexAndroid = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "switchFlexAndroid",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "52dp",
"width": "100%",
"zIndex": 1
}, {}, {});
switchFlexAndroid.setDefaultUnit(kony.flex.DP);
var AlertReq = new kony.ui.Label({
"centerY": "50%",
"id": "AlertReq",
"isVisible": true,
"right": "15dp",
"skin": "skn",
"text": kony.i18n.getLocalizedString("i18n.settings.alertsRequired"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "14dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxImageKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "30dp",
"id": "flxImageKA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_72ce46efe78749e09207a1944491db95,
"left": "20dp",
"skin": "slFbox",
"top": "5dp",
"width": "30dp",
"zIndex": 1
}, {}, {});
flxImageKA.setDefaultUnit(kony.flex.DP);
var infoImg = new kony.ui.Image2({
"centerY": "50.37%",
"height": "30dp",
"id": "infoImg",
"isVisible": true,
"right": 0,
"skin": "slImage",
"src": "checkbox_off.png",
"top": "0dp",
"width": "30dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxImageKA.add(infoImg);
switchFlexAndroid.add(AlertReq, flxImageKA);
var alertsTypes = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "alertsTypes",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox",
"top": "102dp",
"width": "100%",
"zIndex": 1
}, {}, {});
alertsTypes.setDefaultUnit(kony.flex.DP);
var AlertsData = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"data": [
[{
"lblHeaderKA": kony.i18n.getLocalizedString("i18n.settings.accountAlerts")
},
[{
"Hidden": "",
"imgProgressKey": "",
"lblSettingsNameKA": "",
"lblSettingsStatusKA": ""
}]
]
],
"groupCells": false,
"id": "AlertsData",
"isVisible": true,
"right": "0dp",
"needPageIndicator": true,
"onRowClick": AS_Segment_5041502381554331b1dc89212f1e3d1b,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowTemplate": Copycontainer09742046d0d2944,
"scrollingEvents": {},
"sectionHeaderTemplate": Copycontainer087f3a0994b334a,
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "64646400",
"separatorRequired": false,
"separatorThickness": 0,
"showScrollbars": false,
"top": "0dp",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"Copycontainer087f3a0994b334a": "Copycontainer087f3a0994b334a",
"Copycontainer09742046d0d2944": "Copycontainer09742046d0d2944",
"Hidden": "Hidden",
"contactListDivider": "contactListDivider",
"imgProgressKey": "imgProgressKey",
"lblHeaderKA": "lblHeaderKA",
"lblSettingsNameKA": "lblSettingsNameKA",
"lblSettingsStatusKA": "lblSettingsStatusKA"
},
"width": "100%"
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var generalAlerts = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"data": [
[{
"lblHeaderKA": kony.i18n.getLocalizedString("i18n.settings.generalAlerts")
},
[{
"HiddenLbl": "",
"imgicontick": "right_chevron_icon.png",
"lblPageNameKA": kony.i18n.getLocalizedString("i18n.settings.SecurityAlerts")
}, {
"HiddenLbl": "",
"imgicontick": "right_chevron_icon.png",
"lblPageNameKA": kony.i18n.getLocalizedString("i18n.settings.dealsAlerts")
}]
]
],
"groupCells": false,
"id": "generalAlerts",
"isVisible": true,
"right": "0dp",
"needPageIndicator": true,
"onRowClick": AS_Segment_35614b8c54b144e9bb127a26c7b1bcec,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "seg2Focus",
"rowSkin": "seg2Normal",
"rowTemplate": container,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"sectionHeaderTemplate": Copycontainer087f3a0994b334a,
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "64646400",
"separatorRequired": false,
"separatorThickness": 0,
"showScrollbars": false,
"top": "0dp",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"Copycontainer087f3a0994b334a": "Copycontainer087f3a0994b334a",
"HiddenLbl": "HiddenLbl",
"contactListDivider": "contactListDivider",
"container": "container",
"imgicontick": "imgicontick",
"lblHeaderKA": "lblHeaderKA",
"lblPageNameKA": "lblPageNameKA"
},
"width": "100%"
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var segmentBorderBottomAndroid = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "segmentBorderBottomAndroid",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"top": "-1dp",
"width": "100%",
"zIndex": 1
}, {}, {});
segmentBorderBottomAndroid.setDefaultUnit(kony.flex.DP);
segmentBorderBottomAndroid.add();
alertsTypes.add(AlertsData, generalAlerts, segmentBorderBottomAndroid);
mainContent.add(TextFieldFlex, switchFlexAndroid, alertsTypes);
var flxAlertHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxAlertHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "s",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAlertHeader.setDefaultUnit(kony.flex.DP);
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_icd29fef4bfd4583a062acacf9df0c14,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 2
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBack = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"centerY": "50%",
"id": "lblBack",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBack.add(lblBackIcon, lblBack);
var lblAlertHeader = new kony.ui.Label({
"centerX": "50%",
"height": "90%",
"id": "lblAlertHeader",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.settings.accountAlerts"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxAlertHeader.add(flxBack, lblAlertHeader);
var flxMain = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": true,
"clipBounds": true,
"enableScrolling": true,
"height": "91%",
"horizontalScrollIndicator": true,
"id": "flxMain",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "0dp",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
flxMain.setDefaultUnit(kony.flex.DP);
var flxAlertNotification = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "15%",
"id": "flxAlertNotification",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAlertNotification.setDefaultUnit(kony.flex.DP);
var flxAlertNotificationDisable = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxAlertNotificationDisable",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_fd38d20d45ae49daa04c720047cf5e98,
"left": "5%",
"skin": "sknflxWhiteOpacity60",
"top": "0dp",
"width": "44dp",
"zIndex": 9
}, {}, {});
flxAlertNotificationDisable.setDefaultUnit(kony.flex.DP);
var flxInnerDisabled = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxInnerDisabled",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknFlxDarkBlueRoundCornerGrey",
"top": "0dp",
"width": "25dp",
"zIndex": 1
}, {}, {});
flxInnerDisabled.setDefaultUnit(kony.flex.DP);
flxInnerDisabled.add();
var flxILineDisabled = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "14dp",
"id": "flxILineDisabled",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 10,
"skin": "sknLineDarkBlueOpacity60",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
flxILineDisabled.setDefaultUnit(kony.flex.DP);
flxILineDisabled.add();
flxAlertNotificationDisable.add(flxInnerDisabled, flxILineDisabled);
var flxAlertNotificationEnable = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxAlertNotificationEnable",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_e746481533d84725929b8854585d280d,
"left": "5%",
"skin": "sknflxyellow",
"top": "0dp",
"width": "44dp",
"zIndex": 10
}, {}, {});
flxAlertNotificationEnable.setDefaultUnit(kony.flex.DP);
var flxInnerEnabled = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxInnerEnabled",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "sknFlxDarkBlueRoundCorner",
"top": "0dp",
"width": "25dp",
"zIndex": 1
}, {}, {});
flxInnerEnabled.setDefaultUnit(kony.flex.DP);
flxInnerEnabled.add();
var flxLineEnabled = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "14dp",
"id": "flxLineEnabled",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10dp",
"skin": "sknLineDarkBlue",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
flxLineEnabled.setDefaultUnit(kony.flex.DP);
flxLineEnabled.add();
flxAlertNotificationEnable.add(flxInnerEnabled, flxLineEnabled);
var flxUnderLineAlert = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2%",
"id": "flxUnderLineAlert",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "flxUnderLine",
"top": "98%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxUnderLineAlert.setDefaultUnit(kony.flex.DP);
flxUnderLineAlert.add();
var lblMaskedAccountNumber = new kony.ui.Label({
"id": "lblMaskedAccountNumber",
"isVisible": true,
"right": "5%",
"maxNumberOfLines": 1,
"skin": "sknLblBack",
"text": "***077",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "45%",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblAccountType = new kony.ui.Label({
"id": "lblAccountType",
"isVisible": true,
"right": "5%",
"maxNumberOfLines": 1,
"skin": "lblAccountType",
"text": "Savings Account",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "23%",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxAlertNotification.add(flxAlertNotificationDisable, flxAlertNotificationEnable, flxUnderLineAlert, lblMaskedAccountNumber, lblAccountType);
var flxAlertWithdrawal = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "15%",
"id": "flxAlertWithdrawal",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAlertWithdrawal.setDefaultUnit(kony.flex.DP);
var flxAlertWithdrawalDisable = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxAlertWithdrawalDisable",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_d7094e909b5246e799d259270321ccf4,
"left": "5%",
"skin": "sknflxWhiteOpacity60",
"top": "0dp",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxAlertWithdrawalDisable.setDefaultUnit(kony.flex.DP);
var CopyflxInnerDisabled0hd9e9f13e3234c = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "CopyflxInnerDisabled0hd9e9f13e3234c",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknFlxDarkBlueRoundCornerGrey",
"top": "0dp",
"width": "25dp",
"zIndex": 1
}, {}, {});
CopyflxInnerDisabled0hd9e9f13e3234c.setDefaultUnit(kony.flex.DP);
CopyflxInnerDisabled0hd9e9f13e3234c.add();
var CopyflxILineDisabled0h47becdf512747 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "14dp",
"id": "CopyflxILineDisabled0h47becdf512747",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 10,
"skin": "sknLineDarkBlueOpacity60",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
CopyflxILineDisabled0h47becdf512747.setDefaultUnit(kony.flex.DP);
CopyflxILineDisabled0h47becdf512747.add();
flxAlertWithdrawalDisable.add(CopyflxInnerDisabled0hd9e9f13e3234c, CopyflxILineDisabled0h47becdf512747);
var flxAlertWithdrawalEnable = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxAlertWithdrawalEnable",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_f0147413333f47ea8594157eff29c0fe,
"left": "5%",
"skin": "sknflxyellow",
"top": "0dp",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxAlertWithdrawalEnable.setDefaultUnit(kony.flex.DP);
var CopyflxInnerEnabled0c1a477c63a8c41 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "CopyflxInnerEnabled0c1a477c63a8c41",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "sknFlxDarkBlueRoundCorner",
"top": "0dp",
"width": "25dp",
"zIndex": 1
}, {}, {});
CopyflxInnerEnabled0c1a477c63a8c41.setDefaultUnit(kony.flex.DP);
CopyflxInnerEnabled0c1a477c63a8c41.add();
var CopyflxLineEnabled0ja3b408f5d0541 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "14dp",
"id": "CopyflxLineEnabled0ja3b408f5d0541",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10dp",
"skin": "sknLineDarkBlue",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
CopyflxLineEnabled0ja3b408f5d0541.setDefaultUnit(kony.flex.DP);
CopyflxLineEnabled0ja3b408f5d0541.add();
flxAlertWithdrawalEnable.add(CopyflxInnerEnabled0c1a477c63a8c41, CopyflxLineEnabled0ja3b408f5d0541);
var lblAlertWithdrawal = new kony.ui.Label({
"centerY": "50%",
"id": "lblAlertWithdrawal",
"isVisible": true,
"right": "5%",
"maxNumberOfLines": 1,
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.search.Withdrawal"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "73%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyflxUnderLineAlert0f38788ae57a349 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2%",
"id": "CopyflxUnderLineAlert0f38788ae57a349",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "flxUnderLine",
"top": "98%",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxUnderLineAlert0f38788ae57a349.setDefaultUnit(kony.flex.DP);
CopyflxUnderLineAlert0f38788ae57a349.add();
flxAlertWithdrawal.add(flxAlertWithdrawalDisable, flxAlertWithdrawalEnable, lblAlertWithdrawal, CopyflxUnderLineAlert0f38788ae57a349);
var flxAlertDeposit = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "15%",
"id": "flxAlertDeposit",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAlertDeposit.setDefaultUnit(kony.flex.DP);
var flxAlertDepositDisable = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxAlertDepositDisable",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_b9010476a8704088bdce7977814cc4d7,
"left": "5%",
"skin": "sknflxWhiteOpacity60",
"top": "0dp",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxAlertDepositDisable.setDefaultUnit(kony.flex.DP);
var CopyflxInnerDisabled0d3bf8f725ed846 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "CopyflxInnerDisabled0d3bf8f725ed846",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknFlxDarkBlueRoundCornerGrey",
"top": "0dp",
"width": "25dp",
"zIndex": 1
}, {}, {});
CopyflxInnerDisabled0d3bf8f725ed846.setDefaultUnit(kony.flex.DP);
CopyflxInnerDisabled0d3bf8f725ed846.add();
var CopyflxILineDisabled0af787070fe7742 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "14dp",
"id": "CopyflxILineDisabled0af787070fe7742",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 10,
"skin": "sknLineDarkBlueOpacity60",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
CopyflxILineDisabled0af787070fe7742.setDefaultUnit(kony.flex.DP);
CopyflxILineDisabled0af787070fe7742.add();
flxAlertDepositDisable.add(CopyflxInnerDisabled0d3bf8f725ed846, CopyflxILineDisabled0af787070fe7742);
var flxAlertDepositEnable = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxAlertDepositEnable",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_cb990460e21d46a9a56b74cb311a897a,
"left": "5%",
"skin": "sknflxyellow",
"top": "0dp",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxAlertDepositEnable.setDefaultUnit(kony.flex.DP);
var CopyflxInnerEnabled0f51e641c7c174b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "CopyflxInnerEnabled0f51e641c7c174b",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "sknFlxDarkBlueRoundCorner",
"top": "0dp",
"width": "25dp",
"zIndex": 1
}, {}, {});
CopyflxInnerEnabled0f51e641c7c174b.setDefaultUnit(kony.flex.DP);
CopyflxInnerEnabled0f51e641c7c174b.add();
var CopyflxLineEnabled0fab2971411c349 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "14dp",
"id": "CopyflxLineEnabled0fab2971411c349",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10dp",
"skin": "sknLineDarkBlue",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
CopyflxLineEnabled0fab2971411c349.setDefaultUnit(kony.flex.DP);
CopyflxLineEnabled0fab2971411c349.add();
flxAlertDepositEnable.add(CopyflxInnerEnabled0f51e641c7c174b, CopyflxLineEnabled0fab2971411c349);
var lblAlertDeposit = new kony.ui.Label({
"centerY": "50%",
"id": "lblAlertDeposit",
"isVisible": true,
"right": "5%",
"maxNumberOfLines": 1,
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.alert.termdeposit"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "73%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyflxUnderLineAlert0adbd98dbd7ca4c = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2%",
"id": "CopyflxUnderLineAlert0adbd98dbd7ca4c",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "flxUnderLine",
"top": "98%",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxUnderLineAlert0adbd98dbd7ca4c.setDefaultUnit(kony.flex.DP);
CopyflxUnderLineAlert0adbd98dbd7ca4c.add();
flxAlertDeposit.add(flxAlertDepositDisable, flxAlertDepositEnable, lblAlertDeposit, CopyflxUnderLineAlert0adbd98dbd7ca4c);
var flxAlertDailyBalance = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "15%",
"id": "flxAlertDailyBalance",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAlertDailyBalance.setDefaultUnit(kony.flex.DP);
var flxAlertDailyBalanceDisable = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxAlertDailyBalanceDisable",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_eddecb81c3d84122b1ea0c0a4187da39,
"left": "5%",
"skin": "sknflxWhiteOpacity60",
"top": "0dp",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxAlertDailyBalanceDisable.setDefaultUnit(kony.flex.DP);
var CopyflxInnerDisabled0j926012d5cb549 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "CopyflxInnerDisabled0j926012d5cb549",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknFlxDarkBlueRoundCornerGrey",
"top": "0dp",
"width": "25dp",
"zIndex": 1
}, {}, {});
CopyflxInnerDisabled0j926012d5cb549.setDefaultUnit(kony.flex.DP);
CopyflxInnerDisabled0j926012d5cb549.add();
var CopyflxILineDisabled0c88dadbd834e4d = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "14dp",
"id": "CopyflxILineDisabled0c88dadbd834e4d",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 10,
"skin": "sknLineDarkBlueOpacity60",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
CopyflxILineDisabled0c88dadbd834e4d.setDefaultUnit(kony.flex.DP);
CopyflxILineDisabled0c88dadbd834e4d.add();
flxAlertDailyBalanceDisable.add(CopyflxInnerDisabled0j926012d5cb549, CopyflxILineDisabled0c88dadbd834e4d);
var flxAlertDailyBalanceEnable = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxAlertDailyBalanceEnable",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_c1e2519b1a634819b42ceb4b727c0215,
"left": "5%",
"skin": "sknflxyellow",
"top": "0dp",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxAlertDailyBalanceEnable.setDefaultUnit(kony.flex.DP);
var CopyflxInnerEnabled0a7ee8878c7ae4a = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "CopyflxInnerEnabled0a7ee8878c7ae4a",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "sknFlxDarkBlueRoundCorner",
"top": "0dp",
"width": "25dp",
"zIndex": 1
}, {}, {});
CopyflxInnerEnabled0a7ee8878c7ae4a.setDefaultUnit(kony.flex.DP);
CopyflxInnerEnabled0a7ee8878c7ae4a.add();
var CopyflxLineEnabled0fb1eeeb657c844 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "14dp",
"id": "CopyflxLineEnabled0fb1eeeb657c844",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10dp",
"skin": "sknLineDarkBlue",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
CopyflxLineEnabled0fb1eeeb657c844.setDefaultUnit(kony.flex.DP);
CopyflxLineEnabled0fb1eeeb657c844.add();
flxAlertDailyBalanceEnable.add(CopyflxInnerEnabled0a7ee8878c7ae4a, CopyflxLineEnabled0fb1eeeb657c844);
var lblAlertDailyBalance = new kony.ui.Label({
"centerY": "50%",
"id": "lblAlertDailyBalance",
"isVisible": true,
"right": "5%",
"maxNumberOfLines": 1,
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.alert.dailybalance"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "73%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyflxUnderLineAlert0d4e78d84c8a245 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2%",
"id": "CopyflxUnderLineAlert0d4e78d84c8a245",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "flxUnderLine",
"top": "98%",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxUnderLineAlert0d4e78d84c8a245.setDefaultUnit(kony.flex.DP);
CopyflxUnderLineAlert0d4e78d84c8a245.add();
flxAlertDailyBalance.add(flxAlertDailyBalanceDisable, flxAlertDailyBalanceEnable, lblAlertDailyBalance, CopyflxUnderLineAlert0d4e78d84c8a245);
var flxAlertMorningBalance = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "15%",
"id": "flxAlertMorningBalance",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAlertMorningBalance.setDefaultUnit(kony.flex.DP);
var flxAlertMorningBalanceDisable = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxAlertMorningBalanceDisable",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_f50aa84f81fe426ea8c83971c3cf1152,
"left": "5%",
"skin": "sknflxWhiteOpacity60",
"top": "0dp",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxAlertMorningBalanceDisable.setDefaultUnit(kony.flex.DP);
var CopyflxInnerDisabled0i6415505d76141 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "CopyflxInnerDisabled0i6415505d76141",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknFlxDarkBlueRoundCornerGrey",
"top": "0dp",
"width": "25dp",
"zIndex": 1
}, {}, {});
CopyflxInnerDisabled0i6415505d76141.setDefaultUnit(kony.flex.DP);
CopyflxInnerDisabled0i6415505d76141.add();
var CopyflxILineDisabled0f4f4fb9013fc42 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "14dp",
"id": "CopyflxILineDisabled0f4f4fb9013fc42",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 10,
"skin": "sknLineDarkBlueOpacity60",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
CopyflxILineDisabled0f4f4fb9013fc42.setDefaultUnit(kony.flex.DP);
CopyflxILineDisabled0f4f4fb9013fc42.add();
flxAlertMorningBalanceDisable.add(CopyflxInnerDisabled0i6415505d76141, CopyflxILineDisabled0f4f4fb9013fc42);
var flxAlertMorningBalanceEnable = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxAlertMorningBalanceEnable",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_ac67fe22d13a4a80b7c3143e1951ac41,
"left": "5%",
"skin": "sknflxyellow",
"top": "0dp",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxAlertMorningBalanceEnable.setDefaultUnit(kony.flex.DP);
var CopyflxInnerEnabled0b8f37886274244 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "CopyflxInnerEnabled0b8f37886274244",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "sknFlxDarkBlueRoundCorner",
"top": "0dp",
"width": "25dp",
"zIndex": 1
}, {}, {});
CopyflxInnerEnabled0b8f37886274244.setDefaultUnit(kony.flex.DP);
CopyflxInnerEnabled0b8f37886274244.add();
var CopyflxLineEnabled0df75cffbf74b49 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "14dp",
"id": "CopyflxLineEnabled0df75cffbf74b49",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10dp",
"skin": "sknLineDarkBlue",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
CopyflxLineEnabled0df75cffbf74b49.setDefaultUnit(kony.flex.DP);
CopyflxLineEnabled0df75cffbf74b49.add();
flxAlertMorningBalanceEnable.add(CopyflxInnerEnabled0b8f37886274244, CopyflxLineEnabled0df75cffbf74b49);
var lblAlertMorningBalance = new kony.ui.Label({
"centerY": "50%",
"id": "lblAlertMorningBalance",
"isVisible": true,
"right": "5%",
"maxNumberOfLines": 1,
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.alert.transfers"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "73%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyflxUnderLineAlert0gc5b2de6837348 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2%",
"id": "CopyflxUnderLineAlert0gc5b2de6837348",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "flxUnderLine",
"top": "98%",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxUnderLineAlert0gc5b2de6837348.setDefaultUnit(kony.flex.DP);
CopyflxUnderLineAlert0gc5b2de6837348.add();
flxAlertMorningBalance.add(flxAlertMorningBalanceDisable, flxAlertMorningBalanceEnable, lblAlertMorningBalance, CopyflxUnderLineAlert0gc5b2de6837348);
var flxAlertCheckCleared = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "15%",
"id": "flxAlertCheckCleared",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAlertCheckCleared.setDefaultUnit(kony.flex.DP);
var flxAlertCheckClearedDisable = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxAlertCheckClearedDisable",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_d8060831cca84749893a5d07dfafc280,
"left": "5%",
"skin": "sknflxWhiteOpacity60",
"top": "0dp",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxAlertCheckClearedDisable.setDefaultUnit(kony.flex.DP);
var CopyflxInnerDisabled0c5c347424ad345 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "CopyflxInnerDisabled0c5c347424ad345",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknFlxDarkBlueRoundCornerGrey",
"top": "0dp",
"width": "25dp",
"zIndex": 1
}, {}, {});
CopyflxInnerDisabled0c5c347424ad345.setDefaultUnit(kony.flex.DP);
CopyflxInnerDisabled0c5c347424ad345.add();
var CopyflxILineDisabled0fee99492e04442 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "14dp",
"id": "CopyflxILineDisabled0fee99492e04442",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 10,
"skin": "sknLineDarkBlueOpacity60",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
CopyflxILineDisabled0fee99492e04442.setDefaultUnit(kony.flex.DP);
CopyflxILineDisabled0fee99492e04442.add();
flxAlertCheckClearedDisable.add(CopyflxInnerDisabled0c5c347424ad345, CopyflxILineDisabled0fee99492e04442);
var flxAlertCheckClearedEnable = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxAlertCheckClearedEnable",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_e9536625a0424c6db54e1618cbfdab1b,
"left": "5%",
"skin": "sknflxyellow",
"top": "0dp",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxAlertCheckClearedEnable.setDefaultUnit(kony.flex.DP);
var CopyflxInnerEnabled0cd65ee8876764b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "CopyflxInnerEnabled0cd65ee8876764b",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "sknFlxDarkBlueRoundCorner",
"top": "0dp",
"width": "25dp",
"zIndex": 1
}, {}, {});
CopyflxInnerEnabled0cd65ee8876764b.setDefaultUnit(kony.flex.DP);
CopyflxInnerEnabled0cd65ee8876764b.add();
var CopyflxLineEnabled0b15cc539fcb942 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "14dp",
"id": "CopyflxLineEnabled0b15cc539fcb942",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10dp",
"skin": "sknLineDarkBlue",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
CopyflxLineEnabled0b15cc539fcb942.setDefaultUnit(kony.flex.DP);
CopyflxLineEnabled0b15cc539fcb942.add();
flxAlertCheckClearedEnable.add(CopyflxInnerEnabled0cd65ee8876764b, CopyflxLineEnabled0b15cc539fcb942);
var lblAlertCheckCleared = new kony.ui.Label({
"centerY": "50%",
"id": "lblAlertCheckCleared",
"isVisible": true,
"right": "5%",
"maxNumberOfLines": 1,
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.alert.cheques"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "73%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyflxUnderLineAlert0b616a9f8befe4d = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2%",
"id": "CopyflxUnderLineAlert0b616a9f8befe4d",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "flxUnderLine",
"top": "98%",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxUnderLineAlert0b616a9f8befe4d.setDefaultUnit(kony.flex.DP);
CopyflxUnderLineAlert0b616a9f8befe4d.add();
flxAlertCheckCleared.add(flxAlertCheckClearedDisable, flxAlertCheckClearedEnable, lblAlertCheckCleared, CopyflxUnderLineAlert0b616a9f8befe4d);
var lblAccountNumber = new kony.ui.Label({
"centerY": "50%",
"id": "lblAccountNumber",
"isVisible": false,
"right": "5%",
"maxNumberOfLines": 1,
"skin": "sknLblBack",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "73%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblAlertDisable = new kony.ui.Label({
"centerY": "50%",
"id": "lblAlertDisable",
"isVisible": false,
"right": "5%",
"maxNumberOfLines": 1,
"skin": "sknLblBack",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "73%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCustID = new kony.ui.Label({
"centerY": "50%",
"id": "lblCustID",
"isVisible": false,
"right": "5%",
"maxNumberOfLines": 1,
"skin": "sknLblBack",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "73%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblLanguage = new kony.ui.Label({
"centerY": "50%",
"id": "lblLanguage",
"isVisible": false,
"right": "5%",
"maxNumberOfLines": 1,
"skin": "sknLblBack",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "73%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblWithdrawalFlag = new kony.ui.Label({
"centerY": "50%",
"id": "lblWithdrawalFlag",
"isVisible": false,
"right": "5%",
"maxNumberOfLines": 1,
"skin": "sknLblBack",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "73%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblDepositFlag = new kony.ui.Label({
"centerY": "50%",
"id": "lblDepositFlag",
"isVisible": false,
"right": "5%",
"maxNumberOfLines": 1,
"skin": "sknLblBack",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "73%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblDailyBalanceFlag = new kony.ui.Label({
"centerY": "50%",
"id": "lblDailyBalanceFlag",
"isVisible": false,
"right": "5%",
"maxNumberOfLines": 1,
"skin": "sknLblBack",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "73%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblMorningBalanceFlag = new kony.ui.Label({
"centerY": "50%",
"id": "lblMorningBalanceFlag",
"isVisible": false,
"right": "5%",
"maxNumberOfLines": 1,
"skin": "sknLblBack",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "73%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblChequeClearedFlag = new kony.ui.Label({
"centerY": "50%",
"id": "lblChequeClearedFlag",
"isVisible": false,
"right": "5%",
"maxNumberOfLines": 1,
"skin": "sknLblBack",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "73%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxMain.add(flxAlertNotification, flxAlertWithdrawal, flxAlertDeposit, flxAlertDailyBalance, flxAlertMorningBalance, flxAlertCheckCleared, lblAccountNumber, lblAlertDisable, lblCustID, lblLanguage, lblWithdrawalFlag, lblDepositFlag, lblDailyBalanceFlag, lblMorningBalanceFlag, lblChequeClearedFlag);
frmAlertsKA.add(mainContent, flxAlertHeader, flxMain);
};
function frmAlertsKAGlobalsAr() {
frmAlertsKAAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmAlertsKAAr,
"bounces": false,
"enabledForIdleTimeout": true,
"id": "frmAlertsKA",
"layoutType": kony.flex.FLOW_VERTICAL,
"needAppMenu": false,
"skin": "sknmainGradient"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FLOW_VERTICAL,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": true,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_e430aa6088454dbdb5e0ca8dd63e660c,
"retainScrollPosition": false,
"titleBar": true,
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
