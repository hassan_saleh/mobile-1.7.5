//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function initializetmpSearchResultsKAAr() {
    CopyFlexContainer0125626bedd894dAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "80dp",
        "id": "CopyFlexContainer0125626bedd894d",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknCopyslFbox07d05709853a74d"
    }, {}, {});
    CopyFlexContainer0125626bedd894d.setDefaultUnit(kony.flex.DP);
    var lblTransactionDateKA = new kony.ui.Label({
        "height": "20dp",
        "id": "lblTransactionDateKA",
        "isVisible": true,
        "right": "7%",
        "skin": "sknRegisterMobileBank",
        "top": "20dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblTransactionNameKA = new kony.ui.Label({
        "centerY": 50,
        "height": "20dp",
        "id": "lblTransactionNameKA",
        "isVisible": true,
        "right": "7.03%",
        "skin": "skn383838LatoRegular107KA",
        "top": "50dp",
        "width": "65%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblTransactionAmountKA = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblTransactionAmountKA",
        "isVisible": true,
        "left": "26dp",
        "skin": "skn383838LatoRegular107KA",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblSepKA = new kony.ui.Label({
        "bottom": "0dp",
        "height": "1dp",
        "id": "lblSepKA",
        "isVisible": true,
        "right": "0dp",
        "left": "0dp",
        "skin": "sknLineEDEDEDKA",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "100%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var ImgWithDraw = new kony.ui.Image2({
        "centerY": "37%",
        "height": "30%",
        "id": "ImgWithDraw",
        "isVisible": false,
        "right": "2.50%",
        "skin": "slImage",
        "src": "pending.png",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    var lblExpiryTime = new kony.ui.Label({
        "id": "lblExpiryTime",
        "isVisible": false,
        "left": "95dp",
        "skin": "sknErrorMessageEC223BKA",
        "text": "21h:22m",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "21dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    CopyFlexContainer0125626bedd894dAr.add(lblTransactionDateKA, lblTransactionNameKA, lblTransactionAmountKA, lblSepKA, ImgWithDraw, lblExpiryTime);
}
