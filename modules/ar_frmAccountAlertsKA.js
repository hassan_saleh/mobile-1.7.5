//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:46 EEST 2020
function addWidgetsfrmAccountAlertsKAAr() {
frmAccountAlertsKA.setDefaultUnit(kony.flex.DP);
var mainContent = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "100%",
"horizontalScrollIndicator": true,
"id": "mainContent",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "sknscrollBkg",
"top": "70dp",
"verticalScrollIndicator": false,
"width": "100%",
"zIndex": 1
}, {}, {});
mainContent.setDefaultUnit(kony.flex.DP);
var AccAlertsSwitchFlexKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "AccAlertsSwitchFlexKA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
AccAlertsSwitchFlexKA.setDefaultUnit(kony.flex.DP);
var AlertReq = new kony.ui.Label({
"centerY": "50%",
"id": "AlertReq",
"isVisible": true,
"right": "15dp",
"skin": "skn",
"text": kony.i18n.getLocalizedString("i18n.settings.alertsRequired"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "14dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxImageKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "30dp",
"id": "flxImageKA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_ec9ee3e54948414387f8ef76faf11038,
"left": "20dp",
"skin": "slFbox",
"top": "5dp",
"width": "30dp",
"zIndex": 1
}, {}, {});
flxImageKA.setDefaultUnit(kony.flex.DP);
var infoImg = new kony.ui.Image2({
"centerY": "50.37%",
"height": "30dp",
"id": "infoImg",
"isVisible": true,
"right": 0,
"skin": "slImage",
"src": "checkbox_off.png",
"top": "0dp",
"width": "30dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxImageKA.add(infoImg);
AccAlertsSwitchFlexKA.add(AlertReq, flxImageKA);
var TextFieldFlex = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_HEIGHT,
"clipBounds": true,
"id": "TextFieldFlex",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "50dp",
"width": "100%",
"zIndex": 1
}, {}, {});
TextFieldFlex.setDefaultUnit(kony.flex.DP);
var RichText024982103203046 = new kony.ui.RichText({
"centerX": "50.00%",
"height": "50dp",
"id": "RichText024982103203046",
"isVisible": true,
"right": "0dp",
"left": 0,
"skin": "sknslRichTextInstructions",
"text": " These alerts are specific to this account only. Need to set the similar once for other accounts.",
"top": "0dp",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
TextFieldFlex.add(RichText024982103203046);
var androidAlerts = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "androidAlerts",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "sknslFbox",
"top": "110dp",
"width": "100%",
"zIndex": 1
}, {}, {});
androidAlerts.setDefaultUnit(kony.flex.DP);
var FlxMinBal = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "FlxMinBal",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0",
"onClick": AS_FlexContainer_8ab6b60bc9e64523a8fb7c7321d9e8f6,
"skin": "sknCopyslFbox07d05709853a74d",
"top": "0",
"width": "100%"
}, {}, {});
FlxMinBal.setDefaultUnit(kony.flex.DP);
var lblSettingsNameKA = new kony.ui.Label({
"centerY": "50%",
"id": "lblSettingsNameKA",
"isVisible": true,
"right": "5%",
"skin": "skn",
"text": kony.i18n.getLocalizedString("i18n.settings.minimumBalance"),
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var minBalance = new kony.ui.Label({
"centerY": "48.72%",
"id": "minBalance",
"isVisible": true,
"left": "35dp",
"skin": "sknSegSecondLabel",
"text": "Status",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var imgProgressKey = new kony.ui.Image2({
"centerY": "50%",
"height": "20dp",
"id": "imgProgressKey",
"isVisible": true,
"left": "4%",
"skin": "sknslImage",
"src": "right_chevron_icon.png",
"width": "20dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
FlxMinBal.add(lblSettingsNameKA, minBalance, imgProgressKey);
var Border1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "1dp",
"id": "Border1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
Border1.setDefaultUnit(kony.flex.DP);
Border1.add();
var FlxBalanceUpdate = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "FlxBalanceUpdate",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0",
"onClick": AS_FlexContainer_54bf8e5bab7142bebb47fb905b2b96fc,
"skin": "sknCopyslFbox07d05709853a74d",
"top": "0",
"width": "100%"
}, {}, {});
FlxBalanceUpdate.setDefaultUnit(kony.flex.DP);
var CopylblSettingsNameKA09a00c7f47f8d4a = new kony.ui.Label({
"centerY": "50.00%",
"id": "CopylblSettingsNameKA09a00c7f47f8d4a",
"isVisible": true,
"right": "4.97%",
"skin": "skn",
"text": kony.i18n.getLocalizedString("i18n.settings.balanceUpdate"),
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var balance = new kony.ui.Label({
"centerY": "48.72%",
"id": "balance",
"isVisible": true,
"left": "35dp",
"skin": "sknSegSecondLabel",
"text": "Status",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyimgProgressKey0ca8daa92e73c4f = new kony.ui.Image2({
"centerY": "50%",
"height": "20dp",
"id": "CopyimgProgressKey0ca8daa92e73c4f",
"isVisible": true,
"left": "4%",
"skin": "sknslImage",
"src": "right_chevron_icon.png",
"width": "20dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
FlxBalanceUpdate.add(CopylblSettingsNameKA09a00c7f47f8d4a, balance, CopyimgProgressKey0ca8daa92e73c4f);
var Border2 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "1dp",
"id": "Border2",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
Border2.setDefaultUnit(kony.flex.DP);
Border2.add();
var FlxDebitLimit = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "FlxDebitLimit",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0",
"onClick": AS_FlexContainer_5cfb1b3a84df49e398fcbf0a45fc0706,
"skin": "sknCopyslFbox07d05709853a74d",
"top": "0",
"width": "100%"
}, {}, {});
FlxDebitLimit.setDefaultUnit(kony.flex.DP);
var CopylblSettingsNameKA099835b41cdf347 = new kony.ui.Label({
"centerY": "50.00%",
"id": "CopylblSettingsNameKA099835b41cdf347",
"isVisible": true,
"right": "4.97%",
"skin": "skn",
"text": kony.i18n.getLocalizedString("i18n.settings.debitLimit"),
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var debit = new kony.ui.Label({
"centerY": "47.83%",
"id": "debit",
"isVisible": true,
"left": "35dp",
"skin": "sknSegSecondLabel",
"text": "Status",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyimgProgressKey0705a83a5a3d643 = new kony.ui.Image2({
"centerY": "47.78%",
"height": "20dp",
"id": "CopyimgProgressKey0705a83a5a3d643",
"isVisible": true,
"left": "4.00%",
"skin": "sknslImage",
"src": "right_chevron_icon.png",
"width": "20dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
FlxDebitLimit.add(CopylblSettingsNameKA099835b41cdf347, debit, CopyimgProgressKey0705a83a5a3d643);
var Border3 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"clipBounds": true,
"height": "1dp",
"id": "Border3",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
Border3.setDefaultUnit(kony.flex.DP);
Border3.add();
var FlxCreditLimit = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "FlxCreditLimit",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0",
"onClick": AS_FlexContainer_f387ea2ff59a48e396a0c902ae3e9edb,
"skin": "sknCopyslFbox07d05709853a74d",
"top": "0",
"width": "100%"
}, {}, {});
FlxCreditLimit.setDefaultUnit(kony.flex.DP);
var CopylblSettingsNameKA039b3a01bca3d4f = new kony.ui.Label({
"centerY": "50.22%",
"id": "CopylblSettingsNameKA039b3a01bca3d4f",
"isVisible": true,
"right": "4.97%",
"skin": "skn",
"text": kony.i18n.getLocalizedString("i18n.settings.creditLimit"),
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var credit = new kony.ui.Label({
"centerY": "48.72%",
"id": "credit",
"isVisible": true,
"left": "35dp",
"skin": "sknSegSecondLabel",
"text": "Status",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyimgProgressKey07facd4ffbbd04d = new kony.ui.Image2({
"centerY": "50%",
"height": "20dp",
"id": "CopyimgProgressKey07facd4ffbbd04d",
"isVisible": true,
"left": "4%",
"skin": "sknslImage",
"src": "right_chevron_icon.png",
"width": "20dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
FlxCreditLimit.add(CopylblSettingsNameKA039b3a01bca3d4f, credit, CopyimgProgressKey07facd4ffbbd04d);
var Border4 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "Border4",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
Border4.setDefaultUnit(kony.flex.DP);
Border4.add();
var FlxPaymentDue = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "FlxPaymentDue",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_d0acb051ddef4b1fbeecf23ce4d42b13,
"skin": "sknCopyslFbox07d05709853a74d",
"top": "0dp",
"width": "100%"
}, {}, {});
FlxPaymentDue.setDefaultUnit(kony.flex.DP);
var CopylblSettingsNameKA0c1f0212a30cd44 = new kony.ui.Label({
"centerY": "50.00%",
"id": "CopylblSettingsNameKA0c1f0212a30cd44",
"isVisible": true,
"right": "4.97%",
"skin": "skn",
"text": kony.i18n.getLocalizedString("i18n.settings.PaymentDue"),
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var payment = new kony.ui.Label({
"centerY": "48.72%",
"id": "payment",
"isVisible": true,
"left": "35dp",
"skin": "sknSegSecondLabel",
"text": "Status",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyimgProgressKey0e05de2940dd043 = new kony.ui.Image2({
"centerY": "50%",
"height": "20dp",
"id": "CopyimgProgressKey0e05de2940dd043",
"isVisible": true,
"left": "4%",
"skin": "sknslImage",
"src": "right_chevron_icon.png",
"width": "20dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
FlxPaymentDue.add(CopylblSettingsNameKA0c1f0212a30cd44, payment, CopyimgProgressKey0e05de2940dd043);
var Border5 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "Border5",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
Border5.setDefaultUnit(kony.flex.DP);
Border5.add();
var FlxDepositRemainder = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "FlxDepositRemainder",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0",
"onClick": AS_FlexContainer_0d9caab252fb4d8fa412739968c1fa93,
"skin": "sknCopyslFbox07d05709853a74d",
"top": "0",
"width": "100%"
}, {}, {});
FlxDepositRemainder.setDefaultUnit(kony.flex.DP);
var CopylblSettingsNameKA0c53aab4265674d = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblSettingsNameKA0c53aab4265674d",
"isVisible": true,
"right": "5%",
"skin": "skn",
"text": kony.i18n.getLocalizedString("i18n.settings.depositMaturityRemainder"),
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var deposit = new kony.ui.Label({
"centerY": "53.33%",
"id": "deposit",
"isVisible": true,
"left": "33dp",
"skin": "sknSegSecondLabel",
"text": "Status",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyimgProgressKey075279a02187542 = new kony.ui.Image2({
"centerY": "50%",
"height": "20dp",
"id": "CopyimgProgressKey075279a02187542",
"isVisible": true,
"left": "4%",
"skin": "sknslImage",
"src": "right_chevron_icon.png",
"width": "20dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
FlxDepositRemainder.add(CopylblSettingsNameKA0c53aab4265674d, deposit, CopyimgProgressKey075279a02187542);
var flxIosTransferAlertsSwitchKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "flxIosTransferAlertsSwitchKA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "2dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxIosTransferAlertsSwitchKA.setDefaultUnit(kony.flex.DP);
var lblIosTransferSuccessKA = new kony.ui.Label({
"centerY": "50%",
"id": "lblIosTransferSuccessKA",
"isVisible": true,
"right": "5%",
"skin": "skn",
"text": kony.i18n.getLocalizedString("i18n.transfer.SuccessfulTransfer"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxTransferSuccessImageKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "30dp",
"id": "flxTransferSuccessImageKA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_873741d30deb40cd9bc229a9a459edfa,
"left": "20dp",
"skin": "slFbox",
"top": "5dp",
"width": "30dp",
"zIndex": 1
}, {}, {});
flxTransferSuccessImageKA.setDefaultUnit(kony.flex.DP);
var successTransferInfoImgKA = new kony.ui.Image2({
"centerY": "50.00%",
"height": "30dp",
"id": "successTransferInfoImgKA",
"isVisible": true,
"right": 3,
"skin": "slImage",
"src": "checkbox_off.png",
"top": "0dp",
"width": "30dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxTransferSuccessImageKA.add(successTransferInfoImgKA);
flxIosTransferAlertsSwitchKA.add(lblIosTransferSuccessKA, flxTransferSuccessImageKA);
var flxIosCheckAlertsSwitchKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "flxIosCheckAlertsSwitchKA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "2dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxIosCheckAlertsSwitchKA.setDefaultUnit(kony.flex.DP);
var lblIosCheckClearanceKA = new kony.ui.Label({
"centerY": "50%",
"id": "lblIosCheckClearanceKA",
"isVisible": true,
"right": "5%",
"skin": "skn",
"text": kony.i18n.getLocalizedString("i18n.settings.checkClearance"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxCheckClearanceImageKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "30dp",
"id": "flxCheckClearanceImageKA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_0412a0ee16dd472485e9d95b01d19133,
"left": "20dp",
"skin": "slFbox",
"top": "5dp",
"width": "30dp",
"zIndex": 1
}, {}, {});
flxCheckClearanceImageKA.setDefaultUnit(kony.flex.DP);
var successCheckClearanceInfoImgKA = new kony.ui.Image2({
"centerY": "50.37%",
"height": "30dp",
"id": "successCheckClearanceInfoImgKA",
"isVisible": true,
"right": 0,
"skin": "slImage",
"src": "checkbox_off.png",
"top": "0dp",
"width": "30dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxCheckClearanceImageKA.add(successCheckClearanceInfoImgKA);
flxIosCheckAlertsSwitchKA.add(lblIosCheckClearanceKA, flxCheckClearanceImageKA);
var HiddenAlertsReq = new kony.ui.Label({
"id": "HiddenAlertsReq",
"isVisible": false,
"right": "109dp",
"skin": "slLabel",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "39dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var HiddenSuccessTransfer = new kony.ui.Label({
"id": "HiddenSuccessTransfer",
"isVisible": false,
"right": "109dp",
"skin": "slLabel",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "10dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var HiddenCheckClear = new kony.ui.Label({
"id": "HiddenCheckClear",
"isVisible": false,
"right": "110dp",
"skin": "slLabel",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "50dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var HiddenAlertId = new kony.ui.Label({
"id": "HiddenAlertId",
"isVisible": false,
"right": "109dp",
"skin": "slLabel",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "39dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var HiddenName = new kony.ui.Label({
"id": "HiddenName",
"isVisible": false,
"right": "109dp",
"skin": "slLabel",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "46dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var HiddenAccNum = new kony.ui.Label({
"id": "HiddenAccNum",
"isVisible": false,
"right": "148dp",
"skin": "slLabel",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "40dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
androidAlerts.add(FlxMinBal, Border1, FlxBalanceUpdate, Border2, FlxDebitLimit, Border3, FlxCreditLimit, Border4, FlxPaymentDue, Border5, FlxDepositRemainder, flxIosTransferAlertsSwitchKA, flxIosCheckAlertsSwitchKA, HiddenAlertsReq, HiddenSuccessTransfer, HiddenCheckClear, HiddenAlertId, HiddenName, HiddenAccNum);
mainContent.add(AccAlertsSwitchFlexKA, TextFieldFlex, androidAlerts);
var flxAccAlertHeader = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxAccAlertHeader",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "s",
"top": "0.00%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAccAlertHeader.setDefaultUnit(kony.flex.DP);
var flxback = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxback",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_e39b25b037dc42eda374ea8b255bf681,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 2
}, {}, {});
flxback.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBack = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"centerY": "50%",
"height": "100%",
"id": "lblBack",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxback.add(lblBackIcon, lblBack);
var lblAccAlertHeader = new kony.ui.Label({
"centerX": "50%",
"height": "90%",
"id": "lblAccAlertHeader",
"isVisible": true,
"skin": "lblAmountCurrency",
"text": kony.i18n.getLocalizedString("i18n.common.alertsAcc"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxAccAlertHeader.add(flxback, lblAccAlertHeader);
var flxMain = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": true,
"clipBounds": true,
"enableScrolling": true,
"height": "91%",
"horizontalScrollIndicator": true,
"id": "flxMain",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "0dp",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 1
}, {}, {});
flxMain.setDefaultUnit(kony.flex.DP);
var lblInfo = new kony.ui.Label({
"bottom": "2%",
"centerX": "50%",
"id": "lblInfo",
"isVisible": true,
"right": "0dp",
"skin": "slInfo",
"text": "Please select alert type you wish to recieve.",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "2%",
"width": "90%",
"zIndex": 2
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxAccount = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "15%",
"id": "flxAccount",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAccount.setDefaultUnit(kony.flex.DP);
var flxSMSSwitchDIsabled = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxSMSSwitchDIsabled",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": "5%",
"skin": "sknflxWhiteOpacity60",
"top": "0dp",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxSMSSwitchDIsabled.setDefaultUnit(kony.flex.DP);
var CopyflxInnerDisabled0fb005163021246 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "CopyflxInnerDisabled0fb005163021246",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknFlxDarkBlueRoundCornerGrey",
"top": "0dp",
"width": "25dp",
"zIndex": 1
}, {}, {});
CopyflxInnerDisabled0fb005163021246.setDefaultUnit(kony.flex.DP);
CopyflxInnerDisabled0fb005163021246.add();
var CopyflxILineDisabled0f2bb691d7a4a4c = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "14dp",
"id": "CopyflxILineDisabled0f2bb691d7a4a4c",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 10,
"skin": "sknLineDarkBlueOpacity60",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
CopyflxILineDisabled0f2bb691d7a4a4c.setDefaultUnit(kony.flex.DP);
CopyflxILineDisabled0f2bb691d7a4a4c.add();
flxSMSSwitchDIsabled.add(CopyflxInnerDisabled0fb005163021246, CopyflxILineDisabled0f2bb691d7a4a4c);
var flxSMSSwitchEnabled = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxSMSSwitchEnabled",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "5%",
"skin": "sknflxyellow",
"top": "0dp",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxSMSSwitchEnabled.setDefaultUnit(kony.flex.DP);
var CopyflxInnerEnabled0fac198457b0545 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "CopyflxInnerEnabled0fac198457b0545",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "sknFlxDarkBlueRoundCorner",
"top": "0dp",
"width": "25dp",
"zIndex": 1
}, {}, {});
CopyflxInnerEnabled0fac198457b0545.setDefaultUnit(kony.flex.DP);
CopyflxInnerEnabled0fac198457b0545.add();
var CopyflxLineEnabled0f5c291405f444e = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "14dp",
"id": "CopyflxLineEnabled0f5c291405f444e",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10dp",
"skin": "sknLineDarkBlue",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
CopyflxLineEnabled0f5c291405f444e.setDefaultUnit(kony.flex.DP);
CopyflxLineEnabled0f5c291405f444e.add();
flxSMSSwitchEnabled.add(CopyflxInnerEnabled0fac198457b0545, CopyflxLineEnabled0f5c291405f444e);
var flcContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flcContainer",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "5%",
"skin": "slFbox",
"top": "0dp",
"width": "75%",
"zIndex": 1
}, {}, {});
flcContainer.setDefaultUnit(kony.flex.DP);
var lblAccountType = new kony.ui.Label({
"id": "lblAccountType",
"isVisible": true,
"right": "0%",
"maxNumberOfLines": 1,
"skin": "lblAccountType",
"text": "Savings Account",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "23%",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblAccountNumber = new kony.ui.Label({
"id": "lblAccountNumber",
"isVisible": true,
"right": "0%",
"maxNumberOfLines": 1,
"skin": "sknLblBack",
"text": "***077",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"top": "0%",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flcContainer.add(lblAccountType, lblAccountNumber);
var flxLineAlertBottom = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2%",
"id": "flxLineAlertBottom",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "flxUnderLine",
"top": "98%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxLineAlertBottom.setDefaultUnit(kony.flex.DP);
flxLineAlertBottom.add();
var flxLineAlertTop = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "2%",
"id": "flxLineAlertTop",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "flxUnderLine",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxLineAlertTop.setDefaultUnit(kony.flex.DP);
flxLineAlertTop.add();
flxAccount.add(flxSMSSwitchDIsabled, flxSMSSwitchEnabled, flcContainer, flxLineAlertBottom, flxLineAlertTop);
var flxWithdrawl = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "12%",
"id": "flxWithdrawl",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxWithdrawl.setDefaultUnit(kony.flex.DP);
var flxWithdrawlDIsabled = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxWithdrawlDIsabled",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": "5%",
"skin": "sknflxWhiteOpacity60",
"top": "0dp",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxWithdrawlDIsabled.setDefaultUnit(kony.flex.DP);
var flxInnerDisabled = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxInnerDisabled",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknFlxDarkBlueRoundCornerGrey",
"top": "0dp",
"width": "25dp",
"zIndex": 1
}, {}, {});
flxInnerDisabled.setDefaultUnit(kony.flex.DP);
flxInnerDisabled.add();
var flxILineDisabled = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "14dp",
"id": "flxILineDisabled",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 10,
"skin": "sknLineDarkBlueOpacity60",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
flxILineDisabled.setDefaultUnit(kony.flex.DP);
flxILineDisabled.add();
flxWithdrawlDIsabled.add(flxInnerDisabled, flxILineDisabled);
var flxWithdrawlEnabled = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxWithdrawlEnabled",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "5%",
"skin": "sknflxyellow",
"top": "0dp",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxWithdrawlEnabled.setDefaultUnit(kony.flex.DP);
var flxInnerEnabled = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxInnerEnabled",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "sknFlxDarkBlueRoundCorner",
"top": "0dp",
"width": "25dp",
"zIndex": 1
}, {}, {});
flxInnerEnabled.setDefaultUnit(kony.flex.DP);
flxInnerEnabled.add();
var flxLineEnabled = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "14dp",
"id": "flxLineEnabled",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10dp",
"skin": "sknLineDarkBlue",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
flxLineEnabled.setDefaultUnit(kony.flex.DP);
flxLineEnabled.add();
flxWithdrawlEnabled.add(flxInnerEnabled, flxLineEnabled);
var lblWithdrawl = new kony.ui.Label({
"centerY": "50%",
"id": "lblWithdrawl",
"isVisible": true,
"right": "5%",
"maxNumberOfLines": 1,
"skin": "sknLblBack",
"text": "Withdrawl",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "73%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxWithdrawl.add(flxWithdrawlDIsabled, flxWithdrawlEnabled, lblWithdrawl);
var flxDeposit = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "12%",
"id": "flxDeposit",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxDeposit.setDefaultUnit(kony.flex.DP);
var flxDepositDIsabled = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxDepositDIsabled",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": "5%",
"skin": "sknflxWhiteOpacity60",
"top": "0dp",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxDepositDIsabled.setDefaultUnit(kony.flex.DP);
var CopyflxInnerDisabled0a4d06c5f907741 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "CopyflxInnerDisabled0a4d06c5f907741",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknFlxDarkBlueRoundCornerGrey",
"top": "0dp",
"width": "25dp",
"zIndex": 1
}, {}, {});
CopyflxInnerDisabled0a4d06c5f907741.setDefaultUnit(kony.flex.DP);
CopyflxInnerDisabled0a4d06c5f907741.add();
var CopyflxILineDisabled0f8a6e2725f4944 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "14dp",
"id": "CopyflxILineDisabled0f8a6e2725f4944",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 10,
"skin": "sknLineDarkBlueOpacity60",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
CopyflxILineDisabled0f8a6e2725f4944.setDefaultUnit(kony.flex.DP);
CopyflxILineDisabled0f8a6e2725f4944.add();
flxDepositDIsabled.add(CopyflxInnerDisabled0a4d06c5f907741, CopyflxILineDisabled0f8a6e2725f4944);
var flxDepositEnabled = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxDepositEnabled",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "5%",
"skin": "sknflxyellow",
"top": "0dp",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxDepositEnabled.setDefaultUnit(kony.flex.DP);
var CopyflxInnerEnabled0i4ab54a5e53d48 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "CopyflxInnerEnabled0i4ab54a5e53d48",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "sknFlxDarkBlueRoundCorner",
"top": "0dp",
"width": "25dp",
"zIndex": 1
}, {}, {});
CopyflxInnerEnabled0i4ab54a5e53d48.setDefaultUnit(kony.flex.DP);
CopyflxInnerEnabled0i4ab54a5e53d48.add();
var CopyflxLineEnabled0cf46cc35922247 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "14dp",
"id": "CopyflxLineEnabled0cf46cc35922247",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10dp",
"skin": "sknLineDarkBlue",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
CopyflxLineEnabled0cf46cc35922247.setDefaultUnit(kony.flex.DP);
CopyflxLineEnabled0cf46cc35922247.add();
flxDepositEnabled.add(CopyflxInnerEnabled0i4ab54a5e53d48, CopyflxLineEnabled0cf46cc35922247);
var lblDeposit = new kony.ui.Label({
"centerY": "50%",
"id": "lblDeposit",
"isVisible": true,
"right": "5%",
"maxNumberOfLines": 1,
"skin": "sknLblBack",
"text": "Deposit",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "73%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxDeposit.add(flxDepositDIsabled, flxDepositEnabled, lblDeposit);
var flxDailyBalance = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "12%",
"id": "flxDailyBalance",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxDailyBalance.setDefaultUnit(kony.flex.DP);
var flxDailyBalanceDIsabled = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxDailyBalanceDIsabled",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "5%",
"skin": "sknflxWhiteOpacity60",
"top": "0dp",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxDailyBalanceDIsabled.setDefaultUnit(kony.flex.DP);
var CopyflxInnerDisabled0h82238eec08045 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "CopyflxInnerDisabled0h82238eec08045",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknFlxDarkBlueRoundCornerGrey",
"top": "0dp",
"width": "25dp",
"zIndex": 1
}, {}, {});
CopyflxInnerDisabled0h82238eec08045.setDefaultUnit(kony.flex.DP);
CopyflxInnerDisabled0h82238eec08045.add();
var CopyflxILineDisabled0f9b78ac6730541 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "14dp",
"id": "CopyflxILineDisabled0f9b78ac6730541",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 10,
"skin": "sknLineDarkBlueOpacity60",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
CopyflxILineDisabled0f9b78ac6730541.setDefaultUnit(kony.flex.DP);
CopyflxILineDisabled0f9b78ac6730541.add();
flxDailyBalanceDIsabled.add(CopyflxInnerDisabled0h82238eec08045, CopyflxILineDisabled0f9b78ac6730541);
var flxDailyBalanceEnabled = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxDailyBalanceEnabled",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": "5%",
"skin": "sknflxyellow",
"top": "0dp",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxDailyBalanceEnabled.setDefaultUnit(kony.flex.DP);
var CopyflxInnerEnabled0e8bc6ab244d34f = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "CopyflxInnerEnabled0e8bc6ab244d34f",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "sknFlxDarkBlueRoundCorner",
"top": "0dp",
"width": "25dp",
"zIndex": 1
}, {}, {});
CopyflxInnerEnabled0e8bc6ab244d34f.setDefaultUnit(kony.flex.DP);
CopyflxInnerEnabled0e8bc6ab244d34f.add();
var CopyflxLineEnabled0a08ea6a4db4d41 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "14dp",
"id": "CopyflxLineEnabled0a08ea6a4db4d41",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10dp",
"skin": "sknLineDarkBlue",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
CopyflxLineEnabled0a08ea6a4db4d41.setDefaultUnit(kony.flex.DP);
CopyflxLineEnabled0a08ea6a4db4d41.add();
flxDailyBalanceEnabled.add(CopyflxInnerEnabled0e8bc6ab244d34f, CopyflxLineEnabled0a08ea6a4db4d41);
var lblDailyBalance = new kony.ui.Label({
"centerY": "50%",
"id": "lblDailyBalance",
"isVisible": true,
"right": "5%",
"maxNumberOfLines": 1,
"skin": "sknLblBack",
"text": "Daily Balance",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "73%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxDailyBalance.add(flxDailyBalanceDIsabled, flxDailyBalanceEnabled, lblDailyBalance);
var flxMorningBalance = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "12%",
"id": "flxMorningBalance",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxMorningBalance.setDefaultUnit(kony.flex.DP);
var flxMorningBalanceDIsabled = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxMorningBalanceDIsabled",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": "5%",
"skin": "sknflxWhiteOpacity60",
"top": "0dp",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxMorningBalanceDIsabled.setDefaultUnit(kony.flex.DP);
var CopyflxInnerDisabled0ee3f410d6b004c = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "CopyflxInnerDisabled0ee3f410d6b004c",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknFlxDarkBlueRoundCornerGrey",
"top": "0dp",
"width": "25dp",
"zIndex": 1
}, {}, {});
CopyflxInnerDisabled0ee3f410d6b004c.setDefaultUnit(kony.flex.DP);
CopyflxInnerDisabled0ee3f410d6b004c.add();
var CopyflxILineDisabled0c0750eeeeb7749 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "14dp",
"id": "CopyflxILineDisabled0c0750eeeeb7749",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 10,
"skin": "sknLineDarkBlueOpacity60",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
CopyflxILineDisabled0c0750eeeeb7749.setDefaultUnit(kony.flex.DP);
CopyflxILineDisabled0c0750eeeeb7749.add();
flxMorningBalanceDIsabled.add(CopyflxInnerDisabled0ee3f410d6b004c, CopyflxILineDisabled0c0750eeeeb7749);
var flxMorningBalanceEnabled = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxMorningBalanceEnabled",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "5%",
"skin": "sknflxyellow",
"top": "0dp",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxMorningBalanceEnabled.setDefaultUnit(kony.flex.DP);
var CopyflxInnerEnabled0i4bb5e8ea5ad43 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "CopyflxInnerEnabled0i4bb5e8ea5ad43",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "sknFlxDarkBlueRoundCorner",
"top": "0dp",
"width": "25dp",
"zIndex": 1
}, {}, {});
CopyflxInnerEnabled0i4bb5e8ea5ad43.setDefaultUnit(kony.flex.DP);
CopyflxInnerEnabled0i4bb5e8ea5ad43.add();
var CopyflxLineEnabled0h6837d1cc96b4f = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "14dp",
"id": "CopyflxLineEnabled0h6837d1cc96b4f",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10dp",
"skin": "sknLineDarkBlue",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
CopyflxLineEnabled0h6837d1cc96b4f.setDefaultUnit(kony.flex.DP);
CopyflxLineEnabled0h6837d1cc96b4f.add();
flxMorningBalanceEnabled.add(CopyflxInnerEnabled0i4bb5e8ea5ad43, CopyflxLineEnabled0h6837d1cc96b4f);
var lblMorningBalance = new kony.ui.Label({
"centerY": "50%",
"id": "lblMorningBalance",
"isVisible": true,
"right": "5%",
"maxNumberOfLines": 1,
"skin": "sknLblBack",
"text": "Morning Balance",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "73%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxMorningBalance.add(flxMorningBalanceDIsabled, flxMorningBalanceEnabled, lblMorningBalance);
var flxCheckCleared = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "12%",
"id": "flxCheckCleared",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxCheckCleared.setDefaultUnit(kony.flex.DP);
var flxCheckClearedDIsabled = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxCheckClearedDIsabled",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": "5%",
"skin": "sknflxWhiteOpacity60",
"top": "0dp",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxCheckClearedDIsabled.setDefaultUnit(kony.flex.DP);
var CopyflxInnerDisabled0e5495a4b18fa46 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "CopyflxInnerDisabled0e5495a4b18fa46",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknFlxDarkBlueRoundCornerGrey",
"top": "0dp",
"width": "25dp",
"zIndex": 1
}, {}, {});
CopyflxInnerDisabled0e5495a4b18fa46.setDefaultUnit(kony.flex.DP);
CopyflxInnerDisabled0e5495a4b18fa46.add();
var CopyflxILineDisabled0f7593a65f77b4c = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "14dp",
"id": "CopyflxILineDisabled0f7593a65f77b4c",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 10,
"skin": "sknLineDarkBlueOpacity60",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
CopyflxILineDisabled0f7593a65f77b4c.setDefaultUnit(kony.flex.DP);
CopyflxILineDisabled0f7593a65f77b4c.add();
flxCheckClearedDIsabled.add(CopyflxInnerDisabled0e5495a4b18fa46, CopyflxILineDisabled0f7593a65f77b4c);
var flxCheckClearedEnabled = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxCheckClearedEnabled",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "5%",
"skin": "sknflxyellow",
"top": "0dp",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxCheckClearedEnabled.setDefaultUnit(kony.flex.DP);
var CopyflxInnerEnabled0c6e607f400b640 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "CopyflxInnerEnabled0c6e607f400b640",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "sknFlxDarkBlueRoundCorner",
"top": "0dp",
"width": "25dp",
"zIndex": 1
}, {}, {});
CopyflxInnerEnabled0c6e607f400b640.setDefaultUnit(kony.flex.DP);
CopyflxInnerEnabled0c6e607f400b640.add();
var CopyflxLineEnabled0e5031a50560047 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "14dp",
"id": "CopyflxLineEnabled0e5031a50560047",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "10dp",
"skin": "sknLineDarkBlue",
"top": "0dp",
"width": "2dp",
"zIndex": 1
}, {}, {});
CopyflxLineEnabled0e5031a50560047.setDefaultUnit(kony.flex.DP);
CopyflxLineEnabled0e5031a50560047.add();
flxCheckClearedEnabled.add(CopyflxInnerEnabled0c6e607f400b640, CopyflxLineEnabled0e5031a50560047);
var lblCheckClearedBalance = new kony.ui.Label({
"centerY": "50%",
"id": "lblCheckClearedBalance",
"isVisible": true,
"right": "5%",
"maxNumberOfLines": 1,
"skin": "sknLblBack",
"text": "Check Cleared",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"textTruncatePosition": constants.TEXT_TRUNCATE_END,
"width": "73%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxCheckCleared.add(flxCheckClearedDIsabled, flxCheckClearedEnabled, lblCheckClearedBalance);
flxMain.add(lblInfo, flxAccount, flxWithdrawl, flxDeposit, flxDailyBalance, flxMorningBalance, flxCheckCleared);
frmAccountAlertsKA.add(mainContent, flxAccAlertHeader, flxMain);
};
function frmAccountAlertsKAGlobalsAr() {
frmAccountAlertsKAAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmAccountAlertsKAAr,
"bounces": false,
"enableScrolling": false,
"enabledForIdleTimeout": true,
"id": "frmAccountAlertsKA",
"layoutType": kony.flex.FLOW_VERTICAL,
"needAppMenu": false,
"skin": "sknmainGradient"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FLOW_VERTICAL,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": true,
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_6f18be741fd34978b903b8ba30480042,
"retainScrollPosition": true,
"titleBar": true,
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
