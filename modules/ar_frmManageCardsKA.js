//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function addWidgetsfrmManageCardsKAAr() {
frmManageCardsKA.setDefaultUnit(kony.flex.DP);
var flxHeaderDashboard = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "9%",
"id": "flxHeaderDashboard",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 10000
}, {}, {});
flxHeaderDashboard.setDefaultUnit(kony.flex.DP);
var FlexContainer0b9fde1e61d3c44 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlexContainer0b9fde1e61d3c44",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "0%",
"width": "100dp",
"zIndex": 1
}, {}, {});
FlexContainer0b9fde1e61d3c44.setDefaultUnit(kony.flex.DP);
var btnNotification = new kony.ui.Button({
"focusSkin": "BtnNotificationMail",
"height": "100%",
"id": "btnNotification",
"isVisible": false,
"left": "0%",
"skin": "BtnNotificationMail",
"text": "Y",
"top": "0%",
"width": "50%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
var btnProfile = new kony.ui.Button({
"focusSkin": "btnUser",
"height": "50dp",
"id": "btnProfile",
"isVisible": true,
"left": "54dp",
"onClick": AS_Button_c3ed4d0cb9474bf89352edcb29c19d77,
"skin": "btnUser",
"text": "F",
"top": "0dp",
"width": "40dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
FlexContainer0b9fde1e61d3c44.add(btnNotification, btnProfile);
var Image0c5157670502748 = new kony.ui.Image2({
"height": "70%",
"id": "Image0c5157670502748",
"isVisible": false,
"left": "5%",
"skin": "slImage",
"src": "logo03.png",
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {});
var flxBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"left": "2%",
"onClick": AS_FlexContainer_f7559ba967054d3ca60ae5d8e395dedb,
"skin": "slFbox",
"top": "0%",
"width": "20%",
"zIndex": 10
}, {}, {});
flxBack.setDefaultUnit(kony.flex.DP);
var lblBackIcon = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Back to"
},
"centerY": "50%",
"id": "lblBackIcon",
"isVisible": true,
"left": "0dp",
"skin": "sknBackIcon",
"text": kony.i18n.getLocalizedString("i18n.common.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblBack = new kony.ui.Label({
"accessibilityConfig": {
"a11yLabel": "Login Screen"
},
"centerY": "50%",
"id": "lblBack",
"isVisible": true,
"left": "0dp",
"skin": "sknLblBack",
"text": kony.i18n.getLocalizedString("i18n.deposit.back"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [0, 0, 0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxBack.add(lblBackIcon, lblBack);
flxHeaderDashboard.add(FlexContainer0b9fde1e61d3c44, Image0c5157670502748, flxBack);
var overview = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "84%",
"id": "overview",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0dp",
"skin": "flexTransparent",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
overview.setDefaultUnit(kony.flex.DP);
var NoCards = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0%",
"clipBounds": true,
"id": "NoCards",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "skncontainerBkgWhite",
"top": "0%",
"width": "100%",
"zIndex": 5
}, {}, {});
NoCards.setDefaultUnit(kony.flex.DP);
var lblNoRecordsKA = new kony.ui.Label({
"centerX": "50%",
"centerY": "40%",
"id": "lblNoRecordsKA",
"isVisible": true,
"skin": "skn383838LatoRegular107KA",
"text": kony.i18n.getLocalizedString("i18n.cards.nocards"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
NoCards.add(lblNoRecordsKA);
var titleBarAccountInfo = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "8%",
"id": "titleBarAccountInfo",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "-1%",
"skin": "skntitleBarGradient",
"top": "-2%",
"width": "101%",
"zIndex": 1
}, {}, {});
titleBarAccountInfo.setDefaultUnit(kony.flex.DP);
var flxAndroidTittleBarKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "50dp",
"id": "flxAndroidTittleBarKA",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknslFbox",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAndroidTittleBarKA.setDefaultUnit(kony.flex.DP);
var titleBarLabel = new kony.ui.Label({
"centerX": "50%",
"id": "titleBarLabel",
"isVisible": true,
"skin": "sknnavBarTitle",
"text": kony.i18n.getLocalizedString("i18n.manage_cards.manageMyCards"),
"top": "12dp",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var androidBack = new kony.ui.Button({
"focusSkin": "sknandroidBackButtonFocus",
"height": "50dp",
"id": "androidBack",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_23bbeb0da7dc4893801bcf83966f2758,
"skin": "sknandroidBackButton",
"top": "0dp",
"width": "50dp",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxAndroidTittleBarKA.add(titleBarLabel, androidBack);
titleBarAccountInfo.add(flxAndroidTittleBarKA);
var segCardsKA = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"data": [{
"CardHolder": "",
"ValidThru": "",
"cardImage": "",
"cardNumber": "",
"cardType": ""
}],
"groupCells": false,
"height": "40%",
"id": "segCardsKA",
"isVisible": true,
"right": "0dp",
"needPageIndicator": false,
"onSwipe": AS_Segment_35e707464fdc4f248a6509580a55c0ff,
"pageOffDotImage": "inactive.png",
"pageOnDotImage": "active.png",
"retainSelection": false,
"rowSkin": "sknsegCards",
"rowTemplate": flxCreditCardTemplateKA,
"scrollingEvents": {},
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorRequired": true,
"separatorThickness": 0,
"showScrollbars": false,
"top": "-3%",
"viewType": constants.SEGUI_VIEW_TYPE_PAGEVIEW,
"widgetDataMap": {
"CardHolder": "CardHolder",
"ValidThru": "ValidThru",
"cardImage": "cardImage",
"cardNumber": "cardNumber",
"cardType": "cardType",
"flxCardDetails": "flxCardDetails",
"flxCreditCardTemplateKA": "flxCreditCardTemplateKA"
},
"width": "100%"
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var flxPageIndicator = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": false,
"bounces": false,
"centerX": "50%",
"clipBounds": true,
"enableScrolling": true,
"height": "4%",
"horizontalScrollIndicator": false,
"id": "flxPageIndicator",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_HORIZONTAL,
"skin": "slFSbox",
"top": "-1%",
"verticalScrollIndicator": true,
"width": "50%",
"zIndex": 3
}, {}, {});
flxPageIndicator.setDefaultUnit(kony.flex.DP);
flxPageIndicator.add();
var flxCardOptions = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "8%",
"id": "flxCardOptions",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 5
}, {}, {});
flxCardOptions.setDefaultUnit(kony.flex.DP);
var btnInfo = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "btnCardsScreen",
"height": "100%",
"id": "btnInfo",
"isVisible": true,
"right": "15%",
"onClick": AS_Button_f08a441c022b41ff884cb92d6e675d66,
"skin": "btnCardFoc",
"text": "E",
"width": "10%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnTransactions = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "btnCardsScreen",
"height": "100%",
"id": "btnTransactions",
"isVisible": true,
"right": "45%",
"onClick": AS_Button_b6e697c56e6a4285ba2f70e37311924a,
"skin": "btnCardsScreen",
"text": "W",
"width": "10%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var btnSettings = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "btnCardFoc",
"height": "100%",
"id": "btnSettings",
"isVisible": true,
"right": "75%",
"onClick": AS_Button_ac0cdcbada414be9884b89344478de37,
"skin": "btnCardsScreen",
"text": "(",
"width": "10%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var Copydivider0de08502260594c = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider0de08502260594c",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 10
}, {}, {});
Copydivider0de08502260594c.setDefaultUnit(kony.flex.DP);
Copydivider0de08502260594c.add();
flxCardOptions.add(btnInfo, btnTransactions, btnSettings, Copydivider0de08502260594c);
var lblSaperatorSettings = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "0.20%",
"id": "lblSaperatorSettings",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "sknsegmentDivider",
"top": "0%",
"width": "100%",
"zIndex": 100
}, {}, {});
lblSaperatorSettings.setDefaultUnit(kony.flex.DP);
lblSaperatorSettings.add();
var flxSegDetails = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0%",
"clipBounds": true,
"height": "51%",
"id": "flxSegDetails",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 5
}, {}, {});
flxSegDetails.setDefaultUnit(kony.flex.DP);
var flxSettings = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxSettings",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 5
}, {}, {});
flxSettings.setDefaultUnit(kony.flex.DP);
var accountsOuterScroll = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "100%",
"horizontalScrollIndicator": true,
"id": "accountsOuterScroll",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "sknCardsBKG",
"top": "0%",
"verticalScrollIndicator": false,
"width": "100%"
}, {}, {});
accountsOuterScroll.setDefaultUnit(kony.flex.DP);
var flxWebPay = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "22%",
"id": "flxWebPay",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"onClick": AS_FlexContainer_i0658a5be5734810b60eec8a561623a8,
"skin": "skncontainerBkgWhite",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxWebPay.setDefaultUnit(kony.flex.DP);
var lblStaticPayWeb = new kony.ui.Label({
"centerY": "50%",
"id": "lblStaticPayWeb",
"isVisible": true,
"right": "17%",
"skin": "sknNumber",
"text": kony.i18n.getLocalizedString("i18n.webcharge.topup"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "83%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copydivider0c263b496ea8345 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider0c263b496ea8345",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
Copydivider0c263b496ea8345.setDefaultUnit(kony.flex.DP);
Copydivider0c263b496ea8345.add();
var CopylblCcPaynowIcon0c2ec04f4fd954c = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "CopylblCcPaynowIcon0c2ec04f4fd954c",
"isVisible": true,
"right": "2%",
"skin": "sknBOJFont2White146OPC55",
"text": "D",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxWebPay.add(lblStaticPayWeb, Copydivider0c263b496ea8345, CopylblCcPaynowIcon0c2ec04f4fd954c);
var flxCreditCardPay = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "22%",
"id": "flxCreditCardPay",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"onClick": AS_FlexContainer_ede0e576463c4c44b01e92c41cac5a59,
"skin": "skncontainerBkgWhite",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxCreditCardPay.setDefaultUnit(kony.flex.DP);
var CopylblStaticPayCredit = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblStaticPayCredit",
"isVisible": true,
"right": "17%",
"skin": "sknNumber",
"text": kony.i18n.getLocalizedString("i18n.cards.paynow"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copydivider0d299926bd85a4f = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider0d299926bd85a4f",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
Copydivider0d299926bd85a4f.setDefaultUnit(kony.flex.DP);
Copydivider0d299926bd85a4f.add();
var lblCcPaynowIcon = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblCcPaynowIcon",
"isVisible": true,
"right": "2%",
"skin": "sknBOJFont2White146OPC55",
"text": "D",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxCreditCardPay.add(CopylblStaticPayCredit, Copydivider0d299926bd85a4f, lblCcPaynowIcon);
var flxReqStmntCreditCard = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "22%",
"id": "flxReqStmntCreditCard",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"onClick": AS_FlexContainer_a343dfc33e9a4cfe8fdb276c1255716b,
"skin": "skncontainerBkgWhite",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxReqStmntCreditCard.setDefaultUnit(kony.flex.DP);
var CopylblStaticPayCredit0bebcd59cf75547 = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblStaticPayCredit0bebcd59cf75547",
"isVisible": true,
"right": "17%",
"skin": "sknNumber",
"text": kony.i18n.getLocalizedString("i18n.accounts.requestStatement"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copydivider0ad469050e86b44 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider0ad469050e86b44",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
Copydivider0ad469050e86b44.setDefaultUnit(kony.flex.DP);
Copydivider0ad469050e86b44.add();
var CopylblCcPaynowIcon0f942149d534c46 = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "CopylblCcPaynowIcon0f942149d534c46",
"isVisible": true,
"right": "2%",
"skin": "sknBOJFont2White146OPC55",
"text": "S",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxReqStmntCreditCard.add(CopylblStaticPayCredit0bebcd59cf75547, Copydivider0ad469050e86b44, CopylblCcPaynowIcon0f942149d534c46);
var flxRedeemNow = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "22%",
"id": "flxRedeemNow",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"onClick": AS_FlexContainer_g56b1feec34e409692fadfd0b922ae1f,
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxRedeemNow.setDefaultUnit(kony.flex.DP);
var CopylblCcPaynowIcon0ia9bd60362544a = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "CopylblCcPaynowIcon0ia9bd60362544a",
"isVisible": true,
"right": "2%",
"skin": "sknBOJFont2White146OPC55",
"text": "N",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblStaticPayCredit0e078b0bd4a304d = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblStaticPayCredit0e078b0bd4a304d",
"isVisible": true,
"right": "17%",
"skin": "sknNumber",
"text": kony.i18n.getLocalizedString("i18n.cards.redeemnow"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copydivider0ce2da82b3c0c4b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider0ce2da82b3c0c4b",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
Copydivider0ce2da82b3c0c4b.setDefaultUnit(kony.flex.DP);
Copydivider0ce2da82b3c0c4b.add();
flxRedeemNow.add(CopylblCcPaynowIcon0ia9bd60362544a, CopylblStaticPayCredit0e078b0bd4a304d, Copydivider0ce2da82b3c0c4b);
var flxCardLimitUpdate = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "22%",
"id": "flxCardLimitUpdate",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"onClick": AS_FlexContainer_ddf42bc10fe04798976309009236eebe,
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxCardLimitUpdate.setDefaultUnit(kony.flex.DP);
var CopylblCcPaynowIcon0g744f3e6b04947 = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "CopylblCcPaynowIcon0g744f3e6b04947",
"isVisible": true,
"right": "2%",
"skin": "sknBOJFont2White146OPC55",
"text": "K",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblStaticPayCredit0d13bc64ee1da46 = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblStaticPayCredit0d13bc64ee1da46",
"isVisible": true,
"right": "17%",
"skin": "sknNumber",
"text": kony.i18n.getLocalizedString("i18n.cards.updatelimit"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copydivider0b9d7b194f2eb47 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider0b9d7b194f2eb47",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
Copydivider0b9d7b194f2eb47.setDefaultUnit(kony.flex.DP);
Copydivider0b9d7b194f2eb47.add();
flxCardLimitUpdate.add(CopylblCcPaynowIcon0g744f3e6b04947, CopylblStaticPayCredit0d13bc64ee1da46, Copydivider0b9d7b194f2eb47);
var flxATMPOSLimit = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "22%",
"id": "flxATMPOSLimit",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"onClick": AS_FlexContainer_f886c30dfe664bcbaec73e5b0b270b12,
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxATMPOSLimit.setDefaultUnit(kony.flex.DP);
var lblATMPOSIcon = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblATMPOSIcon",
"isVisible": true,
"right": "2%",
"skin": "sknBOJFont2White146OPC55",
"text": "b",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblATMPOSLimitTitle = new kony.ui.Label({
"centerY": "50%",
"id": "lblATMPOSLimitTitle",
"isVisible": true,
"right": "17%",
"skin": "sknNumber",
"text": kony.i18n.getLocalizedString("i18n.atmpos.updatelimit"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copydivider0e2383c368c2e42 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider0e2383c368c2e42",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
Copydivider0e2383c368c2e42.setDefaultUnit(kony.flex.DP);
Copydivider0e2383c368c2e42.add();
flxATMPOSLimit.add(lblATMPOSIcon, lblATMPOSLimitTitle, Copydivider0e2383c368c2e42);
var flxCardChequeBook = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "22%",
"id": "flxCardChequeBook",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"onClick": AS_FlexContainer_h90ab4723fb144b4b05edb7bdf377722,
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxCardChequeBook.setDefaultUnit(kony.flex.DP);
var lblChequeBookIcon = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblChequeBookIcon",
"isVisible": true,
"right": "2%",
"skin": "sknBOJFont2White146OPC55",
"text": "b",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblChequeBookTitle = new kony.ui.Label({
"centerY": "50%",
"id": "lblChequeBookTitle",
"isVisible": true,
"right": "17%",
"skin": "sknNumber",
"text": kony.i18n.getLocalizedString("i18n.accounts.orderchequebook"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxDividerChequeBook = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "flxDividerChequeBook",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
flxDividerChequeBook.setDefaultUnit(kony.flex.DP);
flxDividerChequeBook.add();
flxCardChequeBook.add(lblChequeBookIcon, lblChequeBookTitle, flxDividerChequeBook);
var flxRequestNewPin = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "22%",
"id": "flxRequestNewPin",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"onClick": AS_FlexContainer_c88036a4223a46309db68b0dc7f9aa65,
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxRequestNewPin.setDefaultUnit(kony.flex.DP);
var CopylblCcPaynowIcon0i407a94003e74f = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "CopylblCcPaynowIcon0i407a94003e74f",
"isVisible": true,
"right": "2%",
"skin": "sknBOJFont2White146OPC55",
"text": "O",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblStaticPayCredit0d266fc4b60cc49 = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblStaticPayCredit0d266fc4b60cc49",
"isVisible": true,
"right": "17%",
"skin": "sknNumber",
"text": kony.i18n.getLocalizedString("i18n.requestPinCard.Title"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copydivider0a84c2ce4ce3b40 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider0a84c2ce4ce3b40",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
Copydivider0a84c2ce4ce3b40.setDefaultUnit(kony.flex.DP);
Copydivider0a84c2ce4ce3b40.add();
flxRequestNewPin.add(CopylblCcPaynowIcon0i407a94003e74f, CopylblStaticPayCredit0d266fc4b60cc49, Copydivider0a84c2ce4ce3b40);
var flxPinManagement = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "22%",
"id": "flxPinManagement",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"onClick": AS_FlexContainer_d3193ec00cd34e9294cac8b6f679259d,
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxPinManagement.setDefaultUnit(kony.flex.DP);
var CopylblCcPaynowIcon0c0a0da7811a942 = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "CopylblCcPaynowIcon0c0a0da7811a942",
"isVisible": true,
"right": "2%",
"skin": "sknBOJFont2White146OPC55",
"text": "O",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblStaticPayCredit0ebc58860502542 = new kony.ui.Label({
"centerY": "50%",
"id": "CopylblStaticPayCredit0ebc58860502542",
"isVisible": true,
"right": "17%",
"skin": "sknNumber",
"text": kony.i18n.getLocalizedString("i18n.title.pinmanagement"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copydivider0e838796d961342 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider0e838796d961342",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
Copydivider0e838796d961342.setDefaultUnit(kony.flex.DP);
Copydivider0e838796d961342.add();
flxPinManagement.add(CopylblCcPaynowIcon0c0a0da7811a942, CopylblStaticPayCredit0ebc58860502542, Copydivider0e838796d961342);
var flxChangeMobileNumber = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "22%",
"id": "flxChangeMobileNumber",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"onClick": AS_FlexContainer_b050356d362049569a7c75eb91b90098,
"skin": "skncontainerBkgWhite",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxChangeMobileNumber.setDefaultUnit(kony.flex.DP);
var lblChangeMobileNumber = new kony.ui.Label({
"centerY": "50%",
"id": "lblChangeMobileNumber",
"isVisible": true,
"right": "17%",
"skin": "sknNumber",
"text": kony.i18n.getLocalizedString("i18n.common.ChangeMobileNumber"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copydivider0a736eb7ad66d4d = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider0a736eb7ad66d4d",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
Copydivider0a736eb7ad66d4d.setDefaultUnit(kony.flex.DP);
Copydivider0a736eb7ad66d4d.add();
var lblCcChangeMobileNumberIcon = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblCcChangeMobileNumberIcon",
"isVisible": true,
"right": "2%",
"skin": "sknBOJFont2White146OPC55",
"text": "R",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxChangeMobileNumber.add(lblChangeMobileNumber, Copydivider0a736eb7ad66d4d, lblCcChangeMobileNumberIcon);
var flxMessageLabelKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "22%",
"id": "flxMessageLabelKA",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "skncontainerBkgWhite",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {}, {});
flxMessageLabelKA.setDefaultUnit(kony.flex.DP);
var lblCardStatusKA = new kony.ui.Label({
"centerX": "50%",
"centerY": "50%",
"id": "lblCardStatusKA",
"isVisible": true,
"skin": "CopysknonboardingHeader0c337cafa49c44c",
"text": "Label",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxMessageLabelKA.add(lblCardStatusKA);
var flxDebitCardLinkedAccounts = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "22%",
"id": "flxDebitCardLinkedAccounts",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"onClick": AS_FlexContainer_cd6e9834e0e046619e9ac2870a6ae06a,
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxDebitCardLinkedAccounts.setDefaultUnit(kony.flex.DP);
var lblDebitCardLinkedAccountsIcon = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "lblDebitCardLinkedAccountsIcon",
"isVisible": true,
"right": "2%",
"skin": "sknBOJFontlinkedActt",
"text": "A",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblDebitCardLinkedAccountsTitle = new kony.ui.Label({
"centerY": "50%",
"id": "lblDebitCardLinkedAccountsTitle",
"isVisible": true,
"right": "17%",
"skin": "sknNumber",
"text": kony.i18n.getLocalizedString("i18n.debitcardlinkedacc.linkedacc"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copydivider0afb9160ce36541 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider0afb9160ce36541",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
Copydivider0afb9160ce36541.setDefaultUnit(kony.flex.DP);
Copydivider0afb9160ce36541.add();
flxDebitCardLinkedAccounts.add(lblDebitCardLinkedAccountsIcon, lblDebitCardLinkedAccountsTitle, Copydivider0afb9160ce36541);
var flxCancelKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "22%",
"id": "flxCancelKA",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"onClick": AS_FlexContainer_aa4bd4fceed14c889dae2ad541b38557,
"skin": "skncontainerBkgWhite",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxCancelKA.setDefaultUnit(kony.flex.DP);
var Copydivider00226d2663f3c46 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider00226d2663f3c46",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
Copydivider00226d2663f3c46.setDefaultUnit(kony.flex.DP);
Copydivider00226d2663f3c46.add();
var btnCancleKA = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "sknsecondaryActionFocus",
"height": "50%",
"id": "btnCancleKA",
"isVisible": false,
"right": "30%",
"onClick": AS_Button_ccee199be9114fafa3ac422bb0f27e9a,
"skin": "sknbtnCards",
"text": kony.i18n.getLocalizedString("i18n.cards.CancelCard"),
"top": "4%",
"width": "60%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopybtnInfo0d62bd32ad43e45 = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "btnCardsScreen",
"height": "100%",
"id": "CopybtnInfo0d62bd32ad43e45",
"isVisible": true,
"right": "2%",
"skin": "btnCardsScreen",
"text": "*",
"top": "2%",
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var lblCancelCard = new kony.ui.Label({
"height": "45%",
"id": "lblCancelCard",
"isVisible": true,
"right": "17%",
"skin": "sknNumber",
"text": kony.i18n.getLocalizedString("i18n.cards.stopCard"),
"top": "5%",
"width": "68%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCancelDesc = new kony.ui.Label({
"height": "50%",
"id": "lblCancelDesc",
"isVisible": true,
"right": "17%",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.cards.cancelCardDesc"),
"top": "50%",
"width": "68%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopySwitch0jca424a0abe547 = new kony.ui.Switch({
"centerY": "50%",
"id": "CopySwitch0jca424a0abe547",
"isVisible": false,
"rightSideText": "ON",
"onSlide": AS_Switch_j9c578f503304ba3952f4fce6e46b1eb,
"left": "3%",
"leftSideText": "OFF",
"selectedIndex": 0,
"skin": "slSwitch",
"width": "14%",
"zIndex": 1
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var flxcancelSwitchOff = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxcancelSwitchOff",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "3%",
"skin": "sknflxGrey",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxcancelSwitchOff.setDefaultUnit(kony.flex.DP);
var CopyflxRoundDBlueOff0c39c11c1bb394a = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "90%",
"id": "CopyflxRoundDBlueOff0c39c11c1bb394a",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknFlxDarkBlueRoundCornerGrey",
"top": "0%",
"width": "55%",
"zIndex": 2
}, {}, {});
CopyflxRoundDBlueOff0c39c11c1bb394a.setDefaultUnit(kony.flex.DP);
CopyflxRoundDBlueOff0c39c11c1bb394a.add();
var CopyflxNaveenbhaiKiLakeer0a84319bcb7e64d = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "60%",
"id": "CopyflxNaveenbhaiKiLakeer0a84319bcb7e64d",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "22%",
"skin": "sknLineDarkBlue",
"top": "0%",
"width": "5%",
"zIndex": 1
}, {}, {});
CopyflxNaveenbhaiKiLakeer0a84319bcb7e64d.setDefaultUnit(kony.flex.DP);
CopyflxNaveenbhaiKiLakeer0a84319bcb7e64d.add();
flxcancelSwitchOff.add(CopyflxRoundDBlueOff0c39c11c1bb394a, CopyflxNaveenbhaiKiLakeer0a84319bcb7e64d);
var flxcancelSwitchOn = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxcancelSwitchOn",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": "3%",
"skin": "sknflxyellow",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxcancelSwitchOn.setDefaultUnit(kony.flex.DP);
var CopyflxRoundDBlue0he9e8c75e3e142 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "90%",
"id": "CopyflxRoundDBlue0he9e8c75e3e142",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "sknFlxDarkBlueRoundCorner",
"top": "0%",
"width": "55%",
"zIndex": 1
}, {}, {});
CopyflxRoundDBlue0he9e8c75e3e142.setDefaultUnit(kony.flex.DP);
CopyflxRoundDBlue0he9e8c75e3e142.add();
var Copyflxlakeer0i27246f817e24a = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "60%",
"id": "Copyflxlakeer0i27246f817e24a",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "22%",
"skin": "sknLineDarkBlue",
"top": "0%",
"width": "5%",
"zIndex": 1
}, {}, {});
Copyflxlakeer0i27246f817e24a.setDefaultUnit(kony.flex.DP);
Copyflxlakeer0i27246f817e24a.add();
flxcancelSwitchOn.add(CopyflxRoundDBlue0he9e8c75e3e142, Copyflxlakeer0i27246f817e24a);
flxCancelKA.add(Copydivider00226d2663f3c46, btnCancleKA, CopybtnInfo0d62bd32ad43e45, lblCancelCard, lblCancelDesc, CopySwitch0jca424a0abe547, flxcancelSwitchOff, flxcancelSwitchOn);
var flxUnblockPin = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "22%",
"id": "flxUnblockPin",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"onClick": AS_FlexContainer_f20fb12748f74d1ba4c769fe41070971,
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxUnblockPin.setDefaultUnit(kony.flex.DP);
var CopylblCcPaynowIcon0g609867d8cbf43 = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "CopylblCcPaynowIcon0g609867d8cbf43",
"isVisible": true,
"right": "2%",
"skin": "sknBOJFont2White146OPC55",
"text": "L",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblStaticPayCredit0df900219f50849 = new kony.ui.Label({
"centerY": "49%",
"id": "CopylblStaticPayCredit0df900219f50849",
"isVisible": true,
"right": "16%",
"skin": "sknNumber",
"text": kony.i18n.getLocalizedString("i18n.cards.unblockPin"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copydivider0df57162110854f = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "1dp",
"id": "Copydivider0df57162110854f",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
Copydivider0df57162110854f.setDefaultUnit(kony.flex.DP);
Copydivider0df57162110854f.add();
flxUnblockPin.add(CopylblCcPaynowIcon0g609867d8cbf43, CopylblStaticPayCredit0df900219f50849, Copydivider0df57162110854f);
var flxDeactivateKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "22%",
"id": "flxDeactivateKA",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"onClick": AS_FlexContainer_fe19cbaf68994ef2b24d6324a92042d6,
"skin": "skncontainerBkgWhite",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxDeactivateKA.setDefaultUnit(kony.flex.DP);
var Copydivider0bd2aac76ff0c4f = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "0.50%",
"id": "Copydivider0bd2aac76ff0c4f",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
Copydivider0bd2aac76ff0c4f.setDefaultUnit(kony.flex.DP);
Copydivider0bd2aac76ff0c4f.add();
var btnActivateKA = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "sknsecondaryActionFocus",
"height": "50%",
"id": "btnActivateKA",
"isVisible": false,
"right": "30%",
"onClick": AS_Button_a804ae36132e488e80cd8f80f302ea68,
"skin": "sknbtnCards",
"text": "Stop Card",
"top": "4%",
"width": "60%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var lblStopCardDesc = new kony.ui.Label({
"height": "50%",
"id": "lblStopCardDesc",
"isVisible": false,
"right": "17%",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.cards.stopCardDesc"),
"top": "50%",
"width": "68%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblStopCard = new kony.ui.Label({
"centerY": "50%",
"height": "45%",
"id": "lblStopCard",
"isVisible": true,
"right": "17%",
"skin": "sknNumber",
"text": kony.i18n.getLocalizedString("i18n.cards.activateordeactivate"),
"width": "68%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopybtnInfo0f7307994311842 = new kony.ui.Button({
"centerY": "50%",
"focusSkin": "btnCardsScreen",
"height": "100%",
"id": "CopybtnInfo0f7307994311842",
"isVisible": true,
"right": "2%",
"skin": "btnCardsScreen",
"text": ")",
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var Switch0d629a02ec15c4b = new kony.ui.Switch({
"centerY": "50%",
"id": "Switch0d629a02ec15c4b",
"isVisible": false,
"rightSideText": "ON",
"onSlide": AS_Switch_da5f6b463ce748a59b262145c9601fd5,
"left": "3%",
"leftSideText": "OFF",
"selectedIndex": 0,
"skin": "slSwitch",
"width": "14%",
"zIndex": 1
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var flxStopCardSwitchOff = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxStopCardSwitchOff",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_gf344720fe1847b499bc348c2d34deac,
"left": "3%",
"skin": "sknflxGrey",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxStopCardSwitchOff.setDefaultUnit(kony.flex.DP);
var CopyflxRoundDBlueOff0ed98162cb87345 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "90%",
"id": "CopyflxRoundDBlueOff0ed98162cb87345",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknFlxDarkBlueRoundCornerGrey",
"top": "0%",
"width": "55%",
"zIndex": 2
}, {}, {});
CopyflxRoundDBlueOff0ed98162cb87345.setDefaultUnit(kony.flex.DP);
CopyflxRoundDBlueOff0ed98162cb87345.add();
var CopyflxNaveenbhaiKiLakeer0b879f5d5aba342 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "60%",
"id": "CopyflxNaveenbhaiKiLakeer0b879f5d5aba342",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "22%",
"skin": "sknLineDarkBlue",
"top": "0%",
"width": "5%",
"zIndex": 1
}, {}, {});
CopyflxNaveenbhaiKiLakeer0b879f5d5aba342.setDefaultUnit(kony.flex.DP);
CopyflxNaveenbhaiKiLakeer0b879f5d5aba342.add();
flxStopCardSwitchOff.add(CopyflxRoundDBlueOff0ed98162cb87345, CopyflxNaveenbhaiKiLakeer0b879f5d5aba342);
var flxStopCardSwitchOn = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "25dp",
"id": "flxStopCardSwitchOn",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_bdd7c303d8aa46ef97afdf1c98e503a7,
"left": "3%",
"skin": "sknflxyellow",
"width": "44dp",
"zIndex": 1
}, {}, {});
flxStopCardSwitchOn.setDefaultUnit(kony.flex.DP);
var CopyflxRoundDBlue0j40e19dd87274a = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "90%",
"id": "CopyflxRoundDBlue0j40e19dd87274a",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "sknFlxDarkBlueRoundCorner",
"top": "0%",
"width": "50%",
"zIndex": 1
}, {}, {});
CopyflxRoundDBlue0j40e19dd87274a.setDefaultUnit(kony.flex.DP);
CopyflxRoundDBlue0j40e19dd87274a.add();
var Copyflxlakeer0e8f83b92988f43 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "60%",
"id": "Copyflxlakeer0e8f83b92988f43",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "22%",
"skin": "sknLineDarkBlue",
"top": "0%",
"width": "5%",
"zIndex": 1
}, {}, {});
Copyflxlakeer0e8f83b92988f43.setDefaultUnit(kony.flex.DP);
Copyflxlakeer0e8f83b92988f43.add();
flxStopCardSwitchOn.add(CopyflxRoundDBlue0j40e19dd87274a, Copyflxlakeer0e8f83b92988f43);
flxDeactivateKA.add(Copydivider0bd2aac76ff0c4f, btnActivateKA, lblStopCardDesc, lblStopCard, CopybtnInfo0f7307994311842, Switch0d629a02ec15c4b, flxStopCardSwitchOff, flxStopCardSwitchOn);
var flxPinKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "22%",
"id": "flxPinKA",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "skncontainerBkgWhite",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxPinKA.setDefaultUnit(kony.flex.DP);
var Copydivider0231b3ee64aa144 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "0.50%",
"id": "Copydivider0231b3ee64aa144",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
Copydivider0231b3ee64aa144.setDefaultUnit(kony.flex.DP);
Copydivider0231b3ee64aa144.add();
var btnChangePin = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "sknsecondaryActionFocus",
"height": "50%",
"id": "btnChangePin",
"isVisible": false,
"right": "30%",
"onClick": AS_Button_d56b1c75f641436a8b7ea4a04631e0bd,
"skin": "sknbtnCards",
"text": kony.i18n.getLocalizedString("i18n.cards.ChangePin"),
"top": "40%",
"width": "60%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopybtnInfo0d2cda38177f548 = new kony.ui.Button({
"centerY": "50%",
"height": "100%",
"id": "CopybtnInfo0d2cda38177f548",
"isVisible": true,
"right": "2%",
"skin": "btnCardsScreen",
"text": "T",
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var lblSpendingLimits = new kony.ui.Label({
"height": "45%",
"id": "lblSpendingLimits",
"isVisible": true,
"right": "18%",
"skin": "sknNumber",
"text": kony.i18n.getLocalizedString("i18n.cards.internetShoppingLimit"),
"top": "5%",
"width": "65%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblLimitsDesc = new kony.ui.Label({
"height": "50%",
"id": "lblLimitsDesc",
"isVisible": true,
"right": "18%",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.cards.enableShoppingLimits"),
"top": "50%",
"width": "65%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopySwitch0g7df2bcb33d54f = new kony.ui.Switch({
"centerY": "50%",
"id": "CopySwitch0g7df2bcb33d54f",
"isVisible": false,
"rightSideText": "ON",
"onSlide": AS_Switch_d22fc48e644c4d20b2d068661169962b,
"left": "3%",
"leftSideText": "OFF",
"selectedIndex": 0,
"skin": "slSwitch",
"width": "14%",
"zIndex": 1
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyflxTouchIDSwitchOff0e66d0041dba045 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "50%",
"id": "CopyflxTouchIDSwitchOff0e66d0041dba045",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "3%",
"skin": "sknflxGreyTransperant",
"width": "12.50%",
"zIndex": 1
}, {}, {});
CopyflxTouchIDSwitchOff0e66d0041dba045.setDefaultUnit(kony.flex.DP);
var CopyflxRoundDBlueOff0b260bd101a6346 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "90%",
"id": "CopyflxRoundDBlueOff0b260bd101a6346",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknFlxDarkBlueRoundCornerGreyWhite",
"top": "0%",
"width": "55%",
"zIndex": 2
}, {}, {});
CopyflxRoundDBlueOff0b260bd101a6346.setDefaultUnit(kony.flex.DP);
CopyflxRoundDBlueOff0b260bd101a6346.add();
var CopyflxNaveenbhaiKiLakeer0h80f8ceb396a49 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "60%",
"id": "CopyflxNaveenbhaiKiLakeer0h80f8ceb396a49",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": "22%",
"skin": "sknflxGrey",
"top": "0%",
"width": "5%",
"zIndex": 1
}, {}, {});
CopyflxNaveenbhaiKiLakeer0h80f8ceb396a49.setDefaultUnit(kony.flex.DP);
CopyflxNaveenbhaiKiLakeer0h80f8ceb396a49.add();
CopyflxTouchIDSwitchOff0e66d0041dba045.add(CopyflxRoundDBlueOff0b260bd101a6346, CopyflxNaveenbhaiKiLakeer0h80f8ceb396a49);
var CopyflxTouchIDSwitchOn0je479136a4024b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "50%",
"id": "CopyflxTouchIDSwitchOn0je479136a4024b",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": "3%",
"skin": "sknflxGreyTransperant",
"width": "12.50%",
"zIndex": 1
}, {}, {});
CopyflxTouchIDSwitchOn0je479136a4024b.setDefaultUnit(kony.flex.DP);
var CopyflxRoundDBlue0c9046f1b78ce46 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "90%",
"id": "CopyflxRoundDBlue0c9046f1b78ce46",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "sknFlxDarkBlueRoundCornerYellowSwitch",
"top": "0%",
"width": "55%",
"zIndex": 1
}, {}, {});
CopyflxRoundDBlue0c9046f1b78ce46.setDefaultUnit(kony.flex.DP);
CopyflxRoundDBlue0c9046f1b78ce46.add();
var Copyflxlakeer0c9567e34bf9746 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "60%",
"id": "Copyflxlakeer0c9567e34bf9746",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "22%",
"skin": "sknLineDarkBlue",
"top": "0%",
"width": "5%",
"zIndex": 1
}, {}, {});
Copyflxlakeer0c9567e34bf9746.setDefaultUnit(kony.flex.DP);
Copyflxlakeer0c9567e34bf9746.add();
CopyflxTouchIDSwitchOn0je479136a4024b.add(CopyflxRoundDBlue0c9046f1b78ce46, Copyflxlakeer0c9567e34bf9746);
flxPinKA.add(Copydivider0231b3ee64aa144, btnChangePin, CopybtnInfo0d2cda38177f548, lblSpendingLimits, lblLimitsDesc, CopySwitch0g7df2bcb33d54f, CopyflxTouchIDSwitchOff0e66d0041dba045, CopyflxTouchIDSwitchOn0je479136a4024b);
var flxReplaceKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "22%",
"id": "flxReplaceKA",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "skncontainerBkgWhite",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxReplaceKA.setDefaultUnit(kony.flex.DP);
var Copydivider0f0d87868ad3f4c = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "0.50%",
"id": "Copydivider0f0d87868ad3f4c",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
Copydivider0f0d87868ad3f4c.setDefaultUnit(kony.flex.DP);
Copydivider0f0d87868ad3f4c.add();
var btnReplaceKA = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "sknsecondaryActionFocus",
"height": "50%",
"id": "btnReplaceKA",
"isVisible": false,
"right": "30%",
"onClick": AS_Button_bb30844208e5400ebee11459de7b7cf6,
"skin": "sknbtnCards",
"text": kony.i18n.getLocalizedString("i18n.cards.ReplaceCard"),
"top": "4%",
"width": "60%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopybtnInfo0a9b4efdd06314b = new kony.ui.Button({
"centerY": "50%",
"height": "100%",
"id": "CopybtnInfo0a9b4efdd06314b",
"isVisible": true,
"right": "2%",
"skin": "btnCardsScreen",
"text": "7",
"width": "15%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var lblLocation = new kony.ui.Label({
"height": "50%",
"id": "lblLocation",
"isVisible": true,
"right": "18%",
"skin": "sknNumber",
"text": kony.i18n.getLocalizedString("i18n.cards.enableECommerceCreditCard"),
"top": "10%",
"width": "65%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblLocationDesc = new kony.ui.Label({
"height": "50%",
"id": "lblLocationDesc",
"isVisible": false,
"right": "18%",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.cards.toProtectAgainstFraud"),
"top": "50%",
"width": "68%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopySwitch0a4693e1175d547 = new kony.ui.Switch({
"centerY": "50%",
"id": "CopySwitch0a4693e1175d547",
"isVisible": false,
"rightSideText": "ON",
"onSlide": AS_Switch_ceb0971d73e247b08022d130f285f5d7,
"left": "3%",
"leftSideText": "OFF",
"selectedIndex": 0,
"skin": "slSwitch",
"width": "14%",
"zIndex": 1
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopyflxTouchIDSwitchOff0a8806fbbc6ff4b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "50%",
"id": "CopyflxTouchIDSwitchOff0a8806fbbc6ff4b",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "3%",
"skin": "sknflxGreyTransperant",
"width": "12.50%",
"zIndex": 1
}, {}, {});
CopyflxTouchIDSwitchOff0a8806fbbc6ff4b.setDefaultUnit(kony.flex.DP);
var CopyflxRoundDBlueOff0cfcb1daff27a40 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "90%",
"id": "CopyflxRoundDBlueOff0cfcb1daff27a40",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknFlxDarkBlueRoundCornerGreyWhite",
"top": "0%",
"width": "55%",
"zIndex": 2
}, {}, {});
CopyflxRoundDBlueOff0cfcb1daff27a40.setDefaultUnit(kony.flex.DP);
CopyflxRoundDBlueOff0cfcb1daff27a40.add();
var CopyflxNaveenbhaiKiLakeer0d4009c1469664a = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "60%",
"id": "CopyflxNaveenbhaiKiLakeer0d4009c1469664a",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": "22%",
"skin": "sknflxGrey",
"top": "0%",
"width": "5%",
"zIndex": 1
}, {}, {});
CopyflxNaveenbhaiKiLakeer0d4009c1469664a.setDefaultUnit(kony.flex.DP);
CopyflxNaveenbhaiKiLakeer0d4009c1469664a.add();
CopyflxTouchIDSwitchOff0a8806fbbc6ff4b.add(CopyflxRoundDBlueOff0cfcb1daff27a40, CopyflxNaveenbhaiKiLakeer0d4009c1469664a);
var CopyflxTouchIDSwitchOn0g8e341f9347247 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "50%",
"id": "CopyflxTouchIDSwitchOn0g8e341f9347247",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": "3%",
"skin": "sknflxGreyTransperant",
"width": "12.50%",
"zIndex": 1
}, {}, {});
CopyflxTouchIDSwitchOn0g8e341f9347247.setDefaultUnit(kony.flex.DP);
var CopyflxRoundDBlue0c1125e09fd7247 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "90%",
"id": "CopyflxRoundDBlue0c1125e09fd7247",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "sknFlxDarkBlueRoundCornerYellowSwitch",
"top": "0%",
"width": "55%",
"zIndex": 1
}, {}, {});
CopyflxRoundDBlue0c1125e09fd7247.setDefaultUnit(kony.flex.DP);
CopyflxRoundDBlue0c1125e09fd7247.add();
var Copyflxlakeer0j85793116f3e4d = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerY": "50%",
"clipBounds": true,
"height": "60%",
"id": "Copyflxlakeer0j85793116f3e4d",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "22%",
"skin": "sknLineDarkBlue",
"top": "0%",
"width": "5%",
"zIndex": 1
}, {}, {});
Copyflxlakeer0j85793116f3e4d.setDefaultUnit(kony.flex.DP);
Copyflxlakeer0j85793116f3e4d.add();
CopyflxTouchIDSwitchOn0g8e341f9347247.add(CopyflxRoundDBlue0c1125e09fd7247, Copyflxlakeer0j85793116f3e4d);
flxReplaceKA.add(Copydivider0f0d87868ad3f4c, btnReplaceKA, CopybtnInfo0a9b4efdd06314b, lblLocation, lblLocationDesc, CopySwitch0a4693e1175d547, CopyflxTouchIDSwitchOff0a8806fbbc6ff4b, CopyflxTouchIDSwitchOn0g8e341f9347247);
var flxReportKA = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "22%",
"id": "flxReportKA",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "skncontainerBkgWhite",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxReportKA.setDefaultUnit(kony.flex.DP);
var Copydivider0f5ad4d2b6efc4f = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": 0,
"clipBounds": true,
"height": "0.50%",
"id": "Copydivider0f5ad4d2b6efc4f",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": 0,
"skin": "sknsegmentDivider",
"width": "100%",
"zIndex": 1
}, {}, {});
Copydivider0f5ad4d2b6efc4f.setDefaultUnit(kony.flex.DP);
Copydivider0f5ad4d2b6efc4f.add();
var btnReportKA = new kony.ui.Button({
"centerX": "49.95%",
"centerY": "49.65%",
"focusSkin": "sknsecondaryActionFocus",
"height": "50%",
"id": "btnReportKA",
"isVisible": true,
"right": "30%",
"onClick": AS_Button_h03ed8d10157444aa4d7bf08ca78264a,
"skin": "sknbtnCards",
"text": kony.i18n.getLocalizedString("i18n.cards.ReportCard"),
"top": "4%",
"width": "60%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxReportKA.add(Copydivider0f5ad4d2b6efc4f, btnReportKA);
accountsOuterScroll.add(flxWebPay, flxCreditCardPay, flxReqStmntCreditCard, flxRedeemNow, flxCardLimitUpdate, flxATMPOSLimit, flxCardChequeBook, flxRequestNewPin, flxPinManagement, flxChangeMobileNumber, flxMessageLabelKA, flxDebitCardLinkedAccounts, flxCancelKA, flxUnblockPin, flxDeactivateKA, flxPinKA, flxReplaceKA, flxReportKA);
flxSettings.add(accountsOuterScroll);
var flxTransactions = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxTransactions",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 5
}, {}, {});
flxTransactions.setDefaultUnit(kony.flex.DP);
var flxSegmentWrapper = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "100%",
"horizontalScrollIndicator": true,
"id": "flxSegmentWrapper",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "sknscrollBkgnone",
"top": "0%",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 2
}, {}, {});
flxSegmentWrapper.setDefaultUnit(kony.flex.DP);
var flxExtraIcons = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "18%",
"id": "flxExtraIcons",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "slFbox",
"top": "0dp",
"width": "100%",
"zIndex": 2
}, {}, {});
flxExtraIcons.setDefaultUnit(kony.flex.DP);
var lblSearch = new kony.ui.Label({
"centerY": "50%",
"id": "lblSearch",
"isVisible": true,
"right": "4%",
"skin": "sknSearchIcon",
"text": "h",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "8%",
"width": "5.50%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var txtSearch = new kony.ui.TextBox2({
"autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
"centerY": "50%",
"focusSkin": "sknTxtBoxSearch",
"height": "80%",
"id": "txtSearch",
"isVisible": true,
"keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
"right": "8%",
"maxTextLength": 30,
"onDone": AS_TextField_d669cf1796924f769522d8f3eb03f365,
"placeholder": kony.i18n.getLocalizedString("i18n.search.SearchTransactions"),
"secureTextEntry": false,
"skin": "sknTxtBoxSearch",
"textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
"top": "13%",
"width": "52%",
"zIndex": 1
}, {
"containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
"contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
"padding": [ 0, 0,3, 0],
"paddingInPixel": false
}, {
"autoFilter": false,
"keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
"viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
});
var lblExport = new kony.ui.Label({
"height": "100%",
"id": "lblExport",
"isVisible": true,
"onTouchEnd": AS_Label_a8a08f40e5f0420e8698cc0896fa5f94,
"left": "12%",
"skin": "CopyslLabel0i2b4c385eb5140",
"text": "l",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": "8%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopylblFilter0f7770a92cff247 = new kony.ui.Label({
"centerY": "50%",
"height": "100%",
"id": "CopylblFilter0f7770a92cff247",
"isVisible": true,
"onTouchEnd": AS_Label_c50d3f14e0f9409c8754f5db31e86013,
"left": "2%",
"skin": "CopyslLabel0b8f2bdd739144d",
"text": "n",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": "8%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxLine = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1%",
"id": "flxLine",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "4%",
"skin": "CopyslFbox0ab42d5f22ae345",
"top": "82%",
"width": "56%",
"zIndex": 2
}, {}, {});
flxLine.setDefaultUnit(kony.flex.DP);
flxLine.add();
var lblCardStatement = new kony.ui.Label({
"height": "100%",
"id": "lblCardStatement",
"isVisible": true,
"onTouchEnd": AS_Label_b2f6f14a22e34b8295b1ea40b1d103ca,
"left": "22%",
"skin": "CopyslLabel0i2b4c385eb5140",
"text": "m",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": "8%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCardStatmentDownload = new kony.ui.Label({
"height": "100%",
"id": "lblCardStatmentDownload",
"isVisible": true,
"onTouchEnd": AS_Label_f60afc5c97b84b23ab5b01c7b5f0c849,
"left": "32%",
"skin": "CopyslLabel0i2b4c385eb5140",
"text": "x",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "0%",
"width": "8%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxExtraIcons.add(lblSearch, txtSearch, lblExport, CopylblFilter0f7770a92cff247, flxLine, lblCardStatement, lblCardStatmentDownload);
var CopyflxLine0a4e3964e40ce45 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1%",
"id": "CopyflxLine0a4e3964e40ce45",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknsegmentDivider",
"top": "0%",
"width": "100%",
"zIndex": 2
}, {}, {});
CopyflxLine0a4e3964e40ce45.setDefaultUnit(kony.flex.DP);
CopyflxLine0a4e3964e40ce45.add();
var accountDetailsTransactions = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "78%",
"id": "accountDetailsTransactions",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "skncontainerBkgWhite",
"top": "1%",
"width": "100%"
}, {}, {});
accountDetailsTransactions.setDefaultUnit(kony.flex.DP);
var transactionSegment = new kony.ui.SegmentedUI2({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"data": [{
"lblCardStatementmerchant": "Payment to City of Austin",
"lblLastTransaction": "",
"transactionAmount": "",
"transactionDate": "Oct 10, 2015 - Recurring Monthly",
"transactionName": "Payment to City of Austin"
}, {
"lblCardStatementmerchant": "Payment to City of Austin",
"lblLastTransaction": "",
"transactionAmount": "",
"transactionDate": "Oct 10, 2015 - Recurring Monthly",
"transactionName": "Payment to City of Austin"
}, {
"lblCardStatementmerchant": "Payment to City of Austin",
"lblLastTransaction": "",
"transactionAmount": "",
"transactionDate": "Oct 10, 2015 - Recurring Monthly",
"transactionName": "Payment to City of Austin"
}],
"groupCells": false,
"height": "100%",
"id": "transactionSegment",
"isVisible": true,
"right": "0%",
"needPageIndicator": true,
"pageOffDotImage": "pageoffdot.png",
"pageOnDotImage": "pageondot.png",
"retainSelection": false,
"rowFocusSkin": "sknsegAcc",
"rowSkin": "sknsegAcc",
"rowTemplate": CopyFlexContainer0a99898df0f2a41,
"scrollingEvents": {},
"sectionHeaderSkin": "sliPhoneSegmentHeader",
"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
"separatorColor": "02acd850",
"separatorRequired": true,
"separatorThickness": 1,
"showScrollbars": false,
"top": "0%",
"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
"widgetDataMap": {
"CopyFlexContainer0a99898df0f2a41": "CopyFlexContainer0a99898df0f2a41",
"lblCardStatementmerchant": "lblCardStatementmerchant",
"lblLastTransaction": "lblLastTransaction",
"transactionAmount": "transactionAmount",
"transactionAmountJOD": "transactionAmountJOD",
"transactionDate": "transactionDate",
"transactionName": "transactionName"
},
"width": "100%"
}, {
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var lblNoResult = new kony.ui.Label({
"centerY": "50%",
"id": "lblNoResult",
"isVisible": false,
"right": "5%",
"skin": "skn383838LatoRegular107KA",
"text": kony.i18n.getLocalizedString("i18n.common.NoTransactions"),
"top": "0%",
"width": "90%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
accountDetailsTransactions.add(transactionSegment, lblNoResult);
flxSegmentWrapper.add(flxExtraIcons, CopyflxLine0a4e3964e40ce45, accountDetailsTransactions);
flxTransactions.add(flxSegmentWrapper);
var flxInfo = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxInfo",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 5
}, {}, {});
flxInfo.setDefaultUnit(kony.flex.DP);
var flxaccountdetailsfordeposits = new kony.ui.FlexScrollContainer({
"allowHorizontalBounce": false,
"allowVerticalBounce": true,
"bounces": false,
"clipBounds": true,
"enableScrolling": true,
"height": "100%",
"horizontalScrollIndicator": true,
"id": "flxaccountdetailsfordeposits",
"isVisible": true,
"layoutType": kony.flex.FLOW_VERTICAL,
"right": "0%",
"pagingEnabled": false,
"scrollDirection": kony.flex.SCROLL_VERTICAL,
"skin": "slFSbox",
"top": "0%",
"verticalScrollIndicator": true,
"width": "100%",
"zIndex": 5
}, {}, {});
flxaccountdetailsfordeposits.setDefaultUnit(kony.flex.DP);
var flxCreditCardStatus = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "22%",
"id": "flxCreditCardStatus",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "flexTransparent",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxCreditCardStatus.setDefaultUnit(kony.flex.DP);
var lblCreditCardStatusTitle = new kony.ui.Label({
"height": "45%",
"id": "lblCreditCardStatusTitle",
"isVisible": true,
"right": "5%",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.accounts.status"),
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCreditCardStatus = new kony.ui.Label({
"height": "50%",
"id": "lblCreditCardStatus",
"isVisible": true,
"right": "5%",
"left": "0%",
"skin": "sknNumber",
"text": "Active",
"top": "50%",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxcardLimit = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxcardLimit",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "slFbox",
"top": "0%",
"width": "50%",
"zIndex": 1
}, {}, {});
flxcardLimit.setDefaultUnit(kony.flex.DP);
var lblCardLimitTilte = new kony.ui.Label({
"height": "45%",
"id": "lblCardLimitTilte",
"isVisible": true,
"right": "5%",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.deposit.availableBalance"),
"top": "5%",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCardLimit = new kony.ui.Label({
"height": "50%",
"id": "lblCardLimit",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "1,000.000 JOD",
"top": "50%",
"width": "95%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxcardLimit.add(lblCardLimitTilte, lblCardLimit);
flxCreditCardStatus.add(lblCreditCardStatusTitle, lblCreditCardStatus, flxcardLimit);
var flxLineInfo = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "0.50%",
"id": "flxLineInfo",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "sknsegmentDivider",
"top": "-1%",
"width": "100%",
"zIndex": 100
}, {}, {});
flxLineInfo.setDefaultUnit(kony.flex.DP);
flxLineInfo.add();
var flxStatusInfo = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "22%",
"id": "flxStatusInfo",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "flexTransparent",
"top": "0.50%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxStatusInfo.setDefaultUnit(kony.flex.DP);
var flxRight2 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxRight2",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "0%",
"width": "50%",
"zIndex": 1
}, {}, {});
flxRight2.setDefaultUnit(kony.flex.DP);
var lblAvailableCreditLimitTitile = new kony.ui.Label({
"height": "45%",
"id": "lblAvailableCreditLimitTitile",
"isVisible": true,
"right": "10%",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.card.availabelcerditlimit"),
"top": "5%",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblAvailableCreditLimit = new kony.ui.Label({
"height": "50%",
"id": "lblAvailableCreditLimit",
"isVisible": true,
"right": "10%",
"skin": "sknNumber",
"text": "1,000.000 JOD",
"top": "50%",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxRight2.add(lblAvailableCreditLimitTitile, lblAvailableCreditLimit);
var flxAvailableBalance = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxAvailableBalance",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "slFbox",
"top": "0%",
"width": "50%",
"zIndex": 1
}, {}, {});
flxAvailableBalance.setDefaultUnit(kony.flex.DP);
var lblAvailableBalanceTitle = new kony.ui.Label({
"height": "45%",
"id": "lblAvailableBalanceTitle",
"isVisible": true,
"right": "5%",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.deposit.availableBalance"),
"top": "5%",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblAvailableBalance = new kony.ui.Label({
"height": "50%",
"id": "lblAvailableBalance",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "1,000.000 JOD",
"top": "50%",
"width": "95%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxAvailableBalance.add(lblAvailableBalanceTitle, lblAvailableBalance);
flxStatusInfo.add(flxRight2, flxAvailableBalance);
var flxLineInfo1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1dp",
"id": "flxLineInfo1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "sknsegmentDivider",
"top": "0%",
"width": "100%",
"zIndex": 100
}, {}, {});
flxLineInfo1.setDefaultUnit(kony.flex.DP);
flxLineInfo1.add();
var flxAccountNumberInfo = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "22%",
"id": "flxAccountNumberInfo",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "flexTransparent",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxAccountNumberInfo.setDefaultUnit(kony.flex.DP);
var lblAccountNumberTitle = new kony.ui.Label({
"height": "45%",
"id": "lblAccountNumberTitle",
"isVisible": true,
"right": "5%",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.bills.IssueDate"),
"top": "5%",
"width": "45%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblAccountNumber = new kony.ui.Label({
"height": "50%",
"id": "lblAccountNumber",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "90010020177801",
"top": "50%",
"width": "45%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyflxInfoLine0afc2797a3f4f43 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "1%",
"id": "CopyflxInfoLine0afc2797a3f4f43",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "CopyslFbox0j28652e02edc44",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0afc2797a3f4f43.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0afc2797a3f4f43.add();
var flxExpiryDate = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxExpiryDate",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "slFbox",
"top": "0%",
"width": "50%",
"zIndex": 1
}, {}, {});
flxExpiryDate.setDefaultUnit(kony.flex.DP);
var lblExpirtDateTitle = new kony.ui.Label({
"height": "45%",
"id": "lblExpirtDateTitle",
"isVisible": true,
"right": "5%",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.common.expirydate"),
"top": "5%",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblExpiryDate = new kony.ui.Label({
"height": "50%",
"id": "lblExpiryDate",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "1,000.000 JOD",
"top": "50%",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxExpiryDate.add(lblExpirtDateTitle, lblExpiryDate);
flxAccountNumberInfo.add(lblAccountNumberTitle, lblAccountNumber, CopyflxInfoLine0afc2797a3f4f43, flxExpiryDate);
var flxLineInfo2 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "0.50%",
"id": "flxLineInfo2",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "sknsegmentDivider",
"top": "-1%",
"width": "100%",
"zIndex": 100
}, {}, {});
flxLineInfo2.setDefaultUnit(kony.flex.DP);
flxLineInfo2.add();
var flxOpeningDate = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "22%",
"id": "flxOpeningDate",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "flexTransparent",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxOpeningDate.setDefaultUnit(kony.flex.DP);
var CopyflxInfoLine0i9acc3be91bd40 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"bottom": "0%",
"clipBounds": true,
"height": "1%",
"id": "CopyflxInfoLine0i9acc3be91bd40",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknflxLineBlue",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0i9acc3be91bd40.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0i9acc3be91bd40.add();
var flxmain = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxmain",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0%",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxmain.setDefaultUnit(kony.flex.DP);
var lblopenDate = new kony.ui.Label({
"centerY": "50%",
"id": "lblopenDate",
"isVisible": false,
"right": "5%",
"skin": "lblListOn",
"text": "T",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxRight = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxRight",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxRight.setDefaultUnit(kony.flex.DP);
var lblCreatedOnTitle = new kony.ui.Label({
"height": "45%",
"id": "lblCreatedOnTitle",
"isVisible": true,
"right": "5%",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.common.createdon"),
"top": "5%",
"width": "95%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCreatedOn = new kony.ui.Label({
"height": "50%",
"id": "lblCreatedOn",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "11 Jan 2015",
"top": "50%",
"width": "95%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxRight.add(lblCreatedOnTitle, lblCreatedOn);
flxmain.add( flxRight,lblopenDate);
flxOpeningDate.add(CopyflxInfoLine0i9acc3be91bd40, flxmain);
var flxLineInfo3 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "0.50%",
"id": "flxLineInfo3",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "sknsegmentDivider",
"top": "0%",
"width": "100%",
"zIndex": 100
}, {}, {});
flxLineInfo3.setDefaultUnit(kony.flex.DP);
flxLineInfo3.add();
var flxOpenedIn = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "22%",
"id": "flxOpenedIn",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "flexTransparent",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxOpenedIn.setDefaultUnit(kony.flex.DP);
var CopyflxInfoLine0b8847aa4c3b749 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "0.50%",
"id": "CopyflxInfoLine0b8847aa4c3b749",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "CopyslFbox0j28652e02edc44",
"top": "10%",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0b8847aa4c3b749.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0b8847aa4c3b749.add();
var lblopen = new kony.ui.Label({
"centerY": "50%",
"id": "lblopen",
"isVisible": false,
"right": "5%",
"skin": "lblListOn",
"text": "T",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxright1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxright1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "0%",
"width": "85%",
"zIndex": 1
}, {}, {});
flxright1.setDefaultUnit(kony.flex.DP);
var lblSpendCreditLimitTitle = new kony.ui.Label({
"height": "45%",
"id": "lblSpendCreditLimitTitle",
"isVisible": true,
"right": "5%",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.common.spendcreditlimit"),
"top": "5%",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblSpendCreditLimit = new kony.ui.Label({
"height": "50%",
"id": "lblSpendCreditLimit",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "1,000.000 JOD",
"top": "50%",
"width": "95%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxright1.add(lblSpendCreditLimitTitle, lblSpendCreditLimit);
flxOpenedIn.add(CopyflxInfoLine0b8847aa4c3b749, lblopen, flxright1);
var flxLineInfo4 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "0.50%",
"id": "flxLineInfo4",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "sknsegmentDivider",
"top": "0%",
"width": "100%",
"zIndex": 100
}, {}, {});
flxLineInfo4.setDefaultUnit(kony.flex.DP);
flxLineInfo4.add();
var flxIBAN = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "22%",
"id": "flxIBAN",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "flexTransparent",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxIBAN.setDefaultUnit(kony.flex.DP);
var CopyLabel0beabfcfbe34248 = new kony.ui.Label({
"height": "45%",
"id": "CopyLabel0beabfcfbe34248",
"isVisible": true,
"right": "53%",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.bills.DueDate"),
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblNextDueDate = new kony.ui.Label({
"height": "50%",
"id": "lblNextDueDate",
"isVisible": true,
"right": "53%",
"skin": "sknNumber",
"text": "31 MAR 2018",
"top": "50%",
"width": "45%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyLabel0ac568cc4aa6246 = new kony.ui.Label({
"height": "45%",
"id": "CopyLabel0ac568cc4aa6246",
"isVisible": true,
"right": "5%",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.bills.DueAmount"),
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblMinimumDueAmount = new kony.ui.Label({
"height": "50%",
"id": "lblMinimumDueAmount",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "50.000 JOD",
"top": "50%",
"width": "45%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxIBAN.add(CopyLabel0beabfcfbe34248, lblNextDueDate, CopyLabel0ac568cc4aa6246, lblMinimumDueAmount);
var flxLineInfo5 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "0.50%",
"id": "flxLineInfo5",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"left": "0%",
"skin": "sknsegmentDivider",
"top": "0%",
"width": "100%",
"zIndex": 100
}, {}, {});
flxLineInfo5.setDefaultUnit(kony.flex.DP);
flxLineInfo5.add();
var CopyflxIBAN0hd4c5a6edeee48 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "22%",
"id": "CopyflxIBAN0hd4c5a6edeee48",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "flexTransparent",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxIBAN0hd4c5a6edeee48.setDefaultUnit(kony.flex.DP);
var CopyLabel0b434f9f1641447 = new kony.ui.Label({
"height": "45%",
"id": "CopyLabel0b434f9f1641447",
"isVisible": true,
"right": "5%",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.card.earnedrewardpoints"),
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblEarnedRewardPoints = new kony.ui.Label({
"height": "50%",
"id": "lblEarnedRewardPoints",
"isVisible": true,
"right": "5%",
"left": "0%",
"skin": "sknNumber",
"text": "140",
"top": "50%",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopyinterestRateLabel0d5c033ed251945 = new kony.ui.Label({
"height": "50%",
"id": "CopyinterestRateLabel0d5c033ed251945",
"isVisible": false,
"right": "55%",
"left": "0%",
"skin": "sknNumber",
"text": kony.i18n.getLocalizedString("i18n.cards.redeemnow"),
"top": "25%",
"width": "45%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCcMobileNumber = new kony.ui.Label({
"height": "50%",
"id": "lblCcMobileNumber",
"isVisible": true,
"right": "53%",
"skin": "sknNumber",
"text": "962790407197",
"top": "50%",
"width": "45%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblCcMobileNumberTitle = new kony.ui.Label({
"height": "45%",
"id": "lblCcMobileNumberTitle",
"isVisible": true,
"right": "53%",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.myprofile.MobileNumber"),
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
CopyflxIBAN0hd4c5a6edeee48.add(CopyLabel0b434f9f1641447, lblEarnedRewardPoints, CopyinterestRateLabel0d5c033ed251945, lblCcMobileNumber, lblCcMobileNumberTitle);
var flxLineInfo6 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "0.50%",
"id": "flxLineInfo6",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknsegmentDivider",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxLineInfo6.setDefaultUnit(kony.flex.DP);
flxLineInfo6.add();
var CopyflxOpenedIn0b6e2187bba4f41 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "22%",
"id": "CopyflxOpenedIn0b6e2187bba4f41",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "flexTransparent",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxOpenedIn0b6e2187bba4f41.setDefaultUnit(kony.flex.DP);
var CopyflxInfoLine0j0b4617c31314f = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "0.50%",
"id": "CopyflxInfoLine0j0b4617c31314f",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "CopyslFbox0j28652e02edc44",
"top": "10%",
"width": "100%",
"zIndex": 1
}, {}, {});
CopyflxInfoLine0j0b4617c31314f.setDefaultUnit(kony.flex.DP);
CopyflxInfoLine0j0b4617c31314f.add();
var Copylblopen0c6ef38f649c841 = new kony.ui.Label({
"centerY": "50%",
"id": "Copylblopen0c6ef38f649c841",
"isVisible": false,
"right": "5%",
"skin": "lblListOn",
"text": "T",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var Copyflxright0if8603499bf54b = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "Copyflxright0if8603499bf54b",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "slFbox",
"top": "0%",
"width": "85%",
"zIndex": 1
}, {}, {});
Copyflxright0if8603499bf54b.setDefaultUnit(kony.flex.DP);
var CopyinterestRateLabel0aaaa0c9e91c743 = new kony.ui.Label({
"centerY": "50%",
"height": "50%",
"id": "CopyinterestRateLabel0aaaa0c9e91c743",
"isVisible": true,
"right": "5%",
"skin": "sknNumber",
"text": "Pay Now",
"width": "95%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
Copyflxright0if8603499bf54b.add(CopyinterestRateLabel0aaaa0c9e91c743);
CopyflxOpenedIn0b6e2187bba4f41.add(CopyflxInfoLine0j0b4617c31314f, Copylblopen0c6ef38f649c841, Copyflxright0if8603499bf54b);
var flxnewEPN = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "22%",
"id": "flxnewEPN",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "flexTransparent",
"top": "0%",
"width": "100%",
"zIndex": 1
}, {}, {});
flxnewEPN.setDefaultUnit(kony.flex.DP);
var lblInfoNewEPN = new kony.ui.Label({
"height": "45%",
"id": "lblInfoNewEPN",
"isVisible": true,
"right": "5%",
"skin": "loansDealsTextSkin",
"text": kony.i18n.getLocalizedString("i18n.cards.epn"),
"top": "5%",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblInfoNewEPN1 = new kony.ui.Label({
"height": "50%",
"id": "lblInfoNewEPN1",
"isVisible": true,
"right": "5%",
"left": "0%",
"skin": "sknNumber",
"top": "50%",
"width": kony.flex.USE_PREFFERED_SIZE
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxnewEPN.add(lblInfoNewEPN, lblInfoNewEPN1);
flxaccountdetailsfordeposits.add(flxCreditCardStatus, flxLineInfo, flxStatusInfo, flxLineInfo1, flxAccountNumberInfo, flxLineInfo2, flxOpeningDate, flxLineInfo3, flxOpenedIn, flxLineInfo4, flxIBAN, flxLineInfo5, CopyflxIBAN0hd4c5a6edeee48, flxLineInfo6, CopyflxOpenedIn0b6e2187bba4f41, flxnewEPN);
flxInfo.add(flxaccountdetailsfordeposits);
flxSegDetails.add(flxSettings, flxTransactions, flxInfo);
var flxAccountdetailsSortContainer = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"focusSkin": "sknflxTransprnt",
"height": "100%",
"id": "flxAccountdetailsSortContainer",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknflxTransprnt",
"top": "0%",
"width": "100%",
"zIndex": 5
}, {}, {});
flxAccountdetailsSortContainer.setDefaultUnit(kony.flex.DP);
var flxAccountDetailsSort = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "30%",
"id": "flxAccountDetailsSort",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0%",
"skin": "sknWhiteBGroundedBorder",
"top": "70%",
"width": "100%",
"zIndex": 3
}, {}, {});
flxAccountDetailsSort.setDefaultUnit(kony.flex.DP);
var lblSortby = new kony.ui.Label({
"centerX": "50%",
"centerY": "10%",
"id": "lblSortby",
"isVisible": true,
"skin": "sknCairoBold120",
"text": kony.i18n.getLocalizedString("i18n.common.sortby"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxLine1 = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "50%",
"centerY": "22%",
"clipBounds": true,
"height": "1%",
"id": "flxLine1",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sknGreyLine50Opc",
"top": 42,
"width": "80%",
"zIndex": 1
}, {}, {});
flxLine1.setDefaultUnit(kony.flex.DP);
flxLine1.add();
var lblSortDate = new kony.ui.Label({
"centerX": "40%",
"centerY": "40.00%",
"id": "lblSortDate",
"isVisible": true,
"skin": "sknCarioSemiBold130perDRKBluefont80Opc",
"text": kony.i18n.getLocalizedString("i18n.accounts.date"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxSortDateAsc = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "60%",
"centerY": "40%",
"clipBounds": true,
"height": "17%",
"id": "flxSortDateAsc",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_c24e978159bc42a58b3777811d3516cc,
"skin": "slFbox",
"width": "6%",
"zIndex": 1
}, {}, {});
flxSortDateAsc.setDefaultUnit(kony.flex.DP);
var imgSortDateAsc = new kony.ui.Image2({
"centerX": "50%",
"centerY": "50%",
"id": "imgSortDateAsc",
"isVisible": true,
"skin": "slImage",
"src": "map_arrow_straight_nonselected.png",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxSortDateAsc.add(imgSortDateAsc);
var flxSortDateDesc = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "68%",
"centerY": "40%",
"clipBounds": true,
"height": "17%",
"id": "flxSortDateDesc",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_jd29c0bc8d82461f8b44e768eacafcdf,
"skin": "slFbox",
"width": "6%",
"zIndex": 1
}, {}, {});
flxSortDateDesc.setDefaultUnit(kony.flex.DP);
var imgSortDateDesc = new kony.ui.Image2({
"centerX": "50%",
"centerY": "50%",
"id": "imgSortDateDesc",
"isVisible": true,
"skin": "slImage",
"src": "map_arrow_down_nonselected.png",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxSortDateDesc.add(imgSortDateDesc);
var lblSortAmount = new kony.ui.Label({
"centerX": "40%",
"centerY": "65%",
"id": "lblSortAmount",
"isVisible": true,
"skin": "sknCarioSemiBold130perDRKBluefont80Opc",
"text": kony.i18n.getLocalizedString("i18n.accounts.amount"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var flxSortAmountAsc = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "60%",
"centerY": "65%",
"clipBounds": true,
"height": "17%",
"id": "flxSortAmountAsc",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_c9556d7feaf7477a82431021398a81f1,
"skin": "slFbox",
"width": "6%",
"zIndex": 1
}, {}, {});
flxSortAmountAsc.setDefaultUnit(kony.flex.DP);
var imgSortAmountAsc = new kony.ui.Image2({
"centerX": "50%",
"centerY": "50%",
"id": "imgSortAmountAsc",
"isVisible": true,
"skin": "slImage",
"src": "map_arrow_straight_nonselected.png",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxSortAmountAsc.add(imgSortAmountAsc);
var flxSortAmountDesc = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"centerX": "68%",
"centerY": "65%",
"clipBounds": true,
"height": "17%",
"id": "flxSortAmountDesc",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"onClick": AS_FlexContainer_bc6eab6059b0481e86141054b550d290,
"skin": "slFbox",
"width": "6%",
"zIndex": 1
}, {}, {});
flxSortAmountDesc.setDefaultUnit(kony.flex.DP);
var imgSortAmountDesc = new kony.ui.Image2({
"centerX": "50%",
"centerY": "50%",
"id": "imgSortAmountDesc",
"isVisible": true,
"skin": "slImage",
"src": "map_arrow_down_nonselected.png",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxSortAmountDesc.add(imgSortAmountDesc);
flxAccountDetailsSort.add(lblSortby, flxLine1, lblSortDate, flxSortDateAsc, flxSortDateDesc, lblSortAmount, flxSortAmountAsc, flxSortAmountDesc);
flxAccountdetailsSortContainer.add(flxAccountDetailsSort);
overview.add(NoCards, titleBarAccountInfo, segCardsKA, flxPageIndicator, flxCardOptions, lblSaperatorSettings, flxSegDetails, flxAccountdetailsSortContainer);
var footerBack = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "7%",
"id": "footerBack",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": 0,
"skin": "skncontainerBkgWhite",
"top": "0%",
"width": "100%"
}, {}, {});
footerBack.setDefaultUnit(kony.flex.DP);
var footerBackground = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": false,
"height": "100%",
"id": "footerBackground",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0",
"skin": "menu",
"top": "0",
"width": "100%"
}, {}, {});
footerBackground.setDefaultUnit(kony.flex.DP);
var FlxAccounts = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlxAccounts",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "sknfocusmenu",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
FlxAccounts.setDefaultUnit(kony.flex.DP);
var img1 = new kony.ui.Image2({
"centerX": "50%",
"height": "28dp",
"id": "img1",
"isVisible": false,
"right": "23dp",
"skin": "sknslImage",
"src": "tab_accounts_icon_inactive.png",
"top": "4dp",
"width": "28dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var Label03174bff69bb54c = new kony.ui.Label({
"centerX": "50%",
"id": "Label03174bff69bb54c",
"isVisible": true,
"skin": "sknlblFootertitle",
"text": kony.i18n.getLocalizedString("i18n.my_money.accounts"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "55%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var btnAccounts = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnCardFoc",
"height": "50dp",
"id": "btnAccounts",
"isVisible": true,
"onClick": AS_Button_a43c1561f0914506a5b32c7edf5a5e36,
"skin": "btnCard",
"text": "H",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 14],
"paddingInPixel": false
}, {});
FlxAccounts.add(img1, Label03174bff69bb54c, btnAccounts);
var FlxTranfers = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlxTranfers",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_a15e1d0c695f43d1b82c298889044e67,
"skin": "slFbox",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
FlxTranfers.setDefaultUnit(kony.flex.DP);
var img2 = new kony.ui.Image2({
"centerX": "50%",
"height": "28dp",
"id": "img2",
"isVisible": false,
"right": "23dp",
"skin": "sknslImage",
"src": "tab_t_and_p_icon_inactive.png",
"top": "4dp",
"width": "28dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var Label02bec01fd5baf4c = new kony.ui.Label({
"centerX": "50%",
"id": "Label02bec01fd5baf4c",
"isVisible": true,
"skin": "sknlblFootertitleFocus",
"text": kony.i18n.getLocalizedString("i18n.billsPay.Cards"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "55%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopybtnAccounts0ia3b1ff37c304b = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnfooterfont2enable",
"height": "50dp",
"id": "CopybtnAccounts0ia3b1ff37c304b",
"isVisible": true,
"onClick": AS_Button_a1b6481b8fb448d5aedd5800b105e99d,
"skin": "sknlblFooter2Enable",
"text": "G",
"width": "100%",
"zIndex": 10
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 14],
"paddingInPixel": false
}, {});
FlxTranfers.add(img2, Label02bec01fd5baf4c, CopybtnAccounts0ia3b1ff37c304b);
var FlxBot = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlxBot",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_be03628a8247475cbe29852ea3ab6c7d,
"skin": "slFbox",
"top": "0dp",
"width": "20%",
"zIndex": 2
}, {}, {});
FlxBot.setDefaultUnit(kony.flex.DP);
var imgBot = new kony.ui.Image2({
"centerX": "50%",
"height": "40dp",
"id": "imgBot",
"isVisible": false,
"right": "13dp",
"skin": "slImage",
"src": "chaticonactive.png",
"top": "4dp",
"width": "40dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var CopybtnAccounts0ff48f9feb0aa42 = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnCardFoc",
"height": "50dp",
"id": "CopybtnAccounts0ff48f9feb0aa42",
"isVisible": true,
"onClick": AS_Button_j5de3baed4d7441daad9d77b0d9d7556,
"skin": "btnCard",
"text": "i",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 14],
"paddingInPixel": false
}, {});
var CopyLabel0i584dcaf2e8546 = new kony.ui.Label({
"centerX": "50%",
"id": "CopyLabel0i584dcaf2e8546",
"isVisible": true,
"skin": "sknlblFootertitle",
"text": kony.i18n.getLocalizedString("i18n.alert.transfers"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "55%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
FlxBot.add(imgBot, CopybtnAccounts0ff48f9feb0aa42, CopyLabel0i584dcaf2e8546);
var FlxDeposits = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlxDeposits",
"isVisible": false,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_cea7224b6676466995b6768a5f214055,
"skin": "sknslFbox",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
FlxDeposits.setDefaultUnit(kony.flex.DP);
var img3 = new kony.ui.Image2({
"centerX": "50%",
"height": "28dp",
"id": "img3",
"isVisible": false,
"right": "23dp",
"skin": "sknslImage",
"src": "tab_deposits_icon_inactive.png",
"top": "4dp",
"width": "28dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var Label04221a71494e848 = new kony.ui.Label({
"centerX": "50%",
"id": "Label04221a71494e848",
"isVisible": false,
"skin": "sknlblmenu",
"text": kony.i18n.getLocalizedString("i18n.common.deposits"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "34dp",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopybtnAccounts0bc68a97c14fb49 = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnCard",
"height": "50dp",
"id": "CopybtnAccounts0bc68a97c14fb49",
"isVisible": true,
"skin": "btnCard",
"text": "J",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
FlxDeposits.add(img3, Label04221a71494e848, CopybtnAccounts0bc68a97c14fb49);
var flxEfawatercoom = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "flxEfawatercoom",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "20%",
"zIndex": 2
}, {}, {});
flxEfawatercoom.setDefaultUnit(kony.flex.DP);
var btnEfawatercoom = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnCardFoc",
"height": "50dp",
"id": "btnEfawatercoom",
"isVisible": true,
"right": "0dp",
"onClick": AS_Button_e963a473d3e842649d8d943f94b08bf1,
"skin": "btnCard",
"text": "x",
"top": "0dp",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 14],
"paddingInPixel": false
}, {});
var lblEfawatercoom = new kony.ui.Label({
"centerX": "50%",
"id": "lblEfawatercoom",
"isVisible": true,
"skin": "sknlblFootertitle",
"text": kony.i18n.getLocalizedString("i18n.footerBill.BillPayment"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "55%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxEfawatercoom.add(btnEfawatercoom, lblEfawatercoom);
var FlxMore = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "100%",
"id": "FlxMore",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"onClick": AS_FlexContainer_bdfd4da9eb104ea2bc06851ca67a1b04,
"skin": "sknslFbox",
"top": "0dp",
"width": "20%",
"zIndex": 1
}, {}, {});
FlxMore.setDefaultUnit(kony.flex.DP);
var img4 = new kony.ui.Image2({
"centerX": "50%",
"height": "28dp",
"id": "img4",
"isVisible": false,
"right": "23dp",
"skin": "sknslImage",
"src": "tab_more_icon_inactive.png",
"top": "4dp",
"width": "28dp"
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var Label0e5331028c2ef41 = new kony.ui.Label({
"centerX": "50%",
"id": "Label0e5331028c2ef41",
"isVisible": true,
"skin": "sknlblFootertitle",
"text": kony.i18n.getLocalizedString("i18n.common.more"),
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "55%",
"width": kony.flex.USE_PREFFERED_SIZE,
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var CopybtnAccounts0ec4d23080f0146 = new kony.ui.Button({
"centerX": "50%",
"centerY": "50%",
"focusSkin": "btnCardFoc",
"height": "50dp",
"id": "CopybtnAccounts0ec4d23080f0146",
"isVisible": true,
"onClick": AS_Button_f92784ea2c1d415b8ac615588b63e8a8,
"skin": "btnCard",
"text": "K",
"width": "100%",
"zIndex": 1
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"displayText": true,
"padding": [ 0, 0,0, 14],
"paddingInPixel": false
}, {});
FlxMore.add(img4, Label0e5331028c2ef41, CopybtnAccounts0ec4d23080f0146);
footerBackground.add( FlxMore, flxEfawatercoom, FlxDeposits, FlxBot, FlxTranfers,FlxAccounts);
footerBack.add(footerBackground);
frmManageCardsKA.add(flxHeaderDashboard, overview, footerBack);
};
function frmManageCardsKAGlobalsAr() {
frmManageCardsKAAr = new kony.ui.Form2({
"addWidgets": addWidgetsfrmManageCardsKAAr,
"bounces": false,
"enableScrolling": false,
"enabledForIdleTimeout": true,
"id": "frmManageCardsKA",
"init": AS_Form_a2f3089586c14ed09e121b2d88633ead,
"layoutType": kony.flex.FLOW_VERTICAL,
"needAppMenu": false,
"postShow": AS_Form_f8a51fc00ee94f90a69129ace255817e,
"preShow": AS_Form_d92cf8a7ef8a450b9d06f2fb4805c227,
"skin": "CopysknmainGradient0j9e216878c1640"
}, {
"displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
"layoutType": kony.flex.FLOW_VERTICAL,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"footerOverlap": false,
"headerOverlap": false,
"inTransitionConfig": {
"formAnimation": 0
},
"menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
"onDeviceBack": AS_Form_cf27ef86f42444f7b4bb0bd5e15cd71c,
"outTransitionConfig": {
"formAnimation": 0
},
"retainScrollPosition": false,
"titleBar": true,
"windowSoftInputMode": constants.FORM_ADJUST_PAN
});
};
