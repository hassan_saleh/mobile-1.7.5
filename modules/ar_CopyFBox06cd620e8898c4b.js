//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:46 EEST 2020
function initializeCopyFBox06cd620e8898c4bAr() {
    CopyFBox06cd620e8898c4bAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "id": "CopyFBox06cd620e8898c4b",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "width": "100%"
    }, {
        "containerWeight": 100
    }, {});
    CopyFBox06cd620e8898c4bAr.setDefaultUnit(kony.flex.DP);
    var lblServiceList = new kony.ui.Label({
        "id": "lblServiceList",
        "isVisible": true,
        "right": "0dp",
        "minHeight": "20dp",
        "skin": "sknNumber",
        "text": kony.i18n.getLocalizedString("i18n.locateus.frmLocatorBranchDetailsKA.CopyatmServices00ff586c7132d45"),
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "hExpand": true,
        "margin": [ 1, 1,1, 1],
        "marginInPixel": false,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false
    });
    CopyFBox06cd620e8898c4bAr.add(lblServiceList);
}
