//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function initializetmpCardsReorderAr() {
    flxCardsReOrderSettingsTmpAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "80dp",
        "id": "flxCardsReOrderSettingsTmp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox"
    }, {}, {});
    flxCardsReOrderSettingsTmp.setDefaultUnit(kony.flex.DP);
    var lblCardsType = new kony.ui.Label({
        "id": "lblCardsType",
        "isVisible": true,
        "right": "8%",
        "skin": "sknlblTouchIdsmall",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCardNum = new kony.ui.Label({
        "id": "lblCardNum",
        "isVisible": true,
        "right": "8%",
        "skin": "sknlblTouchIdsmall",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "42dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 4],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCardCode = new kony.ui.Label({
        "id": "lblCardCode",
        "isVisible": false,
        "right": "500dp",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblN = new kony.ui.Label({
        "height": "0px",
        "id": "lblN",
        "isVisible": true,
        "right": "0dp",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblSH = new kony.ui.Label({
        "height": "0px",
        "id": "lblSH",
        "isVisible": true,
        "right": "0dp",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblPref = new kony.ui.Label({
        "height": "0px",
        "id": "lblPref",
        "isVisible": true,
        "right": "0dp",
        "skin": "slLabel",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var ArrowR = new kony.ui.Label({
        "centerY": "50%",
        "id": "ArrowR",
        "isVisible": false,
        "left": "4%",
        "skin": "sknBackIconDisabled",
        "text": kony.i18n.getLocalizedString("i18n.appsettings.more"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblIncommingRing = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblIncommingRing",
        "isVisible": false,
        "left": "2%",
        "skin": "sknBOJttfwhitee",
        "text": "W",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxCardsReOrderSettingsTmpAr.add(lblCardsType, lblCardNum, lblCardCode, lblN, lblSH, lblPref, ArrowR, lblIncommingRing);
}
