//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function initializesegdirectionInfoKAAr() {
    CopyFlexContainer08af3de49f89842Ar = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100dp",
        "id": "CopyFlexContainer08af3de49f89842",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknCopyslFbox07d05709853a74d"
    }, {}, {});
    CopyFlexContainer08af3de49f89842.setDefaultUnit(kony.flex.DP);
    var lblMilesKA = new kony.ui.Label({
        "height": "26dp",
        "id": "lblMilesKA",
        "isVisible": true,
        "right": "23%",
        "skin": "skn383838LatoRegular107KA",
        "top": "10%",
        "width": "256dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAddress1KA = new kony.ui.Label({
        "height": "55dp",
        "id": "lblAddress1KA",
        "isVisible": true,
        "right": "23%",
        "skin": "skn383838LatoRegular107KA",
        "top": "38%",
        "width": "256dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAddress2KA = new kony.ui.Label({
        "centerY": "81.21%",
        "height": "20dp",
        "id": "lblAddress2KA",
        "isVisible": false,
        "right": "30%",
        "skin": "skn383838LatoRegular107KA",
        "top": "60dp",
        "width": "65%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var contactListDivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "contactListDivider",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "top": "100dp",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    contactListDivider.setDefaultUnit(kony.flex.DP);
    contactListDivider.add();
    var informationListDivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "informationListDivider",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "sknsegmentDivider",
        "top": "87dp",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    informationListDivider.setDefaultUnit(kony.flex.DP);
    informationListDivider.add();
    var imgDirKA = new kony.ui.Image2({
        "centerY": "50%",
        "height": "39%",
        "id": "imgDirKA",
        "isVisible": true,
        "right": "6%",
        "skin": "slImage",
        "src": "imagedrag.png",
        "top": "24dp",
        "width": "10%",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {});
    CopyFlexContainer08af3de49f89842Ar.add(lblMilesKA, lblAddress1KA, lblAddress2KA, contactListDivider, informationListDivider, imgDirKA);
}
