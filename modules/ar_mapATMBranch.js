//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function initializemapATMBranchAr() {
flxMapATMBranchAr = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "140dp",
"id": "flxMapATMBranch",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"skin": "sFBoxToolTip",
"width": "250dp"
}, {}, {});
flxMapATMBranch.setDefaultUnit(kony.flex.DP);
var flxTopDetails = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "120dp",
"id": "flxTopDetails",
"isVisible": true,
"layoutType": kony.flex.FLOW_HORIZONTAL,
"right": "0%",
"skin": "slFbox",
"top": "0%",
"width": "250dp",
"zIndex": 1
}, {}, {});
flxTopDetails.setDefaultUnit(kony.flex.DP);
var flxDistance = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxDistance",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"skin": "slFbox",
"top": "0dp",
"width": "33%",
"zIndex": 1
}, {}, {});
flxDistance.setDefaultUnit(kony.flex.DP);
var lblDistance = new kony.ui.Label({
"height": "50.93%",
"id": "lblDistance",
"isVisible": true,
"right": "0%",
"skin": "slblMap",
"text": "22.2",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "13.89%",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblDistanceUnit = new kony.ui.Label({
"height": "36.04%",
"id": "lblDistanceUnit",
"isVisible": true,
"right": "0%",
"skin": "slblMapmiles",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "46.78%",
"width": "100%"
}, {
"contentAlignment": constants.CONTENT_ALIGN_CENTER,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
flxDistance.add(lblDistance, lblDistanceUnit);
var flxDetails = new kony.ui.FlexContainer({
"autogrowMode": kony.flex.AUTOGROW_NONE,
"clipBounds": true,
"height": "90%",
"id": "flxDetails",
"isVisible": true,
"layoutType": kony.flex.FREE_FORM,
"right": "0dp",
"left": "5dp",
"skin": "slFbox",
"top": "0dp",
"width": 160,
"zIndex": 1
}, {}, {});
flxDetails.setDefaultUnit(kony.flex.DP);
var lblName = new kony.ui.Label({
"height": "33.33%",
"id": "lblName",
"isVisible": true,
"right": "9dp",
"left": "2dp",
"skin": "sknName",
"text": "HDFC",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "11dp"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblAddress = new kony.ui.Label({
"height": "33.33%",
"id": "lblAddress",
"isVisible": true,
"right": "9dp",
"left": "0dp",
"skin": "sknaccountName",
"text": "Kondapur Kondapur  Kondapur  Kondapur Kondapur ",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "31dp"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var lblStatus = new kony.ui.Label({
"height": "33.33%",
"id": "lblStatus",
"isVisible": true,
"right": "28dp",
"left": "0dp",
"skin": "sknstatus",
"text": "CLOSED",
"textStyle": {
"letterSpacing": 0,
"strikeThrough": false
},
"top": "57dp"
}, {
"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {
"textCopyable": false
});
var imgStatus = new kony.ui.Image2({
"height": "45dp",
"id": "imgStatus",
"isVisible": true,
"right": "-9dp",
"skin": "slImage",
"src": "location_open.png",
"top": "55dp",
"width": "45dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
var imgNext = new kony.ui.Image2({
"height": "120dp",
"id": "imgNext",
"isVisible": true,
"left": "0dp",
"skin": "slImage",
"src": "map_drilldown.png",
"top": "0dp",
"width": "16dp",
"zIndex": 1
}, {
"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
"padding": [ 0, 0,0, 0],
"paddingInPixel": false
}, {});
flxDetails.add(lblName, lblAddress, lblStatus, imgStatus, imgNext);
flxTopDetails.add( flxDetails,flxDistance);
flxMapATMBranchAr.add(flxTopDetails);
}
