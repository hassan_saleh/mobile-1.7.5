//Do not Modify!! This is an auto generated module for 'android'. Generated on Wed Aug 19 10:50:47 EEST 2020
function initializetmpQuickBalanceSegAr() {
    flxSegQuickBalanceAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "65dp",
        "id": "flxSegQuickBalance",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox"
    }, {}, {});
    flxSegQuickBalance.setDefaultUnit(kony.flex.DP);
    var lblAccount = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblAccount",
        "isVisible": true,
        "right": "5%",
        "skin": "sknlblTouchIdsmall",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "75%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAccNumber = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblAccNumber",
        "isVisible": false,
        "right": "0%",
        "skin": "sknlblTouchIdsmall",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "101dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblIncommingTick = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblIncommingTick",
        "isVisible": true,
        "left": "3.80%",
        "skin": "sknBOJttfwhiteeSmall",
        "text": "r",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblIncommingRing = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblIncommingRing",
        "isVisible": true,
        "left": "2%",
        "skin": "sknBOJttfwhitee",
        "text": "s",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAccountName = new kony.ui.Label({
        "id": "lblAccountName",
        "isVisible": false,
        "right": "100%",
        "skin": "CopylblSegName0f5b732ec6b8142",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
        "padding": [ 0, 0,0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxSegQuickBalanceAr.add(lblAccount, lblAccNumber, lblIncommingTick, lblIncommingRing, lblAccountName);
}
