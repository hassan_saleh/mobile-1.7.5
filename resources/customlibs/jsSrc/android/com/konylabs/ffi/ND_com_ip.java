package com.konylabs.ffi;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import com.konylabs.api.TableLib;
import com.konylabs.vm.LuaTable;



import com.cpcg.network.IPAddr;
import com.konylabs.libintf.Library;
import com.konylabs.libintf.JSLibrary;
import com.konylabs.vm.LuaError;
import com.konylabs.vm.LuaNil;


public class ND_com_ip extends JSLibrary {

 
 
	public static final String getIP = "getIP";
 
	String[] methods = { getIP };


 Library libs[] = null;
 public Library[] getClasses() {
 libs = new Library[0];
 return libs;
 }



	public ND_com_ip(){
	}

	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		case 0:
 if (paramLen != 0){ return new Object[] {new Double(100),"Invalid Params"}; }
 ret = this.getIP( );
 
 			break;
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "com.ip";
	}


	/*
	 * return should be status(0 and !0),address
	 */
 
 
 	public final Object[] getIP( ){
 
		Object[] ret = null;
 java.lang.String val = com.cpcg.network.IPAddr.getIP( );
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
};
