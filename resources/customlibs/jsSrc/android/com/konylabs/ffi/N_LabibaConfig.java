package com.konylabs.ffi;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import com.konylabs.api.TableLib;
import com.konylabs.vm.LuaTable;



import com.labiba.bot.Others.LabibaConfig;
import com.konylabs.libintf.Library;
import com.konylabs.libintf.JSLibrary;
import com.konylabs.vm.LuaError;
import com.konylabs.vm.LuaNil;


public class N_LabibaConfig extends JSLibrary {

 
 
	public static final String connLabiba = "connLabiba";
 
	String[] methods = { connLabiba };


 Library libs[] = null;
 public Library[] getClasses() {
 libs = new Library[0];
 return libs;
 }



	public N_LabibaConfig(){
	}

	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		case 0:
 if (paramLen != 3){ return new Object[] {new Double(100),"Invalid Params"}; }
 java.lang.String language0 = null;
 if(params[0] != null && params[0] != LuaNil.nil) {
 language0 = (java.lang.String)params[0];
 }
 java.lang.String custID0 = null;
 if(params[1] != null && params[1] != LuaNil.nil) {
 custID0 = (java.lang.String)params[1];
 }
 com.konylabs.vm.Function lKonyCallBack0 = null;
 if(params[2] != null && params[2] != LuaNil.nil) {
 lKonyCallBack0 = (com.konylabs.vm.Function)params[2];
 }
 ret = this.connLabiba( language0, custID0, lKonyCallBack0 );
 
 			break;
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "LabibaConfig";
	}


	/*
	 * return should be status(0 and !0),address
	 */
 
 
 	public final Object[] connLabiba( java.lang.String inputKey0, java.lang.String inputKey1, com.konylabs.vm.Function inputKey2 ){
 
		Object[] ret = null;
 java.lang.String val = com.labiba.bot.Others.LabibaConfig.connLabiba( inputKey0
 , inputKey1
 , (com.konylabs.vm.Function)inputKey2
 );
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
};
