package com.konylabs.ffi;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import com.konylabs.api.TableLib;
import com.konylabs.vm.LuaTable;



import labiba.bank.labiba_banks.Others.LabibaOnBoardingConfig;
import com.konylabs.libintf.Library;
import com.konylabs.libintf.JSLibrary;
import com.konylabs.vm.LuaError;
import com.konylabs.vm.LuaNil;


public class N_onboardingNav extends JSLibrary {

 
 
	public static final String SMS_URL = "SMS_URL";
 
 
	public static final String MOBILE_FORM_URL = "MOBILE_FORM_URL";
 
 
	public static final String setSEND_IMAGES_URL = "setSEND_IMAGES_URL";
 
	String[] methods = { SMS_URL, MOBILE_FORM_URL, setSEND_IMAGES_URL };


 Library libs[] = null;
 public Library[] getClasses() {
 libs = new Library[0];
 return libs;
 }



	public N_onboardingNav(){
	}

	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		case 0:
 if (paramLen != 1){ return new Object[] {new Double(100),"Invalid Params"}; }
 java.lang.String URL0 = null;
 if(params[0] != null && params[0] != LuaNil.nil) {
 URL0 = (java.lang.String)params[0];
 }
 ret = this.SMS_URL( URL0 );
 
 			break;
 		case 1:
 if (paramLen != 1){ return new Object[] {new Double(100),"Invalid Params"}; }
 java.lang.String form_url1 = null;
 if(params[0] != null && params[0] != LuaNil.nil) {
 form_url1 = (java.lang.String)params[0];
 }
 ret = this.MOBILE_FORM_URL( form_url1 );
 
 			break;
 		case 2:
 if (paramLen != 2){ return new Object[] {new Double(100),"Invalid Params"}; }
 java.lang.String BaseURL2 = null;
 if(params[0] != null && params[0] != LuaNil.nil) {
 BaseURL2 = (java.lang.String)params[0];
 }
 java.lang.String BodyURL2 = null;
 if(params[1] != null && params[1] != LuaNil.nil) {
 BodyURL2 = (java.lang.String)params[1];
 }
 ret = this.setSEND_IMAGES_URL( BaseURL2, BodyURL2 );
 
 			break;
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "onboardingNav";
	}


	/*
	 * return should be status(0 and !0),address
	 */
 
 
 	public final Object[] SMS_URL( java.lang.String inputKey0 ){
 
		Object[] ret = null;
 labiba.bank.labiba_banks.Others.LabibaOnBoardingConfig.setSMS_URL( inputKey0
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] MOBILE_FORM_URL( java.lang.String inputKey0 ){
 
		Object[] ret = null;
 labiba.bank.labiba_banks.Others.LabibaOnBoardingConfig.setMOBILE_FORM_URL( inputKey0
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] setSEND_IMAGES_URL( java.lang.String inputKey0, java.lang.String inputKey1 ){
 
		Object[] ret = null;
 labiba.bank.labiba_banks.Others.LabibaOnBoardingConfig.setSEND_IMAGES_URL( inputKey0
 , inputKey1
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
};
